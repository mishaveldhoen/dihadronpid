// Test reading a run list.

void testReadRunList(const char* fileName) {
	
	ifstream in;
	in.open(fileName);
	int runnumber;
	int nRuns = 0;

	while (in.good()) {
		in >> runnumber;			// Expects collumn, no commas.
		cout << runnumber << endl;
		nRuns++;
	}
	in.close();

	cout << "Number of runs in the list: " << nRuns << endl;

}
