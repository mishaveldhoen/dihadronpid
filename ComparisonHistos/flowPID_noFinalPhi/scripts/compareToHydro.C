double leftheader = 0.22;
int color[8] = { kBlack, kOrange+1, kBlack, kGreen+2, kRed-3, kBlue-3, kMagenta+2,kPink+9};
int colorReflection[8] = { kBlack, kBlack, kBlack, kBlack, kBlack, kBlack, kMagenta+2,kPink+9};
Int_t gMarkerStyle[8] = {24,22,20,21,20,29,23,30};
Int_t gMarkerStyleReflection[8] = {24,26,27,25,24,30,23,30};
//Double_t gMarkerSize[8] = {1.2,1.2,1.2,1.2,1.6,1.2,1.2,1.2};
Double_t gMarkerSize[8] = {1.0,1.0,1.0,1.0,1.0,1.2,1.0,1.2};

void compareToHydro() {
  gROOT->LoadMacro("helper.C");
  TCanvas *fig = new TCanvas("fig","fig");//,1200,650);
  fig->SetRightMargin(0.2);
  fig->SetLeftMargin(0.2);
  fig->SetBottomMargin(0.2);

  TLatex *texting = new TLatex();
  texting->SetTextFont(42);

  fig->Divide(2,2,0.00,0.00);
  fig->cd(1); PilePlots("1020","Centrality: 10-20\%",5.9, true, 1 );
  fig->cd(3); PilePlots("1020","Centrality: 10-20\%",5.9, true, 2 );
  texting->SetTextSize(0.085);
  texting->DrawLatex(2.6,-0.08,"#font[12]{p}_{T}");

  fig->cd(2); PilePlots("4050","Centrality: 40-50\%",5.9, false, 1);
  fig->cd(4); PilePlots("4050","Centrality: 40-50\%",5.9, false, 2);
  texting->SetTextSize(0.08);
  texting->DrawLatex(0.0,-0.08,"(GeV/#font[12]{c})");

  TString saveas = "../../fig/v2DataVsHydro.eps";
  fig->SaveAs(saveas.Data(),"EPS");
  printf("%s has been saved\n",saveas.Data());
}

TLegend* PilePlots(TString cc,TString header, double maxX, bool left, Int_t iGroup) {
  double xfactor = left?1:1/(0.96-leftheader);
  TGraphAsymmErrors* pion  = getGraph( Form("../pion_v2_SPVZE_%s.txt",  cc.Data()),1,color[0], gMarkerStyle[0]);
  if(pion) pion->SetMarkerSize(gMarkerSize[0]);
  TGraphAsymmErrors* kaon  = getGraph( Form("../kaon_v2_SPVZE_%s.txt",  cc.Data()),1,color[1], gMarkerStyle[1] );
  if(kaon) kaon->SetMarkerSize(gMarkerSize[1]);
  TGraphAsymmErrors* kzero = getGraph( Form("../kzero_v2_SPVZE_%s.txt", cc.Data()),1,color[2], gMarkerStyle[2] );
  if(kzero) kzero->SetMarkerSize(gMarkerSize[2]);
  TGraphAsymmErrors* proton= getGraph( Form("../proton_v2_SPVZE_%s.txt",cc.Data()),1,color[3], gMarkerStyle[3] );
  if(proton) proton->SetMarkerSize(gMarkerSize[3]);
  TGraphAsymmErrors* phi   = getGraph( Form("../phi_v2_SPVZE_%s.txt",   cc.Data()),1,color[4], gMarkerStyle[4] );
  if(phi) phi->SetMarkerSize(gMarkerSize[4]);
  TGraphAsymmErrors* lambda= getGraph( Form("../lambda_v2_SPVZE_%s.txt",cc.Data()),1,color[5], gMarkerStyle[5] );
  if(lambda) lambda->SetMarkerSize(gMarkerSize[5]);
  TGraphAsymmErrors* xi    = getGraph( Form("../xi_v2_SPVZE_%s.txt",    cc.Data()),1,color[6], gMarkerStyle[6] );
  if(xi) xi->SetMarkerSize(gMarkerSize[6]);
  TGraphAsymmErrors* omega = getGraph( Form("../omega_v2_SPVZE_%s.txt", cc.Data()),1,color[7], gMarkerStyle[7] );
  if(omega) omega->SetMarkerSize(gMarkerSize[7]);

  //Reflection with same markers and black perimeter for visibilit
  TGraphAsymmErrors* pionReflection  = getGraph( Form("../pion_v2_SPVZE_%s.txt",  cc.Data()),1,colorReflection[0], gMarkerStyleReflection[0]);
  if(pionReflection) pionReflection->SetMarkerSize(gMarkerSize[0]);
  TGraphAsymmErrors* kaonReflection  = getGraph( Form("../kaon_v2_SPVZE_%s.txt",  cc.Data()),1,colorReflection[1], gMarkerStyleReflection[1] );
  if(kaonReflection) kaonReflection->SetMarkerSize(gMarkerSize[1]);
  TGraphAsymmErrors* kzeroReflection = getGraph( Form("../kzero_v2_SPVZE_%s.txt", cc.Data()),1,colorReflection[2], gMarkerStyleReflection[2] );
  if(kzeroReflection) kzeroReflection->SetMarkerSize(gMarkerSize[2]);
  TGraphAsymmErrors* protonReflection= getGraph( Form("../proton_v2_SPVZE_%s.txt",cc.Data()),1,colorReflection[3], gMarkerStyleReflection[3] );
  if(protonReflection) protonReflection->SetMarkerSize(gMarkerSize[3]);
  TGraphAsymmErrors* phiReflection   = getGraph( Form("../phi_v2_SPVZE_%s.txt",   cc.Data()),1,colorReflection[4], gMarkerStyleReflection[4] );
  if(phiReflection) phiReflection->SetMarkerSize(gMarkerSize[4]);
  TGraphAsymmErrors* lambdaReflection= getGraph( Form("../lambda_v2_SPVZE_%s.txt",cc.Data()),1,colorReflection[5], gMarkerStyleReflection[5] );
  if(lambdaReflection) lambdaReflection->SetMarkerSize(gMarkerSize[5]);
  TGraphAsymmErrors* xiReflection    = getGraph( Form("../xi_v2_SPVZE_%s.txt",    cc.Data()),1,colorReflection[6], gMarkerStyleReflection[6] );
  if(xiReflection) xiReflection->SetMarkerSize(gMarkerSize[6]);
  TGraphAsymmErrors* omegaReflection = getGraph( Form("../omega_v2_SPVZE_%s.txt", cc.Data()),1,colorReflection[7], gMarkerStyleReflection[7] );
  if(omegaReflection) omegaReflection->SetMarkerSize(gMarkerSize[7]);

  //Systematic errors shown with shaded area
  TGraphAsymmErrors* pionSyst  = getGraph( Form("../pion_v2_SPVZE_%s.txt",  cc.Data()),2,color[0], gMarkerStyle[0]);
  if(pionSyst) {
    pionSyst->SetMarkerSize(gMarkerSize[0]);
    pionSyst->SetFillStyle(3001);
    pionSyst->SetFillColor(color[0]);
  }
  TGraphAsymmErrors* kaonSyst  = getGraph( Form("../kaon_v2_SPVZE_%s.txt",  cc.Data()),2,color[1], gMarkerStyle[1] );
  if(kaonSyst) {
    kaonSyst->SetMarkerSize(gMarkerSize[1]);
    kaonSyst->SetFillStyle(3001);
    kaonSyst->SetFillColor(color[1]);
  }
  TGraphAsymmErrors* kzeroSyst = getGraph( Form("../kzero_v2_SPVZE_%s.txt", cc.Data()),2,color[2], gMarkerStyle[2] );
  if(kzeroSyst) {
    kzeroSyst->SetMarkerSize(gMarkerSize[2]);
    kzeroSyst->SetFillStyle(3001);
    kzeroSyst->SetFillColor(color[2]);
  }
  TGraphAsymmErrors* protonSyst= getGraph( Form("../proton_v2_SPVZE_%s.txt",cc.Data()),2,color[3], gMarkerStyle[3] );
  if(protonSyst) {
    protonSyst->SetMarkerSize(gMarkerSize[3]);
    protonSyst->SetFillStyle(3001);
    protonSyst->SetFillColor(color[3]);
  }
  TGraphAsymmErrors* phiSyst   = getGraph( Form("../phi_v2_SPVZE_%s.txt",   cc.Data()),2,color[4], gMarkerStyle[4] );
  if(phiSyst) {
    phiSyst->SetMarkerSize(gMarkerSize[4]);
    phiSyst->SetFillStyle(3001);
    phiSyst->SetFillColor(color[4]);
  }
  TGraphAsymmErrors* lambdaSyst= getGraph( Form("../lambda_v2_SPVZE_%s.txt",cc.Data()),2,color[5], gMarkerStyle[5] );
  if(lambdaSyst) {
    lambdaSyst->SetMarkerSize(gMarkerSize[5]);
    lambdaSyst->SetFillStyle(3001);
    lambdaSyst->SetFillColor(color[5]);
  }
  TGraphAsymmErrors* xiSyst    = getGraph( Form("../xi_v2_SPVZE_%s.txt",    cc.Data()),2,color[6], gMarkerStyle[6] );
  if(xiSyst) {
    xiSyst->SetMarkerSize(gMarkerSize[6]);
    xiSyst->SetFillStyle(3001);
    xiSyst->SetFillColor(color[6]);
  }
  TGraphAsymmErrors* omegaSyst = getGraph( Form("../omega_v2_SPVZE_%s.txt", cc.Data()),2,color[7], gMarkerStyle[7] );
  if(omegaSyst) {
    omegaSyst->SetMarkerSize(gMarkerSize[7]);
    omegaSyst->SetFillStyle(3001);
    omegaSyst->SetFillColor(color[7]);
  }

  TGraph *vispion   = getGraphFromVIS( Form("../models/VISH/Pionv2_C%s.dat",   cc.Data()),color[0],1 );
  TGraph *viskaon   = getGraphFromVIS( Form("../models/VISH/Kaon0_C%s.dat",    cc.Data()),color[1],1 );
  TGraph *visproton = getGraphFromVIS( Form("../models/VISH/Protonv2_C%s.dat", cc.Data()),color[3],1 );
  TGraph *visphi    = getGraphFromVIS2( Form("../models/VISH/phiv2_C%s.dat",   cc.Data()),color[4],1 );
  TGraph *vislambda = getGraphFromVIS( Form("../models/VISH/Lambda_C%s.dat",   cc.Data()),color[5],1 );
  TGraph *visxi     = getGraphFromVIS( Form("../models/VISH/Xi_C%s.dat",       cc.Data()),color[6],1 );


  TGraph *vnupion   = getGraphFromVIS( Form("../models/VISHNU/Pionv2_C%s.dat",   cc.Data()),color[0],1,1 );
  TGraph *vnukaon   = getGraphFromVIS( Form("../models/VISHNU/Kaonv2_C%s.dat",    cc.Data()),color[1],1,1 );
  TGraph *vnuproton = getGraphFromVIS( Form("../models/VISHNU/Protonv2_C%s.dat", cc.Data()),color[3],1,2 );
  TGraph *vnuphi    = getGraphFromVIS( Form("../models/VISHNU/Phiv2_C%s.dat",   cc.Data()),color[4],1,9 );
  TGraph *vnulambda = getGraphFromVIS( Form("../models/VISHNU/Lambdav2_C%s.dat",   cc.Data()),color[5],1,7 );
  TGraph *vnuxi     = getGraphFromVIS( Form("../models/VISHNU/Xiv2_C%s.dat",       cc.Data()),color[6],1,7 );
  TGraph *vnuomega     = getGraphFromVIS( Form("../models/VISHNU/Omegav2_C%s.dat",       cc.Data()),color[7],1,4 );

  TH2D *axis = new TH2D( Form("MultiGraph_%s-%d",cc.Data(),iGroup),"",
			 10, -0.11, 2.93,
			 10, -0.02, +0.32 );
  axis->SetStats( kFALSE );
  axis->SetTitleSize(33);
  //if(iGroup == 1) axis->SetTitle(header.Data());
  //else axis->SetTitle("");

  //axis->GetXaxis()->SetDecimals( kTRUE );
  //axis->GetYaxis()->SetDecimals( kTRUE );
  axis->GetXaxis()->CenterTitle( kFALSE);
  axis->GetYaxis()->SetNdivisions(506);
  TString extra = "";
  if(left) {
    axis->GetYaxis()->SetTitle("v_{2}(SP,|#Delta#eta| > 2.0)");
    //axis->GetXaxis()->SetLabelOffset(+0.0);
    //axis->GetXaxis()->SetTitleOffset(+0.7);
    //axis->GetXaxis()->SetTitle("p_{T}");
  } else {
    axis->GetYaxis()->SetTitle("");
    //axis->GetXaxis()->SetLabelOffset(-0.1*(1-1/xfactor));
    //extra = "Y+";
    //axis->GetXaxis()->SetTitle("(GeV/c)");
    //axis->GetXaxis()->SetTitleOffset(+0.7/xfactor);
  }
  cout << xfactor << endl;
  
  axis->GetYaxis()->SetNdivisions(5);
  if(iGroup == 1) axis->GetXaxis()->SetNdivisions(0);
  else 
    axis->GetXaxis()->SetNdivisions(5);
  if(iGroup == 1) {
    axis->GetYaxis()->SetTitleOffset(+1.0);
    axis->GetXaxis()->SetTitleSize(0.085*xfactor);
    axis->GetYaxis()->SetTitleSize(0.085*xfactor);
    axis->GetXaxis()->SetLabelSize(0.085*xfactor);
    axis->GetYaxis()->SetLabelSize(0.085*xfactor);
  }
  else if(iGroup == 2) {
    axis->GetYaxis()->SetTitleOffset(+1.2);
    axis->GetXaxis()->SetTitleSize(0.07*xfactor);
    axis->GetYaxis()->SetTitleSize(0.07*xfactor);
    if(left) 
      axis->GetXaxis()->SetLabelSize(0.07*xfactor);
    else 
      axis->GetXaxis()->SetLabelSize(0.055*xfactor);
    axis->GetYaxis()->SetLabelSize(0.07*xfactor);
  }
  axis->Draw(extra.Data());

  /*if(visxi) visxi->Draw("csame");
  if(vispion) vispion->Draw("csame");
  if(viskaon) viskaon->Draw("csame");
  if(visproton) visproton->Draw("csame");
  if(visphi) visphi->Draw("csame");
  if(vislambda) vislambda->Draw("csame");*/

  
  if(iGroup == 1) {
    if(pionSyst) pionSyst->Draw("p2");
    if(protonSyst) protonSyst->Draw("p2");
    if(lambdaSyst) lambdaSyst->Draw("p2");

    if(pion) pion->Draw("psame");
    if(proton) proton->Draw("psame");
    if(lambda) lambda->Draw("psame");

    if(pionReflection) pionReflection->Draw("psame");
    if(protonReflection) protonReflection->Draw("psame");
    if(lambdaReflection) lambdaReflection->Draw("psame");

    if(vnupion) vnupion->Draw("csame");
    if(vnuproton) vnuproton->Draw("csame");
    if(vnulambda) vnulambda->Draw("csame");
  }

  if(iGroup == 2) {
    if(vnukaon) vnukaon->Draw("csame");
    if(vnuphi) vnuphi->Draw("csame");
    if(vnuxi) vnuxi->Draw("csame");
    if(vnuomega) vnuomega->Draw("csame");
    
    if(kaonSyst) kaonSyst->Draw("p2");
    if(kzeroSyst) kzeroSyst->Draw("p2");
    if(xiSyst) xiSyst->Draw("p2");
    if(phiSyst) phiSyst->Draw("p2");
    if(omegaSyst) omegaSyst->Draw("p2");

    if(kaon) kaon->Draw("psame");
    if(kzero) kzero->Draw("psame");
    if(xi) xi->Draw("psame");
    if(phi) phi->Draw("psame");
    if(omega) omega->Draw("psame");

    if(kaonReflection) kaonReflection->Draw("psame");
    if(kzeroReflection) kzeroReflection->Draw("psame");
    if(xiReflection) xiReflection->Draw("psame");
    if(phiReflection) phiReflection->Draw("psame");
    if(omegaReflection) omegaReflection->Draw("psame");
  }


  TGraph *visleg = new TGraph();
  visleg->SetLineColor(kBlack);
  visleg->SetLineStyle(2);
  visleg->SetFillColor(kWhite);

  TGraph *vnuleg = new TGraph();
  vnuleg->SetLineColor(kBlack);
  vnuleg->SetLineStyle(1);
  vnuleg->SetLineWidth(5);
  vnuleg->SetFillColor(kWhite);

  TLatex head;
  head.SetTextFont(42);
  head.SetNDC();
  head.SetTextSize(0.085);
  if(iGroup == 1) {
    if(left) head.DrawLatex(0.42,0.92,header.Data());
    else head.DrawLatex(0.33,0.92,header.Data());
  }
  TLegend *one = new TLegend(0.25,0.57,0.4,0.9);
  one->SetLineColor(10);
  if(iGroup == 1) one->SetTextSize(0.075);
  else if(iGroup == 2) one->SetTextSize(0.065);
  one->SetHeader("ALICE");
  if(iGroup == 1) {
    if(pion)   one->AddEntry(pion,"");
    if(proton) one->AddEntry(proton,"");
    if(lambda) one->AddEntry(lambda,"");
  }
  else if(iGroup == 2) {
    if(kaon)   one->AddEntry(kaon,"");
    //if(kzero)  one->AddEntry(kzero,"K^{0}_{s}");
    if(phi)    one->AddEntry(phi,"");
    if(xi)     one->AddEntry(xi,"");
    if(omega)  one->AddEntry(omega,"");
  }
  //one->AddEntry(visleg,"VISH2+1");
  //one->AddEntry(vnuleg,"VISHNU");
  one->SetFillColor( kWhite );
  //one->SetNColumns(2);
  if(left) one->Draw();

  TLegend *two = new TLegend(0.42,0.57,0.65,0.9);
  two->SetLineColor(10);
  if(iGroup == 1) two->SetTextSize(0.075);
  else if(iGroup == 2) two->SetTextSize(0.065);
  two->SetHeader("VISHNU");
  if(iGroup == 1) {
    if(vnupion) two->AddEntry(vnupion,"#pi^{#pm}");
    if(vnuproton) two->AddEntry(vnuproton,"p(#bar{p})");
    if(vnulambda) two->AddEntry(vnulambda,"#Lambda(#bar{#Lambda})");
  }
  else if(iGroup == 2) {
   if(vnukaon) two->AddEntry(vnukaon,"K^{#pm}");
    if(vnuphi) two->AddEntry(vnuphi,"#phi");
    if(vnuxi) two->AddEntry(vnuxi,"#Xi^{-}(#bar{#Xi}^{+})");
    if(vnuomega) two->AddEntry(vnuomega,"#Omega^{-}(#bar{#Omega}^{+})");
  }
  two->SetFillColor( kWhite );
  if(left) two->Draw();

  return one;
}

TGraphErrors* getGraphFromVIS( TString loadfrom, Int_t color=kBlack, Int_t marker=20, Int_t style=2 ) {
  //cout<<"\n";
  //cout<<loadfrom.Data()<<endl;
  ifstream fin;
  fin.open( loadfrom.Data() );
  double x[100], y[100], ex[100], ey[100], dump;
  TString line;
  Int_t n=0;
  for(;!fin.eof();++n) {
    fin >> x[n];
    if( !fin.good() ) break;
    fin >> dump >> y[n] >> ey[n];
    ex[n] = 0;
    ey[n] = 0.0 ;
    //cout<<x[n] << " "<<y[n]<<endl;
  }
  n--;
  fin.close();
  if(n<1) return NULL;
  TGraphErrors *one = new TGraphErrors(n,x,y,ex,ey);
  format(one,marker,color,"");
  one->SetLineWidth(3);
  one->SetLineStyle(style);
  return one;
}

TGraphErrors* getGraphFromVIS2( TString loadfrom, Int_t color=kBlack, Int_t marker=20 ) {
  ifstream fin;
  fin.open( loadfrom.Data() );
  double x[100], y[100], ex[100], ey[100], dump;
  TString line;
  Int_t n=0;
  for(;!fin.eof();++n) {
    fin >> x[n];
    if( !fin.good() ) break;
    fin >> dump >> dump >> y[n] >> ey[n];
    ex[n] = 0.0 ;
    ey[n] = 0.0 ;
  }
  n--;
  fin.close();
  if(n<1) return NULL;
  TGraphErrors *one = new TGraphErrors(n,x,y,ex,ey);
  format(one,marker,color,"");
  one->SetLineWidth(1);
  one->SetLineStyle(2);
  return one;
}
