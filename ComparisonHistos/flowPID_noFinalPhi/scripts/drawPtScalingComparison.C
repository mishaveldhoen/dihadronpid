TF1 *fit1 = new TF1("fit1","pol7");
TF1 *fit2 = new TF1("fit2","pol7");
TF1 *fit3 = new TF1("fit3","pol7");
TF1 *fit4 = new TF1("fit4","pol7");
TF1 *fit5 = new TF1("fit5","pol7");
TF1 *fit6 = new TF1("fit6","pol7");

int color[8] = { kRed-3 ,kBlack, kYellow+1, kGreen+2,  kBlack,   kBlue-3, kMagenta+2,kPink+9};
int colorReflection[8] = { kBlack, kBlack, kYellow+1, kBlack, kBlack, kBlack, kMagenta+2,kPink+9};
Int_t gMarkerStyle[8] = {20,27,22,21,28,29,30,23};
Int_t gMarkerStyleReflection[8] = {24,27,26,25,28,30,30,23};
//Double_t gMarkerSize[8] = {1.2,1.2,1.2,1.2,1.6,1.2,1.2};
Double_t gMarkerSize[8] = {1.0,1.4,1.0,1.0,1.0,1.2,1.0,1.2};

void drawPtScalingComparison() {
  gROOT->LoadMacro("helper.C");
  gSystem->Load("libANALYSIS.so");
  gSystem->Load("libANALYSISalice.so");

  TLatex head;
  head.SetTextFont(42);
  head.SetNDC();
  head.SetTextSize(0.045);

  //Centrality 10-20%
  TCanvas *fig1 = new TCanvas("fig1","fig1",0,0,1200,600);
  fig1->SetLeftMargin(0.20);
  fig1->SetRightMargin(0.20);
  fig1->SetBottomMargin(0.20);
  fig1->Divide(2,1,0.00,0.00);
  fig1->cd(1); PilePlots("1020","10-20\%","fit1",kTRUE,kTRUE)->Draw();
  fig1->cd(2); PilePlots("1020","10-20\%","fit2",kFALSE,kFALSE)->Draw();
  fig1->cd(3); 
  TString saveas1 = "../../fig/v2nqRhicComparison10To20.eps";
  fig1->SaveAs(saveas1.Data(),"EPS");
  printf("%s has been saved\n",saveas1.Data());

  TCanvas *fig2 = new TCanvas("fig2","fig2",100,100,1000,800);
  fig2->SetLeftMargin(0.20);
  fig2->SetBottomMargin(0.20);
  //fig2->SetGridy();
  PileRatio("1020","10-20\%",fit1,fit2,"(#it{v}_{2}/#it{n}_{q}) / (#it{v}_{2}/#it{n}_{q})_{Fit p}",0.45);
  head.DrawLatex(0.52,0.85,"10-20\%");
  TString saveas2 = "../../fig/v2nqRhicComparisonDoubleRatio10To20.eps";
  fig2->SaveAs(saveas2.Data(),"EPS");
  printf("%s has been saved\n",saveas2.Data());

  //Centrality 20-40%
  TCanvas *fig3 = new TCanvas("fig3","fig3",0,0,1200,600);
  fig3->SetLeftMargin(0.20);
  fig3->SetRightMargin(0.20);
  fig3->SetBottomMargin(0.20);
  fig3->Divide(2,1,0.00,0.00);
  fig3->cd(1); PilePlots("2040","20-40\%","fit3",kTRUE,kTRUE)->Draw();
  fig3->cd(2); PilePlots("2040","20-40\%","fit4",kFALSE,kFALSE)->Draw();
  fig3->cd(3); 
  TString saveas3 = "../../fig/v2nqRhicComparison20To40.eps";
  fig3->SaveAs(saveas3.Data(),"EPS");
  printf("%s has been saved\n",saveas3.Data());

  TCanvas *fig4 = new TCanvas("fig4","fig4",100,100,1000,800);
  fig4->SetLeftMargin(0.20);
  fig4->SetBottomMargin(0.20);
  //fig4->SetGridy();
  PileRatio("2040","20-40\%",fit3,fit4,"(#it{v}_{2}/#it{n}_{q}) / (#it{v}_{2}/#it{n}_{q})_{Fit p}",0.45);
  head.DrawLatex(0.52,0.85,"20-40\%");
  TString saveas4 = "../../fig/v2nqRhicComparisonDoubleRatio20To40.eps";
  fig4->SaveAs(saveas4.Data(),"EPS");
  printf("%s has been saved\n",saveas4.Data());

  //Centrality 40-60%
  TCanvas *fig5 = new TCanvas("fig5","fig5",0,0,1200,600);
  fig5->SetLeftMargin(0.20);
  fig5->SetRightMargin(0.20);
  fig5->SetBottomMargin(0.20);
  fig5->Divide(2,1,0.00,0.00);
  fig5->cd(1); PilePlots("4060","40-60\%","fit5",kTRUE,kTRUE)->Draw();
  fig5->cd(2); PilePlots("4060","40-60\%","fit6",kFALSE,kFALSE)->Draw();
  fig5->cd(3); 
  TString saveas5 = "../../fig/v2nqRhicComparison40To60.eps";
  fig5->SaveAs(saveas5.Data(),"EPS");
  printf("%s has been saved\n",saveas5.Data());

  TCanvas *fig6 = new TCanvas("fig6","fig6",100,100,1000,800);
  fig6->SetLeftMargin(0.20);
  fig6->SetBottomMargin(0.20);
  //fig6->SetGridy();
  PileRatio("4060","40-60\%",fit5,fit6,"(#it{v}_{2}/#it{n}_{q}) / (#it{v}_{2}/#it{n}_{q})_{Fit p}",0.45);
  head.DrawLatex(0.52,0.85,"40-60\%");
  TString saveas6 = "../../fig/v2nqRhicComparisonDoubleRatio40To60.eps";
  fig6->SaveAs(saveas6.Data(),"EPS");
  printf("%s has been saved\n",saveas6.Data());

  //All ratios 
  TCanvas *figAll = new TCanvas("figAll","figAll",0,0,1200,600);
  figAll->SetFixedAspectRatio(kTRUE);
  figAll->SetLeftMargin(0.20);
  figAll->SetRightMargin(0.20);
  figAll->SetBottomMargin(0.20);
  figAll->Divide(3,1,0.00,0.00);
  figAll->cd(1);   
  TH2D *axis = PileRatio("1020","10-20\%",fit1,fit2,"(#it{v}_{2}/#it{n}_{q}) / (#it{v}_{2}/#it{n}_{q})_{Fit p}",0.7,kTRUE,kFALSE,0.015,kFALSE);
  head.SetTextSize(0.06);
  head.DrawLatex(0.48,0.93,"10-20\%");
  axis->GetXaxis()->SetLabelOffset(1.35);
  figAll->cd(2); 
  PileRatio("2040","20-40\%",fit3,fit4,"",0.9,kFALSE,kTRUE,0.005,kFALSE);
  head.SetTextSize(0.075);
  head.DrawLatex(0.42,0.93,"20-40\%");
  figAll->cd(3); 
  PileRatio("4060","40-60\%",fit5,fit6,"",0.9,kFALSE,kFALSE,0.005,kFALSE);
  head.DrawLatex(0.43,0.93,"40-60\%");
  
  TString saveasAll = "../../fig/v2nqRhicComparisonDoubleRatioAll.eps";
  figAll->SaveAs(saveasAll.Data(),"EPS");
  printf("%s has been saved\n",saveasAll.Data());


}

TLegend* PilePlots(TString cc,TString header,TString fit, Bool_t left,Bool_t kAlice) {
  gStyle->SetTitleX(0.5);
  gStyle->SetTitleAlign(23);

  //Central values
  TGraphAsymmErrors* pion   = 0x0;
  TGraphAsymmErrors* kaon   = 0x0;
  TGraphAsymmErrors* proton   = 0x0;
  TGraphAsymmErrors* pionSyst   = 0x0;
  TGraphAsymmErrors* kaonSyst   = 0x0;
  TGraphAsymmErrors* protonSyst   = 0x0;
  TGraphAsymmErrors* pionReflection   = 0x0;
  TGraphAsymmErrors* kaonReflection   = 0x0;
  TGraphAsymmErrors* protonReflection   = 0x0;
  
  if(kAlice) {
    //Central values 
    pion = NScale( Form("../pion_v2_SPVZE_%s.txt",  cc.Data()),1,color[0],gMarkerStyle[0],2 );
    kaon = NScale( Form("../kaonAvg_v2_SPVZE_%s.txt",  cc.Data()),1,color[1],gMarkerStyle[1],2 );
    proton = NScale( Form("../proton_v2_SPVZE_%s.txt",cc.Data()),1,color[3],gMarkerStyle[3],3 );
    
    //Central values reflection (plot visibility)
    pionReflection = NScale( Form("../pion_v2_SPVZE_%s.txt",  cc.Data()),1,colorReflection[0],gMarkerStyleReflection[0],2 );
    kaonReflection = NScale( Form("../kaonAvg_v2_SPVZE_%s.txt",  cc.Data()),1,colorReflection[1],gMarkerStyleReflection[1],2 );
    protonReflection = NScale( Form("../proton_v2_SPVZE_%s.txt",cc.Data()),1,colorReflection[3],gMarkerStyleReflection[3],3 );

    //Systematic errors
    pionSyst   = NScale( Form("../pion_v2_SPVZE_%s.txt",  cc.Data()),2,color[0],gMarkerStyle[0],2 );
    if(pionSyst) {
      pionSyst->SetMarkerSize(gMarkerSize[0]);
      pionSyst->SetFillStyle(0);
      pionSyst->SetFillColor(color[0]);
    }
    kaonSyst   = NScale( Form("../kaonAvg_v2_SPVZE_%s.txt",  cc.Data()),2,color[1],gMarkerStyle[1],2 );
    if(kaonSyst) {
      kaonSyst->SetMarkerSize(gMarkerSize[1]);
      kaonSyst->SetFillStyle(0);
      kaonSyst->SetFillColor(color[1]);
    }
    protonSyst = NScale( Form("../proton_v2_SPVZE_%s.txt",cc.Data()),2,color[3],gMarkerStyle[3],3 );
    if(protonSyst) {
      protonSyst->SetMarkerSize(gMarkerSize[3]);
      protonSyst->SetFillStyle(0);
      protonSyst->SetFillColor(color[3]);
    }
  }//Alice points
  else {
    //Central values 
    pion = NScale( Form("../rhic/phenixPion%s.txt",  cc.Data()),1,color[0],gMarkerStyle[0],2 );
    kaon = NScale( Form("../rhic/phenixKaon%s.txt",  cc.Data()),1,color[1],gMarkerStyle[1],2 );
    proton = NScale( Form("../rhic/phenixProton%s.txt",cc.Data()),1,color[3],gMarkerStyle[3],3 );
  }

  if(pion) pion->SetMarkerSize(gMarkerSize[0]);
  if(kaon) kaon->SetMarkerSize(gMarkerSize[1]);
  if(proton) proton->SetMarkerSize(gMarkerSize[3]);

  if(pionReflection) pionReflection->SetMarkerSize(gMarkerSize[0]);
  if(kaonReflection) kaonReflection->SetMarkerSize(gMarkerSize[1]);
  if(protonReflection) protonReflection->SetMarkerSize(gMarkerSize[3]);
  
  TH2D *axis = new TH2D( Form("MultiGraph_%s_%s",cc.Data(),fit.Data()),"",
			 10, -0.11, +2.99,
			 10, -0.01, +0.29 );
  axis->SetStats( kFALSE );
  axis->GetXaxis()->SetTitle("#font[12]{p}_{T}/#it{n}_{q}  (GeV/#it{c})");
  axis->GetYaxis()->SetTitle("#it{v}_{2}(SP,|#Delta#eta| > 2)/#it{n}_{q}");
  //axis->GetXaxis()->SetDecimals( kTRUE );
  //axis->GetYaxis()->SetDecimals( kTRUE );
  axis->GetYaxis()->SetTitleOffset(1.4);
  axis->GetXaxis()->SetTitleOffset(1.2);
  axis->GetYaxis()->SetTitleSize(0.075);
  axis->GetYaxis()->SetLabelSize(0.075);
  axis->GetXaxis()->SetTitleSize(0.075);
  axis->GetXaxis()->SetLabelSize(0.075);
  axis->GetYaxis()->SetNdivisions(5);
  axis->GetXaxis()->SetNdivisions(5);
  axis->GetYaxis()->CenterTitle( kTRUE );
  axis->GetXaxis()->CenterTitle( kTRUE );
  axis->Draw();
  
  if(pionSyst) pionSyst->Draw("p2");
  if(kaonSyst) kaonSyst->Draw("p2");
  if(protonSyst) protonSyst->Draw("p2");

  if(pion) pion->Draw("psame");
  if(kaon) kaon->Draw("psame");
  if(proton) proton->Draw("psame");

  if(pionReflection) pionReflection->Draw("psame");
  if(kaonReflection) kaonReflection->Draw("psame");
  if(protonReflection) protonReflection->Draw("psame");

  TLatex head;
  head.SetTextFont(42);
  head.SetNDC();
  head.SetTextSize(0.065);
  if(left) head.DrawLatex(0.42,0.92,header.Data());
  else head.DrawLatex(0.33,0.92,header.Data());

  TLegend *one = 0x0;
  if(left) one = new TLegend(0.23,0.55,0.4,0.85,"Pb-Pb #sqrt{#it{s}_{NN}} = 2.76 TeV","brNDC");
  else one = new TLegend(0.05,0.55,0.4,0.85,"Au-Au #sqrt{#it{s}_{NN}} = 200 GeV","brNDC");
  one->SetLineColor(10);
  one->SetTextSize(0.065);
  if(pion)   one->AddEntry(pion,"#pi^{#pm}");
  //if(kaon)   one->AddEntry(kaon,"K^{#pm}");
  if(kaon)   one->AddEntry(kaon,"K");
  if(proton)  one->AddEntry(proton,"p(#bar{p})");
  one->SetFillColor( kWhite );

  return one;
}

TH2D* PileRatio(TString cc,TString header, TF1 *fit1, TF1 *fit2, TString yaxis, double scale = 1., Bool_t kDrawLegend = kTRUE, Bool_t kDrawTitle = kTRUE, double koffset = 0.005, Bool_t kSeparate = kTRUE) {
  //Central Phenix values
  TGraphAsymmErrors* pionPhenix = NScale( Form("../rhic/phenixPion%s.txt",  cc.Data()),1,1,gMarkerStyle[0],2 );
  //pionPhenix->SetMarkerStyle(24);
  pionPhenix->SetMarkerSize(1.4);
  //pionPhenix->SetFillColor(kRed-3);
  //pionPhenix->SetFillStyle(0);
  //pionPhenix->SetLineWidth(5);
  pionPhenix->SetLineStyle(1);
  TGraphAsymmErrors *kaonPhenix = NScale( Form("../rhic/phenixKaon%s.txt",  cc.Data()),1,1,gMarkerStyle[1],2 );
  //kaonPhenix->SetMarkerStyle(26);
  kaonPhenix->SetMarkerSize(1.6);
  //kaonPhenix->SetFillColor(kOrange+1);
  //kaonPhenix->SetFillStyle(0);
  //kaonPhenix->SetLineWidth(5);
  kaonPhenix->SetLineStyle(1);
  TGraphAsymmErrors *protonPhenix = NScale( Form("../rhic/phenixProton%s.txt",cc.Data()),1,1,gMarkerStyle[3],3 );
  //protonPhenix->SetMarkerStyle(25);
  protonPhenix->SetMarkerSize(1.4);
  //protonPhenix->SetFillColor(kYellow+1);
  //protonPhenix->SetFillStyle(0);
  //protonPhenix->SetLineWidth(5);
  protonPhenix->SetLineStyle(1);
  protonPhenix->Fit( fit2->GetName(), "qn","" );

  makeRatio(pionPhenix,  fit2);
  makeRatio(kaonPhenix,  fit2);
  makeRatio(protonPhenix,fit2);

  //Central ALICE values
  TGraphAsymmErrors* pion   = NScale( Form("../pion_v2_SPVZE_%s.txt",  cc.Data()),1,color[0],gMarkerStyle[0],2 );
  if(pion) pion->SetMarkerSize(gMarkerSize[0]);
  TGraphAsymmErrors* kaon   = NScale( Form("../kaonAvg_v2_SPVZE_%s.txt",  cc.Data()),1,color[1],gMarkerStyle[1],2 );
  if(kaon) kaon->SetMarkerSize(gMarkerSize[1]);
  TGraphAsymmErrors* proton = NScale( Form("../proton_v2_SPVZE_%s.txt",cc.Data()),1,color[3],gMarkerStyle[3],3 );
  if(proton) proton->SetMarkerSize(gMarkerSize[3]);
  proton->Fit( fit1->GetName(), "qn","" );

  makeRatio(pion,  fit1);
  makeRatio(kaon,  fit1);
  makeRatio(proton,fit1);

  //Central ALICE values
  TGraphAsymmErrors* pionReflection   = NScale( Form("../pion_v2_SPVZE_%s.txt",  cc.Data()),1,colorReflection[0],gMarkerStyleReflection[0],2 );
  if(pionReflection) pionReflection->SetMarkerSize(gMarkerSize[0]);
  TGraphAsymmErrors* kaonReflection   = NScale( Form("../kaonAvg_v2_SPVZE_%s.txt",  cc.Data()),1,colorReflection[1],gMarkerStyleReflection[1],2 );
  if(kaonReflection) kaonReflection->SetMarkerSize(gMarkerSize[1]);
  TGraphAsymmErrors* protonReflection = NScale( Form("../proton_v2_SPVZE_%s.txt",cc.Data()),1,colorReflection[3],gMarkerStyleReflection[3],3 );
  if(protonReflection) protonReflection->SetMarkerSize(gMarkerSize[3]);

  makeRatio(pionReflection,  fit1);
  makeRatio(kaonReflection,  fit1);
  makeRatio(protonReflection,fit1);

  //Systematics
  TGraphAsymmErrors* pionSyst   = NScale( Form("../pion_v2_SPVZE_%s.txt",  cc.Data()),2,color[0],gMarkerStyle[0],2 );
  if(pionSyst) {
    pionSyst->SetMarkerSize(gMarkerSize[0]);
    pionSyst->SetFillStyle(0);
    pionSyst->SetFillColor(color[0]);
    makeRatio(pionSyst,fit1);
  }
  TGraphAsymmErrors* kaonSyst   = NScale( Form("../kaonAvg_v2_SPVZE_%s.txt",  cc.Data()),2,color[1],gMarkerStyle[1],2 );
  if(kaonSyst) {
    kaonSyst->SetMarkerSize(gMarkerSize[1]);
    kaonSyst->SetFillStyle(0);
    kaonSyst->SetFillColor(color[1]);
    makeRatio(kaonSyst,fit1);
  }
  TGraphAsymmErrors* protonSyst = NScale( Form("../proton_v2_SPVZE_%s.txt",cc.Data()),2,color[3],gMarkerStyle[3],3 );
  if(protonSyst) {
    protonSyst->SetMarkerSize(gMarkerSize[3]);
    protonSyst->SetFillStyle(0);
    protonSyst->SetFillColor(color[3]);
    makeRatio(protonSyst,fit1);
  }

  TH2D *axis = new TH2D( Form("MultiGraphRatio_%s%s",cc.Data(),fit1->GetName()),"",
			 10, -0.11, +1.99,
			 10, 0.80, +2.10 );
  //10, -0.10, +2.10 );
  axis->SetStats( kFALSE );
  if(kDrawTitle) 
    axis->GetXaxis()->SetTitle("#font[12]{p}_{T}/#it{n}_{q}  (GeV/#it{c})");
  axis->GetYaxis()->SetTitle( yaxis.Data() );
  //axis->GetXaxis()->SetDecimals( kTRUE );
  //axis->GetYaxis()->SetDecimals( kTRUE );
  axis->GetYaxis()->SetTitleOffset(1.3);
  axis->GetXaxis()->SetLabelOffset(koffset);
  axis->GetYaxis()->SetTitleSize(0.075*scale);
  axis->GetYaxis()->SetLabelSize(0.075*scale);
  axis->GetXaxis()->SetTitleSize(0.075*scale);
  axis->GetXaxis()->SetLabelSize(0.075*scale);
  axis->GetYaxis()->SetNdivisions(10);
  axis->GetXaxis()->SetNdivisions(5);
  axis->GetYaxis()->CenterTitle( kTRUE );
  axis->GetXaxis()->CenterTitle( kTRUE );
  axis->DrawCopy();
  

  if(pionSyst) pionSyst->Draw("p2");
  if(kaonSyst) kaonSyst->Draw("p2");
  if(protonSyst) protonSyst->Draw("p2");

  if(pion) pion->Draw("psame");
  if(kaon) kaon->Draw("psame");
  if(proton) proton->Draw("psame");

  if(pionReflection) pionReflection->Draw("psame");
  if(kaonReflection) kaonReflection->Draw("psame");
  if(protonReflection) protonReflection->Draw("psame");

  pionPhenix->Draw("p");
  kaonPhenix->Draw("p");
  protonPhenix->Draw("p");

  TLegend *one = new TLegend(0.53,0.65,0.65,0.9,"ALICE","brNDC");
  one->SetLineColor(10);
  one->SetTextSize(0.075*scale);
  if(pion)   one->AddEntry(pion,"#pi^{#pm}");
  if(kaon)   one->AddEntry(kaon,"K");
  if(proton)  one->AddEntry(proton,"p(#bar{p})");
  one->SetFillColor( kWhite );
  
  TLegend *two = 0x0;
  if(!kSeparate) two = new TLegend(0.75,0.65,0.87,0.9,"PHENIX","brNDC");
  else if(kSeparate) two = new TLegend(0.6,0.65,0.77,0.85,"PHENIX","brNDC");
  two->SetLineColor(10);
  two->SetTextSize(0.075*scale);
  if(pion)   two->AddEntry(pionPhenix,"#pi^{#pm}");
  if(kaon)   two->AddEntry(kaonPhenix,"K^{#pm}");
  if(proton)  two->AddEntry(protonPhenix,"p(#bar{p})");
  two->SetFillColor( kWhite );
  
  if(kDrawLegend) {
    one->Draw();
    two->Draw();
  }

  TString outputHEPfilename;
  AliHEPDataParser *gHEPPion = new AliHEPDataParser(pion,pionSyst);
  gHEPPion->SetXaxisName("pT/nq (GeV/c)");
  gHEPPion->SetName("(v2/nq)/(v2/nq)_{Fit p}");
  outputHEPfilename = "HEPData/figure12-pion";
  outputHEPfilename += "-";
  outputHEPfilename += cc.Data();
  outputHEPfilename += ".txt";
  gHEPPion->SaveHEPDataFile(outputHEPfilename.Data(),kTRUE);
  
  AliHEPDataParser *gHEPKaon = new AliHEPDataParser(kaon,kaonSyst);
  gHEPKaon->SetXaxisName("pT/nq (GeV/c)");
  gHEPKaon->SetName("(v2/nq)/(v2/nq)_{Fit p}");
  outputHEPfilename = "HEPData/figure12-kaon";
  outputHEPfilename += "-";
  outputHEPfilename += cc.Data();
  outputHEPfilename += ".txt";
  gHEPKaon->SaveHEPDataFile(outputHEPfilename.Data(),kTRUE);

  AliHEPDataParser *gHEPProton = new AliHEPDataParser(proton,protonSyst);
  gHEPProton->SetXaxisName("pT/nq (GeV/c)");
  gHEPProton->SetName("(v2/nq)/(v2/nq)_{Fit p}");
  outputHEPfilename = "HEPData/figure12-proton";
  outputHEPfilename += "-";
  outputHEPfilename += cc.Data();
  outputHEPfilename += ".txt";
  gHEPProton->SaveHEPDataFile(outputHEPfilename.Data(),kTRUE);

  return axis;
}

void makeRatio(TGraphAsymmErrors *graph, TF1 *fit) {
  if(!graph) return;
  Double_t x,y,eL,eH;
  for(Int_t i=0; i!=graph->GetN(); ++i) {
    graph->GetPoint(i,x,y);
    eL = graph->GetErrorYlow(i) / fit->Eval(x);
    eH = graph->GetErrorYhigh(i) / fit->Eval(x);
    y /= fit->Eval(x);
    graph->SetPoint(i,x,y);
    graph->SetPointEYlow(i,eL);
    graph->SetPointEYhigh(i,eH);
  }
}

TGraphAsymmErrors* NScale( TString  file, Int_t error, Int_t color, Int_t marker, Int_t nq ) {
  TGraphAsymmErrors *one = getGraph(file,error,color,marker);
  if(!one) return NULL;
  Double_t x,y,exL,exH,eyL,eyH;
  for(Int_t i=0; i!=one->GetN(); ++i) {
    one->GetPoint(i,x,y);
    exL = one->GetErrorXlow(i) / nq;
    exH = one->GetErrorXhigh(i) / nq;
    eyL = one->GetErrorYlow(i) / nq;
    eyH = one->GetErrorYhigh(i) / nq;
    y /= nq;
    x /= nq;
    one->SetPoint(i,x,y);
    one->SetPointEYlow(i,eyL);
    one->SetPointEYhigh(i,eyH);
    one->SetPointEXlow(i,exL);
    one->SetPointEXhigh(i,exH);
  }
  return one;
}
