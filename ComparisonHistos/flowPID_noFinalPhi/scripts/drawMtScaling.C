int color[8] = { kBlack, kBlack, kYellow+1, kGreen+2, kRed-3, kBlue-3, kMagenta+2,kPink+9};
int colorReflection[8] = { kBlack, kBlack, kYellow+1, kBlack, kBlack, kBlack, kMagenta+2,kPink+9};
Int_t gMarkerStyle[8] = {24,27,22,21,20,29,30,23};
Int_t gMarkerStyleReflection[8] = {24,27,26,25,24,30,30,23};
//Double_t gMarkerSize[8] = {1.2,1.2,1.2,1.2,1.6,1.2,1.2};
Double_t gMarkerSize[8] = {1.0,1.4,1.0,1.0,1.0,1.2,1.0,1.2};
//TString speciesString[8] = { "#pi^{#pm}", "K^{#pm}", "K_{s}^{0}","p(#bar{p})","#phi","#Lambda(#bar{#Lambda})","#Xi^{-}(#bar{#Xi}^{+})","#Omega^{-}(#bar{#Omega}^{+})"};
TString speciesString[8] = { "#pi^{#pm}", "K", "K_{s}^{0}","p+#bar{p}","#phi","#Lambda+#bar{#Lambda}","#Xi^{-}+#bar{#Xi}^{+}","#Omega^{-}+#bar{#Omega}^{+}"};

void drawMtScaling() {
  gROOT->LoadMacro("helper.C");
  gSystem->Load("libANALYSIS.so");
  gSystem->Load("libANALYSISalice.so");

  TLatex *texting = new TLatex();
  texting->SetTextFont(42);
  texting->SetTextSize(0.085);

  TH2D *hEmpty = new TH2D("hEmpty","",1,0,1,1,0,0.3);
  hEmpty->SetStats(kFALSE);
  hEmpty->GetXaxis()->SetNdivisions(0);
  hEmpty->GetYaxis()->SetNdivisions(0);

  //>>>legend
  TLegend *legend = new TLegend( 0.15,0.15,0.95,0.95, "Particle species");
  legend->SetFillColor( kWhite );
  legend->SetLineColor(10);
  legend->SetTextAlign(12);
  legend->SetTextFont(42);
  legend->SetNColumns(2);
  legend->SetTextSize(0.09);
  TGraph *leg[8];
  for(int i=0; i!=7; ++i) {
    if(i == 2) continue;
    leg[i] = new TGraph(1);
    leg[i]->SetMarkerSize(gMarkerSize[i]);
    format( leg[i], gMarkerStyle[i], color[i], "" );
    legend->AddEntry( leg[i] , Form("%s",speciesString[i].Data()));
  }

  //TCanvas *fig = new TCanvas("fig","fig",0,0,500,700);
  TCanvas *fig = new TCanvas("fig","fig",0,0,800,800);
  fig->SetFixedAspectRatio(kTRUE);
  fig->SetLeftMargin(0.22);
  fig->SetBottomMargin(0.25);
  //fig->Divide(2,4,0.00,0.00);
  fig->Divide(3,3,0.00,0.00);
  fig->cd(1); PilePlots("0005","0-5\%",kTRUE,0.9);
  texting->SetTextAngle(0);
  texting->DrawLatex(0.0,0.13,"0-5%");
  fig->cd(2); PilePlots("0510","5-10\%",kTRUE,0.9);
  texting->SetTextAngle(0);
  texting->DrawLatex(0.0,0.13,"5-10%");
  fig->cd(3); hEmpty->DrawCopy();
  texting->DrawLatex(0.3,0.245,"ALICE");
  texting->DrawLatex(0.1,0.205,"Pb-Pb #sqrt{#it{s}_{NN}} = 2.76 TeV");
  texting->DrawLatex(0.3,0.165,"|y| < 0.5");
  //texting->DrawLatex(0.3,0.125,"and |y| < 0.5");
  fig->cd(4); PilePlots("1020","10-20\%",kFALSE,0.9);
  texting->SetTextAngle(90);
  texting->DrawLatex(-0.9,-0.005,"#it{v}_{2}{SP,|#Delta#eta| > 0.9}/#it{n}_{q}");
  texting->SetTextAngle(0);
  texting->DrawLatex(0.0,0.13,"10-20%");
  fig->cd(5); PilePlots("2030","20-30\%",kTRUE,0.9);
  texting->SetTextAngle(0);
  texting->DrawLatex(0.0,0.13,"20-30%");
  texting->SetTextAngle(90);
  fig->cd(6); hEmpty->DrawCopy(); legend->Draw();
  fig->cd(7); TH2D *axis1 = PilePlots("3040","30-40\%",kFALSE,0.65);
  axis1->GetXaxis()->SetLabelOffset(0.035);
  texting->SetTextAngle(0);
  texting->SetTextSize(0.06);
  texting->DrawLatex(0.0,0.13,"30-40%");
  fig->cd(8); TH2D *axis2 = PilePlots("4050","40-50\%",kTRUE,0.85);
  axis2->GetXaxis()->SetLabelOffset(0.02);
  texting->SetTextSize(0.08);
  texting->SetTextAngle(0);
  texting->DrawLatex(0.0,0.13,"40-50%");
  texting->SetTextAngle(0);
  texting->SetTextSize(0.085);
  texting->DrawLatex(0.3,-0.055,"(#it{m}_{T} - #it{m}_{0})/#it{n}_{q} (GeV/#font[12]{c}^{2})");
  //texting->DrawLatex(0.8,-0.065,"(#it{m}_{T} - #it{m}_{0})/#it{n}_{q}");
  fig->cd(9); PilePlots("5060","50-60\%",kFALSE,0.85);
  texting->SetTextSize(0.08);
  texting->SetTextAngle(0);
  texting->DrawLatex(0.0,0.13,"50-60%");
  texting->SetTextSize(0.095);
  
  TString saveas = "../../fig/v2mqAll.eps";
  fig->SaveAs(saveas.Data(),"EPS");
  printf("%s has been saved\n",saveas.Data());

  TLegend *legend1 = new TLegend(0.17,0.63, 0.33,0.895, "","brNDC");
  legend1->SetFillColor( kWhite );
  legend1->SetLineColor( kWhite );
  legend1->SetTextAlign(12);
  legend1->SetTextFont(42);
  legend1->SetNColumns(2);
  legend1->SetTextSize(0.04);
  TGraph *leg[8];
  for(int i=0; i!=8; ++i) {
    if(i == 2) continue;
    leg[i] = new TGraph(1);
    leg[i]->SetMarkerSize(gMarkerSize[i]);
    format( leg[i], gMarkerStyle[i], color[i], "" );
    legend1->AddEntry( leg[i] , Form("%s",speciesString[i].Data()));
  }

  TLegend *legend2 = new TLegend(0.17,0.63, 0.33,0.895, "","brNDC");
  legend2->SetFillColor( kWhite );
  legend2->SetLineColor( kWhite );
  legend2->SetTextAlign(12);
  legend2->SetTextFont(42);
  legend2->SetNColumns(2);
  legend2->SetTextSize(0.04);
  TGraph *leg[8];
  for(int i=0; i!=8; ++i) {
    if((i == 2)||(i == 4)) continue;
    leg[i] = new TGraph(1);
    leg[i]->SetMarkerSize(gMarkerSize[i]);
    format( leg[i], gMarkerStyle[i], color[i], "" );
    legend2->AddEntry( leg[i] , Form("%s",speciesString[i].Data()));
  }

  TLegend *legend3 = new TLegend(0.17,0.63, 0.33,0.895, "","brNDC");
  legend3->SetFillColor( kWhite );
  legend3->SetLineColor( kWhite );
  legend3->SetTextAlign(12);
  legend3->SetTextFont(42);
  legend3->SetNColumns(2);
  legend3->SetTextSize(0.04);
  TGraph *leg[8];
  for(int i=0; i!=8; ++i) {
    if((i == 2)||(i == 4)||(i == 7)) continue;
    leg[i] = new TGraph(1);
    leg[i]->SetMarkerSize(gMarkerSize[i]);
    format( leg[i], gMarkerStyle[i], color[i], "" );
    legend3->AddEntry( leg[i] , Form("%s",speciesString[i].Data()));
  }

  TCanvas *fig1 = new TCanvas("fig1","fig1");
  fig1->SetLeftMargin(0.15);
  fig1->SetBottomMargin(0.15);
  fig1->cd(1); PilePlots("0005","0-5\%",kTRUE,0.45);
  texting->SetTextAngle(0);
  texting->SetTextSize(0.045);
  texting->DrawLatex(0.27,0.155,"ALICE 0-5% Pb-Pb #sqrt{#it{s}_{NN}} = 2.76 TeV");
  //texting->DrawLatex(0.85,0.155,"Pb-Pb #sqrt{#it{s}_{NN}} = 2.76 TeV");
  texting->SetTextAngle(90);
  texting->DrawLatex(-0.55,0.03,"#it{v}_{2}{SP,|#Delta#eta| > 0.9}/#it{n}_{q}");
  texting->SetTextAngle(0);
  texting->DrawLatex(0.9,-0.04,"(#it{m}_{T} - #it{m}_{0})/#it{n}_{q} (GeV/#font[12]{c}^{2})");
  legend3->Draw();
  saveas = "../../fig/v2mqAllCenta0To5.eps";
  fig1->SaveAs(saveas.Data(),"EPS");
  printf("%s has been saved\n",saveas.Data());

  TCanvas *fig2 = new TCanvas("fig2","fig2");
  fig2->SetLeftMargin(0.15);
  fig2->SetBottomMargin(0.15);
  fig2->cd(1); PilePlots("0510","5-10\%",kTRUE,0.45);
  texting->SetTextAngle(0);
  texting->SetTextSize(0.045);
  texting->DrawLatex(0.27,0.155,"ALICE 5-10% Pb-Pb #sqrt{#it{s}_{NN}} = 2.76 TeV");
  //texting->DrawLatex(0.85,0.155,"Pb-Pb #sqrt{#it{s}_{NN}} = 2.76 TeV");
  texting->SetTextAngle(90);
  texting->DrawLatex(-0.55,0.03,"#it{v}_{2}{SP,|#Delta#eta| > 0.9}/#it{n}_{q}");
  texting->SetTextAngle(0);
  texting->DrawLatex(0.9,-0.04,"(#it{m}_{T} - #it{m}_{0})/#it{n}_{q} (GeV/#font[12]{c}^{2})");
  legend2->Draw();
  saveas = "../../fig/v2mqAllCenta5To10.eps";
  fig2->SaveAs(saveas.Data(),"EPS");
  printf("%s has been saved\n",saveas.Data());

  TCanvas *fig3 = new TCanvas("fig3","fig3");
  fig3->SetLeftMargin(0.15);
  fig3->SetBottomMargin(0.15);
  fig3->cd(1); PilePlots("1020","10-20\%",kTRUE,0.45);
  texting->SetTextAngle(0);
  texting->SetTextSize(0.045);
  texting->DrawLatex(0.27,0.155,"ALICE 10-20% Pb-Pb #sqrt{#it{s}_{NN}} = 2.76 TeV");
  //texting->DrawLatex(0.85,0.155,"Pb-Pb #sqrt{#it{s}_{NN}} = 2.76 TeV");
  texting->SetTextAngle(90);
  texting->DrawLatex(-0.55,0.03,"#it{v}_{2}{SP,|#Delta#eta| > 0.9}/#it{n}_{q}");
  texting->SetTextAngle(0);
  texting->DrawLatex(0.9,-0.04,"(#it{m}_{T} - #it{m}_{0})/#it{n}_{q} (GeV/#font[12]{c}^{2})");
  legend1->Draw();
  saveas = "../../fig/v2mqAllCenta10To20.eps";
  fig3->SaveAs(saveas.Data(),"EPS");
  printf("%s has been saved\n",saveas.Data());

  TCanvas *fig4 = new TCanvas("fig4","fig4");
  fig4->SetLeftMargin(0.15);
  fig4->SetBottomMargin(0.15);
  fig4->cd(1); PilePlots("2030","20-30\%",kTRUE,0.45);
  texting->SetTextAngle(0);
  texting->SetTextSize(0.045);
  texting->DrawLatex(0.27,0.155,"ALICE 20-30% Pb-Pb #sqrt{#it{s}_{NN}} = 2.76 TeV");
  //texting->DrawLatex(0.85,0.155,"Pb-Pb #sqrt{#it{s}_{NN}} = 2.76 TeV");
  texting->SetTextAngle(90);
  texting->DrawLatex(-0.55,0.03,"#it{v}_{2}{SP,|#Delta#eta| > 0.9}/#it{n}_{q}");
  texting->SetTextAngle(0);
  texting->DrawLatex(0.9,-0.04,"(#it{m}_{T} - #it{m}_{0})/#it{n}_{q} (GeV/#font[12]{c}^{2})");
  legend1->Draw();
  saveas = "../../fig/v2mqAllCenta20To30.eps";
  fig4->SaveAs(saveas.Data(),"EPS");
  printf("%s has been saved\n",saveas.Data());

  TCanvas *fig5 = new TCanvas("fig5","fig5");
  fig5->SetLeftMargin(0.15);
  fig5->SetBottomMargin(0.15);
  fig5->cd(1); PilePlots("3040","30-40\%",kTRUE,0.45);
  texting->SetTextAngle(0);
  texting->SetTextSize(0.045);
  texting->DrawLatex(0.27,0.155,"ALICE 30-40% Pb-Pb #sqrt{#it{s}_{NN}} = 2.76 TeV");
  //texting->DrawLatex(0.85,0.155,"Pb-Pb #sqrt{#it{s}_{NN}} = 2.76 TeV");
  texting->SetTextAngle(90);
  texting->DrawLatex(-0.55,0.03,"#it{v}_{2}{SP,|#Delta#eta| > 0.9}/#it{n}_{q}");
  texting->SetTextAngle(0);
  texting->DrawLatex(0.9,-0.04,"(#it{m}_{T} - #it{m}_{0})/#it{n}_{q} (GeV/#font[12]{c}^{2})");
  legend1->Draw();
  saveas = "../../fig/v2mqAllCenta30To40.eps";
  fig5->SaveAs(saveas.Data(),"EPS");
  printf("%s has been saved\n",saveas.Data());

  TCanvas *fig6 = new TCanvas("fig6","fig6");
  fig6->SetLeftMargin(0.15);
  fig6->SetBottomMargin(0.15);
  fig6->cd(1); PilePlots("4050","40-50\%",kTRUE,0.45);
  texting->SetTextAngle(0);
  texting->SetTextSize(0.045);
  texting->DrawLatex(0.27,0.155,"ALICE 40-50% Pb-Pb #sqrt{#it{s}_{NN}} = 2.76 TeV");
  //texting->DrawLatex(0.85,0.155,"Pb-Pb #sqrt{#it{s}_{NN}} = 2.76 TeV");
  texting->SetTextAngle(90);
  texting->DrawLatex(-0.55,0.03,"#it{v}_{2}{SP,|#Delta#eta| > 0.9}/#it{n}_{q}");
  texting->SetTextAngle(0);
  texting->DrawLatex(0.9,-0.04,"(#it{m}_{T} - #it{m}_{0})/#it{n}_{q} (GeV/#font[12]{c}^{2})");
  legend1->Draw();
  saveas = "../../fig/v2mqAllCenta40To50.eps";
  fig6->SaveAs(saveas.Data(),"EPS");
  printf("%s has been saved\n",saveas.Data());

  TCanvas *fig7 = new TCanvas("fig7","fig7");
  fig7->SetLeftMargin(0.15);
  fig7->SetBottomMargin(0.15);
  fig7->cd(1); PilePlots("5060","50-60\%",kTRUE,0.45);
  texting->SetTextAngle(0);
  texting->SetTextSize(0.045);
  texting->DrawLatex(0.27,0.155,"ALICE 50-60% Pb-Pb #sqrt{#it{s}_{NN}} = 2.76 TeV");
  //texting->DrawLatex(0.85,0.155,"Pb-Pb #sqrt{#it{s}_{NN}} = 2.76 TeV");
  texting->SetTextAngle(90);
  texting->DrawLatex(-0.55,0.03,"#it{v}_{2}{SP,|#Delta#eta| > 0.9}/#it{n}_{q}");
  texting->SetTextAngle(0);
  texting->DrawLatex(0.9,-0.04,"(#it{m}_{T} - #it{m}_{0})/#it{n}_{q} (GeV/#font[12]{c}^{2})");
  legend1->Draw();
  saveas = "../../fig/v2mqAllCenta50To60.eps";
  fig7->SaveAs(saveas.Data(),"EPS");
  printf("%s has been saved\n",saveas.Data());
}

TH2D* PilePlots(TString cc,TString header, Bool_t left, Double_t scale) {
  gStyle->SetTitleX(0.5);
  gStyle->SetTitleAlign(23);

  //Central ALICE values
  TGraphAsymmErrors* pion   = MScale( Form("../pion_v2_SPVZE_%s.txt",  cc.Data()),1,color[0],gMarkerStyle[0],2,0.13957 );
  if(pion) pion->SetMarkerSize(gMarkerSize[0]);
  TGraphAsymmErrors* kaon   = MScale( Form("../kaonAvg_v2_SPVZE_%s.txt",  cc.Data()),1,color[1],gMarkerStyle[1],2,0.493677 );
  if(kaon) kaon->SetMarkerSize(gMarkerSize[1]);
  TGraphAsymmErrors* kzero   = MScale( Form("../kzero_v2_SPVZE_%s.txt",  cc.Data()),1,color[2],gMarkerStyle[2],2,0.497648 );
  if(kzero) kzero->SetMarkerSize(gMarkerSize[2]);
  TGraphAsymmErrors* proton = MScale( Form("../proton_v2_SPVZE_%s.txt",cc.Data()),1,color[3],gMarkerStyle[3],3,0.938272 );
  if(proton) proton->SetMarkerSize(gMarkerSize[3]);
  TGraphAsymmErrors* phi    = MScale( Form("../phi_v2_SPVZE_%s.txt",   cc.Data()),1,color[4],gMarkerStyle[4],2,1.019455 );
  if(phi) phi->SetMarkerSize(gMarkerSize[4]);
  TGraphAsymmErrors* lambda = MScale( Form("../lambda_v2_SPVZE_%s.txt",cc.Data()),1,color[5],gMarkerStyle[5],3,1.115683 );
  if(lambda) lambda->SetMarkerSize(gMarkerSize[5]);
  TGraphAsymmErrors* xi     = MScale( Form("../xi_v2_SPVZE_%s.txt",    cc.Data()),1,color[6],gMarkerStyle[6],3,1.32171);
  if(xi) xi->SetMarkerSize(gMarkerSize[6]);
  TGraphAsymmErrors* omega     = MScale( Form("../omega_v2_SPVZE_%s.txt",    cc.Data()),1,color[7],gMarkerStyle[7],3,1.67245);
  if(omega) omega->SetMarkerSize(gMarkerSize[7]);

  //Central ALICE values: reflection for visibility
  TGraphAsymmErrors* pionReflection   = MScale( Form("../pion_v2_SPVZE_%s.txt",  cc.Data()),1,colorReflection[0],gMarkerStyleReflection[0],2,0.13957 );
  if(pionReflection) pionReflection->SetMarkerSize(gMarkerSize[0]);
  TGraphAsymmErrors* kaonReflection   = MScale( Form("../kaonAvg_v2_SPVZE_%s.txt",  cc.Data()),1,colorReflection[1],gMarkerStyleReflection[1],2,0.493677 );
  if(kaonReflection) kaonReflection->SetMarkerSize(gMarkerSize[1]);
  TGraphAsymmErrors* kzeroReflection   = MScale( Form("../kzero_v2_SPVZE_%s.txt",  cc.Data()),1,colorReflection[2],gMarkerStyleReflection[2],2,0.497648 );
  if(kzeroReflection) kzeroReflection->SetMarkerSize(gMarkerSize[2]);
  TGraphAsymmErrors* protonReflection = MScale( Form("../proton_v2_SPVZE_%s.txt",cc.Data()),1,colorReflection[3],gMarkerStyleReflection[3],3,0.938272 );
  if(protonReflection) protonReflection->SetMarkerSize(gMarkerSize[3]);
  TGraphAsymmErrors* phiReflection    = MScale( Form("../phi_v2_SPVZE_%s.txt",   cc.Data()),1,colorReflection[4],gMarkerStyleReflection[4],2,1.019455 );
  if(phiReflection) phiReflection->SetMarkerSize(gMarkerSize[4]);
  TGraphAsymmErrors* lambdaReflection = MScale( Form("../lambda_v2_SPVZE_%s.txt",cc.Data()),1,colorReflection[5],gMarkerStyleReflection[5],3,1.115683 );
  if(lambdaReflection) lambdaReflection->SetMarkerSize(gMarkerSize[5]);
  TGraphAsymmErrors* xiReflection     = MScale( Form("../xi_v2_SPVZE_%s.txt",    cc.Data()),1,colorReflection[6],gMarkerStyleReflection[6],3,1.32171);
  if(xiReflection) xiReflection->SetMarkerSize(gMarkerSize[6]);
  TGraphAsymmErrors* omegaReflection     = MScale( Form("../omega_v2_SPVZE_%s.txt",    cc.Data()),1,colorReflection[7],gMarkerStyleReflection[7],3,1.67245);
  if(omegaReflection) omegaReflection->SetMarkerSize(gMarkerSize[7]);

  //Systematic errors ALICE
  TGraphAsymmErrors* pionSyst   = MScale( Form("../pion_v2_SPVZE_%s.txt",  cc.Data()),2,color[0],gMarkerStyle[0],2,0.13957 );
 if(pionSyst) {
   pionSyst->SetMarkerSize(gMarkerSize[0]);
   pionSyst->SetFillStyle(0);
   pionSyst->SetFillColor(color[0]);
  }
  TGraphAsymmErrors* kaonSyst   = MScale( Form("../kaonAvg_v2_SPVZE_%s.txt",  cc.Data()),2,color[1],gMarkerStyle[1],2,0.493677 );
 if(kaonSyst) {
   kaonSyst->SetMarkerSize(gMarkerSize[1]);
   kaonSyst->SetFillStyle(0);
   kaonSyst->SetFillColor(color[1]);
  }
  TGraphAsymmErrors* kzeroSyst   = MScale( Form("../kzero_v2_SPVZE_%s.txt",  cc.Data()),2,color[2],gMarkerStyle[2],2,0.497648 );
 if(kzeroSyst) {
   kzeroSyst->SetMarkerSize(gMarkerSize[2]);
   kzeroSyst->SetFillStyle(0);
   kzeroSyst->SetFillColor(color[2]);
  }
  TGraphAsymmErrors* protonSyst = MScale( Form("../proton_v2_SPVZE_%s.txt",cc.Data()),2,color[3],gMarkerStyle[3],3,0.938272 );
 if(protonSyst) {
   protonSyst->SetMarkerSize(gMarkerSize[3]);
   protonSyst->SetFillStyle(0);
   protonSyst->SetFillColor(color[3]);
  }
  TGraphAsymmErrors* phiSyst    = MScale( Form("../phi_v2_SPVZE_%s.txt",   cc.Data()),2,color[4],gMarkerStyle[4],2,1.019455 );
 if(phiSyst) {
   phiSyst->SetMarkerSize(gMarkerSize[4]);
   phiSyst->SetFillStyle(0);
   phiSyst->SetFillColor(color[4]);
  }
  TGraphAsymmErrors* lambdaSyst = MScale( Form("../lambda_v2_SPVZE_%s.txt",cc.Data()),2,color[5],gMarkerStyle[5],3,1.115683 );
 if(lambdaSyst) {
   lambdaSyst->SetMarkerSize(gMarkerSize[5]);
   lambdaSyst->SetFillStyle(0);
   lambdaSyst->SetFillColor(color[5]);
  }
  TGraphAsymmErrors* xiSyst     = MScale( Form("../xi_v2_SPVZE_%s.txt",    cc.Data()),2,color[6],gMarkerStyle[6],3,1.32171);
 if(xiSyst) {
   xiSyst->SetMarkerSize(gMarkerSize[6]);
   xiSyst->SetFillStyle(0);
   xiSyst->SetFillColor(color[6]);
  }
  TGraphAsymmErrors* omegaSyst     = MScale( Form("../omega_v2_SPVZE_%s.txt",    cc.Data()),2,color[7],gMarkerStyle[7],3,1.67245);
 if(omegaSyst) {
   omegaSyst->SetMarkerSize(gMarkerSize[7]);
   omegaSyst->SetFillStyle(0);
   omegaSyst->SetFillColor(color[7]);
  }

  TH2D *axis = new TH2D( Form("MultiGraph_%s",cc.Data()),"",
			 10, -0.16, +2.99,
			 10, -0.015, +0.149 );
  axis->SetStats( kFALSE );
  //axis->GetXaxis()->SetDecimals( kTRUE );
  //axis->GetYaxis()->SetDecimals( kTRUE );
  axis->GetYaxis()->SetTitleOffset(0.9);
  axis->GetYaxis()->SetTitleSize(0.10*scale);
  axis->GetYaxis()->SetLabelSize(0.10*scale);
  axis->GetXaxis()->SetTitleSize(0.10*scale);
  axis->GetXaxis()->SetLabelSize(0.10*scale);
  
  axis->GetYaxis()->SetNdivisions(5);
  axis->GetXaxis()->SetNdivisions(5);
  axis->GetYaxis()->CenterTitle( kTRUE );
  axis->GetXaxis()->CenterTitle( kTRUE );
  axis->SetTitleSize(20);
  axis->Draw();
  
  if(pionSyst) pionSyst->Draw("p2");
  if(kaonSyst) kaonSyst->Draw("p2");
  //if(kzeroSyst) kzeroSyst->Draw("p2");
  if(protonSyst) protonSyst->Draw("p2");
  if(lambdaSyst) lambdaSyst->Draw("p2");
  if(xiSyst) xiSyst->Draw("p2");
  //if(omegaSyst) omegaSyst->Draw("p2");
  if(phiSyst) phiSyst->Draw("p2");

  if(pion) pion->Draw("psame");
  if(kaon) kaon->Draw("psame");
  //if(kzero) kzero->Draw("psame");
  if(proton) proton->Draw("psame");
  if(lambda) lambda->Draw("psame");
  if(xi) xi->Draw("psame");
  //if(omega) omega->Draw("psame");
  if(phi) phi->Draw("psame");

  if(pionReflection) pionReflection->Draw("psame");
  if(kaonReflection) kaonReflection->Draw("psame");
  //if(kzeroReflection) kzeroReflection->Draw("psame");
  if(protonReflection) protonReflection->Draw("psame");
  if(lambdaReflection) lambdaReflection->Draw("psame");
  if(xiReflection) xiReflection->Draw("psame");
  //if(omegaReflection) omegaReflection->Draw("psame");
  if(phiReflection) phiReflection->Draw("psame");

  TLatex head;
  head.SetTextFont(42);
  head.SetNDC();
  head.SetTextSize(0.085*scale);
  
  TLegend *one = 0x0;
  if(left) one = new TLegend(0.23,0.55,0.6,0.92);
  else one = new TLegend(0.05,0.55,0.5,0.92);
  one->SetLineColor(10);
  one->SetTextSize(0.095);
  if(left) {
    if(pion)   one->AddEntry(pion,"#pi^{#pm}");
    if(kaon)   one->AddEntry(kaon,"K");
    //if(kzero)  one->AddEntry(kzero,"K^{0}_{s}");
    if(proton) one->AddEntry(proton,"p+#bar{p}");
  }
  else {
    if(phi)    one->AddEntry(phi,"#phi");
    if(lambda) one->AddEntry(lambda,"#Lambda+#bar{#Lambda}");
    if(xi)     one->AddEntry(xi,"#Xi^{-}+#bar{#Xi}^{+}");
    //if(omega)  one->AddEntry(omega,"#Omega^{-}+#bar{#Omega}^{+}");
  }
  one->SetFillColor( kWhite );
  one->SetNColumns(2);

  TString outputHEPfilename;
  AliHEPDataParser *gHEPPion = new AliHEPDataParser(pion,pionSyst);
  gHEPPion->SetXaxisName("(mT-m0)/nq (GeV/c)");
  gHEPPion->SetName("v2/nq");
  outputHEPfilename = "HEPData/figure10-pion";
  outputHEPfilename += "-";
  outputHEPfilename += cc.Data();
  outputHEPfilename += ".txt";
  gHEPPion->SaveHEPDataFile(outputHEPfilename.Data(),kTRUE);
  
  AliHEPDataParser *gHEPKaon = new AliHEPDataParser(kaon,kaonSyst);
  gHEPKaon->SetXaxisName("(mT-m0)/nq (GeV/c)");
  gHEPKaon->SetName("v2/nq");
  outputHEPfilename = "HEPData/figure10-kaon";
  outputHEPfilename += "-";
  outputHEPfilename += cc.Data();
  outputHEPfilename += ".txt";
  gHEPKaon->SaveHEPDataFile(outputHEPfilename.Data(),kTRUE);

  AliHEPDataParser *gHEPProton = new AliHEPDataParser(proton,protonSyst);
  gHEPProton->SetXaxisName("(mT-m0)/nq (GeV/c)");
  gHEPProton->SetName("v2/nq");
  outputHEPfilename = "HEPData/figure10-proton";
  outputHEPfilename += "-";
  outputHEPfilename += cc.Data();
  outputHEPfilename += ".txt";
  gHEPProton->SaveHEPDataFile(outputHEPfilename.Data(),kTRUE);

  AliHEPDataParser *gHEPPhi = new AliHEPDataParser(phi,phiSyst);
  gHEPPhi->SetXaxisName("(mT-m0)/nq (GeV/c)");
  gHEPPhi->SetName("v2/nq");
  outputHEPfilename = "HEPData/figure10-phi";
  outputHEPfilename += "-";
  outputHEPfilename += cc.Data();
  outputHEPfilename += ".txt";
  gHEPPhi->SaveHEPDataFile(outputHEPfilename.Data(),kTRUE);

  AliHEPDataParser *gHEPLambda = new AliHEPDataParser(lambda,lambdaSyst);
  gHEPLambda->SetXaxisName("(mT-m0)/nq (GeV/c)");
  gHEPLambda->SetName("v2/nq");
  outputHEPfilename = "HEPData/figure10-lambda";
  outputHEPfilename += "-";
  outputHEPfilename += cc.Data();
  outputHEPfilename += ".txt";
  gHEPLambda->SaveHEPDataFile(outputHEPfilename.Data(),kTRUE);

  AliHEPDataParser *gHEPXi = new AliHEPDataParser(xi,xiSyst);
  gHEPXi->SetXaxisName("(mT-m0)/nq (GeV/c)");
  gHEPXi->SetName("v2/nq");
  outputHEPfilename = "HEPData/figure10-xi";
  outputHEPfilename += "-";
  outputHEPfilename += cc.Data();
  outputHEPfilename += ".txt";
  gHEPXi->SaveHEPDataFile(outputHEPfilename.Data(),kTRUE);
  
  /*AliHEPDataParser *gHEPOmega = new AliHEPDataParser(omega,omegaSyst);
  gHEPOmega->SetXaxisName("(mT-m0)/nq (GeV/c)");
  gHEPOmega->SetName("v2/nq");
  outputHEPfilename = "HEPData/figure10-omega";
  outputHEPfilename += "-";
  outputHEPfilename += cc.Data();
  outputHEPfilename += ".txt";
  gHEPOmega->SaveHEPDataFile(outputHEPfilename.Data(),kTRUE);*/

  return axis;
}

TGraphAsymmErrors* MScale( TString  file, Int_t error, Int_t color, Int_t marker, Int_t nq, Double_t m ) {
  TGraphAsymmErrors *one = getGraph(file,error,color,marker);
  if(!one) return NULL;
  Double_t x,y,exL,exH,eyL,eyH;
  for(Int_t i=0; i!=one->GetN(); ++i) {
    one->GetPoint(i,x,y);
    exL = one->GetErrorXlow(i) / nq;
    exH = one->GetErrorXhigh(i) / nq;
    eyL = one->GetErrorYlow(i) / nq;
    eyH = one->GetErrorYhigh(i) / nq;
    y /= nq;
    x = (TMath::Sqrt(m*m+x*x) - m)/nq;
    one->SetPoint(i,x,y);
    one->SetPointEYlow(i,eyL);
    one->SetPointEYhigh(i,eyH);
    one->SetPointEXlow(i,exL);
    one->SetPointEXhigh(i,exH);
  }
  return one;
}
