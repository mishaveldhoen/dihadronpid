# Generate documentation for the project, and immediately upload the result to 
# www.phys.uu.nl/~veldh122
#!/bin/bash

# Variables
SOLISID="veldh122"
SERVER="storage.staff.science.uu.nl"

# Update the documentation
doxygen

# Mount the www drive
mkdir www
mount -t smbfs //$SOLISID@$SERVER/$SOLISID/www ./www

# Copy files from ./html to ./www recursively.
# r = recursive, u = update, v = verbose, t = preserve mod. time info for files. 
rsync -ruvt ./html/ ./www/

# Unmount drive (take half a second for rsync to release its resources)
sleep 0.5 
umount //$SOLISID@$SERVER/$SOLISID/www
rmdir www
