#ifndef ALICORRELATIONDIHADRONPID_H
#define ALICORRELATIONDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TMath.h"
#include "TList.h"
#include "TAxis.h"
#include "TObjArray.h"
#include "TH2I.h"
#include "TString.h"

class AliCorrelationDiHadronPID {

public:
	AliCorrelationDiHadronPID();
	AliCorrelationDiHadronPID(const TList* outputlist, const TString& detector = "TOFTPC");
	~AliCorrelationDiHadronPID();
	Bool_t CopyObjectsToBuffer(const TList* list);
	void CleanBuffer();

private:
	
	// Operations on histograms (hist, const hist2, option) -> void.
	typedef void (AliCorrelationDiHadronPID::*Operation)(TObject*, const TObject*, const TString&);
	void ScaleMaxBinToOne(TObject* hist, const TObject*, const TString&);
	void DivideBy2DHist(TObject* hist, const TObject* hist2D, const TString&);
	void DivideTHnBy2DHist(TObject* hist, const TObject* hist2D, const TString&);
	void MultiplyBy2DMap(TObject* hist, const TObject* map2D, const TString&);
	void MultiplyTHnBy2DMap(TObject* hist, const TObject* map2D, const TString&);
	void SelectMassAssumption(TObject* hist, const TObject*, const TString&);
	void OperateOnBuffer(TObjArray* buffer, Operation opr, const TObject* hist = 0x0, const TString& opt = "");
	void OperateOnBuffer(TObjArray* buffer1, const TObjArray* buffer2, Operation opr, const TString& opt = "");

	// Numerical property of a histogram (hist, ptbin, species) -> number.
	typedef Float_t (AliCorrelationDiHadronPID::*Property)(const TObject*, const Int_t&, const Int_t&) const;
	TObjArray* PropertyOfBuffer(const TObjArray*, Property) const {return 0x0;} /* TODO */

	// Projection of a histogram (hist, ptbin, Zaxis) -> hist.
	typedef TObject* (AliCorrelationDiHadronPID::*Projection)(const TObject*, const Int_t&, const Bool_t&) const;
	TObject* Project3DOnXY(const TObject* hist3D, const Int_t& iPtBin = -1, const Bool_t& ZisPt = kFALSE) const;
	TObject* Project3DOnZ(const TObject* hist3D, const Int_t&, const Bool_t&) const;
	TObject* ProjectTHn(const TObject* histnD, const Int_t&, const Bool_t& FirstTwoAxes) const;
	TObjArray* ProjectionOfBuffer(const TObjArray* buffer, Projection proj, const Bool_t& opt = kFALSE) const;

	// Shape of a region in DPhiDEta (x,y,specifications) -> kTRUE/kFALSE
	typedef Bool_t (AliCorrelationDiHadronPID::*Shape)(const Float_t&, const Float_t&, const TArrayF&) const;
	Bool_t Oval(const Float_t& DPhi, const Float_t& DEta, const TArrayF& specs) const; 
	Bool_t Rectangle(const Float_t& DPhi, const Float_t& DEta, const TArrayF& specs) const;
	Bool_t SymRectangles(const Float_t& DPhi, const Float_t& DEta, const TArrayF& specs) const;
	TH2I* CreateDPhiDEtaMap(Shape sh, const TArrayF& specs, const TString& title) const;

	// Other private methods (TODO: should move to AliFunctionsDiHadronPID).
	Float_t RatioError(const Float_t& xx, const Float_t& xe, const Float_t& yy, const Float_t& ye) const {
		return TMath::Sqrt( (xx * xx * ye * ye + yy * yy * xe * xe) / (yy * yy * yy * yy) );
	}

public:

	// Settings.
	void SetDebugLevel(Int_t debuglevel) {fDebug = debuglevel;}
	void SetUsedDetectors(TString& useddetectors) {fUsedDetectors = useddetectors;}

	Int_t GetDebugLevel() const {return fDebug;}
	TString GetUsedDetectors() {return fUsedDetectors;}

	// Interface for operations on internal Mixed Event buffer.
	TObjArray* GetCloneMixedEvents() const {return (TObjArray*)fMixedEvents->Clone();}
	void NormalizeMixedEvents() {
		if (!fMixedEventsAreNormalized) {OperateOnBuffer(fMixedEvents, &AliCorrelationDiHadronPID::ScaleMaxBinToOne);}
		fMixedEventsAreNormalized = kTRUE;
	}						

	// Interface for operations on internal CorrelationsUI buffer.
	TObjArray* GetCloneCorrelationsUI() {return (TObjArray*)fCorrelationsUI->Clone();}
	void DivideCorrelationsUIByMixedEvents() {
		if (!fCorrelationsUIAreCorrected) {OperateOnBuffer(fCorrelationsUI, fMixedEvents, &AliCorrelationDiHadronPID::DivideBy2DHist);}
		fCorrelationsUIAreCorrected = kTRUE;
	};

	// Interface for operations on internal CorrelationsTOF buffer.
	TObjArray* GetCloneCorrelationsTOF() const {return (TObjArray*)fCorrelationsPID->Clone();}
	TObjArray* GetCloneCorrelationsTOFProjDPhiDEta() const {return ProjectionOfBuffer(fCorrelationsPID, &AliCorrelationDiHadronPID::Project3DOnXY);}
	TObjArray* GetCloneCorrelationsTOFProjTOF() const {return ProjectionOfBuffer(fCorrelationsPID, &AliCorrelationDiHadronPID::Project3DOnZ);}
	void DivideCorrelationsTOFByMixedEvents() {
		if (!fCorrelationsPIDAreCorrected) {OperateOnBuffer(fCorrelationsPID, fMixedEvents, &AliCorrelationDiHadronPID::DivideBy2DHist);}
		fCorrelationsPIDAreCorrected = kTRUE;
	}

	// Interface for operations on internal CorrelationsTOFTPC buffer (array of 4D histograms).
	TObjArray* GetCloneCorrelationsTOFTPC() const {return (TObjArray*)fCorrelationsPID->Clone();}
	TObjArray* GetCloneCorrelationsTOFTPCProjDPhiDEta() const {return ProjectionOfBuffer(fCorrelationsPID, &AliCorrelationDiHadronPID::ProjectTHn, kTRUE);}
	TObjArray* GetCloneCorrelationsTOFTPCProjTOFTPC() const {return ProjectionOfBuffer(fCorrelationsPID, &AliCorrelationDiHadronPID::ProjectTHn, kFALSE);} 
	void DivideCorrelationsTOFTPCByMixedEvents() {
		if (!fCorrelationsPIDAreCorrected) {OperateOnBuffer(fCorrelationsPID, fMixedEvents, &AliCorrelationDiHadronPID::DivideTHnBy2DHist);}
		fCorrelationsPIDAreCorrected = kTRUE;
	}
	void MultiplyCorrelationsTOFTPCByMap(const TObject* map) {OperateOnBuffer(fCorrelationsPID, &AliCorrelationDiHadronPID::MultiplyTHnBy2DMap, map);}
	void SelectMassAssumptionBuffer() {OperateOnBuffer(fCorrelationsPID, &AliCorrelationDiHadronPID::SelectMassAssumption);}

	// Interface for operations on external buffer.
	void MultiplyExternalBufferBy2DMap(TObjArray* buffer, const TObject* map); 
	TObjArray* GetCloneExternalBufferTOFProj(const TObjArray* buffer) const {return ProjectionOfBuffer(buffer, &AliCorrelationDiHadronPID::Project3DOnZ);}
	TObjArray* GetCloneExternalBufferTOFTPCProj(const TObjArray* buffer) const {return ProjectionOfBuffer(buffer, &AliCorrelationDiHadronPID::ProjectTHn, kFALSE);}
	TObjArray* GetCloneExternalBufferDPhiDEtaProj(const TObjArray* buffer) const; 

	// Interface for DPhiDEta maps (how can we make default arguments for const ref's?). 
	TH2I* CreateDPhiDEtaMapWithOvalArea(const TArrayF& specs, const TString& title) const {
		return CreateDPhiDEtaMap(&AliCorrelationDiHadronPID::Oval, specs, title.EqualTo("") ? "Oval" : title);}
	TH2I* CreateDPhiDEtaMapWithRectangularArea(const TArrayF& specs, const TString& title) const {
		return CreateDPhiDEtaMap(&AliCorrelationDiHadronPID::Rectangle, specs, title.EqualTo("") ? "Rectangle" : title);}
	TH2I* CreateDPhiDEtaMapWithSymmetricRectangularArea(const TArrayF& specs, const TString& title) const {
		return CreateDPhiDEtaMap(&AliCorrelationDiHadronPID::SymRectangles, specs, title.EqualTo("") ? "SymRectangle" : title);}

public:
	TAxis* GetClonePtAxis() {return (TAxis*)(fPtAxis->Clone());}

private:
	
	// Pointers to the cloned objects.
	TAxis*							fDPhiAxis;
	TAxis*							fDEtaAxis;
	TAxis*							fPtAxis;					// Variable pT axis (TOF bins).
	TObjArray*						fMixedEvents;			// Contains TH2F's
	TObjArray*						fCorrelationsUI;			// Contains TH2F's
	TObjArray*						fCorrelationsPID;				// Contains TH3F's

	// Used Detectors
	TString 						fUsedDetectors;

	// Flags for performed operations.
	Bool_t							fBufferIsFilled;
	Bool_t							fMixedEventsAreNormalized;
	Bool_t							fCorrelationsUIAreCorrected;	
	Bool_t							fCorrelationsPIDAreCorrected;

	// Debug flag.
	Int_t							fDebug;

};

#endif
