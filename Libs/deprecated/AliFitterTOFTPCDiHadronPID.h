#ifndef ALIFITTERTOFTPCDIHADRONPID_H
#define ALIFITTERTOFTPCDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

/* TODO: MismatchMap: In TPC/TOF, mismatches aren't spread out uniformly in TPC,
         so we need some sort of "mismatch map". 

         Alternatively, we will need to make a 2D mismatch histogram, i.e, 
         where a track is matched with a random TOF hit, but with the correct
         TPC hit. */


#include "AliFitterBaseDiHadronPID.h"
#include "AliSpectrumDiHadronPID.h"

//class AliSpectrumDiHadronPID;

class AliFitterTOFTPCDiHadronPID : public AliFitterBaseDiHadronPID {

public:
	AliFitterTOFTPCDiHadronPID();
	virtual ~AliFitterTOFTPCDiHadronPID();

private:
	void InitializeParameterArrays();

public:
	enum EFitFunction {
		kNotIncluded = 0,
		kGauss2D = 1,
		kGauss2DWithTailTOF = 2,
		kGauss2DWithTailTPC = 3,
		kGauss2DWithTailTOFAndTPC = 4
	};

	enum EFitStrategy {
		kPbPbDefault = 0
	};
	
	// Prints of the fit.
	void PrintFit(TCanvas* cvs, Int_t pad);
	void PrintFitPull(TCanvas* cvs, Int_t pad);
	void PrintFitTOFProjection(TCanvas* cvs, Int_t pad, Int_t TPCbinMin, Int_t TPCbinMax);
	void PrintFitTPCProjection(TCanvas* cvs, Int_t pad, Int_t TOFbinMin, Int_t TOFbinMax);

	Int_t DoFitWithStrategyPbPb(Int_t strategy, TObjArray* fitstages, AliSpectrumDiHadronPID* spectrum = 0x0,
		const char* vary_par = "", Int_t vary_species = 0, Bool_t vary_updown = 0);
	Int_t DoFitWithStrategypp(Int_t strategy, TObjArray* fitstages, AliSpectrumDiHadronPID* spectrum = 0x0,
		const char* vary_par = "", Int_t vary_species = 0, Bool_t vary_updown = 0);

	void SetHistogram(const TH1* hist);
	void SetMismatchedBackground(const TH1* hist);
	void SetUsedFunction(Int_t species, Int_t function);	

	void EstimateNParticles();
	void EstimateNMismatch(Int_t option);

	// Getters for parameter indices (every parameter has an index in the Par values array, par error array, etc.)
	Int_t GetNBgdParN() const {return 0;}
	Int_t GetIntegralParN(Int_t species) const {return 1 + species * 7;}
	Int_t GetSigmaTOFParN(Int_t species) const {return 2 + species * 7;}
	Int_t GetMuTOFParN(Int_t species) const {return 3 + species * 7;}
	Int_t GetTailTOFParN(Int_t species) const {return 4 + species * 7;}
	Int_t GetSigmaTPCParN(Int_t species) const {return 5 + species * 7;}
	Int_t GetMuTPCParN(Int_t species) const {return 6 + species * 7;}
	Int_t GetTailTPCParN(Int_t species) const {return 7 + species * 7;}

	// Getters for parameter values.
	Double_t GetNBgd() const {return fParValue->At(GetNBgdParN());}
	Double_t GetIntegral(Int_t species) const {return fParValue->At(GetIntegralParN(species));}
	Double_t GetSigmaTOF(Int_t species) const {return fParValue->At(GetSigmaTOFParN(species));}
	Double_t GetMuTOF(Int_t species) const {return fParValue->At(GetMuTOFParN(species));}
	Double_t GetTailTOF(Int_t species) const {return fParValue->At(GetTailTOFParN(species));}
	Double_t GetSigmaTPC(Int_t species) const {return fParValue->At(GetSigmaTPCParN(species));}
	Double_t GetMuTPC(Int_t species) const {return fParValue->At(GetMuTPCParN(species));}
	Double_t GetTailTPC(Int_t species) const {return fParValue->At(GetTailTPCParN(species));}

	// Getters for paramter errors.
	Double_t GetNBgdError() const {return fParError->At(GetNBgdParN());}
	Double_t GetIntegralError(Int_t species) const {return fParError->At(GetIntegralParN(species));}
	Double_t GetSigmaTOFError(Int_t species) const {return fParError->At(GetSigmaTOFParN(species));}
	Double_t GetMuTOFError(Int_t species) const {return fParError->At(GetMuTOFParN(species));}
	Double_t GetTailTOFError(Int_t species) const {return fParError->At(GetTailTOFParN(species));}
	Double_t GetSigmaTPCError(Int_t species) const {return fParError->At(GetSigmaTPCParN(species));}
	Double_t GetMuTPCError(Int_t species) const {return fParError->At(GetMuTPCParN(species));}
	Double_t GetTailTPCError(Int_t species) const {return fParError->At(GetTailTPCParN(species));}

	// Misc. getters.
	Int_t GetNbinsInFit() const;

	Double_t GetSumNChargedAllBins() const;
	Double_t GetSumNChargedInFitRegion() const;
	Double_t GetIntegralOverFitFunction();
	Double_t GetIntegralOverFitFunctionInFitRegion();
	
	TArrayI* GetAllParsForSystematics() const;
	TArrayI* GetUsedParsForSystematics(Int_t fitstrategy) const;

	// Setters for parameter values.
	void SetParameter(Int_t iPar, Double_t value) {fParValue->SetAt(value, iPar);} // Still safe since fParValue derives from TArray/
	void SetNBgd(Double_t value) {fParValue->SetAt(value, GetNBgdParN());}
	void SetIntegral(Int_t species, Double_t value) {fParValue->SetAt(value, GetIntegralParN(species));}
	void SetSigmaTOF(Int_t species, Double_t value) {fParValue->SetAt(value, GetSigmaTOFParN(species));}
	void SetMuTOF(Int_t species, Double_t value) {fParValue->SetAt(value, GetMuTOFParN(species));}
	void SetTailTOF(Int_t species, Double_t value) {fParValue->SetAt(value, GetTailTOFParN(species));}
	void SetSigmaTPC(Int_t species, Double_t value) {fParValue->SetAt(value, GetSigmaTPCParN(species));}
	void SetMuTPC(Int_t species, Double_t value) {fParValue->SetAt(value, GetMuTPCParN(species));}
	void SetTailTPC(Int_t species, Double_t value) {fParValue->SetAt(value, GetTailTPCParN(species));}

	// Setters for fixing parameters.
	void SetFixParameter(Int_t iPar, Bool_t fixed = kTRUE) {fIsParFixed->SetAt(fixed, iPar);} // Still safe since fIsParFixed derives from TArray.
	void SetFixNBgd(Bool_t fixed = kTRUE) {fIsParFixed->SetAt(fixed, GetNBgdParN());}
	void SetFixIntegral(Int_t species, Bool_t fixed = kTRUE) {fIsParFixed->SetAt(fixed, GetIntegralParN(species));}
	void SetFixSigmaTOF(Int_t species, Bool_t fixed = kTRUE) {fIsParFixed->SetAt(fixed, GetSigmaTOFParN(species));}	
	void SetFixMuTOF(Int_t species, Bool_t fixed = kTRUE) {fIsParFixed->SetAt(fixed, GetMuTOFParN(species));}
	void SetFixTailTOF(Int_t species, Bool_t fixed = kTRUE) {fIsParFixed->SetAt(fixed, GetTailTOFParN(species));}
	void SetFixSigmaTPC(Int_t species, Bool_t fixed = kTRUE) {fIsParFixed->SetAt(fixed, GetSigmaTPCParN(species));}	
	void SetFixMuTPC(Int_t species, Bool_t fixed = kTRUE) {fIsParFixed->SetAt(fixed, GetMuTPCParN(species));}
	void SetFixTailTPC(Int_t species, Bool_t fixed = kTRUE) {fIsParFixed->SetAt(fixed, GetTailTPCParN(species));}

	// Setters for fit range.
	void SetTOFMin(Double_t min) {SetTOFRange(min, fTOFmax);}
	void SetTOFMax(Double_t max) {SetTOFRange(fTOFmin, max);}
	void SetTPCMin(Double_t min) {SetTPCRange(min, fTPCmax);}
	void SetTPCMax(Double_t max) {SetTPCRange(fTPCmin, max);}
	void SetTOFRange(Double_t min, Double_t max);
	void SetTPCRange(Double_t min, Double_t max);

private:
	// Private methods (overloaded).
	double ChiSquared(const double* xx) const;	
	void UpdateFitDescription();

	// Fit ranges (not overloaded).
	void SetRangePbPbDefault();
	void SetRangeppDefault();

	// Peaks - how are the different peaks represented in the fit (not overloaded).
	TArrayI* GetPeakDescriptionPbPbDefault();
	TArrayI* GetPeakDescriptionppDefault();

	// Parameterisations
	Double_t GetFitParameterFromParameterisationPbPbDefault(Int_t iPar);
	Double_t GetFitParameterFromParameterisationppDefault(Int_t iPar);

	// Fit strategies (not overloaded).
	Int_t DoFitWithStrategyPbPbDefault(TObjArray* fitstages, Int_t mismatchrange = 0, const TArrayI* fromParam = 0x0);

	void FixParameters(const TArrayI* pars);

	// Fit strategies pp
	Int_t DoFitWithStrategyppFixedTOFTail(TObjArray* fitstages, Int_t mismatchrange = 0, Bool_t fixtailpi = kTRUE, Bool_t fixtailka = kTRUE, Bool_t fixtailpr =kTRUE);
	Int_t DoFitWithStrategyppDefault(TObjArray* fitstages, Int_t mismatchrange = 0);

	ClassDef(AliFitterTOFTPCDiHadronPID, 1);

};

#endif
