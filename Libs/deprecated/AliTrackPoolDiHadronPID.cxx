/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Track Pool
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include <iostream>
#include "AliTrackPoolDiHadronPID.h"

using namespace std;

ClassImp(AliTrackPoolDiHadronPID);

// -----------------------------------------------------------------------
AliTrackPoolDiHadronPID::AliTrackPoolDiHadronPID(TArrayD* vtxZ, TArrayD* cent):
	TObject(),
	fVertexZAxis(0x0),
	fCentralityAxis(0x0),
	fPoolArray(0x0),
	fRndGen(0x0),
	fBufferPhi(-999.),
	fBufferEta(-999.)

{

	// Constructor. 
	fVertexZAxis 		= new TAxis(vtxZ->GetSize() - 1, vtxZ->GetArray());
	fCentralityAxis 	= new TAxis(cent->GetSize() - 1, cent->GetArray());

	Int_t poolBins = fVertexZAxis->GetNbins() * fCentralityAxis->GetNbins();
	fPoolArray = new TObjArray(poolBins);
	fPoolArray->SetOwner(kTRUE);

	// Create the TTrees, branches point to the class members fBufferPhi, fBufferEta.
	for (Int_t iBin = 0; iBin < poolBins; ++iBin) {
		TTree* tmpTree = new TTree(Form("pool_%i",iBin), Form("pool_%i",iBin));
		tmpTree->Branch("phi", &fBufferPhi);
		tmpTree->Branch("eta", &fBufferEta);
		fPoolArray->AddAt(tmpTree, iBin);
	}

	// Create a random number generator.
	fRndGen = new TRandom3();

}

// -----------------------------------------------------------------------
AliTrackPoolDiHadronPID::~AliTrackPoolDiHadronPID() {

	if (fVertexZAxis) {delete fVertexZAxis; fVertexZAxis = 0x0;}
	if (fCentralityAxis) {delete fCentralityAxis; fCentralityAxis = 0x0;}
	if (fPoolArray) {delete fPoolArray; fPoolArray = 0x0;}

}

/*
// -----------------------------------------------------------------------
TTree* AliTrackPoolDiHadronPID::GetClonePool(Int_t vtxZBin, Int_t centBin) const {

	Int_t index = GetArrayIndex(vtxZBin, centBin);
	if (index < 0) {return 0x0;} 
	else {return (TTree*)(fPoolArray->At(index));}

}

// -----------------------------------------------------------------------
TTree* AliTrackPoolDiHadronPID::GetClonePool(Double_t vtxZ, Double_t cent) const {

	Int_t index = GetArrayIndex(vtxZ, cent);
	if (index < 0) {return 0x0;} 
	else {return (TTree*)(fPoolArray->At(index));}

}
*/

// -----------------------------------------------------------------------
void AliTrackPoolDiHadronPID::GetRandomTrigger(TArrayD& phi_eta, AliMiniEventDiHadronPID* event) {

	if (phi_eta.GetSize() != 2) {
		cout << "ERROR: phi_eta needs to have 2 entries." << endl;
		return;
	}

	// Fetch correct pool.
	Int_t index = GetArrayIndex(event->GetVtxZ(), event->GetCentrality());
	TTree* poolTmp = ((TTree*)fPoolArray->At(index));
	
	// Get a random trigger index.
	Int_t rndTriggerIndex = fRndGen->Integer((UInt_t)poolTmp->GetEntriesFast());
	poolTmp->GetEntry(rndTriggerIndex);	// Read out the random trigger.

	phi_eta.SetAt(fBufferPhi, 0);
	phi_eta.SetAt(fBufferEta, 1);

}

// -----------------------------------------------------------------------
void AliTrackPoolDiHadronPID::Fill(AliMiniEventDiHadronPID* event, AliMiniTrackDiHadronPID* track) {

	fBufferPhi = track->Phi();
	fBufferEta = track->Eta();

	Int_t index = GetArrayIndex(event->GetVtxZ(), event->GetCentrality());
	((TTree*)fPoolArray->At(index))->Fill();

}

// -----------------------------------------------------------------------
void AliTrackPoolDiHadronPID::PrintPool() const {

	// Prints the pool contents on the screen.
	for (Int_t iVtxZbin = 0; iVtxZbin <= fVertexZAxis->GetNbins(); ++iVtxZbin) {

		for (Int_t iCentBin = 0; iCentBin <= fCentralityAxis->GetNbins(); ++iCentBin) {

			// "row 0" is the row with the collumn titles.
			if (iVtxZbin == 0) {

				if (iCentBin == 0) {cout<<"|           |";}
				else {cout << Form(" %3.1f - %3.1f |", fCentralityAxis->GetBinLowEdge(iCentBin), fCentralityAxis->GetBinUpEdge(iCentBin));}

			} else {

				if (iCentBin == 0) {cout << Form("| %3.1f - %3.1f |", fVertexZAxis->GetBinLowEdge(iVtxZbin), fVertexZAxis->GetBinUpEdge(iVtxZbin));}
				else {cout << Form("   %5lli   |", ((TTree*)fPoolArray->At(GetArrayIndex(iVtxZbin, iCentBin)))->GetEntriesFast());}

			}

		}
		
		cout << endl;

	}

	cout << endl;

}

// -----------------------------------------------------------------------
Int_t AliTrackPoolDiHadronPID::GetArrayIndex(Int_t vtxZBin, Int_t centBin) const {

	if (vtxZBin < 0 || vtxZBin > fVertexZAxis->GetNbins() ||
		centBin < 0 || centBin > fCentralityAxis->GetNbins()) {return -999;}

	else {return ((vtxZBin - 1) * fCentralityAxis->GetNbins() + (centBin - 1));} 	

}

// -----------------------------------------------------------------------
Int_t AliTrackPoolDiHadronPID::GetArrayIndex(Double_t vtxZ, Double_t cent) const {

	return GetArrayIndex(fVertexZAxis->FindBin(vtxZ), fCentralityAxis->FindBin(cent));

}
