#ifndef ALIRATIOSDIHADRONPID_H
#define ALIRATIOSDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

/*
class TObjArray;
class AliSpectrumDiHadronPID;
*/

#include "TNamed.h"
#include "TObjArray.h"
#include "TGraphErrors.h"
#include "TArrayI.h"
#include "TArrayD.h"
#include "TH1F.h"
#include "TFile.h"

#include "AliSpectrumDiHadronPID.h"

class AliRatiosDiHadronPID : public TNamed {

public:
	AliRatiosDiHadronPID();
	AliRatiosDiHadronPID(const char* name, const char* title);
private:
	Bool_t Initialize();
public:
	~AliRatiosDiHadronPID();

	// Setters.
	void SetDebugLevel(Int_t debuglvl) {fDebug = debuglvl;}
	void SetOutputDir(const char* outputdir);					// Where the PDF's will be saved.
	void SetCompanion(AliRatiosDiHadronPID* companion);			// What ratios to compare to.
	void SetNtriggers(Int_t species, Double_t Ntriggers) {fNtriggersForSpecies->SetAt(Ntriggers, species);}
	void SetVarySingleParameter(Bool_t varysingleparameter, const char* parametername);
	void SetUseRebinnedSpectra(Bool_t usereb = kTRUE) {fUseRebinnedSpectra = usereb;}
	void SetUseCorrectedSpectra(Bool_t usecorr = kTRUE) {fUseCorrectedSpectra = usecorr;}

	// Visualization options.
	void SetHistLineWidth(Int_t width) {fHistLineWidth = width;}
	void SetSystErrorbarWidth(Double_t width) {fSystErrorbarWidth = width;}
	void SetYAxisMargin(Double_t margin) {fYAxisMargin = margin;}
	void SetFirstHistogramFillColor(Int_t color) {fFirstHistogramFillColor = color;}
	void SetSecondHistogramFillColor(Int_t color) {fSecondHistogramFillColor = color;}
	void SetThirdHistogramFillColor(Int_t color) {fThirdHistogramFillColor = color;}
	void SetFourthHistogramFillColor(Int_t color) {fFourthHistogramFillColor = color;}
	void SetSecondHistogramLineColor(Int_t color) {fSecondHistogramLineColor = color;}	
	void SetFirstHistogramMarkerStyle(Int_t style) {fFirstHistogramMarkerStyle = style;}
	void SetSecondHistogramMarkerStyle(Int_t style) {fSecondHistogramMarkerStyle = style;}
	void SetThirdHistogramMarkerStyle(Int_t style) {fThirdHistogramMarkerStyle = style;}
	void SetFourthHistogramMarkerStyle(Int_t style) {fFourthHistogramMarkerStyle = style;}

	// Expects a file containing an AliSpectrumDiHadronPID class.
	//  - region should be either "All", "Peak", or "Bulk"
	//  - name can be anything, but should be unique for different variations (for
	//    example PeakI, Peak4x4, etc.)
	void InputSpectrum(const char* filename, const char* region, const char* name, Int_t species, Int_t Nbins);	
	
	void ExportSpectraToPDF(const char* region, Bool_t showsystematics = 1, Bool_t makecomparison = kFALSE);
	void ExportJetSpectraToPDF(Int_t syst_strat_subtr = 1, Bool_t makecomparison = kFALSE);

	void ExportRatioToPDF(const char* region, Int_t syst_strat_div = 3, Bool_t makecomparison = kFALSE);
	void ExportJetRatioToPDF(Int_t syst_strat_subtr = 1, Int_t syst_strat_div = 3, Bool_t makecomparison = kFALSE);

	void ExportFinalPicturesToPDF(Int_t syst_strat_subtr = 1, Int_t syst_strat_div = 3, Bool_t makecomparison = kFALSE);

	// Retrieve a deep copy of the spectra (not very efficient)
	TObjArray* GetDeepCopyOfSpectra() const {return (TObjArray*)fSpectra->Clone();}
	TArrayI* GetCopyNbinsForRegion() const {return (new TArrayI(*fNbinsForRegion));}

private:
	// Conversions.
	Int_t RegionToInt(const char* region) const;
	const char* IntToRegion(Int_t region) const;
	const char* IntToSpecies(Int_t region) const;	
	const char* IntToSpeciesLaTeX(Int_t region) const;
	Bool_t HistogramIsTrivial(const TH1F* hist) const;

	// Methods to get the spectra and variations on it in the desired presentation.	
	TObjArray* GetSpectrumArray(const char* region, Bool_t fromcompanion = kFALSE) {return GetSpectrumArray(RegionToInt(region), fromcompanion);}
	TObjArray* GetSpectrumArray(Int_t region, Bool_t fromcompanion = kFALSE);
	AliSpectrumDiHadronPID* GetSpectrum(const char* region, Int_t species, Bool_t fromcompanion = kFALSE) {return GetSpectrum(RegionToInt(region), species, fromcompanion);}
	AliSpectrumDiHadronPID* GetSpectrum(Int_t region, Int_t species, Bool_t fromcompanion = kFALSE) {return (AliSpectrumDiHadronPID*)(GetSpectrumArray(region, fromcompanion)->At(species));}
	TObjArray* GetMeasurementWithVariations(const char* region, Int_t species, Bool_t fromcompanion = kFALSE) {return GetMeasurementWithVariations(RegionToInt(region), species, fromcompanion);}
	TObjArray* GetMeasurementWithVariations(Int_t region, Int_t species, Bool_t fromcompanion = kFALSE);
	TObjArray* GetJetMeasurementWithVariations(Int_t species, Int_t syst_strat_subtr, Bool_t fromcompanion = kFALSE);

	// Properties of arrays.
	Bool_t ArraysAreCompatible(const TObjArray* array1, const TObjArray* array2) const;
	Double_t GetSTD(const TArrayD* array, const Double_t* measurement = 0x0) const;
	Double_t GetMean(const TArrayD* array) const;
	Double_t GetMaxDeviation(const TArrayD* array, const Double_t* measurement = 0x0) const;

	// Manipulations of arrays with histograms.
	void Scale(TObjArray* measurementwithvariations, Double_t factor = 1., Bool_t width = kFALSE);
	void CheckOutArrayOfHistograms(const TObjArray* array) const;
	TObjArray* Subtract(const TObjArray* measurementwithvariations1, const TObjArray* measurementwithvariations2, Int_t syst_strat_subtr = 0);
	TH1F* Subtract(const TH1F* hist1, const TH1F* hist2) const;
	TObjArray* Divide(const TObjArray* measurementwithvariations1, const TObjArray* measurementwithvariations2, Int_t syst_strat_div = 0);
	TH1F* Divide(const TH1F* hist1, const TH1F* hist2) const;

	// Creating graphs.
	TGraphErrors* CreateStatErrorGraph(TObjArray* measurementwithvariations, TGraphErrors* systerrors = 0x0) {return CreateStatErrorGraph((TH1F*)measurementwithvariations->At(0), systerrors);}
	TGraphErrors* CreateStatErrorGraph(TH1F* measurement, TGraphErrors* systerrors = 0x0);
	TGraphErrors* CreateSystErrorGraph(TObjArray* measurementwithvariations);	

	// Manipulations with TGraphErrors
	//void SetSystErrorBinWidthToPercentage(TGraphErrors* graph, Double_t percentage) const;
	void SetErrorAsGroupMember(TGraphErrors* staterror, TGraphErrors* systerror, Int_t position, Int_t groupsize = 2) const;
	Double_t GetMinimum(const TGraphErrors* graph) const; 
	Double_t GetMaximum(const TGraphErrors* graph) const;
	TH1F* GetPercentages(const TGraphErrors* systerrors);
	TH1F* GetRatio(const TGraphErrors* staterrors1, const TGraphErrors* staterrors2) const;

	// Manipulations of a canvas.
	void OptimizeYRange(TCanvas* canvas);

	// Output directory.
	Bool_t DoesOutputDirExist(Bool_t create = kTRUE);

private:
	TObjArray*						fSpectra;
	TObjArray*						fCompanionSpectra;
	TObjArray*						fNameForRegion;
	TArrayI*						fNbinsForRegion;
	TArrayI*						fNbinsForRegionCompanion;
	TArrayD*						fNtriggersForSpecies;
	TAxis*							fXaxis;	
	TString*						fDirName;
	Int_t							fDebug;

	// Visualization options.
	Int_t							fHistLineWidth;
	Double_t 						fSystErrorbarWidth;
	Double_t						fYAxisMargin;
	Int_t							fFirstHistogramFillColor;
	Int_t							fSecondHistogramFillColor;
	Int_t							fThirdHistogramFillColor;
	Int_t							fFourthHistogramFillColor;
	Int_t							fSecondHistogramLineColor;		// Only used when no systematic errors are plotted.
	Int_t							fFirstHistogramMarkerStyle;
	Int_t							fSecondHistogramMarkerStyle;
	Int_t							fThirdHistogramMarkerStyle;
	Int_t							fFourthHistogramMarkerStyle;

	// Ohter options.
	Int_t							fSystErrorDefinition;
	Bool_t							fVarySingleParameter;
	Bool_t							fUseRebinnedSpectra;
	Bool_t							fUseCorrectedSpectra;
	TString*						fSingleParameterName;

	ClassDef(AliRatiosDiHadronPID, 1);

};

#endif
