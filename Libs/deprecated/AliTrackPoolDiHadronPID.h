#ifndef ALITRACKPOOLDIHADRONPID_H
#define ALITRACKPOOLDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TObject.h"
#include "TTree.h"
#include "TArrayD.h"
#include "TAxis.h"
#include "TObjArray.h"
#include "TRandom3.h"
#include "AliMiniTrackDiHadronPID.h"
#include "AliMiniEventDiHadronPID.h"

class AliTrackPoolDiHadronPID : public TObject {

public:
	AliTrackPoolDiHadronPID(TArrayD* vtxZ, TArrayD* cent);
	~AliTrackPoolDiHadronPID();
	// Add merge functionality!

	//TTree*					GetClonePool(Int_t vtxZBin, Int_t centBin) const;
	//TTree*					GetClonePool(Double_t vtxZ, Double_t cent) const;

	void					GetRandomTrigger(TArrayD& phi_eta, AliMiniEventDiHadronPID* event);

	void					Fill(AliMiniEventDiHadronPID* event, AliMiniTrackDiHadronPID* track);
	void					PrintPool() const;

private:
	Int_t					GetArrayIndex(Int_t vtxZBin, Int_t centBin) const;	// Q: Does it make sense to make private
	Int_t					GetArrayIndex(Double_t vtxZ, Double_t cent) const;  //    functions const?

private:
	TAxis*					fVertexZAxis;			//-> TAxis has "FindBin" functionality. 
	TAxis*					fCentralityAxis;		//->
	TObjArray*				fPoolArray;				//-> Array holding all the pools
	TRandom3*				fRndGen;				//!

	Double_t				fBufferPhi;				//!
	Double_t				fBufferEta;				//!

	ClassDef(AliTrackPoolDiHadronPID, 1);

};

#endif
