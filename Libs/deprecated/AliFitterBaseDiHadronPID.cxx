/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Virtual Base Class for fitter.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "AliFitterBaseDiHadronPID.h"

#include "TString.h"
#include "TBrowser.h"
#include "TArray.h"

#include "Math/Factory.h"
#include "Math/Functor.h"

#include "AliFunctionsDiHadronPID.h"

ClassImp(AliFitterBaseDiHadronPID);

// -----------------------------------------------------------------------
AliFitterBaseDiHadronPID::AliFitterBaseDiHadronPID() :
	TObject(), 
	fData(0x0),
	fMismatch(0x0),
	fPeaksHisto(0x0),
	fFitResult(0x0),
	fMismatchScaled(0x0),
	fPeaksFunc(0x0),
	fPeakSwapPenalty(kFALSE),
	fPeakSwapPenaltyOrder(0),
	fPeakDevPenalty(kFALSE),
	fPeakDevPenaltyTOFRange(100.),
	fPeakDevPenaltyTPCRange(1.),	
	fPeakDevPenaltyOrder(0),
	fPeakSigmaPenalty(kFALSE),
	fPeakSigmaPenaltyTOFRange(100.),
	fPeakSigmaPenaltyTPCRange(5.),
	fPeakSigmaPenaltyOrder(0),
	fNegIntPenalty(kFALSE),
	fNegIntPenaltyOrder(0),
	fMinimizerType("Minuit2"),
	fMinimizerMethod("Migrad"),
	fMaxFunctionCalls(200000),
	fMaxIterations(20000),
	fTolerance(0.001),
	fFitStrategy(0),
	fUsedFunction(0x0),
	fNFitParams(0),
	fParName(0x0),
	fParValue(0x0),
	fParError(0x0),
	fParStepSize(0x0),
	fIsParFixed(0x0),
	fParSmallVariation(0x0),
	fFitStatus(-1),
	fMinEntriesForFit(0),
	fTOFmin(-1.e10),
	fTOFmax(1.e10),
	fTPCmin(-1.e10),
	fTPCmax(1.e10),
	fPtMin(-999.),
	fPtMax(-999.),
	fEtaMin(-0.8),
	fEtaMax(0.8), 
	fPtRangeIsSet(kFALSE),
	fMassAssumption(-1),
	fMassAssumptionIsSet(kFALSE),
	fCanvas(0x0),
	fUseTailSlope(kTRUE),
	fDebug(0),
	fPrintHessian(kFALSE)

{

	// Constructor
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}	

	fUsedFunction = new TArrayI(4);	// Array keeping track of what function is used to describe what particle (functions defined in derived classes).
	fPeaksFunc = new TObjArray(4);	// Array keeping TF2 descriptions of the peak.

}

// -----------------------------------------------------------------------
AliFitterBaseDiHadronPID::~AliFitterBaseDiHadronPID() {

	// Destructor.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	if (fPeaksHisto) {delete fPeaksHisto; fPeaksHisto = 0x0;}
	if (fPeaksFunc) {delete fPeaksFunc; fPeaksFunc = 0x0;}
	if (fUsedFunction) {delete fUsedFunction; fUsedFunction = 0x0;}
	if (fParName) {delete fParName; fParName = 0x0;}
	if (fParValue) {delete fParValue; fParValue = 0x0;}
	if (fParError) {delete fParError; fParError = 0x0;}
	if (fParStepSize) {delete fParStepSize; fParStepSize = 0x0;}
	if (fParSmallVariation) {delete fParSmallVariation; fParSmallVariation = 0x0;}
	if (fIsParFixed) {delete fIsParFixed; fIsParFixed = 0x0;}

	if (fCanvas) {delete fCanvas; fCanvas = 0x0;}

}

// -----------------------------------------------------------------------
Int_t AliFitterBaseDiHadronPID::DoFit() {

	// Method creates a new instance of a minimizer, configures it as desired, and performs the fit.
	// Example minimzer usage: http://root.cern.ch/drupal/content/numerical-minimization
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fNFitParams) {cout << Form("%s -> ERROR: No fit parameters defined.",__func__) << endl; return 0;}

	// 1) Creates a function and fitter according to the current settings.
	ROOT::Math::Minimizer* minimizer = CreateMinimizer();
	ROOT::Math::Functor currentchisquared(this, &AliFitterBaseDiHadronPID::ChiSquared, fNFitParams);	// ChiSquared overloaded in derived class.
	minimizer->SetFunction(currentchisquared);

	// 2) Set parameter names, and parameter values as they are currently known by AliFitterTOF or AliFitterTOFTPC.
	for (Int_t iPar = 0; iPar < fNFitParams; iPar++) {
		if (fIsParFixed->GetAt(iPar)) {
			minimizer->SetFixedVariable(iPar,((TString*)fParName->At(iPar))->Data(),(double)fParValue->At(iPar));
		} else {
			minimizer->SetVariable(iPar,((TString*)fParName->At(iPar))->Data(),(double)fParValue->At(iPar),(double)fParStepSize->At(iPar));
		}
	}

	// 3) Performs the fit.
	minimizer->Minimize();

	// 4) Calculates full Hessian (for error calculation), and print status of fitter.
	minimizer->Hesse();
	fFitStatus = (Int_t)(minimizer->Status());	
	cout << "Done minimizing, status = " << fFitStatus << ", EDM = " << minimizer->Edm() << ", NCalls = " << minimizer->NCalls() << endl;

	// 5) Print the Hessian (TODO: put in a separate method).
	if (fPrintHessian) {
		const int NDim = minimizer->NDim();
		double* h = new double[NDim * NDim];
		if (fFitStatus < 100) {minimizer->GetHessianMatrix(h);}
		if (h) {
			cout<<endl;
			cout<<"Hessian Matrix:"<<endl;
			for (Int_t iX = 0; iX < (Int_t)minimizer->NDim(); iX++) {

				cout<<Form("%20s",((TString*)GetParNames()->At(iX))->Data());

				for (Int_t iY = 0; iY < (Int_t)minimizer->NDim(); iY++) {
					if (h[iX + iY * minimizer->NDim()]) {
						if (TMath::Abs(h[iX + iY * minimizer->NDim()]) < 1e-80) {
							cout<<"   ~0   ";
						} else {
							cout<<Form("%+7.0e ",h[iX + iY * minimizer->NDim()]);
						}
					} else {
						cout<<"    0   ";
					}
				}
				cout<<endl;
			}
			cout<<endl;
		} else {
			cout<<"Couldn't calculate Hessian."<<endl;
		}

		delete h;
	}

	// 6) Updates the values and fit errors.
	const double* xfitresult = minimizer->X();
	const double* efitresult = minimizer->Errors();		// Probably some flag needs to be set to get these errors OK.
	for (Int_t iPar = 0; iPar < fNFitParams; iPar++) {
		if (!fIsParFixed->At(iPar)) {
			fParValue->SetAt(xfitresult[iPar], iPar);
			fParError->SetAt(efitresult[iPar], iPar);
		}
	}

	// 7) Small variation in the background is set to 5% of the yield (default value was a meaningless 1.)
	SetSmallVariationAt(0.05 * fParValue->At(0), 0);	

	return fFitStatus;

} 

// -----------------------------------------------------------------------
Double_t AliFitterBaseDiHadronPID::DetermineYieldAfterVaryingPar(Int_t par, Bool_t varyup, Int_t strategy) {

	// Available strategies for determining systematics:
	//  0 -> Vary all parameters (except yields for pi,ka,pr) up/ down by a value defined in the fitter, and refit only 
	//       the yields for pi, ka, pr, while keeping the rest fixed. (Quick but not very neat)
	//  1 -> Same as 0, but now the entire fit is redone except for that the varied parameter is fixed.

	// Assumes a fit has been completed, and hence that all parameter values 
	// make sense.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (fFitStatus != 0) {cout << Form("%s -> ERROR: Can't determine systematics before getting a fit with status = 0.",__func__) << endl; return 0.;}
	if (par < 0 || par > fNFitParams) {cout << Form("%s -> ERROR: Parameter %i doesn't exist",__func__,par) << endl; return 0.;}
	if (strategy < 0 || strategy > 1) {cout << Form("%s -> ERROR: Strategy %i doesn't exist",__func__,strategy) << endl; return 0.;}

	// Dump current status of the parameters (it's a copy).
	TArrayS* parfixed_tmp = new TArrayS(*GetIsParFixed());
	TArrayD* parvalues_tmp = new TArrayD(*GetParValues());
	TArrayD* parerrors_tmp = new TArrayD(*GetParErrors());
	TArrayD* parstepsize_tmp = new TArrayD(*GetParStepSize());
	Int_t fitstatus_tmp = GetFitStatus();

	//cout<<endl<<"BEFORE FIXING PARS"<<endl;
	//PrintFitParams();

	switch (strategy) {

		{case 0:
			// Fix all parameters except for the yields.
			Int_t yieldpars[4] = {GetIntegralParN(0), GetIntegralParN(1), GetIntegralParN(2), GetIntegralParN(3)}; 
			for (Int_t iPar = 0; iPar < fNFitParams; iPar++) {
				
				// Check whether the parameter is a yield. 
				Bool_t isYield = kFALSE;
				for (Int_t jPar = 0; jPar < 4; jPar++) {
					if (yieldpars[jPar] == iPar) {isYield = kTRUE;}
				}
				if (isYield) {continue;}

				// If not a yield, then fix the parameter.
				SetIsParFixedAt(kTRUE, iPar);

			}

			break;
		}

		{case 1:
			// Fix only the parameter that we're varying.
			SetIsParFixedAt(kTRUE, par);
		}

	}

	//cout<<endl<<"AFTER FIXING PARS"<<endl;
	//PrintFitParams();

	// Increase/ decrease one of the parameters by a bit.
	Double_t newparval = GetParValues()->GetAt(par);	
	if (varyup) {newparval += (*GetParSmallVariation())[par];} 
	else {newparval -= (*GetParSmallVariation())[par];}
	SetParValueAt(newparval, par);

	//cout<<endl<<"AFTER VARYING PAR"<<endl;
	//PrintFitParams();

	// Perform the fit.
	DoFit();

	//cout<<endl<<"AFTER FITTING"<<endl;
	//PrintFitParams();

	// Obtain the return value.
	Double_t newyield = GetIntegral(GetMassAssumption());

	// Restore all parameters to the succeeded fit. 
	SetParValues(parvalues_tmp);
	SetParErrors(parerrors_tmp);
	SetParStepSize(parstepsize_tmp);
	SetIsParFixed(parfixed_tmp);
	fFitStatus = fitstatus_tmp;

	//cout<<endl<<"AFTER RESTORING PARS"<<endl;
	//PrintFitParams();

	return newyield;

}

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::Reset() {

	// Restores all data members to default values.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	delete fParName; fParName = 0x0; fParName = new TObjArray(fNFitParams);
	fParName->SetOwner(kTRUE);
	delete fParValue; fParValue = 0x0; fParValue = new TArrayD(fNFitParams);
	delete fParError; fParError = 0x0; fParError = new TArrayD(fNFitParams);
	delete fParStepSize; fParStepSize = 0x0; fParStepSize = new TArrayD(fNFitParams);
	delete fIsParFixed; fIsParFixed = 0x0; fIsParFixed = new TArrayS(fNFitParams);

	InitializeParameterArrays();

}

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::SetParValueAt(Double_t value, Int_t index) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fParValue) {cout << Form("%s -> ERROR: array doesn't exist.",__func__) << endl;}
	if (index < 0 || index > fParValue->GetSize()) {cout << Form("%s -> ERROR: index %i out of bound (max: %i).",__func__,index,fParValue->GetSize()) << endl;}

	fParValue->SetAt(value, index);

}

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::SetParErrorAt(Double_t error, Int_t index) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fParError) {cout << Form("%s -> ERROR: array doesn't exist.",__func__) << endl;}
	if (index < 0 || index > fParError->GetSize()) {cout << Form("%s -> ERROR: index %i out of bound (max: %i).",__func__,index,fParError->GetSize()) << endl;}

	fParError->SetAt(error, index);

}

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::SetParStepSizeAt(Double_t stepsize, Int_t index) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fParStepSize) {cout << Form("%s -> ERROR: array doesn't exist.",__func__) << endl;}
	if (index < 0 || index > fParStepSize->GetSize()) {cout << Form("%s -> ERROR: index %i out of bound (max: %i).",__func__,index,fParStepSize->GetSize()) << endl;}

	fParStepSize->SetAt(stepsize, index);

}

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::SetIsParFixedAt(Bool_t isfixed, Int_t index) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fIsParFixed) {cout << Form("%s -> ERROR: array doesn't exist.",__func__) << endl;}
	if (index < 0 || index > fIsParFixed->GetSize()) {cout << Form("%s -> ERROR: index %i out of bound (max: %i).",__func__,index,fIsParFixed->GetSize()) << endl;}

	fIsParFixed->SetAt((Char_t)isfixed, index);

}

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::SetSmallVariationAt(Double_t smallvar, Int_t index) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fParSmallVariation) {cout << Form("%s -> ERROR: array doesn't exist.",__func__) << endl;}
	if (index < 0 || index > fParSmallVariation->GetSize()) {cout << Form("%s -> ERROR: index %i out of bound (max: %i).",__func__,index,fParSmallVariation->GetSize()) << endl;}

	fParSmallVariation->SetAt(smallvar, index);

}

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::SetParValues(const TArrayD* values) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (values->GetSize() != fNFitParams) {cout << Form("%s -> ERROR: supplied array is of size: %i, while size: %i was expected.",__func__,values->GetSize(),fNFitParams) << endl; return;}
	
	if (fParValue) {values->Copy(*fParValue);}
	else {fParValue = new TArrayD(*values);}

}

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::SetParErrors(const TArrayD* errors) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (errors->GetSize() != fNFitParams) {cout << Form("%s -> ERROR: supplied array is of size: %i, while size: %i was expected.",__func__,errors->GetSize(),fNFitParams) << endl; return;}
	
	if (fParError) {errors->Copy(*fParError);}
	else {fParError = new TArrayD(*errors);}

}

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::SetParStepSize(const TArrayD* stepsize) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (stepsize->GetSize() != fNFitParams) {cout << Form("%s -> ERROR: supplied array is of size: %i, while size: %i was expected.",__func__,stepsize->GetSize(),fNFitParams) << endl; return;}
	
	if (fParStepSize) {stepsize->Copy(*fParStepSize);}
	else {fParStepSize = new TArrayD(*stepsize);}

}

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::SetIsParFixed(const TArrayS* isfixed) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (isfixed->GetSize() != fNFitParams) {cout << Form("%s -> ERROR: supplied array is of size: %i, while size: %i was expected.",__func__,isfixed->GetSize(),fNFitParams) << endl; return;}
	
	if (fIsParFixed) {isfixed->Copy(*fIsParFixed);}
	else {fIsParFixed = new TArrayS(*isfixed);}

} 

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::SetParSmallVariation(const TArrayD* smallvar) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (smallvar->GetSize() != fNFitParams) {cout << Form("%s -> ERROR: supplied array is of size: %i, while size: %i was expected.",__func__,smallvar->GetSize(),fNFitParams) << endl; return;}
	
	if (fParSmallVariation) {smallvar->Copy(*fParSmallVariation);}
	else {fParSmallVariation = new TArrayD(*smallvar);}

} 

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::SetPtRange(Double_t min, Double_t max) {

	// Sets pT range (used to estimate peak positions)
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (min > max) {cout << Form("%s -> ERROR: min > max.",__func__) << endl; return;}

	fPtMin = min; 
	fPtMax = max;
	fPtRangeIsSet = kTRUE;

}

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::SetEtaRange(Double_t min, Double_t max) {

	// Sets pT range (used to estimate peak positions)
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (min > max) {cout << Form("%s -> ERROR: min > max.",__func__) << endl; return;}

	fEtaMin = min; 
	fEtaMax = max;
	//fPtRangeIsSet = kTRUE;

}

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::SetPeakSwapPenalty(Bool_t set, Int_t order) {

	// Chi squared penalty for peaks being swapped.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (order < 1) {cout << Form("%s -> ERROR: order should be positive.",__func__) << endl; return;}

	fPeakSwapPenalty = set;
	fPeakSwapPenaltyOrder = order;

}
// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::SetPeakDevPenalty(Bool_t set, Double_t TOFrange, Double_t TPCrange, const Int_t order) {

	// TODO: should include a different range for TPC/TOF.

	// Chi squared penalty for if a peak deviates too much from the expected position.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (order < 1) {cout << Form("%s -> ERROR: order should be positive.",__func__) << endl; return;}
	if (TOFrange < 0. || TPCrange < 0.) {cout << Form("%s -> ERROR: range should be positive.",__func__) << endl; return;}

	fPeakDevPenalty = set;
	fPeakDevPenaltyTOFRange = TOFrange;
	fPeakDevPenaltyTPCRange = TPCrange;
	fPeakDevPenaltyOrder = order;

}

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::SetPeakSigmaPenalty(Bool_t set, Double_t TOFrange, Double_t TPCrange, Int_t order) {

		// Chi squared penalty for if a peak deviates too much from the expected position.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (order < 1) {cout << Form("%s -> ERROR: order should be positive.",__func__) << endl; return;}
	if (TOFrange < 0. || TPCrange < 0.) {cout << Form("%s -> ERROR: range should be positive.",__func__) << endl; return;}

	fPeakSigmaPenalty = set;
	fPeakSigmaPenaltyTOFRange = TOFrange;
	fPeakSigmaPenaltyTPCRange = TPCrange;
	fPeakSigmaPenaltyOrder = order;

}

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::SetNegIntPenalty(Bool_t set, Int_t order) {
	
	// Chi squared penalty for peaks having a negative integral.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (order < 1) {cout << Form("%s -> ERROR: order should be positive.",__func__) << endl; return;}

	fNegIntPenalty = set;
	fNegIntPenaltyOrder = order;

}

// -----------------------------------------------------------------------
TH1F* AliFitterBaseDiHadronPID::GetChiSquaredOnePar(Int_t par, Double_t domain, Int_t Nbins) {

	// Investigate chi^2 as a function of one parameter.
	// User is responsible for deleting this object.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fNFitParams) {cout << Form("%s -> ERROR: fNFitParams = 0.",__func__) << endl; return 0x0;} 

	TH1F* hout = new TH1F(Form("ChiSquaredOnePar_%i",par), 
		Form("#chi^{2} vs %s;%s;#chi^{2}", ((TString*)fParName->At(par))->Data(), ((TString*)fParName->At(par))->Data()),
		Nbins, fParValue->At(par) - domain, fParValue->At(par) + domain);
	hout->SetDirectory(0);

	// Point in parameter space:
	TArrayD PointInParSpace(fNFitParams);
	for (Int_t iPar = 0; iPar < fNFitParams; iPar++) {PointInParSpace.SetAt(fParValue->At(iPar), iPar);}

	// Setting the bins:
	for (Int_t iBin = 1; iBin < (Nbins + 1); iBin++) {
		PointInParSpace.SetAt(hout->GetBinCenter(iBin), par);
		hout->SetBinContent(iBin,(Double_t)ChiSquared(PointInParSpace.GetArray()));
	}

	return hout;

}

// -----------------------------------------------------------------------
TH2F* AliFitterBaseDiHadronPID::GetChiSquaredTwoPars(Int_t par1, Int_t par2, Double_t domain1, Double_t domain2, Int_t Nbins1, Int_t Nbins2) {

	// Investigate chi^2 as a function of two parameters.
	// User is responsible for deleting this object.	
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fNFitParams) {cout << Form("%s -> ERROR: fNFitParams = 0.",__func__) << endl; return 0x0;} 

	TH2F* hout = new TH2F(Form("ChiSquaredOnePar_%i_%i",par1,par2), 
		Form("#chi^{2} vs %s and %s;%s;%s;#chi^{2}",((TString*)fParName->At(par1))->Data(),((TString*)fParName->At(par2))->Data(),((TString*)fParName->At(par1))->Data(),((TString*)fParName->At(par2))->Data()),
		Nbins1, fParValue->At(par1) - domain1, fParValue->At(par1) + domain1, 
		Nbins2, fParValue->At(par2) - domain2, fParValue->At(par2) + domain2);
	hout->SetDirectory(0);

	// Point in parameter space:
	TArrayD PointInParSpace(fNFitParams);
	for (Int_t iPar = 0; iPar < 17; iPar++) {PointInParSpace.SetAt(fParValue->At(iPar), iPar);}

	// Setting the bins:
	TAxis* xaxis = hout->GetXaxis();
	TAxis* yaxis = hout->GetYaxis();

	for (Int_t iBin1 = 1; iBin1 < (Nbins1 + 1); iBin1++) {
		for (Int_t iBin2 = 1; iBin2 < (Nbins2 + 1); iBin2++) {
			
			PointInParSpace.SetAt(xaxis->GetBinCenter(iBin1), par1);
			PointInParSpace.SetAt(yaxis->GetBinCenter(iBin2), par2);

			hout->SetBinContent(iBin1,iBin2,(Double_t)ChiSquared(PointInParSpace.GetArray()));
		}
	}

	return hout;

}

// -----------------------------------------------------------------------
Double_t AliFitterBaseDiHadronPID::EstimateTOFPosition(Int_t species) const {

	// Returns the expected time difference between the mass assumption and the argument species.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Calculate mean pt.
	Double_t ptmean = fPtMin + (fPtMax - fPtMin)/2.;

	// Calculate expected times (taken at eta = 0.4). 
	Double_t centertime = AliFunctionsDiHadronPID::TOFExpTime(ptmean, AliFunctionsDiHadronPID::GetAbsAvgEta(fEtaMin, fEtaMax), AliFunctionsDiHadronPID::M(fMassAssumption));
	Double_t speciestime = AliFunctionsDiHadronPID::TOFExpTime(ptmean, AliFunctionsDiHadronPID::GetAbsAvgEta(fEtaMin, fEtaMax), AliFunctionsDiHadronPID::M(species));

	return speciestime - centertime;
}

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::EstimateTOFPositions() {

	// Sets the values of mu_TOF to their expected values, based on pt range and mass assumption.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (fMassAssumption == -1) {cout << Form("%s -> ERROR: First you have to set the mass assumption.",__func__) << endl; return;}
	if ((fPtMin < 0) || (fPtMax < 0)) {cout << Form("%s -> ERROR: First you have to set the p_T range.",__func__) << endl; return;}

	// Setting the mu parameters for each species.
	for (Int_t ii = 0; ii < 4; ii++) {
		SetMuTOF(ii, EstimateTOFPosition(ii));
	}

}

// -----------------------------------------------------------------------
Double_t AliFitterBaseDiHadronPID::EstimateTPCPosition(Int_t species) const {

	// Returns the expected time difference between the mass assumption and the argument species.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}	

	// Calculate mean pt.
	Double_t ptmean = fPtMin + (fPtMax - fPtMin)/2.;

	// Calculate expected dE/dx (taken at eta = 0.4).
	Double_t centerdEdX = AliFunctionsDiHadronPID::TPCExpdEdX(ptmean, AliFunctionsDiHadronPID::GetAbsAvgEta(fEtaMin, fEtaMax), AliFunctionsDiHadronPID::M(fMassAssumption));	
	Double_t speciesdEdX = AliFunctionsDiHadronPID::TPCExpdEdX(ptmean, AliFunctionsDiHadronPID::GetAbsAvgEta(fEtaMin, fEtaMax), AliFunctionsDiHadronPID::M(species));

	return speciesdEdX - centerdEdX;

}

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::EstimateTPCPositions() {

	// Sets the values of mu_TPC to their expected values, based on pt range and mass assumption.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}	
	if (fMassAssumption == -1) {cout << Form("%s -> ERROR: First you have to set the mass assumption.",__func__) << endl; return;}
	if ((fPtMin < 0) || (fPtMax < 0)) {cout << Form("%s -> ERROR: First you have to set the p_T range.",__func__) << endl; return;}

	// Setting the mu parameters for each species.
	for (Int_t ii = 0; ii < 4; ii++) {
		SetMuTPC(ii, EstimateTPCPosition(ii));
	}

}

// -----------------------------------------------------------------------
TCanvas* AliFitterBaseDiHadronPID::GetCloneOfCurrentCanvas() const {

	// Returns a clone of the canvas that fCanvas points to.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	
	if (fCanvas) {return (TCanvas*)fCanvas->Clone();}
	else {return 0x0;}

}

// -----------------------------------------------------------------------
ROOT::Math::Minimizer* AliFitterBaseDiHadronPID::CreateMinimizer() const {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Create a new minimizer.
	ROOT::Math::Minimizer* Minimizer = ROOT::Math::Factory::CreateMinimizer(fMinimizerType.Data(),fMinimizerMethod.Data());

	// Configure new minimizer.
	Minimizer->SetMaxFunctionCalls(fMaxFunctionCalls);
	Minimizer->SetMaxIterations(fMaxIterations);
	Minimizer->SetTolerance(fTolerance);

	//cout<<Form("Created a new minimzer of type %s, and method %s.",fMinimizerType.Data(),fMinimizerMethod.Data())<<endl;
	
	return Minimizer;

}

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::CreateCanvas(Bool_t withforce) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Delete canvas if present.
	if (fCanvas) {
		if (withforce) {
			cout<<"Deleting current canvas..."<<endl;
			delete fCanvas;
			fCanvas = 0x0;
		} else return;
	}

	fCanvas = TCanvas::MakeDefCanvas(); //new TCanvas("fCanvas","Fit Result",0,0,500,500);

}

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::PrintFitParams() const {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Check if fit parameters have been defined.
	if (!fNFitParams) {
		cout<<"No fit paramters have been defined."<<endl;
		return;
	}

	// Prints out the current status of the fit parameters.
	cout<<Form("%-20s %-10s %-10s %-10s %-10s","Par Name","Value","Error","StepSize","Fixed")<<endl;

	for (Int_t iPar = 0; iPar < fNFitParams; iPar++) {

		TString YesOrNo = "No";
		if (fIsParFixed->At(iPar)) {YesOrNo = "Yes";}

		cout<<Form("%-20s %10.4e %10.4e %10.4e %-10s",((TString*)fParName->At(iPar))->Data(),fParValue->At(iPar),fParError->At(iPar),fParStepSize->At(iPar),YesOrNo.Data())<<endl;

	}

}

// -----------------------------------------------------------------------
void AliFitterBaseDiHadronPID::Browse(TBrowser*) {

	// Called by the TBrowser.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	/* Nothing to browse */
	return;

} 
