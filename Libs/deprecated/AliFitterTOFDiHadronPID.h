#ifndef ALIFITTERTOFDIHADRONPID_H
#define ALIFITTERTOFDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

/* TODO: Should include fit strategy */
/* TODO: Should include beam type(?) */

#include "AliFitterBaseDiHadronPID.h"
#include "AliSpectrumDiHadronPID.h"

//class AliSpectrumDiHadronPID;

class AliFitterTOFDiHadronPID : public AliFitterBaseDiHadronPID {

public:
	AliFitterTOFDiHadronPID();
	virtual ~AliFitterTOFDiHadronPID();

private:	
	void InitializeParameterArrays();

public:
	enum EFitFunction {
		kNotIncluded = 0,
		kGauss = 1,
		kGaussWithTail = 2,
		kHistogram = 3,
		kSpline = 4
	};

	enum EFitStrategy {
		kPbPbDefault = 0
	};

	// Fitter interface (Overloaded methods).
	void PrintFit(TCanvas* cvs, Int_t pad);

	Int_t DoFitWithStrategyPbPb(Int_t strategy, TObjArray* fitstages, AliSpectrumDiHadronPID*, const char*, Int_t, const Bool_t);
	Int_t DoFitWithStrategypp(Int_t strategy, TObjArray* fitstages, AliSpectrumDiHadronPID*, const char*, Int_t, const Bool_t);

	void SetHistogram(const TH1* hist);
	void SetMismatchedBackground(const TH1* hist);
	void SetUsedFunction(Int_t species, Int_t function);		

	void EstimateNParticles();
	void EstimateNMismatch(Int_t option);

	// Getters for parameter indices (every parameter has an index in the Par values array, par error array, etc.)
	Int_t GetNBgdParN() const {return 0;}
	Int_t GetIntegralParN(Int_t species) const {return 1 + species * 4;}
	Int_t GetSigmaTOFParN(Int_t species) const {return 2 + species * 4;}
	Int_t GetMuTOFParN(Int_t species) const {return 3 + species * 4;}
	Int_t GetTailTOFParN(Int_t species) const {return 4 + species * 4;}
	Int_t GetSigmaTPCParN(Int_t) const {return -1;}
	Int_t GetMuTPCParN(Int_t) const {return -1;}
	Int_t GetTailTPCParN(Int_t) const {return -1;}

	// Getters for parameter values.
	Double_t GetNBgd() const {return fParValue->At(GetNBgdParN());}
	Double_t GetIntegral(Int_t species) const {return fParValue->At(GetIntegralParN(species));}
	Double_t GetSigmaTOF(Int_t species) const {return fParValue->At(GetSigmaTOFParN(species));}
	Double_t GetMuTOF(Int_t species) const {return fParValue->At(GetMuTOFParN(species));}
	Double_t GetTailTOF(Int_t species) const {return fParValue->At(GetTailTOFParN(species));}

	Double_t GetNBgdError() const {return fParError->At(GetNBgdParN());}
	Double_t GetIntegralError(Int_t species) const {return fParError->At(GetIntegralParN(species));}
	Double_t GetSigmaTOFError(Int_t species) const {return fParError->At(GetSigmaTOFParN(species));}
	Double_t GetMuTOFError(Int_t species) const {return fParError->At(GetMuTOFParN(species));}
	Double_t GetTailTOFError(Int_t species) const {return fParError->At(GetTailTOFParN(species));}

	Int_t GetNbinsInFit() const {return (fData->FindBin(fTOFmax) - fData->FindBin(fTOFmin) + 1);}

	Double_t GetSumNChargedInFitRegion() const;
	Double_t GetIntegralOverFitFunctionInFitRegion();

	//Double_t GetSumOverFittedData() const;
	//Double_t GetSumOverFit();

	TArrayI* GetAllParsForSystematics() const;
	TArrayI* GetUsedParsForSystematics(Int_t /*fitstrategy*/) const;

	void SetNBgd(Double_t value) {fParValue->SetAt(value, GetNBgdParN());}
	void SetIntegral(Int_t species, Double_t value) {fParValue->SetAt(value, GetIntegralParN(species));}
	void SetSigmaTOF(Int_t species, Double_t value) {fParValue->SetAt(value, GetSigmaTOFParN(species));}
	void SetMuTOF(Int_t species, Double_t value) {fParValue->SetAt(value, GetMuTOFParN(species));}
	void SetTailTOF(Int_t species, Double_t value) {fParValue->SetAt(value, GetTailTOFParN(species));}

	void SetFixNBgd(Bool_t fixed = kTRUE) {fIsParFixed->SetAt(fixed, GetNBgdParN());}
	void SetFixIntegral(Int_t species, Bool_t fixed = kTRUE) {fIsParFixed->SetAt(fixed, GetIntegralParN(species));}
	void SetFixSigmaTOF(Int_t species, Bool_t fixed = kTRUE) {fIsParFixed->SetAt(fixed, GetSigmaTOFParN(species));}	
	void SetFixMuTOF(Int_t species, Bool_t fixed = kTRUE) {fIsParFixed->SetAt(fixed, GetMuTOFParN(species));}
	void SetFixTailTOF(Int_t species, Bool_t fixed = kTRUE) {fIsParFixed->SetAt(fixed, GetTailTOFParN(species));}

	void SetTOFRange(Double_t min, Double_t max);

	void SetMergePiKaPeak();

private:
	// Private methods (overloaded).
	double ChiSquared(const double* xx) const;		
	void UpdateFitDescription();

	// Fit ranges (not overloaded).
	void SetRangePbPbDefault();
	void SetRangeppDefault();

	// Peaks - how are the different peaks represented in the fit (not overloaded).
	TArrayI* GetPeakDescriptionPbPbDefault();
	TArrayI* GetPeakDescriptionppDefault();


	// Fit strategies - steps towards the final result (not overloaded).
	Int_t DoFitWithStrategyPbPbDefault(TObjArray* fitstages);
	Int_t DoFitWithStrategyppDefault(TObjArray* fitstages);
	Int_t DoFitWithStrategyPbPbTweaked(TObjArray* fitstages);

	ClassDef(AliFitterTOFDiHadronPID, 1);

};

#endif
