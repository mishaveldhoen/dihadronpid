/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Holds several spectra objects, and creates ratios of those spectra.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "AliRatiosDiHadronPID.h"

#include <iostream>
using namespace std;

#include "TCanvas.h"
#include "TArray.h"
#include "TSystem.h"
#include "TNamed.h"
#include "TMath.h"
#include "TString.h"
#include "TLegend.h"
#include "TObjString.h"

ClassImp(AliRatiosDiHadronPID);

// -----------------------------------------------------------------------
AliRatiosDiHadronPID::AliRatiosDiHadronPID() :
	TNamed(),
	fSpectra(0x0),
	fCompanionSpectra(0x0),
	fNameForRegion(0x0),
	fNbinsForRegion(0x0),
	fNbinsForRegionCompanion(0x0),
	fNtriggersForSpecies(0x0),
	fXaxis(0x0),
	fDirName(0x0),
	fDebug(0),
	fHistLineWidth(2),
	fSystErrorbarWidth(0.5),
	fYAxisMargin(0.05),	
	fFirstHistogramFillColor(kRed-9),
	fSecondHistogramFillColor(kCyan+2),
	fThirdHistogramFillColor(kGreen-6),
	fFourthHistogramFillColor(kYellow-6),
	fSecondHistogramLineColor(kRed),
	fFirstHistogramMarkerStyle(20),
	fSecondHistogramMarkerStyle(21),
	fThirdHistogramMarkerStyle(22),
	fFourthHistogramMarkerStyle(23),
	fSystErrorDefinition(1),
	fVarySingleParameter(kFALSE),
	fUseRebinnedSpectra(kFALSE),
	fUseCorrectedSpectra(kFALSE),
	fSingleParameterName(0x0)

{

	// Default constructor.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	Initialize();

}

// -----------------------------------------------------------------------
AliRatiosDiHadronPID::AliRatiosDiHadronPID(const char* name, const char* title) :
	TNamed(name, title),
	fSpectra(0x0),
	fCompanionSpectra(0x0),
	fNameForRegion(0x0),
	fNbinsForRegion(0x0),
	fNbinsForRegionCompanion(0x0),	
	fNtriggersForSpecies(0x0),
	fXaxis(0x0),
	fDirName(0x0),	
	fDebug(0),
	fHistLineWidth(2),
	fSystErrorbarWidth(0.5),
	fYAxisMargin(0.05),		
	fFirstHistogramFillColor(kRed-9),
	fSecondHistogramFillColor(kCyan+2),
	fThirdHistogramFillColor(kGreen-6),
	fFourthHistogramFillColor(kYellow-6),		
	fSecondHistogramLineColor(kRed),
	fFirstHistogramMarkerStyle(20),
	fSecondHistogramMarkerStyle(21),
	fThirdHistogramMarkerStyle(22),
	fFourthHistogramMarkerStyle(23),	
	fSystErrorDefinition(1),
	fVarySingleParameter(kFALSE),
	fUseRebinnedSpectra(kFALSE),	
	fUseCorrectedSpectra(kFALSE),	
	fSingleParameterName(0x0)

{

	// Named constructor.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	Initialize();

}

// -----------------------------------------------------------------------
Bool_t AliRatiosDiHadronPID::Initialize() {

	// Creates the Array structure containing pointers to the imported spectra.
	fSpectra = new TObjArray(3);
	fSpectra->SetName("Spectra");
	fSpectra->SetOwner(kTRUE);

	fNameForRegion = new TObjArray(3);
	fNameForRegion->SetName("RegionNames");
	fNameForRegion->SetOwner(kTRUE);

	const char* regions[3] = {"All", "Peak", "Bulk"};
	for (Int_t iRegion = 0; iRegion < 3; ++iRegion) {
		TObjArray* arraytmp = new TObjArray(3);
		arraytmp->SetName(Form("Region%s", regions[iRegion]));
		arraytmp->SetOwner(kFALSE);
		fSpectra->AddAt(arraytmp, iRegion);
	}

	// Creates an array that keeps track of the number of bins in each region.
	fNbinsForRegion = new TArrayI(3);

	// Creates an array that keeps track of the number of triggers for each species.
	fNtriggersForSpecies = new TArrayD(3);

	// Default output directory name.
	fDirName = new TString("output");

	return 1;

}

// -----------------------------------------------------------------------
AliRatiosDiHadronPID::~AliRatiosDiHadronPID() {

	// Destructor constructor.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Class owns the fSpectra, but not its content.
	delete fSpectra; fSpectra = 0x0;
	delete fNameForRegion; fNameForRegion = 0x0;	
	delete fNbinsForRegion; fNbinsForRegion = 0x0;

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::SetOutputDir(const char* outputdir) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	if (fDirName) {delete fDirName;}

	fDirName = new TString(outputdir);

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::SetCompanion(AliRatiosDiHadronPID* companion) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	if (fCompanionSpectra) {
		cout << Form("%s -> WARNING: Throwing out the old companion.",__func__) << endl;
		delete fCompanionSpectra;
		delete fNbinsForRegionCompanion;
	}

	fCompanionSpectra = companion->GetDeepCopyOfSpectra();
	fNbinsForRegionCompanion = companion->GetCopyNbinsForRegion();

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::SetVarySingleParameter(Bool_t varysingleparameter, const char* parametername) {

	// With this function one can instruct the class to only consider a parameter
	// with a certain name in the calculation of the systematic error. The option
	// can also be turned off.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	fVarySingleParameter = varysingleparameter;

	if (fVarySingleParameter) {
		if (fSingleParameterName) {
			(*fSingleParameterName) = parametername;
		} else {
			fSingleParameterName = new TString(parametername);
		}
	} 
}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::InputSpectrum(const char* filename, const char* region, const char* name, Int_t species, Int_t Nbins) {

	// FIXME: Nbins can be used, even if there was an error reading the file.

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (Nbins < 1) {cout << Form("%s -> ERROR: Nbins cannot be smaller than one.",__func__) << endl; return;}

	Int_t regionint = RegionToInt(region);
	if (regionint == -1) {return;}	// Warning given by RegionToInt

	// Check wether the number of bins is the same as the currently known value. 
	if (fNbinsForRegion->GetAt(RegionToInt(region))) {
		// Complain if the number of bins is nonzero for a certain region, and we try to add a spectrum with a different
		// number of bins.
		if (fNbinsForRegion->GetAt(RegionToInt(region)) != Nbins) {
			cout << Form("%s -> ERROR: NBins is %i, while it was expected to be %i.",__func__, Nbins, (Int_t)fNbinsForRegion->GetAt(RegionToInt(region))) << endl; 
			return;
		} 
	} else {
		// If number of known bins == 0, then set it to Nbins.
		fNbinsForRegion->SetAt(Nbins, RegionToInt(region));		
	}

	TFile* fin = TFile::Open(filename);
	if (!fin) {cout << Form("%s -> ERROR: File %s cannot be found.",__func__, filename) << endl; return;}

	AliSpectrumDiHadronPID* spec = (AliSpectrumDiHadronPID*)fin->Get(Form("spectrumTOFTPC_%i",species));
	if (!fin) {cout << Form("%s -> ERROR: File %s does not seem to contain the right spectrum.",__func__, filename) << endl; return;}

	// Add the spectrum to the array structure.
	GetSpectrumArray(regionint)->AddAt(spec, species);

	// If the name hasn't been set, then set it.
	if (!fNameForRegion->At(regionint)) {
		TObjString* namestr = new TObjString(name);
		fNameForRegion->AddAt(namestr, regionint);
	}

	// Make a clone of the x axis if no x axis is known yet. Note that in case different spectra are imported with
	// different x axes, then problems arise.
	if (!fXaxis) {
		TH1F* hist = spec->GetCloneRawSpectrum(fUseRebinnedSpectra);
		fXaxis = (TAxis*)(hist->GetXaxis())->Clone();
	}

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::ExportSpectraToPDF(const char* region, Bool_t showsystematics, Bool_t makecomparison) {

	// systematic strategy = 0 -> Don't show any systematics.
	// systematic strategy = 1 -> Show systematics.

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!DoesOutputDirExist(kTRUE)) {cout << Form("%s -> ERROR: output directory could not be created.",__func__) << endl; return;}

	gSystem->cd(fDirName->Data());

	for (Int_t iSpecies = 0; iSpecies < 3; ++iSpecies) {

		TObjArray* arraytmp = GetMeasurementWithVariations(region, iSpecies);
		Scale(arraytmp, 1., kTRUE);											// Scale with binwidth.
		TGraphErrors* staterr = CreateStatErrorGraph(arraytmp);
		TGraphErrors* systerr = CreateSystErrorGraph(arraytmp);

		// REMOVE ME
		staterr->RemovePoint(15);
		systerr->RemovePoint(15);

		// Shift the spectrum the the left if we're making a comparison.
		if (makecomparison) {SetErrorAsGroupMember(staterr, systerr, 0, 2);}

		// Setting the y axis title.
		if (showsystematics) {
			(systerr->GetYaxis())->SetTitle(Form("#frac{1}{N_{trig}}#frac{dN_{%s}}{dp_{T}}", IntToSpeciesLaTeX(iSpecies)));
			(systerr->SetTitle(Form("%s Spectrum in %s", IntToSpeciesLaTeX(iSpecies), region)));
		} else {
			(staterr->GetYaxis())->SetTitle(Form("#frac{1}{N_{trig}}#frac{dN_{%s}}{dp_{T}}", IntToSpeciesLaTeX(iSpecies)));
			(staterr->SetTitle(Form("%s Spectrum in %s", IntToSpeciesLaTeX(iSpecies), region)));			
		}

		// Plotting.
		TCanvas* canvas = new TCanvas(Form("Spectrum_%s_%i",region,iSpecies), Form("Spectrum_%s_%i",region,iSpecies), 0, 0, 800, 800);
		if (showsystematics) {
			systerr->Draw("a2");	// Axis and filling.
			staterr->Draw("p");		// Just the points.

			// Make a plot of the relative errors.
			TCanvas* canvas2 = new TCanvas(Form("Spectrum_%s_%i_pct",region,iSpecies), Form("Spectrum_%s_%i_pct",region,iSpecies), 0, 0, 800, 800);
			GetPercentages(systerr)->Draw();
			canvas2->SaveAs(Form("Spectrum_%s_%i_pct.pdf",region,iSpecies));
			canvas->cd();	

		} else {staterr->Draw("ap");}

		// Plotting the comparison if requested.
		if (makecomparison) {

			TObjArray* arraycomp = GetMeasurementWithVariations(region, iSpecies);
			Scale(arraycomp, 1., kTRUE);											// Scale with binwidth.
			TGraphErrors* staterrcomp = CreateStatErrorGraph(arraycomp);
			TGraphErrors* systerrcomp = CreateSystErrorGraph(arraycomp);	
			systerrcomp->SetFillColor(fSecondHistogramFillColor);
			staterrcomp->SetMarkerStyle(fSecondHistogramMarkerStyle);

			// Shift to the right.
			SetErrorAsGroupMember(staterrcomp, systerrcomp, 1, 2);

			// Plot.
			if (showsystematics) {systerrcomp->Draw("2");}
			else {
				// In case no systematic errors are plotted, but a comparison is made.				
				staterrcomp->SetLineColor(fSecondHistogramLineColor);
				staterrcomp->SetMarkerColor(fSecondHistogramLineColor);
			}
			staterrcomp->Draw("p");

			// Make a plot the ratio of compared histograms.
			TCanvas* canvas3 = new TCanvas(Form("Spectrum_%s_%i_rat",region,iSpecies), Form("Spectrum_%s_%i_rat",region,iSpecies), 0, 0, 800, 800);
			GetRatio(staterr, staterrcomp)->Draw();
			canvas3->SaveAs(Form("Spectrum_%s_%i_rat.pdf",region,iSpecies));
			canvas->cd();		

			delete arraycomp;

		}

		OptimizeYRange(canvas);

		canvas->SaveAs(Form("Spectrum_%s_%i.pdf", region, iSpecies));

		delete arraytmp;

	}

	gSystem->cd("..");

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::ExportJetSpectraToPDF(Int_t syst_strat_subtr, Bool_t makecomparison) {

	// Exports (peak - bulk) to PDF. Systematic strategy is directly propagated to the subtration function.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!DoesOutputDirExist(kTRUE)) {cout << Form("%s -> ERROR: output directory could not be created.",__func__) << endl; return;}

	gSystem->cd(fDirName->Data());

	for (Int_t iSpecies = 0; iSpecies < 3; ++iSpecies) {

		TObjArray* arrayjet = GetJetMeasurementWithVariations(iSpecies, syst_strat_subtr);
		TGraphErrors* staterr = CreateStatErrorGraph(arrayjet);
		TGraphErrors* systerr = CreateSystErrorGraph(arrayjet);

		if (makecomparison) {SetErrorAsGroupMember(staterr, systerr, 0, 2);}

		// Setting the y axis title.
		if (syst_strat_subtr) {
			(systerr->GetYaxis())->SetTitle(Form("#frac{1}{N_{trig}}#frac{dN_{%s}}{dp_{T}}", IntToSpeciesLaTeX(iSpecies)));
			(systerr->SetTitle(Form("%s Spectrum in jet", IntToSpeciesLaTeX(iSpecies))));
		} else {
			(staterr->GetYaxis())->SetTitle(Form("#frac{1}{N_{trig}}#frac{dN_{%s}}{dp_{T}}", IntToSpeciesLaTeX(iSpecies)));
			(staterr->SetTitle(Form("%s Spectrum in jet", IntToSpeciesLaTeX(iSpecies))));			
		}

		// Plotting.
		TCanvas* canvas = new TCanvas(Form("Spectrum_Jet_%i", iSpecies), Form("Spectrum_Jet_%i", iSpecies), 0, 0, 800, 800);
		if (syst_strat_subtr) {
			systerr->Draw("a2");	// Axis and filling.
			staterr->Draw("p");		// Just the points.

			// Make a plot of the relative errors.
			TCanvas* canvas2 = new TCanvas(Form("Spectrum_Jet_%i_pct", iSpecies), Form("Spectrum_Jet_%i_pct", iSpecies), 0, 0, 800, 800);
			GetPercentages(systerr)->Draw();
			canvas2->SaveAs(Form("Spectrum_Jet_%i_pct.pdf", iSpecies));	
			canvas->cd();	

		} else {staterr->Draw("ap");}

		// Plotting the comparison if requested.
		if (makecomparison) {

			TObjArray* arrayjetcomp = GetJetMeasurementWithVariations(iSpecies, syst_strat_subtr, kTRUE);
			TGraphErrors* staterrcomp = CreateStatErrorGraph(arrayjetcomp);
			TGraphErrors* systerrcomp = CreateSystErrorGraph(arrayjetcomp);	
			systerrcomp->SetFillColor(fSecondHistogramFillColor);
			staterrcomp->SetMarkerStyle(fSecondHistogramMarkerStyle);

			// Shift to the right.
			SetErrorAsGroupMember(staterrcomp, systerrcomp, 1, 2);

			// Plot.
			if (syst_strat_subtr) {systerrcomp->Draw("2");}
			else {
				// In case no systematic errors are plotted, but a comparison is made.
				staterrcomp->SetLineColor(fSecondHistogramLineColor);
				staterrcomp->SetMarkerColor(fSecondHistogramLineColor);
			}			
			canvas->cd();
			staterrcomp->Draw("p");	

			// Make a plot the ratio of compared histograms.
			TCanvas* canvas3 = new TCanvas(Form("Spectrum_Jet_%i_rat",iSpecies), Form("Spectrum_Jet_%i",iSpecies), 0, 0, 800, 800);
			GetRatio(staterr, staterrcomp)->Draw();
			canvas3->SaveAs(Form("Spectrum_Jet_%i_rat.pdf",iSpecies));
			canvas->cd();	

			delete arrayjetcomp;

		}

		OptimizeYRange(canvas);

		canvas->SaveAs(Form("Spectrum_Jet_%i.pdf", iSpecies));

		delete arrayjet;

	}

	gSystem->cd("..");

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::ExportRatioToPDF(const char* region, Int_t syst_strat_div, Bool_t makecomparison) {

	// Ratio within a certain region. Systematic strategy is directly propagated to the subtration function.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!DoesOutputDirExist(kTRUE)) {cout << Form("%s -> ERROR: output directory could not be created.",__func__) << endl; return;}

	gSystem->cd(fDirName->Data());

	// Load the spectra for different species.
	TObjArray* arraytmp[3];
	TObjArray* arraytmpcomp[3];
	for (Int_t iSpecies = 0; iSpecies < 3; ++iSpecies) {
		arraytmp[iSpecies] = GetMeasurementWithVariations(region, iSpecies);
		if (makecomparison) {arraytmpcomp[iSpecies] = GetMeasurementWithVariations(region, iSpecies, kTRUE);}
		else {arraytmpcomp[iSpecies] = 0x0;}
	}

	// Create the ratios.
	for (Int_t iDenominator = 0; iDenominator < 2; ++iDenominator) {
		for (Int_t iNumerator = (iDenominator + 1); iNumerator < 3; ++iNumerator) {

			TObjArray* ratios = Divide(arraytmp[iNumerator], arraytmp[iDenominator], 1);

			TGraphErrors* staterr = CreateStatErrorGraph(ratios);
			TGraphErrors* systerr = CreateSystErrorGraph(ratios);

			if (makecomparison) {SetErrorAsGroupMember(staterr, systerr, 0, 2);}

			// Setting the y axis title.
			if (syst_strat_div) {
				(systerr->GetYaxis())->SetTitle(Form("%s/%s", IntToSpeciesLaTeX(iNumerator), IntToSpeciesLaTeX(iDenominator)));
				(systerr->SetTitle(Form("%s/%s ratio in %s", IntToSpeciesLaTeX(iNumerator), IntToSpeciesLaTeX(iDenominator), region)));
			} else {
				(staterr->GetYaxis())->SetTitle(Form("%s/%s", IntToSpeciesLaTeX(iNumerator), IntToSpeciesLaTeX(iDenominator)));
				(staterr->SetTitle(Form("%s/%s ratio in %s", IntToSpeciesLaTeX(iNumerator), IntToSpeciesLaTeX(iDenominator), region)));
			}

			TCanvas* canvas = new TCanvas(Form("Ratio_%s_%iover%i",region,iNumerator,iDenominator), Form("Ratio_%s_%iover%i",region,iNumerator,iDenominator), 0, 0, 800, 800);
			if (syst_strat_div) {
				systerr->Draw("a2");	// Axis and filling.
				staterr->Draw("p");		// Just the points.

				// Make a plot of the relative errors.
				TCanvas* canvas2 = new TCanvas(Form("Ratio_%s_%iover%i_pct",region,iNumerator,iDenominator), Form("Ratio_%s_%iover%i_pct",region,iNumerator,iDenominator), 0, 0, 800, 800);
				GetPercentages(systerr)->Draw();
				canvas2->SaveAs(Form("Ratio_%s_%iover%i_pct.pdf",region,iNumerator,iDenominator));	
				canvas->cd();	
			
			} else {staterr->Draw("ap");}

			if (makecomparison) {

				TObjArray* ratioscomp = Divide(arraytmpcomp[iNumerator], arraytmpcomp[iDenominator], 1);

				TGraphErrors* staterrcomp = CreateStatErrorGraph(ratioscomp);
				TGraphErrors* systerrcomp = CreateSystErrorGraph(ratioscomp);
				systerrcomp->SetFillColor(fSecondHistogramFillColor);
				staterrcomp->SetMarkerStyle(fSecondHistogramMarkerStyle);

				SetErrorAsGroupMember(staterrcomp, systerrcomp, 1, 2);

				if (syst_strat_div) {systerrcomp->Draw("2");}
				else {
					// In case no systematic errors are plotted, but a comparison is made.					
					staterrcomp->SetLineColor(fSecondHistogramLineColor);
					staterrcomp->SetMarkerColor(fSecondHistogramLineColor);
				}
				staterrcomp->Draw("p");

				// Make a plot the ratio of compared histograms.
				TCanvas* canvas3 = new TCanvas(Form("Ratio_%s_%iover%i_rat",region,iNumerator,iDenominator), Form("Ratio_%s_%iover%i_rat",region,iNumerator,iDenominator), 0, 0, 800, 800);
				GetRatio(staterr, staterrcomp)->Draw();
				canvas3->SaveAs(Form("Ratio_%s_%iover%i_rat.pdf",region,iNumerator,iDenominator));
				canvas->cd();	

				delete ratioscomp;

			}

			OptimizeYRange(canvas);

			canvas->SaveAs(Form("Ratio_%s_%iover%i.pdf",region,iNumerator,iDenominator));

			delete ratios;

		}
	}

	for (Int_t iSpecies = 0; iSpecies < 3; ++iSpecies) {delete arraytmp[iSpecies];}

	gSystem->cd("..");
}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::ExportJetRatioToPDF(Int_t syst_strat_subtr, Int_t syst_strat_div, Bool_t makecomparison) {

	// Different systematic strategy for subtraction and division.

	// Exports particle ratios in the jet.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!DoesOutputDirExist(kTRUE)) {cout << Form("%s -> ERROR: output directory could not be created.",__func__) << endl; return;}

	gSystem->cd(fDirName->Data());

	// Load the spectra for different species.
	TObjArray* arraytmp[3];
	TObjArray* arraytmpcomp[3];
	for (Int_t iSpecies = 0; iSpecies < 3; ++iSpecies) {
		arraytmp[iSpecies] = GetJetMeasurementWithVariations(iSpecies, syst_strat_subtr);
		if (makecomparison) {arraytmpcomp[iSpecies] = GetJetMeasurementWithVariations(iSpecies, syst_strat_subtr, kTRUE);}
		else {arraytmpcomp[iSpecies] = 0x0;}
	}

	// Create the ratios.
	for (Int_t iDenominator = 0; iDenominator < 2; ++iDenominator) {
		for (Int_t iNumerator = (iDenominator + 1); iNumerator < 3; ++iNumerator) {

			TObjArray* ratios = Divide(arraytmp[iNumerator], arraytmp[iDenominator], syst_strat_div);
			
			TGraphErrors* staterr = CreateStatErrorGraph(ratios);
			TGraphErrors* systerr = CreateSystErrorGraph(ratios);

			if (makecomparison) {SetErrorAsGroupMember(staterr, systerr, 0, 2);}					

			// Setting the y axis title.
			if (syst_strat_subtr) {
				(systerr->GetYaxis())->SetTitle(Form("%s/%s", IntToSpeciesLaTeX(iNumerator), IntToSpeciesLaTeX(iDenominator)));
				(systerr->SetTitle(Form("%s/%s ratio in jet", IntToSpeciesLaTeX(iNumerator), IntToSpeciesLaTeX(iDenominator))));
			} else {
				(staterr->GetYaxis())->SetTitle(Form("%s/%s", IntToSpeciesLaTeX(iNumerator), IntToSpeciesLaTeX(iDenominator)));
				(staterr->SetTitle(Form("%s/%s ratio in jet", IntToSpeciesLaTeX(iNumerator), IntToSpeciesLaTeX(iDenominator))));
			}

			TCanvas* canvas = new TCanvas(Form("JetRatio_%iover%i",iNumerator,iDenominator), Form("JetRatio_%iover%i",iNumerator,iDenominator), 0, 0, 800, 800);
			if (syst_strat_subtr) {
				systerr->Draw("a2");	// Axis and filling.
				staterr->Draw("p");		// Just the points.

				// Make a plot of the relative errors.
				TCanvas* canvas2 = new TCanvas(Form("JetRatio_%iover%i_pct",iNumerator,iDenominator), Form("JetRatio_%iover%i_pct",iNumerator,iDenominator), 0, 0, 800, 800);
				GetPercentages(systerr)->Draw();
				canvas2->SaveAs(Form("JetRatio_%iover%i_pct.pdf",iNumerator,iDenominator));	
				canvas->cd();	

			} else {staterr->Draw("ap");}

			if (makecomparison) {

				TObjArray* ratioscomp = Divide(arraytmpcomp[iNumerator], arraytmpcomp[iDenominator], syst_strat_div);

				TGraphErrors* staterrcomp = CreateStatErrorGraph(ratioscomp);
				TGraphErrors* systerrcomp = CreateSystErrorGraph(ratioscomp);
				systerrcomp->SetFillColor(fSecondHistogramFillColor);
				staterrcomp->SetMarkerStyle(fSecondHistogramMarkerStyle);

				SetErrorAsGroupMember(staterrcomp, systerrcomp, 1, 2);

				if (syst_strat_subtr) {systerrcomp->Draw("2");}
				else {
					// In case no systematic errors are plotted, but a comparison is made.				
					staterrcomp->SetLineColor(fSecondHistogramLineColor);
					staterrcomp->SetMarkerColor(fSecondHistogramLineColor);
				}
				
				staterrcomp->Draw("p");

				// Make a plot the ratio of compared histograms.
				TCanvas* canvas3 = new TCanvas(Form("SJetRatio_%iover%i_rat",iNumerator,iDenominator), Form("JetRatio_%iover%i_rat",iNumerator,iDenominator), 0, 0, 800, 800);
				GetRatio(staterr, staterrcomp)->Draw();
				canvas3->SaveAs(Form("JetRatio_%iover%i_rat.pdf",iNumerator,iDenominator));
				canvas->cd();	

				delete ratioscomp;

			}

			OptimizeYRange(canvas);

			canvas->SaveAs(Form("JetRatio_%iover%i.pdf",iNumerator,iDenominator));

			delete ratios;

		}
	}

	for (Int_t iSpecies = 0; iSpecies < 3; ++iSpecies) {delete arraytmp[iSpecies];}

	gSystem->cd("..");

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::ExportFinalPicturesToPDF(Int_t syst_strat_subtr, Int_t syst_strat_div, Bool_t makecomparison) {

	// Exports bulk ratio and jet ratio in one picture.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!DoesOutputDirExist(kTRUE)) {cout << Form("%s -> ERROR: output directory could not be created.",__func__) << endl; return;}

	gSystem->cd(fDirName->Data());
	
	// Load the spectra for different species.
	TObjArray* arrayjettmp[3];
	TObjArray* arraybulktmp[3];	

	TObjArray* arrayjettmpcomp[3];
	TObjArray* arraybulktmpcomp[3];	

	for (Int_t iSpecies = 0; iSpecies < 3; ++iSpecies) {
		arrayjettmp[iSpecies] = GetJetMeasurementWithVariations(iSpecies, syst_strat_subtr);
		arraybulktmp[iSpecies] = GetMeasurementWithVariations("bulk", iSpecies);
		
		if (makecomparison) {
			arrayjettmpcomp[iSpecies] = GetJetMeasurementWithVariations(iSpecies, syst_strat_subtr, kTRUE);
			arraybulktmpcomp[iSpecies] = GetMeasurementWithVariations("bulk", iSpecies, kTRUE);
		}
	}

	// Create the ratios.
	for (Int_t iDenominator = 0; iDenominator < 2; ++iDenominator) {
		for (Int_t iNumerator = (iDenominator + 1); iNumerator < 3; ++iNumerator) {

			// Ratio in the jet.
			TObjArray* ratiosjet = Divide(arrayjettmp[iNumerator], arrayjettmp[iDenominator], syst_strat_div);
			TGraphErrors* staterrjet = CreateStatErrorGraph(ratiosjet);
			TGraphErrors* systerrjet = CreateSystErrorGraph(ratiosjet);			
			SetErrorAsGroupMember(staterrjet, systerrjet, 0, 4);

			// Ratio in the bulk.
			TObjArray* ratiosbulk = Divide(arraybulktmp[iNumerator], arraybulktmp[iDenominator], syst_strat_div);
			TGraphErrors* staterrbulk = CreateStatErrorGraph(ratiosbulk);
			TGraphErrors* systerrbulk = CreateSystErrorGraph(ratiosbulk);
			systerrbulk->SetFillColor(fSecondHistogramFillColor);
			systerrbulk->SetMarkerStyle(fSecondHistogramMarkerStyle);
			staterrbulk->SetMarkerStyle(fSecondHistogramMarkerStyle);
			SetErrorAsGroupMember(staterrbulk, systerrbulk, 1, 4);

			// Setting the y axis title.
			(systerrjet->GetYaxis())->SetTitle(Form("%s/%s", IntToSpeciesLaTeX(iNumerator), IntToSpeciesLaTeX(iDenominator)));
			(systerrjet->SetTitle(Form("%s/%s ratio in jet and bulk", IntToSpeciesLaTeX(iNumerator), IntToSpeciesLaTeX(iDenominator))));
			systerrjet->SetMinimum(0.);
			systerrjet->SetMaximum(1.);
			if (iDenominator == 1) {systerrjet->SetMaximum(1.5);}

			TCanvas* canvas = new TCanvas(Form("Ratio_%iover%i",iNumerator,iDenominator), Form("Ratio_%iover%i",iNumerator,iDenominator), 0, 0, 800, 800);
			systerrjet->Draw("a2");	    // Axis and filling.
			staterrjet->Draw("p");		// Just the points.
			systerrbulk->Draw("2");
			staterrbulk->Draw("p");

			if (makecomparison) {

				// Ratio in the jet comparison.
				TObjArray* ratiosjetcomparison = Divide(arrayjettmpcomp[iNumerator], arrayjettmpcomp[iDenominator], syst_strat_div);
				TGraphErrors* staterrjetcomparison = CreateStatErrorGraph(ratiosjetcomparison);
				TGraphErrors* systerrjetcomparison = CreateSystErrorGraph(ratiosjetcomparison);			
				SetErrorAsGroupMember(staterrjetcomparison, systerrjetcomparison, 2, 4);

				// Ratio in the bulk comparison.
				TObjArray* ratiosbulkcomparison = Divide(arraybulktmpcomp[iNumerator], arraybulktmpcomp[iDenominator], syst_strat_div);
				TGraphErrors* staterrbulkcomparison = CreateStatErrorGraph(ratiosbulkcomparison);
				TGraphErrors* systerrbulkcomparison = CreateSystErrorGraph(ratiosbulkcomparison);
				SetErrorAsGroupMember(staterrbulkcomparison, systerrbulkcomparison, 3, 4);

				staterrjetcomparison->SetMarkerStyle(fThirdHistogramMarkerStyle);
				staterrbulkcomparison->SetMarkerStyle(fFourthHistogramMarkerStyle);

				systerrjetcomparison->SetFillColor(fThirdHistogramFillColor);
				systerrbulkcomparison->SetFillColor(fFourthHistogramFillColor);

				systerrjetcomparison->Draw("2");	    // Axis and filling.
				staterrjetcomparison->Draw("p");		// Just the points.
				systerrbulkcomparison->Draw("2");
				staterrbulkcomparison->Draw("p");

				delete ratiosjetcomparison;
				delete ratiosbulkcomparison;

			}

			OptimizeYRange(canvas);

			TLegend* legend = new TLegend(0.8,0.8,0.95,0.95);
			legend->AddEntry(systerrjet,"Jet");
			legend->AddEntry(systerrbulk,"Bulk");
			legend->Draw();

			canvas->SaveAs(Form("Ratio_%iover%i.pdf",iNumerator,iDenominator));

			delete ratiosjet;
			delete ratiosbulk;

		}
	}

	for (Int_t iSpecies = 0; iSpecies < 3; ++iSpecies) {
		delete arrayjettmp[iSpecies];
		delete arraybulktmp[iSpecies];
	}

	gSystem->cd("..");

}

// -----------------------------------------------------------------------
Int_t AliRatiosDiHadronPID::RegionToInt(const char* region) const {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	TString regionstr(region);
	regionstr.ToLower();

	if (regionstr.EqualTo("all")) {return 0;}
	if (regionstr.EqualTo("peak")) {return 1;}
	if (regionstr.EqualTo("bulk")) {return 2;}

	cout << Form("%s -> ERROR: %s is an unknown region.",__func__, region) << endl; 
	return -1;

}

// -----------------------------------------------------------------------
const char* AliRatiosDiHadronPID::IntToRegion(Int_t region) const {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (region < 0 || region > 2) {cout << Form("%s -> ERROR: %i is an unknown region.",__func__, region) << endl; return 0x0;}

	const char* regions[3] = {"all", "peak", "bulk"};

	return regions[region];

}

// -----------------------------------------------------------------------
const char* AliRatiosDiHadronPID::IntToSpecies(Int_t species) const {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (species < 0 || species > 2) {cout << Form("%s -> ERROR: %i is an unknown species.",__func__, species) << endl; return 0x0;}

	const char* speciess[3] = {"pion", "kaon", "proton"};

	return speciess[species];

}

// -----------------------------------------------------------------------
const char* AliRatiosDiHadronPID::IntToSpeciesLaTeX(Int_t species) const {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (species < 0 || species > 2) {cout << Form("%s -> ERROR: %i is an unknown species.",__func__, species) << endl; return 0x0;}

	const char* speciess[3] = {"#pi", "K", "p"};

	return speciess[species];
}

// -----------------------------------------------------------------------
Bool_t AliRatiosDiHadronPID::HistogramIsTrivial(const TH1F* hist) const {

	// Checks wheather a histogram is trivial (i.e., only has "zero" entries)
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	
	for (Int_t iBin = 1; iBin <= hist->GetNbinsX(); ++iBin) {
		if (hist->GetBinContent(iBin)) {return kFALSE;}
	}

	return kTRUE;

} 

// -----------------------------------------------------------------------
TObjArray* AliRatiosDiHadronPID::GetSpectrumArray(Int_t region, Bool_t fromcompanion) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;
	cout<<" arguments: "<<region<<" "<<fromcompanion<<endl;}
	if (fromcompanion) {return ((TObjArray*)fCompanionSpectra->At(region));}
	else {return ((TObjArray*)fSpectra->At(region));}

}

// -----------------------------------------------------------------------
TObjArray* AliRatiosDiHadronPID::GetMeasurementWithVariations(Int_t region, Int_t species, Bool_t fromcompanion) {

	// Returns a TObjArray with the "measurement" as the first entry, and the variations
	// as subsequent entries. The variations obtained from the AliSpectrumDiHadronPID object
	// often contains histograms with only zero bins (variation has not been performed). These
	// histogram will be left out.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Getting the measurement and array with the variations.
	TObjArray* arraytmp = GetSpectrumArray(region, fromcompanion);
	AliSpectrumDiHadronPID* spectrum = (AliSpectrumDiHadronPID*)(arraytmp->At(species));
	TH1F* measurement = 0x0;
	if (fUseCorrectedSpectra) {measurement = spectrum->GetCloneCorrSpectrum(fUseRebinnedSpectra);}
	else {measurement = spectrum->GetCloneRawSpectrum(fUseRebinnedSpectra);}
	TObjArray* variations = 0x0;
	if (fUseCorrectedSpectra) {variations = spectrum->GetCloneVariedCorrSpectra(fUseRebinnedSpectra);}
	else {variations = spectrum->GetCloneVariedRawSpectra(fUseRebinnedSpectra);}
	variations->SetOwner(kFALSE);

	// Creating the output array.
	TObjArray* aout = new TObjArray(variations->GetSize() + 1);
	aout->SetOwner(kTRUE);
	
	// Add the measurement on the 0th entry of the array.
	aout->AddFirst(measurement);

	// Loop over all all variations. Delete them if they are trivial, and add them
	// to the output if they're not.
	for (Int_t iVariation = 0; iVariation < variations->GetSize(); ++iVariation) {
		TH1F* hist = (TH1F*)variations->At(iVariation);
		if (HistogramIsTrivial(hist)) {delete hist; continue;}
		if (fVarySingleParameter) {
			//cout<<hist->GetName()<< " " << fSingleParameterName->Data() <<" "<< !TString(hist->GetName()).Contains(*fSingleParameterName)<<endl;
			if (!TString(hist->GetName()).Contains(*fSingleParameterName)) {delete hist; continue;}
		}
		aout->AddLast(hist);
	}

	// Variations doesn't own any of it's content anymore.
	delete variations;

	aout->Expand(aout->GetEntries());

	// Scale the histograms with the number of triggers.
	if (fNtriggersForSpecies->GetAt(species)) {
		Scale(aout, 1./fNtriggersForSpecies->GetAt(species));
	} else {
		delete aout;
		cout << Form("%s -> ERROR: Amount of triggers unknown.",__func__) << endl; 
		return 0x0;
	}

	return aout;

}

// -----------------------------------------------------------------------
TObjArray* AliRatiosDiHadronPID::GetJetMeasurementWithVariations(Int_t species, Int_t syst_strat_subtr, Bool_t fromcompanion) {

	// Returns jet spectra with variation. 
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	TObjArray* arraypeak = GetMeasurementWithVariations("peak", species, fromcompanion);
	TObjArray* arraybulk = GetMeasurementWithVariations("bulk", species, fromcompanion);

	//arraypeak->ls();
	//arraybulk->ls();

	// Scale arrays appropriately.
	Scale(arraypeak, 1., kTRUE);
	Double_t scalefactor = 0.;
	if (fromcompanion) {
		scalefactor = fNbinsForRegionCompanion->GetAt(RegionToInt("peak"));
		scalefactor /= fNbinsForRegionCompanion->GetAt(RegionToInt("bulk"));
	} else {
		scalefactor = fNbinsForRegion->GetAt(RegionToInt("peak"));
		scalefactor /= fNbinsForRegion->GetAt(RegionToInt("bulk"));		
	}
	//cout << "Companion: " << fromcompanion << " scalefactor: " << scalefactor << endl;
	Scale(arraybulk, scalefactor, kTRUE);

	TObjArray* arrayjet = Subtract(arraypeak, arraybulk, syst_strat_subtr);

	delete arraypeak;
	delete arraybulk;

	return arrayjet;

}

// -----------------------------------------------------------------------
Bool_t AliRatiosDiHadronPID::ArraysAreCompatible(const TObjArray* array1, const TObjArray* array2) const {

	// Checks if the arrays contain elements with the same name. 
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (array1->GetSize() != array2->GetSize()) {return kFALSE;}

	// The first entry of the array is allowed to have a different name, since it can depend on
	// the species.
	for (Int_t iEntry = 1; iEntry < array1->GetSize(); ++iEntry) {
		TString array1name((array1->At(iEntry))->GetName());
		TString array2name((array2->At(iEntry))->GetName());

		if (!array1name.EqualTo(array2name)) {return kFALSE;}

	}

	return kTRUE;

}

// -----------------------------------------------------------------------
Double_t AliRatiosDiHadronPID::GetSTD(const TArrayD* array, const Double_t* measurement) const {

	// Returns the STD of an array. If "measurement" is provided, then this is used
	// instead of mean.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	Int_t size = array->GetSize();
	Int_t realsize = 0;		// Size not counting the 0's. 
	
	Double_t mean = GetMean(array);
	if (measurement) {mean = measurement[0];}
	
	Double_t std = 0.;
	for (Int_t iEntry = 0; iEntry < size; ++iEntry) {
		if (!array->GetAt(iEntry)) {continue;}		
		std += (array->GetAt(iEntry) - mean) * (array->GetAt(iEntry) - mean);
		realsize++;
	}

	std /= realsize;

	return TMath::Sqrt(std);

}

// -----------------------------------------------------------------------
Double_t AliRatiosDiHadronPID::GetMean(const TArrayD* array) const {

	// Returns the mean of an array.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}	

	Int_t size = array->GetSize();
	Int_t realsize = 0;		// Size not counting the 0's. 	
	Double_t mean = 0.;
	for (Int_t iEntry = 0; iEntry < size; ++iEntry) {
		if (!array->GetAt(iEntry)) {continue;}		
		mean += array->GetAt(iEntry);
		realsize++;		
	}

	mean /= realsize;
	return mean;

}

// -----------------------------------------------------------------------
Double_t AliRatiosDiHadronPID::GetMaxDeviation(const TArrayD* array, const Double_t* measurement) const {

	// Returns the absolute value of the maximum deviation from the mean.
	// If "measurement" is provided, then this is used instead of the mean.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	Int_t size = array->GetSize();
	Double_t mean = GetMean(array);
	if (measurement) {mean = measurement[0];}

	Double_t maxdev = 0.;
	for (Int_t iEntry = 0; iEntry < size; ++iEntry) {
		if (!array->GetAt(iEntry)) {continue;}
		Double_t currentdev = TMath::Abs(mean - array->GetAt(iEntry));
		if (currentdev > maxdev) {maxdev = currentdev;}	
	}

	return maxdev;

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::Scale(TObjArray* measurementwithvariations, Double_t factor, Bool_t width) {

	// Scales all histograms in the array with the bin width.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	for (Int_t iEntry = 0; iEntry < measurementwithvariations->GetSize(); ++iEntry) {
		TH1F* hist = (TH1F*)measurementwithvariations->At(iEntry);
		hist->Scale(factor, (width ? "width" : ""));
	}

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::CheckOutArrayOfHistograms(const TObjArray* array) const {

	// Gives info about the histograms in the array and bin content.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	for (Int_t iEntry = 0; iEntry < array->GetSize(); ++iEntry) {

		TObject* obj = array->At(iEntry);
		if (obj) {
			cout << "Entry: " << iEntry;
			if (obj->InheritsFrom("TH1F")) {
				TH1F* hist = (TH1F*)obj;
				cout << " Name: " << hist->GetName() << endl;
				for (Int_t iBin = 1; iBin <= hist->GetNbinsX(); ++iBin) {
					cout << "   Bin: " << iBin << " Content: " << hist->GetBinContent(iBin) << endl;
				}
			} else {
				cout << " is not a histogram." << endl;
			}
		}
	}
}

// -----------------------------------------------------------------------
TObjArray* AliRatiosDiHadronPID::Subtract(const TObjArray* measurementwithvariations1, const TObjArray* measurementwithvariations2, Int_t syst_strat_subtr) {

	// Returns a new array with:
	//  - element 0 is the "measurement", i.e., measurementwithvariations1[0] - measurementwithvariatons2[0].
	//  - if usevariations = 0 then: variations are ignored and only the measurement remains.
	//  - if usevariations = 1 then: variations are treated as completely correlated (i.e., only variations with the same name are mixed).
	//  - if usevariations = 2 then: variations are treated as partially correlated (i.e., variations with the same name up to _up, _down) are also mixed.
	//  - if usevariations = 3 then: variations are treated as uncorrelated (i.e., all variations are mixed).
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	//if (!ArraysAreCompatible(measurementwithvariations1, measurementwithvariations2)) {cout << Form("%s -> ERROR: Arrays not compatible",__func__) << endl; return 0x0;}

	// Create an output array with the right size.
	TObjArray* aout = new TObjArray(10);
	aout->SetOwner(kTRUE);

	// First element is the subtraction of the measurements.
	aout->AddFirst(Subtract((TH1F*)measurementwithvariations1->At(0), (TH1F*)measurementwithvariations2->At(0)));

	// Make subtractions of the variations.
	if (syst_strat_subtr) {

		for (Int_t iVariation1 = 1; iVariation1 < measurementwithvariations1->GetSize(); ++iVariation1) {

			if (syst_strat_subtr == 1) {

				// For this procedure the arrays need to be compatible (i.e., contain exactly the same variations)
				if (!ArraysAreCompatible(measurementwithvariations1, measurementwithvariations2)) {cout << Form("%s -> ERROR: Arrays not compatible",__func__) << endl; return 0x0;}
				aout->AddLast(Subtract((TH1F*)measurementwithvariations1->At(iVariation1), (TH1F*)measurementwithvariations2->At(iVariation1)));

			} else if (syst_strat_subtr == 2) {

				// For this procedure the arrays need to be compatible (i.e., contain exactly the same variations)
				if (!ArraysAreCompatible(measurementwithvariations1, measurementwithvariations2)) {cout << Form("%s -> ERROR: Arrays not compatible",__func__) << endl; return 0x0;}
				/* TODO */

			} else {

				for (Int_t iVariation2 = 1; iVariation2 < measurementwithvariations2->GetSize(); ++iVariation2) {
					
					// For this procedure it's ok if both arrays are different.
					aout->AddLast(Subtract((TH1F*)measurementwithvariations1->At(iVariation1), (TH1F*)measurementwithvariations2->At(iVariation2)));
				
				}
			
			}

		}

	}

	aout->Expand(aout->GetEntriesFast());

	return aout;

}

// -----------------------------------------------------------------------
TH1F* AliRatiosDiHadronPID::Subtract(const TH1F* hist1, const TH1F* hist2) const {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	const char* newname = Form("%s_MINUS_%s",hist1->GetName(), hist2->GetName());
	TH1F* subtraction = (TH1F*)(hist1->Clone(newname));
	subtraction->Add(hist2, -1.);

	return subtraction;

}

// -----------------------------------------------------------------------
TObjArray* AliRatiosDiHadronPID::Divide(const TObjArray* measurementwithvariations1, const TObjArray* measurementwithvariations2, Int_t syst_strat_div) {

	// Similar to Subtract (could have made a function Operation(array1, array2).
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	//if (!ArraysAreCompatible(measurementwithvariations1, measurementwithvariations2)) {cout << Form("%s -> ERROR: Arrays not compatible",__func__) << endl; return 0x0;}

	// Create an output array with the right size.
	TObjArray* aout = new TObjArray(10);
	aout->SetOwner(kTRUE);

	// First element is the subtraction of the measurements.
	aout->AddFirst(Divide((TH1F*)measurementwithvariations1->At(0), (TH1F*)measurementwithvariations2->At(0)));

	// Make subtractions of the variations.
	if (syst_strat_div) {

		for (Int_t iVariation1 = 1; iVariation1 < measurementwithvariations1->GetSize(); ++iVariation1) {

			if (syst_strat_div == 1) {

				// For this procedure the arrays need to be compatible (i.e., contain exactly the same variations)
				if (!ArraysAreCompatible(measurementwithvariations1, measurementwithvariations2)) {cout << Form("%s -> ERROR: Arrays not compatible",__func__) << endl; return 0x0;}
				aout->AddLast(Divide((TH1F*)measurementwithvariations1->At(iVariation1), (TH1F*)measurementwithvariations2->At(iVariation1)));

			} else if (syst_strat_div == 2) {

				// For this procedure the arrays need to be compatible (i.e., contain exactly the same variations)
				if (!ArraysAreCompatible(measurementwithvariations1, measurementwithvariations2)) {cout << Form("%s -> ERROR: Arrays not compatible",__func__) << endl; return 0x0;}
				/* TODO */

			} else {

				for (Int_t iVariation2 = 1; iVariation2 < measurementwithvariations2->GetSize(); ++iVariation2) {
					
					// For this procedure it's ok if both arrays are different.
					aout->AddLast(Divide((TH1F*)measurementwithvariations1->At(iVariation1), (TH1F*)measurementwithvariations2->At(iVariation2)));
				
				}
			
			}

		}

	}

	aout->Expand(aout->GetEntriesFast());
	//aout->ls();
	return aout;

}

// -----------------------------------------------------------------------
TH1F* AliRatiosDiHadronPID::Divide(const TH1F* hist1, const TH1F* hist2) const {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	const char* newname = Form("%s_OVER_%s",hist1->GetName(), hist2->GetName());
	TH1F* division = (TH1F*)(hist1->Clone(newname));
	division->Divide(hist2);

	return division;

}

// -----------------------------------------------------------------------
TGraphErrors* AliRatiosDiHadronPID::CreateStatErrorGraph(TH1F* measurement, TGraphErrors* systerrors) {

	// Creates a TGraphErrors for statistical errors. 
	//  - If only a TH1F* is supplied, then the information is just copied. 
	//  - If also a systematic error graph is given, then it's assumed that that one contains a more
	//    'correct' measurement (since it took the mean of many reasonable measurements). Statistical errors
	//    will still be taken from the TH1F, as they may not be just sqrt(N).

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!measurement) {return 0x0;}

	// Copy the information from the measurement.
	TGraphErrors* graph = new TGraphErrors(measurement);
	graph->SetLineWidth(fHistLineWidth);
	for (Int_t iPoint = 0; iPoint < graph->GetN(); ++iPoint) {graph->SetPointError(iPoint, 0., graph->GetErrorY(iPoint));}

	// If systematic errors are calculated, then use those points.
	if (systerrors) {
		for (Int_t iPoint = 0; iPoint < graph->GetN(); ++iPoint) {
			Double_t x = 0;
			Double_t y = 0;
			systerrors->GetPoint(iPoint,x,y);
			graph->SetPoint(iPoint,x,y);
		}
	}

	graph->SetMarkerStyle(fFirstHistogramMarkerStyle);

	// Set x-axis title.
	(graph->GetXaxis())->SetTitle("p_{T} (GeV/c)");

	return graph;

}

// -----------------------------------------------------------------------
TGraphErrors* AliRatiosDiHadronPID::CreateSystErrorGraph(TObjArray* measurementwithvariations) {

	// Creates a TGraphErrors out of an array of varied spectra by taking the mean, and depending
	// on the strategy, as an error we take:
	//  0 - STD of the sample,
	//  1 - Maximum deviation from the mean (symmetrically)
	//  2 - Same as 1, but ignoring the 10% worst deviations. (TODO)
	// If the "measurement" argument is supplied, then those points are taken insted of the mean.

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	const Int_t NVariations = measurementwithvariations->GetSize() - 1;
	const TH1F* measurement = (TH1F*)(measurementwithvariations->At(0));
	const Int_t NBins = measurement->GetNbinsX();

	// Create temporary array to fit with all variations.
	TArrayD* arraytmp = new TArrayD(NVariations);

	// Create the arrays to use for creating the TGraph
	TArrayD xx(NBins);
	TArrayD yy(NBins);
	TArrayD ex(NBins);
	TArrayD ey(NBins);
	for (Int_t iBin = 1; iBin <= NBins; ++iBin) {

		// Fill the temporary array with the different values of the varied spectra.
		for (Int_t iVariation = 1; iVariation < (NVariations + 1); ++iVariation) {	
			arraytmp->SetAt( ((TH1F*)(measurementwithvariations->At(iVariation)))->GetBinContent(iBin), iVariation - 1);
		}

		// Set the y value to the mean.
		if (measurement) {yy.SetAt(measurement->GetBinContent(iBin), iBin - 1);}
		else {yy.SetAt(GetMean(arraytmp), iBin - 1);}

		// Set the y error depending on strategy.
		switch (fSystErrorDefinition) {
			case 0:
				ey.SetAt(GetSTD(arraytmp), iBin - 1);
				break;
			case 1:
				ey.SetAt(GetMaxDeviation(arraytmp), iBin - 1);
				break;
		}

		xx.SetAt(measurement->GetBinCenter(iBin), iBin - 1);
		ex.SetAt(fSystErrorbarWidth * measurement->GetBinWidth(iBin) / 2., iBin - 1);

		arraytmp->Reset(0.);	// Probably not needed, but seems safer.

	}

	TGraphErrors* graph = new TGraphErrors(NBins, xx.GetArray(), yy.GetArray(), ex.GetArray(), ey.GetArray());

	graph->SetFillStyle(1001);		// fix this!
	graph->SetFillColor(fFirstHistogramFillColor);
	graph->SetMarkerStyle(fFirstHistogramMarkerStyle);
	
	// Set x-axis title.
	(graph->GetXaxis())->SetTitle("p_{T} (GeV/c)");

	return graph;

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::SetErrorAsGroupMember(TGraphErrors* staterror, TGraphErrors* systerror, const Int_t position, const Int_t groupsize) const {

	// If we want to compare different graphs in one window, we want to plot the points
	// in a group, and next to one another. This function repositions/ resizes the markers
	// of the statistical and systematic error properly, assuming that it's on a certain position,
	// and member of a certain group with a groupsize.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (groupsize < 0) {cout << Form("%s -> ERROR: Group size %i not allowed.",__func__, groupsize) << endl; return;}
	if (position < 0 || position > groupsize) {cout << Form("%s -> ERROR: position out of group.",__func__) << endl; return;}

	// Loop over all entries.
	for (Int_t iPoint = 0; iPoint < staterror->GetN(); ++iPoint) {

		Double_t binwidth = fXaxis->GetBinWidth(iPoint + 1);
		Double_t binstart = fXaxis->GetBinLowEdge(iPoint + 1);

		// Calculate the new width and position.
		Double_t systerrwidth = 0.5 * (fSystErrorbarWidth * binwidth) / (Double_t)groupsize;
		Double_t xpos = binstart + 0.5 * (1. - fSystErrorbarWidth) * binwidth + (0.5 + (Double_t)position) * 2. * systerrwidth;

		Double_t ypos = (staterror->GetY())[iPoint];

		staterror->SetPoint(iPoint, xpos, ypos);
		systerror->SetPoint(iPoint, xpos, ypos);

		Double_t yerr = (systerror->GetEY())[iPoint];

		systerror->SetPointError(iPoint, systerrwidth, yerr);

	}

}

// -----------------------------------------------------------------------
Double_t AliRatiosDiHadronPID::GetMinimum(const TGraphErrors* graph) const {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	Int_t Npoints = graph->GetN();
	Double_t* Yvals = graph->GetY();
	Double_t* Yerr = graph->GetEY();

	Double_t minimum = 0.;//Yvals[0] - Yerr[0];
	for (Int_t iPoint = 1; iPoint < Npoints; ++iPoint) {
		if (Yvals[iPoint] - Yerr[iPoint] < minimum) {minimum = Yvals[iPoint] - Yerr[iPoint];}
	}

	return minimum;

}

// -----------------------------------------------------------------------
Double_t AliRatiosDiHadronPID::GetMaximum(const TGraphErrors* graph) const {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	Int_t Npoints = graph->GetN();
	Double_t* Yvals = graph->GetY();
	Double_t* Yerr = graph->GetEY();

	Double_t maximum = Yvals[0] + Yerr[0];
	for (Int_t iPoint = 1; iPoint < Npoints; ++iPoint) {
		if (Yvals[iPoint] + Yerr[iPoint] > maximum) {maximum = Yvals[iPoint] + Yerr[iPoint];}
	}

	return maximum;

}

// -----------------------------------------------------------------------
TH1F* AliRatiosDiHadronPID::GetPercentages(const TGraphErrors* systerrors) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	const char* name = Form("%s_pct",systerrors->GetName());

	TH1F* hout = new TH1F(name, name ,fXaxis->GetNbins(), (fXaxis->GetXbins())->GetArray() );
	hout->SetDirectory(0);
	Double_t* values = systerrors->GetY();
	Double_t* errors = systerrors->GetEY();

	for (Int_t iBin = 1; iBin <= hout->GetNbinsX(); ++iBin) {

		cout << "Bin: " << iBin << " val: " << values[iBin-1] << " err: " << errors[iBin-1] << endl;

		hout->SetBinContent(iBin, errors[iBin-1]/values[iBin-1]);

	}

	hout->SetMinimum(0.);

	hout->SetLineWidth(2);

	return hout;

}

// -----------------------------------------------------------------------
TH1F* AliRatiosDiHadronPID::GetRatio(const TGraphErrors* staterrors1, const TGraphErrors* staterrors2) const {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	const char* name = Form("%s_%s_ratio", staterrors1->GetName(), staterrors2->GetName());

	TH1F* hout = new TH1F(name, name ,fXaxis->GetNbins(), (fXaxis->GetXbins())->GetArray() );
	hout->SetDirectory(0);
	Double_t* values1 = staterrors1->GetY();
	Double_t* values2 = staterrors2->GetY();

	for (Int_t iBin = 1; iBin <= hout->GetNbinsX(); ++iBin) {

		hout->SetBinContent(iBin, values2[iBin - 1]/values1[iBin - 1]);

	}

	hout->SetLineWidth(2);

	return hout;

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::OptimizeYRange(TCanvas* canvas) {

	// Loops over all objects plotted in the canvas (primitives), and finds
	// the optimal range for the Y axis. Expects that only TGraphErrors are plotted.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	TList* primitives = canvas->GetListOfPrimitives();
	if (primitives->GetEntries() == 1) {return;}

	Double_t minimum = GetMinimum((TGraphErrors*)primitives->At(0));
	Double_t maximum = GetMaximum((TGraphErrors*)primitives->At(0));

	for (Int_t iPrimitive = 1; iPrimitive < primitives->GetEntries(); ++iPrimitive) {

		//cout << "minimum: " << minimum << endl;
		//cout << "maximum: " << maximum << endl;

		Double_t minimumtmp = GetMinimum((TGraphErrors*)primitives->At(iPrimitive));
		Double_t maximumtmp = GetMaximum((TGraphErrors*)primitives->At(iPrimitive));

		if (minimumtmp < minimum) {minimum = minimumtmp;}
		if (maximumtmp > maximum) {maximum = maximumtmp;}

	}

	Double_t margin = fYAxisMargin * (maximum - minimum);

	((TGraphErrors*)primitives->At(0))->SetMinimum(minimum - margin);
	((TGraphErrors*)primitives->At(0))->SetMaximum(maximum + margin);

	cout<<endl;

}

// -----------------------------------------------------------------------
Bool_t AliRatiosDiHadronPID::DoesOutputDirExist(Bool_t create) {

	// Checks if the current output directory exists (fDirName). If the option
	// "create" is true, then the directory will be created if it doesn't exist.	
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	
	if (gSystem->cd(fDirName->Data())) {
		gSystem->cd("..");
		return kTRUE;
	} else {
		if (create) {
			gSystem->mkdir(fDirName->Data());
			return kTRUE;
		} else {
			return kFALSE;
		}
	}
}
