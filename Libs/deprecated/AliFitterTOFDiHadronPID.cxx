/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  TOF fitter class.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "AliFitterTOFDiHadronPID.h"

#include <iostream>
using namespace std;

#include "TCanvas.h"
#include "TString.h"
#include "TMath.h"

#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"

#include "TF1.h"
#include "TH1F.h"
#include "TH2F.h"

#include "AliFunctionsDiHadronPID.h"

ClassImp(AliFitterTOFDiHadronPID);

// -----------------------------------------------------------------------
AliFitterTOFDiHadronPID::AliFitterTOFDiHadronPID() : 
	AliFitterBaseDiHadronPID()

{

	// Constructor.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	const Int_t NFitParams = 17;

	// Creating arrays for fit paramters.
	fNFitParams = NFitParams;
	fParName = new TObjArray(fNFitParams);
	fParName->SetOwner(kTRUE);
	fParValue = new TArrayD(fNFitParams);
	fParError = new TArrayD(fNFitParams);
	fParStepSize = new TArrayD(fNFitParams);
	fIsParFixed = new TArrayS(fNFitParams);
	fParSmallVariation = new TArrayD(fNFitParams);

	InitializeParameterArrays();
	
}

// -----------------------------------------------------------------------
void AliFitterTOFDiHadronPID::InitializeParameterArrays() {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Default values.
	const Int_t NFitParams = 17;
	const char* ParName[NFitParams] = {"Background",
										"NPions","sPions","muPions","tailPions",
										"NKaons","sKaons","muKaons","tailKaons",
										"NProtons","sProtons","muProtons","tailProtons",
										"NDeuterons","sDeuterons","muDeuterons","tailDeuterons"};
	// Standard values
	Double_t defBgd = 1.;
	Double_t defIntegral = 1.;
	Double_t defSigma = 120.;
	Double_t defMu = 0.;
	Double_t defTail = (fUseTailSlope ? 0.008 : 100.);

	Double_t ParValue[NFitParams] = {defBgd,
									defIntegral, defSigma, defMu, defTail,
									defIntegral, defSigma, defMu, defTail,
									defIntegral, defSigma, defMu, defTail,
									defIntegral, defSigma, defMu, defTail};

	// Standard "small" values
	Double_t defBgdSmallVar = 1.;		// Has no meaning, just a place holder in the array.
	Double_t defIntegralSmallVar = 1.;	// Has no meaning, just a place holder in the array.
	Double_t defSigmaSmallVar = 5.;
	Double_t defMuSmallVar = 5.;
	Double_t defTailSmallVar = (fUseTailSlope ? 0.001 : 10.);

	Double_t ParValueSmallVar[NFitParams] = {defBgdSmallVar,
									defIntegralSmallVar, defSigmaSmallVar, defMuSmallVar, defTailSmallVar,
									defIntegralSmallVar, defSigmaSmallVar, defMuSmallVar, defTailSmallVar,
									defIntegralSmallVar, defSigmaSmallVar, defMuSmallVar, defTailSmallVar,
									defIntegralSmallVar, defSigmaSmallVar, defMuSmallVar, defTailSmallVar};


	for (Int_t iPar = 0; iPar < NFitParams; iPar++) {
		fParName->AddAt((TObject*)(new TString(ParName[iPar])), iPar);
		fParValue->SetAt(ParValue[iPar], iPar);
		fParSmallVariation->SetAt(ParValueSmallVar[iPar], iPar);
		fParError->SetAt(0., iPar);
		fParStepSize->SetAt(1e-4, iPar);
		fIsParFixed->SetAt(kTRUE, iPar);

	}

}

// -----------------------------------------------------------------------
AliFitterTOFDiHadronPID::~AliFitterTOFDiHadronPID() {

	// Destructor.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

}

// -----------------------------------------------------------------------
void AliFitterTOFDiHadronPID::PrintFit(TCanvas* cvs, Int_t pad) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!cvs) {cout << Form("%s -> ERROR: No canvas supplied.",__func__) << endl; return;}
	TPad* CurrentPad = (TPad*)cvs->GetPad(pad);
	if (!CurrentPad) {cout << Form("%s -> ERROR: No invalid pad number.",__func__) << endl; return;}
	if (!fData) {cout << Form("%s -> ERROR: No data histogram set.",__func__) << endl; return;}

	// Select the desired pad.
	cvs->cd(pad);
	CurrentPad->SetLogy();

	// Printing Data.
	UpdateFitDescription();
	fData->DrawCopy();

	// Printing the fit result.
	if (fFitResult) {fFitResult->DrawCopy("same");}

	for (Int_t ii = 0; ii < 4; ii++) {
		if (fPeaksFunc->At(ii)) {((TF1*)fPeaksFunc->At(ii))->DrawCopy("same");}
	}

	// Printing the mismatch.
	if (fMismatchScaled) {fMismatchScaled->DrawCopy("same");}

}

// -----------------------------------------------------------------------
Int_t AliFitterTOFDiHadronPID::DoFitWithStrategyPbPb(Int_t strategy, TObjArray* fitstages, AliSpectrumDiHadronPID*, const char*, Int_t, Bool_t) {

	// Performs fit according to a standardized strategy.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	switch (strategy) {
		case 0:
			SetUseTailSlope(kTRUE);
			return DoFitWithStrategyPbPbDefault(fitstages);			// Default strategy.
		break;
		case 1:
			SetUseTailSlope(kTRUE);
			return DoFitWithStrategyPbPbTweaked(fitstages);
		default:
			cout << Form("%s -> ERROR: Unknown strategy.",__func__) << endl;
	}

	return 0;

}

// -----------------------------------------------------------------------
Int_t AliFitterTOFDiHadronPID::DoFitWithStrategypp(const Int_t strategy, TObjArray* fitstages, AliSpectrumDiHadronPID*, const char*, Int_t, Bool_t) {

	// Performs fit according to a standardized strategy.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	switch (strategy) {
		case 0:
			return DoFitWithStrategyppDefault(fitstages);			// Default strategy.
		break;
		default:
			cout << Form("%s -> ERROR: Unknown strategy.",__func__) << endl;
	}

	return 0;

}

// -----------------------------------------------------------------------
void AliFitterTOFDiHadronPID::SetHistogram(const TH1* hist) {

	// Clones hist to data member fData
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!hist) {cout << Form("%s -> ERROR: No histogram supplied.",__func__) << endl; return;}

	// Check if the histogram is of the right kind.
	TString classname(hist->ClassName());
	if (!classname.Contains("TH1")) {cout << Form("%s -> ERROR: I don't accept a histogram of type: %s.",__func__, classname.Data()) << endl; return;}

	fData = (TH1F*)hist->Clone();
	fData->SetDirectory(0);

	// Automatically set the TOF fit range to the range of the TOF axis.
	TAxis* TOFaxis = fData->GetXaxis();
	//cout<<TOFaxis->GetBinLowEdge(1)<<" "<<TOFaxis->GetBinUpEdge(fData->GetNbinsX())<<endl;
	SetTOFRange(TOFaxis->GetBinLowEdge(1), TOFaxis->GetBinUpEdge(fData->GetNbinsX()));

}

// -----------------------------------------------------------------------
void AliFitterTOFDiHadronPID::SetMismatchedBackground(const TH1* hist) {

	// Clones hist to data member fMismatch
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!hist) {cout << Form("%s -> ERROR: No histogram supplied.",__func__) << endl; return;}

	// Check if the histogram is of the right kind.
	TString classname(hist->ClassName());
	if (!classname.Contains("TH1")) {cout << Form("%s -> ERROR: I don't accept a histogram of type: %s.",__func__, classname.Data()) << endl; return;}

	fMismatch = (TH1F*)hist->Clone();
	fMismatch->SetDirectory(0);

	// Include the background in the fit.
	SetFixNBgd(kFALSE);

}

// -----------------------------------------------------------------------
void AliFitterTOFDiHadronPID::SetUsedFunction(Int_t species, Int_t function) {

	// Implemented function types:
	// 0 - no function;
	// 1 - Gaussian;
	// 2 - Gaussian + Tail;
	// 3 - Histogram (not yet implemented);
	// 4 - Spline (not yet implemented);
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if ( (species < 0) || (species > 3)) {cout << Form("%s -> ERROR: Not a valid species.",__func__) << endl; return;}

	fUsedFunction->SetAt(function, species);

	switch (function) {
		case 0:
			SetFixIntegral(species,kTRUE);
			SetFixSigmaTOF(species,kTRUE);
			SetFixMuTOF(species,kTRUE);
			SetFixTailTOF(species,kTRUE);
			break;
		case 1:
			SetFixIntegral(species,kFALSE);
			SetFixSigmaTOF(species,kFALSE);
			SetFixMuTOF(species,kFALSE);
			SetFixTailTOF(species,kTRUE);
			break;
		case 2:
			SetFixIntegral(species,kFALSE);
			SetFixSigmaTOF(species,kFALSE);
			SetFixMuTOF(species,kFALSE);
			SetFixTailTOF(species,kFALSE);
			break;
		default:
			cout << Form("%s -> ERROR: Requested function not implemented.",__func__) << endl;
	}

}

// -----------------------------------------------------------------------
void AliFitterTOFDiHadronPID::EstimateNParticles() {

	// Given that the values for mu and sigma are reasonable, a simple estimate for
	// the number of particles in a peak is given by (N_{central bin} * 2 sigma)/bin width. 
	// NOTE: Approximation is quite crude.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Check prerequisites.
	if (!fData) {cout<<"ERROR: First set a data histogram..."<<endl; return;}

	// Find the corresponding bins to the peaks.
	for (Int_t ii = 0; ii < 4; ii++) {
		Int_t peakbin = fData->FindBin(GetMuTOF(ii));
		Double_t peakheight = fData->GetBinContent(peakbin);
		Double_t integralestimate = 2.*(GetSigmaTOF(ii))*peakheight/(fData->GetBinWidth(peakbin));
		SetIntegral(ii,integralestimate);
	}

}

// -----------------------------------------------------------------------
void AliFitterTOFDiHadronPID::EstimateNMismatch(Int_t option) {

	// option = 0 -> estimate background by looking at the final three bins.
	// option = 1 -> estimate background in a predefined range in TOF. (optimized for the TOF axes in PID histos of track cuts object)
	// option = 2 -> estimate background in a predefined range in TOF. (optimized for "LargeTOFaxis" size of DiHadronPID data)

	// Estimates the number of mismatches.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}


	const Int_t NbinsTOF = fData->GetNbinsX();
	const Int_t NbinsTPC = fData->GetNbinsY();

	Int_t minTOFbin = NbinsTOF - 3;
	Int_t maxTOFbin = NbinsTOF;

	TAxis* TOFaxis = fData->GetXaxis();
	if (option != 0) {

		Double_t ptmean = (fPtMax + fPtMin) / 2.;
		
		/* TODO: Don't use NbinsTOF explicitly. */
		//cout<<"ptmean: "<<ptmean<<endl;

		switch (fMassAssumption) {

			// Mass assumption: Pions.
			case 0:
			
				switch (option) {
					case 1:
						// Tuned to the spectra data.
						if (ptmean < 0.65) 					{minTOFbin = TOFaxis->FindBin(6000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 0.65 && ptmean < 0.80) {minTOFbin = TOFaxis->FindBin(4000.); maxTOFbin = TOFaxis->FindBin(6000.);}
						if (ptmean > 0.80 && ptmean < 0.90) {minTOFbin = TOFaxis->FindBin(3000.); maxTOFbin = TOFaxis->FindBin(4500.);}
						if (ptmean > 0.90 && ptmean < 1.00) {minTOFbin = TOFaxis->FindBin(6000.); maxTOFbin = TOFaxis->FindBin(8000.);}
						if (ptmean > 1.00 && ptmean < 1.10) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = TOFaxis->FindBin(3000.);}
						if (ptmean > 1.10 && ptmean < 1.50) {minTOFbin = TOFaxis->FindBin(4500.); maxTOFbin = TOFaxis->FindBin(6000.);}
						if (ptmean > 1.50 && ptmean < 2.00) {minTOFbin = TOFaxis->FindBin(3000.); maxTOFbin = TOFaxis->FindBin(3500.);}
						if (ptmean > 2.00 && ptmean < 2.30) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = TOFaxis->FindBin(3000.);}
						if (ptmean > 2.30 && ptmean < 2.50) {minTOFbin = TOFaxis->FindBin(5000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.50 && ptmean < 3.00) {minTOFbin = TOFaxis->FindBin(4000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.00 && ptmean < 3.40) {minTOFbin = TOFaxis->FindBin(3000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.40 && ptmean < 5.00) {minTOFbin = TOFaxis->FindBin(2300.); maxTOFbin = NbinsTOF;}
					break;

					case 2:
						// Tuned to the DiHadron Data.
						if (ptmean > 2.0 && ptmean < 2.1) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.1 && ptmean < 2.2) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.2 && ptmean < 2.3) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.3 && ptmean < 2.4) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = TOFaxis->FindBin(3000.);}
						if (ptmean > 2.4 && ptmean < 2.5) {minTOFbin = TOFaxis->FindBin(1800.); maxTOFbin = TOFaxis->FindBin(2400.);}
						if (ptmean > 2.5 && ptmean < 2.6) {minTOFbin = TOFaxis->FindBin(1600.); maxTOFbin = TOFaxis->FindBin(2100.);}
						if (ptmean > 2.6 && ptmean < 2.8) {minTOFbin = TOFaxis->FindBin(1500.); maxTOFbin = TOFaxis->FindBin(2000.);}
						if (ptmean > 2.8 && ptmean < 3.0) {minTOFbin = TOFaxis->FindBin(1400.); maxTOFbin = TOFaxis->FindBin(1900.);}
						if (ptmean > 3.0 && ptmean < 3.2) {minTOFbin = TOFaxis->FindBin(1300.); maxTOFbin = TOFaxis->FindBin(1600.);}
						if (ptmean > 3.2 && ptmean < 3.4) {minTOFbin = TOFaxis->FindBin(2300.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.4 && ptmean < 3.6) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.6 && ptmean < 3.8) {minTOFbin = TOFaxis->FindBin(1800.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.8 && ptmean < 4.2) {minTOFbin = TOFaxis->FindBin(1800.); maxTOFbin = NbinsTOF;}
						if (ptmean > 4.2 && ptmean < 4.6) {minTOFbin = TOFaxis->FindBin(1500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 4.6 && ptmean < 5.0) {minTOFbin = TOFaxis->FindBin(1200.); maxTOFbin = NbinsTOF;}
					break;

					default:
						cout << Form("%s -> ERROR: Invalid option",__func__) << endl;

					}

			break;


			// Mass assumption: Kaons.
			case 1:	
				switch (option) {
					case 1:
						// Tuned to the spectra data.		
						if (ptmean < 0.70) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = TOFaxis->FindBin(4000.);}
						if (ptmean > 0.70 && ptmean < 0.90) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = TOFaxis->FindBin(2500.);}
						if (ptmean > 0.90 && ptmean < 1.00) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = TOFaxis->FindBin(2000.);}						
						if (ptmean > 1.00 && ptmean < 1.10) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = TOFaxis->FindBin(1600.);}	
						if (ptmean > 1.10 && ptmean < 1.50) {minTOFbin = TOFaxis->FindBin(4000.); maxTOFbin = TOFaxis->FindBin(6000.);}	
						if (ptmean > 1.50 && ptmean < 1.80) {minTOFbin = TOFaxis->FindBin(2500.); maxTOFbin = TOFaxis->FindBin(4000.);}
						if (ptmean > 1.80 && ptmean < 2.10) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = TOFaxis->FindBin(3000.);}
						if (ptmean > 2.10 && ptmean < 2.30) {minTOFbin = TOFaxis->FindBin(1800.); maxTOFbin = TOFaxis->FindBin(2500.);}
						if (ptmean > 2.30 && ptmean < 3.00) {minTOFbin = TOFaxis->FindBin(4000.); maxTOFbin = TOFaxis->FindBin(6000.);}
						if (ptmean > 3.00 && ptmean < 3.60) {minTOFbin = TOFaxis->FindBin(2500.); maxTOFbin = TOFaxis->FindBin(4000.);}
						if (ptmean > 3.60 && ptmean < 4.20) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = TOFaxis->FindBin(4000.);}
						if (ptmean > 4.20 && ptmean < 5.00) {minTOFbin = TOFaxis->FindBin(1500.); maxTOFbin = TOFaxis->FindBin(4000.);}
					break;
					case 2:
						if (ptmean > 2.0 && ptmean < 2.1) {minTOFbin = TOFaxis->FindBin(2200.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.1 && ptmean < 2.2) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = TOFaxis->FindBin(3000.);}
						if (ptmean > 2.2 && ptmean < 2.3) {minTOFbin = TOFaxis->FindBin(1900.); maxTOFbin = TOFaxis->FindBin(2500.);}
						if (ptmean > 2.3 && ptmean < 2.4) {minTOFbin = TOFaxis->FindBin(1800.); maxTOFbin = TOFaxis->FindBin(2400.);}
						if (ptmean > 2.4 && ptmean < 2.5) {minTOFbin = TOFaxis->FindBin(1700.); maxTOFbin = TOFaxis->FindBin(2200.);}
						if (ptmean > 2.5 && ptmean < 2.6) {minTOFbin = TOFaxis->FindBin(1600.); maxTOFbin = TOFaxis->FindBin(2000.);}
						if (ptmean > 2.6 && ptmean < 2.8) {minTOFbin = TOFaxis->FindBin(1500.); maxTOFbin = TOFaxis->FindBin(1800.);}
						if (ptmean > 2.8 && ptmean < 3.0) {minTOFbin = TOFaxis->FindBin(2500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.0 && ptmean < 3.2) {minTOFbin = TOFaxis->FindBin(2200.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.2 && ptmean < 3.4) {minTOFbin = TOFaxis->FindBin(2100.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.4 && ptmean < 3.6) {minTOFbin = TOFaxis->FindBin(1900.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.6 && ptmean < 3.8) {minTOFbin = TOFaxis->FindBin(1800.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.8 && ptmean < 4.2) {minTOFbin = TOFaxis->FindBin(1700.); maxTOFbin = NbinsTOF;}
						if (ptmean > 4.2 && ptmean < 4.6) {minTOFbin = TOFaxis->FindBin(1500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 4.6 && ptmean < 5.0) {minTOFbin = TOFaxis->FindBin(1300.); maxTOFbin = NbinsTOF;}

					break;
					default:
						cout << Form("%s -> ERROR: Invalid option",__func__) << endl;
				}

			break;

			// Mass assumption: Protons.
			case 2:
				switch (option) {
					case 1:
						if (ptmean < 1.00) {minTOFbin = TOFaxis->FindBin(3000.); maxTOFbin = TOFaxis->FindBin(6000.);}
						if (ptmean > 1.00 && ptmean < 1.50) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = TOFaxis->FindBin(6000.);}						
						if (ptmean > 1.50 && ptmean < 2.00) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = TOFaxis->FindBin(2400.);}	
						if (ptmean > 2.00 && ptmean < 2.50) {minTOFbin = TOFaxis->FindBin(4000.); maxTOFbin = TOFaxis->FindBin(6000.);}
						if (ptmean > 2.50 && ptmean < 3.00) {minTOFbin = TOFaxis->FindBin(3000.); maxTOFbin = TOFaxis->FindBin(6000.);}	
						if (ptmean > 3.00 && ptmean < 5.00) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = TOFaxis->FindBin(6000.);}	
					break;

					case 2:
						if (ptmean > 2.0 && ptmean < 2.1) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.1 && ptmean < 2.2) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.2 && ptmean < 2.3) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.3 && ptmean < 2.4) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.4 && ptmean < 2.5) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = TOFaxis->FindBin(1800.);}
						if (ptmean > 2.5 && ptmean < 2.6) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = TOFaxis->FindBin(1800.);}
						if (ptmean > 2.6 && ptmean < 2.8) {minTOFbin = TOFaxis->FindBin(800.); maxTOFbin = TOFaxis->FindBin(1500.);}
						if (ptmean > 2.8 && ptmean < 3.0) {minTOFbin = TOFaxis->FindBin(700.); maxTOFbin = TOFaxis->FindBin(1400.);}
						if (ptmean > 3.0 && ptmean < 3.2) {minTOFbin = TOFaxis->FindBin(600.); maxTOFbin = TOFaxis->FindBin(1100.);}
						if (ptmean > 3.2 && ptmean < 3.4) {minTOFbin = TOFaxis->FindBin(1700.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.4 && ptmean < 3.6) {minTOFbin = TOFaxis->FindBin(1500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.6 && ptmean < 3.8) {minTOFbin = TOFaxis->FindBin(1500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.8 && ptmean < 4.2) {minTOFbin = TOFaxis->FindBin(1400.); maxTOFbin = NbinsTOF;}
						if (ptmean > 4.2 && ptmean < 4.6) {minTOFbin = TOFaxis->FindBin(1200.); maxTOFbin = NbinsTOF;}
						if (ptmean > 4.6 && ptmean < 5.0) {minTOFbin = TOFaxis->FindBin(900.); maxTOFbin = NbinsTOF;}
					break;


					default:
						cout << Form("%s -> ERROR: Invalid option",__func__) << endl;
				}
			break;

			default:
				cout << Form("%s -> ERROR: Not a valid mass assumption (fMassAssumption)",__func__) << endl;

			break;

		}

	}

	// Prevent using the under/ overflow bins. 
	if (minTOFbin == 0) {minTOFbin++;}
	if (maxTOFbin == (NbinsTOF + 1)) {maxTOFbin--;}

	cout<<"TOFminbin: "<<minTOFbin<<" maxbin: "<<maxTOFbin<<" val: "<<TOFaxis->GetBinCenter(maxTOFbin)<<endl;

	Double_t DataSum = 0, MismatchSum = 0;
	for (Int_t iBinTOF = minTOFbin; iBinTOF < (maxTOFbin + 1); iBinTOF++) {
		for (Int_t iBinTPC = 1; iBinTPC < (NbinsTPC + 1); iBinTPC++) {
			DataSum += fData->GetBinContent(iBinTOF, iBinTPC);
			MismatchSum += fMismatch->GetBinContent(iBinTOF, iBinTPC);
		}
	}

	Double_t Ratio = DataSum / MismatchSum;
	SetNBgd( ((Double_t)fMismatch->GetEntries()) * Ratio);

}

// -----------------------------------------------------------------------
Double_t AliFitterTOFDiHadronPID::GetSumNChargedInFitRegion() const {

	// Returns a sum of the bin contents included in the fit.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	Int_t minbin = fData->FindBin(fTOFmin);
	Int_t maxbin = fData->FindBin(fTOFmax);
	Double_t sum = 0;
	for (Int_t iBin = minbin; iBin < (maxbin + 1); iBin++) {
		sum += fData->GetBinContent(iBin);
	}

	return sum;

}

// -----------------------------------------------------------------------
Double_t AliFitterTOFDiHadronPID::GetIntegralOverFitFunctionInFitRegion() {

	// Returns the sum of the bin contents of the fit.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	Int_t minbin = fData->FindBin(fTOFmin);
	Int_t maxbin = fData->FindBin(fTOFmax);
	Double_t sum = 0;

	// Update fFitResult.
	UpdateFitDescription();
	
	for (Int_t iBin = minbin; iBin < (maxbin + 1); iBin++) {
		sum += fFitResult->GetBinContent(iBin);
	}

	return sum;

}

// -----------------------------------------------------------------------
TArrayI* AliFitterTOFDiHadronPID::GetAllParsForSystematics() const {

	// Returns a list of all parameters that could be varied as a systematic check.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	Int_t allpars[13] = {GetNBgdParN(), GetSigmaTOFParN(0), GetMuTOFParN(0), GetTailTOFParN(0),
		GetSigmaTOFParN(1), GetMuTOFParN(1), GetTailTOFParN(1),
		GetSigmaTOFParN(2), GetMuTOFParN(2), GetTailTOFParN(2),
		GetSigmaTOFParN(3), GetMuTOFParN(3), GetTailTOFParN(3)};

	return (new TArrayI(13, allpars));

}

// -----------------------------------------------------------------------
TArrayI* AliFitterTOFDiHadronPID::GetUsedParsForSystematics(Int_t /*fitstrategy*/) const {

	// Returns subset of the list generated by GetAllParsForSystematics(), 
	// of the parameters that were actually used in the last performed fit (i.e., 
	// we rely on fUsedFunction being up to date).
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	Int_t iPar = 0;
	TArrayI* usedpars = new TArrayI(fNFitParams);	// The array will for sure not be bigger than this number.

	// If there's a mismatch histogram known, try to vary it.
	if (fMismatch) {
		usedpars->AddAt(GetNBgdParN(),iPar);
		iPar++;
	}

	// Loop over all function descriptions.
	for (Int_t iSpecies = 0; iSpecies <= 3; iSpecies++) {
		switch (fUsedFunction->At(iSpecies)) {
			case 1:
				usedpars->AddAt(GetSigmaTOFParN(iSpecies), iPar); iPar++;
				usedpars->AddAt(GetMuTOFParN(iSpecies), iPar); iPar++;
			break;
			case 2:
				usedpars->AddAt(GetSigmaTOFParN(iSpecies), iPar); iPar++;
				usedpars->AddAt(GetMuTOFParN(iSpecies), iPar); iPar++;
				usedpars->AddAt(GetTailTOFParN(iSpecies), iPar); iPar++;				
			break;
		}
	}

	// Set array length to the actual length.
	usedpars->Set(iPar);

	return usedpars;

}

// -----------------------------------------------------------------------
void AliFitterTOFDiHadronPID::SetTOFRange(Double_t min, Double_t max) {

	// Sets the TOF fitting range.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (min > max) {cout << Form("%s -> ERROR: min > max.",__func__) << endl; return;}
	if (fData) {
		TAxis* TOFaxis = fData->GetXaxis();
		Double_t TOFmin = TOFaxis->GetBinLowEdge(1);
		Double_t TOFmax = TOFaxis->GetBinUpEdge(fData->GetNbinsX());
		if (min < TOFmin) {cout << Form("%s -> ERROR: min is smaller than the minimum of the supplied data histogram.",__func__) << endl; return;}
		if (max > TOFmax) {cout << Form("%s -> ERROR: max is bigger than the maximum of the supplied data histogram.",__func__) << endl; return;}
	}

	fTOFmin = min; 
	fTOFmax = max;

}

// -----------------------------------------------------------------------
void AliFitterTOFDiHadronPID::SetMergePiKaPeak() {

	// TODO: Make change of parameters work.

	// Merges the Pion and Kaon peak (can be used in the proton fit
	// at high p_T).
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	SetUsedFunction(1,0);			// Ignore the Kaon peak.
	Double_t pikamuestimate = (EstimateTOFPosition(0) + EstimateTOFPosition(1))/2.;
	SetMuTOF(0, pikamuestimate);
/*		fParName[1] = "NPiKa";
	fParName[2] = "sPiKa";
	fParName[3] = "muPiKa";
	fParName[4] = "sPiKa";
	fParName[5] = "n/a 1";
	fParName[6] = "n/a 2";
	fParName[7] = "n/a 3";	
	fParName[8] = "n/a 4";*/
}

// -----------------------------------------------------------------------
double AliFitterTOFDiHadronPID::ChiSquared(const double* xx) const {

	/* TODO: Error should include error on mismatch! */

	// Let's say the gaussian continues in a exponent at point p, then the "slope" of the exponent,
	// i.e., b in exp(-bx), is given by: b = s^2/(p-mu).

	// Chi^2 calculated given a set of parameters xx, and the
	// supplied histograms, fit range, and function perscriptions.
	if (fDebug > 1) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Loop over all included bins:
	Int_t minbin = fData->FindBin(fTOFmin);
	Int_t maxbin = fData->FindBin(fTOFmax);

	Double_t chisq = 0.;

	for (Int_t iBin = minbin; iBin < (maxbin + 1); iBin++) {

		Double_t dataval = fData->GetBinContent(iBin);
		Double_t dataerror = fData->GetBinError(iBin);

		Double_t modelval = 0.;
		if (fMismatch) {
			modelval += (fMismatch->GetBinContent(iBin) * xx[GetNBgdParN()] / fMismatch->GetEntries() );
		}

		for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {
		
			Double_t tailparameter = 0.;
			switch (fUsedFunction->At(iSpecies)) {
				case 0:
					// Particle is not part of the fit.
				break;
				case 1:
					// Particle is described by a 1D Gaussian.
					modelval += AliFunctionsDiHadronPID::Gaussian1D(
						fData->GetBinCenter(iBin), xx[GetIntegralParN(iSpecies)], xx[GetMuTOFParN(iSpecies)],
						xx[GetSigmaTOFParN(iSpecies)], fData->GetBinWidth(iBin));
				break;
				case 2:
					// Particle is described by a 1D Gaussian with tail in positive TOF direction.
					// The tail parameter can either mean the position at which the gaussian and exponent join,
					// or it can mean b.
					tailparameter = xx[GetTailTOFParN(iSpecies)];
					if (fUseTailSlope) {tailparameter = (xx[GetSigmaTOFParN(iSpecies)] * xx[GetSigmaTOFParN(iSpecies)]) * xx[GetTailTOFParN(iSpecies)];}

					modelval += AliFunctionsDiHadronPID::Gaussian1DTail(
						fData->GetBinCenter(iBin), xx[GetIntegralParN(iSpecies)], xx[GetMuTOFParN(iSpecies)],
						xx[GetSigmaTOFParN(iSpecies)], tailparameter ,fData->GetBinWidth(iBin));
				break;
				case 3:
					// Particle is described by a template histogram.
					cout<<"Function type not yet implemented "<<endl;
				break;
				default:
					cout<<"Not a valid function was entered for species "<<iSpecies<<endl;
			}

		}

		// Protect against division through 0. Not so neat...
		Double_t currentchi = 0.;
		if (dataerror != 0.) currentchi = (dataval - modelval) / dataerror;

		chisq += (currentchi * currentchi);

	}

	// Applying Peak Swap Penalty, i.e., chi^2 gets multiplied with a punishment factor if
	// peaks of particles are swapped.
	if (fPeakSwapPenalty) {

		// Loop over pairs.
		for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {

			if (fUsedFunction->At(iSpecies) && fUsedFunction->At(iSpecies + 1)) {
				if (xx[GetMuTOFParN(iSpecies + 1)] < xx[GetMuTOFParN(iSpecies)]) {
					chisq *= AliFunctionsDiHadronPID::PolyPenalty(xx[GetMuTOFParN(iSpecies + 1)] - xx[GetMuTOFParN(iSpecies)], 0., 0., fPeakSwapPenaltyOrder); 
				}
			}
		}
	}

	// Applying Peak Deviation Penalty, i.e., chi^2 gets multiplied with a punishment factor if
	// peaks of particles deviate too much (specified by user) form the expected value.
	if (fPeakDevPenalty) {

		for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {

			if (fUsedFunction->At(iSpecies)) {

				Double_t currentmu = xx[GetMuTOFParN(iSpecies)];
				Double_t expectedmu = EstimateTOFPosition(iSpecies);

				chisq *= AliFunctionsDiHadronPID::PolyPenalty(currentmu, expectedmu, fPeakDevPenaltyTOFRange, fPeakDevPenaltyOrder);

			}
		}
	}

	// Applying negative integral penalty.
	if (fNegIntPenalty) {

		for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {

			if (fUsedFunction->At(iSpecies) && (xx[GetIntegralParN(iSpecies)] < 0.)) {

				chisq *= AliFunctionsDiHadronPID::PolyPenalty(xx[GetIntegralParN(iSpecies)], 0., 0., fNegIntPenaltyOrder);

			}
		}
	}

	return chisq;

}

// -----------------------------------------------------------------------
void AliFitterTOFDiHadronPID::UpdateFitDescription() {

	// Updates the data members used to display the fit as it currently is, i.e.,
	// fFitResult, fMismatchScaled, fPeaksFunc[]
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fPeaksFunc) {

	}


	// 1) Updating fPeaksFunc[]
	for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {
		
		// Deleting existing functions.	
		if (fPeaksFunc->At(iSpecies)) {
			delete fPeaksFunc->At(iSpecies); 
			fPeaksFunc->AddAt(0x0, iSpecies);
		}

		// Creating the new function.
		TF1* ftmp = 0x0;
		Double_t tailparameter = 0.;
		switch (fUsedFunction->At(iSpecies)) {

			case 0:
				// Particle is not part of the fit.
			break;
			case 1:
				// Particle is described by a 1D Gaussian.
				ftmp = new TF1(Form("fPeaksFunc_%i",iSpecies),
					"AliFunctionsDiHadronPID::Gaussian1D(x,[0],[1],[2],[3])",fTOFmin,fTOFmax);
				ftmp->SetLineColor(iSpecies+2);
				ftmp->SetParNames("Integral","#mu","#sigma","binwidth");
				ftmp->SetParameters(GetIntegral(iSpecies),GetMuTOF(iSpecies),GetSigmaTOF(iSpecies),fData->GetBinWidth(1));
				fPeaksFunc->AddAt(ftmp, iSpecies);
			break;
			case 2:
				// Particle is described by a 1D Gaussian + Tail.
				ftmp = new TF1(Form("fPeaksFunc_%i",iSpecies),
					"AliFunctionsDiHadronPID::Gaussian1DTail(x,[0],[1],[2],[3],[4])",fTOFmin,fTOFmax);
				ftmp->SetLineColor(iSpecies+2);
				ftmp->SetParNames("Integral","#mu","#sigma","tail","binwidth");

				tailparameter = GetTailTOF(iSpecies);
				if (fUseTailSlope) {
					// in this case the tail means the slope.
					tailparameter = GetTailTOF(iSpecies) * GetSigmaTOF(iSpecies) * GetSigmaTOF(iSpecies);
					//cout << "Updating fit description, slope = " << GetTailTOF(iSpecies) << " hence, p = "<<tailparameter<<endl;
				}

				ftmp->SetParameters(GetIntegral(iSpecies),GetMuTOF(iSpecies),GetSigmaTOF(iSpecies),tailparameter,fData->GetBinWidth(1));
				fPeaksFunc->AddAt(ftmp, iSpecies);
			break;
			default:
				cout<<"Not a valid function was entered for species "<<iSpecies<<endl;
		}
	}

	// 2) Updating fMismatchScaled
	if (fMismatch) {

		// Deleting existing histogram.
		if (fMismatchScaled) {
			delete fMismatchScaled;
			fMismatchScaled = 0x0;
		}

		// Creating new histogram.
		fMismatchScaled = (TH1F*)fMismatch->Clone("fMismatchScaled");
		fMismatchScaled->SetDirectory(0);
		fMismatchScaled->Scale( GetNBgd() / fMismatch->GetEntries() );
		fMismatchScaled->SetLineColor(kGray+1);
		fMismatchScaled->SetMarkerColor(kGray+1);
		fMismatchScaled->SetLineWidth(2);

	}  

	// 3) Updating fFitResult
	if (fFitResult) {
		delete fFitResult; 
		fFitResult = 0x0;
	}

	fData->SetLineColor(kBlack);
	fData->SetLineWidth(2);

	Int_t Nbins = fData->GetNbinsX();
	Double_t minhist = fData->GetBinLowEdge(1);
	Double_t maxhist = fData->GetBinLowEdge(Nbins+1);

	fFitResult = new TH1F("fFitResult","fFitResult",Nbins,minhist,maxhist);
	fFitResult->SetDirectory(0);
	fFitResult->SetLineColor(kViolet);
	fFitResult->SetLineWidth(2);
	fFitResult->SetMarkerColor(kViolet);
	fFitResult->SetMarkerStyle(kFullCircle);

	Int_t minbin = fData->FindBin(fTOFmin);
	Int_t maxbin = fData->FindBin(fTOFmax);

	for (Int_t iBin = minbin; iBin < (maxbin+1); iBin++) {

		Double_t modelval = 0.;

		if (fMismatch) {
			modelval += (fMismatch->GetBinContent(iBin) * GetNBgd() / fMismatch->GetEntries() );
		}

		for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {
			if (fPeaksFunc->At(iSpecies)) {
				modelval += ( ((TF1*)fPeaksFunc->At(iSpecies))->Eval(fData->GetBinCenter(iBin)) );
			}
		}

		fFitResult->SetBinContent(iBin,modelval);

	}

}

// -----------------------------------------------------------------------
void AliFitterTOFDiHadronPID::SetRangePbPbDefault() {

	// Sets the range on the TOF axis.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Set fit ranges and fit functions.
	switch (fMassAssumption) {
		case 0:
			if (fPtMin < 0.7) {SetTOFRange(-300., GetMuTOF(1)/2.);}
			else if (fPtMin < 2.0) {SetTOFRange(-300., GetMuTOF(2));} 
			else {SetTOFRange(-300., GetMuTOF(2) + 500.);}
		break;

		case 1:
			if (fPtMin < 0.7) {SetTOFRange(GetMuTOF(0)/2., GetMuTOF(2)/2.);}
			else if (fPtMin < 2.0) {SetTOFRange(GetMuTOF(0) - 250., GetMuTOF(2)/2.);}
			else {SetTOFRange(GetMuTOF(0) - 250., GetMuTOF(2) + 300.);}
		break;

		case 2:
			if (fPtMin < 1.2) {SetTOFRange(GetMuTOF(1)/2., 3000.);}
			else if (fPtMin < 2.2) {SetTOFRange(GetMuTOF(1), 3000.);} 
			else if (fPtMin < 3.2) {SetTOFRange(GetMuTOF(1), 3000.);} 
			else if (fPtMin < 3.6){
				SetTOFRange(GetMuTOF(0) - 300., 3000.);
				SetPeakSwapPenalty();	
				SetPeakDevPenalty();	
				SetNegIntPenalty();
			} else {
				SetTOFRange(GetMuTOF(0) - 300., 3000.);
				SetPeakSwapPenalty();	
				SetNegIntPenalty();
				SetMergePiKaPeak();		// FIXME!
			}
		break;

		default:
			cout << Form("%s -> ERROR: %i is not a valid mass assumption.", __func__, fMassAssumption) << endl;
	}


}

// -----------------------------------------------------------------------
void AliFitterTOFDiHadronPID::SetRangeppDefault() {

	// Sets the range on the TOF axis.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Set fit ranges and fit functions.
	switch (fMassAssumption) {
		case 0:
			if (fPtMin < 0.7) {SetTOFRange(-300., GetMuTOF(1)/2.);}
			else if (fPtMin < 2.0) {SetTOFRange(-300., GetMuTOF(2) + 1000. );} 
			else {SetTOFRange(-300., GetMuTOF(2) + 500.);}
		break;

		case 1:
			if (fPtMin < 0.7) {SetTOFRange(GetMuTOF(0)/2., GetMuTOF(2)/2.);}
			else if (fPtMin < 2.0) {SetTOFRange(GetMuTOF(0) - 250., GetMuTOF(2)/2.);}
			else {SetTOFRange(GetMuTOF(0) - 250., GetMuTOF(2) + 300.);}
		break;

		case 2:
			if (fPtMin < 1.2) {SetTOFRange(GetMuTOF(1)/2., 3000.);}
			else if (fPtMin < 2.2) {SetTOFRange(GetMuTOF(1), 3000.);} 
			else if (fPtMin < 3.2) {SetTOFRange(GetMuTOF(1), 3000.);} 
			else if (fPtMin < 3.6){
				SetTOFRange(GetMuTOF(0) - 300., 3000.);
				SetPeakSwapPenalty();	
				SetPeakDevPenalty();	
				SetNegIntPenalty();
			} else {
				SetTOFRange(GetMuTOF(0) - 300., 3000.);
				SetPeakSwapPenalty();	
				SetNegIntPenalty();
				SetMergePiKaPeak();		// FIXME!
			}
		break;

		default:
			cout << Form("%s -> ERROR: %i is not a valid mass assumption.", __func__, fMassAssumption) << endl;
	}


}

// -----------------------------------------------------------------------
TArrayI* AliFitterTOFDiHadronPID::GetPeakDescriptionPbPbDefault() {

	// 1) Returns an array with the type of peak desired for each species,
	//    appropriate for the mass assumption and the pt range. 
	// 2) Adds penalties for various undesirable situations which can occur during the fit.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Set fit ranges and fit functions.
	TArrayI* aout = 0x0;

	switch (fMassAssumption) {

		case 0:
			if (fPtMin < 0.7) {Int_t peakincluded[4] = {2,0,0,0}; aout = new TArrayI(4, peakincluded);}
			else if (fPtMin < 2.0) {Int_t peakincluded[4] = {2,2,1,0}; aout = new TArrayI(4, peakincluded);}	
			else {Int_t peakincluded[4] = {2,2,2,0}; aout = new TArrayI(4, peakincluded);}
		break;

		case 1:
			if (fPtMin < 0.7) {Int_t peakincluded[4] = {0,2,0,0}; aout = new TArrayI(4, peakincluded);}
			else if (fPtMin < 2.0) {Int_t peakincluded[4] = {2,2,0,0}; aout = new TArrayI(4, peakincluded);}	
			else {Int_t peakincluded[4] = {2,2,2,0}; aout = new TArrayI(4, peakincluded);}
		break;

		case 2:
			if (fPtMin < 1.2) {Int_t peakincluded[4] = {0,0,2,0}; aout = new TArrayI(4, peakincluded);}
			else if (fPtMin < 2.2) {Int_t peakincluded[4] = {0,2,2,0}; aout = new TArrayI(4, peakincluded);}	
			else if (fPtMin < 3.2) {Int_t peakincluded[4] = {0,2,2,1}; aout = new TArrayI(4, peakincluded);}	
			else if (fPtMin < 3.6) {Int_t peakincluded[4] = {1,2,2,1}; aout = new TArrayI(4, peakincluded);}	
			else {Int_t peakincluded[4] = {2,0,2,1}; aout = new TArrayI(4, peakincluded);}
		break;

		default:
			cout << Form("%s -> ERROR: %i is not a valid mass assumption.", __func__, fMassAssumption) << endl;
	}

	return aout;

}

// -----------------------------------------------------------------------
TArrayI* AliFitterTOFDiHadronPID::GetPeakDescriptionppDefault() {

	// 1) Returns an array with the type of peak desired for each species,
	//    appropriate for the mass assumption and the pt range. 
	// 2) Adds penalties for various undesirable situations which can occur during the fit.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Set fit ranges and fit functions.
	TArrayI* aout = 0x0;

	switch (fMassAssumption) {

		case 0:
			if (fPtMin < 0.7) {Int_t peakincluded[4] = {2,0,0,0}; aout = new TArrayI(4, peakincluded);}
			else if (fPtMin < 2.0) {Int_t peakincluded[4] = {2,2,2,0}; aout = new TArrayI(4, peakincluded);}	
			else {Int_t peakincluded[4] = {2,2,2,0}; aout = new TArrayI(4, peakincluded);}
		break;

		case 1:
			if (fPtMin < 0.7) {Int_t peakincluded[4] = {0,2,0,0}; aout = new TArrayI(4, peakincluded);}
			else if (fPtMin < 2.0) {Int_t peakincluded[4] = {2,2,0,0}; aout = new TArrayI(4, peakincluded);}	
			else {Int_t peakincluded[4] = {2,2,2,0}; aout = new TArrayI(4, peakincluded);}
		break;

		case 2:
			if (fPtMin < 1.2) {Int_t peakincluded[4] = {0,0,2,0}; aout = new TArrayI(4, peakincluded);}
			else if (fPtMin < 2.2) {Int_t peakincluded[4] = {0,2,2,0}; aout = new TArrayI(4, peakincluded);}	
			else if (fPtMin < 3.2) {Int_t peakincluded[4] = {0,2,2,1}; aout = new TArrayI(4, peakincluded);}	
			else if (fPtMin < 3.6) {Int_t peakincluded[4] = {1,2,2,1}; aout = new TArrayI(4, peakincluded);}	
			else {Int_t peakincluded[4] = {2,0,2,1}; aout = new TArrayI(4, peakincluded);}
		break;

		default:
			cout << Form("%s -> ERROR: %i is not a valid mass assumption.", __func__, fMassAssumption) << endl;
	}

	return aout;

}


// -----------------------------------------------------------------------
Int_t AliFitterTOFDiHadronPID::DoFitWithStrategyPbPbDefault(TObjArray* fitstages) {

	// Implementation of the default fit strategy of PbPb histograms.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Prelimenary estimates.
	EstimateTOFPositions();
	EstimateNMismatch(1); // for spectra
	EstimateNParticles();

	SetRangePbPbDefault();
	TArrayI* peakincluded = GetPeakDescriptionPbPbDefault();

	TCanvas* cFit = TCanvas::MakeDefCanvas();
	cFit->SetName("Fit");
	cFit->SetTitle("Fit");
	cFit->Divide(3,2);

	// 1) Initial estimates. Every included peak represented by a gaussian.
	for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies,1);
		}
	}

	PrintFit(cFit, 1);	

	// 2) Estimate the resolution and yield.
	SetFixNBgd(kTRUE);
	for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies,1);
			SetFixIntegral(iSpecies,kFALSE);
			SetFixMuTOF(iSpecies,kTRUE);
			SetFixSigmaTOF(iSpecies,kFALSE);			
		}
	}
	DoFit();

	PrintFit(cFit, 2);	

	// 3) Add tail, release only mu, integral - tail should be roughly ok.
	SetFixNBgd(kTRUE);
	for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies,peakincluded->At(iSpecies));
			SetFixIntegral(iSpecies,kFALSE);
			SetFixMuTOF(iSpecies,kFALSE);
			SetFixSigmaTOF(iSpecies,kTRUE);
			if (peakincluded->At(iSpecies) == 2) {SetFixTailTOF(iSpecies,kFALSE);}
		}
	}
	DoFit();

	PrintFit(cFit, 3);	

	// 4) For every species, release everything, but from right to left. Background fixed.
	SetFixNBgd(kTRUE);
	for (Int_t iSpecies = 3; iSpecies > -1; iSpecies--) {
		if (peakincluded->At(iSpecies)) {
			SetFixIntegral(iSpecies,kFALSE);
			SetFixMuTOF(iSpecies,kFALSE);
			SetFixSigmaTOF(iSpecies,kFALSE);
			if (peakincluded->At(iSpecies) == 2) {SetFixTailTOF(iSpecies,kFALSE);}
			DoFit();
		}
	}
	DoFit();

	PrintFit(cFit, 4);	

	// 5) Release everyhting except the background.
	SetFixNBgd(kTRUE);
	for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetFixIntegral(iSpecies,kFALSE);
			SetFixMuTOF(iSpecies,kTRUE);
			SetFixSigmaTOF(iSpecies,kFALSE);
			if (peakincluded->At(iSpecies) == 2) {SetFixTailTOF(iSpecies,kFALSE);}
		}
	}
	DoFit();

	PrintFit(cFit, 5);	
	
	// 6) Release everything.
	SetFixNBgd(kTRUE);	
	for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetFixIntegral(iSpecies,kFALSE);
			SetFixMuTOF(iSpecies,kFALSE);
			SetFixSigmaTOF(iSpecies,kFALSE);
			if (peakincluded->At(iSpecies) == 2) {SetFixTailTOF(iSpecies,kFALSE);}
		}
	}
	DoFit();

	PrintFit(cFit, 6);	

/*
	if (verbose||debug) {
		Double_t countdata = GetSumNChargedInFitRegion();
		Double_t countfit = GetIntegralOverFitFunctionInFitRegion();
		Double_t ratio = (countfit - countdata)/countdata;
		cout<<"Data Count: "<<countdata<<" Fit Count: "<<countfit<<" Ratio (fit-data)/data: "<<ratio<<endl;
	}
*/

	if (fitstages) {fitstages->AddLast(cFit->Clone());}
	delete cFit;

	return 0;

}

// -----------------------------------------------------------------------
Int_t AliFitterTOFDiHadronPID::DoFitWithStrategyppDefault(TObjArray* fitstages) {

	// Implementation of the default fit strategy of PbPb histograms.

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Prelimenary estimates.
	EstimateTOFPositions();
	EstimateNMismatch(1); // for spectra
	EstimateNParticles();

	SetRangeppDefault();
	TArrayI* peakincluded = GetPeakDescriptionPbPbDefault();

	TCanvas* cFit = TCanvas::MakeDefCanvas();
	cFit->SetName("Fit");
	cFit->SetTitle("Fit");
	cFit->Divide(3,2);

	// 1) Initial estimates. Every included peak represented by a gaussian.
	for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies,1);
		}
	}

	PrintFit(cFit, 1);	

	// 2) Estimate the resolution and yield.
	SetFixNBgd(kTRUE);
	for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies,1);
			SetFixIntegral(iSpecies,kFALSE);
			SetFixMuTOF(iSpecies,kTRUE);
			SetFixSigmaTOF(iSpecies,kFALSE);			
		}
	}
	DoFit();

	PrintFit(cFit, 2);	

	// 3) Add tail, release only mu, integral - tail should be roughly ok.
	SetFixNBgd(kTRUE);
	for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies,peakincluded->At(iSpecies));
			SetFixIntegral(iSpecies,kFALSE);
			SetFixMuTOF(iSpecies,kFALSE);
			SetFixSigmaTOF(iSpecies,kTRUE);
			if (peakincluded->At(iSpecies) == 2) {SetFixTailTOF(iSpecies,kFALSE);}
		}
	}
	DoFit();

	PrintFit(cFit, 3);	

	// 4) For every species, release everything, but from right to left. Background fixed.
	SetFixNBgd(kTRUE);
	for (Int_t iSpecies = 3; iSpecies > -1; iSpecies--) {
		if (peakincluded->At(iSpecies)) {
			SetFixIntegral(iSpecies,kFALSE);
			SetFixMuTOF(iSpecies,kFALSE);
			SetFixSigmaTOF(iSpecies,kFALSE);
			if (peakincluded->At(iSpecies) == 2) {SetFixTailTOF(iSpecies,kFALSE);}
			DoFit();
		}
	}
	DoFit();

	PrintFit(cFit, 4);	

	// 5) Release everyhting except the background.
	SetFixNBgd(kTRUE);
	for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetFixIntegral(iSpecies,kFALSE);
			SetFixMuTOF(iSpecies,kTRUE);
			SetFixSigmaTOF(iSpecies,kFALSE);
			if (peakincluded->At(iSpecies) == 2) {SetFixTailTOF(iSpecies,kFALSE);}
		}
	}
	DoFit();

	PrintFit(cFit, 5);	
	
	// 6) Release everything.
	SetFixNBgd(kTRUE);	
	for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetFixIntegral(iSpecies,kFALSE);
			SetFixMuTOF(iSpecies,kFALSE);
			SetFixSigmaTOF(iSpecies,kFALSE);
			if (peakincluded->At(iSpecies) == 2) {SetFixTailTOF(iSpecies,kFALSE);}
		}
	}
	DoFit();

	PrintFit(cFit, 6);	

/*
	if (verbose||debug) {
		Double_t countdata = GetSumOverFittedData();
		Double_t countfit = GetSumOverFit();
		Double_t ratio = (countfit - countdata)/countdata;
		cout<<"Data Count: "<<countdata<<" Fit Count: "<<countfit<<" Ratio (fit-data)/data: "<<ratio<<endl;
	}
*/

	if (fitstages) {fitstages->AddLast(cFit->Clone());}
	delete cFit;

	return 0;

}

// -----------------------------------------------------------------------
Int_t AliFitterTOFDiHadronPID::DoFitWithStrategyPbPbTweaked(TObjArray* fitstages) {

	// Implementation of the tweaked fit strategy of PbPb histograms.
	// -> Similar to the default for TOF/TPC PbPb.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Prelimenary estimates.
	EstimateTOFPositions();
	EstimateNMismatch(0);
	EstimateNParticles();

	SetRangePbPbDefault();
	TArrayI* peakincluded = GetPeakDescriptionPbPbDefault();

	TCanvas* cFit = TCanvas::MakeDefCanvas();
	cFit->SetName("Fit");
	cFit->SetTitle("Fit");
	cFit->Divide(3,2);

	// 1) Initial estimates. Every included peak represented by a gaussian.
	for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies,1);
		}
	}

	PrintFit(cFit, 1);	

	// 2) Fit the sigma, mu and integral for each particle separately, the tails are still fixed (3 dim fit).
	//    -> Method expects a reasonable estimate for the tail parameter.
	SetFixNBgd(kTRUE);
	for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			for (Int_t jSpecies = 0; jSpecies < 3; jSpecies++) {
				if (peakincluded->At(jSpecies)) {
					SetUsedFunction(jSpecies, peakincluded->At(jSpecies));
					SetFixIntegral(jSpecies,(iSpecies == jSpecies) ? kFALSE : kTRUE);
					SetFixMuTOF(jSpecies,(iSpecies == jSpecies) ? kFALSE : kTRUE);
					SetFixSigmaTOF(jSpecies,(iSpecies == jSpecies) ? kFALSE : kTRUE);
					if (peakincluded->At(jSpecies) == 2) {SetFixTailTOF(jSpecies, kTRUE);}					
				}
			}	
			DoFit();		
		}
	}

	PrintFit(cFit, 2);	

	// 3) Same as before, but now we release mu, sigma and integral of every peak simultaneously.
	SetFixNBgd(kTRUE);
	for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies, peakincluded->At(iSpecies));
			SetFixIntegral(iSpecies, kFALSE); 
			SetFixMuTOF(iSpecies, kFALSE);
			if (peakincluded->At(iSpecies) == 2) {SetFixTailTOF(iSpecies, kTRUE);}
			SetFixSigmaTOF(iSpecies, kFALSE);
		}
	}

	DoFit();

	PrintFit(cFit, 3);	

	// 4) Release Tails if applicable (from right to left in TOF).
	SetFixNBgd(kTRUE);
	for (Int_t iSpecies = 2; iSpecies >= 0; iSpecies--) {
		SetUsedFunction(iSpecies, peakincluded->At(iSpecies));

		// Release tails of one peak.
		if (peakincluded->At(iSpecies) == 2) {
			SetFixIntegral(iSpecies, kFALSE);
			SetFixMuTOF(iSpecies, kFALSE);
			SetFixSigmaTOF(iSpecies, kFALSE);
			SetFixTailTOF(iSpecies, kFALSE);

			// and fix all the rest.
			for (Int_t jSpecies = 0; jSpecies < 4; jSpecies++) {
				if (jSpecies == iSpecies) {continue;}
				SetFixIntegral(jSpecies,kTRUE);
				SetFixMuTOF(jSpecies,kTRUE);
				SetFixSigmaTOF(jSpecies,kTRUE);
				SetFixTailTOF(jSpecies,kTRUE);	
			}

			DoFit();

		}

	}


	PrintFit(cFit, 4);	

	// 5) Release Everything.
	SetFixNBgd(kTRUE);
	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies, peakincluded->At(iSpecies));
			SetFixIntegral(iSpecies,kFALSE);
			SetFixMuTOF(iSpecies,kFALSE);
			SetFixSigmaTOF(iSpecies,kFALSE);
			if (peakincluded->At(iSpecies) == 2) {SetFixTailTOF(iSpecies,kFALSE);}
		}
	}
	DoFit();

	PrintFit(cFit, 5);	

/*
	if (verbose||debug) {
		Double_t countdata = GetSumNChargedInFitRegion();
		Double_t countfit = GetIntegralOverFitFunctionInFitRegion();
		Double_t ratio = (countfit - countdata)/countdata;
		cout<<"Data Count: "<<countdata<<" Fit Count: "<<countfit<<" Ratio (fit-data)/data: "<<ratio<<endl;
	}
*/

	if (fitstages) {fitstages->AddLast(cFit->Clone());}
	delete cFit;

	return 0;

}
