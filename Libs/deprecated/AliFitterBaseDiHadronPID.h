#ifndef ALIFITTERBASEDIHADRONPID_H
#define ALIFITTERBASEDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include <iostream>
using namespace std;

#include "TObjArray.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TMath.h"
#include "TFile.h"

#include "Math/Minimizer.h"

#include "AliSpectrumDiHadronPID.h"

class AliFitterBaseDiHadronPID : public TObject {

public:
	AliFitterBaseDiHadronPID();

protected:
	virtual 			~AliFitterBaseDiHadronPID();
	
private:
	virtual void 		InitializeParameterArrays() = 0;					// Initialize paramter arrays (fParValues, etc.) to its default values.

public:
	virtual void	 	PrintFit(TCanvas*, Int_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}

	virtual Int_t		DoFitWithStrategyPbPb(Int_t, TObjArray*, AliSpectrumDiHadronPID*, const char*, Int_t, Bool_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0;}
	virtual Int_t		DoFitWithStrategypp(Int_t, TObjArray*, AliSpectrumDiHadronPID*, const char*, Int_t, Bool_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0;}

	// Misc. setters. 
	virtual void		SetHistogram(const TH1*) = 0;						// Input a data histogram.
	virtual void		SetMismatchedBackground(const TH1*) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}
	virtual void 		SetUsedFunction(Int_t, Int_t) = 0;		// Specify the function used to fit the peak of a certain particle species.

	// Methods for estimating various fit parameters.
	virtual void 		EstimateNParticles() {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}
	virtual void 		EstimateNMismatch(Int_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}

	// Getters for parameter indices (every parameter has an index in the Par values array, par error array, etc.)
	virtual Int_t 		GetNBgdParN() const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return -1;}
	virtual Int_t 		GetIntegralParN(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return -1;};					// Get the index of the yield parameter.
	virtual Int_t 		GetSigmaTOFParN(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return -1;}
	virtual Int_t 		GetMuTOFParN(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return -1;}
	virtual Int_t 		GetTailTOFParN(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return -1;}
	virtual Int_t 		GetSigmaTPCParN(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return -1;}
	virtual Int_t 		GetMuTPCParN(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return -1;}
	virtual Int_t 		GetTailTPCParN(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return -1;}

	// Getters for parameter values.
	virtual Double_t 	GetNBgd() const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}
	virtual Double_t 	GetIntegral(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}
	virtual Double_t 	GetSigmaTOF(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}
	virtual Double_t 	GetMuTOF(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}
	virtual Double_t 	GetTailTOF(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}
	virtual Double_t 	GetSigmaTPC(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}
	virtual Double_t 	GetMuTPC(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}
	virtual Double_t 	GetTailTPC(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}

	// Getters for paramter errors.
	virtual Double_t 	GetNBgdError() const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}
	virtual Double_t 	GetIntegralError(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}
	virtual Double_t 	GetSigmaTOFError(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}
	virtual Double_t 	GetMuTOFError(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}
	virtual Double_t 	GetTailTOFError(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}
	virtual Double_t 	GetSigmaTPCError(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}
	virtual Double_t 	GetMuTPCError(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}
	virtual Double_t 	GetTailTPCError(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}

	// Misc. getters.
	virtual Int_t 		GetNbinsInFit() const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0;}

	// TODO1: GetIntegral functions can also be const's in principle, however as of now they call UpdateFitDescription(), so they can't.
	// TODO2: Could add a member which gives the counts and integral within an NSigma region. 
	virtual Double_t	GetSumNChargedAllBins() const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}
	virtual Double_t	GetSumNChargedInFitRegion() const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}
	virtual Double_t	GetIntegralOverFitFunction() {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}
	virtual Double_t	GetIntegralOverFitFunctionInFitRegion() {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}

	// Replaced by the above functions.
	//virtual Double_t	GetSumOverFittedData() const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}
	//virtual Double_t	GetSumOverFit() {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}
	
	virtual TArrayI*	GetAllParsForSystematics() const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0x0;}
	virtual TArrayI*	GetUsedParsForSystematics(Int_t) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0x0;}

	// Setters for parameter values.
	virtual void 		SetNBgd(Double_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}
	virtual void 		SetIntegral(Int_t, Double_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}
	virtual void 		SetSigmaTOF(Int_t, Double_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}	
	virtual void 		SetMuTOF(Int_t, Double_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}
	virtual void 		SetTailTOF(Int_t, Double_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}
	virtual void 		SetSigmaTPC(Int_t, Double_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}	
	virtual void 		SetMuTPC(Int_t, Double_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}
	virtual void 		SetTailTPC(Int_t, Double_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}

	// Setters for fixing parameters.
	virtual void 		SetFixNBgd(Bool_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}
	virtual void 		SetFixIntegral(Int_t, Bool_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}
	virtual void 		SetFixSigmaTOF(Int_t, Bool_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}	
	virtual void 		SetFixMuTOF(Int_t, Bool_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}
	virtual void 		SetFixTailTOF(Int_t, Bool_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}
	virtual void 		SetFixSigmaTPC(Int_t, Bool_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}	
	virtual void 		SetFixMuTPC(Int_t, Bool_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}
	virtual void 		SetFixTailTPC(Int_t, Bool_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}

	// Setters for fit range.
	virtual void 		SetTOFRange(Double_t, Double_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}
	virtual void 		SetTPCRange(Double_t, Double_t) {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}

	virtual void		SetMergePiKaPeak() {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}

public:
	// Fitter interface (Methods implemented in the base class).
	Int_t 				DoFit();
	Double_t			DetermineYieldAfterVaryingPar(Int_t par, Bool_t varyup, Int_t strategy); 
	void				Reset();

	// Setters for content of instance's parameter arrays by index. Method will not work if for some reason the parameter arrays don't exist.
	void 				SetParValueAt(Double_t value, Int_t index);
	void				SetParErrorAt(Double_t error, Int_t index);
	void				SetParStepSizeAt(Double_t stepsize, Int_t index);
	void				SetIsParFixedAt(Bool_t isfixed, Int_t index);
	void				SetSmallVariationAt(Double_t smallvar, Int_t index);

	// Replaces the content of the instance's parameter arrays. It's not allowed to give an array which size doesn't match fNFitParams.
	void				SetParValues(const TArrayD* values);
	void 				SetParErrors(const TArrayD* errors);
	void				SetParStepSize(const TArrayD* stepsize);
	void				SetIsParFixed(const TArrayS* isfixed); 
	void				SetParSmallVariation(const TArrayD* smallvar);

	void 				SetPtRange(Double_t min, Double_t max);
	void				SetEtaRange(Double_t min, Double_t max);
	void 				SetMassAssumption(Int_t species) {fMassAssumption = species; fMassAssumptionIsSet = kTRUE;}
	void 				SetMinimizerType(const char* mintype) {fMinimizerType = mintype;}
	void 				SetMinimizerMethod(const char* method) {fMinimizerMethod = method;}
	void 				SetMaxFunctionCalls(Int_t maxfunctioncalls) {fMaxFunctionCalls = maxfunctioncalls;}
	void 				SetMaxIterations(Int_t maxiterations) {fMaxIterations = maxiterations;}
	void				SetTolerance(Double_t tolerance) {fTolerance = tolerance;}
	void 				SetDebugLevel(Int_t debuglevel) {fDebug = debuglevel;}
	void				SetPrintHessian(Bool_t printhessian = kTRUE) {fPrintHessian = printhessian;}
	void 				SetUseTailSlope(Bool_t usetailslope = kTRUE) {fUseTailSlope = usetailslope; InitializeParameterArrays();}

	void 				SetPeakSwapPenalty(Bool_t set = kTRUE, Int_t order = 3);
	void 				SetPeakDevPenalty(Bool_t set = kTRUE, Double_t TOFrange = 100., Double_t TPCrange = 1., Int_t order = 3);
	void				SetPeakSigmaPenalty(Bool_t set = kTRUE, Double_t TOFrange = 200., Double_t TPCrange = 10., Int_t order = 3);
	void 				SetNegIntPenalty(Bool_t set = kTRUE, Int_t order = 3);

	Double_t			GetTOFmin() const {return fTOFmin;}
	Double_t			GetTOFmax() const {return fTOFmax;}
	Double_t			GetTPCmin() const {return fTPCmin;}
	Double_t			GetTPCmax() const {return fTPCmax;}

	Int_t 				GetNumberOfParams() const {return fNFitParams;}		
	const TObjArray* 	GetParNames() const {return fParName;}			
	const TArrayD* 		GetParValues() const {return fParValue;}
	const TArrayD* 		GetParErrors() const {return fParError;}		
	const TArrayD* 		GetParStepSize() const {return fParStepSize;}		
	const TArrayS* 		GetIsParFixed() const {return fIsParFixed;}
	const TArrayD*		GetParSmallVariation() const {return fParSmallVariation;}		
	Int_t 				GetFitStatus() const {return fFitStatus;}
	Double_t 			GetMinPt() const {return fPtMin;}
	Double_t 			GetMaxPt() const {return fPtMax;}
	Double_t			GetMinEta() const {return fEtaMin;}
	Double_t			GetMaxEta() const {return fEtaMax;}
	Int_t 				GetMassAssumption() const {return fMassAssumption;}
	Int_t 				GetFitStrategy() const {return fFitStrategy;}
	Int_t 				GetMaxFunctionCalls() const {return fMaxFunctionCalls;}
	Int_t 				GetMaxIterations() const {return fMaxIterations;}
	Double_t 			GetTolerance() const {return fTolerance;}
	Int_t 				GetDebugLevel() const {return fDebug;}
	Bool_t				GetPrintHessian() const {return fPrintHessian;}
	Bool_t				GetUseTailSlope() const {return fUseTailSlope;}

	Double_t 			GetNData() const {return fData->GetEntries();}

	Double_t 			GetCurrentChiSquared() {return (Double_t)ChiSquared(fParValue->GetArray());}
	Double_t 			GetCurrentChiSquaredPerDOF() {return GetCurrentChiSquared()/((Double_t)GetNbinsInFit());}
	TH1F* 				GetChiSquaredOnePar(Int_t par, Double_t domain, Int_t Nbins = 100);
	TH2F* 				GetChiSquaredTwoPars(Int_t par1, Int_t par2, Double_t domain1, Double_t domain2, Int_t Nbins1 = 100, Int_t Nbins2 = 100);

	Double_t 			EstimateTOFPosition(Int_t species) const;		// Estimates the position of the peak on the TOF axis
	void  				EstimateTOFPositions();								
	Double_t 			EstimateTPCPosition(Int_t species) const;
	void 				EstimateTPCPositions();

	TCanvas* 			GetCloneOfCurrentCanvas() const;

	void 				PrintFitParams() const;		// Prints all fit parameters, plus all current values, errors, step size.

	void				Browse(TBrowser*);

private:
	// Private methods to be overloaded.
	virtual double 		ChiSquared(const double*) const {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return 0.;}
	virtual void 		UpdateFitDescription() {cout << Form("%s -> ERROR: Not implemented.",__func__) << endl; return;}

private:
	// Private methods (Methods implemented in the base class).
	void 				CreateCanvas(Bool_t withforce = kFALSE);		// (Re)creates the canvas.
	ROOT::Math::Minimizer* CreateMinimizer() const;

protected:
	// Histograms used in the fit.
	TH1*				fData;					// The Data histogram.
	TH1*				fMismatch;				// The Mismatch histogram.
	TObjArray*			fPeaksHisto;			// MC histograms which describe the peak.

	// Objects used to display the fit.
	TH1*				fFitResult;				// Histogram of the total fit result.
	TH1*				fMismatchScaled;		// Scaled version of the mismatch histogram.
	TObjArray*			fPeaksFunc;				// Histogram objects (TF1*) describing the four peaks.

	// Penalties.
	Bool_t 				fPeakSwapPenalty;
	Int_t				fPeakSwapPenaltyOrder;

	Bool_t				fPeakDevPenalty;
	Double_t			fPeakDevPenaltyTOFRange;
	Double_t			fPeakDevPenaltyTPCRange;	
	Int_t				fPeakDevPenaltyOrder;

	Bool_t				fPeakSigmaPenalty;
	Double_t 			fPeakSigmaPenaltyTOFRange;
	Double_t 			fPeakSigmaPenaltyTPCRange;
	Int_t				fPeakSigmaPenaltyOrder;

	Bool_t				fNegIntPenalty;
	Int_t				fNegIntPenaltyOrder;

	// Minimizer.
	TString 			fMinimizerType;			// Standard: Minuit2.
	TString				fMinimizerMethod;		// Standard: Migrad.
	Int_t 				fMaxFunctionCalls;
	Int_t 				fMaxIterations;
	Double_t 			fTolerance;
	Int_t				fFitStrategy;			// User implemented fit strategies.

	// Chi Squared Function.
	TArrayI*			fUsedFunction;  	 	// The function used to represent the species. 
	
	// Fit Parameters (All these should be set in the constructor of the derived class!)
	Int_t				fNFitParams;			// Number of fit paramters.
	TObjArray*			fParName;				// Names of the parameters (Array of TString objects).
	TArrayD*			fParValue;				// Values of the paramters.
	TArrayD*			fParError;				// Errors of the paramters.
	TArrayD*			fParStepSize;			// Stepsize of the paramters.
	TArrayS*			fIsParFixed;			// Is the parameter fixed, 1 = Yes, 0 = No.
	TArrayD*			fParSmallVariation;		// Small variation (used for systematics).
	Int_t				fFitStatus;				// Status of the fit. Updated after every fit.
	Int_t				fMinEntriesForFit;		// Minimum entries for a bin to be considered for the fit.

	// Fit Range.
	Double_t			fTOFmin;				// Fitting range. Automatically adapted
	Double_t			fTOFmax;				// to the histogram fed into the fitter,
	Double_t			fTPCmin;				// so no unrealistic values expected.
	Double_t			fTPCmax;

	// Kinematical variables.
	Double_t			fPtMin;
	Double_t			fPtMax;
	Double_t 			fEtaMin;
	Double_t 			fEtaMax;
	Bool_t 				fPtRangeIsSet;			// Essential for TOF/TPC peak position estimate.
	Int_t				fMassAssumption;
	Bool_t				fMassAssumptionIsSet;	// Essential for TOF/TPC peak position estimate.

	// Graphics.
	TCanvas*			fCanvas;

	// Misc. options.
	Bool_t				fUseTailSlope;			//

	// Debug.
	Int_t				fDebug;					// Debug level.
	Bool_t				fPrintHessian;			// DoFit() prints the hessian matrix after fit.

	ClassDef(AliFitterBaseDiHadronPID, 1);

};

#endif
