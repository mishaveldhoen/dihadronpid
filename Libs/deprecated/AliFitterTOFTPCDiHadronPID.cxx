/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  TPC/TOF fitter class.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "AliFitterTOFTPCDiHadronPID.h"

#include <iostream>
using namespace std;

#include "TCanvas.h"
#include "TString.h"
#include "TMath.h"

#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"

#include "TF1.h"
#include "TF2.h"
#include "TH1F.h"
#include "TH2F.h"

#include "TBox.h"
#include "TMarker.h"
#include "TEllipse.h"

#include "AliHistToolsDiHadronPID.h"
#include "AliFunctionsDiHadronPID.h"

ClassImp(AliFitterTOFTPCDiHadronPID);

// -----------------------------------------------------------------------
AliFitterTOFTPCDiHadronPID::AliFitterTOFTPCDiHadronPID() :
	AliFitterBaseDiHadronPID() 

{

	// Constructor.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Only consider bins in the fit that have 5 or more entries.
	fMinEntriesForFit = 5;

	const Int_t NFitParams = 29;

	// Creating arrays for fit paramters.
	fNFitParams = NFitParams;
	fParName = new TObjArray(fNFitParams);
	fParName->SetOwner(kTRUE);
	fParValue = new TArrayD(fNFitParams);
	fParError = new TArrayD(fNFitParams);
	fParStepSize = new TArrayD(fNFitParams);
	fIsParFixed = new TArrayS(fNFitParams);
	fParSmallVariation = new TArrayD(fNFitParams);

	InitializeParameterArrays();
	
}

// -----------------------------------------------------------------------
void AliFitterTOFTPCDiHadronPID::InitializeParameterArrays() {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Default values.
	const Int_t NFitParams = 29;
	const char* ParName[NFitParams] = {"Background",
										"NPions","sTOFPions","muTOFPions","tailTOFPions","sTPCPions","muTPCPions","tailTPCPions",
										"NKaons","sTOFKaons","muTOFKaons","tailTOFKaons","sTPCKaons","muTPCKaons","tailTPCKaons",
										"NProtons","sTOFProtons","muTOFProtons","tailTOFProtons","sTPCProtons","muTPCProtons","tailTPCProtons",
										"NDeuterons","sTOFDeuterons","muTOFDeuterons","tailTOFDeuterons","sTPCDeuterons","muTPCDeuterons","tailTPCDeuterons"};
	
	// TODO: distinguish between pp and PbPb inital estimates.
	Double_t defBgd = 1.;
	Double_t defIntegral = 1.;
	Double_t defSigmaTOF = 110.;
	Double_t defMuTOF = 0.;	// set later
	Double_t defTailTOF = (fUseTailSlope ? 0.008 : 100.);
	Double_t defSigmaTPC = 3.;
	Double_t defMuTPC = 0.; // set later
	Double_t defTailTPC = 10.;

	Double_t ParValue[NFitParams] = {defBgd,
									defIntegral, 105, defMuTOF, defTailTOF, defSigmaTPC, defMuTPC, defTailTPC,
									defIntegral, defSigmaTOF, defMuTOF, defTailTOF, defSigmaTPC, defMuTPC, defTailTPC,
									defIntegral, defSigmaTOF, defMuTOF, defTailTOF, defSigmaTPC, defMuTPC, defTailTPC,
									defIntegral, defSigmaTOF, defMuTOF, defTailTOF, defSigmaTPC, defMuTPC, defTailTPC};

	// Standard "small" values
	Double_t defBgdSmallVar = 1.;		// Has no meaning, just a place holder in the array.
	Double_t defIntegralSmallVar = 1.;	// Has no meaning, just a place holder in the array.
	Double_t defSigmaTOFSmallVar = 5.;
	Double_t defMuTOFSmallVar = 5.;
	//Double_t defTailTOFSmallVar = (fUseTailSlope ? 0.001 : 10.);
	Double_t defSigmaTPCSmallVar = 0.2;
	Double_t defMuTPCSmallVar = 0.2;
	Double_t defTailTPCSmallVar = 1.;	// NEVER USED!

	Double_t ParValueSmallVar[NFitParams] = {defBgdSmallVar,
									defIntegralSmallVar, defSigmaTOFSmallVar, defMuTOFSmallVar, 0.0002, defSigmaTPCSmallVar, defMuTPCSmallVar, defTailTPCSmallVar,
									defIntegralSmallVar, defSigmaTOFSmallVar, defMuTOFSmallVar, 0.001, defSigmaTPCSmallVar, defMuTPCSmallVar, defTailTPCSmallVar,
									defIntegralSmallVar, defSigmaTOFSmallVar, defMuTOFSmallVar, 0.0005, defSigmaTPCSmallVar, defMuTPCSmallVar, defTailTPCSmallVar,
									defIntegralSmallVar, defSigmaTOFSmallVar, defMuTOFSmallVar, 0.001, defSigmaTPCSmallVar, defMuTPCSmallVar, defTailTPCSmallVar};

	// Setting all fit paramters to global variables.
	for (Int_t iPar = 0; iPar < NFitParams; iPar++) {
		fParName->AddAt((TObject*)(new TString(ParName[iPar])), iPar);
		fParValue->SetAt(ParValue[iPar], iPar);
		fParSmallVariation->SetAt(ParValueSmallVar[iPar], iPar);		
		fParError->SetAt(0., iPar);
		fParStepSize->SetAt(1e-4, iPar);
		fIsParFixed->SetAt(kTRUE, iPar);

	}

}

// -----------------------------------------------------------------------
AliFitterTOFTPCDiHadronPID::~AliFitterTOFTPCDiHadronPID() {

	// Destructor.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

}

// -----------------------------------------------------------------------
void AliFitterTOFTPCDiHadronPID::PrintFit(TCanvas* cvs, Int_t pad) {

	// Prints the data on a log scale, and a box for the fit range.
	// Also dots are printed for 
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!cvs) {cout << Form("%s -> ERROR: No canvas supplied.",__func__) << endl; return;}
	TPad* CurrentPad = (TPad*)cvs->GetPad(pad);
	if (!CurrentPad) {cout << Form("%s -> ERROR: No invalid pad number.",__func__) << endl; return;}

	// Select the desired pad.
	cvs->cd(pad);
	CurrentPad->SetLogz();

	// Print the data.
	UpdateFitDescription();
	//const char* titletmp = fData->GetTitle();
	//fData->SetTitle(Form("%s (Step %i)", fData->GetTitle(), pad));
	fData->DrawCopy("colz");
	//fData->SetTitle(titletmp);

	// Print the fit region.
	TBox* fitregion = new TBox(GetTOFmin(), GetTPCmin(), GetTOFmax(), GetTPCmax());
	fitregion->SetLineColor(kBlack);
	fitregion->SetFillStyle(0);
	fitregion->SetLineWidth(2);
	fitregion->Draw("same");

	// Print the position of the peaks.
	Int_t usedmarkers[4] = {20, 21, 22, 23};
	Double_t markersize = 0.5;
	//TLegend* legend = new TLegend();
	for (Int_t ii = 0; ii < 4; ii++) {
		if (fPeaksFunc->At(ii)) {	
			
			// Draw markers.
			TMarker* marker = new TMarker(GetMuTOF(ii), GetMuTPC(ii), usedmarkers[ii]);
			marker->SetMarkerSize(markersize);
			marker->SetMarkerColor(kBlack);
			marker->Draw("same");

			// Draw 1 sigma contour.
			TEllipse* onesigma = new TEllipse(GetMuTOF(ii), GetMuTPC(ii), GetSigmaTOF(ii), GetSigmaTPC(ii));
			onesigma->SetLineColor(kBlack);
			onesigma->SetFillStyle(0);
			onesigma->SetLineWidth(2);
			onesigma->Draw("same");

		}
	}

	// Print a legend.

	/* TODO */

}

// -----------------------------------------------------------------------
void AliFitterTOFTPCDiHadronPID::PrintFitPull(TCanvas* cvs, Int_t pad) {

	// Prints the pull of the fit (data - fit) / error. 
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!cvs) {cout << Form("%s -> ERROR: No canvas supplied.",__func__) << endl; return;}
	TPad* CurrentPad = (TPad*)cvs->GetPad(pad);
	if (!CurrentPad) {cout << Form("%s -> ERROR: No invalid pad number.",__func__) << endl; return;}

	// Select the desired pad.
	cvs->cd(pad);
	UpdateFitDescription();

	TH2F* pull = (TH2F*)fData->Clone();
	pull->SetName("pull");
	pull->SetTitle(Form("%s (Step %i)", fData->GetTitle(), pad));
	pull->SetDirectory(0);

	TAxis* TOFaxis = pull->GetXaxis();
	TAxis* TPCaxis = pull->GetYaxis();

	for (Int_t iBinTOF = 0; iBinTOF < (pull->GetNbinsX() + 2); iBinTOF++) {
		for (Int_t iBinTPC = 0; iBinTPC < (pull->GetNbinsY() + 2); iBinTPC++) {
			Double_t fitval = fFitResult->GetBinContent(iBinTOF, iBinTPC);
			Double_t dataval = fData->GetBinContent(iBinTOF, iBinTPC);
			
			// Don't plot if out of fit range, or if bin has not enough entries.
			//cout << "(" << iBinTOF << "," << iBinTPC << ") ";
			if (dataval < fMinEntriesForFit || 
				iBinTOF < TOFaxis->FindBin(GetTOFmin()) ||
				iBinTOF > TOFaxis->FindBin(GetTOFmax()) ||
				iBinTPC < TPCaxis->FindBin(GetTPCmin()) ||
				iBinTPC > TPCaxis->FindBin(GetTPCmax()) ) {

				pull->SetBinContent(iBinTOF, iBinTPC, 0);
				pull->SetBinError(iBinTOF, iBinTPC, 0);
				//cout<< "OUT! ";
			} else {

				Double_t dataerr = fData->GetBinError(iBinTOF, iBinTPC);

				pull->SetBinContent(iBinTOF, iBinTPC, (dataval - fitval)/ dataerr); 
				pull->SetBinError(iBinTOF, iBinTPC, 0);
				//cout<< "IN! ";
			}	
			//cout << pull->GetBinContent(iBinTOF, iBinTPC) << endl;
		}		
	}

	pull->Draw("colz");

	// Print the fit region.
	TBox* fitregion = new TBox(GetTOFmin(), GetTPCmin(), GetTOFmax(), GetTPCmax());
	fitregion->SetLineColor(kBlack);
	fitregion->SetFillStyle(0);
	fitregion->SetLineWidth(2);
	fitregion->Draw("same");

	// Print the position of the peaks.
	Int_t usedmarkers[4] = {20, 21, 22, 23};
	Double_t markersize = 0.5;
	//TLegend* legend = new TLegend();
	for (Int_t ii = 0; ii < 4; ii++) {
		if (fPeaksFunc->At(ii)) {	
			
			// Draw markers.
			TMarker* marker = new TMarker(GetMuTOF(ii), GetMuTPC(ii), usedmarkers[ii]);
			marker->SetMarkerSize(markersize);
			marker->SetMarkerColor(kBlack);
			marker->Draw("same");

			// Draw 1 sigma contour.
			TEllipse* onesigma = new TEllipse(GetMuTOF(ii), GetMuTPC(ii), GetSigmaTOF(ii), GetSigmaTPC(ii));
			onesigma->SetLineColor(kBlack);
			onesigma->SetFillStyle(0);
			onesigma->SetLineWidth(2);
			onesigma->Draw("same");

		}
	}
	
}

// -----------------------------------------------------------------------
void AliFitterTOFTPCDiHadronPID::PrintFitTOFProjection(TCanvas* cvs, 
	const Int_t pad, const Int_t TPCbinMin, const Int_t TPCbinMax) {

	// Prints a slice in the TOF direction.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!cvs) {cout << Form("%s -> ERROR: No canvas supplied.",__func__) << endl; return;}
	TPad* CurrentPad = (TPad*)cvs->GetPad(pad);
	if (!CurrentPad) {cout << Form("%s -> ERROR: No invalid pad number.",__func__) << endl; return;}

	// Select the desired pad.
	cvs->cd(pad);
	CurrentPad->SetLogy();

	// Print Data Projection.
	TH1F* data_proj = (TH1F*)((TH2F*)fData)->ProjectionX(Form("%s_%i",fData->GetName(), pad), TPCbinMin, TPCbinMax);
	data_proj->SetDirectory(0);
	data_proj->SetLineWidth(2);
	data_proj->SetTitle(Form("%s (Step %i)", fData->GetTitle(), pad));
	data_proj->DrawCopy();

	UpdateFitDescription();

	// Print Fit Result Projection.
	if (fFitResult) {
		TH1F* fitresult_proj = (TH1F*)((TH2F*)fFitResult)->ProjectionX(Form("%s_%i",fFitResult->GetName(), pad), TPCbinMin, TPCbinMax);
		fitresult_proj->SetDirectory(0);
		fitresult_proj->SetLineWidth(2);
		fitresult_proj->DrawCopy("same");
	}

	for (Int_t ii = 0; ii < 4; ii++) {
		if (fPeaksFunc->At(ii)) {
			TH2F* hist = AliHistToolsDiHadronPID::Function2DToHist2D((TF2*)fPeaksFunc->At(ii), (TH2F*)fData);
			TH1F* hist_proj = (TH1F*)((TH2F*)hist)->ProjectionX(Form("%s_%i",hist->GetName(), pad), TPCbinMin, TPCbinMax);
			hist_proj->SetLineWidth(2);
			hist_proj->DrawCopy("same");
			delete hist;
		}
	}

	// Print Mismatch projection.
	if (fMismatchScaled) {
		TH1F* mismatchscaled_proj = (TH1F*)((TH2F*)fMismatchScaled)->ProjectionX(Form("%s_%i",fMismatchScaled->GetName(), pad), TPCbinMin, TPCbinMax);
		mismatchscaled_proj->SetDirectory(0);
		mismatchscaled_proj->SetLineWidth(2);
		mismatchscaled_proj->DrawCopy("same");
	}

}

// -----------------------------------------------------------------------
void AliFitterTOFTPCDiHadronPID::PrintFitTPCProjection(TCanvas* cvs, Int_t pad, Int_t TOFbinMin, Int_t TOFbinMax) {

	// Prints a slice in the TPC direction.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!cvs) {cout << Form("%s -> ERROR: No canvas supplied.",__func__) << endl; return;}
	TPad* CurrentPad = (TPad*)cvs->GetPad(pad);
	if (!CurrentPad) {cout << Form("%s -> ERROR: No invalid pad number.",__func__) << endl; return;}

	// Select the desired pad.
	cvs->cd(pad);
	CurrentPad->SetLogy();

	// Print Data Projection.
	TH1F* data_proj = (TH1F*)((TH2F*)fData)->ProjectionY(Form("%s_%i",fData->GetName(), pad), TOFbinMin, TOFbinMax);
	data_proj->SetDirectory(0);
	data_proj->SetLineWidth(2);
	data_proj->SetTitle(Form("%s (Step %i)", fData->GetTitle(), pad));	
	data_proj->DrawCopy();

	UpdateFitDescription();

	// Print Fit Result Projection.
	if (fFitResult) {
		TH1F* fitresult_proj = (TH1F*)((TH2F*)fFitResult)->ProjectionY(Form("%s_%i",fFitResult->GetName(), pad), TOFbinMin, TOFbinMax);
		fitresult_proj->SetDirectory(0);
		fitresult_proj->SetLineWidth(2);
		fitresult_proj->DrawCopy("same");
	}

	for (Int_t ii = 0; ii < 4; ii++) {
		if (fPeaksFunc->At(ii)) {
			TH2F* hist = AliHistToolsDiHadronPID::Function2DToHist2D((TF2*)fPeaksFunc->At(ii), (TH2F*)fData);
			TH1F* hist_proj = (TH1F*)((TH2F*)hist)->ProjectionY(Form("%s_%i",hist->GetName(), pad), TOFbinMin, TOFbinMax);
			hist_proj->SetLineWidth(2);
			hist_proj->DrawCopy("same");
			delete hist;
		}
	}

	// Print Mismatch projection.
	if (fMismatchScaled) {
		TH1F* mismatchscaled_proj = (TH1F*)((TH2F*)fMismatchScaled)->ProjectionY(Form("%s_%i",fMismatchScaled->GetName(), pad), TOFbinMin, TOFbinMax);
		mismatchscaled_proj->SetDirectory(0);
		mismatchscaled_proj->SetLineWidth(2);
		mismatchscaled_proj->DrawCopy("same");
	}

}

// -----------------------------------------------------------------------
Int_t AliFitterTOFTPCDiHadronPID::DoFitWithStrategyPbPb(
	Int_t strategy, TObjArray* fitstages, AliSpectrumDiHadronPID* /*spectrum*/,
	const char* /*vary_par*/, Int_t /*vary_species*/, Bool_t /*vary_updown*/) {

	// Fit Strategies:
	//  0 -> No parameters fixed, optimized for inclusive spectra.
	//  1 -> No parameters fixed, optimized for di-hadron spectra in PbPb, "long" TOF axis.
	//  2 -> Same as 1, but with parameterized tailTOF for kaon and proton.
	//  3 -> Same as 2, but now also with parameterized tailTOF for pions.

	// Performs fit according to a standardized strategy.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	switch (strategy) {
		case 0:
			return DoFitWithStrategyPbPbDefault(fitstages, 1); // Fixed mismatch, determined in sideband, optimized for very large TOF axis.
		break;
		case 1:
			return DoFitWithStrategyPbPbDefault(fitstages, 2); // Fixed mismatch, determined in sideband, optimized for di-hadron analysis (shorter TOF axis).
		break;
		{case 2: 
			TArrayI* pars = new TArrayI(fNFitParams);		   // initializes to 0;
			switch (fMassAssumption) {
				case 0:
					pars->SetAt(kTRUE, GetTailTOFParN(1));
					pars->SetAt(kTRUE, GetTailTOFParN(2));					
					break;
				case 1:
					pars->SetAt(kTRUE, GetTailTOFParN(1));
					pars->SetAt(kTRUE, GetTailTOFParN(2));	
					break;
				case 2:
					pars->SetAt(kTRUE, GetTailTOFParN(1));
					pars->SetAt(kTRUE, GetTailTOFParN(2));	
					break;
			}

			Int_t fitstatus = DoFitWithStrategyPbPbDefault(fitstages, 2, pars); // Fixed mismatch, sideband, di-hadron.
			delete pars; pars = 0x0;
			return fitstatus;
		
		break;}	
		{case 3: 
			TArrayI* pars = new TArrayI(fNFitParams);		   // initializes to 0;
			switch (fMassAssumption) {
				case 0:
					pars->SetAt(kTRUE, GetTailTOFParN(0));					
					pars->SetAt(kTRUE, GetTailTOFParN(1));
					pars->SetAt(kTRUE, GetTailTOFParN(2));							
					break;
				case 1:
					pars->SetAt(kTRUE, GetTailTOFParN(0));				
					pars->SetAt(kTRUE, GetTailTOFParN(1));
					pars->SetAt(kTRUE, GetTailTOFParN(2));	
					break;
				case 2:
					pars->SetAt(kTRUE, GetTailTOFParN(0));				
					pars->SetAt(kTRUE, GetTailTOFParN(1));
					pars->SetAt(kTRUE, GetTailTOFParN(2));	
					break;
			}

			Int_t fitstatus = DoFitWithStrategyPbPbDefault(fitstages, 2, pars); // Fixed mismatch, sideband, di-hadron.
			delete pars; pars = 0x0;
			return fitstatus;
		
		break;}	
		{case 4: 
			TArrayI* pars = new TArrayI(fNFitParams);		   // Same as strategy 0 (i.e., long TOF axis, but with fixed par's)
			switch (fMassAssumption) {
				case 0:
					pars->SetAt(kTRUE, GetTailTOFParN(1));
					pars->SetAt(kTRUE, GetTailTOFParN(2));					
					break;
				case 1:
					pars->SetAt(kTRUE, GetTailTOFParN(1));
					pars->SetAt(kTRUE, GetTailTOFParN(2));	
					break;
				case 2:
					pars->SetAt(kTRUE, GetTailTOFParN(1));
					pars->SetAt(kTRUE, GetTailTOFParN(2));	
					break;
			}

			Int_t fitstatus = DoFitWithStrategyPbPbDefault(fitstages, 1, pars); // Fixed mismatch, sideband, di-hadron.
			delete pars; pars = 0x0;
			return fitstatus;
		
		break;}			

		default:
			cout << Form("%s -> ERROR: Unknown strategy.",__func__) << endl;
	}

	return 0;

}

// -----------------------------------------------------------------------
Int_t AliFitterTOFTPCDiHadronPID::DoFitWithStrategypp(
	Int_t strategy, TObjArray* fitstages, AliSpectrumDiHadronPID* /*spectrum*/,
	const char* /*vary_par*/, Int_t /*vary_species*/, Bool_t /*vary_updown*/) {

	// In the constrained fit one can choose to increase or decrease parameter values somewhat for
	// systematic checks by using vary_par, vary_species, vary_updown.


	// Performs fit according to a standardized strategy.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	switch (strategy) {
		case 0:
			// return DoFitWithStrategyppDefault(fitstages, 1); // spectra
			return DoFitWithStrategyppFixedTOFTail(fitstages, 1, kFALSE, kTRUE, kTRUE); // spectra, fixed TOF tail for ka, pr and mismatches estimated.
		break;
		case 1:
			// return DoFitWithStrategyppDefault(fitstages, -1); // dihadron
			return DoFitWithStrategyppDefault(fitstages, -1); // dihadron, all tails free
		break;
		case 2:
			return DoFitWithStrategyppFixedTOFTail(fitstages, -1, kFALSE, kTRUE, kTRUE); // dihadron, fixed TOF tail for ka, pr
		break;
		case 3:
			return DoFitWithStrategyppFixedTOFTail(fitstages, -1, kTRUE, kTRUE, kTRUE); // dihadron, fixed TOF tail for pi, ka, pr
		break;
		case 4:
			return DoFitWithStrategyppFixedTOFTail(fitstages, -1, kFALSE, kFALSE, kFALSE); // TEMP
		break;

		default:
			cout << Form("%s -> ERROR: Unknown strategy.",__func__) << endl;
	}

	return 0;

}

// -----------------------------------------------------------------------
void AliFitterTOFTPCDiHadronPID::SetHistogram(const TH1* hist) {

	// Clones hist to data member fData
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!hist) {cout << Form("%s -> ERROR: No histogram supplied.",__func__) << endl; return;}

	// Check if the histogram is of the right kind.
	TString classname(hist->ClassName());
	if (!classname.Contains("TH2")) {cout << Form("%s -> ERROR: I don't accept a histogram of type: %s.",__func__, classname.Data()) << endl; return;}

	fData = (TH2F*)hist->Clone();
	fData->SetDirectory(0);

	// Automatically set the TOF/TPC fit range to the range of the TOF/TPC axis.
	TAxis* TOFaxis = fData->GetXaxis();
	SetTOFRange(TOFaxis->GetBinLowEdge(1), TOFaxis->GetBinUpEdge(TOFaxis->GetNbins()));

	TAxis* TPCaxis = fData->GetYaxis();
	SetTPCRange(TPCaxis->GetBinLowEdge(1), TPCaxis->GetBinUpEdge(TPCaxis->GetNbins()));

}

// -----------------------------------------------------------------------
void AliFitterTOFTPCDiHadronPID::SetMismatchedBackground(const TH1* hist) {

	// Clones hist to data member fMismatch
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!hist) {cout << Form("%s -> ERROR: No histogram supplied.",__func__) << endl; return;}

	// Check if the histogram is of the right kind.
	TString classname(hist->ClassName());
	if (!classname.Contains("TH2")) {cout << Form("%s -> ERROR: I don't accept a histogram of type: %s.",__func__, classname.Data()) << endl; return;}

	fMismatch = (TH2F*)hist->Clone();
	fMismatch->SetDirectory(0);

	// Include the background in the fit.
	SetFixNBgd(kFALSE);

}

// -----------------------------------------------------------------------
void AliFitterTOFTPCDiHadronPID::SetUsedFunction(Int_t species, Int_t function) {

	// Implemented function types:
	// 0 - no function;
	// 1 - Gaussian2D;
	// 2 - Gaussian2D + TailTOF;
	// 3 - Gaussian2D + TailTPC;
	// 4 - Gaussian2D + Tail TPC/TOF;
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if ( (species < 0) || (species > 3)) {cout << Form("%s -> ERROR: Not a valid species.",__func__) << endl; return;}

	fUsedFunction->SetAt(function, species);

	switch (function) {
		case 0:
			SetFixIntegral(species, kTRUE);
			SetFixSigmaTOF(species, kTRUE);
			SetFixMuTOF(species, kTRUE);
			SetFixTailTOF(species, kTRUE);
			SetFixSigmaTPC(species, kTRUE);
			SetFixMuTPC(species, kTRUE);
			SetFixTailTPC(species, kTRUE);
			break;
		case 1:
			SetFixIntegral(species, kFALSE);
			SetFixSigmaTOF(species, kFALSE);
			SetFixMuTOF(species, kFALSE);
			SetFixTailTOF(species, kTRUE);
			SetFixSigmaTPC(species, kFALSE);
			SetFixMuTPC(species, kFALSE);
			SetFixTailTPC(species, kTRUE);			
			break;
		case 2:
			SetFixIntegral(species, kFALSE);
			SetFixSigmaTOF(species, kFALSE);
			SetFixMuTOF(species, kFALSE);
			SetFixTailTOF(species, kFALSE);
			SetFixSigmaTPC(species, kFALSE);
			SetFixMuTPC(species, kFALSE);
			SetFixTailTPC(species, kTRUE);
			break;
		case 3:
			SetFixIntegral(species, kFALSE);
			SetFixSigmaTOF(species, kFALSE);
			SetFixMuTOF(species, kFALSE);
			SetFixTailTOF(species, kTRUE);
			SetFixSigmaTPC(species, kFALSE);
			SetFixMuTPC(species, kFALSE);
			SetFixTailTPC(species, kFALSE);
			break;
		case 4:
			SetFixIntegral(species, kFALSE);
			SetFixSigmaTOF(species, kFALSE);
			SetFixMuTOF(species, kFALSE);
			SetFixTailTOF(species, kFALSE);
			SetFixSigmaTPC(species, kFALSE);
			SetFixMuTPC(species, kFALSE);
			SetFixTailTPC(species, kFALSE);
			break;		
		default:
			cout << Form("%s -> ERROR: Requested function not implemented.",__func__) << endl;
	}

}

// -----------------------------------------------------------------------
void AliFitterTOFTPCDiHadronPID::EstimateNParticles() {

	// Given that the values for mu and sigma are reasonable, a simple estimate for
	// the number of particles in a peak is given by (N_{central bin} * 2 sigma)/bin width. 
	// NOTE: Approximation is quite crude.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fData) {cout<<"ERROR: First set a data histogram..."<<endl; return;}
	
	TAxis* TOFaxis = fData->GetXaxis();
	TAxis* TPCaxis = fData->GetYaxis();

	// Assuming fixed bin size.
	Double_t binarea = (TOFaxis->GetBinWidth(1)) * (TPCaxis->GetBinWidth(1));

	// Find the corresponding bins to the peaks.
	for (Int_t ii = 0; ii < 4; ii++) {

		Int_t peakbinTOF = TOFaxis->FindBin(GetMuTOF(ii));
		Int_t peakbinTPC = TPCaxis->FindBin(GetMuTPC(ii));

		Double_t peakheight = fData->GetBinContent(peakbinTOF, peakbinTPC);

		Double_t integralestimate = (2 * TMath::Pi() * GetSigmaTOF(ii) * GetSigmaTPC(ii) * peakheight) / binarea;
		
		SetIntegral(ii, integralestimate);
	}

}

// -----------------------------------------------------------------------
void AliFitterTOFTPCDiHadronPID::EstimateNMismatch(Int_t option) {

	// option = 0 -> estimate background by looking at the final three bins.
	// option = 1 -> estimate background in a predefined range in TOF. (optimized for the TOF axes in PID histos of track cuts object)
	// option = 2 -> estimate background in a predefined range in TOF. (optimized for "LargeTOFaxis" size of DiHadronPID data)

	// Estimates the number of mismatches.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}


	const Int_t NbinsTOF = fData->GetNbinsX();
	const Int_t NbinsTPC = fData->GetNbinsY();

	Int_t minTOFbin = NbinsTOF - 3;
	Int_t maxTOFbin = NbinsTOF;

	TAxis* TOFaxis = fData->GetXaxis();
	if (option != 0) {

		Double_t ptmean = (fPtMax + fPtMin) / 2.;
		
		/* TODO: Don't use NbinsTOF explicitly. */
		//cout<<"ptmean: "<<ptmean<<endl;

		switch (fMassAssumption) {

			// Mass assumption: Pions.
			case 0:
			
				switch (option) {
					case 1:
						// Tuned to the spectra data.
						if (ptmean < 0.60) {minTOFbin = TOFaxis->FindBin(1500.); maxTOFbin = TOFaxis->FindBin(2100.);}
						if (ptmean > 0.65 && ptmean < 0.80) {minTOFbin = TOFaxis->FindBin(4000.); maxTOFbin = TOFaxis->FindBin(6000.);}
						if (ptmean > 0.80 && ptmean < 0.90) {minTOFbin = TOFaxis->FindBin(3000.); maxTOFbin = TOFaxis->FindBin(4500.);}
						if (ptmean > 0.90 && ptmean < 1.00) {minTOFbin = TOFaxis->FindBin(6000.); maxTOFbin = TOFaxis->FindBin(8000.);}
						if (ptmean > 1.00 && ptmean < 1.10) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = TOFaxis->FindBin(3000.);}
						if (ptmean > 1.10 && ptmean < 1.50) {minTOFbin = TOFaxis->FindBin(4500.); maxTOFbin = TOFaxis->FindBin(6000.);}
						if (ptmean > 1.50 && ptmean < 2.00) {minTOFbin = TOFaxis->FindBin(3000.); maxTOFbin = TOFaxis->FindBin(3500.);}
						if (ptmean > 2.00 && ptmean < 2.30) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = TOFaxis->FindBin(3000.);}
						if (ptmean > 2.30 && ptmean < 2.50) {minTOFbin = TOFaxis->FindBin(5000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.50 && ptmean < 3.00) {minTOFbin = TOFaxis->FindBin(4000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.00 && ptmean < 3.40) {minTOFbin = TOFaxis->FindBin(3000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.40 && ptmean < 5.00) {minTOFbin = TOFaxis->FindBin(2300.); maxTOFbin = NbinsTOF;}
					break;

					case 2:
						// Tuned to the DiHadron Data.
						if (ptmean > 1.0 && ptmean < 1.1) {minTOFbin = TOFaxis->FindBin(6200.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.1 && ptmean < 1.2) {minTOFbin = TOFaxis->FindBin(5000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.2 && ptmean < 1.3) {minTOFbin = TOFaxis->FindBin(4500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.3 && ptmean < 1.4) {minTOFbin = TOFaxis->FindBin(4000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.4 && ptmean < 1.5) {minTOFbin = TOFaxis->FindBin(3500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.5 && ptmean < 1.6) {minTOFbin = TOFaxis->FindBin(3200.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.6 && ptmean < 1.7) {minTOFbin = TOFaxis->FindBin(3000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.7 && ptmean < 1.8) {minTOFbin = TOFaxis->FindBin(2700.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.8 && ptmean < 1.9) {minTOFbin = TOFaxis->FindBin(2500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.9 && ptmean < 2.0) {minTOFbin = TOFaxis->FindBin(2200.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.0 && ptmean < 2.1) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.1 && ptmean < 2.2) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.2 && ptmean < 2.3) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.3 && ptmean < 2.4) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = TOFaxis->FindBin(3000.);}
						if (ptmean > 2.4 && ptmean < 2.5) {minTOFbin = TOFaxis->FindBin(1800.); maxTOFbin = TOFaxis->FindBin(2400.);}
						if (ptmean > 2.5 && ptmean < 2.6) {minTOFbin = TOFaxis->FindBin(1600.); maxTOFbin = TOFaxis->FindBin(2100.);}
						if (ptmean > 2.6 && ptmean < 2.8) {minTOFbin = TOFaxis->FindBin(1500.); maxTOFbin = TOFaxis->FindBin(2000.);}
						if (ptmean > 2.8 && ptmean < 3.0) {minTOFbin = TOFaxis->FindBin(1400.); maxTOFbin = TOFaxis->FindBin(1900.);}
						if (ptmean > 3.0 && ptmean < 3.2) {minTOFbin = TOFaxis->FindBin(1300.); maxTOFbin = TOFaxis->FindBin(1600.);}
						if (ptmean > 3.2 && ptmean < 3.4) {minTOFbin = TOFaxis->FindBin(2300.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.4 && ptmean < 3.6) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.6 && ptmean < 3.8) {minTOFbin = TOFaxis->FindBin(1800.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.8 && ptmean < 4.2) {minTOFbin = TOFaxis->FindBin(1800.); maxTOFbin = NbinsTOF;}
						if (ptmean > 4.2 && ptmean < 4.6) {minTOFbin = TOFaxis->FindBin(1500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 4.6 && ptmean < 5.0) {minTOFbin = TOFaxis->FindBin(1200.); maxTOFbin = NbinsTOF;}
					break;

					default:
						cout << Form("%s -> ERROR: Invalid option",__func__) << endl;

					}

			break;

			// Mass assumption: Kaons.
			case 1:	
				switch (option) {
					case 1:
						// Tuned to the spectra data.		
						if (ptmean < 0.70) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = TOFaxis->FindBin(4000.);}
						if (ptmean > 0.70 && ptmean < 0.90) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = TOFaxis->FindBin(2500.);}
						if (ptmean > 0.90 && ptmean < 1.00) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = TOFaxis->FindBin(2000.);}						
						if (ptmean > 1.00 && ptmean < 1.10) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = TOFaxis->FindBin(1600.);}	
						if (ptmean > 1.10 && ptmean < 1.50) {minTOFbin = TOFaxis->FindBin(4000.); maxTOFbin = TOFaxis->FindBin(6000.);}	
						if (ptmean > 1.50 && ptmean < 1.80) {minTOFbin = TOFaxis->FindBin(2500.); maxTOFbin = TOFaxis->FindBin(4000.);}
						if (ptmean > 1.80 && ptmean < 2.10) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = TOFaxis->FindBin(3000.);}
						if (ptmean > 2.10 && ptmean < 2.30) {minTOFbin = TOFaxis->FindBin(1800.); maxTOFbin = TOFaxis->FindBin(2500.);}
						if (ptmean > 2.30 && ptmean < 3.00) {minTOFbin = TOFaxis->FindBin(4000.); maxTOFbin = TOFaxis->FindBin(6000.);}
						if (ptmean > 3.00 && ptmean < 3.60) {minTOFbin = TOFaxis->FindBin(2500.); maxTOFbin = TOFaxis->FindBin(4000.);}
						if (ptmean > 3.60 && ptmean < 4.20) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = TOFaxis->FindBin(4000.);}
						if (ptmean > 4.20 && ptmean < 5.00) {minTOFbin = TOFaxis->FindBin(1500.); maxTOFbin = TOFaxis->FindBin(4000.);}
					break;
					case 2:
						if (ptmean > 1.0 && ptmean < 1.1) {minTOFbin = TOFaxis->FindBin(5000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.1 && ptmean < 1.2) {minTOFbin = TOFaxis->FindBin(4500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.2 && ptmean < 1.3) {minTOFbin = TOFaxis->FindBin(4000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.3 && ptmean < 1.4) {minTOFbin = TOFaxis->FindBin(3500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.4 && ptmean < 1.5) {minTOFbin = TOFaxis->FindBin(3500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.5 && ptmean < 1.6) {minTOFbin = TOFaxis->FindBin(3000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.6 && ptmean < 1.7) {minTOFbin = TOFaxis->FindBin(3000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.7 && ptmean < 1.8) {minTOFbin = TOFaxis->FindBin(3000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.8 && ptmean < 1.9) {minTOFbin = TOFaxis->FindBin(2500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.9 && ptmean < 2.0) {minTOFbin = TOFaxis->FindBin(2500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.0 && ptmean < 2.1) {minTOFbin = TOFaxis->FindBin(2200.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.1 && ptmean < 2.2) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = TOFaxis->FindBin(3000.);}
						if (ptmean > 2.2 && ptmean < 2.3) {minTOFbin = TOFaxis->FindBin(1900.); maxTOFbin = TOFaxis->FindBin(2500.);}
						if (ptmean > 2.3 && ptmean < 2.4) {minTOFbin = TOFaxis->FindBin(1800.); maxTOFbin = TOFaxis->FindBin(2400.);}
						if (ptmean > 2.4 && ptmean < 2.5) {minTOFbin = TOFaxis->FindBin(1700.); maxTOFbin = TOFaxis->FindBin(2200.);}
						if (ptmean > 2.5 && ptmean < 2.6) {minTOFbin = TOFaxis->FindBin(1600.); maxTOFbin = TOFaxis->FindBin(2000.);}
						if (ptmean > 2.6 && ptmean < 2.8) {minTOFbin = TOFaxis->FindBin(1500.); maxTOFbin = TOFaxis->FindBin(1800.);}
						if (ptmean > 2.8 && ptmean < 3.0) {minTOFbin = TOFaxis->FindBin(2500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.0 && ptmean < 3.2) {minTOFbin = TOFaxis->FindBin(2200.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.2 && ptmean < 3.4) {minTOFbin = TOFaxis->FindBin(2100.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.4 && ptmean < 3.6) {minTOFbin = TOFaxis->FindBin(1900.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.6 && ptmean < 3.8) {minTOFbin = TOFaxis->FindBin(1800.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.8 && ptmean < 4.2) {minTOFbin = TOFaxis->FindBin(1700.); maxTOFbin = NbinsTOF;}
						if (ptmean > 4.2 && ptmean < 4.6) {minTOFbin = TOFaxis->FindBin(1500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 4.6 && ptmean < 5.0) {minTOFbin = TOFaxis->FindBin(1300.); maxTOFbin = NbinsTOF;}

					break;
					default:
						cout << Form("%s -> ERROR: Invalid option",__func__) << endl;
				}

			break;

			// Mass assumption: Protons.
			case 2:
				switch (option) {
					case 1:
						if (ptmean < 1.00) {minTOFbin = TOFaxis->FindBin(3000.); maxTOFbin = TOFaxis->FindBin(6000.);}
						if (ptmean > 1.00 && ptmean < 1.50) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = TOFaxis->FindBin(6000.);}						
						if (ptmean > 1.50 && ptmean < 2.00) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = TOFaxis->FindBin(2400.);}	
						if (ptmean > 2.00 && ptmean < 2.50) {minTOFbin = TOFaxis->FindBin(4000.); maxTOFbin = TOFaxis->FindBin(6000.);}
						if (ptmean > 2.50 && ptmean < 3.00) {minTOFbin = TOFaxis->FindBin(3000.); maxTOFbin = TOFaxis->FindBin(6000.);}	
						if (ptmean > 3.00 && ptmean < 5.00) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = TOFaxis->FindBin(6000.);}	
					break;

					case 2:
						if (ptmean > 1.0 && ptmean < 1.1) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.1 && ptmean < 1.2) {minTOFbin = TOFaxis->FindBin(2000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.2 && ptmean < 1.3) {minTOFbin = TOFaxis->FindBin(1700.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.3 && ptmean < 1.4) {minTOFbin = TOFaxis->FindBin(1500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.4 && ptmean < 1.5) {minTOFbin = TOFaxis->FindBin(1500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.5 && ptmean < 1.6) {minTOFbin = TOFaxis->FindBin(1500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.6 && ptmean < 1.7) {minTOFbin = TOFaxis->FindBin(1200.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.7 && ptmean < 1.8) {minTOFbin = TOFaxis->FindBin(1200.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.8 && ptmean < 1.9) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 1.9 && ptmean < 2.0) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.0 && ptmean < 2.1) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.1 && ptmean < 2.2) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.2 && ptmean < 2.3) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.3 && ptmean < 2.4) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = NbinsTOF;}
						if (ptmean > 2.4 && ptmean < 2.5) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = TOFaxis->FindBin(1800.);}
						if (ptmean > 2.5 && ptmean < 2.6) {minTOFbin = TOFaxis->FindBin(1000.); maxTOFbin = TOFaxis->FindBin(1800.);}
						if (ptmean > 2.6 && ptmean < 2.8) {minTOFbin = TOFaxis->FindBin(800.); maxTOFbin = TOFaxis->FindBin(1500.);}
						if (ptmean > 2.8 && ptmean < 3.0) {minTOFbin = TOFaxis->FindBin(700.); maxTOFbin = TOFaxis->FindBin(1400.);}
						if (ptmean > 3.0 && ptmean < 3.2) {minTOFbin = TOFaxis->FindBin(600.); maxTOFbin = TOFaxis->FindBin(1100.);}
						if (ptmean > 3.2 && ptmean < 3.4) {minTOFbin = TOFaxis->FindBin(1700.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.4 && ptmean < 3.6) {minTOFbin = TOFaxis->FindBin(1500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.6 && ptmean < 3.8) {minTOFbin = TOFaxis->FindBin(1500.); maxTOFbin = NbinsTOF;}
						if (ptmean > 3.8 && ptmean < 4.2) {minTOFbin = TOFaxis->FindBin(1400.); maxTOFbin = NbinsTOF;}
						if (ptmean > 4.2 && ptmean < 4.6) {minTOFbin = TOFaxis->FindBin(1200.); maxTOFbin = NbinsTOF;}
						if (ptmean > 4.6 && ptmean < 5.0) {minTOFbin = TOFaxis->FindBin(900.); maxTOFbin = NbinsTOF;}
					break;

					default:
						cout << Form("%s -> ERROR: Invalid option",__func__) << endl;
				}
			break;

			default:
				cout << Form("%s -> ERROR: Not a valid mass assumption (fMassAssumption)",__func__) << endl;

			break;

		}

	}

	// Prevent using the under/ overflow bins. 
	if (minTOFbin == 0) {minTOFbin++;}
	if (maxTOFbin == (NbinsTOF + 1)) {maxTOFbin--;}

	cout<<"TOFminbin: "<<minTOFbin<<" maxbin: "<<maxTOFbin<<" val: "<<TOFaxis->GetBinCenter(maxTOFbin)<<endl;

	Double_t DataSum = 0, MismatchSum = 0;
	for (Int_t iBinTOF = minTOFbin; iBinTOF < (maxTOFbin + 1); iBinTOF++) {
		for (Int_t iBinTPC = 1; iBinTPC < (NbinsTPC + 1); iBinTPC++) {
			DataSum += fData->GetBinContent(iBinTOF, iBinTPC);
			MismatchSum += fMismatch->GetBinContent(iBinTOF, iBinTPC);
		}
	}

	Double_t Ratio = DataSum / MismatchSum;
	SetNBgd( ((Double_t)fMismatch->GetEntries()) * Ratio);

}

// -----------------------------------------------------------------------
Int_t AliFitterTOFTPCDiHadronPID::GetNbinsInFit() const {

	// Returns the number of bins which are used to calculate chi^2.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	if (fData) {
		TAxis* TOFaxis = fData->GetXaxis();
		TAxis* TPCaxis = fData->GetYaxis();
		Int_t NTOFbins = TOFaxis->FindBin(fTOFmax) - TOFaxis->FindBin(fTOFmin) + 1;
		Int_t NTPCbins = TPCaxis->FindBin(fTPCmin) - TPCaxis->FindBin(fTPCmin) + 1;
		return (NTOFbins * NTPCbins);
	} else {return 0;}

}

// -----------------------------------------------------------------------
Double_t AliFitterTOFTPCDiHadronPID::GetSumNChargedAllBins() const {

	// Returns the sum of all bins in the data histogram (including under/over flow).
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	Double_t sum = 0.;
	if (fData) {

		TAxis* Xaxis = fData->GetXaxis();
		TAxis* Yaxis = fData->GetYaxis();

		for (Int_t iX = 0; iX <= (Xaxis->GetNbins() + 1); ++iX) {
			for (Int_t iY = 0; iY <= (Yaxis->GetNbins() + 1); ++iY) {
				sum += fData->GetBinContent(iX, iY);
			}
		}

	}

	return sum;

}

// -----------------------------------------------------------------------
Double_t AliFitterTOFTPCDiHadronPID::GetSumNChargedInFitRegion() const {

	// Returns the sum of all bins in the data histogram in the fit region.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	Double_t sum = 0.;
	Double_t epsilon = 10e-5;	// Small number.
	if (fData) {

		TAxis* Xaxis = fData->GetXaxis();
		TAxis* Yaxis = fData->GetYaxis();

		for (Int_t iX = Xaxis->FindBin(GetTOFmin() + epsilon); iX <= Xaxis->FindBin(GetTOFmax() - epsilon); ++iX) {
			for (Int_t iY = Yaxis->FindBin(GetTPCmin() + epsilon); iY <= Yaxis->FindBin(GetTPCmax() - epsilon); ++iY) {
				sum += fData->GetBinContent(iX, iY);
			}
		}

	}

	return sum;

}

// -----------------------------------------------------------------------
Double_t AliFitterTOFTPCDiHadronPID::GetIntegralOverFitFunction() {

	// Returns the integral over all fit functions (including mismatches if available)
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	Double_t sum = 0.;

	UpdateFitDescription();

	if (fFitStatus == 0) {

		if (fMismatch) {sum += GetNBgd();}
		for (Int_t iSpecies = 0; iSpecies < 4; ++iSpecies) {
			if ((*fUsedFunction)[iSpecies]) {sum += GetIntegral(iSpecies);}
		}

	}

	return sum;

}
	
// -----------------------------------------------------------------------
Double_t AliFitterTOFTPCDiHadronPID::GetIntegralOverFitFunctionInFitRegion() {

	// Returns integral over all fit functions within the fit region.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	UpdateFitDescription();

	Double_t sum = 0.;
	Double_t epsilon = 10e-5;	// Small number.
	if (fFitStatus == 0) {

		TAxis* Xaxis = fData->GetXaxis();
		TAxis* Yaxis = fData->GetYaxis();

		for (Int_t iX = Xaxis->FindBin(GetTOFmin() + epsilon); iX <= Xaxis->FindBin(GetTOFmax() - epsilon); ++iX) {
			for (Int_t iY = Yaxis->FindBin(GetTPCmin() + epsilon); iY <= Yaxis->FindBin(GetTPCmax() - epsilon); ++iY) {
				sum += fFitResult->GetBinContent(iX, iY);
			}
		}

	}

	return sum;

}

// -----------------------------------------------------------------------
TArrayI* AliFitterTOFTPCDiHadronPID::GetAllParsForSystematics() const {

	// Returns a list of all parameters that could be varied as a systematic check.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	Int_t allpars[21] = {GetNBgdParN(), GetSigmaTOFParN(0), GetMuTOFParN(0), GetTailTOFParN(0), GetSigmaTPCParN(0), GetMuTPCParN(0),
		GetSigmaTOFParN(1), GetMuTOFParN(1), GetTailTOFParN(1), GetSigmaTPCParN(1), GetMuTPCParN(1),
		GetSigmaTOFParN(2), GetMuTOFParN(2), GetTailTOFParN(2), GetSigmaTPCParN(2), GetMuTPCParN(2),
		GetSigmaTOFParN(3), GetMuTOFParN(3), GetTailTOFParN(3), GetSigmaTPCParN(3), GetMuTPCParN(3)};

	return (new TArrayI(21, allpars));

}

// -----------------------------------------------------------------------
TArrayI* AliFitterTOFTPCDiHadronPID::GetUsedParsForSystematics(Int_t fitstrategy) const {

	// Returns subset of the list generated by GetAllParsForSystematics(), 
	// of the parameters that were actually used in the last performed fit (i.e., 
	// we rely on fUsedFunction being up to date).
	//
	// UPDATE: Only the parameters which are fixed in the fit. Also the mismatch should be varied,
	// 		   since that's not truly a part of the fit. Which parameters are fixed is determined in
	//		   
	//
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	Int_t iPar = 0;
	TArrayI* usedpars = new TArrayI(fNFitParams);	// The array will for sure not be bigger than this number.

	// If there's a mismatch histogram known, try to vary it.
	if (fMismatch) {
		usedpars->AddAt(GetNBgdParN(),iPar); iPar++;
	}

	switch (fitstrategy) {
		case 2:
		case 4:
			usedpars->AddAt(GetTailTOFParN(1),iPar); iPar++;
			usedpars->AddAt(GetTailTOFParN(2),iPar); iPar++;			
			break;
		case 3:
			usedpars->AddAt(GetTailTOFParN(0),iPar); iPar++;		
			usedpars->AddAt(GetTailTOFParN(1),iPar); iPar++;
			usedpars->AddAt(GetTailTOFParN(2),iPar); iPar++;							
			break;		
	}

/*
	// Loop over all function descriptions.
	for (Int_t iSpecies = 0; iSpecies <= 3; iSpecies++) {
		switch (fUsedFunction->At(iSpecies)) {
			case 1:
				usedpars->AddAt(GetSigmaTOFParN(iSpecies), iPar); iPar++;
				usedpars->AddAt(GetMuTOFParN(iSpecies), iPar); iPar++;
				usedpars->AddAt(GetSigmaTPCParN(iSpecies), iPar); iPar++;
				usedpars->AddAt(GetMuTPCParN(iSpecies), iPar); iPar++;
			break;
			case 2:
				usedpars->AddAt(GetSigmaTOFParN(iSpecies), iPar); iPar++;
				usedpars->AddAt(GetMuTOFParN(iSpecies), iPar); iPar++;
				usedpars->AddAt(GetTailTOFParN(iSpecies), iPar); iPar++;
				usedpars->AddAt(GetSigmaTPCParN(iSpecies), iPar); iPar++;
				usedpars->AddAt(GetMuTPCParN(iSpecies), iPar); iPar++;								
			break;
		}
	}
*/


	// Set array length to the actual length.
	usedpars->Set(iPar);

	return usedpars;

}

// -----------------------------------------------------------------------
void AliFitterTOFTPCDiHadronPID::SetTOFRange(Double_t min, Double_t max) {

	// Sets the TOF fitting range.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (min > max) {cout << Form("%s -> ERROR: min > max.",__func__) << endl; return;}
	if (fData) {
		TAxis* TOFaxis = fData->GetXaxis();
		Double_t TOFmin = TOFaxis->GetBinLowEdge(1);
		Double_t TOFmax = TOFaxis->GetBinUpEdge(TOFaxis->GetNbins());
		if (min < TOFmin) {cout << Form("%s -> ERROR: min (%f) is smaller than the minimum of the supplied data histogram (%f).",__func__, min, TOFmin) << endl; return;}
		if (max > TOFmax) {cout << Form("%s -> ERROR: max (%f) is bigger than the maximum of the supplied data histogram (%f).",__func__, max, TOFmax) << endl; return;}
	}

	cout << Form("%s -> INFO: Setting TOF range to: (%f, %f).",__func__, min, max) << endl;
	fTOFmin = min; 
	fTOFmax = max;

}

// -----------------------------------------------------------------------
void AliFitterTOFTPCDiHadronPID::SetTPCRange(Double_t min, Double_t max) {

	// Sets the TPC fitting range.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (min > max) {cout << Form("%s -> ERROR: min > max.",__func__) << endl; return;}
	if (fData) {
		TAxis* TPCaxis = fData->GetYaxis();
		Double_t TPCmin = TPCaxis->GetBinLowEdge(1);
		Double_t TPCmax = TPCaxis->GetBinUpEdge(TPCaxis->GetNbins());
		if (min < TPCmin) {cout << Form("%s -> ERROR: min (%f) is smaller than the minimum of the supplied data histogram (%f).",__func__, min, TPCmin) << endl; return;}
		if (max > TPCmax) {cout << Form("%s -> ERROR: max (%f) is bigger than the maximum of the supplied data histogram (%f).",__func__, max, TPCmax) << endl; return;}
	}

	cout << Form("%s -> INFO: Setting TPC range to: (%f, %f).",__func__, min, max) << endl;
	fTPCmin = min; 
	fTPCmax = max;

}

// double AliFitterTOFTPCDiHadronPID::ChiSquared(const double* xx) const {

// 	/* TODO: Error should include error on mismatch! */

// 	// Let's say the gaussian continues in a exponent at point p, then the "slope" of the exponent,
// 	// i.e., b in exp(-bx), is given by: b = s^2/(p-mu).

// 	// Chi^2 calculated given a set of parameters xx, and the
// 	// supplied histograms, fit range, and function perscriptions.
// 	if (fDebug > 1) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

// 	// Determine the bins which are included in the fit.
// 	TAxis* TOFaxis = fData->GetXaxis();
// 	Int_t minbinTOF = TOFaxis->FindBin(fTOFmin);
// 	Int_t maxbinTOF = TOFaxis->FindBin(fTOFmax);

// 	TAxis* TPCaxis = fData->GetYaxis();
// 	Int_t minbinTPC = TPCaxis->FindBin(fTOFmin);
// 	Int_t maxbinTPC = TPCaxis->FindBin(fTOFmax);

// 	Double_t chisq = 0.;

// 	// Loop over all included bins.
// 	for (Int_t iBinTOF = minbinTOF; iBinTOF < (maxbinTOF + 1); iBinTOF++) {
// 		for (Int_t iBinTPC = minbinTPC; iBinTPC < (maxbinTPC + 1); iBinTPC++) {

// 			Double_t dataval = fData->GetBinContent(iBinTOF, iBinTPC);
// 			Double_t dataerror = fData->GetBinError(iBinTOF, iBinTPC);			

// 			// Ignore the very low statistics.
// 			if (dataval < 1.) {continue;}

// 			Double_t modelval = 0.;
// 			if (fMismatch) {

// 				// if mismatch is defined, we assume that it should have reasonable statistics too.
// 				if (fMismatch->GetBinContent(iBinTOF, iBinTPC) < 5.) {continue;}
// 				modelval += (fMismatch->GetBinContent(iBinTOF, iBinTPC) * xx[GetNBgdParN()] / fMismatch->GetEntries() );
// 			}

// 			for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {
				
// 				Double_t tailparameter = 0.;
// 				switch (fUsedFunction->At(iSpecies)) {
// 					case 0:
// 						// Particle is not part of the fit.
// 					break;
// 					case 1:
// 						// Particle is described by a 2D Gaussian.
// 						modelval += AliFunctionsDiHadronPID::Gaussian2D(
// 							TOFaxis->GetBinCenter(iBinTOF), TPCaxis->GetBinCenter(iBinTPC), xx[GetIntegralParN(iSpecies)], 
// 							xx[GetMuTOFParN(iSpecies)], xx[GetMuTPCParN(iSpecies)], xx[GetSigmaTOFParN(iSpecies)], xx[GetSigmaTPCParN(iSpecies)],
// 							TOFaxis->GetBinWidth(iBinTOF), TPCaxis->GetBinWidth(iBinTPC));
// 					break;
// 					case 2:
// 						// Particle is described by a 1D Gaussian with tail in positive TOF direction.
// 						// The tail parameter can either mean the position at which the gaussian and exponent join,
// 						// or it can mean b.
// 						tailparameter = xx[GetTailTOFParN(iSpecies)];
// 						if (fUseTailSlope) {tailparameter = (xx[GetSigmaTOFParN(iSpecies)] * xx[GetSigmaTOFParN(iSpecies)]) * xx[GetTailTOFParN(iSpecies)];}

// 						// Particle is described by a 2D Gaussian with tail in positive TOF direction.
// 						modelval += AliFunctionsDiHadronPID::Gaussian2DTailX(
// 							TOFaxis->GetBinCenter(iBinTOF), TPCaxis->GetBinCenter(iBinTPC), xx[GetIntegralParN(iSpecies)], 
// 							xx[GetMuTOFParN(iSpecies)], xx[GetMuTPCParN(iSpecies)], xx[GetSigmaTOFParN(iSpecies)], xx[GetSigmaTPCParN(iSpecies)],
// 							tailparameter, TOFaxis->GetBinWidth(iBinTOF), TPCaxis->GetBinWidth(iBinTPC));
// 					break;

// 					default:
// 						cout << Form("%s -> ERROR: Not a valid function was entered for species %i",__func__, iSpecies) << endl;
// 				}

// 			}

// 			// Protect against division through 0. Not so neat...
// 			Double_t currentchi = 0.;
// 			if (dataerror != 0.) currentchi = (dataval - modelval) / dataerror;

// 			chisq += (currentchi * currentchi);

// 		}
// 	}

// 	if (fPeakSigmaPenalty) {

// 		// Loop over pairs.
// 		for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {

// 			if (fUsedFunction->At(iSpecies)) {

// 				Double_t currentsigmaTOF = xx[GetSigmaTOFParN(iSpecies)];
// 				Double_t currentsigmaTPC = xx[GetSigmaTPCParN(iSpecies)];

// 				chisq *= AliFunctionsDiHadronPID::PolyPenalty(currentsigmaTOF, 120., fPeakSigmaPenaltyTOFRange, fPeakSigmaPenaltyOrder);
// 				chisq *= AliFunctionsDiHadronPID::PolyPenalty(currentsigmaTPC, 5., fPeakSigmaPenaltyTPCRange, fPeakSigmaPenaltyOrder);

// 			}
// 		}
// 	}

// 	if (fPeakDevPenalty) {

// 		for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {

// 			if (fUsedFunction->At(iSpecies)) {

// 				Double_t currentmuTOF = xx[GetMuTOFParN(iSpecies)];
// 				Double_t expectedmuTOF = EstimateTOFPosition(iSpecies);

// 				Double_t currentmuTPC = xx[GetMuTPCParN(iSpecies)];
// 				Double_t expectedmuTPC = EstimateTPCPosition(iSpecies);

// 				chisq *= AliFunctionsDiHadronPID::PolyPenalty(currentmuTOF, expectedmuTOF, fPeakDevPenaltyTOFRange, fPeakDevPenaltyOrder);
// 				chisq *= AliFunctionsDiHadronPID::PolyPenalty(currentmuTPC, expectedmuTPC, fPeakDevPenaltyTPCRange, fPeakDevPenaltyOrder);

// 			}
// 		}
// 	}


// 	// Applying negative integral penalty.
// 	if (fNegIntPenalty) {

// 		for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {

// 			if (fUsedFunction->At(iSpecies) && (xx[GetIntegralParN(iSpecies)] < 0. )) {

// 				chisq *= AliFunctionsDiHadronPID::PolyPenalty(xx[GetIntegralParN(iSpecies)], 0., 0., fNegIntPenaltyOrder);

// 			}
// 		}
// 	}

// 	return chisq;

// }

// -----------------------------------------------------------------------
double AliFitterTOFTPCDiHadronPID::ChiSquared(const double* xx) const {

	// Let's say the gaussian continues in a exponent at point p, then the "slope" of the exponent,
	// i.e., b in exp(-bx), is given by: b = s^2/(p-mu).

	// Chi^2 calculated given a set of parameters xx, and the
	// supplied histograms, fit range, and function perscriptions.
	if (fDebug > 1) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Determine the bins which are included in the fit.
	TAxis* TOFaxis = fData->GetXaxis();
	Int_t minbinTOF = TOFaxis->FindBin(fTOFmin);
	Int_t maxbinTOF = TOFaxis->FindBin(fTOFmax);

	TAxis* TPCaxis = fData->GetYaxis();
	Int_t minbinTPC = TPCaxis->FindBin(fTOFmin);
	Int_t maxbinTPC = TPCaxis->FindBin(fTOFmax);

	Double_t chisq = 0.;

	// Loop over all included bins.
	for (Int_t iBinTOF = minbinTOF; iBinTOF < (maxbinTOF + 1); iBinTOF++) {
		for (Int_t iBinTPC = minbinTPC; iBinTPC < (maxbinTPC + 1); iBinTPC++) {

			Double_t dataval = fData->GetBinContent(iBinTOF, iBinTPC);
			Double_t dataerror = fData->GetBinError(iBinTOF, iBinTPC);			

			// Ignore the very low statistics.
			if (dataval < fMinEntriesForFit) {continue;}

			Double_t modelval = 0.;
			if (fMismatch) {

				// if mismatch is defined, we assume that it should have reasonable statistics too. // DROPPED THIS
				//if (fMismatch->GetBinContent(iBinTOF, iBinTPC) < 10.) {continue;}
				modelval += (fMismatch->GetBinContent(iBinTOF, iBinTPC) * xx[GetNBgdParN()] / fMismatch->GetEntries() );
			}

			for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {
				
				Double_t tailparameter = 0.;
				switch (fUsedFunction->At(iSpecies)) {
					case 0:
						// Particle is not part of the fit.
					break;
					case 1:
						// Particle is described by a 2D Gaussian.
						modelval += AliFunctionsDiHadronPID::Gaussian2D(
							TOFaxis->GetBinCenter(iBinTOF), TPCaxis->GetBinCenter(iBinTPC), xx[GetIntegralParN(iSpecies)], 
							xx[GetMuTOFParN(iSpecies)], xx[GetMuTPCParN(iSpecies)], xx[GetSigmaTOFParN(iSpecies)], xx[GetSigmaTPCParN(iSpecies)],
							TOFaxis->GetBinWidth(iBinTOF), TPCaxis->GetBinWidth(iBinTPC));
					break;
					case 2:
						// Particle is described by a 1D Gaussian with tail in positive TOF direction.
						// The tail parameter can either mean the position at which the gaussian and exponent join,
						// or it can mean b.
						tailparameter = xx[GetTailTOFParN(iSpecies)];
						if (fUseTailSlope) {tailparameter = (xx[GetSigmaTOFParN(iSpecies)] * xx[GetSigmaTOFParN(iSpecies)]) * xx[GetTailTOFParN(iSpecies)];}

						// Particle is described by a 2D Gaussian with tail in positive TOF direction.
						modelval += AliFunctionsDiHadronPID::Gaussian2DTailX(
							TOFaxis->GetBinCenter(iBinTOF), TPCaxis->GetBinCenter(iBinTPC), xx[GetIntegralParN(iSpecies)], 
							xx[GetMuTOFParN(iSpecies)], xx[GetMuTPCParN(iSpecies)], xx[GetSigmaTOFParN(iSpecies)], xx[GetSigmaTPCParN(iSpecies)],
							tailparameter, TOFaxis->GetBinWidth(iBinTOF), TPCaxis->GetBinWidth(iBinTPC));
					break;

					default:
						cout << Form("%s -> ERROR: Not a valid function was entered for species %i",__func__, iSpecies) << endl;
				}

			}

			// Protect against division through 0. Not so neat...
			Double_t currentchi = 0.;
			if (dataerror != 0.) currentchi = (dataval - modelval) / dataerror;

			chisq += (currentchi * currentchi);

		}
	}

	if (fPeakSigmaPenalty) {

		// Loop over pairs.
		for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {

			if (fUsedFunction->At(iSpecies)) {

				Double_t currentsigmaTOF = xx[GetSigmaTOFParN(iSpecies)];
				Double_t currentsigmaTPC = xx[GetSigmaTPCParN(iSpecies)];

				chisq *= AliFunctionsDiHadronPID::PolyPenalty(currentsigmaTOF, 120., fPeakSigmaPenaltyTOFRange, fPeakSigmaPenaltyOrder);
				chisq *= AliFunctionsDiHadronPID::PolyPenalty(currentsigmaTPC, 5., fPeakSigmaPenaltyTPCRange, fPeakSigmaPenaltyOrder);

			}
		}
	}

	if (fPeakDevPenalty) {

		for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {

			if (fUsedFunction->At(iSpecies)) {

				Double_t currentmuTOF = xx[GetMuTOFParN(iSpecies)];
				Double_t expectedmuTOF = EstimateTOFPosition(iSpecies);

				Double_t currentmuTPC = xx[GetMuTPCParN(iSpecies)];
				Double_t expectedmuTPC = EstimateTPCPosition(iSpecies);

				chisq *= AliFunctionsDiHadronPID::PolyPenalty(currentmuTOF, expectedmuTOF, fPeakDevPenaltyTOFRange, fPeakDevPenaltyOrder);
				chisq *= AliFunctionsDiHadronPID::PolyPenalty(currentmuTPC, expectedmuTPC, fPeakDevPenaltyTPCRange, fPeakDevPenaltyOrder);

			}
		}
	}


	// Applying negative integral penalty.
	if (fNegIntPenalty) {

		for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {

			if (fUsedFunction->At(iSpecies) && (xx[GetIntegralParN(iSpecies)] < 0. )) {

				chisq *= AliFunctionsDiHadronPID::PolyPenalty(xx[GetIntegralParN(iSpecies)], 0., 0., fNegIntPenaltyOrder);

			}
		}
	}

	return chisq;

}

// -----------------------------------------------------------------------
void AliFitterTOFTPCDiHadronPID::UpdateFitDescription() { 

	// Updates the data members used to display the fit as it currently is, i.e.,
	// fFitResult, fMismatchScaled, fPeaksFunc[]
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	TAxis* TOFaxis = fData->GetXaxis();
	TAxis* TPCaxis = fData->GetYaxis();

	// 1) Updating fPeaksFunc[]
	for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {

		// Deleting existing functions.	
		if (fPeaksFunc->At(iSpecies)) {
			delete fPeaksFunc->At(iSpecies); 
			fPeaksFunc->AddAt(0x0, iSpecies);
		}

		// Creating the new function.
		TF2* ftmp = 0x0;	
		Double_t tailparameter = 0.;			
		switch (fUsedFunction->At(iSpecies)) {

			case 0:
				// Particle is not part of the fit.
			break;
			case 1:
				// Particle is described by a 2D Gaussian.
				ftmp = new TF2(Form("fPeaksFunc_%i",iSpecies),
					"AliFunctionsDiHadronPID::Gaussian2D(x,y,[0],[1],[2],[3],[4],[5],[6])",fTOFmin,fTOFmax,fTPCmin,fTPCmax);
				ftmp->SetLineColor(iSpecies+2);
				ftmp->SetParNames("Integral","#mu_{TOF}","#mu_{TPC}","#sigma_{TOF}","#sigma_{TPC}","binwidth_{TOF}","binwidth_{TPC}");
				ftmp->SetParameters(GetIntegral(iSpecies),
					GetMuTOF(iSpecies), GetMuTPC(iSpecies), GetSigmaTOF(iSpecies), GetSigmaTPC(iSpecies),
					TOFaxis->GetBinWidth(1), TPCaxis->GetBinWidth(1));
				fPeaksFunc->AddAt(ftmp, iSpecies);
			break;
			case 2:
				// Particle is described by a 2D Gaussian + Tail in TOF.
				ftmp = new TF2(Form("fPeaksFunc_%i",iSpecies),
					"AliFunctionsDiHadronPID::Gaussian2DTailX(x,y,[0],[1],[2],[3],[4],[5],[6],[7])",fTOFmin,fTOFmax,fTPCmin,fTPCmax);
				ftmp->SetLineColor(iSpecies+2);
				ftmp->SetParNames("Integral","#mu_{TOF}","#mu_{TPC}","#sigma_{TOF}","#sigma_{TPC}","tail_{TOF}","binwidth_{TOF}","binwidth_{TPC}");
				
				tailparameter = GetTailTOF(iSpecies);
				if (fUseTailSlope) {
					// in this case the tail means the slope.
					tailparameter = GetTailTOF(iSpecies) * GetSigmaTOF(iSpecies) * GetSigmaTOF(iSpecies);
					//cout << "Updating fit description, slope = " << GetTailTOF(iSpecies) << " hence, p = "<<tailparameter<<endl;
				}

				ftmp->SetParameters(GetIntegral(iSpecies),
					GetMuTOF(iSpecies), GetMuTPC(iSpecies), GetSigmaTOF(iSpecies), GetSigmaTPC(iSpecies),
					tailparameter, TOFaxis->GetBinWidth(1), TPCaxis->GetBinWidth(1));
				fPeaksFunc->AddAt(ftmp, iSpecies);				
			break;
/*			
			case 3:
				// Particle is described by a 2D Gaussian + Tail in TPC.			
				ftmp = new TF2(Form("fPeaksFunc_%i",iSpecies),
					"AliFunctionsDiHadronPID::Gaussian2DTailY(x,y,[0],[1],[2],[3],[4],[5],[6],[7])",fTOFmin,fTOFmax,fTPCmin,fTPCmax);
				ftmp->SetLineColor(iSpecies+2);
				ftmp->SetParNames("Integral","#mu_{TOF}","#mu_{TPC}","#sigma_{TOF}","#sigma_{TPC}","tail_{TPC}","binwidth_{TOF}","binwidth_{TPC}");
				ftmp->SetParameters(GetIntegral(iSpecies),
					GetMuTOF(iSpecies), GetMuTPC(iSpecies), GetSigmaTOF(iSpecies), GetSigmaTPC(iSpecies),
					GetTailTPC(iSpecies), TOFaxis->GetBinWidth(1), TPCaxis->GetBinWidth(1));
				fPeaksFunc->AddAt(ftmp, iSpecies);				
			break;
			case 4:
				// Particle is described by a 2D Gaussian + Tail in TOF and TPC.			
				ftmp = new TF2(Form("fPeaksFunc_%i",iSpecies),
					"AliFunctionsDiHadronPID::Gaussian2DTailXTPC(x,y,[0],[1],[2],[3],[4],[5],[6],[7],[8])",fTOFmin,fTOFmax,fTPCmin,fTPCmax);
				ftmp->SetLineColor(iSpecies+2);
				ftmp->SetParNames("Integral","#mu_{TOF}","#mu_{TPC}","#sigma_{TOF}","#sigma_{TPC}","tail_{TOF}","tail_{TPC}","binwidth_{TOF}","binwidth_{TPC}");
				ftmp->SetParameters(GetIntegral(iSpecies),
					GetMuTOF(iSpecies), GetMuTPC(iSpecies), GetSigmaTOF(iSpecies), GetSigmaTPC(iSpecies),
					GetTailTOF(iSpecies), GetTailTPC(iSpecies), TOFaxis->GetBinWidth(1), TPCaxis->GetBinWidth(1));	
				fPeaksFunc->AddAt(ftmp, iSpecies);							
			break;
*/
			default:
				cout << Form("%s -> ERROR: Not a valid function was entered for species %i",__func__, iSpecies) << endl;
		}
	}

	// 2) Updating fMismatchScaled
	if (fMismatch) {

		// Deleting existing histogram.
		if (fMismatchScaled) {
			delete fMismatchScaled;
			fMismatchScaled = 0x0;
		}

		// Creating new histogram.
		fMismatchScaled = (TH2F*)fMismatch->Clone("fMismatchScaled");
		fMismatchScaled->SetDirectory(0);
		fMismatchScaled->Scale( GetNBgd() / fMismatch->GetEntries() );
		fMismatchScaled->SetLineColor(kGray+1);
		fMismatchScaled->SetMarkerColor(kGray+1);
		fMismatchScaled->SetLineWidth(2);

	} 

	// 3) Updating fFitResult
	if (fFitResult) {
		delete fFitResult; 
		fFitResult = 0x0;
	}

	fData->SetLineColor(kBlack);
	fData->SetLineWidth(2);

	Int_t NbinsTOF = TOFaxis->GetNbins();
	Double_t minTOF = TOFaxis->GetBinLowEdge(1);
	Double_t maxTOF = TOFaxis->GetBinUpEdge(NbinsTOF);

	Int_t NbinsTPC = TPCaxis->GetNbins();
	Double_t minTPC = TPCaxis->GetBinLowEdge(1);
	Double_t maxTPC = TPCaxis->GetBinUpEdge(NbinsTPC);

	fFitResult = new TH2F("fFitResult","fFitResult", NbinsTOF, minTOF, maxTOF, NbinsTPC, minTPC, maxTPC);
	fFitResult->SetDirectory(0);
	fFitResult->SetLineColor(kViolet);
	fFitResult->SetLineWidth(2);
	fFitResult->SetMarkerColor(kViolet);
	//fFitResult->SetMarkerStyle(kFullCircle);

	Int_t minbinTOF = TOFaxis->FindBin(fTOFmin);
	Int_t maxbinTOF = TOFaxis->FindBin(fTOFmax);

	Int_t minbinTPC = TPCaxis->FindBin(fTPCmin);
	Int_t maxbinTPC = TPCaxis->FindBin(fTPCmax);

	for (Int_t iBinTOF = minbinTOF; iBinTOF < (maxbinTOF + 1); iBinTOF++) {
		for (Int_t iBinTPC = minbinTPC; iBinTPC < (maxbinTPC + 1); iBinTPC++) {

			Double_t modelval = 0.;

			if (fMismatch) {
				modelval += fMismatchScaled->GetBinContent(iBinTOF, iBinTPC);//(fMismatch->GetBinContent(iBinTOF, iBinTPC) * GetNBgd() / fMismatch->GetEntries() );
			}

			for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {
				if (fPeaksFunc->At(iSpecies)) {
					modelval += ( ((TF2*)fPeaksFunc->At(iSpecies))->Eval(TOFaxis->GetBinCenter(iBinTOF), TPCaxis->GetBinCenter(iBinTPC)) );
				}
			}

			fFitResult->SetBinContent(iBinTOF, iBinTPC, modelval);

		}
	}
}

// -----------------------------------------------------------------------
void AliFitterTOFTPCDiHadronPID::SetRangePbPbDefault() {

	// Set ranges optimized for finely binned PID axes with a large range (such as
	// the default in the track cuts objects).
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	switch (fMassAssumption) {

		case 0:
			if (fPtMin < 0.7) {
				SetTOFMin(-300.);
				SetTOFMax(GetMuTOF(1)/2.);
				SetTPCMax(15.);
				SetTPCMin(-15.);
			} else if (fPtMin < 1.4) {
				SetTOFMin(-200.);
				SetTOFMax(GetMuTOF(1) + 5. * GetSigmaTOF(2));				
				SetTPCMax(15.);	
				SetTPCMin(-15.);										
			} else {
				SetTOFMin(-200.);
				SetTOFMax(GetMuTOF(2) + 5. * GetSigmaTOF(2));					
				SetTPCMax(15.);		
				SetTPCMin(-15.);							
			} 
		break;

		case 1:
			if (fPtMin < 0.7) {
				SetTOFMin(GetMuTOF(0)/2.);			// FIXME
			} else if (fPtMin < 2.0) {		
				SetTOFMin(GetMuTOF(0) - 250.);		// FIXME	
			} else {
				SetTOFMin(GetMuTOF(0) - 250.);
				SetTOFMax(GetMuTOF(2) + 5. * GetSigmaTOF(2));
				SetTPCMin(-10);					
			}
		break;

		case 2:
			if (fPtMin < 1.2) {
				SetTOFMin(GetMuTOF(1)/2.);			
			} else if (fPtMin < 2.0) {
				SetTOFMin(GetMuTOF(0) - 300.);
				SetTOFMax(GetMuTOF(2) + 5.*GetSigmaTOF(2));
				SetTPCMin(-10.);
				SetTPCMax(TMath::Max(10.,GetMuTPC(0) + GetSigmaTPC(0)));								
			} else {
				SetTOFMin(GetMuTOF(0) - 300.);
				SetTOFMax(GetMuTOF(2) + 5. * GetSigmaTOF(2));				
				SetTPCRange(-10., TMath::Max(10.,GetMuTPC(0) + GetSigmaTPC(0)));					
			}
		break;

		default:
			cout << Form("%s -> ERROR: %i is not a valid mass assumption.", __func__, fMassAssumption) << endl;

	}

}

// -----------------------------------------------------------------------
void AliFitterTOFTPCDiHadronPID::SetRangeppDefault() {

	// Set ranges optimized for finely binned PID axes with a large range (such as
	// the default in the track cuts objects).
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	switch (fMassAssumption) {

		case 0:
			if (fPtMin < 0.7) {
				SetTOFMin(-1000.);
				SetTOFMax(GetMuTOF(1)/2.);
				SetTPCMax(15.);
				SetTPCMin(-15.);
			} else if (fPtMin < 1.4) {
				SetTOFMin(-500.);
				SetTOFMax(GetMuTOF(1)/2. + GetMuTOF(2)/2.);
				SetTPCMax(15.);
				SetTPCMin(-15.);
			} else if (fPtMin < 2.0) {
				SetTOFMin(-500.);
				SetTOFMax(GetMuTOF(1)/2. + GetMuTOF(2)/2.);
				SetTPCMax(15.);							
			} else {
				SetTOFMin(-500.);
				SetTOFMax(GetMuTOF(2) + 3.0 * GetSigmaTOF(2));				
				SetTPCMin(-15.);
				SetTPCMax(GetMuTPC(0) + 3.0 * GetSigmaTPC(0));
			} 
		break;

		case 1:
			if (fPtMin < 0.7) {
				SetTOFMin(GetMuTOF(0)/2.);
				SetTOFMax(GetMuTOF(1) + 3.0*GetSigmaTOF(1) );
				SetTPCMin(-15.);
				SetTPCMax(15.);
			} else if (fPtMin < 2.0) {		
				SetTOFMin(GetMuTOF(0) - 3.0*GetSigmaTOF(0));
				SetTOFMax(GetMuTOF(1) + 3.0*GetSigmaTOF(1) );
				SetTPCMin(-15.);
				SetTPCMax(15.);
			} else {
				SetTOFMin(GetMuTOF(0) - 3.0*GetSigmaTOF(0));
				SetTOFMax(GetMuTOF(2) + 3.0*GetSigmaTOF(2));
				SetTPCMin(-15.);
				SetTPCMax(15.);			}
		break;

		case 2:
			if (fPtMin < 1.2) {
 				SetTOFMin(GetMuTOF(1)/2.);		
				SetTOFMax(GetMuTOF(2) + 3000. );	
				SetTPCMin(-20.);
				SetTPCMax(15.);
			} else if (fPtMin < 2.0) {
				SetTOFMin(GetMuTOF(0) - 1000.);
				SetTOFMax(GetMuTOF(2) + 2000.);
				SetTPCMin(-20.);
				SetTPCMax(TMath::Max(10.,GetMuTPC(0) + GetSigmaTPC(0)));								
			} else {
				// SetTPCMin(-10.);
				SetTOFMin(GetMuTOF(0) - 200.);
				SetTOFMax(GetMuTOF(2) + 3. * GetSigmaTOF(2));				
				SetTPCMin(GetMuTPC(2) - 2.0 * GetSigmaTPC(2));
				SetTPCMax(GetMuTPC(0) + 2.0 * GetSigmaTPC(0));
			}
		break;

		default:
			cout << Form("%s -> ERROR: %i is not a valid mass assumption.", __func__, fMassAssumption) << endl;

	}

}

// -----------------------------------------------------------------------
TArrayI* AliFitterTOFTPCDiHadronPID::GetPeakDescriptionPbPbDefault() {

	// 1) Returns an array with the type of peak desired for each species,
	//    appropriate for the mass assumption and the pt range. 
	// 2) Adds penalties for various undesirable situations which can occur during the fit.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	TArrayI* aout = 0x0;

	switch (fMassAssumption) {

		case 0:
			if (fPtMin < 0.7) {Int_t peakincluded[4] = {2,0,0,0}; aout = new TArrayI(4, peakincluded);}
			else if (fPtMin < 1.4) {Int_t peakincluded[4] = {2,2,0,0}; aout = new TArrayI(4, peakincluded);}
			//else if (fPtMin < 2.0) {Int_t peakincluded[4] = {2,2,1,0}; aout = new TArrayI(4, peakincluded);}	
			else {Int_t peakincluded[4] = {2,2,2,0}; aout = new TArrayI(4, peakincluded);}
		break;

		case 1:
			if (fPtMin < 0.7) {Int_t peakincluded[4] = {0,2,0,0}; aout = new TArrayI(4, peakincluded);}
			else if (fPtMin < 2.0) {Int_t peakincluded[4] = {2,2,0,0}; aout = new TArrayI(4, peakincluded);}	
			else {Int_t peakincluded[4] = {2,2,2,0}; aout = new TArrayI(4, peakincluded);}
		break;

		case 2:
			if (fPtMin < 1.2) {Int_t peakincluded[4] = {0,0,2,0}; aout = new TArrayI(4, peakincluded);}
			else if (fPtMin < 2.0) {Int_t peakincluded[4] = {0,1,2,0}; aout = new TArrayI(4, peakincluded);}	
			else {Int_t peakincluded[4] = {2,2,2,0}; aout = new TArrayI(4, peakincluded);}	
		break;

		default:
			cout << Form("%s -> ERROR: %i is not a valid mass assumption.", __func__, fMassAssumption) << endl;
	}

	return aout;

}

// -----------------------------------------------------------------------
TArrayI* AliFitterTOFTPCDiHadronPID::GetPeakDescriptionppDefault() {

	// 1) Returns an array with the type of peak desired for each species,
	//    appropriate for the mass assumption and the pt range. 
	// 2) Adds penalties for various undesirable situations which can occur during the fit.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	TArrayI* aout = 0x0;

	switch (fMassAssumption) {

		case 0:
			if (fPtMin < 0.7) {Int_t peakincluded[4] = {2,0,0,0}; aout = new TArrayI(4, peakincluded);}
			else if (fPtMin < 1.4) {Int_t peakincluded[4] = {2,2,0,0}; aout = new TArrayI(4, peakincluded);}	
			else if (fPtMin < 2.0) {Int_t peakincluded[4] = {2,2,1,0}; aout = new TArrayI(4, peakincluded);}	
			else {Int_t peakincluded[4] = {2,2,2,0}; aout = new TArrayI(4, peakincluded);}
		break;

		case 1:
			if (fPtMin < 0.7) {Int_t peakincluded[4] = {0,2,0,0}; aout = new TArrayI(4, peakincluded);}
			else if (fPtMin < 2.0) {Int_t peakincluded[4] = {2,2,0,0}; aout = new TArrayI(4, peakincluded);}	
			else {Int_t peakincluded[4] = {2,2,2,0}; aout = new TArrayI(4, peakincluded);}
		break;

		case 2:
			if (fPtMin < 1.2) {Int_t peakincluded[4] = {0,0,2,0}; aout = new TArrayI(4, peakincluded);}
			else if (fPtMin < 2.0) {Int_t peakincluded[4] = {2,2,2,0}; aout = new TArrayI(4, peakincluded);}	
			else {Int_t peakincluded[4] = {2,2,2,0}; aout = new TArrayI(4, peakincluded);}	
		break;

		default:
			cout << Form("%s -> ERROR: %i is not a valid mass assumption.", __func__, fMassAssumption) << endl;
	}

	return aout;

}

// -----------------------------------------------------------------------
Double_t AliFitterTOFTPCDiHadronPID::GetFitParameterFromParameterisationPbPbDefault(Int_t iPar) {

	// The main purpose of this function is to get a reasonable value for a certain parameter if
	// it is hard to fit. This should always be accompanied by a sensible variation (for systematics).

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	Double_t meanpt = 0.5 * (fPtMax + fPtMin);

	switch (fMassAssumption) {

		// MA: Pions
		case 0:
			switch (iPar) {
				case 4:
					return 0.00821029 + meanpt * 0.000596398;
				case 11:
					return 0.00896201;	// tailK
				case 18:
					return 0.00886947;	// tailp
				default:
					cout << Form("%s -> ERROR: no parameterisation available for parameter %i, mass assumption %i.", __func__, iPar, fMassAssumption) << endl;
			}
		break;

		// MA: Kaons
		case 1:
			switch (iPar) {
				case 4:
					return 0.00733511 + meanpt * 0.000799057;
				case 11:
					return 0.00892376;	// tailK
				case 18:
					return 0.00876097;	// tailp
				default:
					cout << Form("%s -> ERROR: no parameterisation available for parameter %i, mass assumption %i.", __func__, iPar, fMassAssumption) << endl;
			}
		break;

		// MA: Protons
		case 2:
			switch (iPar) {
				case 4:
					return (-0.0085554) + meanpt * 0.00895532 + meanpt * meanpt * (-0.00102602);
				case 11:
					return 0.00829111;	// tailK
				case 18:
					return 0.00909928;	// tailp
				default:
					cout << Form("%s -> ERROR: no parameterisation available for parameter %i, mass assumption %i.", __func__, iPar, fMassAssumption) << endl;
			}
		break;

		default:
			cout << Form("%s -> ERROR: %i is not a valid mass assumption.", __func__, fMassAssumption) << endl;

	}

	return -999.;

}

// -----------------------------------------------------------------------
Double_t AliFitterTOFTPCDiHadronPID::GetFitParameterFromParameterisationppDefault(Int_t iPar) {

	// The main purpose of this function is to get a reasonable value for a certain parameter if
	// it is hard to fit. This should always be accompanied by a sensible variation (for systematics).

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	switch (fMassAssumption) {

		// MA: Pions
		case 0:
			switch (iPar) {
				case 0:
					return 0.01000; // tailPi
				case 1:
					return 0.00785;	// tailK
				case 2:
					return 0.00645;	// tailp
				default:
					cout << Form("%s -> ERROR: no parameterisation available for parameter %i, mass assumption %i.", __func__, iPar, fMassAssumption) << endl;
			}
		break;

		// MA: Kaons
		case 1:
			switch (iPar) {
				case 0:
					return 0.01005; // tailPi
				case 1:
					return 0.00793;	// tailK
				case 2:
					return 0.00645;	// tailp
				default:
					cout << Form("%s -> ERROR: no parameterisation available for parameter %i, mass assumption %i.", __func__, iPar, fMassAssumption) << endl;
			}
		break;

		// MA: Protons
		case 2:
			switch (iPar) {
				case 0:
					return 0.00951; // tailPi
				case 1:
					return 0.00736;	// tailK
				case 2:
					return 0.00649;	// tailp
				default:
					cout << Form("%s -> ERROR: no parameterisation available for parameter %i, mass assumption %i.", __func__, iPar, fMassAssumption) << endl;
			}
		break;

		default:
			cout << Form("%s -> ERROR: %i is not a valid mass assumption.", __func__, fMassAssumption) << endl;

	}

	return -999.;

}

// -----------------------------------------------------------------------
Int_t AliFitterTOFTPCDiHadronPID::DoFitWithStrategyPbPbDefault(TObjArray* fitstages, Int_t mismatchrange, const TArrayI* fromParam) {

	// Description: 
	//  * Fit pi/ka/pr peaks where overlap is significant.
	//  * Don't fit deuterons (excluded from the range).
	//  * Measure background in sidebands, and keep it fixed.
	//  * Npar's: 5 - 15.

	// Implementation of the default fit strategy of PbPb histograms.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (fromParam && fromParam->GetSize() != fNFitParams) {cout << Form("%s -> ERROR: Array fromParam has length %i instead of %i.", __func__, fromParam->GetSize(), fNFitParams) << endl; return 0;}

	// Perform initial estimates.
	EstimateTOFPositions();				// Standard estimate.
	EstimateTPCPositions();				// Standard estimate.
	EstimateNMismatch(mismatchrange);	// Estimate in sideband.
	EstimateNParticles();				// Estimate using expected peak positons (not very good when fitting peak/bulk regions)

	SetRangePbPbDefault();				// Range never includes the deuterons.
	TArrayI* peakincluded = GetPeakDescriptionPbPbDefault();

	// Create TCanvases for the fit results.
	TCanvas* cFit = TCanvas::MakeDefCanvas();
	cFit->SetName("Fit");
	cFit->SetTitle("Fit");
	cFit->Divide(3,2);

	TCanvas* cPull = TCanvas::MakeDefCanvas();
	cPull->SetName("Pull");
	cPull->SetTitle("Pull");	
	cPull->Divide(3,2);

	TCanvas* cTOFProj = TCanvas::MakeDefCanvas();
	cTOFProj->SetName("TOFProjection");
	cTOFProj->SetTitle("TOFProjection");
	cTOFProj->Divide(3,2);

	TCanvas* cTPCProj = TCanvas::MakeDefCanvas();
	cTPCProj->SetName("TPCProjection");
	cTPCProj->SetTitle("TPCProjection");
	cTPCProj->Divide(3,2);

	// 1) Print Initial estimates.
	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies, peakincluded->At(iSpecies));
		}
	}

	TAxis* TOFaxis = fData->GetXaxis();
	TAxis* TPCaxis = fData->GetYaxis();
	Int_t TOFbinMin = TOFaxis->FindBin(GetTOFmin());
	Int_t TOFbinMax = TOFaxis->FindBin(GetTOFmax());
	Int_t TPCbinMin = TPCaxis->FindBin(GetTPCmin());
	Int_t TPCbinMax = TPCaxis->FindBin(GetTPCmax()); 

	if (fromParam) {FixParameters(fromParam);}

	PrintFit(cFit, 1);
	PrintFitPull(cPull, 1);
	PrintFitTOFProjection(cTOFProj, 1, TPCbinMin, TPCbinMax);
	PrintFitTPCProjection(cTPCProj, 1, TOFbinMin, TOFbinMax);

	// 2) Fit the sigma, mu and integral for each particle separately, the tails are still fixed (5 dim fit).
	SetFixNBgd(kTRUE);
	for (Int_t iSpecies = 2; iSpecies >= 0; iSpecies--) {
		if (peakincluded->At(iSpecies)) {
			for (Int_t jSpecies = 2; jSpecies >= 0; jSpecies--) {
				if (peakincluded->At(jSpecies)) {
					SetUsedFunction(jSpecies, peakincluded->At(jSpecies));
					SetFixIntegral(jSpecies, (iSpecies == jSpecies) ? kFALSE : kTRUE); 
					SetFixMuTOF(jSpecies, (iSpecies == jSpecies) ? kFALSE : kTRUE);
					if (peakincluded->At(jSpecies) == 2) {SetFixTailTOF(jSpecies, kTRUE);}
					SetFixSigmaTOF(jSpecies, (iSpecies == jSpecies) ? kFALSE : kTRUE);
					SetFixMuTPC(jSpecies, (iSpecies == jSpecies) ? kFALSE : kTRUE);
					SetFixSigmaTPC(jSpecies, (iSpecies == jSpecies) ? kFALSE : kTRUE);
				}
			}
			if (fromParam) {FixParameters(fromParam);}
			DoFit();
		}
	}
	
	PrintFit(cFit, 2);
	PrintFitPull(cPull, 2);
	PrintFitTOFProjection(cTOFProj, 2, TPCbinMin, TPCbinMax);
	PrintFitTPCProjection(cTPCProj, 2, TOFbinMin, TOFbinMax);

	// 3) Same as before, but now we release mu, sigma and integral of every peak simultaneously.
	SetFixNBgd(kTRUE);
	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies, peakincluded->At(iSpecies));
			SetFixIntegral(iSpecies, kFALSE); 
			SetFixMuTOF(iSpecies, kFALSE);
			if (peakincluded->At(iSpecies) == 2) {SetFixTailTOF(iSpecies, kTRUE);}
			SetFixSigmaTOF(iSpecies, kFALSE);
			SetFixMuTPC(iSpecies, kFALSE);
			SetFixSigmaTPC(iSpecies, kFALSE);
		}
	}
	if (fromParam) {FixParameters(fromParam);}	
	DoFit();

	PrintFit(cFit, 3);
	PrintFitPull(cPull, 3);
	PrintFitTOFProjection(cTOFProj, 3, TPCbinMin, TPCbinMax);
	PrintFitTPCProjection(cTPCProj, 3, TOFbinMin, TOFbinMax);

	// 4) Release Tails if applicable (from right to left in TOF).
	SetFixNBgd(kTRUE);
	for (Int_t iSpecies = 2; iSpecies >= 0; iSpecies--) {
		SetUsedFunction(iSpecies, peakincluded->At(iSpecies));
		
		// Release tails of one peak.
		if (peakincluded->At(iSpecies) == 2) {
			SetFixIntegral(iSpecies, kFALSE);
			SetFixMuTOF(iSpecies, kFALSE);
			SetFixSigmaTOF(iSpecies, kFALSE);
			SetFixTailTOF(iSpecies, kFALSE);
			SetFixMuTPC(iSpecies, kFALSE);
			SetFixSigmaTPC(iSpecies, kFALSE);

			// and fix all the rest.
			for (Int_t jSpecies = 0; jSpecies < 3; jSpecies++) {
				if (jSpecies == iSpecies) {continue;}
				SetFixIntegral(jSpecies,kTRUE);
				SetFixMuTOF(jSpecies,kTRUE);
				SetFixSigmaTOF(jSpecies,kTRUE);
				SetFixTailTOF(jSpecies,kTRUE);	
				SetFixMuTPC(jSpecies,kTRUE);
				SetFixSigmaTPC(jSpecies,kTRUE);
			}
			if (fromParam) {FixParameters(fromParam);}
			DoFit();

		}

	}

	PrintFit(cFit, 4);
	PrintFitPull(cPull, 4);
	PrintFitTOFProjection(cTOFProj, 4, TPCbinMin, TPCbinMax);
	PrintFitTPCProjection(cTPCProj, 4, TOFbinMin, TOFbinMax);

	// 5) Release Everything.
	SetFixNBgd(kTRUE);
	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies, peakincluded->At(iSpecies));
			SetFixIntegral(iSpecies,kFALSE);
			SetFixMuTOF(iSpecies,kFALSE);
			SetFixSigmaTOF(iSpecies,kFALSE);
			if (peakincluded->At(iSpecies) == 2) {SetFixTailTOF(iSpecies,kFALSE);}
			SetFixMuTPC(iSpecies,kFALSE);
			SetFixSigmaTPC(iSpecies,kFALSE);
		}
	}
	if (fromParam) {FixParameters(fromParam);}
	DoFit();

	PrintFit(cFit, 5);
	PrintFitPull(cPull, 5);
	PrintFitTOFProjection(cTOFProj, 5, TPCbinMin, TPCbinMax);
	PrintFitTPCProjection(cTPCProj, 5, TOFbinMin, TOFbinMax);

	if (fitstages) {
		fitstages->AddLast(cFit->Clone());
		fitstages->AddLast(cPull->Clone());
		fitstages->AddLast(cTOFProj->Clone());
		fitstages->AddLast(cTPCProj->Clone());
	}

	delete cFit;
	delete cPull;
	delete cTOFProj;
	delete cTPCProj;

	return 0;

}

// -----------------------------------------------------------------------
Int_t AliFitterTOFTPCDiHadronPID::DoFitWithStrategyppFixedTOFTail(TObjArray* fitstages, Int_t mismatchrange, Bool_t fixtailpi, Bool_t fixtailka, Bool_t fixtailpr) {

	// Description: 
	//  * Fit pi/ka/pr peaks where overlap is significant.
	//  * Don't fit deuterons (excluded from the range).
	//  * Measure background in sidebands, and keep it fixed.
	//  * Npar's: 5 - 15.
	//  * Fixed TOF Tail at 0.08 for some species

	// Implementation of the default fit strategy of PbPb histograms.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	Bool_t fixTailPerSpecies[3] = {fixtailpi, fixtailka, fixtailpr};

	// Perform initial estimates.
	EstimateTOFPositions();					// Standard estimate.
	EstimateTPCPositions();					// Standard estimate.
	if (mismatchrange != -1) {
		EstimateNMismatch(mismatchrange);	// Estimate in sideband.
	} else {
		fMismatch = 0x0; 					// delete mismatches.
		SetFixNBgd(kTRUE);
	}
	EstimateNParticles();					// Estimate using expected peak positons (not very good when fitting peak/bulk regions)

	SetRangeppDefault();					// Range never includes the deuterons.
	TArrayI* peakincluded = GetPeakDescriptionppDefault();

	// Fix TOF tail parameters if necesary.
	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		if (fixTailPerSpecies[iSpecies]) {
			SetTailTOF(iSpecies, GetFitParameterFromParameterisationppDefault(iSpecies));
		}
		SetFixTailTOF(iSpecies, kTRUE);
	}

	// Create TCanvases for the fit results.
	TCanvas* cFit = TCanvas::MakeDefCanvas();
	cFit->SetName("Fit");
	cFit->SetTitle("Fit");
	cFit->Divide(3,2);

	TCanvas* cPull = TCanvas::MakeDefCanvas();
	cPull->SetName("Pull");
	cPull->SetTitle("Pull");	
	cPull->Divide(3,2);

	TCanvas* cTOFProj = TCanvas::MakeDefCanvas();
	cTOFProj->SetName("TOFProjection");
	cTOFProj->SetTitle("TOFProjection");
	cTOFProj->Divide(3,2);

	TCanvas* cTPCProj = TCanvas::MakeDefCanvas();
	cTPCProj->SetName("TPCProjection");
	cTPCProj->SetTitle("TPCProjection");
	cTPCProj->Divide(3,2);

	Int_t fitStage = 1;
	// 1) Print Initial estimates.
	cout<<Form("Fit stage %i.",fitStage)<<endl<<"Initial estimation."<<endl;
	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies, peakincluded->At(iSpecies));
		}
	}

	TAxis* TOFaxis = fData->GetXaxis();
	TAxis* TPCaxis = fData->GetYaxis();
	Int_t TOFbinMin = TOFaxis->FindBin(GetTOFmin());
	Int_t TOFbinMax = TOFaxis->FindBin(GetTOFmax());
	Int_t TPCbinMin = TPCaxis->FindBin(GetTPCmin());
	Int_t TPCbinMax = TPCaxis->FindBin(GetTPCmax()); 

	PrintFit(cFit, fitStage);
	PrintFitPull(cPull, fitStage);
	PrintFitTOFProjection(cTOFProj, fitStage, TPCbinMin, TPCbinMax);
	PrintFitTPCProjection(cTPCProj, fitStage, TOFbinMin, TOFbinMax);
	fitStage++;

	// 2) Fit integral mu
	cout<<Form("Fit stage %i.",fitStage)<<endl;
	SetFixNBgd(kTRUE);
	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies, peakincluded->At(iSpecies));
			SetFixIntegral(iSpecies, kFALSE); 
			SetFixMuTOF(iSpecies, kFALSE);
			SetFixSigmaTOF(iSpecies, kTRUE);
			SetFixTailTOF(iSpecies,kTRUE);
			SetFixMuTPC(iSpecies, kFALSE);
			SetFixSigmaTPC(iSpecies, kTRUE);
		}
	}
	
	DoFit();
	
	PrintFit(cFit, fitStage);
	PrintFitPull(cPull, fitStage);
	PrintFitTOFProjection(cTOFProj, fitStage, TPCbinMin, TPCbinMax);
	PrintFitTPCProjection(cTPCProj, fitStage, TOFbinMin, TOFbinMax);
	fitStage++;

	// 3) Fit integral and sigmas, fix mu
	cout<<Form("Fit stage %i.",fitStage)<<endl;
	SetFixNBgd(kTRUE);

	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies, peakincluded->At(iSpecies));
			SetFixIntegral(iSpecies, kTRUE); 
			SetFixMuTOF(iSpecies, kTRUE);
			SetFixSigmaTOF(iSpecies, kFALSE);
			// SetFixTailTOF(iSpecies, kTRUE);
			SetFixTailTOF(iSpecies, fixTailPerSpecies[iSpecies] ? kTRUE : kFALSE); // added
			SetFixMuTPC(iSpecies, kTRUE);
			SetFixSigmaTPC(iSpecies, kFALSE);
		}
	}
	
	DoFit();

	PrintFit(cFit, fitStage);
	PrintFitPull(cPull, fitStage);
	PrintFitTOFProjection(cTOFProj, fitStage, TPCbinMin, TPCbinMax);
	PrintFitTPCProjection(cTPCProj, fitStage, TOFbinMin, TOFbinMax);
	fitStage++;

	// // so far so good.

	// Fit everything except integral.
	cout<<Form("Fit stage %i.",fitStage)<<endl;
	SetFixNBgd(kTRUE);

	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies, peakincluded->At(iSpecies));
			SetFixIntegral(iSpecies, kTRUE);
			SetFixMuTOF(iSpecies, kFALSE);
			SetFixSigmaTOF(iSpecies, kFALSE);
			SetFixTailTOF(iSpecies, fixTailPerSpecies[iSpecies] ? kTRUE : kFALSE);
			SetFixMuTPC(iSpecies, kFALSE);
			SetFixSigmaTPC(iSpecies, kFALSE);
		}
	}
	
	DoFit();

	PrintFit(cFit, fitStage);
	PrintFitPull(cPull, fitStage);
	PrintFitTOFProjection(cTOFProj, fitStage, TPCbinMin, TPCbinMax);
	PrintFitTPCProjection(cTPCProj, fitStage, TOFbinMin, TOFbinMax);
	fitStage++;


	// Fit only the integral.
	cout<<Form("Fit stage %i.",fitStage)<<endl;
	SetFixNBgd(kTRUE);

	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies, peakincluded->At(iSpecies));
			SetFixIntegral(iSpecies, kFALSE);
			SetFixMuTOF(iSpecies, kTRUE);
			SetFixSigmaTOF(iSpecies, kTRUE);
			SetFixTailTOF(iSpecies, kTRUE);
			SetFixMuTPC(iSpecies, kTRUE);
			SetFixSigmaTPC(iSpecies, kTRUE);
		}
	}
	
	DoFit();

	PrintFit(cFit, fitStage);
	PrintFitPull(cPull, fitStage);
	PrintFitTOFProjection(cTOFProj, fitStage, TPCbinMin, TPCbinMax);
	PrintFitTPCProjection(cTPCProj, fitStage, TOFbinMin, TOFbinMax);
	fitStage++;

	if (fitstages) {
		fitstages->AddLast(cFit->Clone());
		fitstages->AddLast(cPull->Clone());
		fitstages->AddLast(cTOFProj->Clone());
		fitstages->AddLast(cTPCProj->Clone());
	}

	delete cFit;
	delete cPull;
	delete cTOFProj;
	delete cTPCProj;

	return 0;

}

// -----------------------------------------------------------------------
Int_t AliFitterTOFTPCDiHadronPID::DoFitWithStrategyppDefault(TObjArray* fitstages, Int_t mismatchrange) {

	// Description: 
	//  * Fit pi/ka/pr peaks where overlap is significant.
	//  * Don't fit deuterons (excluded from the range).
	//  * Measure background in sidebands, and keep it fixed.
	//  * Npar's: 5 - 15.

	// Implementation of the default fit strategy of PbPb histograms.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Perform initial estimates.
	EstimateTOFPositions();					// Standard estimate.
	EstimateTPCPositions();					// Standard estimate.
	if (mismatchrange != -1) {
		EstimateNMismatch(mismatchrange);	// Estimate in sideband.
	} else {
		fMismatch = 0x0; 					// delete mismatches.
		SetFixNBgd(kTRUE);
	}
	EstimateNParticles();					// Estimate using expected peak positons (not very good when fitting peak/bulk regions)

	SetRangeppDefault();					// Range never includes the deuterons.
	TArrayI* peakincluded = GetPeakDescriptionppDefault();

	// Create TCanvases for the fit results.
	TCanvas* cFit = TCanvas::MakeDefCanvas();
	cFit->SetName("Fit");
	cFit->SetTitle("Fit");
	cFit->Divide(3,2);

	TCanvas* cPull = TCanvas::MakeDefCanvas();
	cPull->SetName("Pull");
	cPull->SetTitle("Pull");	
	cPull->Divide(3,2);

	TCanvas* cTOFProj = TCanvas::MakeDefCanvas();
	cTOFProj->SetName("TOFProjection");
	cTOFProj->SetTitle("TOFProjection");
	cTOFProj->Divide(3,2);

	TCanvas* cTPCProj = TCanvas::MakeDefCanvas();
	cTPCProj->SetName("TPCProjection");
	cTPCProj->SetTitle("TPCProjection");
	cTPCProj->Divide(3,2);

	Int_t fitStage = 1;
	// 1) Print Initial estimates.
	cout<<Form("Fit stage %i.",fitStage)<<endl<<"Initial estimation."<<endl;
	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies, peakincluded->At(iSpecies));
		}
	}

	TAxis* TOFaxis = fData->GetXaxis();
	TAxis* TPCaxis = fData->GetYaxis();
	Int_t TOFbinMin = TOFaxis->FindBin(GetTOFmin());
	Int_t TOFbinMax = TOFaxis->FindBin(GetTOFmax());
	Int_t TPCbinMin = TPCaxis->FindBin(GetTPCmin());
	Int_t TPCbinMax = TPCaxis->FindBin(GetTPCmax()); 

	PrintFit(cFit, fitStage);
	PrintFitPull(cPull, fitStage);
	PrintFitTOFProjection(cTOFProj, fitStage, TPCbinMin, TPCbinMax);
	PrintFitTPCProjection(cTPCProj, fitStage, TOFbinMin, TOFbinMax);
	fitStage++;

	// 2) Fit integral mu
	cout<<Form("Fit stage %i.",fitStage)<<endl;
	SetFixNBgd(kTRUE);
	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies, peakincluded->At(iSpecies));
			SetFixIntegral(iSpecies, kFALSE); 
			SetFixMuTOF(iSpecies, kFALSE);
			SetFixSigmaTOF(iSpecies, kTRUE);
			SetFixTailTOF(iSpecies,kTRUE);
			SetFixMuTPC(iSpecies, kFALSE);
			SetFixSigmaTPC(iSpecies, kTRUE);
		}
	}
	
	DoFit();
	
	PrintFit(cFit, fitStage);
	PrintFitPull(cPull, fitStage);
	PrintFitTOFProjection(cTOFProj, fitStage, TPCbinMin, TPCbinMax);
	PrintFitTPCProjection(cTPCProj, fitStage, TOFbinMin, TOFbinMax);
	fitStage++;

	// 3) Fit integral and sigmas, fix mu
	cout<<Form("Fit stage %i.",fitStage)<<endl;
	SetFixNBgd(kTRUE);

	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies, peakincluded->At(iSpecies));
			SetFixIntegral(iSpecies, kTRUE); 
			SetFixMuTOF(iSpecies, kTRUE);
			SetFixSigmaTOF(iSpecies, kFALSE);
			SetFixTailTOF(iSpecies, kTRUE);
			SetFixMuTPC(iSpecies, kTRUE);
			SetFixSigmaTPC(iSpecies, kFALSE);
		}
	}
	
	DoFit();

	PrintFit(cFit, fitStage);
	PrintFitPull(cPull, fitStage);
	PrintFitTOFProjection(cTOFProj, fitStage, TPCbinMin, TPCbinMax);
	PrintFitTPCProjection(cTPCProj, fitStage, TOFbinMin, TOFbinMax);
	fitStage++;

	// 5) Release Tails if applicable (from right to left in TOF).
	cout<<Form("Fit stage %i.",fitStage)<<endl;
	SetFixNBgd(kTRUE);
	for (Int_t iSpecies = 2; iSpecies >= 0; iSpecies--) {
		SetUsedFunction(iSpecies, peakincluded->At(iSpecies));
		
		// Release tails of one peak.
		if (peakincluded->At(iSpecies) == 2) {
			SetFixIntegral(iSpecies, kTRUE);
			SetFixMuTOF(iSpecies, kTRUE);
			SetFixSigmaTOF(iSpecies, kTRUE);
			SetFixTailTOF(iSpecies,kFALSE);
			SetFixMuTPC(iSpecies, kTRUE);
			SetFixSigmaTPC(iSpecies, kTRUE );

			// and fix all the rest.
			for (Int_t jSpecies = 0; jSpecies < 3; jSpecies++) {
				if (jSpecies == iSpecies) {continue;}
				SetFixIntegral(jSpecies,kTRUE);
				SetFixMuTOF(jSpecies,kTRUE);
				SetFixSigmaTOF(jSpecies,kTRUE);
				SetFixTailTOF(jSpecies,kTRUE);	
				SetFixMuTPC(jSpecies,kTRUE);
				SetFixSigmaTPC(jSpecies,kTRUE);
			}

			DoFit();

		}

	} 

	PrintFit(cFit, fitStage);
	PrintFitPull(cPull, fitStage);
	PrintFitTOFProjection(cTOFProj, fitStage, TPCbinMin, TPCbinMax);
	PrintFitTPCProjection(cTPCProj, fitStage, TOFbinMin, TOFbinMax);
	fitStage++;

	// 6) Release everything EXCEPT tof tail. TODO FIXME
	cout<<Form("Fit stage %i.",fitStage)<<endl;
	SetFixNBgd(kTRUE);
	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		if (peakincluded->At(iSpecies)) {
			SetUsedFunction(iSpecies, peakincluded->At(iSpecies));
			SetFixIntegral(iSpecies,kFALSE);
			SetFixMuTOF(iSpecies,kFALSE);
			SetFixSigmaTOF(iSpecies,kFALSE);
			SetFixTailTOF(iSpecies,kTRUE);
			SetFixMuTPC(iSpecies,kFALSE);
			SetFixSigmaTPC(iSpecies,kFALSE);
		}
	}
	DoFit();

	PrintFit(cFit, fitStage);
	PrintFitPull(cPull, fitStage);
	PrintFitTOFProjection(cTOFProj, fitStage, TPCbinMin, TPCbinMax);
	PrintFitTPCProjection(cTPCProj, fitStage, TOFbinMin, TOFbinMax);
	fitStage++;

	if (fitstages) {
		fitstages->AddLast(cFit->Clone());
		fitstages->AddLast(cPull->Clone());
		fitstages->AddLast(cTOFProj->Clone());
		fitstages->AddLast(cTPCProj->Clone());
	}

	delete cFit;
	delete cPull;
	delete cTOFProj;
	delete cTPCProj;

	return 0;

}

// -----------------------------------------------------------------------
void AliFitterTOFTPCDiHadronPID::FixParameters(const TArrayI* pars) {

	// Not too pretty way of make sure that all the desired parameters
	// are fixed in the DoFitWithStrategy function.

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	for (Int_t iPar = 0; iPar < pars->GetSize(); ++iPar) {
		if (pars->At(iPar)) {
			SetParameter(iPar, GetFitParameterFromParameterisationPbPbDefault(iPar));
			SetFixParameter(iPar);
		}
	}

}

