#ifndef ALIMTFILEMERGERDIHADRONPID_H
#define ALIMTFILEMERGERDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TString.h"
#include "TObjArray.h"
#include "TObjString.h"

class AliMTFileMergerDiHadronPID : public TObject {

public:

	AliMTFileMergerDiHadronPID();
	~AliMTFileMergerDiHadronPID();

	void					PrintStatus();
	Bool_t					AddFile(TString filename);
	Bool_t					SetOutputFile(TString filename);
	Bool_t					OutputFile(TString filename) {return SetOutputFile(filename);} 	// Consistent with TFileMerger interface.
	void					GenerateFileList(TString filename, TString path = ".");			// Adds all files in path with name filename.
	void					SetMaxThreads(Int_t nThreads);
	Bool_t					Merge();

private:
	static void*			merge(void *list);

private:

	Int_t					fMaxThreads;
	TString					fOutputFile;
	TObjArray				fFileList;

	ClassDef(AliMTFileMergerDiHadronPID, 1);

};

#endif
