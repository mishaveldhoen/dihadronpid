/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  This class processes the output of the AliAnalysisTaskDiHadronPID 
//  task.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

// Class Header.
#include "AliCorrelationDiHadronPID.h"

// Basic Includes.
#include <iostream>
using namespace std;

#include "TCanvas.h"
#include "TIterator.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "THn.h"

// -----------------------------------------------------------------------
AliCorrelationDiHadronPID::AliCorrelationDiHadronPID():
	fDPhiAxis(new TAxis(32,-TMath::Pi()/2.,3.*TMath::Pi()/2.)),
	fDEtaAxis(new TAxis(32,-1.6,1.6)),
	fPtAxis(0x0),	
	fMixedEvents(0x0),
	fCorrelationsUI(0x0),				
	fCorrelationsPID(0x0),	
	fUsedDetectors("TOF"),		
	fBufferIsFilled(kFALSE),
	fMixedEventsAreNormalized(kFALSE),
	fCorrelationsUIAreCorrected(kFALSE),
	fCorrelationsPIDAreCorrected(kFALSE),
	fDebug(0)

{

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

}

// -----------------------------------------------------------------------
AliCorrelationDiHadronPID::AliCorrelationDiHadronPID(const TList* inputList, const TString& detector):
	fDPhiAxis(0x0),
	fDEtaAxis(0x0),
	fPtAxis(0x0),	
	fMixedEvents(0x0),
	fCorrelationsUI(0x0),				
	fCorrelationsPID(0x0),
	fUsedDetectors(detector),			
	fBufferIsFilled(kFALSE),
	fMixedEventsAreNormalized(kFALSE),
	fCorrelationsUIAreCorrected(kFALSE),
	fCorrelationsPIDAreCorrected(kFALSE),
	fDebug(0)

{

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}


	CopyObjectsToBuffer(inputList);

}

// -----------------------------------------------------------------------
AliCorrelationDiHadronPID::~AliCorrelationDiHadronPID() {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	CleanBuffer();

}

// -----------------------------------------------------------------------
Bool_t AliCorrelationDiHadronPID::CopyObjectsToBuffer(const TList* list) {

	// Copies the results of the analysis output to the local buffer.
	// -> Statistical error is calculated of all histograms in the buffer.

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!list) {cout << Form("%s -> ERROR: No list supplied.",__func__) << endl; return 0;}
	if (fBufferIsFilled)  {cout << Form("%s -> INFO: Buffer is already filled. First call CleanBuffer().",__func__) << endl; return fBufferIsFilled;}

	// Check if the right histograms are available.
	if (list->FindObject(Form("fPtSpectrum%sbins", fUsedDetectors.Data()))) {
		cout << Form("%s -> INFO: %s histograms found.",__func__,fUsedDetectors.Data()) << endl;	
	} else {
		cout << Form("%s -> ERROR: %s histograms not found.",__func__,fUsedDetectors.Data()) << endl;	
		return 0;
	}

	// Loading the mixed events and UI correlations.
	const TH3F* MixedEventsTmp = (const TH3F*)list->FindObject(Form("fMixedEvents%sbins",fUsedDetectors.Data()));
	const TH3F* CorrelationsUITmp = (const TH3F*)list->FindObject(Form("fCorrelations%sbins",fUsedDetectors.Data()));

	// Cleanup the DPhi and DEta axes, which are created by the default ctor.
	if (fDPhiAxis) {delete fDPhiAxis; fDPhiAxis = 0x0;}
	if (fDEtaAxis) {delete fDEtaAxis; fDEtaAxis = 0x0;}	

	// Clone the Axes.
	fDPhiAxis = (TAxis*)(MixedEventsTmp->GetXaxis())->Clone();
	fDEtaAxis = (TAxis*)(MixedEventsTmp->GetYaxis())->Clone();
	fPtAxis = (TAxis*)(MixedEventsTmp->GetZaxis())->Clone();
	cout << Form("%s -> INFO: Axes cloned.",__func__) << endl;	

	// Putting projections of the mixed events and UI correlations into TObjArrays.
	fMixedEvents = new TObjArray(10); fMixedEvents->SetName("fMixedEvents");
	fCorrelationsUI = new TObjArray(10); fCorrelationsUI->SetName("fCorrelationsUI");

	TH2F* htmp = 0x0;
	for (Int_t iPtBin = 1; iPtBin < (fPtAxis->GetNbins() + 1); iPtBin++) {
		htmp = (TH2F*)Project3DOnXY(MixedEventsTmp, iPtBin, kTRUE);
		if (((TH1*)htmp)->GetSumw2N() == 0) {((TH1*)htmp)->Sumw2();}
		fMixedEvents->AddLast(htmp);
		htmp = (TH2F*)Project3DOnXY(CorrelationsUITmp, iPtBin, kTRUE);
		if (((TH1*)htmp)->GetSumw2N() == 0) {((TH1*)htmp)->Sumw2();}
		fCorrelationsUI->AddLast(htmp);
	}
	htmp = 0x0;
	cout << Form("%s -> INFO: UI Correlations and Mixed Events imported.",__func__) << endl;	

	const TObjArray* CorrelationsTmp = (const TObjArray*)list->FindObject(Form("Correlations%s",fUsedDetectors.Data())); 
	if (CorrelationsTmp) {
		fCorrelationsPID = (TObjArray*)CorrelationsTmp->Clone();
		cout << Form("%s -> INFO: PID Correlations imported.",__func__) << endl;	
	} else {
		cout << Form("%s -> ERROR: PID Correlations not imported.",__func__) << endl;			
		return fBufferIsFilled;
	}

	fBufferIsFilled = kTRUE;

	return fBufferIsFilled;

}

// -----------------------------------------------------------------------
void AliCorrelationDiHadronPID::CleanBuffer() {

	// Throw away the buffer's contents.

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	if (fDPhiAxis) {delete fDPhiAxis; fDPhiAxis = 0x0;}
	if (fDEtaAxis) {delete fDEtaAxis; fDEtaAxis = 0x0;}
	if (fPtAxis) {delete fPtAxis; fPtAxis = 0x0;}					
	if (fMixedEvents) {delete fMixedEvents; fMixedEvents = 0x0;}			
	if (fCorrelationsUI) {delete fCorrelationsUI; fCorrelationsUI = 0x0;}			
	if (fCorrelationsPID) {delete fCorrelationsPID; fCorrelationsPID = 0x0;}				

	fUsedDetectors = "TOF";

	fBufferIsFilled = kFALSE;
	fMixedEventsAreNormalized = kFALSE;
	fCorrelationsUIAreCorrected = kFALSE;
	fCorrelationsPIDAreCorrected = kFALSE;

}

// -----------------------------------------------------------------------
//  Operations on histograms (should check the ID of hist).
// -----------------------------------------------------------------------
void AliCorrelationDiHadronPID::ScaleMaxBinToOne(TObject* hist, const TObject*, const TString&) {

	// Scales the histogram, such that the maxiumum bin is 1.
	// Accepts TH1,TH2,TH3.

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!hist) {cout << Form("%s -> ERROR: No histogram supplied.",__func__) << endl; return;}
	if (!( ((TH1*)hist)->GetSumw2N()) ) {cout << Form("%s -> WARNING: No errors were defined for histogram.",__func__) << endl; return;}

	// Checking the identity of the object.
	TString classname(hist->ClassName());
	if (!classname.Contains("TH") || classname.Contains("THn")) {
		cout << Form("ERROR: Operation %s does not work on %s.", __func__, classname.Data()) << endl; 
		return;
	}

	// Scale histogram with value in the largest bin.
	Int_t maxbin = ((TH1*)hist)->GetMaximumBin();
	Float_t maxval = ((TH1*)hist)->GetBinContent(maxbin);
	((TH1*)hist)->Scale(1./maxval);

}

// -----------------------------------------------------------------------
void AliCorrelationDiHadronPID::DivideBy2DHist(TObject* hist, const TObject* hist2D, const TString&) {

	// Divides the histogram "hist" through a 2D histogram hist2D,
	// (for example mixed events).

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!hist) {cout << Form("%s -> ERROR: First argument is null.",__func__) << endl; return;}
	if (!hist2D) {cout << Form("%s -> ERROR: Second argument is null.",__func__) << endl; return;}

	// Checking the identities of the objects.
	TString classname(hist->ClassName());
	TString classname2D(hist2D->ClassName());
	if ((!classname.Contains("TH2") && !classname.Contains("TH3")) || !classname2D.Contains("TH2")) {
		cout << Form("ERROR: Operation %s cannot be used to let an object of type %s act on an object of type %s.", __func__, classname2D.Data(), classname.Data()) << endl; 
		return;
	}

	// Flag for calculating errors.
	//Bool_t CalcErr = opt.Contains("e");
	Float_t hval = 0., herr = 0., h2val = 0., h2err = 0., rval = 0., rerr = 0.; 

	// Loop over the DPhi axis.
	for (Int_t iBinX = 1; iBinX < (((TH1*)hist)->GetNbinsX() + 1); iBinX++) {

		// Loop over DEta axis.
		for (Int_t iBinY = 1; iBinY < (((TH1*)hist)->GetNbinsY() + 1); iBinY++) {

			h2val = ((TH2*)hist2D)->GetBinContent(iBinX, iBinY);
			/*if (CalcErr)*/ {h2err = ((TH2*)hist2D)->GetBinError(iBinX, iBinY);}
			//else {h2err = 0.;}

			// Loop over TOF axis (In case the histogram is 2D, GetNbinsZ will give 1).
			for (Int_t iBinZ = 1; iBinZ < (((TH1*)hist)->GetNbinsZ() + 1); iBinZ++) {

				hval = ((TH1*)hist)->GetBinContent(iBinX, iBinY, iBinZ);
				/*if (CalcErr)*/ {herr = ((TH1*)hist)->GetBinError(iBinX, iBinY, iBinZ);}
				//else {herr = 0.;}
				
				rval = 0.;
				rerr = 0.;

				if (h2val > 10e-10) {
					rval = hval/h2val;
					/*if (CalcErr)*/ {rerr = RatioError(hval, herr, h2val, h2err);}
				}

				((TH1*)hist)->SetBinContent(iBinX, iBinY, iBinZ, rval);
				/*if (CalcErr)*/ {((TH1*)hist)->SetBinError(iBinX, iBinY, iBinZ, rerr);}

			} // iBinZ

		} // iBinY
	
	} // iBinX

}

// -----------------------------------------------------------------------
void AliCorrelationDiHadronPID::DivideTHnBy2DHist(TObject* hist, const TObject* hist2D, const TString&) {

	// Divides the THn "hist" through a 2D histogram hist2D,
	// (for example mixed events).

	// Note that it's hard to combine this method with the DivideBy2DHist method,
	// since THn doesn't inherit from TH1.

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!hist) {cout << Form("%s -> ERROR: First argument is null.",__func__) << endl; return;}
	if (!hist2D) {cout << Form("%s -> ERROR: Second argument is null.",__func__) << endl; return;}

	// Checking the identities of the objects.
	TString classname(hist->ClassName());
	TString classname2D(hist2D->ClassName());
	if ((!classname.Contains("THn")) || !classname2D.Contains("TH2")) {
		cout << Form("ERROR: Operation %s cannot be used to let an object of type %s act on an object of type %s.", __func__, classname2D.Data(), classname.Data()) << endl; 
		return;
	}

	if (((THn*)hist)->GetNdimensions() != 4) {
		cout << Form("ERROR: Operation %s cannot be used to on a THn with dimenstion %i.", __func__, ((THn*)hist)->GetNdimensions()) << endl; 
		return;
	}

	// Flag for calculating errors.
	Float_t hval = 0., herr = 0., h2val = 0., h2err = 0., rval = 0., rerr = 0.; 
	TObjArray* listofaxes = ((THn*)hist)->GetListOfAxes();

	// Loop over the DPhi axis.
	for (Int_t iBinX = 1; iBinX < (((TAxis*)listofaxes->At(0))->GetNbins() + 1); iBinX++) {

		// Loop over DEta axis.
		for (Int_t iBinY = 1; iBinY < (((TAxis*)listofaxes->At(1))->GetNbins() + 1); iBinY++) {

			h2val = ((TH2*)hist2D)->GetBinContent(iBinX, iBinY);
			h2err = ((TH2*)hist2D)->GetBinError(iBinX, iBinY);

			// Loop over TOF and TPC axis.
			for (Int_t iBin2 = 1; iBin2 < (((TAxis*)listofaxes->At(2))->GetNbins() + 1); iBin2++) {
				for (Int_t iBin3 = 1; iBin3 < (((TAxis*)listofaxes->At(3))->GetNbins() + 1); iBin3++) {

					Int_t coords[4] = {iBinX, iBinY, iBin2, iBin3};

					hval = ((THn*)hist)->GetBinContent(coords);
					herr = TMath::Sqrt(hval); // Not a great approximation for small n.

					rval = 0.;
					rerr = 0.;

					if (h2val > 10e-10) {
						rval = hval/h2val;
						rerr = RatioError(hval, herr, h2val, h2err);
					}

					((THn*)hist)->SetBinContent(coords, rval);
					((THn*)hist)->SetBinError(coords, rerr);

				}
			} // iBinZ

		} // iBinY
	
	} // iBinX

}

// -----------------------------------------------------------------------
void AliCorrelationDiHadronPID::MultiplyBy2DMap(TObject* hist, const TObject* map2D, const TString&) {

	// Multiplies histogram "hist" with a 2D map map2D, of type TH2I.
	// Multiplication is done on the first two axes, which are (DPhi,DEta) by default.

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!hist) {cout << Form("%s -> ERROR: First argument is null.",__func__) << endl; return;}
	if (!map2D) {cout << Form("%s -> ERROR: Second argument is null.",__func__) << endl; return;}

	// Checking the identities of the objects.
	TString classname(hist->ClassName());
	TString classname2D(map2D->ClassName());
	if ((!classname.Contains("TH2") && !classname.Contains("TH3")) || !classname2D.Contains("TH2I")) {
		cout << Form("ERROR: Operation %s cannot be used to let an object of type %s act on an object of type %s.", __func__, classname2D.Data(), classname.Data()) << endl; 
		return;
	}

	Int_t h2val = 0;

	// Loop over the DPhi axis.
	for (Int_t iBinX = 1; iBinX < (((TH1*)hist)->GetNbinsX() + 1); iBinX++) {

		// Loop over DEta axis.
		for (Int_t iBinY = 1; iBinY < (((TH1*)hist)->GetNbinsY() + 1); iBinY++) {

			h2val = ((TH2I*)map2D)->GetBinContent(iBinX, iBinY);
			if (!h2val) {

				// Loop over TOF axis (In case the histogram is 2D, GetNbinsZ will give 1).
				for (Int_t iBinZ = 1; iBinZ < (((TH1*)hist)->GetNbinsZ() + 1); iBinZ++) {

					((TH1*)hist)->SetBinContent(iBinX, iBinY, iBinZ, 0.);
					((TH1*)hist)->SetBinError(iBinX, iBinY, iBinZ, 0.);

				} // iBinZ

			} // if statement

		} // iBinY
	
	} // iBinX

}

// -----------------------------------------------------------------------
void AliCorrelationDiHadronPID::MultiplyTHnBy2DMap(TObject* hist, const TObject* map2D, const TString&) {

	// Multiplies THn "hist" with a 2D map map2D, of type TH2I,
	// (for example a region in (DPhi,DEta)).	

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!hist) {cout << Form("%s -> ERROR: First argument is null.",__func__) << endl; return;}
	if (!map2D) {cout << Form("%s -> ERROR: Second argument is null.",__func__) << endl; return;}

	// Checking the identities of the objects.
	TString classname(hist->ClassName());
	TString classname2D(map2D->ClassName());
	if (!classname.Contains("THn") && !classname2D.Contains("TH2I")) {
		cout << Form("ERROR: Operation %s cannot be used to let an object of type %s act on an object of type %s.", __func__, classname2D.Data(), classname.Data()) << endl; 
		return;
	}

	if (((THn*)hist)->GetNdimensions() != 4) {
		cout << Form("ERROR: Operation %s cannot be used to on a THn with dimenstion %i.", __func__, ((THn*)hist)->GetNdimensions()) << endl; 
		return;
	}

	Int_t h2val = 0;
	TObjArray* listofaxes = ((THn*)hist)->GetListOfAxes();

	// Loop over the DPhi axis.
	for (Int_t iBinX = 1; iBinX < (((TAxis*)listofaxes->At(0))->GetNbins() + 1); iBinX++) {

		// Loop over DEta axis.
		for (Int_t iBinY = 1; iBinY < (((TAxis*)listofaxes->At(1))->GetNbins() + 1); iBinY++) {

			h2val = ((TH2I*)map2D)->GetBinContent(iBinX, iBinY);
			if (!h2val) {

				// Loop over TOF and TPC axis.
				for (Int_t iBin2 = 1; iBin2 < (((TAxis*)listofaxes->At(2))->GetNbins() + 1); iBin2++) {
					for (Int_t iBin3 = 1; iBin3 < (((TAxis*)listofaxes->At(3))->GetNbins() + 1); iBin3++) {

						Int_t coords[4] = {iBinX, iBinY, iBin2, iBin3};

						((THn*)hist)->SetBinContent(coords, 0.);
						((THn*)hist)->SetBinError(coords, 0.);

					} // Axis 3
				} // Axis 2

			} // if statement

		} // iBinY
	
	} // iBinX

}

// -----------------------------------------------------------------------
void AliCorrelationDiHadronPID::SelectMassAssumption(TObject* hist, const TObject*, const TString&) {

	// Selects nine bins around the mass assumption.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!hist) {cout << Form("%s -> ERROR: First argument is null.",__func__) << endl; return;}

	// Expect a 4D histogram.
	TString classname(hist->ClassName());
	if (((THn*)hist)->GetNdimensions() != 4) {
		cout << Form("ERROR: Operation %s cannot be used to on a THn with dimenstion %i.", __func__, ((THn*)hist)->GetNdimensions()) << endl; 
		return;
	}

	TObjArray* listofaxes = ((THn*)hist)->GetListOfAxes();

	// Find the bin in the center of the mass assumed peak in (TPC,TOF).
	const Int_t centerBinTOF = ((TAxis*)listofaxes->At(2))->FindFixBin(0.);
	const Int_t centerBinTPC = ((TAxis*)listofaxes->At(3))->FindFixBin(0.);

	// Loop over the TPC axis.
	for (Int_t iBinTPC = 1; iBinTPC < (((TAxis*)listofaxes->At(3))->GetNbins() + 1); iBinTPC++) {

		// Loop over TOF axis.
		for (Int_t iBinTOF = 1; iBinTOF < (((TAxis*)listofaxes->At(2))->GetNbins() + 1); iBinTOF++) {

			// Ignore the mass assumed species in TOF.
			if (iBinTOF >= (centerBinTOF - 2) && iBinTOF <= (centerBinTOF + 2) && 
				iBinTPC >= (centerBinTPC - 2) && iBinTPC <= (centerBinTPC + 2)) {continue;}

			// Loop over the DPhi axis.
			for (Int_t iBinDPhi = 1; iBinDPhi < (((TAxis*)listofaxes->At(0))->GetNbins() + 1); iBinDPhi++) {
				for (Int_t iBinDEta = 1; iBinDEta < (((TAxis*)listofaxes->At(1))->GetNbins() + 1); iBinDEta++) {

					Int_t coords[4] = {iBinDPhi, iBinDEta, iBinTOF, iBinTPC};

					((THn*)hist)->SetBinContent(coords, 0.);
					((THn*)hist)->SetBinError(coords, 0.);

				}

			}

		}

	}

}

// -----------------------------------------------------------------------
void AliCorrelationDiHadronPID::OperateOnBuffer(TObjArray* buffer, Operation opr, const TObject* hist, const TString& opt) {

	// Carries out an operation on the buffer. The operation may take as an argument
	// a constant object (hist) and some options (opt).

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!buffer) {cout << Form("%s -> ERROR: No buffer supplied.",__func__) << endl; return;}

	// Create an iterator for the buffer. The buffer may contain
	// TObjArray's (ID Correlations), or Histograms (UI Buffers)
	TIterator* Iterator = buffer->MakeIterator();
	TObject* currentItem = Iterator->Next();

	while (currentItem) {

		// If the currentItem is a TObjArray, then we call ourselves.
		if (TString(currentItem->ClassName()).Contains("TObjArray")) {
			OperateOnBuffer((TObjArray*)currentItem, opr, hist, opt);
		} else {
			(this->*opr)(currentItem, hist, opt);
		}

		currentItem = Iterator->Next();

	}
}

// -----------------------------------------------------------------------
void AliCorrelationDiHadronPID::OperateOnBuffer(TObjArray* buffer1, const TObjArray* buffer2, Operation opr, const TString& opt) {

	// Carries out an operation the buffer1. The operation takes as argument 
	// a histogram from buffer2, and some options (opt).

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!buffer1) {cout << Form("%s -> ERROR: First buffer not supplied.",__func__) << endl; return;}
	if (!buffer2) {cout << Form("%s -> ERROR: Second buffer not supplied.",__func__) << endl; return;}
	//if (buffer1->GetEntriesFast() != buffer2->GetEntriesFast()) {cout << Form("%s -> ERROR: Supplied buffers of unequal size.",__func__) << endl; return;}

	// Create an iterator for the buffers.
	TIterator* Iterator1 = buffer1->MakeIterator();
	TIterator* Iterator2 = buffer2->MakeIterator();

	TObject* currentItem1 = Iterator1->Next();
	Int_t i1 = 1, i2 = 1;
	TObject* currentItem2 = Iterator2->Next();

	while (currentItem1 && currentItem2) {

		cout<<buffer1->GetName()<<"("<<i1<<"): "<<currentItem1->ClassName()<<" mixed with "
			<<buffer2->GetName()<<"("<<i2<<"): "<<currentItem2->ClassName()<<endl;

		// If the currentItem is a TObjArray, then we call ourselves.
		if (TString(currentItem1->ClassName()).Contains("TObjArray")) {
			if (TString(currentItem2->ClassName()).Contains("TObjArray")) {
				OperateOnBuffer((TObjArray*)currentItem1, (TObjArray*)currentItem2, opr, opt);
				currentItem2 = Iterator2->Next();
				i2++;
			} else {
				// This case occurs when for example fCorrelationsTOF is operated
				// on by the mixed event buffer (which is not as deep)
				// NOTE: In this case the second iterator should not go to the next object.
				OperateOnBuffer((TObjArray*)currentItem1, (TObjArray*)buffer2, opr, opt);
			}
		} else {
			(this->*opr)(currentItem1, currentItem2, opt);
			currentItem2 = Iterator2->Next();
			i2++;
		}

		// The first iterator should always go to the next item.
		currentItem1 = Iterator1->Next();
		i1++;

	}
}

// -----------------------------------------------------------------------
//  Numerical property of a histogram.
// -----------------------------------------------------------------------

// TODO!

// -----------------------------------------------------------------------
//  Projection of a histogram.
// -----------------------------------------------------------------------
TObject* AliCorrelationDiHadronPID::Project3DOnXY(const TObject* hist, const Int_t& iPtBin, const Bool_t& ZisPt) const {
	
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!hist) {cout << Form("%s -> ERROR: No histogram supplied.",__func__) << endl; return 0x0;}

	// Check whether the histogram is 3D.
	if (!(TString(hist->ClassName()).Contains("TH3"))) {return 0x0;}
	
	if (ZisPt) {(((TH3*)hist)->GetZaxis())->SetRange(iPtBin,iPtBin);}
	else {(((TH3*)hist)->GetZaxis())->SetRange(1,((TH3*)hist)->GetNbinsZ());}
	TH2F* hout = (TH2F*)((TH3*)hist)->Project3D("yx"); // Bit strange, but otherwise the x,y axes get swapped...

	// Set the name in case a pt bin has been provided.
	if (iPtBin != -1) {
		hout->SetName(Form("%s_%i",hist->GetName(),iPtBin));
		Float_t minpt = fPtAxis->GetBinLowEdge(iPtBin);
		Float_t maxpt = fPtAxis->GetBinUpEdge(iPtBin);
		hout->SetTitle(Form("%s (%5.3f < p_{T} < %5.3f)",hist->GetTitle(),minpt,maxpt));
	}

	hout->SetDirectory(0);

	return hout;
}

// -----------------------------------------------------------------------
TObject* AliCorrelationDiHadronPID::Project3DOnZ(const TObject* hist, const Int_t&, const Bool_t&) const {
	
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!hist) {cout << Form("%s -> ERROR: No histogram supplied.",__func__) << endl; return 0x0;}

	if (!(TString(hist->ClassName()).Contains("TH3"))) {return 0x0;}		
	TH1F* hout = (TH1F*)((TH3*)hist)->Project3D("z");
	hout->SetDirectory(0);
	return hout;
}

// -----------------------------------------------------------------------
TObject* AliCorrelationDiHadronPID::ProjectTHn(const TObject* histnD, const Int_t&, const Bool_t& FirstTwoAxes) const {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!histnD) {cout << Form("%s -> ERROR: No histogram supplied.",__func__) << endl; return 0x0;}	
	if (!(TString(histnD->ClassName()).Contains("THn"))) {return 0x0;}	

	TH2F* hout = 0x0;

	// Exclude under/overflow.
	TObjArray* listofaxes = ((THn*)histnD)->GetListOfAxes();
	TAxis* axes[4];
	for (Int_t iAxis = 0; iAxis < 4; iAxis++) {
		axes[iAxis] = (TAxis*)(listofaxes->At(iAxis));
		axes[iAxis]->SetRange(1, axes[iAxis]->GetNbins());
	}

	if (FirstTwoAxes) {
		hout = (TH2F*)(((THn*)histnD)->Projection(1,0));
	} else {
		hout = (TH2F*)(((THn*)histnD)->Projection(3,2));
	}

	hout->SetDirectory(0);
	return hout;

}

// -----------------------------------------------------------------------
TObjArray* AliCorrelationDiHadronPID::ProjectionOfBuffer(const TObjArray* buffer, Projection proj, const Bool_t& opt) const {

	// Creates a buffer with the same structure as "buffer", filled with
	// the projected objects.

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!buffer) {cout << Form("%s -> ERROR: No buffer supplied.",__func__) << endl; return 0x0;}

	// Create an output TObjArray.
	TObjArray* buffout = new TObjArray(buffer->GetEntriesFast());
	buffout->SetName(buffer->GetName());
	buffout->SetOwner(kTRUE);

	// Create an iterator for the buffer. The buffer may contain
	// TObjArray's (ID Correlations), or Histograms (UI Buffers)
	TIterator* Iterator = buffer->MakeIterator();
	TObject* currentItem = Iterator->Next();
	Int_t iPtBin = 1;

	while (currentItem) {

		// If the currentItem is a TObjArray, then we call ourselves.
		if (TString(currentItem->ClassName()).Contains("TObjArray")) {
			buffout->AddLast(ProjectionOfBuffer((TObjArray*)currentItem, proj, opt));
		} else {
			// Note that we assume the Z axis of the histogram is TOF.
			buffout->AddLast((this->*proj)(currentItem, iPtBin, opt));
			iPtBin++;
		}
		currentItem = Iterator->Next();

	}

	return buffout;

}

// -----------------------------------------------------------------------
//  Shapes.
// -----------------------------------------------------------------------
Bool_t AliCorrelationDiHadronPID::Oval(const Float_t& DPhi, const Float_t& DEta, const TArrayF& specs) const {

	// An oval in (DPhi, DEta).
	// specs = {DPhi_center, DEta_center, DPhi_radius, DEta_radius}

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (specs.GetSize() != 4) {cout << Form("%s -> ERROR: Supplied %i parameters, where 4 are expected.",__func__,specs.GetSize()) << endl; return 0x0;}

	return ((TMath::Power((DPhi - specs[0]) / specs[2], 2) + TMath::Power((DEta - specs[1]) / specs[3], 2)) < 1.);

}

// -----------------------------------------------------------------------
Bool_t AliCorrelationDiHadronPID::Rectangle(const Float_t& DPhi, const Float_t& DEta, const TArrayF& specs) const {

	// A rectangle in (DPhi, DEta).
	// specs = {DPhi_min, DEta_min, DPhi_max, DEta_max}

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (specs.GetSize() != 4) {cout << Form("%s -> ERROR: Supplied %i parameters, where 4 are expected.",__func__,specs.GetSize()) << endl; return 0x0;}

	return ((DPhi > specs[0]) && (DEta > specs[1]) && (DPhi < specs[2]) && DEta < specs[3]);

}

// -----------------------------------------------------------------------
Bool_t AliCorrelationDiHadronPID::SymRectangles(const Float_t& DPhi, const Float_t& DEta, const TArrayF& specs) const {

	// A rectangle in (DPhi, DEta) plus it's mirror image in DEta.
	// specs = {DPhi_min, DEta_min, DPhi_max, DEta_max}

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (specs.GetSize() != 4) {cout << Form("%s -> ERROR: Supplied %i parameters, where 4 are expected.",__func__,specs.GetSize()) << endl; return 0x0;}
	if (specs[1] < 0.) {cout << Form("%s -> WARNING: DEta_min is expected to be positive.",__func__) << endl;}

	return (Rectangle(DPhi, DEta, specs) + Rectangle(DPhi, -DEta, specs));

}

// -----------------------------------------------------------------------
void AliCorrelationDiHadronPID::MultiplyExternalBufferBy2DMap(TObjArray* buffer, const TObject* map) {

	// Multiplies a supplied buffer by a 2D map.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!buffer) {cout << Form("%s -> ERROR: No buffer supplied.",__func__) << endl; return;}
	if (!map) {cout << Form("%s -> ERROR: No map supplied.",__func__) << endl; return;}
	if (!buffer->GetEntriesFast()) {cout << Form("%s -> ERROR: Buffer is empty.",__func__) << endl; return;}

	TObject* objTmp = buffer->At(0);
	TString objTmpClassName = TString(objTmp->ClassName());

	if (objTmpClassName.Contains("THn")) {
		OperateOnBuffer(buffer, &AliCorrelationDiHadronPID::MultiplyTHnBy2DMap, map);
	} else {
		OperateOnBuffer(buffer, &AliCorrelationDiHadronPID::MultiplyBy2DMap, map);
	}

}

// -----------------------------------------------------------------------
TObjArray* AliCorrelationDiHadronPID::GetCloneExternalBufferDPhiDEtaProj(const TObjArray* buffer) const {

	// Multiplies a supplied buffer by a 2D map.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!buffer) {cout << Form("%s -> ERROR: No buffer supplied.",__func__) << endl; return 0x0;}
	if (!buffer->GetEntriesFast()) {cout << Form("%s -> ERROR: Buffer is empty.",__func__) << endl; return 0x0;}

	TObject* objTmp = buffer->At(0);
	TString objTmpClassName = TString(objTmp->ClassName());

	if (objTmpClassName.Contains("THn")) {
		return ProjectionOfBuffer(buffer, &AliCorrelationDiHadronPID::ProjectTHn, kTRUE);
	} else {
		return ProjectionOfBuffer(buffer, &AliCorrelationDiHadronPID::Project3DOnXY);
	}

}

// -----------------------------------------------------------------------
TH2I* AliCorrelationDiHadronPID::CreateDPhiDEtaMap(Shape sh, const TArrayF& specs, const TString& title) const {

	// Returns a TH2I with a shape sh with specs marked with ones.

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fBufferIsFilled) {cout << Form("%s -> WARNING: Buffer not filled, using default ranges.",__func__) << endl;}

	// Create the map.
	TH2I* hout = new TH2I(title.Data(),Form("%s;#Delta#phi;#Delta#eta",title.Data()),
		fDPhiAxis->GetNbins(), fDPhiAxis->GetXmin(), fDPhiAxis->GetXmax(),
		fDEtaAxis->GetNbins(), fDEtaAxis->GetXmin(), fDEtaAxis->GetXmax());
	hout->SetDirectory(0);

	// Loop over all bins, and check whether the bin center falls within the shape.
	for (Int_t iBinDPhi = 1; iBinDPhi < (fDPhiAxis->GetNbins() + 1); iBinDPhi++) {
		for (Int_t iBinDEta = 1; iBinDEta < (fDEtaAxis->GetNbins() + 1); iBinDEta++) {

			Float_t currentDPhi = fDPhiAxis->GetBinCenter(iBinDPhi);
			Float_t currentDEta = fDEtaAxis->GetBinCenter(iBinDEta);

			// Sets bin content to 1 if the current bin center falls within the desired.
			hout->SetBinContent(iBinDPhi, iBinDEta, (this->*sh)(currentDPhi, currentDEta, specs) );

		}
	}

	return hout;

}
