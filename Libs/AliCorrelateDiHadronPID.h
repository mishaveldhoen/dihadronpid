#ifndef ALICORRELATEDIHADRONPID_H
#define ALICORRELATEDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TAxis.h"
#include "TH1D.h"
#include "TH2D.h"
#include "THn.h"
#include "TVectorF.h"
#include "AliAnalyzeDataDiHadronPID.h"
#include "AliMiniEventPoolDiHadronPID.h"

class AliCorrelateDiHadronPID : public AliAnalyzeDataDiHadronPID {
	
public:
	AliCorrelateDiHadronPID(const char* name, const char* title);
	~AliCorrelateDiHadronPID();

private:
	void							UserCreateOutput();
	void 							UserExec();
	void							UserFinalizeAnalysis();
	void							UserSaveOutput();

public:	
	void							MakeHistogramOfEventIndicesWithTriggers() {fEventsWithTriggers = new TVectorF(1000); fPool->SetCheckOutgoingEventFrequency();}
	void							SetMassAssumption(Int_t massAssumption) {fMassAssumption = ((massAssumption > -1 && massAssumption < 3) ? massAssumption : -1);}
	void 							Print(Option_t*) const;

private:
	AliMiniEventPoolDiHadronPID*	fPool;								//!
	TVectorF*						fEventsWithTriggers;	 			//! Keeps track of event indices that have triggers.

	// Settings.
	Int_t							fMassAssumption;					// If < 0 then no PID correlations are made.

	// Axes. 
	// In principle we would have liked to only talk to the axes through AliSettingsDiHadronPID::GetPtAxis() etc.,
	// however since that method returns a const TAxis*, and TAxis::FindBin is non-const, we need deep-copies...
	TAxis*							fPtAxis;							//!
	TAxis*							fEtaAxis;							//!
	TAxis*							fCentralityAxis;					//!
	TAxis*							fVtxZAxis;							//!

	// QA histograms.
	TH1D*							fNTotTriggers;						//!
    TH2D*                           fTriggersPhiEta;                    //!
	TH1D*							fPtHistogram; 						//!

	TH2D*							fNSamePairs; 						//!
	TH2D*							fNMixedPairs;						//!
	TH2D*							fNMixedOverSamePairs;				//!

	TH2D* 							fNSameTriggers;						//!
	TH2D*							fNMixedTriggers;					//!
	TH2D*							fNMixedOverSameTriggers;			//!

	// UI correlation histograms (DPhi, DEta), [pt, cent, vtxZ].
	TH2D****						fUICorrelations;					//!
	TH2D****						fUIMixedEvents;						//!
	TH2D****						fUICorrected;						//!

	// UI correlation histograms, sum before ME corr. (DPhi, DEta), [pt].
	TH2D**							fUICorrelationsSumBeforeMECorr;		//!
	TH2D**							fUIMixedEventsSumBeforeMECorr;		//!
	TH2D**							fUICorrectedSumBeforeMECorr;		//!

	// UI correlation histograms, sum after ME corr. (DPhi, DEta), [pt].
	TH2D**							fUICorrectedSumAfterMECorr;			//!

	// PID correlation histograms (DPhi, DEta, TOF, TPC), [pt].
	THnF**							fPIDCorrelations;					//!
	THnF**							fPIDMixedEvents;					//!

	// Inclusive histograms, to be compared to the di-hadron histograms.
	TH2F***							fSignalInclusive;					//!
	TH2F***							fMismatchInclusive;					//!

	ClassDef(AliCorrelateDiHadronPID, 1);

};

#endif
