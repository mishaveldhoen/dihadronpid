/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Definitions the mathematical functions used in the DiHadronPID
//  analysis.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "AliFunctionsDiHadronPID.h"

#include <iostream>
using namespace std;

#include "AliExternalTrackParam.h"
#include "TF1.h"

// -----------------------------------------------------------------------
AliFunctionsDiHadronPID::AliFunctionsDiHadronPID()

{

	// Constructor.

} 

// -----------------------------------------------------------------------
AliFunctionsDiHadronPID::~AliFunctionsDiHadronPID()

{

	// Destructor.

} 

// -----------------------------------------------------------------------
Int_t AliFunctionsDiHadronPID::Power(Int_t base, Int_t power) {

	// Power function for integers (not available in TMath).

	if (power > 0) {
		Int_t result = 1;
		for (Int_t ii = 0; ii < power; ++ii) {result *= base;}
		return result;
	} else {
		if (power == 0) {return 1;}
		else {
			cout << Form("%s::%s -> WARNING: Method doesn't work for negative powers.",__FILE__,__func__) << endl;
			return -999;
		}
	}

}

// -----------------------------------------------------------------------
Double_t AliFunctionsDiHadronPID::Gaussian1D(Double_t xx, Double_t integral, Double_t mu, Double_t sigma, Double_t binwidth) {

	// Note that this implementation basically takes the riemann sum, i.e., it returns f(x)*dx.
	Double_t norm = (binwidth*integral)/(TMath::Sqrt(2.*TMath::Pi())*TMath::Abs(sigma)); // abs to avoid trouble with neg sigma in sum
	Double_t gaussian = TMath::Exp(-(xx-mu)*(xx-mu)/(2.*sigma*sigma));

	return (norm*gaussian);

}

// -----------------------------------------------------------------------
Double_t AliFunctionsDiHadronPID::Gaussian1DTail(Double_t xx, Double_t integral, Double_t mu, Double_t sigma, Double_t tail, Double_t binwidth) {

	// Gaussian with exponential tail on the right, I is the integral.
	// For function definition see: FitFunctions.nb

        sigma = TMath::Abs(sigma); // protect against negative sigma
	Double_t kappa = mu + tail;

	if (mu >= kappa) return 0.; 	// Function becomes ill-defined.

	Double_t beta = sigma*sigma/(kappa-mu);
	//Double_t expo = ;
	Double_t norm1 = beta * SafeExp( -(mu-kappa)*(mu-kappa)/(2.*sigma*sigma) );
	Double_t norm2 = TMath::Sqrt(TMath::Pi()/2.)*sigma*TMath::Erfc( (mu-kappa)/(TMath::Sqrt2()*sigma) );
	Double_t norm = norm1 + norm2;

	Double_t funcleft = binwidth * (integral/norm)*SafeExp(-(xx-mu)*(xx-mu)/(2.*sigma*sigma));

	if (xx <= kappa) return funcleft;
	
	Double_t BB = SafeExp( (kappa*kappa-mu*mu)/(2.*sigma*sigma) );
	Double_t funcright = binwidth * (integral/norm)*BB*SafeExp(-xx/beta);

	return funcright;

}

// -----------------------------------------------------------------------
Double_t AliFunctionsDiHadronPID::Gaussian2D(Double_t xx, Double_t yy, Double_t integral, 
	Double_t mux, Double_t muy, Double_t sigmax, Double_t sigmay, 
	Double_t binwidthx, Double_t binwidthy) {

	// 2D Gaussian.
	Double_t GaussianX = Gaussian1D(xx, 1., mux, sigmax, binwidthx);
	Double_t GaussianY = Gaussian1D(yy, 1., muy, sigmay, binwidthy);

	return integral * GaussianX * GaussianY; 

}

// -----------------------------------------------------------------------
Double_t AliFunctionsDiHadronPID::Gaussian2DTailX(Double_t xx, Double_t yy, Double_t integral, 
	Double_t mux, Double_t muy, Double_t sigmax, Double_t sigmay, 
	Double_t tailx, Double_t binwidthx, Double_t binwidthy) {

	// 2D Gaussian with exponential tail in X direction.
	Double_t GaussianTailX = Gaussian1DTail(xx, 1., mux, sigmax, tailx, binwidthx);
	Double_t GaussianY = Gaussian1D(yy, 1., muy, sigmay, binwidthy);

	return integral * GaussianTailX * GaussianY;

}

// -----------------------------------------------------------------------
Double_t AliFunctionsDiHadronPID::Gaussian2DTailY(Double_t xx, Double_t yy, Double_t integral, 
	Double_t mux, Double_t muy, Double_t sigmax, Double_t sigmay, 
	Double_t taily, Double_t binwidthx, Double_t binwidthy) {

	// 2D Gaussian with exponential tail in Y direction.
	Double_t GaussianX = Gaussian1D(xx, 1., mux, sigmax, binwidthx);
	Double_t GaussianTailY = Gaussian1DTail(yy, 1., muy, sigmay, taily, binwidthy);

	return integral * GaussianX * GaussianTailY;

}

// -----------------------------------------------------------------------
Double_t AliFunctionsDiHadronPID::Gaussian2DTailXY(Double_t xx, Double_t yy, Double_t integral, 
	Double_t mux, Double_t muy, Double_t sigmax, Double_t sigmay, 
	Double_t tailx, Double_t taily, Double_t binwidthx, Double_t binwidthy) {

	// 2D Gaussian with exponential tail in X- and Y direction.
	Double_t GaussianTailX = Gaussian1DTail(xx, 1., mux, sigmax, tailx, binwidthx);
	Double_t GaussianTailY = Gaussian1DTail(yy, 1., muy, sigmay, taily, binwidthy);

	return integral * GaussianTailX * GaussianTailY;

}

// -----------------------------------------------------------------------
Double_t AliFunctionsDiHadronPID::IntegralGaussian1D(Double_t xMin, Double_t xMax, Double_t mu, Double_t sigma) {

	return (TMath::Erf( (xMax - mu) / (TMath::Sqrt2() * sigma)) - TMath::Erf( (xMin - mu) / (TMath::Sqrt2() * sigma))) / 2.;
}

// -----------------------------------------------------------------------
Double_t AliFunctionsDiHadronPID::IntegralGaussian1DTail(Double_t xMin, Double_t xMax, Double_t mu, Double_t sigma, Double_t lambda) {

        //
        //  N.B.: This integral is not normalised; need to use 'Norm' function to get normalisation constat for area = 1
        //  
	// For description see Analysis Note.
	if (lambda < 0.) {return 0.;}
	if (xMin > xMax) {return 0.;}

	Double_t kappa = lambda * sigma * sigma;
	Double_t muPlusKappa = mu + kappa;
	if (xMax < muPlusKappa) {return IntegralGaussian1D(xMin, xMax, mu, sigma);}
	else {

		Double_t BB = lambda * kappa / 2.;
		Double_t norm = SafeExp(BB) / (Sqrt2Pi() * lambda * TMath::Abs(sigma)); // Added abs to avoid trouble with negative sigma in fit
		Double_t maxExp = SafeExp(-lambda * (xMax - mu));

		if (xMin < muPlusKappa) {

			return (IntegralGaussian1D(xMin, muPlusKappa, mu, sigma) + norm * (SafeExp(-lambda * kappa) - maxExp));

		} else {

			return (norm * (SafeExp(-lambda * (xMin - mu)) - maxExp));

		}

	}

}

// -----------------------------------------------------------------------
Double_t AliFunctionsDiHadronPID::NormGaussian1DTail(Double_t totIntegral, Double_t sigma, Double_t lambda) {

	/**
	 * Returns the normalization factor for a Gaussian that is smoothly joined with an exponent.
	 */

        sigma = TMath::Abs(sigma);  // avoid trouble with negative sigma
	Double_t kappa = lambda * sigma * sigma;
	Double_t BB = lambda * kappa / 2.;
	Double_t norm = 2. / (Sqrt2Pi() * lambda * sigma);
	return ((2. * totIntegral) / (TMath::Erf(kappa / (TMath::Sqrt2() * sigma)) + norm * SafeExp(-BB) + 1));

}

// -----------------------------------------------------------------------
Double_t AliFunctionsDiHadronPID::PolyPenalty(Double_t xx, Double_t center, Double_t flatwidth, Int_t polyorder) {

	// Penalty function for a chi^2 fit. The function is defined as:
	// 1 											for |xx - center| < flatwidth,
	// (|xx - center| - flatwidth) ^ polyorder		for |xx - center| > flatwidth.

	Double_t fx = 1.;
	if (TMath::Abs(xx - center) > flatwidth) {
		fx = TMath::Power( (TMath::Abs(xx - center) - flatwidth), polyorder ) + 1.;
	}

	return fx;

}

// -----------------------------------------------------------------------
TCanvas* AliFunctionsDiHadronPID::TestPolyPenalty(Double_t range, Double_t center, Double_t flatwidth, Int_t polyorder) {

	// Creates an example of the TestPolyPenalty function.
	TF1* tf = new TF1("tf",Form("AliFunctionsDiHadronPID::PolyPenalty(x,[0],[1],%i)",polyorder),-range,range);
	tf->SetParameters(center,flatwidth);
	TCanvas* cvs = TCanvas::MakeDefCanvas();
	tf->Draw();

	return cvs;

}

// -----------------------------------------------------------------------
Double_t AliFunctionsDiHadronPID::GetAbsAvgEta(Double_t mineta, Double_t maxeta) {

 	// Returns the absolute average of the eta interval.
 	if (TMath::Abs(mineta) < 1.e-10 || TMath::Abs(maxeta) < 1.e-10 || (mineta < 0. && maxeta < 0.) || (mineta > 0. && maxeta > 0.)) {
 		return ((TMath::Abs(mineta) + TMath::Abs(maxeta)) / 2.);
 	} else {
 		return ( (mineta * mineta + maxeta * maxeta) / (2. * (TMath::Abs(mineta) + TMath::Abs(maxeta)) ));
 	}

}

// -----------------------------------------------------------------------
Double_t AliFunctionsDiHadronPID::Eta(Int_t species, Double_t pT, Double_t Y) {

	// returns Eta for given species, pT and rapidity.
	return (TMath::ASinH( TMath::SinH(Y) * TMath::Sqrt(1 + M(species)*M(species)/(pT*pT))));

}

// -----------------------------------------------------------------------
void AliFunctionsDiHadronPID::ConstFit(Double_t* data, Double_t* errors, Int_t nDim, Double_t& fit, Double_t& fiterr) {

	// This method performs a constant chi^2 fit on the data array.
	fit = 0.;
	fiterr = 0.;
	Double_t fiterr_tmp = 0.;
	for (Int_t iPoint = 0; iPoint < nDim; iPoint++) {

		// Ignore empty bins...
		if (data[iPoint] < 1e-100 || errors[iPoint] < 1e-100) {continue;}

		fit += (data[iPoint] / (errors[iPoint] * errors[iPoint]));
		fiterr_tmp += (1. / (errors[iPoint] * errors[iPoint]));

	}

	fit /= fiterr_tmp;
	fiterr = TMath::Sqrt(1./fiterr_tmp);

}

// -----------------------------------------------------------------------
Double_t AliFunctionsDiHadronPID::TOFExpTime(Double_t pT, Double_t eta, Double_t mass) {

	// For description see ../Documents/TOFtime.tex

	Double_t AA = (2. * pT) / ( Charge() * BTPC() * GeVperkg() );
	Double_t BB = TMath::ASin( (Charge() * BTPC() * 0.01 * RTOF() * GeVperkg() ) / (2. * pT * C()) ); 
	Double_t CC = TMath::Sqrt( mass*mass/(pT*pT) + TMath::CosH(eta)*TMath::CosH(eta) );

	return (1.e12*AA*BB*CC);   // Time returned in ps.

}

// -----------------------------------------------------------------------
Double_t AliFunctionsDiHadronPID::TPCExpdEdX(Double_t pT, Double_t eta, Double_t mass) {

	// Not so neat solution, however the easiest for now.

	// Prameters taken from the constructor of AliTPCPIDResponse:
	Double_t MIP = 50.;
	Double_t Kp[5] = {0.0283086, 2.63394e+01, 5.04114e-11, 2.12543, 4.88663};

	Double_t betaGamma = TMath::Abs( (pT * TMath::CosH(eta)) / mass );

	// Implementation as in AliTPCPIDResponse.
	return MIP * AliExternalTrackParam::BetheBlochAleph(betaGamma,Kp[0],Kp[1],Kp[2],Kp[3],Kp[4]);

}

// -----------------------------------------------------------------------
Double_t AliFunctionsDiHadronPID::ErrorRatio(Double_t x1, Double_t e1, Double_t x2, Double_t e2) {

	// Calculates the error on y = x1/x2, assuming V_12 = 0.

	Double_t err2 = (x1*x1*e2*e2 + x2*x2*e1*e1)/(x2*x2*x2*x2);

	return TMath::Sqrt(err2);

}
	
