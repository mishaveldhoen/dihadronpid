/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Figure Class
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "AliFigureDiHadronPID.h"
#include "AliLogDiHadronPID.h"
#include "TCanvas.h"
#include "TList.h"

ClassImp(AliFigureDiHadronPID);

// -----------------------------------------------------------------------
AliFigureDiHadronPID::AliFigureDiHadronPID():
	TNamed(),
	fGraphCollections(0x0),
	fOwnedObjects(0x0),
	fUseAutoRangeAndDomain(kTRUE),
	fDomainMin(0),
	fDomainMax(1),
	fRangeMin(0),
	fRangeMax(1),
	fLegendXmin(0.68),
	fLegendXmax(0.88),
	fLegendYmin(0.68),
	fLegendYmax(0.88),
    fXAxisTitle("dontUse"),
    fYAxisTitle("dontUse"),
    fCustomLabelSizeX(-1),
    fCustomLabelSizeY(-1)

{
	
	// Default Constructor
	AliLogDiHadronPID::Debug2("Default constructor called.", __func__, ClassName());

	fGraphCollections = new TObjArray();
	fGraphCollections->SetOwner(kTRUE);

	fOwnedObjects = new TObjArray();
	fOwnedObjects->SetOwner(kTRUE);

}

// -----------------------------------------------------------------------
AliFigureDiHadronPID::AliFigureDiHadronPID(const char* name, const char* title):
	TNamed(name, title),
	fGraphCollections(0x0),
	fOwnedObjects(0x0),
	fUseAutoRangeAndDomain(kTRUE),
	fDomainMin(0),
	fDomainMax(1),
	fRangeMin(0),
	fRangeMax(1),
	fLegendXmin(0.68),
	fLegendXmax(0.88),
	fLegendYmin(0.68),
	fLegendYmax(0.88),
    fXAxisTitle("dontUse"),
    fYAxisTitle("dontUse"),
    fCustomLabelSizeX(-1),
    fCustomLabelSizeY(-1)

{
	
	// Default Constructor
	AliLogDiHadronPID::Debug2("Default constructor called.", __func__, ClassName());

	fGraphCollections = new TObjArray();
	fGraphCollections->SetOwner(kTRUE);

	fOwnedObjects = new TObjArray();
	fOwnedObjects->SetOwner(kTRUE);

}

// -----------------------------------------------------------------------
AliFigureDiHadronPID::~AliFigureDiHadronPID() {
	
	// Destructor
	AliLogDiHadronPID::Debug2("Destructor called.", __func__, ClassName());

	if (fGraphCollections) {delete fGraphCollections; fGraphCollections = 0x0;}
	if (fOwnedObjects) {delete fOwnedObjects; fOwnedObjects = 0x0;}

}

// -----------------------------------------------------------------------
void AliFigureDiHadronPID::AddGraph(AliGraphDiHadronPID* graph) {

	AliGraphCollectionDiHadronPID* collection = new AliGraphCollectionDiHadronPID(graph->GetName(), graph->GetTitle());
	collection->AddGraph(graph);
	AddGraphCollection(collection);

}

// -----------------------------------------------------------------------
TCanvas* AliFigureDiHadronPID::Show(Bool_t addLegend, Bool_t logScale, Bool_t showSyst, TPad* pad) {

	Int_t nGraphCollections = fGraphCollections->GetEntriesFast();
	if (!nGraphCollections) {
		AliLogDiHadronPID::Error(Form("Figure does not contain any graphs or graph collections."), __func__, ClassName());
		return 0x0;
	}	

	// Make a canvas.
    Bool_t padWasProvided = kTRUE;
	if (!pad) {
        padWasProvided = kFALSE;
        pad = TCanvas::MakeDefCanvas();
    	fOwnedObjects->AddLast(pad);
    }

	// Draw a frame if the pad doesn't contain any primitives.
	if ((pad->GetListOfPrimitives())->GetEntries() == 0) { 
		TH1F* frame = 0x0;
		if (fUseAutoRangeAndDomain) {
			frame = pad->DrawFrame(GetMinX(), GetMinY(logScale), GetMaxX(), GetMaxY(logScale), GetTitle());			
		} else {
			frame = pad->DrawFrame(fDomainMin, fRangeMin, fDomainMax, fRangeMax, GetTitle());			
		}
		if (logScale) {pad->SetLogy();}
		fOwnedObjects->AddLast(frame);

		// Put the correct labels on the axes.
		frame->SetXTitle(fXAxisTitle.EqualTo("dontUse") ? ((AliGraphCollectionDiHadronPID*)fGraphCollections->At(0))->GetTitleX() : fXAxisTitle.Data());
		frame->SetYTitle(fYAxisTitle.EqualTo("dontUse") ? ((AliGraphCollectionDiHadronPID*)fGraphCollections->At(0))->GetTitleY() : fYAxisTitle.Data());

        if (fCustomLabelSizeX >= 0.) {frame->GetXaxis()->SetLabelSize(fCustomLabelSizeX);}
        if (fCustomLabelSizeY >= 0.) {frame->GetYaxis()->SetLabelSize(fCustomLabelSizeY);}

	}

	// Draw the syst. errors of the graph collections.
	if (showSyst) {
		for (Int_t iGraphCollection = 0; iGraphCollection < nGraphCollections; iGraphCollection++) {
			((AliGraphCollectionDiHadronPID*)fGraphCollections->At(iGraphCollection))->ShowSyst(pad);
		}
	}

	// Draw the stat. errors of the graph collections.
	for (Int_t iGraphCollection = 0; iGraphCollection < nGraphCollections; iGraphCollection++) {
		((AliGraphCollectionDiHadronPID*)fGraphCollections->At(iGraphCollection))->ShowStat(pad);
	}

	if (addLegend) {

		// Make a legend.
		TLegend* legend = new TLegend(fLegendXmin, fLegendYmin, fLegendXmax, fLegendYmax);
		legend->SetBorderSize(0);
		legend->SetFillColor(0);
		fOwnedObjects->Add(legend);

		// Update the legend.
		for (Int_t iGraphCollection = 0; iGraphCollection < nGraphCollections; iGraphCollection++) {
			((AliGraphCollectionDiHadronPID*)fGraphCollections->At(iGraphCollection))->UpdateLegend(legend);
		}	

		legend->Draw();
	}

	return (padWasProvided ? 0x0 : (TCanvas*)pad);

}

// -----------------------------------------------------------------------
Double_t AliFigureDiHadronPID::GetMinX() {

	Int_t nGraphCollections = fGraphCollections->GetEntriesFast();
	if (!nGraphCollections) {
		AliLogDiHadronPID::Error(Form("GraphCollection does not contain any graphs."), __func__, ClassName());
		return 0.;
	}	

	Double_t min = ((AliGraphCollectionDiHadronPID*)fGraphCollections->At(0))->GetMinX();
	for (Int_t iGraphCollection = 0; iGraphCollection < nGraphCollections; iGraphCollection++) {

		min = TMath::Min(min, ((AliGraphCollectionDiHadronPID*)fGraphCollections->At(iGraphCollection))->GetMinX());

	}

	return min;

}

// -----------------------------------------------------------------------
Double_t AliFigureDiHadronPID::GetMaxX() {

	Int_t nGraphCollections = fGraphCollections->GetEntriesFast();
	if (!nGraphCollections) {
		AliLogDiHadronPID::Error(Form("GraphCollection does not contain any graphs."), __func__, ClassName());
		return 0.;
	}	

	Double_t max = ((AliGraphCollectionDiHadronPID*)fGraphCollections->At(0))->GetMaxX();
	for (Int_t iGraphCollection = 0; iGraphCollection < nGraphCollections; iGraphCollection++) {

		max = TMath::Max(max, ((AliGraphCollectionDiHadronPID*)fGraphCollections->At(iGraphCollection))->GetMaxX());

	}

	return max;

}

// -----------------------------------------------------------------------
Double_t AliFigureDiHadronPID::GetMinY(Bool_t useLog) {

	Int_t nGraphCollections = fGraphCollections->GetEntriesFast();
	if (!nGraphCollections) {
		AliLogDiHadronPID::Error(Form("GraphCollection does not contain any graphs."), __func__, ClassName());
		return 0.;
	}	

	Double_t min = ((AliGraphCollectionDiHadronPID*)fGraphCollections->At(0))->GetMinY(useLog);
	for (Int_t iGraphCollection = 0; iGraphCollection < nGraphCollections; iGraphCollection++) {

		min = TMath::Min(min, ((AliGraphCollectionDiHadronPID*)fGraphCollections->At(iGraphCollection))->GetMinY(useLog));

	}

	return min;

}

// -----------------------------------------------------------------------
Double_t AliFigureDiHadronPID::GetMaxY(Bool_t useLog) {

	Int_t nGraphCollections = fGraphCollections->GetEntriesFast();
	if (!nGraphCollections) {
		AliLogDiHadronPID::Error(Form("GraphCollection does not contain any graphs."), __func__, ClassName());
		return 0.;
	}	

	Double_t max = ((AliGraphCollectionDiHadronPID*)fGraphCollections->At(0))->GetMaxY(useLog);
	for (Int_t iGraphCollection = 0; iGraphCollection < nGraphCollections; iGraphCollection++) {

		max = TMath::Max(max, ((AliGraphCollectionDiHadronPID*)fGraphCollections->At(iGraphCollection))->GetMaxY(useLog));

	}

	return max;

}

// -----------------------------------------------------------------------
void AliFigureDiHadronPID::Scale(Double_t scaleFactor) {

    /** 
     * Scale all currently owned graphs with scaleFactor.
     */

    AliGraphCollectionDiHadronPID* currentGraphCollection = 0x0;
    TIterator* iter = fGraphCollections->MakeIterator();    
    while ((currentGraphCollection = (AliGraphCollectionDiHadronPID*)iter->Next())) {
        currentGraphCollection->Scale(scaleFactor);
    }

}

// -----------------------------------------------------------------------
void AliFigureDiHadronPID::SetCustomRangeAndDomain(Double_t domMin, Double_t domMax, Double_t ranMin, Double_t ranMax) {

	/**
	 * Sets custom Range and Domain, and disables the automatic range and domain flag.
	 * This flag can be reenabled with SetUseAutoRangeAndDomain(kTRUE);
	 */

	fUseAutoRangeAndDomain = kFALSE;
	fDomainMin = domMin;
	fDomainMax = domMax;
	fRangeMin = ranMin;
	fRangeMax = ranMax;

}

// -----------------------------------------------------------------------
void AliFigureDiHadronPID::SetLegendSize(Double_t Xmin, Double_t Ymin, Double_t Xmax, Double_t Ymax) {

	/**
	 * Sets the legend size (in percentages).
	 */

	fLegendXmin = Xmin;
	fLegendXmax = Xmax;
	fLegendYmin = Ymin;
	fLegendYmax = Ymax;

}

// -----------------------------------------------------------------------
void AliFigureDiHadronPID::Print(Option_t*) const {
	
	// Print info about the class.
	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

}
