/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Fitter class, expecting a AliTOFTPCFitContainer. This class does not
//  own anything.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "AliTOFTPCFitterLeanDiHadronPID.h"
#include "AliLogDiHadronPID.h"
#include "AliTOFTPCPeakDiHadronPID.h"

#include "Minuit2/Minuit2Minimizer.h"
#include "Math/Functor.h"

#include <iostream>
using namespace std;

ClassImp(AliTOFTPCFitterLeanDiHadronPID);

// -----------------------------------------------------------------------
AliTOFTPCFitterLeanDiHadronPID::AliTOFTPCFitterLeanDiHadronPID():
	TObject(),
	fSettings(AliSettingsDiHadronPID::Instance()),
	fFitContainer(0x0),
	fMinimizer(0x0)

{
	
	// Default Constructor
	AliLogDiHadronPID::Debug2("Default constructor called.", __func__, ClassName());

	// Error message when settings singleton is not found!
	if (!fSettings) {AliLogDiHadronPID::Error(Form("Settings Singleton not found!"), __func__, ClassName());}

}

// -----------------------------------------------------------------------
AliTOFTPCFitterLeanDiHadronPID::AliTOFTPCFitterLeanDiHadronPID(AliTOFTPCFitContainerDiHadronPID* container): 
	TObject(),
	fSettings(AliSettingsDiHadronPID::Instance()),	
	fFitContainer(container),
	fMinimizer(0x0)

{
	
	// Default Constructor
	AliLogDiHadronPID::Debug2("Constructor called.", __func__, ClassName());

	// Error message when settings singleton is not found!
	if (!fSettings) {AliLogDiHadronPID::Error(Form("Settings Singleton not found!"), __func__, ClassName());}

}

// -----------------------------------------------------------------------
AliTOFTPCFitterLeanDiHadronPID::~AliTOFTPCFitterLeanDiHadronPID() {
	
	// Destructor
	AliLogDiHadronPID::Debug2("Destructor called.", __func__, ClassName());

}

// -----------------------------------------------------------------------
Int_t AliTOFTPCFitterLeanDiHadronPID::Fit(Bool_t reset, Bool_t showFullDebugInfo, Bool_t checkTolerances) {

	// Fit the current container.
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	// Check if there's a fit container available.
	if (!fFitContainer) {
		AliLogDiHadronPID::Error(Form("No fit container available."), __func__, ClassName());
		return -999;
	}

	// Throw out previous fit results, and start from scratch (i.e., the default parameters).
	if (reset) {fFitContainer->ResetFitStages();}

	// Fit Status variable.
	Int_t fitStatus = 0;

	// Create a minimizer with the proper parameters.
	ROOT::Math::Functor* minimizer_func = 0x0;

	// Ask Settings if we should use chi2 or not.
	if (fFitContainer->GetMeanPt() < fSettings->GetUseChi2BelowPt()) {
		minimizer_func = new ROOT::Math::Functor(fFitContainer, &AliTOFTPCFitContainerDiHadronPID::currentChi2, AliTOFTPCFitFunctionDiHadronPID::GetNParameters());
	} else {
		minimizer_func = new ROOT::Math::Functor(fFitContainer, &AliTOFTPCFitContainerDiHadronPID::currentNegll, AliTOFTPCFitFunctionDiHadronPID::GetNParameters());
	}

	// Set up a a minimizer.
	if (fMinimizer) {delete fMinimizer; fMinimizer = 0x0;}
	fMinimizer = new ROOT::Minuit2::Minuit2Minimizer(ROOT::Minuit2::kMigrad);	
	fMinimizer->SetMaxFunctionCalls(1000000);
	fMinimizer->SetMaxIterations(10000);
	fMinimizer->SetTolerance(0.001);
	if (showFullDebugInfo) {fMinimizer->SetPrintLevel(100);}

	fMinimizer->SetFunction(*minimizer_func);


	// If the fit container doesn't contain any fit results yet, then create a "fit result", with the default parameters.
	if (!(fFitContainer->LastFitStage())) {
		AliLogDiHadronPID::Info(Form("No fit stage exist in current container, creating a new one with default parameters."), __func__, ClassName());		
		fFitContainer->NewFitStage();
             if (checkTolerances) {
		AliLogDiHadronPID::Info("Request to check toletrances, but no prior fits exist in current container, ignoring checkTolerances flag", __func__, ClassName());		
                checkTolerances = kFALSE;
             }
	}

	// Fix mismatches.
	(fFitContainer->LastFitStage())->SetMismatchIsFixed();
	cout << __LINE__ << endl;
	(fFitContainer->LastFitStage())->Print(0); // Printout BEFORE the fit.	

	// Stage 1) Regardless of the settings, this stage will not include electrons, muons or deuterons.
	//          Release any parameter that is marked as free, except the TOF/TPC tails, those will be fixed regardless.
	//
	//  -- Update 2016/06/10: Keeping positions fixed for now (trouble at high pt) 
	//  -- Update 2016/07/01: allow muons, electrons deuterons if requested
        Double_t TOFpositions[10] = {0}; // fixed length array not so nice, but should work
        Double_t TPCpositions[10] = {0}; // fixed length array not so nice, but should work
	fFitContainer->NewFitStage();
	for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {

		AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
		AliTOFTPCPeakDiHadronPID* currentPeak = (fFitContainer->LastFitStage())->GetPeak(currentSpecies);
                // Store positions from before the fit
                TOFpositions[iSpecies] = currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTOF);
                TPCpositions[iSpecies] = currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTPC);

		// Exclude Deuterons, Electrons and Muons.
		/*
		if (currentSpecies == AliSettingsDiHadronPID::kDeuteron || 
			currentSpecies == AliSettingsDiHadronPID::kMuon || 
			currentSpecies == AliSettingsDiHadronPID::kElectron) {

			currentPeak->SetUseInFit(kFALSE);

		} else {
		*/
			// Check for Pions, Kaons and Protons whether they're included or not.
			if (fSettings->IsPeakIncludedInFit(fFitContainer->GetMassAssumption(), currentSpecies, fFitContainer->GetMeanPt())) {

				// If the peak is included, then release yields, mu's, sigma's and tailTOF if requested.
				currentPeak->ReleaseParameter(AliTOFTPCPeakDiHadronPID::kYield);
                                // 2016-06-10
                                if (currentSpecies == 0) { // Only release pion peak pos
				  currentPeak->ReleaseParameter(AliTOFTPCPeakDiHadronPID::kMuTOF);
  				  currentPeak->ReleaseParameter(AliTOFTPCPeakDiHadronPID::kMuTPC);
                                }
                                else { // Fix explicitly in case we are running a refit
				  currentPeak->FixParameter(AliTOFTPCPeakDiHadronPID::kMuTOF);
  				  currentPeak->FixParameter(AliTOFTPCPeakDiHadronPID::kMuTPC);

                                }
                                // temp hack to fix kaon peak
                                /*
                                if (currentSpecies != 1) {
  				currentPeak->ReleaseParameter(AliTOFTPCPeakDiHadronPID::kMuTPC);
                                }
                                else {
  				currentPeak->FixParameter(AliTOFTPCPeakDiHadronPID::kMuTPC);
                                  cout << "N.B.: Keeping kaon peak pos in dE/dx fixed" << endl;
                                }
                                */
				AliSettingsDiHadronPID::Species massAssumption = fFitContainer->GetMassAssumption();
                                if (!fSettings->IsTOFSigmaFixed(massAssumption, currentSpecies))
				  currentPeak->ReleaseParameter(AliTOFTPCPeakDiHadronPID::kSigmaTOF);
                                if (!fSettings->IsTPCSigmaFixed(massAssumption, currentSpecies))
				   currentPeak->ReleaseParameter(AliTOFTPCPeakDiHadronPID::kSigmaTPC);
				currentPeak->FixParameter(AliTOFTPCPeakDiHadronPID::kTailTOF);
				currentPeak->FixParameter(AliTOFTPCPeakDiHadronPID::kTailTPC);
/*
				if (fSettings->IsTOFTailFixed(fFitContainer->GetMassAssumption(), currentSpecies, fFitContainer->GetMeanPt())) {
					currentPeak->FixParameter(AliTOFTPCPeakDiHadronPID::kTailTOF);
				} else {
					currentPeak->ReleaseParameter(AliTOFTPCPeakDiHadronPID::kTailTOF);
				}


				// CHECK THIS: WE WANT TO INCLUDE A FIXED TAIL FOR TPC IF REQUESTED
				if (fSettings->IsTPCTailIncluded(fFitContainer->GetMassAssumption(), currentSpecies, fFitContainer->GetMeanPt()) &&
					!(fSettings->IsTPCTailFixed(fFitContainer->GetMassAssumption(), currentSpecies, fFitContainer->GetMeanPt()))) {
					currentPeak->ReleaseParameter(AliTOFTPCPeakDiHadronPID::kTailTPC);
				} else {
					currentPeak->FixParameter(AliTOFTPCPeakDiHadronPID::kTailTPC);
				}
*/
			} else {

				currentPeak->SetUseInFit(kFALSE);

			}
			//}
	}

	(fFitContainer->LastFitStage())->UpdateFitRegion();
	SetMinimizerSettings();
	fitStatus = Minimize();
	(fFitContainer->LastFitStage())->Print(0);

	// Stage 2) Regardless of the settings, this stage will not include electrons, muons or deuterons.
	//          If any TOF tail is requested to be free, then it's released here. If no TOF tails are
	//          requested to be free, then this stage is skipped.
	//
	//  -- Update 2016/06/10: changed previous stage, so cannot skip this one... 
	//  -- Update 2016/07/01: allow muons, electrons deuterons if requested
	fFitContainer->NewFitStage();
	for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {

		AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
		AliTOFTPCPeakDiHadronPID* currentPeak = (fFitContainer->LastFitStage())->GetPeak(currentSpecies);

		// Exclude Deuterons, Electrons and Muons.
		/*
		if (currentSpecies == AliSettingsDiHadronPID::kDeuteron || 
			currentSpecies == AliSettingsDiHadronPID::kMuon || 
			currentSpecies == AliSettingsDiHadronPID::kElectron) {

			currentPeak->SetUseInFit(kFALSE);

		} else {
		*/
			// Check for Pions, Kaons and Protons whether they're included or not.
			if (fSettings->IsPeakIncludedInFit(fFitContainer->GetMassAssumption(), currentSpecies, fFitContainer->GetMeanPt())) {

				// If the peak is included, then release yields, mu's, sigma's and tailTOF if requested.
				AliSettingsDiHadronPID::Species massAssumption = fFitContainer->GetMassAssumption();
				currentPeak->ReleaseParameter(AliTOFTPCPeakDiHadronPID::kYield);
                                if (!fSettings->IsTOFPositionFixed(massAssumption, currentSpecies))
         				currentPeak->ReleaseParameter(AliTOFTPCPeakDiHadronPID::kMuTOF);
                                if (!fSettings->IsTPCPositionFixed(massAssumption, currentSpecies))
         				currentPeak->ReleaseParameter(AliTOFTPCPeakDiHadronPID::kMuTPC);
                                // temp hack to fix kaon peak
                                /*
                                if (currentSpecies != 1) {
  				currentPeak->ReleaseParameter(AliTOFTPCPeakDiHadronPID::kMuTPC);
                                }
                                else {
  				currentPeak->FixParameter(AliTOFTPCPeakDiHadronPID::kMuTPC);
                                  cout << "N.B.: Keeping kaon peak pos in dE/dx fixed" << endl;
                                }
                                */

                                if (!fSettings->IsTOFSigmaFixed(massAssumption, currentSpecies))
         				currentPeak->ReleaseParameter(AliTOFTPCPeakDiHadronPID::kSigmaTOF);
                                if (!fSettings->IsTPCSigmaFixed(massAssumption, currentSpecies))
        				currentPeak->ReleaseParameter(AliTOFTPCPeakDiHadronPID::kSigmaTPC);

				if (fSettings->IsTOFTailFixed(fFitContainer->GetMassAssumption(), currentSpecies, fFitContainer->GetMeanPt())) {
					currentPeak->FixParameter(AliTOFTPCPeakDiHadronPID::kTailTOF);
				} else {
					currentPeak->ReleaseParameter(AliTOFTPCPeakDiHadronPID::kTailTOF);
				}

				if (fSettings->IsTPCTailIncluded(fFitContainer->GetMassAssumption(), currentSpecies, fFitContainer->GetMeanPt()) &&
					!(fSettings->IsTPCTailFixed(fFitContainer->GetMassAssumption(), currentSpecies, fFitContainer->GetMeanPt()))) {
					currentPeak->ReleaseParameter(AliTOFTPCPeakDiHadronPID::kTailTPC);
				} else {
					currentPeak->FixParameter(AliTOFTPCPeakDiHadronPID::kTailTPC);
				}

			} else {

				currentPeak->SetUseInFit(kFALSE);

			}
			//}
	}

	(fFitContainer->LastFitStage())->UpdateFitRegion();
	SetMinimizerSettings();
	fitStatus = Minimize();
	(fFitContainer->LastFitStage())->Print(0);
	
        // Check position movements if requested
        if (checkTolerances) {
           Bool_t refitFlag = kFALSE;
           for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {
		AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
		AliTOFTPCPeakDiHadronPID* currentPeak = (fFitContainer->LastFitStage())->GetPeak(currentSpecies);
                if (fabs(currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTOF) - TOFpositions[iSpecies]) > fSettings->GetMuTOFTolerance(fFitContainer->GetMeanPt()) ||
                    fabs(currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTPC) - TPCpositions[iSpecies]) > fSettings->GetMuTPCTolerance())
                  refitFlag = kTRUE;
           }
 
	   if (refitFlag) {
             fFitContainer->NewFitStage();
	     AliLogDiHadronPID::Info("Refitting because peaks positions deviate too much", __func__, ClassName());		
             for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {
		AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
		AliTOFTPCPeakDiHadronPID* currentPeak = (fFitContainer->LastFitStage())->GetPeak(currentSpecies);
                currentPeak->SetParValue(AliTOFTPCPeakDiHadronPID::kMuTOF, TOFpositions[iSpecies]);
		currentPeak->FixParameter(AliTOFTPCPeakDiHadronPID::kMuTOF);
                currentPeak->SetParValue(AliTOFTPCPeakDiHadronPID::kMuTPC, TPCpositions[iSpecies]);
		currentPeak->FixParameter(AliTOFTPCPeakDiHadronPID::kMuTPC);
             }
	     (fFitContainer->LastFitStage())->UpdateFitRegion();
             SetMinimizerSettings();
	     fitStatus = Minimize();
	     (fFitContainer->LastFitStage())->Print(0);
           }
        }
	AliLogDiHadronPID::Info(Form("Fit completed with minimizer status: %s (%i).", GetMinuit2StatusMessage(fitStatus), fitStatus), __func__, ClassName());

	if (minimizer_func) {delete minimizer_func; minimizer_func = 0x0;}
	
	return fitStatus;

}

// -----------------------------------------------------------------------
Int_t AliTOFTPCFitterLeanDiHadronPID::FitOnlyYields() {

	// This method is similar to Fit(), except that it will fix all parameters
	// except for the yields. This is handy when for example the fit needs to
	// be redone, using smoothened parameters.

	// TODO: This method has a lot overlap with the Fit() method. Should make
	// sort of an init method for the fitter.
	
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	// Check if there's a fit container available.
	if (!fFitContainer) {
		AliLogDiHadronPID::Error(Form("No fit container available."), __func__, ClassName());
		return -999;
	}

	// If the fit container doesn't contain any fit results yet, then create a "fit result", with the default parameters.
	if (!(fFitContainer->LastFitStage())) {
		AliLogDiHadronPID::Error(Form("No fit stage exist in current container."), __func__, ClassName());		
		return -999;
	}


	// Fit Status variable.
	Int_t fitStatus = 0;

	// Create a minimizer with the proper parameters.
	ROOT::Math::Functor* minimizer_func = 0x0;

	// Ask Settings if we should use chi2 or not.
	if (fFitContainer->GetMeanPt() < fSettings->GetUseChi2BelowPt()) {
		minimizer_func = new ROOT::Math::Functor(fFitContainer, &AliTOFTPCFitContainerDiHadronPID::currentChi2, AliTOFTPCFitFunctionDiHadronPID::GetNParameters());
	} else {
		minimizer_func = new ROOT::Math::Functor(fFitContainer, &AliTOFTPCFitContainerDiHadronPID::currentNegll, AliTOFTPCFitFunctionDiHadronPID::GetNParameters());
	}

	// Set up a a minimizer.
	if (fMinimizer) {delete fMinimizer; fMinimizer = 0x0;}
	fMinimizer = new ROOT::Minuit2::Minuit2Minimizer(ROOT::Minuit2::kMigrad);	
	fMinimizer->SetMaxFunctionCalls(1000000);
	fMinimizer->SetMaxIterations(10000);
	fMinimizer->SetTolerance(0.001);
	//fMinimizer->SetPrintLevel(100);

	fMinimizer->SetFunction(*minimizer_func);

	// Fix mismatches.
	(fFitContainer->LastFitStage())->SetMismatchIsFixed();

	for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {

		AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
		AliTOFTPCPeakDiHadronPID* currentPeak = (fFitContainer->LastFitStage())->GetPeak(currentSpecies);

		// Check for Pions, Kaons and Protons whether they're included or not.
		if (fSettings->IsPeakIncludedInFit(fFitContainer->GetMassAssumption(), currentSpecies, fFitContainer->GetMeanPt())) {

			// If the peak is included, then release yields, mu's, sigma's and tailTOF if requested.
			currentPeak->ReleaseParameter(AliTOFTPCPeakDiHadronPID::kYield);
			currentPeak->FixParameter(AliTOFTPCPeakDiHadronPID::kMuTOF);
			currentPeak->FixParameter(AliTOFTPCPeakDiHadronPID::kMuTPC);
			currentPeak->FixParameter(AliTOFTPCPeakDiHadronPID::kSigmaTOF);
			currentPeak->FixParameter(AliTOFTPCPeakDiHadronPID::kSigmaTPC);
			currentPeak->FixParameter(AliTOFTPCPeakDiHadronPID::kTailTOF);
			currentPeak->FixParameter(AliTOFTPCPeakDiHadronPID::kTailTPC);

		}
	}

	(fFitContainer->LastFitStage())->UpdateFitRegion();
	SetMinimizerSettings();
	fitStatus = Minimize();
	(fFitContainer->LastFitStage())->Print(0);

	AliLogDiHadronPID::Info(Form("Fit completed with minimizer status: %s (%i).", GetMinuit2StatusMessage(fitStatus), fitStatus), __func__, ClassName());

	if (minimizer_func) {delete minimizer_func; minimizer_func = 0x0;}
	
	return fitStatus;

}

// -----------------------------------------------------------------------
void AliTOFTPCFitterLeanDiHadronPID::Print(Option_t*) const {
	
	// Print info about the class.
	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	cout << "--------------------------" << endl;
	cout << " Lean TOF/TPC Fitter" << endl;
	cout << "--------------------------" << endl;
	cout << " Container availanble: " << (fFitContainer ? "Y" : "N") << endl;
	cout << "--------------------------" << endl;

}

// -----------------------------------------------------------------------
const char* AliTOFTPCFitterLeanDiHadronPID::GetMinuit2StatusMessage(Int_t status) const {

	if (status < 0) {return "Fit not converged.";}

	switch (status) {
		case 0:
			return "Converged, no problems.";
		case 1:
			return "Converged, but covariance was made positive definite.";
		case 2:
			return "Converged, but Hesse was invalid.";
		case 3:
			return "Converged, but EDM above max.";
		case 4:
			return "Reached call limit.";
		case 5:
			return "Other failure.";
		default:
			return "Unknown error code.";

	}

	return 0;

}

// -----------------------------------------------------------------------
Bool_t AliTOFTPCFitterLeanDiHadronPID::SetMinimizerSettings() {

	// Set the parameters of the minimizer to the ones in the last fit stage.
	// Note that parameters of peaks that are not used will automatically be fixed.
	// The method AliTOFTPCFitFunctionDiHadronPID::operator() will ignore peaks
	// that are not included.
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	AliTOFTPCFitFunctionDiHadronPID* currentFitStage = fFitContainer->LastFitStage();

	// Set the mismatches separately.
	if (currentFitStage->IsParFixedAtIndex(0)) {fMinimizer->SetFixedVariable(0, currentFitStage->GetParNameAtIndex(0).Data(), currentFitStage->GetParValueAtIndex(0));}
	else {fMinimizer->SetVariable(0, currentFitStage->GetParNameAtIndex(0).Data(), currentFitStage->GetParValueAtIndex(0), currentFitStage->GetParStepAtIndex(0));}

	// Loop over all peaks.
	for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {
		
		// Check if the peak is included.
		AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
		AliTOFTPCPeakDiHadronPID* currentPeak = currentFitStage->GetPeak(currentSpecies);

		if (currentPeak->IsUsedInFit()) {

			// If the peak is used in the fit, then set the parameters according to whether they're fixed or not.
			for (Int_t iPar = 0; iPar < AliTOFTPCPeakDiHadronPID::GetNParameters(); iPar++) {

				// Get the index (see fit function) belonging to the species and the parameter.
				AliTOFTPCPeakDiHadronPID::Parameter currentParameter = (AliTOFTPCPeakDiHadronPID::Parameter)iPar;
				Int_t currentParIndex = AliTOFTPCFitFunctionDiHadronPID::GetIndexFromSpeciesAndParameter(currentSpecies, currentParameter);
			
				if (currentPeak->GetParIsFixed(currentParameter)) {
					fMinimizer->SetFixedVariable(currentParIndex, currentPeak->GetParNameWithSpecies(currentParameter).Data(), currentPeak->GetParValue(currentParameter));
				} else {
					switch (currentParameter) {
						case AliTOFTPCPeakDiHadronPID::kYield:
							fMinimizer->SetLowerLimitedVariable(currentParIndex, currentPeak->GetParNameWithSpecies(currentParameter).Data(), currentPeak->GetParValue(currentParameter), currentPeak->GetParStepSize(currentParameter), 1.0);
							break;
						case AliTOFTPCPeakDiHadronPID::kSigmaTOF:
							fMinimizer->SetLowerLimitedVariable(currentParIndex, currentPeak->GetParNameWithSpecies(currentParameter).Data(), currentPeak->GetParValue(currentParameter), currentPeak->GetParStepSize(currentParameter), 1.0);
							break;
						case AliTOFTPCPeakDiHadronPID::kSigmaTPC:
                                                        if (fSettings->GetUseDEDxRatios())
              							fMinimizer->SetLowerLimitedVariable(currentParIndex, currentPeak->GetParNameWithSpecies(currentParameter).Data(), currentPeak->GetParValue(currentParameter), currentPeak->GetParStepSize(currentParameter), 0.001);
                                                        else
              							fMinimizer->SetLowerLimitedVariable(currentParIndex, currentPeak->GetParNameWithSpecies(currentParameter).Data(), currentPeak->GetParValue(currentParameter), currentPeak->GetParStepSize(currentParameter), 0.1);
							break;
						case AliTOFTPCPeakDiHadronPID::kTailTOF:
							fMinimizer->SetLowerLimitedVariable(currentParIndex, currentPeak->GetParNameWithSpecies(currentParameter).Data(), currentPeak->GetParValue(currentParameter), currentPeak->GetParStepSize(currentParameter), 10e-6);
							break;
						case AliTOFTPCPeakDiHadronPID::kTailTPC:
							fMinimizer->SetLowerLimitedVariable(currentParIndex, currentPeak->GetParNameWithSpecies(currentParameter).Data(), currentPeak->GetParValue(currentParameter), currentPeak->GetParStepSize(currentParameter), 10e-6);
							break;
						default:
							fMinimizer->SetVariable(currentParIndex, currentPeak->GetParNameWithSpecies(currentParameter).Data(), currentPeak->GetParValue(currentParameter), currentPeak->GetParStepSize(currentParameter));
							break;
					}
				}
			}

		} else {

			// If the peak is not used in the fit, then all parameters are automatically fixed.
			for (Int_t iPar = 0; iPar < AliTOFTPCPeakDiHadronPID::GetNParameters(); iPar++) {
			
				// Get the index (see fit function) belonging to the species and the parameter.
				AliTOFTPCPeakDiHadronPID::Parameter currentParameter = (AliTOFTPCPeakDiHadronPID::Parameter)iPar;
				Int_t currentParIndex = AliTOFTPCFitFunctionDiHadronPID::GetIndexFromSpeciesAndParameter(currentSpecies, currentParameter);
			
				fMinimizer->SetFixedVariable(currentParIndex, currentPeak->GetParNameWithSpecies(currentParameter).Data(), currentPeak->GetParValue(currentParameter));
		
			}
		}		
	}

	return kTRUE;

}

// -----------------------------------------------------------------------
Int_t AliTOFTPCFitterLeanDiHadronPID::Minimize() {

	// Minimizes the fMinimizer as is, and calculates the Hessian.
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	// Performs the fit.
	fMinimizer->Minimize();

	// Calculates full Hessian (for error calculation), and print status of fitter.
	fMinimizer->Hesse();
	//PrintHessian();

	Int_t fitStatus = (Int_t)(fMinimizer->Status());

	// Set the fitStatus and errors to the current fit stage.
	AliTOFTPCFitFunctionDiHadronPID* currentFitStage = fFitContainer->CurrentFitStage();
	currentFitStage->SetFitStatus(fitStatus);
	currentFitStage->SetParErrors(fMinimizer->Errors());
	
	return fitStatus;

}

// -----------------------------------------------------------------------
Bool_t AliTOFTPCFitterLeanDiHadronPID::PrintHessian() {

	// Prints the hessian matrix.
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	const int NDim = fMinimizer->NDim();
	double* hessianMatrix = new double[NDim * NDim];
	if (fMinimizer->Status() < 100) {fMinimizer->GetHessianMatrix(hessianMatrix);}
	if (hessianMatrix) {
		cout << endl;
		cout << "Hessian Matrix:" << endl;
		for (Int_t iDim = 0; iDim < (Int_t)fMinimizer->NDim(); iDim++) {

			cout << Form("%20s", fSettings->GetSpeciesName(AliTOFTPCFitFunctionDiHadronPID::GetSpeciesFromIndex(iDim)));

			for (Int_t jDim = 0; jDim < (Int_t)fMinimizer->NDim(); jDim++) {
				if (hessianMatrix[iDim + jDim * fMinimizer->NDim()]) {
					if (TMath::Abs(hessianMatrix[iDim + jDim * fMinimizer->NDim()]) < 1e-80) {
						cout << "   ~0   ";
					} else {
						cout << Form("%+7.0e ",hessianMatrix[iDim + jDim * fMinimizer->NDim()]);
					}
				} else {
					cout << "    0   ";
				}
			}
			cout << endl;
		}
		cout << endl;
	} else {
		return kFALSE;
	}

	delete [] hessianMatrix;

	return kTRUE;

}
