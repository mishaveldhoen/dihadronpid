/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Class description
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "TMath.h"
#include "TLegend.h"
#include "TRandom3.h"
#include "TLatex.h"
#include "THStack.h"

#include "AliLogDiHadronPID.h"
#include "AliHistToolsDiHadronPID.h"

#include "AliAssociatedYieldDiHadronPID.h"

ClassImp(AliAssociatedYieldDiHadronPID);

// -----------------------------------------------------------------------
AliAssociatedYieldDiHadronPID::AliAssociatedYieldDiHadronPID(): 
	TH2F(),
	fSettings(AliSettingsDiHadronPID::Instance()),
    fNTriggers(-1),
    fYieldIsPerTrigger(kFALSE),
	fDPhiProjection(0x0),
	fDPhiProjectionJet(0x0),
	fDPhiProjectionNoJet(0x0),
	fDPhiProjectionUsedJetRadius(-1.),
	fDPhiProjectionUsedMaxAbsDEta(-1.),
	fDPhiProjectionUsedConstFit(kTRUE),
	fDPhiProjectionGaussianFit(0x0),
	fDPhiProjectionGaussianFitStatus(-999),
	fDPhiProjectionNoJetDCT(0x0),
	fDPhiProjectionTotalFlowFunc(0x0),
	fDphiProjectionTotalFlowUncertaintyBand(0x0),
	fDPhiProjectionMinVn(0),
	fDPhiProjectionMaxVn(0),
	fDEtaProjection(0x0),
	fDEtaProjectionUsedMaxAbsDPhi(-1.),
	fDEtaProjectionUsedMaxAbsDEta(-1.),
	fDEtaProjectionGaussianFit(0x0),
	fDEtaProjectionGaussianFitStatus(-999),
	fDEtaProjectionDoubleGaussianFit(0x0),
	fDEtaProjectionDoubleGaussianFitStatus(-999)

{
	
	/** 
	 * Default constructor.
	 */
	AliLogDiHadronPID::Debug2("Constructor called.", __func__, ClassName());
    UseCurrentStyle();
    SetXTitle("#Delta#phi (rad)");
    SetYTitle("#Delta#eta"); 
    SetZTitle("#frac{d^{2}N}{d#Delta#phi d#Delta#eta}");

}

// -----------------------------------------------------------------------
AliAssociatedYieldDiHadronPID::AliAssociatedYieldDiHadronPID(const TH2F& other, Double_t nTriggers):
	TH2F(other),
	fSettings(AliSettingsDiHadronPID::Instance()),	
    fNTriggers(nTriggers),
    fYieldIsPerTrigger(kFALSE),
	fDPhiProjection(0x0),
	fDPhiProjectionJet(0x0),	
	fDPhiProjectionNoJet(0x0),
	fDPhiProjectionUsedJetRadius(-1.),
	fDPhiProjectionUsedMaxAbsDEta(-1.),
	fDPhiProjectionUsedConstFit(kTRUE),
	fDPhiProjectionGaussianFit(0x0),
	fDPhiProjectionGaussianFitStatus(-999),
	fDPhiProjectionNoJetDCT(0x0),
	fDPhiProjectionTotalFlowFunc(0x0),
	fDphiProjectionTotalFlowUncertaintyBand(0x0),	
	fDPhiProjectionMinVn(0),
	fDPhiProjectionMaxVn(0),
	fDEtaProjection(0x0),
	fDEtaProjectionUsedMaxAbsDPhi(-1.),
	fDEtaProjectionUsedMaxAbsDEta(-1.),
	fDEtaProjectionGaussianFit(0x0),
	fDEtaProjectionGaussianFitStatus(-999),
	fDEtaProjectionDoubleGaussianFit(0x0),
	fDEtaProjectionDoubleGaussianFitStatus(-999)	

{
	
	/** 
	 * Copy constructor from a TH2F instance.
	 */
	AliLogDiHadronPID::Debug2("Constructor called.", __func__, ClassName());
    UseCurrentStyle();
    SetXTitle("#Delta#phi (rad)");
    SetYTitle("#Delta#eta"); 
    SetZTitle("#frac{d^{2}N}{d#Delta#phi d#Delta#eta}");

    // Handle the nTrigger parameter.
    if (fNTriggers < -1.) {
        AliLogDiHadronPID::Warning("You tried to initialize this class with a negative number of triggers.", __func__, ClassName());
    }

    if (fNTriggers > 0.) {
        AliLogDiHadronPID::Info(Form("Initialized with %f triggers", fNTriggers), __func__, ClassName());
        AliLogDiHadronPID::Info("You initialized this object with a certain number of triggers, to get the results per trigger first call: DivideThroughNTriggers()", __func__, ClassName());

        if (fNTriggers < 10.) {
            AliLogDiHadronPID::Warning(Form("You tried to initialize this class with a very small number of triggers: %f", fNTriggers), __func__, ClassName());
        }

    }

}

// -----------------------------------------------------------------------
AliAssociatedYieldDiHadronPID::AliAssociatedYieldDiHadronPID(const AliAssociatedYieldDiHadronPID& other): 
	TH2F(other),
	fSettings(AliSettingsDiHadronPID::Instance()),	
    fNTriggers(other.fNTriggers),
    fYieldIsPerTrigger(other.fYieldIsPerTrigger),
	fDPhiProjection(0x0),
	fDPhiProjectionJet(0x0),	
	fDPhiProjectionNoJet(0x0),
	fDPhiProjectionUsedJetRadius(-1.),
	fDPhiProjectionUsedMaxAbsDEta(-1.),
	fDPhiProjectionUsedConstFit(kTRUE),
	fDPhiProjectionGaussianFit(0x0),
	fDPhiProjectionGaussianFitStatus(-999),
	fDPhiProjectionNoJetDCT(0x0),
	fDPhiProjectionTotalFlowFunc(0x0),
	fDphiProjectionTotalFlowUncertaintyBand(0x0),	
	fDPhiProjectionMinVn(0),
	fDPhiProjectionMaxVn(0),
	fDEtaProjection(0x0),
	fDEtaProjectionUsedMaxAbsDPhi(-1.),
	fDEtaProjectionUsedMaxAbsDEta(-1.),
	fDEtaProjectionGaussianFit(0x0),
	fDEtaProjectionGaussianFitStatus(-999),
	fDEtaProjectionDoubleGaussianFit(0x0),
	fDEtaProjectionDoubleGaussianFitStatus(-999)	

{
	
	/** 
	 * Copy constructor from a AliAssociatedYieldDiHadronPID instace. It is in a sense not 
	 * a true copy constructor, as it does not take over anything else from the base except
	 * for the data, and the number of triggers.
	 */
	AliLogDiHadronPID::Debug2("Constructor called.", __func__, ClassName());
    UseCurrentStyle();

}

// -----------------------------------------------------------------------
AliAssociatedYieldDiHadronPID::~AliAssociatedYieldDiHadronPID() {
	
	/** Destructor */
	AliLogDiHadronPID::Debug2("Destructor called.", __func__, ClassName());

	CleanUp();

}

// -----------------------------------------------------------------------
void AliAssociatedYieldDiHadronPID::Randomize() {

	/** 
	 * Randomize all points in the histogram, assuming that every bin has a 
 	 * Gaussian distribution, characterized by that bin's \f$\sigma\f$. This
 	 * method can in particular be used to disentangle statistical uncertainties
 	 * from systematic ones by constructing a copy, randomizing it, and then
 	 * redoing the analysis.
 	 *
 	 * \note This method will throw away all results generated by the 
 	 * Perform***Analysis methods.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	CleanUp();
	AliHistToolsDiHadronPID::RandomizeHist(this);

}

// -----------------------------------------------------------------------
TH2F* AliAssociatedYieldDiHadronPID::GetSigmaOverSqrtNDPhiDEta() const {

	/**
	 * Return a histogram which returns \f$\sqrt{N}/\sigma\f$ as a function
	 * of \f$(\Delta\phi,\Delta\eta)\f$.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	// Create a deep copy of the data of this instance.
	TH2F* hOut = new TH2F(*this);

	// Fill every bin with sqrtN/sigma.
	for (Int_t iBinX = 1; iBinX <= GetNbinsX(); iBinX++) {
		for (Int_t iBinY = 1; iBinY <= GetNbinsY(); iBinY++) {
			if (GetBinContent(iBinX, iBinY) > 10e-10) {
				hOut->SetBinContent(iBinX, iBinY, 
					hOut->GetBinError(iBinX, iBinY) / TMath::Sqrt(hOut->GetBinContent(iBinX, iBinY)));	
			}	
		}	
	}

	return hOut;

}

// -----------------------------------------------------------------------
void AliAssociatedYieldDiHadronPID::FillSigmaOverSqrtN(TH1F* hist) const {
	
	/**
	 * Updates a 1D histogram \f$N(\sqrt{N}/\sigma)\f$ with the values of 
	 * every bin of this instance.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	// Protect agains null pointers.
	if (!hist) {return;}

	// Loop over all bins of this instance, and update the supplied histogram.
	for (Int_t iBinX = 1; iBinX <= GetNbinsX(); iBinX++) {
		for (Int_t iBinY = 1; iBinY <= GetNbinsY(); iBinY++) {
			if (GetBinContent(iBinX, iBinY) > 10e-10) {
				hist->Fill(GetBinError(iBinX, iBinY)/ TMath::Sqrt(GetBinContent(iBinX, iBinY)));
			}
		}	
	}	

}

// -----------------------------------------------------------------------
void AliAssociatedYieldDiHadronPID::FillRandom(Double_t nTriggers, Double_t bgdDensityPerTrigger, Double_t peakYield, Double_t peakWidth, 
	Int_t nFlowCoefficients, const Float_t* flowCoefficients, TH2F* signalHist, TH2F* bgdHist) {

	/**
	 * Using this method, one can generate a Monte Carlo associated distribution,
	 * with a desired near-side peak shape, signal-over-background ratio, and
	 * flow coefficients. This procedure includes an artificial mixed event correction (perfect triangle),
	 * so that the statistics as a function of \f$\Delta\eta\f$ closely approximates a realistic situation.
	 *
	 * \param bgdDensityPerTrigger The desired number of background tracks per unit \f$\Delta\phi\times\Delta\eta\f$.
	 * \param peakYield The number of particles in the peak, absolute value.
	 * \param peakWidth The \f$\sigma\f$ of the jet (same in \f$\Delta\phi\f$ and \f$\Delta\eta\f$).
	 * \param nFlowCoefficients The size of the array flowCoefficients.
	 * \param flowCoefficients An array of flow coefficients \f$V_{n\Delta}\f$. If left to the default 0x0, then
	 * it is assumed that all flow coefficients are 0.
	 * \param signalHist Ptr to a histogram that will be filled with the signal hits only (can safely be left 0x0).
	 * \param bgdHist Ptr to a histogram that will be filled with the background hits only (can safely be left 0x0).
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

    fNTriggers = nTriggers;
    fYieldIsPerTrigger = kTRUE;

	// Somehow the GetEntries() method internally (TH1.cxx) casts a double to an int, and then returns a double again... very strange...
	if (GetEntries() > 10e-6) {
		AliLogDiHadronPID::Warning(Form("This histogram is not empty, are you sure this is what you want?"), __func__, ClassName());
	}

	// Sanity check.
	if ((!flowCoefficients) && nFlowCoefficients > 0) {
		AliLogDiHadronPID::Error(Form("No flow coefficients array was provided, even though nFlowCoefficients > 0..."), __func__, ClassName());
		return;
	}

	Double_t maxAbsDEta = GetYaxis()->GetXmax();

	// Create a 2D triangle (perfect two-particle acceptance).
	TF2* funcTriangle2D = new TF2("funcTriangle2D", "(1-[0]*TMath::Abs(y))", -TMath::Pi()/2., 3.*TMath::Pi()/2., -maxAbsDEta, maxAbsDEta);
	funcTriangle2D->SetParameter(0, 1./maxAbsDEta);
	funcTriangle2D->SetNpy(1000);

	// Create a description of the flow background.
	TString backgroundFormula("(1-[0]*TMath::Abs(y))*");
	backgroundFormula.Append(AliMiscToolsDiHadronPID::GetFlowFormula(nFlowCoefficients, 1));

	// Reset bgdHist and signalHist if provided.
	if (bgdHist) {bgdHist->Reset();}
	if (signalHist) {signalHist->Reset();}

	// PDF for the background distribution.
	Double_t binSize = GetXaxis()->GetBinWidth(1) * GetYaxis()->GetBinWidth(1);
	TF2* pdfBackground = new TF2("pdfBackground", "[0]*(1-[1]*TMath::Abs(y))*(1+2*[2]*cos(x)+2*[3]*cos(2*x)+2*[4]*cos(3*x)+2*[5]*cos(4*x)+2*[6]*cos(5*x))", 
		-TMath::Pi(), TMath::Pi(), -maxAbsDEta + 10e-20, maxAbsDEta - 10e-20); // This range makes sure we never divide through 0.
	pdfBackground->SetParameter(0, bgdDensityPerTrigger * binSize * fNTriggers);
	pdfBackground->SetParameter(1, 1./maxAbsDEta);
	if (nFlowCoefficients > 0) {
		for (Int_t iFlowCoefficient = 1; iFlowCoefficient <= nFlowCoefficients; iFlowCoefficient++) {
			pdfBackground->SetParameter(iFlowCoefficient+1, flowCoefficients[iFlowCoefficient - 1]);
		}
	}
	pdfBackground->SetNpy(1000);

	// Create a description of the signal (NS peak, Gaussian).
	TF2* pdfSignal = new TF2("pdfSignal", "(1-[0]*TMath::Abs(y))*([1]/(2.*TMath::Pi()*[2]*[2]))*TMath::Exp(-(x*x+y*y)/(2*[2]*[2]))", 
		-TMath::Pi(), TMath::Pi(), -maxAbsDEta + 10e-20, maxAbsDEta - 10e-20); // This range makes sure we never divide through 0.
	pdfSignal->SetParameter(0, 1./maxAbsDEta);
	pdfSignal->SetParameter(1, peakYield * binSize * fNTriggers);   // Note that this number can just be exactly the same as the number of particles that we aim to get in the jet peak.
	pdfSignal->SetParameter(2, peakWidth); // Sigma
	pdfSignal->SetNpy(1000);

	// Fill the background histogram by drawing random numbers from a poisson distribution.
	TRandom3 rndGen(0);
	for (Int_t iBinDPhi = 1; iBinDPhi <= GetNbinsX(); iBinDPhi++) {

		Double_t currentBinCenterDPhi = GetXaxis()->GetBinCenter(iBinDPhi);

		for (Int_t iBinDEta = 1; iBinDEta <= GetNbinsY(); iBinDEta++) {

			Double_t currentBinCenterDEta = GetYaxis()->GetBinCenter(iBinDEta);

			bgdHist->SetBinContent(iBinDPhi, iBinDEta, rndGen.Poisson(pdfBackground->Eval(currentBinCenterDPhi, currentBinCenterDEta)));

			signalHist->SetBinContent(iBinDPhi, iBinDEta, rndGen.Poisson(pdfSignal->Eval(currentBinCenterDPhi, currentBinCenterDEta)));
			
			SetBinContent(iBinDPhi, iBinDEta, bgdHist->GetBinContent(iBinDPhi, iBinDEta) + signalHist->GetBinContent(iBinDPhi, iBinDEta));
		}
	}

   

	delete pdfBackground; pdfBackground = 0x0;
	delete pdfSignal; pdfSignal = 0x0;

	// Put the uncertainty on every bin to sqrt(N).
	Sumw2();

	// Make a density.
	Scale(1./fNTriggers, "width");

	// Perform mixed event correction.
	Divide(funcTriangle2D);

	// Also perform corrections on bgdHist and signalHist if they exist.
	if (bgdHist) {
		bgdHist->Sumw2();
		bgdHist->Scale(1./fNTriggers, "width");
		bgdHist->Divide(funcTriangle2D);
	}

	if (signalHist) {
		signalHist->Sumw2();
		signalHist->Scale(1./fNTriggers, "width");
		signalHist->Divide(funcTriangle2D);
	}

	delete funcTriangle2D;

}

// -----------------------------------------------------------------------
void AliAssociatedYieldDiHadronPID::CleanUp() {

	/**
	 * Reset the class to its default values, except for the di-hadron data.
	 * This should for example be done when randomizing the data, since such
	 * an opperation renders all analysis results outdated.
	 */

	if (fDPhiProjection) {delete fDPhiProjection; fDPhiProjection = 0x0;}
	if (fDPhiProjectionJet) {delete fDPhiProjectionJet; fDPhiProjectionJet = 0x0;}
	if (fDPhiProjectionNoJet) {delete fDPhiProjectionNoJet; fDPhiProjectionNoJet = 0x0;}
	if (fDPhiProjectionGaussianFit) {delete fDPhiProjectionGaussianFit; fDPhiProjectionGaussianFit = 0x0;}
	if (fDPhiProjectionNoJetDCT) {delete fDPhiProjectionNoJetDCT; fDPhiProjectionNoJetDCT = 0x0;}
	if (fDPhiProjectionTotalFlowFunc) {delete fDPhiProjectionTotalFlowFunc; fDPhiProjectionTotalFlowFunc = 0x0;}
	if (fDphiProjectionTotalFlowUncertaintyBand) {delete fDphiProjectionTotalFlowUncertaintyBand; fDphiProjectionTotalFlowUncertaintyBand = 0x0;}

	if (fDEtaProjection) {delete fDEtaProjection; fDEtaProjection = 0x0;}
	if (fDEtaProjectionGaussianFit) {delete fDEtaProjectionGaussianFit; fDEtaProjectionGaussianFit = 0x0;}

	fDPhiProjectionUsedJetRadius = -1.;
	fDPhiProjectionUsedMaxAbsDEta = -1.;
	fDPhiProjectionUsedConstFit = kTRUE;
	fDPhiProjectionGaussianFitStatus = -999;
	fDPhiProjectionMinVn = 0;
	fDPhiProjectionMaxVn = 0;

	fDEtaProjectionUsedMaxAbsDPhi = -1.;
	fDEtaProjectionUsedMaxAbsDEta = -1.;
	fDEtaProjectionGaussianFitStatus = -999;

}

// -----------------------------------------------------------------------
Bool_t AliAssociatedYieldDiHadronPID::AnalysisIsDone() const {

	/**
	 * Returns TRUE if the \f$\Delta\phi\f$ and \f$\Delta\eta\f$ analyses
	 * have been performed.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	if (fDPhiProjection &&
		fDPhiProjectionJet &&
		fDPhiProjectionNoJet &&
		fDPhiProjectionGaussianFit &&
		fDPhiProjectionNoJetDCT &&
		fDPhiProjectionTotalFlowFunc &&
		fDphiProjectionTotalFlowUncertaintyBand &&
		fDEtaProjection &&
		fDEtaProjectionGaussianFit) {
		return kTRUE;
	}

	AliLogDiHadronPID::Error(Form("It seems like either the DPhi or DEta analysis was not carried out."), __func__, ClassName());
	
	return kFALSE;

}

// -----------------------------------------------------------------------
void AliAssociatedYieldDiHadronPID::DivideThroughNTriggers() {

    /**
     * Divides the current associated yield with the fNTriggers. This number
     * should have been set during construction of the object (currently this is the 
     * only sensible use, so other uses aren't allowed).
     *
     * Calling this method also makes sure that all Plot* functions produce scaled
     * results (which is probably what you'd commonly want).
     */

    if (fYieldIsPerTrigger) {
        AliLogDiHadronPID::Error("You already divided through the number of triggers. No division is performed.", __func__, ClassName());
        return;
    } 

    if (fNTriggers < 1.) {
        AliLogDiHadronPID::Error("You probably did not supply a number of triggers during initialization. No division was performed.", __func__, ClassName());
        return;
    }

    Scale(1./fNTriggers);
    fYieldIsPerTrigger = kTRUE;
    SetZTitle("#frac{1}{N_{trig}} #frac{d^{2}N}{d#Delta#phi d#Delta#eta}");

}

// -----------------------------------------------------------------------
void AliAssociatedYieldDiHadronPID::PerformDPhiAnalysis(Double_t maxAbsDEta, Double_t jetRadius, Int_t minVn, Int_t maxVn, Bool_t useConstFit) {

	/** 
	 * Make a projection on the \f$\Delta\phi\f$ axis. If jetRadius is a positive number,
	 * then all bins for which the bin center falls within the circle 
	 * \f$\sqrt{\Delta\phi^2+\Delta\eta^2} < jetRadius \f$ are ignored, otherwise nothing
	 * special happens. If useConstFit is true, then then a constant fit is applied on all bins
	 * with the same \f$\Delta\eta\f$, otherwise a regular projection is made. Note that the constant
	 * fit only makes sense when all points have the same expectation value, i.e., far away from the 
	 * peak region.
	 *
	 * After this method has been done, the methods become available: GetDCTCoefficients, GetNSJetYield, 
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
    // Check consistency of the function arugments.
	if (maxVn > 8 || maxVn < minVn || minVn < 0) {
		AliLogDiHadronPID::Error(Form("Maximum VnD is 8, the minimum is 0 (meaning no flow)."), __func__, ClassName());
		return;
	}
	// Cleanup the histograms of the DPhi analysis.
    if (fDPhiProjection) {delete fDPhiProjection; fDPhiProjection = 0x0;}
    if (fDPhiProjectionJet) {delete fDPhiProjectionJet; fDPhiProjectionJet = 0x0;}
    if (fDPhiProjectionNoJet) {delete fDPhiProjectionNoJet; fDPhiProjectionNoJet = 0x0;}
    if (fDPhiProjectionNoJetDCT) {delete fDPhiProjectionNoJetDCT; fDPhiProjectionNoJetDCT = 0x0;}
    if (fDPhiProjectionTotalFlowFunc) {delete fDPhiProjectionTotalFlowFunc; fDPhiProjectionTotalFlowFunc = 0x0;}
    if (fDPhiProjectionGaussianFit) {delete fDPhiProjectionGaussianFit; fDPhiProjectionGaussianFit = 0x0;}

	// Make a projection on the DPhi axis.
	fDPhiProjectionUsedMaxAbsDEta = maxAbsDEta;
	fDPhiProjectionUsedJetRadius = jetRadius;
	fDPhiProjectionUsedConstFit = useConstFit;
	fDPhiProjectionNoJet = AliHistToolsDiHadronPID::CutOutJetAndProjectOnDPhi((const TH2F*)this, fDPhiProjectionUsedMaxAbsDEta, 
		fDPhiProjectionUsedJetRadius, fDPhiProjectionUsedJetRadius, fDPhiProjectionUsedConstFit);

	// Create a symmetrized version of the DPhi background (range should be (0,pi)).
	TH1F* symmBgd = 0x0;
	if (fSettings->GetBeamType() == AliSettingsDiHadronPID::kPbPb) {
		symmBgd = AliHistToolsDiHadronPID::CreateSymmetrisedHist("symmBgd", fDPhiProjectionNoJet);
	} else {
		const TH1F* nearSideBgd = AliHistToolsDiHadronPID::TrimHisto(fDPhiProjectionNoJet, 1, fDPhiProjectionNoJet->GetXaxis()->FindBin(TMath::Pi()/2. - 0.0001));
		symmBgd = AliHistToolsDiHadronPID::ShiftHistogram(nearSideBgd, nearSideBgd->GetXaxis()->FindBin(0.0001));
        delete nearSideBgd;
	}

	// Perform a discrete Fourier transform on the background.
	fDPhiProjectionNoJetDCT = AliHistToolsDiHadronPID::DCT("DCT", symmBgd);

	// Create a TF1, representing the background description from the DCT.
	fDPhiProjectionMinVn = minVn;
	fDPhiProjectionMaxVn = maxVn;

	fDPhiProjectionTotalFlowFunc = GetTotalFlowFunc(fDPhiProjectionMinVn, fDPhiProjectionMaxVn);	
	fDphiProjectionTotalFlowUncertaintyBand = AliMiscToolsDiHadronPID::GetNSigmaBand(fDPhiProjectionTotalFlowFunc);

	// Cleanup.
    if (symmBgd) {delete symmBgd;}

	// Create a DPhi projection.
	fDPhiProjection = (TH1F*)ProjectionX("fDPhiProjection;#Delta#phi", GetYaxis()->FindBin(-fDPhiProjectionUsedMaxAbsDEta + 0.0001), GetYaxis()->FindBin(fDPhiProjectionUsedMaxAbsDEta - 0.0001));

    // To do an integral, scale with the bin width in DEta. Divide though integration range to get an average.
	fDPhiProjection->Scale(GetYaxis()->GetBinWidth(1) / (2. * fDPhiProjectionUsedMaxAbsDEta));
    fDPhiProjection->SetYTitle(fYieldIsPerTrigger ? "#frac{1}{N_{trig}} #LT#frac{dN}{d#Delta#phi}#GT_{|#Delta#eta|}" : "#LT#frac{dN}{d#Delta#phi}#GT_{|#Delta#eta|}");

	// Perform a Gaussian fit on top of the flow background. In principle one should take into account 
	// the statistical uncertainty on the flow background properly, however we have not done this yet.
	// For now we just do a Gaussian fit to the data minus the flow background.
	TH2F* dphideta_minusbgd = new TH2F(*this);
	for (Int_t iBinDPhi = 1; iBinDPhi <= GetNbinsX(); iBinDPhi++) {
		for (Int_t iBinDEta = 1; iBinDEta <= GetNbinsY(); iBinDEta++) {
			dphideta_minusbgd->SetBinContent(iBinDPhi, iBinDEta, GetBinContent(iBinDPhi, iBinDEta) - fDPhiProjectionTotalFlowFunc->Eval(GetXaxis()->GetBinCenter(iBinDPhi)));
		}
	}

	fDPhiProjectionJet = (TH1F*)dphideta_minusbgd->ProjectionX("fDPhiProjectionJet", GetYaxis()->FindBin(-fDPhiProjectionUsedJetRadius + 0.0001), GetYaxis()->FindBin(fDPhiProjectionUsedJetRadius - 0.0001));
    fDPhiProjectionJet->SetYTitle(fYieldIsPerTrigger ? "#frac{1}{N_{trig}} #frac{dN}{d#Delta#phi}" : "#frac{dN}{d#Delta#phi}");

	// fDPhiProjectionGaussianFit = new TF1("fDPhiProjectionGaussianFit", "[0]/(AliFunctionsDiHadronPID::Sqrt2Pi()*[1])*TMath::Exp(-x*x/(2*[1]*[1]))+[2]*(1+2*[3]*cos(x)+2*[4]*cos(2*x)+2*[5]*cos(3*x)+2*[6]*cos(4*x)+2*[7]*cos(5*x)+2*[8]*cos(6*x)+2*[9]*cos(7*x)+2*[10]*cos(8*x))", -1., 1.);
	if (fSettings->GetBeamType() == AliSettingsDiHadronPID::kPbPb) {
		fDPhiProjectionGaussianFit = new TF1("fDPhiProjectionGaussianFit", "[0]+[1]/(AliFunctionsDiHadronPID::Sqrt2Pi()*[2])*TMath::Exp(-x*x/(2*[2]*[2]))", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
	} else {
		fDPhiProjectionGaussianFit = new TF1("fDPhiProjectionGaussianFit", "[0]+[1]/(AliFunctionsDiHadronPID::Sqrt2Pi()*[2])*TMath::Exp(-x*x/(2*[2]*[2]))+[3]/(AliFunctionsDiHadronPID::Sqrt2Pi()*[4])*TMath::Exp(-(x-TMath::Pi())*(x-TMath::Pi())/(2*[4]*[4]))", -TMath::Pi()/2., 3.*TMath::Pi()/2.);	
		fDPhiProjectionGaussianFit->SetParameter(3, fDPhiProjectionNoJet->GetBinContent(GetXaxis()->FindBin(TMath::Pi())));
		fDPhiProjectionGaussianFit->FixParameter(4, 0.8);
	}
	fDPhiProjectionGaussianFit->FixParameter(0,0.);
	fDPhiProjectionGaussianFit->SetParameter(1, fDPhiProjectionNoJet->GetBinContent(GetXaxis()->FindBin(0.)));
	fDPhiProjectionGaussianFit->FixParameter(2, 0.4);

	// Fit DPhi yield in three stages.
	fDPhiProjectionGaussianFitStatus = (Int_t)(fDPhiProjectionJet->Fit(fDPhiProjectionGaussianFit, "rq0"));

	fDPhiProjectionGaussianFit->ReleaseParameter(2);
	if (fSettings->GetBeamType() != AliSettingsDiHadronPID::kPbPb) {fDPhiProjectionGaussianFit->ReleaseParameter(4);}
	fDPhiProjectionGaussianFitStatus = (Int_t)(fDPhiProjectionJet->Fit(fDPhiProjectionGaussianFit, "rq0"));

	fDPhiProjectionGaussianFit->ReleaseParameter(0);
	fDPhiProjectionGaussianFitStatus = (Int_t)(fDPhiProjectionJet->Fit(fDPhiProjectionGaussianFit, "rq0"));


/*
	for (Int_t iFlowCoefficient = 1; iFlowCoefficient <= 8; iFlowCoefficient++) {
		if (iFlowCoefficient >= fDPhiProjectionMinVn && iFlowCoefficient <= fDPhiProjectionMaxVn) {
			fDPhiProjectionGaussianFit->FixParameter(iFlowCoefficient + 2, fDPhiProjectionNoJetDCT->GetBinContent(iFlowCoefficient+1) / fDPhiProjectionNoJetDCT->GetBinContent(1));
		} else {
			fDPhiProjectionGaussianFit->FixParameter(iFlowCoefficient + 2, 0.);
		}
	}
*/

}

// -----------------------------------------------------------------------
TH1F* AliAssociatedYieldDiHadronPID::GetCloneDPhiProjection() const {

	/**
	 * Get a clone of the \f$\Delta\phi\f$ projection. Return 0x0 if no \f$\Delta\phi\f$ 
	 * analysis was performed.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	return (fDPhiProjectionNoJet ? (TH1F*)fDPhiProjectionNoJet->Clone() : 0x0);

}

// -----------------------------------------------------------------------
TH1F* AliAssociatedYieldDiHadronPID::GetCloneDCTCoefficients(Bool_t convertToVnDelta) const {

	/**
	 * Get a clone of the discrete cosine transform (type II) coefficients \f$X_n\f$.
	 * Return 0x0 if no \f$\Delta\phi\f$ analysis was performed. If the option scaleFlow
	 * is true, then the raw DCT coefficients are converted \f$V_{n\Delta}\f$.
	 *  
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	if (!fDPhiProjectionNoJetDCT) {return 0x0;}

	// Scale to flow coefficients if desired.
	TH1F* fOut = (TH1F*)fDPhiProjectionNoJetDCT->Clone();
	if (convertToVnDelta) {
		fOut->Scale(1./fOut->GetBinContent(1));
		fOut->GetXaxis()->SetRange(2, fOut->GetNbinsX()); // No longer plot the first bin (X_0 represents the average density, not a flow coefficient).
		fOut->GetYaxis()->SetTitle("V_{n#Delta}");
	}

	return fOut;

}

// -----------------------------------------------------------------------
void AliAssociatedYieldDiHadronPID::GetFlowCoefficient(Int_t nn, Double_t& vnD, Double_t& error) const {

	/**
	 * Returns the value of flow coefficient and uncertainty.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());	
	if (fDPhiProjectionNoJetDCT && nn > 0 && nn <= GetNbinsX()/2) {

		vnD = fDPhiProjectionNoJetDCT->GetBinContent(nn + 1) / fDPhiProjectionNoJetDCT->GetBinContent(1);
		error = fDPhiProjectionNoJetDCT->GetBinError(nn + 1) / fDPhiProjectionNoJetDCT->GetBinContent(1);

	}
}

// -----------------------------------------------------------------------
void AliAssociatedYieldDiHadronPID::GetNSPeakWidthDPhi(Double_t& width, Double_t& error) const {
	
	/**
	 * If the \f$\Delta\eta\f$ fit has been performed, then return the
	 * fitted width.
	 */

	if (fDPhiProjectionGaussianFit) {
		width = fDPhiProjectionGaussianFit->GetParameter(2);
		error = fDPhiProjectionGaussianFit->GetParError(2);
	}

}

// -----------------------------------------------------------------------
TCanvas* AliAssociatedYieldDiHadronPID::PlotDPhiFit(Color_t dataColor, Style_t markerStyle) const {

	/**
	 * Draws a plot of the fit of the \f$\Delta\phi\f$ projection. Note that this is a projection
     * that is restricted in \f$\Delta\eta\f$, and the flow background has been subtracted.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());	

	if (fDPhiProjectionJet && fDPhiProjectionGaussianFit) {
		TCanvas* cvs = TCanvas::MakeDefCanvas();

        fDPhiProjectionJet->UseCurrentStyle();
        fDPhiProjectionJet->SetMarkerStyle(markerStyle);
        fDPhiProjectionJet->SetMarkerColor(dataColor);
        fDPhiProjectionJet->SetLineColor(dataColor);

		fDPhiProjectionJet->DrawClone();

		fDPhiProjectionGaussianFit->DrawClone("same");

        TLatex* textRange = new TLatex();
        textRange->DrawLatexNDC(0.73,0.87,Form("|#Delta#eta| < %3.1f", fDPhiProjectionUsedMaxAbsDEta));
        
		return cvs;

	}

	return 0x0;

}

// -----------------------------------------------------------------------
TCanvas* AliAssociatedYieldDiHadronPID::PlotBackgroundDPhiDecomposed(Color_t color) const {

	/**
	 * Plots the background and the signal histograms, and the Fourier
	 * coefficients separately.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());	 

	// Check if the analysis was performed.
	if (!AnalysisIsDone()) {return 0x0;}

	// Draw the total projection.
	TCanvas* cvs = PlotSignalAndBackgroundDPhi();

	// Draw the flow decomposition.
	fDPhiProjectionTotalFlowFunc->SetLineColor(color);
	fDPhiProjectionTotalFlowFunc->DrawClone("same");

	if (fDPhiProjectionMinVn > 0) {
		for (Int_t iFlowCoefficient = fDPhiProjectionMinVn; iFlowCoefficient <= fDPhiProjectionMaxVn; iFlowCoefficient++) {
						
			TF1* oneFlowFunc = GetFlowFuncOneHarmonic(iFlowCoefficient);			
			oneFlowFunc->SetLineColor(color - iFlowCoefficient);
			oneFlowFunc->DrawClone("same");

		}
	}

	return cvs;

}

// -----------------------------------------------------------------------
TCanvas* AliAssociatedYieldDiHadronPID::PlotBackgroundDPhiErrorBand(Color_t color) const {

	/**
	 * Plots the background and the signal histograms, and the total fourier
	 * decomposition, including an uncertainty band.
	 *
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());	 

	// Check if the DPhi analysis was performed.
	if (!AnalysisIsDone()) {return 0x0;}

	// Draw a clone of the total projection.
	TCanvas* cvs = PlotSignalAndBackgroundDPhi();

	// Draw a clone of the uncertainty band.
	fDphiProjectionTotalFlowUncertaintyBand->SetFillColorAlpha(color, 0.35);
	fDphiProjectionTotalFlowUncertaintyBand->DrawClone("same3");

	fDPhiProjectionTotalFlowFunc->SetLineColor(color);
	fDPhiProjectionTotalFlowFunc->DrawClone("same");

	return cvs;

}

// -----------------------------------------------------------------------
TCanvas* AliAssociatedYieldDiHadronPID::PlotSignalAndBackgroundDPhi(Color_t backgroundColor, Color_t signalColor) const {

	/**
	 * Plot a \f$\Delta\phi\f$ projection of the signal and of the background.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());		

	// Check if the analysis was performed.
	if (!AnalysisIsDone()) {return 0x0;}

    // Force the current style on the histograms.
    fDPhiProjection->UseCurrentStyle();

	// Draw the total projection.
	TCanvas* cvs = TCanvas::MakeDefCanvas();
    TLegend* lgnd = new TLegend(0.6,0.7,0.9,0.85);

	TH1F* tmpProj = (TH1F*)fDPhiProjection->Clone("tmpProj");
	tmpProj->SetLineColor(signalColor);
	tmpProj->SetMarkerColor(signalColor);
	tmpProj->SetMarkerStyle(21);
	tmpProj->Draw();
    //lgnd->AddEntry(tmpProj, Form("|#Delta#eta| < %3.1f", fDPhiProjectionUsedMaxAbsDEta));

	TH1F* tmpProjBgd = (TH1F*)fDPhiProjectionNoJet->Clone("tmpProjBgd");
	tmpProjBgd->SetMarkerStyle(22);
	tmpProjBgd->SetLineColor(backgroundColor);
	tmpProjBgd->SetMarkerColor(backgroundColor);
	tmpProjBgd->Draw("same");
    lgnd->AddEntry(tmpProjBgd, Form("#sqrt{#Delta#phi^{2}+#Delta#eta^{2}} > %3.1f", fDPhiProjectionUsedJetRadius));

    // The overall eta range of the entire plot.
    TLatex* textRange = new TLatex();
    textRange->DrawLatexNDC(0.73,0.87,Form("|#Delta#eta| < %3.1f", fDPhiProjectionUsedMaxAbsDEta));

    // The legend, to signify the jet cut.
    lgnd->Draw();

	return cvs;

}

// -----------------------------------------------------------------------
TF1* AliAssociatedYieldDiHadronPID::GetTotalFlowFunc(Int_t minVn, Int_t maxVn) const {

	/**
	 * Returns a function of the total flow, using only harmonics between minVn and maxVn.
	 */

	// Check consistency of the function arugments.
	if (maxVn > 8 || maxVn < minVn || minVn < 0) {
		AliLogDiHadronPID::Error(Form("Maximum VnD is 8, the minimum is 0 (meaning no flow)."), __func__, ClassName());
		return 0x0;
	}

	if (!fDPhiProjectionNoJetDCT) {return 0x0;}

	TF1* totalFlowFunc = new TF1("totalFlowFunc", "[0]*(1+2*[1]*cos(x)+2*[2]*cos(2*x)+2*[3]*cos(3*x)+2*[4]*cos(4*x)+2*[5]*cos(5*x)+2*[6]*cos(6*x)+2*[7]*cos(7*x)+2*[8]*cos(8*x))", -TMath::Pi()/2., 3.*TMath::Pi()/2.);

	if (fSettings->GetBeamType() == AliSettingsDiHadronPID::kPbPb) {
		totalFlowFunc->SetParameter(0, fDPhiProjectionNoJetDCT->GetBinContent(1) / GetNbinsX());
		totalFlowFunc->SetParError(0, fDPhiProjectionNoJetDCT->GetBinError(1) / GetNbinsX());
	} else {
		totalFlowFunc->SetParameter(0, 2. * fDPhiProjectionNoJetDCT->GetBinContent(1) / GetNbinsX());
		totalFlowFunc->SetParError(0, 2. * fDPhiProjectionNoJetDCT->GetBinError(1) / GetNbinsX());
	}

	if (minVn > 0) {
		for (Int_t iFlowCoefficient = minVn; iFlowCoefficient <= maxVn; iFlowCoefficient++) {
			
			Double_t scaledFlow = fDPhiProjectionNoJetDCT->GetBinContent(iFlowCoefficient + 1) / fDPhiProjectionNoJetDCT->GetBinContent(1);	
			Double_t scaledFlowError = fDPhiProjectionNoJetDCT->GetBinError(iFlowCoefficient + 1) / fDPhiProjectionNoJetDCT->GetBinContent(1);

			totalFlowFunc->SetParameter(iFlowCoefficient, scaledFlow);
			totalFlowFunc->SetParError(iFlowCoefficient, scaledFlowError);
		
		}
	}

	return totalFlowFunc;

}

// -----------------------------------------------------------------------
TF1* AliAssociatedYieldDiHadronPID::GetFlowFuncOneHarmonic(Int_t nn) const {

	/**
	 * Returns a function of one of the flow harmonics.
	 */

	// Check if the analysis was performed.
	if (!AnalysisIsDone()) {return 0x0;}

	TF1* oneFlowFunc = new TF1("oneFlowFunc", "[0]*(1+2*[1]*cos([2]*x))", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
	oneFlowFunc->SetLineStyle(2);

	if (fSettings->GetBeamType() == AliSettingsDiHadronPID::kPbPb) {
		oneFlowFunc->SetParameter(0, fDPhiProjectionNoJetDCT->GetBinContent(1) / GetNbinsX());
		oneFlowFunc->SetParError(0, fDPhiProjectionNoJetDCT->GetBinError(1) / GetNbinsX());
	} else {
		oneFlowFunc->SetParameter(0, 2. * fDPhiProjectionNoJetDCT->GetBinContent(1) / GetNbinsX());
		oneFlowFunc->SetParError(0, 2. * fDPhiProjectionNoJetDCT->GetBinError(1) / GetNbinsX());
	}


	Double_t scaledFlow = fDPhiProjectionNoJetDCT->GetBinContent(nn + 1) / fDPhiProjectionNoJetDCT->GetBinContent(1);
	Double_t scaledFlowError = fDPhiProjectionNoJetDCT->GetBinError(nn + 1) / fDPhiProjectionNoJetDCT->GetBinContent(1);

	oneFlowFunc->SetParameter(1, scaledFlow);
	oneFlowFunc->SetParError(1, scaledFlowError);
	oneFlowFunc->SetParameter(2, (Double_t)nn);
	oneFlowFunc->SetParError(2, 0.);

	return oneFlowFunc;

}

// -----------------------------------------------------------------------
void AliAssociatedYieldDiHadronPID::PerformDEtaAnalysis(Double_t maxAbsDPhi, Double_t maxAbsDEta) {

	/**
	 * Make a projection on the \f$\Delta\eta\f$ axis. The user can specify the 
	 * desired range of the analysis: \f$|\Delta\phi| < maxAbsDPhi\f$.
	 *
	 * After applying this method, the following methods become available: ...
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	// Cleanup.
	if (fDEtaProjection) {delete fDEtaProjection; fDEtaProjection = 0x0;}
	if (fDEtaProjectionGaussianFit) {delete fDEtaProjectionGaussianFit; fDEtaProjectionGaussianFit = 0x0;}

	// Create a dEta projection.
	fDEtaProjectionUsedMaxAbsDPhi = maxAbsDPhi;
	fDEtaProjectionUsedMaxAbsDEta = maxAbsDEta;
	fDEtaProjection = (TH1F*)ProjectionY("dEtaProjection", GetXaxis()->FindBin(-fDEtaProjectionUsedMaxAbsDPhi + 0.00001), GetXaxis()->FindBin(fDEtaProjectionUsedMaxAbsDPhi - 0.00001));
	fDEtaProjection->Scale(GetXaxis()->GetBinWidth(1)); // Needed because the Projection method doesn't know the original histogram is a density.
    fDEtaProjection->SetYTitle(fYieldIsPerTrigger ? "#frac{1}{N_{trig}} #frac{dN}{d#Delta#eta}" : "#frac{dN}{d#Delta#eta}");

	// Perform a Gaussian fit on the projection.
	fDEtaProjectionGaussianFit = new TF1("etaFit", "[0]+[1]/(AliFunctionsDiHadronPID::Sqrt2Pi()*[2])*TMath::Exp(-x*x/(2*[2]*[2]))", -fDEtaProjectionUsedMaxAbsDEta, fDEtaProjectionUsedMaxAbsDEta);
	fDEtaProjectionGaussianFit->SetParameter(0, fDEtaProjection->GetBinContent(3));
    Double_t yieldEstimate = 4. * (fDEtaProjection->GetBinContent(fDEtaProjection->FindBin(0.)) - fDEtaProjection->GetBinContent(3)) * fDEtaProjection->GetBinWidth(1);
	fDEtaProjectionGaussianFit->SetParameter(1, yieldEstimate);
	fDEtaProjectionGaussianFit->SetParameter(2, 0.5);
	fDEtaProjectionGaussianFitStatus = (Int_t)(fDEtaProjection->Fit(fDEtaProjectionGaussianFit, "rq0"));

	// Perform a Double-Gaussian fit on the projection.
	fDEtaProjectionDoubleGaussianFit = new TF1("etaFit", "[0]+[1]*([2]/(AliFunctionsDiHadronPID::Sqrt2Pi()*[3])*TMath::Exp(-x*x/(2*[3]*[3]))+(1-[2])/(AliFunctionsDiHadronPID::Sqrt2Pi()*[4])*TMath::Exp(-x*x/(2*[4]*[4])))", -fDEtaProjectionUsedMaxAbsDEta, fDEtaProjectionUsedMaxAbsDEta);
	fDEtaProjectionDoubleGaussianFit->SetParameter(0, fDEtaProjectionGaussianFit->GetParameter(0));
	fDEtaProjectionDoubleGaussianFit->SetParameter(1, fDEtaProjectionGaussianFit->GetParameter(1));
	fDEtaProjectionDoubleGaussianFit->SetParameter(2, 0.9); // Fraction in first peak
	fDEtaProjectionDoubleGaussianFit->SetParameter(3, fDEtaProjectionGaussianFit->GetParameter(2));
	fDEtaProjectionDoubleGaussianFit->SetParameter(4, 2.*fDEtaProjectionGaussianFit->GetParameter(2)); // Twice as wide as first peak
	fDEtaProjectionDoubleGaussianFitStatus = (Int_t)(fDEtaProjection->Fit(fDEtaProjectionDoubleGaussianFit, "rq0"));

}

// -----------------------------------------------------------------------
TH1F* AliAssociatedYieldDiHadronPID::GetCloneDEtaProjection() const {

	/**
	 * Get a clone of the \f$\Delta\eta\f$ projection. Return 0x0 if no \f$\Delta\eta\f$ 
	 * analysis was performed.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	return (fDEtaProjection ? (TH1F*)fDEtaProjection->Clone() : 0x0);

}

// -----------------------------------------------------------------------
void AliAssociatedYieldDiHadronPID::GetNSJetYieldDEtaFit(Double_t& yield, Double_t& error) const {

	/**
	 * If the \f$\Delta\eta\f$ fit has been performed, then return the
	 * fitted yield.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	if (fDEtaProjectionGaussianFit) {
		yield = fDEtaProjectionGaussianFit->GetParameter(1);
		error = fDEtaProjectionGaussianFit->GetParError(1);
	}

}

// -----------------------------------------------------------------------
void AliAssociatedYieldDiHadronPID::GetNSPeakWidthDEta(Double_t& width, Double_t& error) const {

	/**
	 * If the \f$\Delta\eta\f$ fit has been performed, then return the
	 * fitted width.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	if (fDEtaProjectionGaussianFit) {
		width = fDEtaProjectionGaussianFit->GetParameter(2);
		error = fDEtaProjectionGaussianFit->GetParError(2);
	}

}

// -----------------------------------------------------------------------
void AliAssociatedYieldDiHadronPID::GetNSPeakDEtaFrac(Double_t& frac, Double_t& error) const {

	/**
	 * If the \f$\Delta\eta\f$ fit has been performed, then return the
	 * fitted width.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	if (fDEtaProjectionDoubleGaussianFit) {
		frac = fDEtaProjectionDoubleGaussianFit->GetParameter(2);
		error = fDEtaProjectionDoubleGaussianFit->GetParError(2);
	}

}

// -----------------------------------------------------------------------
void AliAssociatedYieldDiHadronPID::GetNSPeakWidth1DEta(Double_t& width, Double_t& error) const {

	/**
	 * If the \f$\Delta\eta\f$ fit has been performed, then return the
	 * fitted width.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	if (fDEtaProjectionDoubleGaussianFit) {
		width = fDEtaProjectionDoubleGaussianFit->GetParameter(3);
		error = fDEtaProjectionDoubleGaussianFit->GetParError(3);
	}

}

// -----------------------------------------------------------------------
void AliAssociatedYieldDiHadronPID::GetNSPeakWidth2DEta(Double_t& width, Double_t& error) const {

	/**
	 * If the \f$\Delta\eta\f$ fit has been performed, then return the
	 * fitted width.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	if (fDEtaProjectionDoubleGaussianFit) {
		width = fDEtaProjectionDoubleGaussianFit->GetParameter(4);
		error = fDEtaProjectionDoubleGaussianFit->GetParError(4);
	}

}

// -----------------------------------------------------------------------
TCanvas* AliAssociatedYieldDiHadronPID::PlotDEtaFit(Color_t dataColor, Style_t markerStyle) const {

	/**
	 * Draws the result of the Gaussian \f$\Delta\eta\f$ fit.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	if (fDEtaProjection && fDEtaProjectionGaussianFit) {

		TCanvas* cOut = TCanvas::MakeDefCanvas();

        // FIXME: It would have been nicer not to enforce the current style onto the original object.
        // It seems however that some settings don't transfer if we act on the ptr returned by DrawClone().
        fDEtaProjection->UseCurrentStyle();
        fDEtaProjection->SetMarkerStyle(markerStyle);
        fDEtaProjection->SetMarkerColor(dataColor);
        fDEtaProjection->SetLineColor(dataColor);
       
		fDEtaProjection->DrawClone();
        
		fDEtaProjectionGaussianFit->DrawClone("same");
        TLatex* textRange = new TLatex();
        textRange->DrawLatexNDC(0.66,0.87,Form("|#Delta#phi| < %3.1f rad", fDEtaProjectionUsedMaxAbsDPhi));
		return cOut;

	}

	return 0x0;

}

// -----------------------------------------------------------------------
TCanvas* AliAssociatedYieldDiHadronPID::PlotDEtaFitDG(Color_t dataColor, Style_t markerStyle) const {

	/**
	 * Draws the result of the double-Gaussian \f$\Delta\eta\f$ fit.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	if (fDEtaProjection && fDEtaProjectionDoubleGaussianFit) {

		TCanvas* cOut = TCanvas::MakeDefCanvas();
        fDEtaProjection->UseCurrentStyle();
        fDEtaProjection->SetMarkerStyle(markerStyle);
        fDEtaProjection->SetMarkerColor(dataColor);
        fDEtaProjection->SetLineColor(dataColor);
        
		fDEtaProjection->DrawClone();
		fDEtaProjectionDoubleGaussianFit->DrawClone("same");

		TF1* tmpWideGauss = new TF1("wideGauss", "[0]+[1]/(AliFunctionsDiHadronPID::Sqrt2Pi()*[2])*TMath::Exp(-x*x/(2*[2]*[2]))", -fDEtaProjectionUsedMaxAbsDEta, fDEtaProjectionUsedMaxAbsDEta);
		tmpWideGauss->SetParameter(0, fDEtaProjectionDoubleGaussianFit->GetParameter(0));
		tmpWideGauss->SetParameter(1, fDEtaProjectionDoubleGaussianFit->GetParameter(1)*(1.-fDEtaProjectionDoubleGaussianFit->GetParameter(2)));
		tmpWideGauss->SetParameter(2, fDEtaProjectionDoubleGaussianFit->GetParameter(4));
		tmpWideGauss->SetLineColor(kBlue);
		tmpWideGauss->SetLineStyle(2);
		tmpWideGauss->Draw("same");

        TLatex* textRange = new TLatex();
        textRange->DrawLatexNDC(0.66,0.87,Form("|#Delta#phi| < %3.1f rad", fDEtaProjectionUsedMaxAbsDPhi));

		return cOut;

	}

	return 0x0;

}

// -----------------------------------------------------------------------
void AliAssociatedYieldDiHadronPID::GetNSJetYield(Double_t& yield, Double_t& error, Double_t jetRadiusSignal, Double_t* bgdYield) const {

	/**
	 * Return the value and stat. uncertainty on the NS peak yield, in a circle jetRadiusSignal.
	 * This method assumes that the \f$\Delta\phi\f$ analysis has been performed, so that
	 * a background can be subtracted. Note that the jet radius used for the signal must be smaller
	 * or equal to the jet radius used for the \f$\Delta\phi\f$ analysis. An example of when one 
	 * would want to make them unequal is when the jet peak has a long tail (doesn't add much to
	 * the peak yield, but it may affect the measured flow coefficients).
	 *
	 * The signal yield is calculated, assuming uncorrelated uncertainties on all points. For the background
	 * yield we do assume correlated uncertainties. The uncertainty on the total yield is calulated as the integral
	 * between the optimal value of the background function and the top of the uncertainty band.
	 *
	 * \param yield The jet yield, measured in the default way.
	 * \param error The uncertainty on the jet yield.
	 * \param jetRadiusSignal The size of the circle in which the peak-bulk is calculated.
	 * \param bgdYield The background yield (evaluated DCT) that is subtracted from the peak yield (first element), and the
	 * uncertainty on that yield (second element). This ptr can be safely left to be 0x0.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	// Check if the analysis was performed.
	if (!AnalysisIsDone()) {return;}

	// Check if the desired jet radius is smaller then or equal to the jet radius in the flow analysis.
	if (jetRadiusSignal > fDPhiProjectionUsedJetRadius) {
		AliLogDiHadronPID::Error(Form("Jet radius used for counting the jet yield cannot be greater then the radius cut away when measuring the background."), __func__, ClassName());
		return;		
	}

	// Obtain a TGraph following the upper edge of the total flow uncertainty band.
	TGraph* totalFlowFuncPlusOneSigma = AliMiscToolsDiHadronPID::GetErrorBandGraph(fDphiProjectionTotalFlowUncertaintyBand);

	// Define yield variables and uncertainties.
	Double_t backgroundYield = 0.;
	Double_t backgroundYieldPlusSigma = 0.;
	Double_t signalYield = 0.;
	Double_t signalUncertainty2 = 0.;

	// Loop over all bins that fall within the jet radius.
	for (Int_t iBinDPhi = 1; iBinDPhi <= GetNbinsX(); iBinDPhi++) {
		
		Double_t binCenterDPhi = GetXaxis()->GetBinCenter(iBinDPhi);
		for (Int_t iBinDEta = 1; iBinDEta <= GetNbinsY(); iBinDEta++) {
		
			Double_t binCenterDEta = GetYaxis()->GetBinCenter(iBinDEta);

			if (TMath::Power(binCenterDPhi / jetRadiusSignal, 2) + 
				TMath::Power(binCenterDEta / jetRadiusSignal, 2) > 1.) {continue;}

			
			backgroundYield += fDPhiProjectionTotalFlowFunc->Eval(binCenterDPhi);
			backgroundYieldPlusSigma += totalFlowFuncPlusOneSigma->Eval(binCenterDPhi);

			signalYield += GetBinContent(iBinDPhi, iBinDEta);
			signalUncertainty2 += (GetBinError(iBinDPhi, iBinDEta) * GetBinError(iBinDPhi, iBinDEta));

		}
	}

	yield = signalYield - backgroundYield;
	error = TMath::Sqrt(signalUncertainty2 + TMath::Power(backgroundYieldPlusSigma - backgroundYield, 2));

	// Scale with bin width of the 2D histogram (correct for the fact that it is a density).
	Double_t binSize = GetXaxis()->GetBinWidth(1) * GetYaxis()->GetBinWidth(1);

	yield *= binSize;
	error *= binSize;

	// If a ptr bgdYield has been supplied, then update it.
	if (bgdYield) {
		bgdYield[0] = backgroundYield * binSize;
		bgdYield[1] = (backgroundYieldPlusSigma - backgroundYield) * binSize;
	}

}

// -----------------------------------------------------------------------
void AliAssociatedYieldDiHadronPID::GetBackgroundDensityDPhiDEta(Double_t& density, Double_t& error) {

	/**
	 * Returns the background yield per unit \f$(\Delta\phi\times\Delta\eta)\f$.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	// Check if the analysis was performed.
	if (!AnalysisIsDone()) {return;}

	density = fDPhiProjectionNoJetDCT->GetBinContent(1) / (fDPhiProjectionNoJetDCT->GetNbinsX());
	error = fDPhiProjectionNoJetDCT->GetBinError(1) / (fDPhiProjectionNoJetDCT->GetNbinsX());

	// Note that the factor 1/2 is needed to compensate for the fact that we symmetrized, i.e.,
	// the true number of bins that we take into account is twice as large as in the symmetrized case.
	// As we don't symmetrize for pp or pPb, this is only needed for PbPb.
	if (fSettings->GetBeamType() == 0) {
		density/=2.;
		error/=2.;
	}

}

// -----------------------------------------------------------------------
TCanvas* AliAssociatedYieldDiHadronPID::PlotJetPeak2DStack() const {

    /**
     * Draws a 2D stack histogram of the background, and, with a 
     * different color, the jet peak on top.
     *
     * \bug Range on the z-axis does not match the range on the 
     * z-axis of the regular distribution.
     */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	// Check if the analysis was performed.
	if (!AnalysisIsDone()) {return 0x0;}

    // Create canvas.
    TCanvas* cvs = TCanvas::MakeDefCanvas();

    // Create histograms for the background and the jet peak.
    TH2F* hBackground = new TH2F(*this);
    hBackground->SetFillColor(kAzure+1);
    hBackground->SetLineColorAlpha(kBlack,0.3);
    hBackground->GetYaxis()->SetRange(GetYaxis()->GetFirst(), GetYaxis()->GetLast());
    
//    hBackground->Draw("lego2");
//    Double_t min = hBackground->GetMinimum();
 //   Double_t max = hBackground->GetMaximum();
  //  cout << min << " " << max << endl;
    TH2F* hJetPeak = new TH2F(*this);
    hJetPeak->SetFillColor(kRed-4);
    hJetPeak->SetLineColorAlpha(kBlack,0.3);
    // hJetPeak->GetYaxis()->SetRange(GetYaxis()->GetFirst(), GetYaxis()->GetLast());

	for (Int_t iBinDPhi = 1; iBinDPhi <= GetNbinsX(); iBinDPhi++) {
		
		Double_t binCenterDPhi = GetXaxis()->GetBinCenter(iBinDPhi);
		for (Int_t iBinDEta = 1; iBinDEta <= GetNbinsY(); iBinDEta++) {
		
			Double_t binCenterDEta = GetYaxis()->GetBinCenter(iBinDEta);
            
            // Check if the bin falls within the circle.
			if (TMath::Power(binCenterDPhi / fDPhiProjectionUsedJetRadius, 2) + 
				TMath::Power(binCenterDEta / fDPhiProjectionUsedJetRadius, 2) > 1.) {
            
                hJetPeak->SetBinContent(iBinDPhi, iBinDEta, 0);
                 
            } else {

                hJetPeak->SetBinContent(iBinDPhi, iBinDEta, hJetPeak->GetBinContent(iBinDPhi, iBinDEta) - fDPhiProjectionTotalFlowFunc->Eval(binCenterDPhi));
                hBackground->SetBinContent(iBinDPhi, iBinDEta, fDPhiProjectionTotalFlowFunc->Eval(binCenterDPhi));

            }
		}
	}

    // Create a stack.
    THStack* sAssocYieldStack = new THStack("sAssocYield", "");
    TH2F* hAxis = new TH2F(*this);
/*    hAxis->GetXaxis()->SetTitle("#Delta#phi"); 
    hAxis->GetYaxis()->SetTitle("#Delta#eta"); 
    hAxis->GetZaxis()->SetTitle("#frac{d^2N}{d#Delta#phi d#Delta#eta}"); 
    sAssocYieldStack->SetHistogram(hAxis);*/

    sAssocYieldStack->Add(hBackground);
    sAssocYieldStack->Add(hJetPeak);
   
   
    // Plot the stack.
    sAssocYieldStack->UseCurrentStyle();
    sAssocYieldStack->Draw();
    //sAssocYieldStack->GetHistogram()->GetZaxis()->SetRangeUser(0.,1.);
    //sAssocYieldStack->Draw("lego1");
    sAssocYieldStack->GetYaxis()->SetRange(GetYaxis()->GetFirst(), GetYaxis()->GetLast());
    sAssocYieldStack->GetXaxis()->SetTitle("#Delta#phi (rad)"); 
    sAssocYieldStack->GetYaxis()->SetTitle("#Delta#eta"); 
    sAssocYieldStack->GetHistogram()->GetZaxis()->SetTitle(fYieldIsPerTrigger ? "#frac{1}{N_{trig}} #frac{d^{2}N}{d#Delta#phi d#Delta#eta}" : "#frac{d^{2}N}{d#Delta#phi d#Delta#eta}"); 
    //sAssocYieldStack->GetHistogram()->GetZaxis()->SetTitleOffset(1.4);   

    // Make sure that the range on the z-axis is the same as for the data plot (FIXME)
    //sAssocYieldStack->SetMinimum(9);
    //sAssocYieldStack->SetMaximum(10*0.96);
    
    sAssocYieldStack->SetMaximum(GetMaximum());
    sAssocYieldStack->SetMinimum(GetMinimum());    
    return cvs;

} 

