#ifndef ALIMINIEVENTDIHADRONPID_H
#define ALIMINIEVENTDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TObject.h"
#include "TClonesArray.h"
#include "AliAODEvent.h"
#include "AliMiniTrackDiHadronPID.h"

class AliMiniEventDiHadronPID : public TObject {

public:
	enum BeamType {
		kPbPb 	= 0,
		kpPb 	= 1,
		kpp 	= 2
	};

public:
	AliMiniEventDiHadronPID();
	~AliMiniEventDiHadronPID();

private:

	// Initializer for lazy instantiation of the array.
	void 						Init();

public:

	// Setters.
	void						SetVtxZ(Float_t vtxz) {fVtxZ = vtxz;}
	void						SetVtxZIsMainSPD(Bool_t isMainSPD) {fVtxZIsMainSPD = isMainSPD;}
	void						SetVtxZNContributers(Int_t nCont) {fVtxZNCont = nCont;}
	
	void						SetCentrality(Float_t cent) {fCent = cent;}
	void						SetCentralityIsBad(Bool_t qualityIsBad) {fCentIsBad = qualityIsBad;}

	// Getters.
	Float_t						GetVtxZ() const {return fVtxZ;}
	Bool_t 						GetVtxZIsMainSPD() const {return fVtxZIsMainSPD;}
	Int_t						GetVtxZNContributers() const {return fVtxZNCont;}

	Float_t						GetCentrality() const {return fCent;}
	Bool_t 						GetCentralityIsBad() const {return fCentIsBad;}

	Int_t						GetNTracks() const {return (fTracks ? fTracks->GetEntriesFast() : 0);}
	AliMiniTrackDiHadronPID* 	GetTrack(Int_t index) {return (fTracks ? (AliMiniTrackDiHadronPID*)(fTracks->At(index)) : 0x0);}

	static Int_t				GetNInstances() {return fNInstances;}

	// Other Functions.
	AliMiniTrackDiHadronPID* 	NextParticle();
	void						Done();			// Remove remaining trivial entries at the end of the track arrays.
	void 						ResetEvent();	

	// Method for taking relevant parameters from an AOD event. Better to use
	// this method than the setters. Note that the pointer is not const, since
	// the GetCentrality() method in AliAODEvent is not marked const.
	void 						CopyPars(AliAODEvent* event, BeamType beamtype, const char* centralityEstimator = "");

private:
	// Counter for the number of constructor calls minus the number of destructor calls.
	static Int_t				fNInstances;

	Float_t 					fVtxZ;
	Bool_t						fVtxZIsMainSPD;	// 
	Int_t						fVtxZNCont;		// The number of contributers used for vtxz determination.
	
	Float_t 					fCent;
	Bool_t						fCentIsBad;		// AliCentrality::GetQuality returns 0 if the centrality is OK

	Int_t 						fTrackIndex;	//!

	// Version 3 and lower only contained fAssocTracks and fTriggerTracks.
	TClonesArray*				fTracks;		//->
	
	ClassDef(AliMiniEventDiHadronPID, 4);

};

#endif
