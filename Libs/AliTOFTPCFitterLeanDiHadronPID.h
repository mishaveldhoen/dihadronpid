#ifndef ALITOFTPCFITTERLEANDIHADRONPID_H
#define ALITOFTPCFITTERLEANDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TObject.h"
#include "Minuit2/Minuit2Minimizer.h"
#include "AliTOFTPCFitContainerDiHadronPID.h"
#include "AliSettingsDiHadronPID.h"

class AliTOFTPCFitterLeanDiHadronPID : public TObject {
	
public:
	AliTOFTPCFitterLeanDiHadronPID();
	AliTOFTPCFitterLeanDiHadronPID(AliTOFTPCFitContainerDiHadronPID* container);
	~AliTOFTPCFitterLeanDiHadronPID();

	void								SetFitContainer(AliTOFTPCFitContainerDiHadronPID* container) {fFitContainer = container;}
	AliTOFTPCFitContainerDiHadronPID*	GetFitContainer() {return fFitContainer;}	

	Int_t								Fit(Bool_t reset = kFALSE, Bool_t showFullDebugInfo = kFALSE, Bool_t checkTolerances = kFALSE);
	Int_t								FitOnlyYields();

	void								Print(Option_t*) const;

private:
	const char*							GetMinuit2StatusMessage(Int_t status) const;

	Bool_t								SetMinimizerSettings();
	Int_t								Minimize();	
	Bool_t								PrintHessian();	

private:

	// pointer to settings singleton
	AliSettingsDiHadronPID* 			fSettings; //!

	AliTOFTPCFitContainerDiHadronPID*	fFitContainer;
	ROOT::Minuit2::Minuit2Minimizer*	fMinimizer;
	
	ClassDef(AliTOFTPCFitterLeanDiHadronPID, 1);

};

#endif
