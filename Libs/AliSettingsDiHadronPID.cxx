/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Singleton class, containing global  settings. For example:
//    - Details about the region where mismatches are fitted.
//    - The size of the Fit Window.
//    - TOF, TPC, phi, dPhi, eta, dEta, pT axes.
//
//  The idea is that before doing any local , one first runs a
//  macro that configures an instance of this class. After that, all other
//  classes will look at the settings class to obtain the settings.
//
//  To make sure that the user doesn't forget to first run a
//  configuration macro, the config. macro will call Instance with a 
//  kTRUE argument.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "AliSettingsDiHadronPID.h"
#include "AliHistToolsDiHadronPID.h"
#include "AliFunctionsDiHadronPID.h"
#include "AliLogDiHadronPID.h"
#include "TStyle.h"

#include <iostream>
using namespace std;

AliSettingsDiHadronPID* AliSettingsDiHadronPID::fSettings = 0x0;

const Int_t AliSettingsDiHadronPID::fNBeamTypes = 3;
const char* AliSettingsDiHadronPID::fBeamTypeNames[3] = {"PbPb", "pPb", "pp"};

const Int_t AliSettingsDiHadronPID::fNSpecies = 6;
const Int_t AliSettingsDiHadronPID::fNMassAssumptions = 3;
const char* AliSettingsDiHadronPID::fSpeciesNames[7] = {"Pion", "Kaon", "Proton", "Deuteron", "Electron", "Muon", "Unknown"};
const char* AliSettingsDiHadronPID::fSpeciesNamesShort[7] = {"Pi", "Ka", "Pr", "De", "El", "Mu", "??"};
const char*	AliSettingsDiHadronPID::fSpeciesLaTeX[7] = {"#pi", "K", "p", "d", "e", "#mu", "??"};
const Double_t AliSettingsDiHadronPID::fSpeciesMass[7] = {0.13957018, 0.493667, 0.938272046, 1.87612384631, 0.000510998910, 0.105658371535, -1.};
const Style_t AliSettingsDiHadronPID::fSpeciesPlotMarker[7] = {20, 21, 22, 23 ,24, 25, 0};
const Color_t AliSettingsDiHadronPID::fSpeciesColor[7] = {kRed, kGreen, kBlue, kYellow, kMagenta, kCyan, kWhite}; // EColor enum defined in Rtypes.h

ClassImp(AliSettingsDiHadronPID);

// -----------------------------------------------------------------------
AliSettingsDiHadronPID::AliSettingsDiHadronPID(Bool_t isConfigured):
	TObject(),
	fIsConfigured(isConfigured),
	fBeamType(0),
	fIsMC(kFALSE),
	fTOFMismatchInfoIsAvailableMC(kFALSE),
	fFilterMask(0),
	fFilterMaskDCATemplates(0),
        fUseDEDxRatios(0),
        fPPEfficiencyFactor(1.0),
	fDataPath(),
	fWorkingDir(),
	fEfficienciesDir(),
	fUISpectrumFileName("PtSpectrumUI.root"),
	fSpectraFileName("PtEtaSpectra.root"),
	fPtSpectraFileName("PtSpectra.root"),
	fTemplatesFileName("Templates.root"),
	fPIDCorrelationsFileName("PIDCorrelations.root"),
	fEfficienciesFileName("Efficiencies.root"),
	fMinClustersdEdx(0),
	fMaxClustersdEdx(160),
	fDemandTOFTPCPIDFlags(kFALSE),
	fDemandTightDCACut(kFALSE),
	fDemandT0TOFStartTime(kFALSE),
	fRequestedCharge(kAllCharged),
	fRapidityCutForInclusiveSpectra(kFALSE),
	fNTriggersToMix(10),
	fMinPtTrig(4.),
	fMaxPtTrig(10.),
	fMinPtAssoc(1.),
	fMaxPtAssoc(4.),	
	fMinDistToDetermineMismatch(300.),
	fDistFromPeakForMismatch(1000.), 
	fMinEntriesForChi2(5),
	fUseSqrtModelValueForErrorChi2(kFALSE),
	fMinEntriesForLL(0),
	fStdTOFResolution(85.),
	fStdTOFShift(0.),
	fStdTPCResolution(3.5),
	fStdTPCElectronShift(0.),
	fUseChi2BelowPt(1.0),
	fTOFFitRangeMin(300.),
	fTOFFitRangeMax(600.),
	fTPCFitRangeMin(10.),
	fTPCFitRangeMax(10.),
	fUseFitPenalty(kFALSE),
	fUseMCLabelForMismatches(kFALSE),
        fMuTOFTolerance(0.2),
        fMuTPCTolerance(0.2),
	fCentralityAxis(0x0),
	fVtxZAxis(0x0),
	fPtAxis(0x0),
	fEtaAxis(0x0),
	fDEtaAxis(0x0),
	fPhiAxis(0x0),
	fDPhiAxis(0x0),
	fDCAAxis(0x0)

{
	
	// Default Constructor
	AliLogDiHadronPID::Debug2(Form("Default constructor called%s", (fIsConfigured ? " by config. macro." : ".")), __func__, Class_Name());

	for (Int_t iMassAssumption = 0; iMassAssumption < fNMassAssumptions; iMassAssumption++) {
			
		fTOFAxis[iMassAssumption] = 0x0;
		fTPCAxis[iMassAssumption] = 0x0;

	}

	// Default Style Settings.
	gStyle->SetCanvasDefW(500);		// Square canvases.
	gStyle->SetOptStat(0);			// No Stat info in the plots.
	gStyle->SetHistLineWidth(2);
	//gStyle->SetTextSizePixels(9);
    gStyle->SetOptTitle(0);         // No histogram titles. 

	// Set default values for fit peak settings.
	for (Int_t iMassAssumption = 0; iMassAssumption <= fNSpecies; iMassAssumption++) {
		for (Int_t iSpecies = 0; iSpecies <= fNSpecies; iSpecies++) {
					
			fPeakIsIncluded[iMassAssumption][iSpecies] = (iMassAssumption == iSpecies ? kTRUE : kFALSE);			
			fPeakIsIncludedMinPt[iMassAssumption][iSpecies] = 0.;
			fPeakIsIncludedMaxPt[iMassAssumption][iSpecies] = 5.;

			fTOFTailIsFixed[iMassAssumption][iSpecies] = kFALSE;
			fTOFTailIsFixedMinPt[iMassAssumption][iSpecies] = 0.;
			fTOFTailIsFixedMaxPt[iMassAssumption][iSpecies] = 5.;
			fTOFTailIsFixedValue[iMassAssumption][iSpecies] = 0.009;

			fMuTOFIsFixed[iMassAssumption][iSpecies] = kFALSE;
			fSigmaTOFIsFixed[iMassAssumption][iSpecies] = kFALSE;

			fTPCTailIsIncluded[iMassAssumption][iSpecies] = kFALSE;
			fTPCTailIsIncludedMinPt[iMassAssumption][iSpecies] = 0.;
			fTPCTailIsIncludedMaxPt[iMassAssumption][iSpecies] = 5.;

			fTPCTailIsFixed[iMassAssumption][iSpecies] = kFALSE;
			fTPCTailIsFixedMinPt[iMassAssumption][iSpecies] = 0.;
			fTPCTailIsFixedMaxPt[iMassAssumption][iSpecies] = 5.;
			fTPCTailIsFixedValue[iMassAssumption][iSpecies] = 0.1;

			fMuTPCIsFixed[iMassAssumption][iSpecies] = kFALSE;
			fSigmaTPCIsFixed[iMassAssumption][iSpecies] = kFALSE;

		}			
	}
}

// -----------------------------------------------------------------------
AliSettingsDiHadronPID::~AliSettingsDiHadronPID() {
	
	// Destructor
	AliLogDiHadronPID::Debug2("Destructor called.", __func__, Class_Name());

}

// -----------------------------------------------------------------------
AliSettingsDiHadronPID* AliSettingsDiHadronPID::Instance(Bool_t configMacroIsCalling) {

	// Return a ptr to the instace.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, Class_Name());

	if ((configMacroIsCalling == kFALSE) && (!fSettings)) {
		AliLogDiHadronPID::Error(Form("Please run a configuration macro before doing any analysis!!"), __func__, Class_Name());
		return 0x0;
	}

	if ((configMacroIsCalling == kTRUE) && (fSettings)) {
		AliLogDiHadronPID::Warning(Form("It seems like you have already ran a config macro."), __func__, Class_Name());		
	}

	// Eiter the settings already exist, or the config macro is calling.
	if (!fSettings) {
		fSettings = new AliSettingsDiHadronPID(configMacroIsCalling);
	}
	
	return fSettings;

}

// -----------------------------------------------------------------------
void AliSettingsDiHadronPID::Kill() {

	// Kills the current instace.
	AliLogDiHadronPID::Debug2(Form("Kill signal."), __func__, Class_Name());

	if (fSettings) {
		delete fSettings;
		fSettings = 0x0;
	}

}

// -----------------------------------------------------------------------
void AliSettingsDiHadronPID::Print(Option_t*) const {
	
	// Print info about the class.
	AliLogDiHadronPID::Debug3("Called.", __func__, Class_Name());

	cout << "-----------------------------------------------------------------------------------------------" << endl;
	cout << " DiHadronPID Analysis Settings." << endl;
	cout << "-----------------------------------------------------------------------------------------------" << endl;
	cout << endl;

	cout << " Basic Settings:" << endl;
	cout << " ------------------------------------------------------ " << endl;
	cout << " Is configured: " << (fIsConfigured ? "Yes" : "No") << endl;
	cout << " Beam Type: " << fBeamTypeNames[fBeamType] << (fIsMC ? " (MC)" : " (Data)") << endl;
	if (fIsMC) {cout << " TOF Mismatch info available (MC): " << (fTOFMismatchInfoIsAvailableMC ? "YES" : "NO") << endl;}
	cout << " Deuterons included in the analysis: " << ((fNMassAssumptions == 4) ? "Yes" : "No") << endl;
	if (fFilterMask) {
        cout << " Selected filter bit(s) for analysis: ";
        for (Int_t iFilterBit = 1; iFilterBit < 12; iFilterBit++) {
            if (fFilterMask & (1 << iFilterBit)) {cout << iFilterBit << " ";}
        }
        cout << endl;
    }
	if (fFilterMaskDCATemplates) {
        cout << " Selected filter bit(s) for DCA templates: "; 
        for (Int_t iFilterBit = 1; iFilterBit < 12; iFilterBit++) {
            if (fFilterMaskDCATemplates & (1 << iFilterBit)) {cout << iFilterBit << " ";}
        }
	    cout << endl;
    }

	cout << " Paths and file settings:" << endl;
	cout << " ------------------------------------------------------ " << endl;	
	cout << " Data Path: " << (fDataPath.IsNull() ? "Not set!" : fDataPath.Data()) << endl;
	cout << " Working Directory: " << (fWorkingDir.IsNull() ? "Not set!" : fWorkingDir.Data()) << endl;
	cout << " Pt Eta Spectra file name: " << fSpectraFileName << endl;
	if (fPtSpectraFileName) {cout << " Pt Spectra file name: " << fPtSpectraFileName << endl;}
	cout << " Template file name: " << fTemplatesFileName << endl;
	cout << " PID Correlations file name: " << fPIDCorrelationsFileName << endl;
	if (fIsMC) {cout << " Efficiencies file name: " << fEfficienciesFileName << endl;}
	if (!fIsMC) {cout << " Efficiencies directory: " << (fEfficienciesDir.IsNull() ? "Not set!" : fEfficienciesDir.Data()) << endl;}
	cout << endl;

	cout << " Basic Axes Settings:" << endl;
	cout << " ------------------------------------------------------ " << endl;	
	cout << " Centrality axis: ";
	if (fBeamType!=2) {
		if (fCentralityAxis) {AliHistToolsDiHadronPID::PrintTAxis(fCentralityAxis);}
		else {cout << "Not set!" << endl;}
	}

	cout << " VtxZ axis: ";
	if (fVtxZAxis) {AliHistToolsDiHadronPID::PrintTAxis(fVtxZAxis);}
	else {cout << "Not set!" << endl;}

	cout << " Pt axis: ";
	if (fPtAxis) {AliHistToolsDiHadronPID::PrintTAxis(fPtAxis);}
	else {cout << "Not set!" << endl;}

	cout << " Eta axis: ";
	if (fEtaAxis) {AliHistToolsDiHadronPID::PrintTAxis(fEtaAxis);}
	else {cout << "Not set!" << endl;}

	cout << " DEta axis: ";
	if (fDEtaAxis) {AliHistToolsDiHadronPID::PrintTAxis(fDEtaAxis);}
	else {cout << "Not set!" << endl;}

	cout << " Phi axis: ";
	if (fPhiAxis) {AliHistToolsDiHadronPID::PrintTAxis(fPhiAxis);}
	else {cout << "Not set!" << endl;}

	cout << " DPhi axis: ";
	if (fDPhiAxis) {AliHistToolsDiHadronPID::PrintTAxis(fDPhiAxis);}
	else {cout << "Not set!" << endl;}

	cout << " DCA axis: ";
	if (fDCAAxis) {AliHistToolsDiHadronPID::PrintTAxis(fDCAAxis);}
	else {cout << "Not set!" << endl;}
	cout << endl;

	cout << " PID Cuts (spectra and associateds):" << endl;
	cout << " ------------------------------------------------------ " << endl;
	cout << " Clusters in the TPC for dE/dx determination, min: " << fMinClustersdEdx << ", max: " << fMaxClustersdEdx << endl; 
	cout << " Perform pT dependent DCA cut (use only when filterbit does not have DCA cut): " << (fDemandTightDCACut ? "YES" : "NO") << endl;
	cout << " Demand standard TOF and TPC PID flags: " << (fDemandTOFTPCPIDFlags ? "YES" : "NO") << endl;
	cout << " Demand T0 TOF start time: " << (fDemandT0TOFStartTime ? "YES" : "NO") << endl;
	cout << " Demand rapidity cut (inclusive spectra): " << (fRapidityCutForInclusiveSpectra ? "YES" : "NO") << endl;
	cout << endl;

	cout << " Correlation Settings:" << endl;
	cout << " ------------------------------------------------------ " << endl;
	cout << " N Triggers to mix: " << fNTriggersToMix << endl;
	cout << " Triggers: " << fMinPtTrig << " < pT < " << fMaxPtTrig << endl;
	cout << " Associateds: " << fMinPtAssoc << " < pT < " << fMaxPtAssoc << endl; 
	cout << endl;

	cout << " General Fit Settings:" << endl;
	cout << " ------------------------------------------------------ " << endl;	
	cout << " Minimum TOF interval to determine mismatch: " << fMinDistToDetermineMismatch << endl;
	cout << " Distance from peak to determine mismatch: " << fDistFromPeakForMismatch << endl;
	cout << endl;
	cout << " Extra TOF fit range around the peaks, min: " << fTOFFitRangeMin << " max: " <<fTOFFitRangeMax << endl; 
	cout << " Extra TPC fit range around the peaks, min: " << fTPCFitRangeMin << " max: " <<fTPCFitRangeMax << endl; 
	cout << endl;
	cout << " Min. entries per bin for Chi2 fits: " << fMinEntriesForChi2 << endl;
	cout << " Error estimate for Chi2 fits: sqrt(N_" << (fUseSqrtModelValueForErrorChi2 ? "model" : "data") << ")" << endl;
	cout << " Use Chi2 fits below pT: " << fUseChi2BelowPt << endl; 
	cout << endl;
	cout << " Min. entries per bin for LL fits: " << fMinEntriesForLL << endl;
	cout << endl;
	cout << " Default TOF resolution: " << fStdTOFResolution << endl;
	cout << " Default TOF time shift: " << fStdTOFShift << endl;
	cout << " Default TPC resolution: " << fStdTPCResolution << endl;
	cout << endl;
	cout << " Use penalty for bad fits: " << (fUseFitPenalty ? "YES" : "NO") << endl;
	cout << endl;

	// Make a table per mass assumption.
	for (Int_t iMassAssumption = 0; iMassAssumption < fNMassAssumptions; iMassAssumption++) {
		cout << " Fit settings for the " << fSpeciesNames[iMassAssumption] << " mass assumption: " << endl;	
		cout << " ------------------------------------------------------ " << endl;	

		cout << " TOF axis: ";
		if (fTOFAxis[iMassAssumption]) {AliHistToolsDiHadronPID::PrintTAxis(fTOFAxis[iMassAssumption]);}
		else {cout << "Not set!" << endl;}
		cout << " TPC axis: ";
		if (fTPCAxis[iMassAssumption]) {AliHistToolsDiHadronPID::PrintTAxis(fTPCAxis[iMassAssumption]);}
		else {cout << "Not set!" << endl;}

		cout << endl;
                /// FIXME: shoudl also print flags for position and sigma fixing
		cout << Form("   | %14s | %14s | %14s | %14s | %14s | %14s | %14s |", "Peak species", "Peak included", "TOF tail fixed", "TOF tail value", "TPC tail used", "TPC tail fixed", "TPC tail value") << endl;
		for (Int_t iSpecies = 0; iSpecies < fNSpecies; iSpecies++) {	
			cout << Form("   | %14s", fSpeciesNames[iSpecies]);
			cout << " | " << (fPeakIsIncluded[iMassAssumption][iSpecies] ? Form("%3.1f < pT < %3.1f", fPeakIsIncludedMinPt[iMassAssumption][iSpecies], fPeakIsIncludedMaxPt[iMassAssumption][iSpecies]) : "      NO      ");
			cout << " | " << (fTOFTailIsFixed[iMassAssumption][iSpecies] ? Form("%3.1f < pT < %3.1f", fTOFTailIsFixedMinPt[iMassAssumption][iSpecies], fTOFTailIsFixedMaxPt[iMassAssumption][iSpecies]) : "      NO      ");
			cout << " | " << Form("    %1.4f    ", fTOFTailIsFixedValue[iMassAssumption][iSpecies]);
			cout << " | " << (fTPCTailIsIncluded[iMassAssumption][iSpecies] ? Form("%3.1f < pT < %3.1f", fTPCTailIsIncludedMinPt[iMassAssumption][iSpecies], fTPCTailIsIncludedMaxPt[iMassAssumption][iSpecies]) : "      NO      ");
			cout << " | " << (fTPCTailIsFixed[iMassAssumption][iSpecies] ? Form("%3.1f < pT < %3.1f", fTPCTailIsFixedMinPt[iMassAssumption][iSpecies], fTPCTailIsFixedMaxPt[iMassAssumption][iSpecies]) : "      NO      ");
			cout << " | " << Form("    %1.4f    ", fTPCTailIsFixedValue[iMassAssumption][iSpecies]);
			cout << " | " << endl;
		}	
		cout << endl;
	}

	cout << "-----------------------------------------------------------------------------------------------" << endl;

}

// -----------------------------------------------------------------------
void AliSettingsDiHadronPID::SetBeamType(Int_t beamType) {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	if (beamType > -1 && beamType < 3) {fBeamType = beamType;}

	// Set also default TOF and TPC resolutions.
	switch (fBeamType) {

		// PbPb
		case 0:
			fStdTOFResolution = 85.;
			fStdTPCResolution = 3.5;
			break;
		
		// pPb
		case 1:
			fStdTOFResolution = 85.;
			fStdTPCResolution = 3.5;
			break;

		// pp
		case 2:
			fStdTOFResolution = 110.;
			fStdTPCResolution = 3.5;
			break;		

	}

	AliLogDiHadronPID::Info(Form("Set Std. PID resolutions to: TOF: %f, TPC: %f.", fStdTOFResolution, fStdTPCResolution), __func__, ClassName());
	

}

// -----------------------------------------------------------------------
Bool_t AliSettingsDiHadronPID::IsTrackSelectedForCharge(const AliMiniTrackDiHadronPID* track) const {

	AliLogDiHadronPID::Debug4(Form("Called."), __func__, ClassName());

	// Always accept.
	if (fRequestedCharge == kAllCharged) {return kTRUE;}
	if (fRequestedCharge == kPositive && track->IsPositive()) {return kTRUE;} 
	if (fRequestedCharge == kNegative && track->IsNegative()) {return kTRUE;}

	return kFALSE;

}

// -----------------------------------------------------------------------
Bool_t AliSettingsDiHadronPID::IsTrackSelectedForTOFPID(const AliMiniTrackDiHadronPID* track) const {

    /**
     * This method should not be used for correcting analysis results, only for comparison purposes.
     * For analysis use IsTrackSelectedForPID().
     */

	// This method checks the given track against de demands for PID.
	AliLogDiHadronPID::Debug4(Form("Called."), __func__, ClassName());
	
	// Check TOF and TPC PID flags if requested.
	if (fDemandTOFTPCPIDFlags) {
		
		// For TOF PID we demand kTIME and kTOFout (corresponds to the PID status calculated by the framework).
		if (!(track->IsFlagSet(AliMiniTrackDiHadronPID::kTOFout))) {return kFALSE;}
		if (!(track->IsFlagSet(AliMiniTrackDiHadronPID::kTIME))) {return kFALSE;}
	
	}

	// Check for T0_TOF flag if requested.
	if (fDemandT0TOFStartTime) {

		// TO_TOF start time given by 1, 3, 5, 7 in starttimemask of track.
		if (!(track->GetStartTimeMask() % 2)) {return kFALSE;}

	}

	return kTRUE;

}

// -----------------------------------------------------------------------
Bool_t AliSettingsDiHadronPID::IsTrackSelectedForTPCPID(const AliMiniTrackDiHadronPID* track) const {

    /**
     * This method should not be used for correcting analysis results, only for comparison purposes.
     * For analysis use IsTrackSelectedForPID().
     */

	// This method checks the given track against de demands for PID.
	AliLogDiHadronPID::Debug4(Form("Called."), __func__, ClassName());
	
	// Check TOF and TPC PID flags if requested.
	if (fDemandTOFTPCPIDFlags) {
		

		// For TPC PID we demand kTPCout and at least one cluter in the TPC (corresponds to the PID status calculated by the framework).
		if (!(track->IsFlagSet(AliMiniTrackDiHadronPID::kTPCout))) {return kFALSE;}
		if (!(track->GetTPCSignalN())) {return kFALSE;}
	
	}
	
	// Check the number of clusters used for dE/dx determination.
	if (track->GetTPCSignalN() < fSettings->GetMinClustersdEdx()) {return kFALSE;}
	if (track->GetTPCSignalN() > fSettings->GetMaxClustersdEdx()) {return kFALSE;}

	return kTRUE;

}
// -----------------------------------------------------------------------
Bool_t AliSettingsDiHadronPID::IsTrackSelectedForPID(const AliMiniTrackDiHadronPID* track) const {

	// This method checks the given track against de demands for PID.
	AliLogDiHadronPID::Debug4(Form("Called."), __func__, ClassName());
	
	// Check TOF and TPC PID flags if requested.
	if (fDemandTOFTPCPIDFlags) {
		
		// For TOF PID we demand kTIME and kTOFout (corresponds to the PID status calculated by the framework).
		if (!(track->IsFlagSet(AliMiniTrackDiHadronPID::kTOFout))) {return kFALSE;}
		if (!(track->IsFlagSet(AliMiniTrackDiHadronPID::kTIME))) {return kFALSE;}

		// For TPC PID we demand kTPCout and at least one cluter in the TPC (corresponds to the PID status calculated by the framework).
		if (!(track->IsFlagSet(AliMiniTrackDiHadronPID::kTPCout))) {return kFALSE;}
		if (!(track->GetTPCSignalN())) {return kFALSE;}
	
	}
	
	// Check the number of clusters used for dE/dx determination.
	if (track->GetTPCSignalN() < fSettings->GetMinClustersdEdx()) {return kFALSE;}
	if (track->GetTPCSignalN() > fSettings->GetMaxClustersdEdx()) {return kFALSE;}

	// Cut on DCA if requested.
	if (fDemandTightDCACut) {

		// Only if there is an SPD hit we can trust the DCA of the track.
		if (track->HasSPDHit()) {

			// Tight cut on DCAxy.
			if (TMath::Abs(track->GetDCAxy()) > (0.0182 + 0.0350 / TMath::Power(track->Pt(), 1.01))) {return kFALSE;}

			// Standard cut on DCAz.
			if (TMath::Abs(track->GetDCAz()) > 2.) {return kFALSE;}

		}

	}

	// Check for T0_TOF flag if requested.
	if (fDemandT0TOFStartTime) {

		// TO_TOF start time given by 1, 3, 5, 7 in starttimemask of track.
		if (!(track->GetStartTimeMask() % 2)) {return kFALSE;}

	}

	return kTRUE;

}

// -----------------------------------------------------------------------
void AliSettingsDiHadronPID::SetIncludePeakInFit(Species massAssumption, Species peakSpecies, Double_t minPt, Double_t maxPt) {

	fPeakIsIncluded[massAssumption][peakSpecies] = kTRUE;
	fPeakIsIncludedMinPt[massAssumption][peakSpecies] = minPt;
	fPeakIsIncludedMaxPt[massAssumption][peakSpecies] = maxPt;

}

// -----------------------------------------------------------------------
void AliSettingsDiHadronPID::SetStdTOFTail(Species massAssumption, Species peakSpecies, Double_t value) {

	if (value > 0.) {fTOFTailIsFixedValue[massAssumption][peakSpecies] = value;}

}

// -----------------------------------------------------------------------
void AliSettingsDiHadronPID::SetFixTOFTail(Species massAssumption, Species peakSpecies, Double_t minPt, Double_t maxPt, Double_t value) {

	fTOFTailIsFixed[massAssumption][peakSpecies] = kTRUE;
	fTOFTailIsFixedMinPt[massAssumption][peakSpecies] = minPt;
	fTOFTailIsFixedMaxPt[massAssumption][peakSpecies] = maxPt;
	if (value > 0.) {fTOFTailIsFixedValue[massAssumption][peakSpecies] = value;}

}

// -----------------------------------------------------------------------
void AliSettingsDiHadronPID::SetStdTPCTail(Species massAssumption, Species peakSpecies, Double_t value) {

	if (value > 0.) {fTPCTailIsFixedValue[massAssumption][peakSpecies] = value;}

}

// -----------------------------------------------------------------------
void AliSettingsDiHadronPID::SetUseTPCTail(Species massAssumption, Species peakSpecies, Double_t minPt, Double_t maxPt) {

	fTPCTailIsIncluded[massAssumption][peakSpecies] = kTRUE;
	fTPCTailIsIncludedMinPt[massAssumption][peakSpecies] = minPt;
	fTPCTailIsIncludedMaxPt[massAssumption][peakSpecies] = maxPt;

}

// -----------------------------------------------------------------------
void AliSettingsDiHadronPID::SetFixTPCTail(Species massAssumption, Species peakSpecies, Double_t minPt, Double_t maxPt, Double_t value) {

	fTPCTailIsFixed[massAssumption][peakSpecies] = kTRUE;
	fTPCTailIsFixedMinPt[massAssumption][peakSpecies] = minPt;
	fTPCTailIsFixedMaxPt[massAssumption][peakSpecies] = maxPt;
	if (value > 0.) {fTPCTailIsFixedValue[massAssumption][peakSpecies] = value;}

}

// -----------------------------------------------------------------------
Bool_t AliSettingsDiHadronPID::IsPeakIncludedInFit(Species massAssumption, Species peakSpecies, Double_t meanPt) const {

	return (fPeakIsIncluded[massAssumption][peakSpecies] && (fPeakIsIncludedMinPt[massAssumption][peakSpecies] < meanPt) && (fPeakIsIncludedMaxPt[massAssumption][peakSpecies] > meanPt));

}

// -----------------------------------------------------------------------
Double_t AliSettingsDiHadronPID::GetStdTOFTailValue(Species massAssumption, Species peakSpecies) const {

	return fTOFTailIsFixedValue[massAssumption][peakSpecies];

}

// -----------------------------------------------------------------------
Bool_t AliSettingsDiHadronPID::IsTOFTailFixed(Species massAssumption, Species peakSpecies, Double_t meanPt) const {

	return (fTOFTailIsFixed[massAssumption][peakSpecies] && (fTOFTailIsFixedMinPt[massAssumption][peakSpecies] < meanPt) && (fTOFTailIsFixedMaxPt[massAssumption][peakSpecies] > meanPt));

}

// -----------------------------------------------------------------------
Double_t AliSettingsDiHadronPID::GetStdTPCTailValue(Species massAssumption, Species peakSpecies) const {

	return fTPCTailIsFixedValue[massAssumption][peakSpecies];

}

// -----------------------------------------------------------------------
Bool_t AliSettingsDiHadronPID::IsTPCTailIncluded(Species massAssumption, Species peakSpecies, Double_t meanPt) const {

	return (fTPCTailIsIncluded[massAssumption][peakSpecies] && (fTPCTailIsIncludedMinPt[massAssumption][peakSpecies] < meanPt) && (fTPCTailIsIncludedMaxPt[massAssumption][peakSpecies] > meanPt));

}

// -----------------------------------------------------------------------
Bool_t AliSettingsDiHadronPID::IsTPCTailFixed(Species massAssumption, Species peakSpecies, Double_t meanPt) const {

	return (fTPCTailIsFixed[massAssumption][peakSpecies] && (fTPCTailIsFixedMinPt[massAssumption][peakSpecies] < meanPt) && (fTPCTailIsFixedMaxPt[massAssumption][peakSpecies] > meanPt));

}
/*
// -----------------------------------------------------------------------
Int_t AliSettingsDiHadronPID::GetFitRegionTOFminBin(Species massAssumption, Double_t avgPt, Double_t absAvgEta) {

	// Get the TOF peak that has the minimum TOF value, out of all peaks included in the fit.
	Double_t TOFmin = 0.;

	for (Int_t iSpecies = 0; iSpecies < fNSpecies; iSpecies++) {
		if (IsPeakIncludedInFit(massAssumption, (Species)iSpecies, avgPt)) {
			TOFmin = TMath::Min(TOFmin, AliFunctionsDiHadronPID::EstimateTOFPosition(avgPt, absAvgEta, massAssumption, iSpecies));
		}
	}

	TOFmin -= fTOFFitRangeMin;

	return (fTOFAxis[massAssumption]->FindBin(TOFmin));

}

// -----------------------------------------------------------------------
Int_t AliSettingsDiHadronPID::GetFitRegionTOFmaxBin(Species massAssumption, Double_t avgPt, Double_t absAvgEta) {

	// Get the TOF peak that has the maximum TOF value, out of all peaks included in the fit.
	Double_t TOFmax = 0.;

	for (Int_t iSpecies = 0; iSpecies < fNSpecies; iSpecies++) {
		if (IsPeakIncludedInFit(massAssumption, (Species)iSpecies, avgPt)) {
			TOFmax = TMath::Max(TOFmax, AliFunctionsDiHadronPID::EstimateTOFPosition(avgPt, absAvgEta, massAssumption, iSpecies));
		}
	}

	TOFmax += fTOFFitRangeMax;

	return (fTOFAxis[massAssumption]->FindBin(TOFmax));

}

// -----------------------------------------------------------------------
Int_t AliSettingsDiHadronPID::GetFitRegionTPCminBin(Species massAssumption, Double_t avgPt, Double_t absAvgEta) {

	// Get the TPC peak that has the minimum TPC value, out of all peaks included in the fit.
	Double_t TPCmin = 0.;

	for (Int_t iSpecies = 0; iSpecies < fNSpecies; iSpecies++) {
		if (IsPeakIncludedInFit(massAssumption, (Species)iSpecies, avgPt)) {
			TPCmin = TMath::Min(TPCmin, AliFunctionsDiHadronPID::EstimateTPCPosition(avgPt, absAvgEta, massAssumption, iSpecies));
		}
	}

	TPCmin -= fTPCFitRangeMin;

	return (fTPCAxis[massAssumption]->FindBin(TPCmin));

}

// -----------------------------------------------------------------------
Int_t AliSettingsDiHadronPID::GetFitRegionTPCmaxBin(Species massAssumption, Double_t avgPt, Double_t absAvgEta) {

	// Get the TPC peak that has the minimum TPC value, out of all peaks included in the fit.
	Double_t TPCmax = 0.;

	for (Int_t iSpecies = 0; iSpecies < fNSpecies; iSpecies++) {
		if (IsPeakIncludedInFit(massAssumption, (Species)iSpecies, avgPt)) {
			TPCmax = TMath::Max(TPCmax, AliFunctionsDiHadronPID::EstimateTPCPosition(avgPt, absAvgEta, massAssumption, iSpecies));
		}
	}

	TPCmax += fTPCFitRangeMax;

	return (fTPCAxis[massAssumption]->FindBin(TPCmax));

}
*/
// -----------------------------------------------------------------------
Int_t AliSettingsDiHadronPID::GetMismatchRegionTOFMinOrMaxBin(Species massAssumption, Double_t avgPt, Double_t absAvgEta, Bool_t returnMin) {

	// Returns the minimum or maximum TOF bin where the mismatch is determined.
	// The calculation is based on:
	//  - The settings fMinDistToDetermineMismatch, and fDistFromPeakForMismatch
	//  - The (expected) peak positions for this pTbin (including the ones not in the fit).
	//  - The Size of the TOF axis.
	//
	// The method tries to calculate mismatches as close to the pion peak as possible,
	// to get maximal statistics.
	//
	// Note that for this method it is crucial that the TOF axis of the data histogram you're working
	// with is the same as the TOF axis in the settings instance.
	AliLogDiHadronPID::Debug3(Form("Called with m.a.: %i, avgPt: %f and avgAbsEta: %f, returning %s bin", massAssumption, avgPt, absAvgEta, (returnMin ? "minimum" : "maximum")), __func__, ClassName());
	
	if (!(fTOFAxis[massAssumption])) {
		AliLogDiHadronPID::Error(Form("No TOF axis known for mass assumption %i", massAssumption), __func__, ClassName());
		return -999;
	}

	Int_t minBin = -999;
	Int_t maxBin = -999;

	// Determine the expected TOF positions of the four main peaks (pi/ka/pr/de)
	Double_t expTOFpos[4] = {0.};
	Bool_t peakIsAvailable[4] = {0};
	for (Int_t iSpecies = 0; iSpecies < 4; iSpecies++) {
		expTOFpos[iSpecies] = AliFunctionsDiHadronPID::EstimateTOFPosition(avgPt, absAvgEta, massAssumption, iSpecies);	
		peakIsAvailable[iSpecies] = ( ((expTOFpos[iSpecies] > fTOFAxis[massAssumption]->GetXmin()) && 
			(expTOFpos[iSpecies] < fTOFAxis[massAssumption]->GetXmax())) ? kTRUE : kFALSE);
		// //AliLogDiHadronPID::Debug1(Form("Peak %i is %savailable", iSpecies, ((peakIsAvailable[iSpecies])? "" : "not ")), __func__, ClassName());
		// cout << "Expected pos: " << expTOFpos[iSpecies] << endl;
	}

	Double_t minDist = (2. * fDistFromPeakForMismatch + fMinDistToDetermineMismatch);

	// -- PION --

	// Check if the kaon peak is available, if so, determine mismatches between pion and kaon peaks.
	if (peakIsAvailable[0] && peakIsAvailable[1] && ((expTOFpos[1] - expTOFpos[0]) > minDist)) {
		minBin = fTOFAxis[massAssumption]->FindBin(expTOFpos[0] + fDistFromPeakForMismatch);
		maxBin = fTOFAxis[massAssumption]->FindBin(expTOFpos[1] - fDistFromPeakForMismatch);
		//AliLogDiHadronPID::Debug1(Form("Mismatch determined between pion and kaon peak."), __func__, ClassName());
	}

	// Else, if there's enough space between the pion peak and the right edge of the histogram, then calculate the mismatches there.
	else if (peakIsAvailable[0] && (!(peakIsAvailable[1])) && (expTOFpos[1] - expTOFpos[0]) > minDist) {
		minBin = fTOFAxis[massAssumption]->FindBin(expTOFpos[0] + fDistFromPeakForMismatch);

		// Include as much data as possible, by looking how far beyond the edge the kaon peak is.
		Double_t subtractedDistanceFromEdge = TMath::Max(0., fDistFromPeakForMismatch - (expTOFpos[1] - fTOFAxis[massAssumption]->GetXmax()));

		maxBin = fTOFAxis[massAssumption]->FindBin(fTOFAxis[massAssumption]->GetXmax() - subtractedDistanceFromEdge);	
		//AliLogDiHadronPID::Debug1(Form("Mismatch determined between pion peak, and the right of the histogram."), __func__, ClassName());
	}

	// -- KAON --

	// Check if the proton peak is available, if so, determine the mismatches between kaon and protons.
	else if (peakIsAvailable[1] && peakIsAvailable[2] && ((expTOFpos[2] - expTOFpos[1]) > minDist)) {
		minBin = fTOFAxis[massAssumption]->FindBin(expTOFpos[1] + fDistFromPeakForMismatch);
		maxBin = fTOFAxis[massAssumption]->FindBin(expTOFpos[2] - fDistFromPeakForMismatch);				
		//AliLogDiHadronPID::Debug1(Form("Mismatch determined between kaon and proton peak."), __func__, ClassName());
	}

	// Else, if there's enough space between the kaon peak and the left edge of the histogram, then calculate the mismatches there.
	else if (peakIsAvailable[1] && (!(peakIsAvailable[0])) && (expTOFpos[1] - expTOFpos[0]) > minDist) {

		// Include as much data as possible, by looking how far beyond the edge the pion peak is.
		Double_t addedDistanceToEdge = TMath::Max(0., fDistFromPeakForMismatch - TMath::Abs((fTOFAxis[massAssumption]->GetXmin() - expTOFpos[0])));

		minBin = fTOFAxis[massAssumption]->FindBin(fTOFAxis[massAssumption]->GetXmin() + addedDistanceToEdge);
		maxBin = fTOFAxis[massAssumption]->FindBin(expTOFpos[1] - fDistFromPeakForMismatch);	
		//AliLogDiHadronPID::Debug1(Form("Mismatch determined between kaon peak, and the left of the histogram."), __func__, ClassName());
	}

	// Else, if there's enough space between the kaon peak and the right edge of the histogram, then calculate the mistmaches there.
	else if (peakIsAvailable[1] && (!(peakIsAvailable[2])) && (expTOFpos[2] - expTOFpos[1]) > minDist) {
		minBin = fTOFAxis[massAssumption]->FindBin(expTOFpos[1] + fDistFromPeakForMismatch);

		Double_t subtractedDistanceFromEdge = TMath::Max(0., fDistFromPeakForMismatch - TMath::Abs((expTOFpos[2] - fTOFAxis[massAssumption]->GetXmax())));
		maxBin = fTOFAxis[massAssumption]->FindBin(fTOFAxis[massAssumption]->GetXmax() - subtractedDistanceFromEdge);	
		//AliLogDiHadronPID::Debug1(Form("Mismatch determined between kaon peak, and the right of the histogram."), __func__, ClassName());
	}

	// -- PROTON --

	// Check if the deuteron peak is available, if so, determine the mismatches between kaon and protons.
	else if (peakIsAvailable[2] && peakIsAvailable[3] && ((expTOFpos[3] - expTOFpos[2]) > minDist)) {
		minBin = fTOFAxis[massAssumption]->FindBin(expTOFpos[2] + fDistFromPeakForMismatch);
		maxBin = fTOFAxis[massAssumption]->FindBin(expTOFpos[3] - fDistFromPeakForMismatch);				
		//AliLogDiHadronPID::Debug1(Form("Mismatch determined between proton and deuteron peak."), __func__, ClassName());
	}

	// Else, if there's enough space between the kaon peak and the left edge of the histogram, then calculate the mismatches there.
	else if (peakIsAvailable[2] && (!(peakIsAvailable[1])) && (expTOFpos[2] - expTOFpos[1]) > minDist) {

		Double_t addedDistanceToEdge = TMath::Max(0., fDistFromPeakForMismatch - TMath::Abs((fTOFAxis[massAssumption]->GetXmin() - expTOFpos[1])));
		minBin = fTOFAxis[massAssumption]->FindBin(fTOFAxis[massAssumption]->GetXmin() + addedDistanceToEdge);
		maxBin = fTOFAxis[massAssumption]->FindBin(expTOFpos[2] - fDistFromPeakForMismatch);				
		//AliLogDiHadronPID::Debug1(Form("Mismatch determined between proton peak, and the left of the histogram."), __func__, ClassName());
	}

	// Else, if there's enough space between the kaon peak and the right edge of the histogram, then calculate the mistmaches there.
	else if (peakIsAvailable[2] && (!(peakIsAvailable[3])) && (expTOFpos[3] - expTOFpos[2]) > minDist) {
		minBin = fTOFAxis[massAssumption]->FindBin(expTOFpos[2] + fDistFromPeakForMismatch);

		Double_t subtractedDistanceFromEdge = TMath::Max(0., fDistFromPeakForMismatch - TMath::Abs(expTOFpos[3] - fTOFAxis[massAssumption]->GetXmax()));		
		maxBin = fTOFAxis[massAssumption]->FindBin(fTOFAxis[massAssumption]->GetXmax() - subtractedDistanceFromEdge);
		//AliLogDiHadronPID::Debug1(Form("Mismatch determined between proton peak, and the right of the histogram."), __func__, ClassName());							
	}

	// -- DEUTERON --	

	// If there's enough space between the deuteron peak and the left edge of the histogram, then calculate the mismatches there.
	else if (peakIsAvailable[3] && (expTOFpos[3] - expTOFpos[2]) > minDist) {

		Double_t addedDistanceToEdge = TMath::Max(0., fDistFromPeakForMismatch - TMath::Abs((fTOFAxis[massAssumption]->GetXmin() - expTOFpos[2])));
		minBin = fTOFAxis[massAssumption]->FindBin(fTOFAxis[massAssumption]->GetXmin() + addedDistanceToEdge);
		maxBin = fTOFAxis[massAssumption]->FindBin(expTOFpos[3] - fDistFromPeakForMismatch);				
		//AliLogDiHadronPID::Debug1(Form("Mismatch determined between deuteron peak, and the left of the histogram."), __func__, ClassName());
	}

	// Else, if there's enough space between the deuteron peak and the right edge of the histogram, then calculate the mistmaches there.
	else if (peakIsAvailable[3] && (fTOFAxis[massAssumption]->GetXmax() - expTOFpos[3]) > (minDist - fDistFromPeakForMismatch)) {
		minBin = fTOFAxis[massAssumption]->FindBin(expTOFpos[3] + fDistFromPeakForMismatch);
		maxBin = fTOFAxis[massAssumption]->FindBin(fTOFAxis[massAssumption]->GetXmax());	// No need to consider peaks beyond deuterons.
		//AliLogDiHadronPID::Debug1(Form("Mismatch determined between deuteron peak, and the right of the histogram."), __func__, ClassName());									
	}

	// Throw an exception.
	else {
		AliLogDiHadronPID::Error(Form("Caught an exception! No place found where TOF mismatches can be determined."), __func__, ClassName());
		// cout << "Expected: deuteron place: " << expTOFpos[3] << endl;
		// cout << (fTOFAxis[massAssumption]->GetXmax() - expTOFpos[3]) << " dist. req.: " << (minDist - fDistFromPeakForMismatch) << endl;
	}

	if (returnMin) {return minBin;}
	else {
		if (maxBin > fTOFAxis[massAssumption]->GetNbins()) {
			//AliLogDiHadronPID::Error(Form("Maximum bin exceeds the number of bins..."), __func__, ClassName());
			return (fTOFAxis[massAssumption]->GetNbins());
		}
		else {return maxBin;}
	}

}

Double_t AliSettingsDiHadronPID::GetMuTOFTolerance(Double_t pt) const 
{
  return fMuTOFTolerance*(AliFunctionsDiHadronPID::TOFExpTime(pt, 0., AliFunctionsDiHadronPID::Mproton())-AliFunctionsDiHadronPID::TOFExpTime(pt, 0, AliFunctionsDiHadronPID::Mkaon()));
}

// -----------------------------------------------------------------------
//  Centrality Axis
// -----------------------------------------------------------------------
void AliSettingsDiHadronPID::SetCentralityAxis(Int_t nBins, const Double_t* axis) {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, Class_Name());
	
	if (fCentralityAxis) {
		AliLogDiHadronPID::Warning(Form("Deleting existing Centrality axis!"), __func__, Class_Name());
		delete fCentralityAxis;
		fCentralityAxis = 0x0;
	}

	fCentralityAxis = new TAxis(nBins, axis);
	fCentralityAxis->SetTitle("centrality");

}

// -----------------------------------------------------------------------
const TAxis* AliSettingsDiHadronPID::GetCentralityAxis() const {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, Class_Name());
	
	if (!fCentralityAxis) {AliLogDiHadronPID::Error(Form("No Centrality axis set."), __func__, Class_Name());}

	return fCentralityAxis;

}

// -----------------------------------------------------------------------
//  Pt Axis
// -----------------------------------------------------------------------
void AliSettingsDiHadronPID::SetPtAxis(Int_t nBins, const Double_t* axis) {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, Class_Name());
	
	if (fPtAxis) {
		AliLogDiHadronPID::Warning(Form("Deleting existing pT axis!"), __func__, Class_Name());
		delete fPtAxis;
		fPtAxis = 0x0;
	}

	fPtAxis = new TAxis(nBins, axis);
	fPtAxis->SetTitle("p_{T} (GeV/c)");

	//ResetFitStrategyHistogram();

}

// -----------------------------------------------------------------------
const TAxis* AliSettingsDiHadronPID::GetPtAxis() const {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, Class_Name());
	
	if (!fPtAxis) {AliLogDiHadronPID::Error(Form("No pT axis set."), __func__, Class_Name());}

	return fPtAxis;

}

// -----------------------------------------------------------------------
//  VtxZ Axis.
// -----------------------------------------------------------------------
void AliSettingsDiHadronPID::SetVtxZAxis(Int_t nBins, Double_t min, Double_t max) {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, Class_Name());
	
	if (fVtxZAxis) {
		AliLogDiHadronPID::Warning(Form("Deleting existing VtxZ axis!"), __func__, Class_Name());
		delete fVtxZAxis;
		fVtxZAxis = 0x0;
	}

	fVtxZAxis = new TAxis(nBins, min, max);
	fVtxZAxis->SetTitle("vtxZ (cm)");

}

// -----------------------------------------------------------------------
const TAxis* AliSettingsDiHadronPID::GetVtxZAxis() const {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, Class_Name());
	
	if (!fVtxZAxis) {AliLogDiHadronPID::Error(Form("No VtxZ axis set."), __func__, Class_Name());}

	return fVtxZAxis;

}

// -----------------------------------------------------------------------
//  Eta Axis.
// -----------------------------------------------------------------------
void AliSettingsDiHadronPID::SetEtaAxis(Int_t nBins, Double_t min, Double_t max) {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, Class_Name());
	
	if (fEtaAxis) {
		AliLogDiHadronPID::Warning(Form("Deleting existing Eta axis!"), __func__, Class_Name());
		delete fEtaAxis;
		fEtaAxis = 0x0;
	}

	fEtaAxis = new TAxis(nBins, min, max);
	fEtaAxis->SetTitle("#eta");

}

// -----------------------------------------------------------------------
const TAxis* AliSettingsDiHadronPID::GetEtaAxis() const {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, Class_Name());
	
	if (!fEtaAxis) {AliLogDiHadronPID::Error(Form("No Eta axis set."), __func__, Class_Name());}

	return fEtaAxis;

}

// -----------------------------------------------------------------------
//  DEta Axis
// -----------------------------------------------------------------------
void AliSettingsDiHadronPID::SetDEtaAxis(Int_t nBins, Double_t min, Double_t max) {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, Class_Name());
	
	if (fDEtaAxis) {
		AliLogDiHadronPID::Warning(Form("Deleting existing DEta axis!"), __func__, Class_Name());
		delete fDEtaAxis;
		fDEtaAxis = 0x0;
	}

	fDEtaAxis = new TAxis(nBins, min, max);
	fDEtaAxis->SetTitle("#Delta#eta");

}

// -----------------------------------------------------------------------
const TAxis* AliSettingsDiHadronPID::GetDEtaAxis() const {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, Class_Name());
	
	if (!fDEtaAxis) {AliLogDiHadronPID::Error(Form("No DEta axis set."), __func__, Class_Name());}

	return fDEtaAxis;

}

// -----------------------------------------------------------------------
//  Phi Axis
// -----------------------------------------------------------------------
void AliSettingsDiHadronPID::SetPhiAxis(Int_t nBins, Double_t min, Double_t max) {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, Class_Name());
	
	if (fPhiAxis) {
		AliLogDiHadronPID::Warning(Form("Deleting existing Phi axis!"), __func__, Class_Name());
		delete fPhiAxis;
		fPhiAxis = 0x0;
	}

	fPhiAxis = new TAxis(nBins, min, max);
	fPhiAxis->SetTitle("#phi (rad)");

}

// -----------------------------------------------------------------------
const TAxis* AliSettingsDiHadronPID::GetPhiAxis() const {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, Class_Name());
	
	if (!fPhiAxis) {AliLogDiHadronPID::Error(Form("No Phi axis set."), __func__, Class_Name());}

	return fPhiAxis;

}

// -----------------------------------------------------------------------
//  DPhi Axis
// -----------------------------------------------------------------------
void AliSettingsDiHadronPID::SetDPhiAxis(Int_t nBins, Double_t min, Double_t max) {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, Class_Name());
	
	if (fDPhiAxis) {
		AliLogDiHadronPID::Warning(Form("Deleting existing DPhi axis!"), __func__, Class_Name());
		delete fDPhiAxis;
		fDPhiAxis = 0x0;
	}

	fDPhiAxis = new TAxis(nBins, min, max);
	fDPhiAxis->SetTitle("#Delta#phi (rad)");

}

// -----------------------------------------------------------------------
const TAxis* AliSettingsDiHadronPID::GetDPhiAxis() const {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, Class_Name());
	
	if (!fDPhiAxis) {AliLogDiHadronPID::Error(Form("No DPhi axis set."), __func__, Class_Name());}

	return fDPhiAxis;

}

// -----------------------------------------------------------------------
//  DCA Axis
// -----------------------------------------------------------------------
void AliSettingsDiHadronPID::SetDCAAxis(Int_t nBins, const Double_t* axis) {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, Class_Name());
	
	if (fDCAAxis) {
		AliLogDiHadronPID::Warning(Form("Deleting existing DCA axis!"), __func__, Class_Name());
		delete fDCAAxis;
		fDCAAxis = 0x0;
	}

	fDCAAxis = new TAxis(nBins, axis);
	fDCAAxis->SetTitle("DCA_{xy} (cm)");

	//ResetFitStrategyHistogram();

}

// -----------------------------------------------------------------------
void AliSettingsDiHadronPID::SetDCAAxis(Int_t nBins, Double_t min, Double_t max) {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, Class_Name());
	
	if (fDCAAxis) {
		AliLogDiHadronPID::Warning(Form("Deleting existing DCA axis!"), __func__, Class_Name());
		delete fDCAAxis;
		fDCAAxis = 0x0;
	}

	fDCAAxis = new TAxis(nBins, min, max);
	fDCAAxis->SetTitle("DCA_{xy} (cm)");

}

// -----------------------------------------------------------------------
const TAxis* AliSettingsDiHadronPID::GetDCAAxis() const {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, Class_Name());
	
	if (!fDCAAxis) {AliLogDiHadronPID::Error(Form("No DCA axis set."), __func__, Class_Name());}

	return fDCAAxis;

}

// -----------------------------------------------------------------------
//  TOF Axis
// -----------------------------------------------------------------------
void AliSettingsDiHadronPID::SetTOFAxis(Int_t nBins, Double_t min, Double_t max, Species massAssumption) {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, Class_Name());
	if (massAssumption < 0 || massAssumption > 3) {
		AliLogDiHadronPID::Error(Form("Not a valid mass assumption."), __func__, ClassName());
		return;
	}

	if (fTOFAxis[massAssumption]) {
		AliLogDiHadronPID::Warning(Form("Deleting existing %s TOF axis!", fSpeciesNames[massAssumption]), __func__, Class_Name());
		delete fTOFAxis[massAssumption];
		fTOFAxis[massAssumption] = 0x0;
	}

	fTOFAxis[massAssumption] = new TAxis(nBins, min, max);
	fTOFAxis[massAssumption]->SetTitle(Form("#Delta t - <#Delta t>_{%s} (ps)", fSpeciesLaTeX[massAssumption]));

}

// -----------------------------------------------------------------------
const TAxis* AliSettingsDiHadronPID::GetTOFAxis(Species massAssumption) const {

	AliLogDiHadronPID::Debug3(Form("Called for species %s.", fSpeciesNames[massAssumption]), __func__, Class_Name());
	if (massAssumption < 0 || massAssumption > 3) {
		AliLogDiHadronPID::Error(Form("Not a valid mass assumption."), __func__, ClassName());
		return 0x0;
	}

	if (!fTOFAxis[massAssumption]) {AliLogDiHadronPID::Error(Form("No TOF axis set for %s.", fSpeciesNames[massAssumption]), __func__, Class_Name());}

	return fTOFAxis[massAssumption];

}

// -----------------------------------------------------------------------
//  TPC Axis
// -----------------------------------------------------------------------
void AliSettingsDiHadronPID::SetTPCAxis(Int_t nBins, Double_t min, Double_t max, Species massAssumption) {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, Class_Name());
	if (massAssumption < 0 || massAssumption > 3) {
		AliLogDiHadronPID::Error(Form("Not a valid mass assumption."), __func__, ClassName());
		return;
	}

	if (fTPCAxis[massAssumption]) {
		AliLogDiHadronPID::Warning(Form("Deleting existing %s TPC axis!", fSpeciesNames[massAssumption]), __func__, Class_Name());
		delete fTPCAxis[massAssumption];
		fTPCAxis[massAssumption] = 0x0;
	}

	fTPCAxis[massAssumption] = new TAxis(nBins, min, max);
        if (fUseDEDxRatios)
  	  fTPCAxis[massAssumption]->SetTitle(Form("dE/dx/<dE/dx>_{%s}", fSpeciesLaTeX[massAssumption]));
        else
  	  fTPCAxis[massAssumption]->SetTitle(Form("dE/dx - <dE/dx>_{%s} (a.u.)", fSpeciesLaTeX[massAssumption]));

}

// -----------------------------------------------------------------------
const TAxis* AliSettingsDiHadronPID::GetTPCAxis(Species massAssumption) const {

	AliLogDiHadronPID::Debug3(Form("Called for species %s.", fSpeciesNames[massAssumption]), __func__, Class_Name());
	if (massAssumption < 0 || massAssumption > 3) {
		AliLogDiHadronPID::Error(Form("Not a valid mass assumption."), __func__, ClassName());
		return 0x0;
	}

	if (!fTPCAxis[massAssumption]) {AliLogDiHadronPID::Error(Form("No TPC axis set for %s.", fSpeciesNames[massAssumption]), __func__, Class_Name());}

	return fTPCAxis[massAssumption];

}
