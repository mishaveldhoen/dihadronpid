/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  This class contains an array of fit results, one for each fit stage.
//  It also serves as a container for the data and mismatch histograms.
//  Furthermore, some visualisation methods are implemented, and chi^2
//  and the log likelihood can be calculated.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include <iostream>
#include "TMath.h"
#include "Math/Util.h"
#include "TBox.h"
#include "TMarker.h"
#include "TEllipse.h"
#include "TLegend.h"
#include "TLatex.h"
#include "AliTOFTPCFitContainerDiHadronPID.h"
#include "AliTOFTPCFitFunctionDiHadronPID.h"
#include "AliTOFTPCPeakDiHadronPID.h"
#include "AliMiscToolsDiHadronPID.h"
#include "AliHistToolsDiHadronPID.h"
#include "AliFunctionsDiHadronPID.h"
#include "AliLogDiHadronPID.h"

using namespace std;

ClassImp(AliTOFTPCFitContainerDiHadronPID);

// -----------------------------------------------------------------------
AliTOFTPCFitContainerDiHadronPID::AliTOFTPCFitContainerDiHadronPID():
	TNamed(),
	fSettings(AliSettingsDiHadronPID::Instance()),	
	fFitFunctions(0x0),
	fCurrentFitFunction(-1),
	fData(0x0),
	fMismatch(0x0),
	fMassAssumption(AliSettingsDiHadronPID::kUnknownSpecies),
	fPtMin(-999.),
	fPtMax(-999.),
	fEtaMin(-999.),
	fEtaMax(-999.),
	fMakeLogPlots(kFALSE)

{

	// Default Constructor
	AliLogDiHadronPID::Debug2("Default constructor called.", __func__, ClassName());
	
	// Error message when settings singleton is not found!
	if (!fSettings) {AliLogDiHadronPID::Error(Form("Settings Singleton not found!"), __func__, ClassName());}

}

// -----------------------------------------------------------------------
AliTOFTPCFitContainerDiHadronPID::AliTOFTPCFitContainerDiHadronPID(const char* name, const char* title, const TH2F* dataHist, const TH2F* mismatchHist, Double_t ptMin, Double_t ptMax, Double_t etaMin, Double_t etaMax, AliSettingsDiHadronPID::Species massAssumption):
	TNamed(name, title),
	fSettings(AliSettingsDiHadronPID::Instance()),
	fFitFunctions(0x0),
	fCurrentFitFunction(-1),
	fData(new TH2F(*dataHist)),
	fMismatch(new TH2F(*mismatchHist)),
	fMassAssumption(massAssumption),
	fPtMin(ptMin),
	fPtMax(ptMax),
	fEtaMin(etaMin),
	fEtaMax(etaMax),
	fMakeLogPlots(kFALSE)

{

	AliLogDiHadronPID::Debug2(Form("Constructor called with: %3.1f < p_T < %3.1f, %3.1f < eta < %3.1f, massAssumption: %i", ptMin, ptMax, etaMin, etaMax, massAssumption), __func__, ClassName());

	// Error message when settings singleton is not found!
	if (!fSettings) {AliLogDiHadronPID::Error(Form("Settings Singleton not found!"), __func__, ClassName());}

	// Create a TObjArray to hold the fit functions. Note: this no longer creates a first fit stage!
	ResetFitStages();

}

// -----------------------------------------------------------------------
AliTOFTPCFitContainerDiHadronPID::~AliTOFTPCFitContainerDiHadronPID() {

	AliLogDiHadronPID::Debug2("Destructor called.", __func__, ClassName());
	
	// Destructor
	if (fFitFunctions) {delete fFitFunctions; fFitFunctions = 0x0;}
	if (fData) {delete fData; fData = 0x0;}
	if (fMismatch) {delete fMismatch; fMismatch = 0x0;}

}

// -----------------------------------------------------------------------
double AliTOFTPCFitContainerDiHadronPID::eval(Int_t fitStage, double* xy) const {

	// Return the function value at xy given the current fit stage.

	// Get the value of the three Gaussians.
  AliTOFTPCFitFunctionDiHadronPID* selectedFunc = 0;
	if (fitStage < 0) 
	  selectedFunc = (AliTOFTPCFitFunctionDiHadronPID*)fFitFunctions->Last();
	else
	  selectedFunc = (AliTOFTPCFitFunctionDiHadronPID*)fFitFunctions->At(fitStage);
	double val = (*selectedFunc)(xy);

	// Add the value of the mismatches.
	TAxis* tofAxis = fData->GetXaxis();
	TAxis* tpcAxis = fData->GetYaxis();

	Double_t mmval = fMismatch->GetBinContent(tofAxis->FindBin(xy[0]), tpcAxis->FindBin(xy[1]));
	mmval *= (selectedFunc->GetMismatchYield() / fMismatch->GetEntries());

	val += mmval;

	return val;

}

// -----------------------------------------------------------------------
double AliTOFTPCFitContainerDiHadronPID::chi2(Int_t fitStage) const {

	// NOTE: This method does not yet use Integral() and Norm() functions.

	// Return the chi squared, given the supplied parameters.
	Double_t chisq = 0.;
	TAxis* tofAxis = fData->GetXaxis();
	TAxis* tpcAxis = fData->GetYaxis();
	double xy[2] = {0., 0.};
	Int_t minEntriesForChi2 = fSettings->GetMinEntriesForChi2();
	Bool_t useSqrtModelForError = fSettings->GetUseSqrtModelValueForErrorChi2();

	// Determine the integration range.
	AliTOFTPCFitFunctionDiHadronPID* selectedFunc = 0;
	if (fitStage < 0) 
	  selectedFunc = (AliTOFTPCFitFunctionDiHadronPID*)fFitFunctions->Last();
	else
	  selectedFunc = (AliTOFTPCFitFunctionDiHadronPID*)fFitFunctions->At(fitStage);
	Int_t tofMinBin = tofAxis->FindBin(selectedFunc->GetFitRegionTOFMin());
	Int_t tofMaxBin = tofAxis->FindBin(selectedFunc->GetFitRegionTOFMax());
	Int_t tpcMinBin = tpcAxis->FindBin(selectedFunc->GetFitRegionTPCMin());
	Int_t tpcMaxBin = tpcAxis->FindBin(selectedFunc->GetFitRegionTPCMax());

	// Loop over all bins with more than a certain number of entries (fMinEntriesForChi2).
	for (Int_t iTOFBin = tofMinBin; iTOFBin <= tofMaxBin; iTOFBin++) {
		for (Int_t iTPCBin = tpcMinBin; iTPCBin <= tpcMaxBin; iTPCBin++) {
			
			Double_t dataVal = fData->GetBinContent(iTOFBin, iTPCBin);

			if (dataVal < minEntriesForChi2) {continue;}

			xy[0] = tofAxis->GetBinCenter(iTOFBin);
			xy[1] = tpcAxis->GetBinCenter(iTPCBin);
			Double_t modelVal = eval(fitStage, xy);

			Double_t error = (useSqrtModelForError ? TMath::Sqrt(modelVal) : fData->GetBinError(iTOFBin, iTPCBin));

			Double_t chi = (dataVal - modelVal) / error;

			chisq += (chi * chi);
		
		}
	}

	return (fSettings->IsFitPenaltyUsed() ? (chisq * CalculatePenaltyFactor(fitStage)) : chisq);

}

// -----------------------------------------------------------------------
double AliTOFTPCFitContainerDiHadronPID::negll(Int_t fitStage) const {

	// Return the negative extended log likelihood, given the supplied parameters.
	// NEW: This method is now improved in the following ways:
	//  - Integral of the fit function is taken instead of the riemann sum.
	//  - Norm is only calculated once, before looping over the bin.
	//  - FIXME: The container now has to know about the details of the fit funcion...

	Double_t nll = 0.;
	TAxis* tofAxis = fData->GetXaxis();
	TAxis* tpcAxis = fData->GetYaxis();
	//double xy[2] = {0., 0.};
	Int_t minEntriesForLL = fSettings->GetMinEntriesForLL();

	// Determine the integration range.
	AliTOFTPCFitFunctionDiHadronPID* selectedFunc = 0;
	if (fitStage < 0) 
	  selectedFunc = (AliTOFTPCFitFunctionDiHadronPID*)fFitFunctions->Last();
	else
	  selectedFunc = (AliTOFTPCFitFunctionDiHadronPID*)fFitFunctions->At(fitStage);
	Int_t tofMinBin = tofAxis->FindBin(selectedFunc->GetFitRegionTOFMin());
	Int_t tofMaxBin = tofAxis->FindBin(selectedFunc->GetFitRegionTOFMax());
	Int_t tpcMinBin = tpcAxis->FindBin(selectedFunc->GetFitRegionTPCMin());
	Int_t tpcMaxBin = tpcAxis->FindBin(selectedFunc->GetFitRegionTPCMax());
        if (tofMinBin < 1) tofMinBin = 1;
        if (tpcMinBin < 1) tpcMinBin = 1;
        if (tofMaxBin > tofAxis->GetNbins()) tofMaxBin = tofAxis->GetNbins();
        if (tpcMaxBin > tpcAxis->GetNbins()) tpcMaxBin = tpcAxis->GetNbins();
         

	// NEW: Determine norms of the included peak functions.
	Double_t norms[AliSettingsDiHadronPID::GetNSpecies()];
	
	for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {
		norms[iSpecies] = 0.;
		AliTOFTPCPeakDiHadronPID* currentPeak = (AliTOFTPCPeakDiHadronPID*)selectedFunc->GetPeak(iSpecies);
		
		if (currentPeak) {
			if (currentPeak->IsUsedInFit()) {
				norms[iSpecies] = currentPeak->Norm();
			}
		} 
	}

	// Loop over all bins with more than a certain number of entries.
	for (Int_t iTOFBin = tofMinBin; iTOFBin <= tofMaxBin; iTOFBin++) {
		for (Int_t iTPCBin = tpcMinBin; iTPCBin <= tpcMaxBin; iTPCBin++) {
			
			Double_t dataVal = fData->GetBinContent(iTOFBin, iTPCBin);

			if (dataVal < minEntriesForLL) {continue;}
			
			// NEW: -----------------
			Double_t modelVal = 0.;

			for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {

				AliTOFTPCPeakDiHadronPID* currentPeak = (AliTOFTPCPeakDiHadronPID*)selectedFunc->GetPeak(iSpecies);
				if (currentPeak) {
					if (currentPeak->IsUsedInFit()) {modelVal += (norms[iSpecies] * currentPeak->Integral(tofAxis->GetBinLowEdge(iTOFBin), tofAxis->GetBinUpEdge(iTOFBin),
						tpcAxis->GetBinLowEdge(iTPCBin), tpcAxis->GetBinUpEdge(iTPCBin)));}
				} 

			}

			// OLD -----------------
			// xy[0] = tofAxis->GetBinCenter(iTOFBin);
			// xy[1] = tpcAxis->GetBinCenter(iTPCBin);
			// Double_t oldVal = eval(fitStage, xy);

			// cout << "Difference: " <<modelVal << "/" << oldVal << " = " << modelVal / oldVal << endl;
			// ---------------------

			// NEW ------------------
			// Add mismatch value.
			if (fMismatch->GetEntries() > 0) {
			  Double_t mmval = fMismatch->GetBinContent(iTOFBin, iTPCBin);
			  mmval *= (selectedFunc->GetMismatchYield() / fMismatch->GetEntries());
			  modelVal += mmval;
                        }
                        else
                          cout << "[WARNING]: no mismatch entries in " << __func__ << endl;
			// ----------------------
	
			nll += modelVal - dataVal * ROOT::Math::Util::EvalLog(modelVal); 	// See Cowan eq. (6.46) for details. //safe log
		
		}
	}

	// Since the LL is negative, we cannot simply scale it with the penalty factor. 
        if ( TMath::IsNaN(nll) )
          cout << "[ERROR]: log likelihood is inf or nan!" << endl;
        if (fSettings->IsFitPenaltyUsed()) {
          double penalty = CalculatePenaltyFactor(fitStage);
          if ( TMath::IsNaN(penalty) )
            cout << "[ERROR]: penalty is inf or nan!" << endl;
          nll = nll + (TMath::Abs(nll) * (penalty - 1));
        }
	return nll;

}
// -----------------------------------------------------------------------
Double_t AliTOFTPCFitContainerDiHadronPID::CalculatePenaltyFactor(Int_t fitStage) const {

	// This method calculates a penalty factor that becomes increasingly bigger if
	// the fit parameters do not make sense.
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	Double_t penaltyFactor = 1.;
	AliTOFTPCFitFunctionDiHadronPID* selectedFunc = 0;
	if (fitStage < 0) 
	  selectedFunc = (AliTOFTPCFitFunctionDiHadronPID*)fFitFunctions->Last();
	else
	  selectedFunc = (AliTOFTPCFitFunctionDiHadronPID*)fFitFunctions->At(fitStage);

	// Loop over all peaks.
	for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {
		
		// Check if the peak is included.
		AliTOFTPCPeakDiHadronPID* currentPeak = selectedFunc->GetPeak(iSpecies);

		if (currentPeak->IsUsedInFit()) {

			// Check sanity of the sigma's.
			penaltyFactor *= AliFunctionsDiHadronPID::PolyPenalty(0.01*currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTOF), 2., 0.6, 3.); //150., 3); // Changed 2016-06-11; should be different for pp, pPb  // added factor 0.01 to reduce to O(1)
			//penaltyFactor *= AliFunctionsDiHadronPID::PolyPenalty(currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTPC), 10., 9., 3);
			// MvL: TPC width is ~3
			penaltyFactor *= AliFunctionsDiHadronPID::PolyPenalty(currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTPC), 3.0, 1.0, 3.); //2.0, 3); // Changed 2016-06-11

			// Check sanity of the TOF tail.
			//penaltyFactor *= AliFunctionsDiHadronPID::PolyPenalty(1000. * currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kTailTOF), 40., 37., 3);
			// MvL: central value should be 8 or 8.5?
			penaltyFactor *= AliFunctionsDiHadronPID::PolyPenalty(1000. * currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kTailTOF), 10., 8., 3);

		}

	}

	if (penaltyFactor > 10.) {
           AliLogDiHadronPID::Warning(Form("Penalty factor is: %g", penaltyFactor), __func__, ClassName());
	   for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {
		// Check if the peak is included.
		AliTOFTPCPeakDiHadronPID* currentPeak = selectedFunc->GetPeak(iSpecies);
		if (currentPeak->IsUsedInFit()) {
	          AliLogDiHadronPID::Warning(Form("   Peak %d sigma TPC %f sigma TOF %f tail TOF %f",iSpecies,currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTPC),currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTOF),currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kTailTOF)), __func__, ClassName());
                }
            }
        }

	return penaltyFactor;

}

// -----------------------------------------------------------------------
Int_t AliTOFTPCFitContainerDiHadronPID::GetFitStatus(Int_t fitStage) const {

	AliTOFTPCFitFunctionDiHadronPID* selectedFunc = (AliTOFTPCFitFunctionDiHadronPID*)fFitFunctions->At(fitStage);
	if (selectedFunc) {
		return selectedFunc->GetFitStatus();
	} else {
		return -999;
	}

} 
/*
// -----------------------------------------------------------------------
void AliTOFTPCFitContainerDiHadronPID::SetFitStatusForFitStage(Int_t status, Int_t fitStage) {

	AliTOFTPCFitFunctionDiHadronPID* selectedFunc = (AliTOFTPCFitFunctionDiHadronPID*)fFitFunctions->At(fitStage);
	if (selectedFunc) {
		return selectedFunc->SetFitStatus(status);
	}

}
*/
// -----------------------------------------------------------------------
double AliTOFTPCFitContainerDiHadronPID::currentChi2(const double* pars) {

	// Change the current parametrers, and return chi2.
	AliTOFTPCFitFunctionDiHadronPID* currentFunc = (AliTOFTPCFitFunctionDiHadronPID*)fFitFunctions->At(fCurrentFitFunction);
	currentFunc->SetPars(pars);

	return chi2(fCurrentFitFunction);

}

// -----------------------------------------------------------------------
double AliTOFTPCFitContainerDiHadronPID::currentNegll(const double* pars) {

	// Change the current parametrers, and return negll.
	AliTOFTPCFitFunctionDiHadronPID* currentFunc = (AliTOFTPCFitFunctionDiHadronPID*)fFitFunctions->At(fCurrentFitFunction);
	currentFunc->SetPars(pars);

	return negll(fCurrentFitFunction);

}
/*
// -----------------------------------------------------------------------
void AliTOFTPCFitContainerDiHadronPID::ReInitialise(AliTOFTPCFitContainerDiHadronPID* otherContainer) {

	// This method copies the tail, mu, sigma parameters from the current stage of
	// other container. After that, the parameters for integrals and mismatches are
	// reinitialised based on the copied mu, sigma and tail.
	//
	// If a complete reset is desired, then first call ResetFitStages().
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	AliTOFTPCFitFunctionDiHadronPID* otherCurrentFitStage = otherContainer->CurrentFitStage();

	for (Int_t iSpecies = 0; fSettings->GetNSpecies(); iSpecies++) {

		CurrentFitStage()->SetMuTOFForSpecies(otherCurrentFitStage->GetMuTOFForSpecies(iSpecies), iSpecies);
		CurrentFitStage()->SetSigmaTOFForSpecies(otherCurrentFitStage->GetSigmaTOFForSpecies(iSpecies), iSpecies);
		CurrentFitStage()->SetTailTOFForSpecies(otherCurrentFitStage->GetTailTOFForSpecies(iSpecies), iSpecies);
		CurrentFitStage()->SetMuTPCForSpecies(otherCurrentFitStage->GetMuTPCForSpecies(iSpecies), iSpecies);
		CurrentFitStage()->SetSigmaTPCForSpecies(otherCurrentFitStage->GetSigmaTPCForSpecies(iSpecies), iSpecies);
		CurrentFitStage()->SetTailTPCForSpecies(otherCurrentFitStage->GetTailTPCForSpecies(iSpecies), iSpecies);

	}

	// Reinitialise the yields of the peaks and the mismatches.
	if (fData && fMismatch) {
		CurrentFitStage()->ReInitialiseYields(fData, fMismatch);
	} else {
		AliLogDiHadronPID::Warning(Form("Either fData or fMismatch is unavailable."), __func__, ClassName());
	}

}
*/
// -----------------------------------------------------------------------
void AliTOFTPCFitContainerDiHadronPID::ResetFitStages() {

	// Delete current fit information, create a new array, with just
	// one fit stage in it.
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	if (fFitFunctions) {delete fFitFunctions; fFitFunctions = 0x0;}
	fFitFunctions = new TObjArray(1);
	fFitFunctions->SetOwner(kTRUE);

	// There's no fit function in the container yet, not even with the standard settings!
	fCurrentFitFunction = -1;

}

// -----------------------------------------------------------------------
AliTOFTPCFitFunctionDiHadronPID* AliTOFTPCFitContainerDiHadronPID::CurrentFitStage() {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	// Lazy instantiation of the fit stages.
	if (fCurrentFitFunction == -1) {NewFitStage();}

	return ((AliTOFTPCFitFunctionDiHadronPID*)fFitFunctions->At(fCurrentFitFunction));

}

// -----------------------------------------------------------------------
Bool_t AliTOFTPCFitContainerDiHadronPID::NewFitStage(Bool_t copyPreviousStage) {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	AliTOFTPCFitFunctionDiHadronPID* func = 0x0;

	// Copy from previous stage if it exists. 
	if (copyPreviousStage && fFitFunctions->Last()) {
		AliTOFTPCFitFunctionDiHadronPID* oldfunc = (AliTOFTPCFitFunctionDiHadronPID*)fFitFunctions->Last();
		func = new AliTOFTPCFitFunctionDiHadronPID(*oldfunc); 

	} else {

		func = new AliTOFTPCFitFunctionDiHadronPID(fMassAssumption, fPtMin, fPtMax, fEtaMin, fEtaMax, fData, fMismatch);

	}

	if (func) {
		fFitFunctions->AddLast(func);
		fCurrentFitFunction++;
	} else {return kFALSE;}

	return kTRUE;

}

// -----------------------------------------------------------------------
TH2F* AliTOFTPCFitContainerDiHadronPID::GetEmptyPIDHist(const char* name, const char* title) {

	// Returns an empty histogram with the same PID axes as the data and
	// mismatch.
	AliLogDiHadronPID::Debug4(Form("Called with name: %s, title: %s.", name, title), __func__, ClassName());

	return ((TH2F*)(AliHistToolsDiHadronPID::MakeHist(name, title, "F", fData->GetXaxis(), fData->GetYaxis())));

}

// -----------------------------------------------------------------------
// AliTOFTPCFitFunctionDiHadronPID* AliTOFTPCFitContainerDiHadronPID::GetFitStage(Int_t fitstage) {

// 	return ((AliTOFTPCFitFunctionDiHadronPID*)fFitFunctions->At(fitstage));

// }

// -----------------------------------------------------------------------
void AliTOFTPCFitContainerDiHadronPID::Print(Option_t*) const {

	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	cout << "------------------------------------------------------------------" << endl;
	cout << " Fit Container" << endl;
	cout << "------------------------------------------------------------------" << endl;
	
	cout << " Number of stages: " << GetNFitStages() << endl;
	cout << " Current fit function index: " << fCurrentFitFunction << endl;

	for (Int_t iFitStage = 0; iFitStage < GetNFitStages(); iFitStage++) {
			
		cout << " Stage " << iFitStage << ": chi^2 = " << chi2(iFitStage) << " negll = " << negll(iFitStage) << endl;
	
	}

	cout << endl;

	// Print Kinematical info.
	cout << " Mass Assumption: " << fMassAssumption << endl;
	cout << " Pt range: " << fPtMin << " < p_T < " << fPtMax << endl;
	cout << " Eta range: " << fEtaMin << " < eta < " << fEtaMax << endl;
	cout << endl;

	cout << "------------------------------------------------------------------" << endl;

}

// -----------------------------------------------------------------------
void AliTOFTPCFitContainerDiHadronPID::DrawFitStage(Int_t fitstage, TCanvas* canvas, Int_t subpad) const {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	// Create a default canvas, and draw the data.
	if (canvas) {canvas->cd(subpad);}
	else {canvas = TCanvas::MakeDefCanvas();}

	TH1* plottedData = fData->DrawCopy("colz");
	plottedData->SetTitle(Form("Stage %i", fitstage));

	// Print the position of the peaks.
	Double_t markersize = 0.5;

	AliTOFTPCFitFunctionDiHadronPID* currentFitFunction = (AliTOFTPCFitFunctionDiHadronPID*)fFitFunctions->At(fitstage);
	if (!currentFitFunction) {
		AliLogDiHadronPID::Warning(Form("No fit found for stage %i", fitstage), __func__, ClassName());
		return;
	}

	Double_t avgPt = (fPtMax + fPtMin) / 2.;
	Double_t absAvgEta = AliFunctionsDiHadronPID::GetAbsAvgEta(fEtaMin, fEtaMax);

	TLegend* legend = new TLegend(0.75,0.75,0.92,0.92);
	Double_t nLegendEntries = 2.;

	// Loop over the peaks.
	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {

		// Get a ptr to the peak of species iSpecies.
		AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
		AliTOFTPCPeakDiHadronPID* currentPeak = currentFitFunction->GetPeak(currentSpecies);

		// Check if the peak was used in the fit.
		if (!currentPeak->IsUsedInFit()) {continue;}

		// Draw markers.
		TMarker* marker = new TMarker(currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTOF), currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTPC), fSettings->GetSpeciesPlotMarker(currentSpecies));
		marker->SetMarkerSize(markersize);
		marker->SetMarkerColor(kBlack);
		marker->Draw("same");
		legend->AddEntry(marker, fSettings->GetSpeciesName(currentSpecies), "P");
		nLegendEntries+=1.;

		// Draw 1 sigma contour.
		TEllipse* onesigma = new TEllipse(currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTOF), currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTPC), currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTOF), currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTPC));
		onesigma->SetLineColor(kBlack);
		onesigma->SetFillStyle(0);
		onesigma->SetLineWidth(2);
		onesigma->Draw("same");

	}

	// Print the fit region.
	TBox* fitregion = new TBox(currentFitFunction->GetFitRegionTOFMin(), currentFitFunction->GetFitRegionTPCMin(), currentFitFunction->GetFitRegionTOFMax(), currentFitFunction->GetFitRegionTPCMax());
	fitregion->SetLineColor(kBlack);
	fitregion->SetFillStyle(0);
	fitregion->SetLineWidth(2);
	fitregion->Draw("same");
	legend->AddEntry(fitregion, "Fit Region", "F");

	// TODO: This should really be "known" by the container, not recalculated.
	const TAxis* tofAxis = fSettings->GetTOFAxis(fMassAssumption);
	const TAxis* tpcAxis = fSettings->GetTPCAxis(fMassAssumption);

	Double_t mismatchRegionTOFMin = tofAxis->GetBinCenter(fSettings->GetMismatchRegionTOFminBin(fMassAssumption, avgPt, absAvgEta));
	Double_t mismatchRegionTOFMax = tofAxis->GetBinCenter(fSettings->GetMismatchRegionTOFmaxBin(fMassAssumption, avgPt, absAvgEta));
	Double_t mismatchRegionTPCMin = tpcAxis->GetXmin();
	Double_t mismatchRegionTPCMax = tpcAxis->GetXmax();

	// Print the mismatch region.
	TBox* mmregion = new TBox(mismatchRegionTOFMin, mismatchRegionTPCMin, mismatchRegionTOFMax, mismatchRegionTPCMax);
	mmregion->SetLineColor(kGray);
	mmregion->SetFillStyle(0);
	mmregion->SetLineWidth(2);
	mmregion->Draw("same");

	legend->AddEntry(mmregion, "Mismatch Region", "F");
	legend->SetY1(0.9 - nLegendEntries * 0.04);
	legend->Draw("same");

}

// -----------------------------------------------------------------------
void AliTOFTPCFitContainerDiHadronPID::DrawResiduals(Int_t fitstage, TCanvas* canvas, Int_t subpad) const {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	// Create a default canvas, and draw the data.
	if (canvas) {canvas->cd(subpad);}
	else {canvas = TCanvas::MakeDefCanvas();}

	TH1* plottedData = fData->DrawCopy("colz");
	plottedData->SetTitle(Form("Stage %i", fitstage));

	// Print the position of the peaks.
	Double_t markersize = 0.5;

	AliTOFTPCFitFunctionDiHadronPID* currentFitFunction = (AliTOFTPCFitFunctionDiHadronPID*)fFitFunctions->At(fitstage);
	if (!currentFitFunction) {
		AliLogDiHadronPID::Warning(Form("No fit found for stage %i", fitstage), __func__, ClassName());
		return;
	}

        // Subtract fit
	Float_t mismatchScale = currentFitFunction->GetMismatchYield() / fMismatch->GetEntries();
        for (Int_t ixbin = 1; ixbin <= plottedData->GetXaxis()->GetNbins(); ixbin++) { 
          Double_t xy[2] = {0};
          xy[0] = plottedData->GetXaxis()->GetBinCenter(ixbin);
          for (Int_t iybin = 1; iybin <= plottedData->GetYaxis()->GetNbins(); iybin++) { 
            xy[1] = plottedData->GetYaxis()->GetBinCenter(iybin);
            plottedData->SetBinContent(ixbin,iybin,plottedData->GetBinContent(ixbin,iybin) 
                                                   -(*currentFitFunction)(xy)
                                                   -mismatchScale*fMismatch->GetBinContent(ixbin,iybin));
          }
        }

	Double_t avgPt = (fPtMax + fPtMin) / 2.;
	Double_t absAvgEta = AliFunctionsDiHadronPID::GetAbsAvgEta(fEtaMin, fEtaMax);

	TLegend* legend = new TLegend(0.75,0.75,0.92,0.92);
	Double_t nLegendEntries = 2.;

	// Loop over the peaks.
	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {

		// Get a ptr to the peak of species iSpecies.
		AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
		AliTOFTPCPeakDiHadronPID* currentPeak = currentFitFunction->GetPeak(currentSpecies);

		// Check if the peak was used in the fit.
		if (!currentPeak->IsUsedInFit()) {continue;}

		// Draw markers.
		TMarker* marker = new TMarker(currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTOF), currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTPC), fSettings->GetSpeciesPlotMarker(currentSpecies));
		marker->SetMarkerSize(markersize);
		marker->SetMarkerColor(kBlack);
		marker->Draw("same");
		legend->AddEntry(marker, fSettings->GetSpeciesName(currentSpecies), "P");
		nLegendEntries+=1.;

		// Draw 1 sigma contour.
		TEllipse* onesigma = new TEllipse(currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTOF), currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTPC), currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTOF), currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTPC));
		onesigma->SetLineColor(kBlack);
		onesigma->SetFillStyle(0);
		onesigma->SetLineWidth(2);
		onesigma->Draw("same");

	}

	plottedData->GetXaxis()->SetRangeUser(currentFitFunction->GetFitRegionTOFMin(), currentFitFunction->GetFitRegionTOFMax());
        plottedData->GetYaxis()->SetRangeUser(currentFitFunction->GetFitRegionTPCMin(), currentFitFunction->GetFitRegionTPCMax());
        TLatex ltx;
        ltx.SetNDC();
        ltx.DrawLatex(0.55,0.85,Form("LL = %.1f",negll(fitstage)));
        Int_t nbins = (currentFitFunction->GetFitRegionTOFMax() - currentFitFunction->GetFitRegionTOFMin() + 1)*(currentFitFunction->GetFitRegionTPCMax() - currentFitFunction->GetFitRegionTPCMin() + 1);
        ltx.DrawLatex(0.55,0.8,Form("nbins = %d",nbins));

}
// -----------------------------------------------------------------------
void AliTOFTPCFitContainerDiHadronPID::DrawTOFProjection(Int_t fitStage, TCanvas* canvas, Int_t subpad) const {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	if (canvas) {canvas->cd(subpad);}
	else {canvas = TCanvas::MakeDefCanvas();}
	canvas->SetLogy();
	
	// Get a pointer to the current fit result.
	AliTOFTPCFitFunctionDiHadronPID* currentFitFunction = (AliTOFTPCFitFunctionDiHadronPID*)fFitFunctions->At(fitStage);
	if (!currentFitFunction) {
		AliLogDiHadronPID::Warning(Form("No fit found for stage %i", fitStage), __func__, ClassName());
		return;
	}

	// Determine the integration range.
	TAxis* tofAxis = fData->GetXaxis();
	TAxis* tpcAxis = fData->GetYaxis();
	Int_t tpcMinBin = tpcAxis->FindBin(currentFitFunction->GetFitRegionTPCMin());
	Int_t tpcMaxBin = tpcAxis->FindBin(currentFitFunction->GetFitRegionTPCMax());

	// Make a Legend.
	TLegend* legend = new TLegend(0.75,0.70,0.92,0.92);
	Double_t nLegendEntries = 3.;

	// Draw TOF projection of the data.
	TH1F* data_proj = (TH1F*)(fData->ProjectionX(Form("Stage %i", fitStage), tpcMinBin, tpcMaxBin));
	data_proj->SetDirectory(0);
	data_proj->SetTitle(Form("Stage %i", fitStage));
	data_proj->SetLineColor(kBlack);
	legend->AddEntry(data_proj->DrawCopy(), "Data");
	delete data_proj;

	// Draw the TOF projection of the mismatch.
	TH1F* mismatch_proj = (TH1F*)(fMismatch->ProjectionX(Form("mm Stage %i", fitStage), tpcMinBin, tpcMaxBin));
	mismatch_proj->SetDirectory(0);
	//mismatch_proj->SetTitle(Form("Stage %i", fitstage));
	mismatch_proj->SetLineColor(kGray + 1);
	mismatch_proj->Scale(currentFitFunction->GetMismatchYield() / fMismatch->GetEntries());
	legend->AddEntry(mismatch_proj->DrawCopy("same"), "TOF Mismatch");

	// Total fit result histogram.
	TH1F* totfit = new TH1F(*mismatch_proj);
	totfit->SetName("totfit");
	totfit->SetDirectory(0);
	totfit->SetLineColor(kViolet);	

	// Determine norms of the included peak functions.
	Double_t norms[AliSettingsDiHadronPID::GetNSpecies()];
	for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {
			
		norms[iSpecies] = 0.;
		AliTOFTPCPeakDiHadronPID* currentPeak = (AliTOFTPCPeakDiHadronPID*)currentFitFunction->GetPeak(iSpecies);
		if (currentPeak) {
			if (currentPeak->IsUsedInFit()) {norms[iSpecies] = currentPeak->Norm();}
		} 
	
	}

	// Draw the peaks.
	//Double_t xy[2] = {0.,0.};	
	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {

		// Get a ptr to the peak of species iSpecies.
		AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
		AliTOFTPCPeakDiHadronPID* currentPeak = currentFitFunction->GetPeak(currentSpecies);

		// Check if the peak was used in the fit.
		if (!currentPeak->IsUsedInFit()) {continue;}

		// Create a histogram of the current peak.
		TH1F* hist = (TH1F*)mismatch_proj->Clone(Form("TOF_%i", iSpecies));
		hist->SetDirectory(0);
		hist->SetLineColor(fSettings->GetSpeciesColor(currentSpecies));

		// Fill the projection histogram.
		for (Int_t iBinTOF = 1; iBinTOF <= hist->GetNbinsX(); iBinTOF++) {

			Double_t binContent = 0.;

			//xy[0] = tofAxis->GetBinCenter(iBinTOF);

			for (Int_t iBinTPC = tpcMinBin; iBinTPC <= tpcMaxBin; iBinTPC++) {

				//xy[1] = tpcAxis->GetBinCenter(iBinTPC);

				//binContent += (*currentPeak)(xy);  //currentFitFunction->EvalSpecies(tofAxis->GetBinCenter(iBinTOF), tpcAxis->GetBinCenter(iBinTPC), iSpecies);
				binContent += currentPeak->Integral(tofAxis->GetBinLowEdge(iBinTOF), tofAxis->GetBinUpEdge(iBinTOF),
						tpcAxis->GetBinLowEdge(iBinTPC), tpcAxis->GetBinUpEdge(iBinTPC));
			}

			hist->SetBinContent(iBinTOF, binContent * norms[iSpecies]);

		}

		legend->AddEntry(hist->DrawCopy("same"), fSettings->GetSpeciesName(currentSpecies));
		nLegendEntries += 1.;
		totfit->Add(hist);
		delete hist;
	
	}

	legend->AddEntry(totfit->DrawCopy("same"), "Total Fit");
	delete totfit;

	delete mismatch_proj;

	legend->SetY1(0.9 - nLegendEntries * 0.04);
	legend->Draw();

}

// -----------------------------------------------------------------------
void AliTOFTPCFitContainerDiHadronPID::DrawTPCProjection(Int_t fitstage, TCanvas* canvas, Int_t subpad) const {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	if (canvas) {canvas->cd(subpad);}
	else {canvas = TCanvas::MakeDefCanvas();}
        // pad1 for main fit
        TPad *pad1 = new TPad(Form("%s_1",canvas->GetName()),canvas->GetTitle(),0.0,0.2,1.0,1.0);
	pad1->Draw();
        // pad2 for pull
        TPad *pad2 = new TPad(Form("%s_2",canvas->GetName()),canvas->GetTitle(),0.0,0.0,1.0,0.2);
	pad2->Draw();

	pad1->cd();
	if (fMakeLogPlots)
	  pad1->SetLogy();

	// Get a pointer to the current fit result.
	AliTOFTPCFitFunctionDiHadronPID* currentFitFunction = (AliTOFTPCFitFunctionDiHadronPID*)fFitFunctions->At(fitstage);
	if (!currentFitFunction) {
		AliLogDiHadronPID::Warning(Form("No fit found for stage %i", fitstage), __func__, ClassName());
		return;
	}

	// Determine the integration range.
	TAxis* tofAxis = fData->GetXaxis();
	TAxis* tpcAxis = fData->GetYaxis();
	Int_t tofMinBin = tofAxis->FindBin(currentFitFunction->GetFitRegionTOFMin());
	Int_t tofMaxBin = tofAxis->FindBin(currentFitFunction->GetFitRegionTOFMax());

	// Make a Legend.
	TLegend* legend = new TLegend(0.75,0.70,0.92,0.92);
	Double_t nLegendEntries = 3.;

	// Draw TOF projection of the data.
	TH1F* data_proj = (TH1F*)(fData->ProjectionY(Form("Stage %i", fitstage), tofMinBin, tofMaxBin));
	data_proj->SetDirectory(0);
	data_proj->SetTitle(Form("Stage %i", fitstage));
	data_proj->SetLineColor(kBlack);
	legend->AddEntry(data_proj->DrawCopy(), "Data");
        TH1F *res_hist = new TH1F(*data_proj);
        res_hist->SetName(Form("%s_residuals",data_proj->GetName()));
        res_hist->Sumw2();
	delete data_proj;

	// Draw the TOF projection of the mismatch.
	TH1F* mismatch_proj = (TH1F*)(fMismatch->ProjectionY(Form("mm Stage %i", fitstage), tofMinBin, tofMaxBin));
	mismatch_proj->SetDirectory(0);
	//mismatch_proj->SetTitle(Form("Stage %i", fitstage));
	mismatch_proj->SetLineColor(kGray + 1);
	mismatch_proj->Scale(currentFitFunction->GetMismatchYield() / fMismatch->GetEntries());
	legend->AddEntry(mismatch_proj->DrawCopy("same"), "TOF Mismatch");

	// Total fit result histogram.
	TH1F* totfit = new TH1F(*mismatch_proj);
	totfit->SetName("totfit");
	totfit->SetDirectory(0);
	totfit->SetLineColor(kViolet);

	// Determine norms of the included peak functions.
	Double_t norms[AliSettingsDiHadronPID::GetNSpecies()];
	for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {
			
		norms[iSpecies] = 0.;
		AliTOFTPCPeakDiHadronPID* currentPeak = (AliTOFTPCPeakDiHadronPID*)currentFitFunction->GetPeak(iSpecies);
		if (currentPeak) {
			if (currentPeak->IsUsedInFit()) {norms[iSpecies] = currentPeak->Norm();}
		} 
	
	}

	// Draw the peaks.
	//Double_t xy[2] = {0.,0.};
	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {

		// Get a ptr to the peak of species iSpecies.
		AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
		AliTOFTPCPeakDiHadronPID* currentPeak = currentFitFunction->GetPeak(currentSpecies);

		// Check if the peak was used in the fit.
		if (!currentPeak->IsUsedInFit()) {continue;}

		// Create a histogram of the current peak.
		TH1F* hist = (TH1F*)mismatch_proj->Clone(Form("TPC_%i", iSpecies));
		hist->SetDirectory(0);
		hist->SetLineColor(fSettings->GetSpeciesColor(currentSpecies));

		for (Int_t iBinTPC = 1; iBinTPC <= hist->GetNbinsX(); iBinTPC++) {

			//xy[1] = tpcAxis->GetBinCenter(iBinTPC);

			Double_t binContent = 0.;

			for (Int_t iBinTOF = tofMinBin; iBinTOF <= tofMaxBin; iBinTOF++) {

				//xy[0] = tpcAxis->GetBinCenter(iBinTOF);

				//binContent += (*currentPeak)(xy);  //currentFitFunction->EvalSpecies(tofAxis->GetBinCenter(iBinTOF), tpcAxis->GetBinCenter(iBinTPC), iSpecies);
				binContent += currentPeak->Integral(tofAxis->GetBinLowEdge(iBinTOF), tofAxis->GetBinUpEdge(iBinTOF),
						tpcAxis->GetBinLowEdge(iBinTPC), tpcAxis->GetBinUpEdge(iBinTPC));
			}

			hist->SetBinContent(iBinTPC, binContent * norms[iSpecies]);

		}

		legend->AddEntry(hist->DrawCopy("same"), fSettings->GetSpeciesName(currentSpecies));
		nLegendEntries += 1.;		
		totfit->Add(hist);
		delete hist;
	
	}

        res_hist->Add(totfit, -1);

	legend->AddEntry(totfit->DrawCopy("same"), "Total Fit");
	delete totfit;

	delete mismatch_proj;
	legend->SetY1(0.9 - nLegendEntries * 0.04);
	legend->Draw();
        
        // Draw residuals
        pad2->cd();
	res_hist->SetDirectory(0);
        res_hist->DrawCopy();
        delete res_hist;

}

// -----------------------------------------------------------------------
void AliTOFTPCFitContainerDiHadronPID::DrawTPCSlices(Int_t fitStage, TString directory) const {

  AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

  TCanvas *canvas = TCanvas::MakeDefCanvas();
  if (fMakeLogPlots)
    canvas->SetLogy();

  // Get a pointer to the current fit result.
  AliTOFTPCFitFunctionDiHadronPID* currentFitFunction = (AliTOFTPCFitFunctionDiHadronPID*)fFitFunctions->At(fitStage);
  if (!currentFitFunction) {
    AliLogDiHadronPID::Warning(Form("No fit found for stage %i", fitStage), __func__, ClassName());
    return;
  }

  for (Int_t iSlice = 0; iSlice < 3; iSlice++) { // slice around each peak
    // Determine the integration range.	
    TAxis* tofAxis = fData->GetXaxis();
    TAxis* tpcAxis = fData->GetYaxis();
    AliTOFTPCPeakDiHadronPID* peak = currentFitFunction->GetPeak(iSlice);
    Int_t tofMinBin = tofAxis->FindBin(peak->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTOF)-peak->GetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTOF));
    Int_t tofMaxBin = tofAxis->FindBin(peak->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTOF)+peak->GetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTOF));

    // Make a Legend.
    TLegend* legend = new TLegend(0.75,0.70,0.92,0.92);
    Double_t nLegendEntries = 3.;

    // Draw TOF projection of the data.
    TH1F* data_proj = (TH1F*)(fData->ProjectionY(Form("Stage_%i_Slice_%d", fitStage,iSlice), tofMinBin, tofMaxBin));
    data_proj->SetDirectory(0);
    data_proj->SetTitle(Form("Stage %i Slice %i", fitStage, iSlice));
    data_proj->SetLineColor(kBlack);
    legend->AddEntry(data_proj->DrawCopy(), "Data");
    delete data_proj;

    // Draw the TOF projection of the mismatch.
    TH1F* mismatch_proj = (TH1F*)(fMismatch->ProjectionY(Form("mm_Stage_%i_Slice_%d", fitStage, iSlice), tofMinBin, tofMaxBin));
    mismatch_proj->SetDirectory(0);
    //mismatch_proj->SetTitle(Form("Stage %i", fitstage));
    mismatch_proj->SetLineColor(kGray + 1);
    mismatch_proj->Scale(currentFitFunction->GetMismatchYield() / fMismatch->GetEntries());
    legend->AddEntry(mismatch_proj->DrawCopy("same"), "TOF Mismatch");

    // Total fit result histogram.
    TH1F* totfit = new TH1F(*mismatch_proj);
    totfit->SetName("totfit");
    totfit->SetDirectory(0);
    totfit->SetLineColor(kViolet);	

    // Determine norms of the included peak functions.
    Double_t norms[AliSettingsDiHadronPID::GetNSpecies()];
    for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {
			
      norms[iSpecies] = 0.;
      AliTOFTPCPeakDiHadronPID* currentPeak = (AliTOFTPCPeakDiHadronPID*)currentFitFunction->GetPeak(iSpecies);
      if (currentPeak) {
	if (currentPeak->IsUsedInFit()) {norms[iSpecies] = currentPeak->Norm();}
      } 
	
    }

    // Draw the peaks.
    //Double_t xy[2] = {0.,0.};
    for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {

      // Get a ptr to the peak of species iSpecies.
      AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
      AliTOFTPCPeakDiHadronPID* currentPeak = currentFitFunction->GetPeak(currentSpecies);

      // Check if the peak was used in the fit.
      if (!currentPeak->IsUsedInFit()) {continue;}

      // Create a histogram of the current peak.
      TH1F* hist = (TH1F*)mismatch_proj->Clone(Form("TPC_%i_%d", iSpecies, iSlice));
      hist->SetDirectory(0);
      hist->SetLineColor(fSettings->GetSpeciesColor(currentSpecies));

      for (Int_t iBinTPC = 1; iBinTPC <= hist->GetNbinsX(); iBinTPC++) {

	//xy[1] = tpcAxis->GetBinCenter(iBinTPC);

	Double_t binContent = 0.;

	for (Int_t iBinTOF = tofMinBin; iBinTOF <= tofMaxBin; iBinTOF++) {

	  //xy[0] = tpcAxis->GetBinCenter(iBinTOF);

	  //binContent += (*currentPeak)(xy);  //currentFitFunction->EvalSpecies(tofAxis->GetBinCenter(iBinTOF), tpcAxis->GetBinCenter(iBinTPC), iSpecies);
	  binContent += currentPeak->Integral(tofAxis->GetBinLowEdge(iBinTOF), tofAxis->GetBinUpEdge(iBinTOF),
					      tpcAxis->GetBinLowEdge(iBinTPC), tpcAxis->GetBinUpEdge(iBinTPC));
	}
	hist->SetBinContent(iBinTPC, binContent * norms[iSpecies]);
      }

      legend->AddEntry(hist->DrawCopy("same"), fSettings->GetSpeciesName(currentSpecies));
      nLegendEntries += 1.;		
      totfit->Add(hist);
      delete hist;
	
    }

    legend->AddEntry(totfit->DrawCopy("same"), "Total Fit");
    delete totfit;

    delete mismatch_proj;
    legend->SetY1(0.9 - nLegendEntries * 0.04);
    legend->Draw();
    canvas->SaveAs(Form("%s/TPC_Stage_%i_Slice_%i.pdf", directory.Data(), fitStage, iSlice));
  }
}

// -----------------------------------------------------------------------
void AliTOFTPCFitContainerDiHadronPID::SaveFitStages(TString directory, Bool_t saveSlices) const {

	// Saves the fit stages on disc, in the indicated directory.
	AliLogDiHadronPID::Debug3(Form("Called, to save files in directory: %s", directory.Data()), __func__, ClassName());

	// Create the directory.
	if (!AliMiscToolsDiHadronPID::CreateDir(directory)) {return;}

	// Create a canvas.
	TCanvas* canvas = 0x0;

	// Loop Over fit Stages.
	for (Int_t iFitStage = 0; iFitStage < GetNFitStages(); iFitStage++) {
		
		canvas = new TCanvas("ctoftpc","TOF vs TPC",1000,600);
                //TCanvas::MakeDefCanvas();
                canvas->Divide(2,1);
                canvas->cd(1);
		DrawFitStage(iFitStage, canvas,1);
                canvas->cd(2);
		DrawResiduals(iFitStage, canvas,2);
		canvas->SaveAs(Form("%s/TOFTPC_Stage_%i.pdf", directory.Data(), iFitStage));
		delete canvas; canvas = 0x0;

		canvas = TCanvas::MakeDefCanvas();
		DrawTOFProjection(iFitStage, canvas);
		canvas->SaveAs(Form("%s/TOF_Stage_%i.pdf", directory.Data(), iFitStage));
		delete canvas; canvas = 0x0;

		canvas = TCanvas::MakeDefCanvas();
		DrawTPCProjection(iFitStage, canvas);
		canvas->SaveAs(Form("%s/TPC_Stage_%i.pdf", directory.Data(), iFitStage));			
		delete canvas; canvas = 0x0;

		if (saveSlices)
		  DrawTPCSlices(iFitStage, directory);

		delete canvas; canvas = 0x0;

	}

}
/*
// -----------------------------------------------------------------------
void AliTOFTPCFitContainerDiHadronPID::DetermineTOFAndTPCRange() {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, "AliTOFTPCFitContainerDiHadronPID");

	Double_t avgPt = (fPtMax + fPtMin) / 2.;
	Double_t absAvgEta = AliFunctionsDiHadronPID::GetAbsAvgEta(fEtaMin, fEtaMax);

	fTOFMinBin = fSettings->GetFitRegionTOFminBin(fMassAssumption, avgPt, absAvgEta);
	fTOFMaxBin = fSettings->GetFitRegionTOFmaxBin(fMassAssumption, avgPt, absAvgEta);
	fTPCMinBin = fSettings->GetFitRegionTPCminBin(fMassAssumption, avgPt, absAvgEta);
	fTPCMaxBin = fSettings->GetFitRegionTPCmaxBin(fMassAssumption, avgPt, absAvgEta);

	const TAxis* tofAxis = fSettings->GetTOFAxis(fMassAssumption);
	const TAxis* tpcAxis = fSettings->GetTPCAxis(fMassAssumption);

	fTOFMin = tofAxis->GetBinCenter(fTOFMinBin);
	fTOFMax = tofAxis->GetBinCenter(fTOFMaxBin);

	fTPCMin = tpcAxis->GetBinCenter(fTPCMinBin);
	fTPCMax = tpcAxis->GetBinCenter(fTPCMaxBin);

}
*/
