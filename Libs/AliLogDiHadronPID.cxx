/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Class handeling debug messages for the di-hadron PID analysis.
//  This is a singleton class.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "AliLogDiHadronPID.h"
#include "TString.h"

AliLogDiHadronPID* AliLogDiHadronPID::fLogger = 0x0;

// -----------------------------------------------------------------------
AliLogDiHadronPID::~AliLogDiHadronPID() {

	CloseLogFile();

}

// -----------------------------------------------------------------------
AliLogDiHadronPID* AliLogDiHadronPID::Instance() {

	/** Returns a pointer to the instance (singleton class). If no 
		instance exists, then one is created. */
	if (!fLogger) {
		fLogger = new AliLogDiHadronPID();
	}

	return fLogger;

}

// -----------------------------------------------------------------------
void AliLogDiHadronPID::Kill() {

	/** If an instance exists, then this method deletes it. */
	if (fLogger) {
		delete fLogger; 
		fLogger = 0x0;
	}

}

// -----------------------------------------------------------------------
void AliLogDiHadronPID::OpenLogFile(const char* filename) {

	/** Create a file to write log messages. If a previous log file was 
		open, then it will be closed. */

	// Close existing log file, or create a new one.
	if (fLogFile) {fLogFile->close();} 
	else {fLogFile = new ofstream();}
	
	// Open new log file.
	if (fLogFile) {fLogFile->open(filename);}

}
// -----------------------------------------------------------------------
void AliLogDiHadronPID::CloseLogFile() {

	/** Close log file. */
	if (fLogFile) {
		fLogFile->close();
		delete fLogFile;
		fLogFile = 0x0;
	}

}

// -----------------------------------------------------------------------
void AliLogDiHadronPID::Print() const {

	/** Print some info about the instance. */
	cout << "----------------------------------------" << endl;
	cout << " AliLogDiHadronPID" << endl;
	cout << "----------------------------------------" << endl;
	cout << " Instance adress: " << fLogger << endl;
	cout << " Ofstream adress: " << fLogFile << endl;
	if (fLogFile) {cout << " Logfile open: " << (fLogFile->is_open() ? "YES" : "NO") << endl;}
	cout << "----------------------------------------" << endl;

}

// -----------------------------------------------------------------------
void AliLogDiHadronPID::Message(msgType type, const char* msg, const char* funcname, const char* clname) {

	/** Generic method to write or print a message of any type. Method not
		directly used. */
	TString printMsg;
	Bool_t willPrintMsg = kFALSE;
	Bool_t willWriteMsg = kFALSE;

	// Start the message, and determine if something will be written to file/ screen.
	switch (type) {
		case kInfo :
			if (fVerboseLevel > 2) {willPrintMsg = kTRUE;}
			if (fLogLevel > 2) {willWriteMsg = kTRUE;}
			printMsg += "[INFO] ";
			break;
		case kWarning :
			if (fVerboseLevel > 1) {willPrintMsg = kTRUE;}
			if (fLogLevel > 1) {willWriteMsg = kTRUE;}		
			printMsg += "[WARNING] ";
			break;
		case kError :
			if (fVerboseLevel > 0) {willPrintMsg = kTRUE;}
			if (fLogLevel > 0) {willWriteMsg = kTRUE;}		
			printMsg += "[ERROR] ";
			break;
		case kDebug1 :
			if (fDebugLevel > 0) {
				willPrintMsg = kTRUE;
				willWriteMsg = kTRUE;			
			}
			printMsg += "[DEBUG1] ";
			break;		
		case kDebug2 :
			if (fDebugLevel > 1) {
				willPrintMsg = kTRUE;
				willWriteMsg = kTRUE;			
			}
			printMsg += "[DEBUG2] ";
			break;		
		case kDebug3 :
			if (fDebugLevel > 2) {
				willPrintMsg = kTRUE;
				willWriteMsg = kTRUE;			
			}
			printMsg += "[DEBUG3] ";
			break;		
		case kDebug4 :
			if (fDebugLevel > 3) {
				willPrintMsg = kTRUE;
				willWriteMsg = kTRUE;			
			}
			printMsg += "[DEBUG4] ";
			break;
		default:
			return;
	}

	// If willWriteMsg = true, check if the file stream is open and available.
	if (willWriteMsg) {
		if (!fLogFile) {willWriteMsg = kFALSE;}
		else if ( !(fLogFile->is_open()) ) {willWriteMsg = kFALSE;}
	}

	// Return if no message will be printed or written.
	if ( (!willPrintMsg) && (!willWriteMsg) ) {return;}

	// If a class name was provided, then add it to the message.
	if (clname) {
		printMsg += clname;
		if (funcname) {printMsg += "::";}
		else {printMsg += ": ";}
	}

	// If a function name was provided, then add that to the message.
	if (funcname) {
		printMsg += funcname; 
		printMsg += ": ";
	}

	printMsg += msg;

	// Write to screen
	if (willPrintMsg) {cout << printMsg.Data() << endl;}

	// Write to file
	if (willWriteMsg) {(*fLogFile) << printMsg.Data() << endl;}

}
