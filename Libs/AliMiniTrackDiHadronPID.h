#ifndef ALIMINITRACKDIHADRONPID_H
#define ALIMINITRACKDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include <iostream>
#include "TObject.h"
#include "AliAODTrack.h"
#include "AliAODMCParticle.h"
#include "AliPIDResponse.h"

class AliMiniTrackDiHadronPID : public TObject {

public:
	// Track Flags.
	enum TrackFlag {
		kIsPositive			= 1,
		kTOFout 			= 2,
		kTIME 				= 4,
		kTPCout				= 8,
	};

public:
	AliMiniTrackDiHadronPID();
	AliMiniTrackDiHadronPID(const AliMiniTrackDiHadronPID& other);
	~AliMiniTrackDiHadronPID();

	// Setters.
	void			SetIsPositive() {AddFlag(kIsPositive);}
	void			SetIsNegative() {RemoveFlag(kIsPositive);}
	void 			AddFlag(Char_t flag) {fFlags = (fFlags | flag);}
	void			RemoveFlag(Char_t flag) {fFlags = (fFlags & (~flag));}
	void	 		SetPt(Float_t pt) {fPt = pt;}
	void			SetEta(Float_t eta) {fEta = eta;}
	void			SetPhi(Float_t phi) {fPhi = phi;}
	void			SetTOFsignalMinusExpected(Float_t TOFsignalMinusExpected, Int_t species) {if (IsValidSpecies(species)) {fTOFsignalMinusExpected[species] = TOFsignalMinusExpected;}}
	void			SetTOFRandom(Float_t TOFRandom, Int_t species) {if (IsValidSpecies(species)) {fTOFRandom[species] = TOFRandom;}}
	void			SetTPCsignalMinusExpected(Float_t TPCsignalMinusExpected, Int_t species) {if (IsValidSpecies(species)) {fTPCsignalMinusExpected[species] = TPCsignalMinusExpected;}}
	void			SetTOFnSigma(Float_t TOFnSigma, Int_t species) {if (IsValidSpecies(species)) {fTOFnSigma[species] = TOFnSigma;}}
	void			SetTOFnSigmaRandom(Float_t TOFnSigmaRandom, Int_t species) {if (IsValidSpecies(species)) {fTOFnSigmaRandom[species] = TOFnSigmaRandom;}}
	void			SetTPCnSigma(Float_t TPCnSigma, Int_t species) {if (IsValidSpecies(species)) {fTPCnSigma[species] = TPCnSigma;}}
	void			SetDCAxy(Float_t dcaxy) {fDCAxy = dcaxy;}
	void			SetDCAz(Float_t dcaz) {fDCAz = dcaz;}
	void			SetTPCSignalN(Short_t tpcSignalN) {fTPCSignalN = tpcSignalN;}
	void			SetStartTimeMask(Char_t starttimemask) {fStartTimeMask = starttimemask;}
	void			SetHasSPDHit(Bool_t hasspdhit = kTRUE) {fHasSPDHit = hasspdhit;}
	void			SetMCPt(Float_t mcpt) {if (!fMCInfo) {InitMCInfo();} fMCInfo->SetMCPt(mcpt);}
	void			SetMCEta(Float_t mceta) {if (!fMCInfo) {InitMCInfo();} fMCInfo->SetMCEta(mceta);}
	void			SetMCY(Float_t mcy) {if (!fMCInfo) {InitMCInfo();} fMCInfo->SetMCY(mcy);}
	void			SetMCPhi(Float_t mcphi) {if (!fMCInfo) {InitMCInfo();} fMCInfo->SetMCPhi(mcphi);}

	// Getters.
	Char_t			GetFlags() const {return fFlags;}
	UInt_t			GetFilterMap() const {return fFilterMap;}
	Bool_t			IsFlagSet(TrackFlag flag) const {return (flag & fFlags);}
	Bool_t			IsPositive() const {return IsFlagSet(kIsPositive);} 
	Bool_t			IsNegative() const {return (!IsPositive());}
	Float_t		 	Pt() const {return fPt;}
	Float_t		 	Eta() const {return fEta;}
	Float_t		 	Phi() const {return fPhi;}
	Float_t		 	GetTOFsignalMinusExpected(Int_t species) const {return (IsValidSpecies(species) ? fTOFsignalMinusExpected[species] : -999.);}
	Float_t			GetTOFRandom(Int_t species) const {return (IsValidSpecies(species) ? fTOFRandom[species] : -999.);}
	Float_t		 	GetTPCsignalMinusExpected(Int_t species) const {return (IsValidSpecies(species) ? fTPCsignalMinusExpected[species] : -999.);}
	Float_t		 	GetTOFnSigma(Int_t species) const {return (IsValidSpecies(species) ? fTOFnSigma[species] : -999.);}
	Float_t			GetTOFnSigmaRandom(Int_t species) const {return (IsValidSpecies(species) ? fTOFnSigmaRandom[species] : -999.);}
	Float_t		 	GetTPCnSigma(Int_t species) const {return (IsValidSpecies(species) ? fTPCnSigma[species] : -999.);}
	Float_t			GetDCAxy() const {return fDCAxy;}
	Float_t			GetDCAz() const {return fDCAz;}
	Short_t			GetTPCSignalN() const {return fTPCSignalN;}
	Char_t			GetStartTimeMask() const {return fStartTimeMask;}
	Bool_t			HasSPDHit() const {return fHasSPDHit;}
 
	// MC Setters.
	void		SetIsMCPion() {if (!fMCInfo) {InitMCInfo();} fMCInfo->SetIsMCPion();}
	void		SetIsMCKaon() {if (!fMCInfo) {InitMCInfo();} fMCInfo->SetIsMCKaon();}
	void		SetIsMCProton() {if (!fMCInfo) {InitMCInfo();} fMCInfo->SetIsMCProton();}
	void		SetIsMCDeuterium() {if (!fMCInfo) {InitMCInfo();} fMCInfo->SetIsMCDeuterium();}
	void		SetIsTOFMatch() {if (!fMCInfo) {InitMCInfo();} fMCInfo->SetIsTOFMatch();}
	void 		SetIsPhysicalPrimary() {if (!fMCInfo) {InitMCInfo();} fMCInfo->SetIsPhysicalPrimary();}
	void 		SetIsSecondaryFromWeakDecay() {if (!fMCInfo) {InitMCInfo();} fMCInfo->SetIsSecondaryFromWeakDecay();}
	void 		SetIsSecondaryFromMaterialDecay() {if (!fMCInfo) {InitMCInfo();} fMCInfo->SetIsSecondaryFromMaterialDecay();}

	// MC Getters.
	Float_t		MCPt() const {return ((fMCInfo) ? fMCInfo->MCPt() : -999.);}
	Float_t		MCEta() const {return ((fMCInfo) ? fMCInfo->MCEta() : -999.);}
	Float_t		MCY() const {return ((fMCInfo) ? fMCInfo->MCY() : -999.);}
	Float_t		MCPhi() const {return ((fMCInfo) ? fMCInfo->MCPhi() : -999.);}

	Bool_t		IsMC() const {return (fMCInfo ? kTRUE : kFALSE);}
	Bool_t		IsMCPion() const {return (fMCInfo ? fMCInfo->IsMCPion() : kFALSE);}
	Bool_t		IsMCKaon() const {return (fMCInfo ? fMCInfo->IsMCKaon() : kFALSE);}
	Bool_t		IsMCProton() const {return (fMCInfo ? fMCInfo->IsMCProton() : kFALSE);}
	Bool_t		IsMCDeuterium() const {return (fMCInfo ? fMCInfo->IsMCDeuterium() : kFALSE);}
	Bool_t		IsTOFMatch() const {return ((fMCInfo) ? fMCInfo->IsTOFMatch() : kFALSE);}
	Bool_t		IsPhysicalPrimary() const {return ((fMCInfo) ? fMCInfo->IsPhysicalPrimary() : kFALSE);}
	Bool_t		IsSecondaryFromWeakDecay() const {return ((fMCInfo) ? fMCInfo->IsSecondaryFromWeakDecay() : kFALSE);}
	Bool_t		IsSecondaryFromMaterialDecay() const {return ((fMCInfo) ? fMCInfo->IsSecondaryFromMaterialDecay() : kFALSE);}

	// Methods for taking relevant parameters from other track objects.
	void		CopyPars(const AliMiniTrackDiHadronPID* other);
	void		CopyPars(AliAODTrack* aodTrack, const AliAODEvent* aodEvent, AliPIDResponse* pidResponse, Bool_t isMC, Bool_t addMismatch = kFALSE, Double_t rndHitTime = -1.e21);
	void		CopyPars(const AliAODMCParticle* aodMCParticle, const AliAODMCParticle* aodMCMother);

	// Other Methods.	
	Bool_t		IsValidSpecies(Int_t species) const;
	Bool_t		IsVerySimilarTo(const AliMiniTrackDiHadronPID* other) const;
	void		ResetTrack();
	void		PrintTrack() const;
	void		InitMCInfo() {
		if (fMCInfo) {delete fMCInfo; fMCInfo = 0x0;}
		fMCInfo = new MCInfo();
	}

	// To still be compatible with the old version of the track, with 3 species.
	static Int_t	GetNSpecies() {return fNSpecies;}
	static void		SetThreeSpecies() {fNSpecies = 3;}
	static void		SetFourSpecies() {fNSpecies = 4;}

	static Int_t	GetNInstances() {return fNInstances;}
 
	// Nested MCInfo class.
	class MCInfo {

	public:
		// Constructor and destructor.
		MCInfo() {
			ResetInfo();
		}
		virtual ~MCInfo() {;}

		void	ResetInfo() {

			// fPdgCode = 0;
			fMCPt = -999.;
			fMCEta = -999.;
			fMCY = -999.;
			fMCPhi = -999.;

			fIsMCPion = kFALSE;
			fIsMCKaon = kFALSE;
			fIsMCProton = kFALSE;
			fIsMCDeuterium = kFALSE;

			fIsTOFMatch = kFALSE;
			fIsPhysicalPrimary = kFALSE;
			fIsSecondaryFromWeakDecay = kFALSE;
			fIsSecondaryFromMaterialDecay = kFALSE;

		}

		// Setters.
		void	SetMCPt(Float_t mcpt) {fMCPt = mcpt;}
		void	SetMCEta(Float_t mceta) {fMCEta = mceta;}
		void	SetMCY(Float_t mcy) {fMCY = mcy;}
		void	SetMCPhi(Float_t mcphi) {fMCPhi = mcphi;}

		// Getters.
		Float_t	MCPt() const {return fMCPt;}
		Float_t	MCEta() const {return fMCEta;}
		Float_t	MCY() const {return fMCY;}
		Float_t	MCPhi() const {return fMCPhi;}

	// Descriptive Flag setters.
		void	SetIsMCPion() {fIsMCPion = kTRUE;}
		void	SetIsMCKaon() {fIsMCKaon = kTRUE;}
		void	SetIsMCProton() {fIsMCProton = kTRUE;}
		void	SetIsMCDeuterium() {fIsMCDeuterium = kTRUE;}
		void	SetIsTOFMatch() {fIsTOFMatch = kTRUE;}
		void 	SetIsPhysicalPrimary() {fIsPhysicalPrimary = kTRUE;}
		void 	SetIsSecondaryFromWeakDecay() {fIsSecondaryFromWeakDecay = kTRUE;}
		void 	SetIsSecondaryFromMaterialDecay() {fIsSecondaryFromMaterialDecay = kTRUE;}

		// Descriptive Flag status.
		Bool_t	IsMCPion() const {return fIsMCPion;}
		Bool_t	IsMCKaon() const {return fIsMCKaon;}
		Bool_t	IsMCProton() const {return fIsMCProton;}
		Bool_t	IsMCDeuterium() const {return fIsMCDeuterium;}
		Bool_t	IsTOFMatch() const {return fIsTOFMatch;}
		Bool_t	IsPhysicalPrimary() const {return fIsPhysicalPrimary;}
		Bool_t	IsSecondaryFromWeakDecay() const {return fIsSecondaryFromWeakDecay;}
		Bool_t	IsSecondaryFromMaterialDecay() const {return fIsSecondaryFromMaterialDecay;}

	private: 
		Float_t				fMCPt;
		Float_t				fMCEta;
		Float_t				fMCY;
		Float_t				fMCPhi;

		Bool_t				fIsMCPion;
		Bool_t				fIsMCKaon;
		Bool_t				fIsMCProton;
		Bool_t				fIsMCDeuterium;
		Bool_t				fIsTOFMatch;
		Bool_t				fIsPhysicalPrimary;
		Bool_t				fIsSecondaryFromWeakDecay;
		Bool_t				fIsSecondaryFromMaterialDecay;

       ClassDef(MCInfo, 1) // 

	};

private:
	// Counter for the number of constructor calls minus the number of destructor calls.
	static Int_t		fNInstances;

	static Int_t 		fNSpecies;
	Char_t 				fFlags;
	UInt_t				fFilterMap; 
	Float_t 			fPt;
	Float_t 			fEta;
	Float_t 			fPhi;
	Float_t 			fTOFsignalMinusExpected[3];
	Float_t				fTOFRandom[3];
	Float_t 			fTPCsignalMinusExpected[3];
	Float_t				fTOFnSigma[3];
	Float_t				fTOFnSigmaRandom[3];
	Float_t				fTPCnSigma[3];
	Float_t				fDCAxy;
	Float_t				fDCAz;
	Short_t				fTPCSignalN;		// Number of clusters used for dE/dx determination.
	Char_t				fStartTimeMask;
	Bool_t				fHasSPDHit;

	MCInfo*				fMCInfo;

	ClassDef(AliMiniTrackDiHadronPID, 6);

};

#endif
