/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Fits Templates
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include <iostream>
using namespace std;

#include "TBox.h"

#include "AliHistToolsDiHadronPID.h"
#include "AliFunctionsDiHadronPID.h"
#include "AliTemplateFitterDiHadronPID.h"
#include "AliLogDiHadronPID.h"

#include "Math/Factory.h"
//#include "Minuit2/Minuit2Minimizer.h"
#include "Math/Util.h"
#include "Math/Functor.h"

#include "TMath.h"
#include "TMatrixF.h"
#include "TMatrixD.h"
#include "TVectorF.h"
#include "TLegend.h"

ClassImp(AliTemplateFitterDiHadronPID);

// -----------------------------------------------------------------------
AliTemplateFitterDiHadronPID::AliTemplateFitterDiHadronPID():
	TObject(),
	fData(0x0),
	fPeakTemplates(0x0),
	fMismatchTemplate(0x0),
	fReady(kFALSE),
	fSettings(AliSettingsDiHadronPID::Instance()),
	fMinimizer(0x0),
	fMassAssumption(AliSettingsDiHadronPID::kUnknownSpecies),
	fMeanPt(-999.),
	fFixedMismatchFraction(-999.),
	fFitRangeMinBinTOF(-1),
	fFitRangeMaxBinTOF(-1),
	fFitRangeMinBinTPC(-1),
	fFitRangeMaxBinTPC(-1),	
	fFitStatus(-9),
	fFitParameters(0x0),
	fFitErrors(0x0) 

{

	/** 
	 * Default Constructor
	 */
	AliLogDiHadronPID::Debug2("Default Constructor called.", __func__, ClassName());

	// Create arrays for the fit parameters and errors.
	fFitParameters = new double[fSettings->GetNSpecies() + 1];
	fFitErrors = new double[fSettings->GetNSpecies() + 1];
	for (Int_t iSpecies = 0; iSpecies <= fSettings->GetNSpecies(); iSpecies++) {
		fFitParameters[iSpecies] = 0.;
		fFitErrors[iSpecies] = 0.;
	}

	// Initialize the fitter.
	Init();

}

// -----------------------------------------------------------------------
AliTemplateFitterDiHadronPID::~AliTemplateFitterDiHadronPID() {
	
	/**
	 * Destructor
	 */
	AliLogDiHadronPID::Debug2("Destructor called.", __func__, ClassName());

	if (fMinimizer) {delete fMinimizer; fMinimizer = 0x0;}
	if (fFitParameters) {delete fFitParameters; fFitParameters = 0x0;}
	if (fFitErrors) {delete fFitErrors; fFitErrors = 0x0;}

}

// -----------------------------------------------------------------------
Bool_t AliTemplateFitterDiHadronPID::Init() {

	/**
	 * Initialize the fitter.
	 */

	// Create a minimizer.
	fMinimizer = ROOT::Math::Factory::CreateMinimizer("Minuit", "Migrad");
	//fMinimizer = new ROOT::Minuit2::Minuit2Minimizer(ROOT::Minuit2::kMigrad);
	fMinimizer->SetMaxFunctionCalls(1000000);
	fMinimizer->SetMaxIterations(20000);
	fMinimizer->SetTolerance(0.01); 
	fMinimizer->SetValidError(1);	

	// Set the negll function as the function to-be-minimized.
	ROOT::Math::Functor* minimizer_func = new ROOT::Math::Functor(this, &AliTemplateFitterDiHadronPID::negll, fSettings->GetNSpecies() + 1);/*}*/
	fMinimizer->SetFunction(*minimizer_func);
	fMinimizer->SetErrorDef(0.5);  // Needed to get the correct error estimates when performing a LL minimization.

	if (fMinimizer) {return kTRUE;}

	return kFALSE;

}

// -----------------------------------------------------------------------
void AliTemplateFitterDiHadronPID::UpdateFitRegion() {

	/** 
	 * Calculates the fit region based on the included peaks and the settings.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	Double_t fitRegionTOFMin = 0., fitRegionTOFMax = 0., fitRegionTPCMin = 0., fitRegionTPCMax = 0.;

	// Loop over species.
	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {

		AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;

		if (fSettings->IsPeakIncludedInFit(fMassAssumption, currentSpecies, fMeanPt)) {
			fitRegionTOFMin = TMath::Min(fitRegionTOFMin, AliFunctionsDiHadronPID::EstimateTOFPosition(fMeanPt, 0.4, fMassAssumption, (AliSettingsDiHadronPID::Species)iSpecies));
			fitRegionTOFMax = TMath::Max(fitRegionTOFMax, AliFunctionsDiHadronPID::EstimateTOFPosition(fMeanPt, 0.4, fMassAssumption, (AliSettingsDiHadronPID::Species)iSpecies));
			fitRegionTPCMin = TMath::Min(fitRegionTPCMin, AliFunctionsDiHadronPID::EstimateTPCPosition(fMeanPt, 0.4, fMassAssumption, (AliSettingsDiHadronPID::Species)iSpecies));
			fitRegionTPCMax = TMath::Max(fitRegionTPCMax, AliFunctionsDiHadronPID::EstimateTPCPosition(fMeanPt, 0.4, fMassAssumption, (AliSettingsDiHadronPID::Species)iSpecies));			
		}

	}

	// Add extra margin around the peak centers.
	fitRegionTOFMin -= fSettings->GetFitRegionTOFmin();
	fitRegionTOFMax += fSettings->GetFitRegionTOFmax();
	fitRegionTPCMin -= fSettings->GetFitRegionTPCmin();
	fitRegionTPCMax += fSettings->GetFitRegionTPCmax();

	// Find the bin corresponding to the edges.
	fFitRangeMinBinTOF = fData->GetXaxis()->FindBin(fitRegionTOFMin);
	fFitRangeMaxBinTOF = fData->GetXaxis()->FindBin(fitRegionTOFMax);
	fFitRangeMinBinTPC = fData->GetYaxis()->FindBin(fitRegionTPCMin);
	fFitRangeMaxBinTPC = fData->GetYaxis()->FindBin(fitRegionTPCMax);

}

// -----------------------------------------------------------------------
Int_t AliTemplateFitterDiHadronPID::Fit() {

	/** 
	 * Performs a log likelihood fit (chi^2 currently not completely implemented).
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	if (!fReady) {
		AliLogDiHadronPID::Error(Form("Ready flag is false, first run Set()."), __func__, ClassName());
		return -999;
	}
	// Set yield of the peaks that are incuded as limited variables, others are fixed to 0.
	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {
		
		AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
		if (fSettings->IsPeakIncludedInFit(fMassAssumption, currentSpecies, fMeanPt)) {
			fMinimizer->SetLimitedVariable(iSpecies, fSettings->GetSpeciesName(currentSpecies), fFitParameters[iSpecies], 0.01, 10e-9, 10. * fData->GetEntries());	
		} else {
			fMinimizer->SetFixedVariable(iSpecies, fSettings->GetSpeciesName(currentSpecies), 0.);
		}

	}

	// Fix the mismatch yield.
	fMinimizer->SetFixedVariable(fSettings->GetNSpecies(), "TOF Mismatch", fFitParameters[fSettings->GetNSpecies()]);

	// Perform minimization.
	fMinimizer->Minimize();
	fMinimizer->Hesse();
	fFitStatus = (Int_t)fMinimizer->Status();

	// Copy errors, if properly calculated.
	if (fMinimizer->IsValidError()) {SetFitErrors(fMinimizer->Errors());}
	else {AliLogDiHadronPID::Warning(Form("Errors not valid..."), __func__, ClassName());}

	// Print current variables.
	//Print(0);
	return fFitStatus;

}

// -----------------------------------------------------------------------
void AliTemplateFitterDiHadronPID::Set(AliSettingsDiHadronPID::Species massAssumption, Double_t meanPt, 
		const TH2F* data, const TObjArray* peakTemplates, const TH2F* mismatchTemplate, Double_t fixedMismatchFraction) {

	/**
	 * This method resets the current internal parameters, and (re)sets them 
	 * appropriately, given the input data.
	 */

	fData = data;
	fPeakTemplates = peakTemplates;
	fMismatchTemplate = mismatchTemplate;

	fMassAssumption = massAssumption;
	fMeanPt = meanPt;
	fFixedMismatchFraction = fixedMismatchFraction;
	fFitStatus = -9;

	for (Int_t iSpecies = 0; iSpecies < (fSettings->GetNSpecies() + 1); iSpecies++) {
		fFitParameters[iSpecies] = 0.;
		fFitErrors[iSpecies] = 0.;
	}

	// Estimates the parameters (including mismatches).
	EstimateParameters();

	// Obtain the fit range (bins) from the settings.
	UpdateFitRegion();

	fReady = kTRUE;

}

// -----------------------------------------------------------------------
void AliTemplateFitterDiHadronPID::Print(Option_t*) const {
	
	/**
	 * Print info about the class.
	 */
	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	cout << "--------------------------------------------" << endl;
	cout << " Template fitter." << endl;
	cout << "--------------------------------------------" << endl;
	cout << " Ready? " << (fReady ? "YES" : "NO") << endl;
	cout << " Total particles: " << (fData ? fData->GetEntries() : -999.) << endl;
	cout << " Fit status: " << fFitStatus << endl;
	
	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {
		AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
		Bool_t isIncluded = fSettings->IsPeakIncludedInFit(fMassAssumption, currentSpecies, fMeanPt);
		cout << " " << fSettings->GetSpeciesName(currentSpecies) << ": " << fFitParameters[iSpecies] << " +/- " << fFitErrors[iSpecies] << ((isIncluded && fFitParameters[iSpecies] > 10e-10) ? Form(" (S/sqrtN = %f)", fFitErrors[iSpecies] / TMath::Sqrt(fFitParameters[iSpecies])) : " (NOT INCLUDED)") << endl;
	}
	
	cout << " Mismatches: " << fFitParameters[fSettings->GetNSpecies()] << " ± " << fFitErrors[fSettings->GetNSpecies()] << endl;
	cout << endl;

	cout << " Data Hist: " << (fData ? "OK" : "N/A") << endl;
	if (fPeakTemplates) {
		for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {
			cout << " " << fSettings->GetSpeciesName((AliSettingsDiHadronPID::Species)iSpecies) << " Template: " << (fPeakTemplates->At(iSpecies) ? "OK" : "N/A") << endl;
		}	
	} else {
		cout << " Peak Templates: N/A" << endl;
	}
	cout << " Mismatch Template: " << (fMismatchTemplate ? "OK" : "N/A") << endl;
	cout << "--------------------------------------------" << endl;

}

// -----------------------------------------------------------------------
void AliTemplateFitterDiHadronPID::PrintHessian() const {

	/**
	 * Print out the Hessian.
	 */
	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	if (!fMinimizer) {return;}

	const Int_t nPars = fMinimizer->NDim();
	double hessian[nPars * nPars];
	for (Int_t index = 0; index < nPars*nPars; index++) {hessian[index] = 0.;}

	fMinimizer->GetHessianMatrix(hessian);

	TMatrixD hessianMatrix(nPars, nPars, hessian);
	hessianMatrix.Print();

}

// -----------------------------------------------------------------------
TCanvas* AliTemplateFitterDiHadronPID::DrawFitData(TCanvas* cvs) {

	/**
	 * Draws the fit region and peaks on a 2D histogram.
	 * \param cvs If a canvas is provided, then it is used, otherwise a new canvas is created.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	if (cvs) {cvs->Clear();}
	TCanvas* canvas = (cvs ? cvs : TCanvas::MakeDefCanvas());
	canvas->SetLogz();

	// Draw a copy of the data.
	TH1* dataClone = fData->DrawCopy("colz");
	dataClone->SetDirectory(0);

	// Draw a Box for the fit region.
	const TAxis* tofAxis = fSettings->GetTOFAxis(fMassAssumption);
	const TAxis* tpcAxis = fSettings->GetTPCAxis(fMassAssumption);

	Double_t fitRegionTOFMin = tofAxis->GetBinCenter(fFitRangeMinBinTOF);
	Double_t fitRegionTOFMax = tofAxis->GetBinCenter(fFitRangeMaxBinTOF);
	Double_t fitRegionTPCMin = tpcAxis->GetBinCenter(fFitRangeMinBinTPC);
	Double_t fitRegionTPCMax = tpcAxis->GetBinCenter(fFitRangeMaxBinTPC);

	TBox* fitregion = new TBox(fitRegionTOFMin, fitRegionTPCMin, fitRegionTOFMax, fitRegionTPCMax);
	fitregion->SetLineColor(kBlack);
	fitregion->SetFillStyle(0);
	fitregion->SetLineWidth(2);
	fitregion->Draw("same");

	Double_t mismatchRegionTOFMin = tofAxis->GetBinCenter(fSettings->GetMismatchRegionTOFminBin(fMassAssumption, fMeanPt, 0.4));
	Double_t mismatchRegionTOFMax = tofAxis->GetBinCenter(fSettings->GetMismatchRegionTOFmaxBin(fMassAssumption, fMeanPt, 0.4));
	Double_t mismatchRegionTPCMin = tpcAxis->GetXmin();
	Double_t mismatchRegionTPCMax = tpcAxis->GetXmax();

	// Print the mismatch region.
	TBox* mmregion = new TBox(mismatchRegionTOFMin, mismatchRegionTPCMin, mismatchRegionTOFMax, mismatchRegionTPCMax);
	mmregion->SetLineColor(kGray);
	mmregion->SetFillStyle(0);
	mmregion->SetLineWidth(2);
	mmregion->Draw("same");
	//legend->AddEntry(mmregion, "Mismatch Region", "F");

	return canvas;

}

// -----------------------------------------------------------------------
TCanvas* AliTemplateFitterDiHadronPID::DrawTOFProjection(TCanvas* cvs) {

	/**
	 * Draws a TOF projection of the fit.
	 * \param cvs If a canvas is provided, then it is used, otherwise a new canvas is created.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	if (cvs) {cvs->Clear();}
	TCanvas* canvas = (cvs ? cvs : TCanvas::MakeDefCanvas());
	
	canvas->SetLogy();

	// Make a Legend.
	TLegend* legend = new TLegend(0.75,0.70,0.92,0.92);
	Double_t nLegendEntries = 3.;

	// Data projection.
	TH1F* dataProj = (TH1F*)fData->ProjectionX("TOFdata", fFitRangeMinBinTPC, fFitRangeMaxBinTPC);
	dataProj->SetLineColor(kBlack);
	dataProj->SetLineWidth(2);
	dataProj->Sumw2();
	legend->AddEntry(dataProj->DrawCopy(), "Data");

	// Mismatch projection.
	TH1F* mismatchProj = (TH1F*)fMismatchTemplate->ProjectionX("TOFProj_mm", fFitRangeMinBinTPC, fFitRangeMaxBinTPC);
	mismatchProj->SetLineColor(kGray + 2);
	mismatchProj->SetLineWidth(2);
	mismatchProj->Scale(fFitParameters[fSettings->GetNSpecies()]);
	legend->AddEntry(mismatchProj->DrawCopy("same"), "TOF Mismatch");

	TH1F* sumProj = 0x0;

	// Loop over all species, and draw them if they're included in the fit.
	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {
		
		AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
		if (fSettings->IsPeakIncludedInFit(fMassAssumption, currentSpecies, fMeanPt)) {

			TH2F* currentTemplate = (TH2F*)fPeakTemplates->At(iSpecies);
			if (!currentTemplate) {
				AliLogDiHadronPID::Error(Form("%s peak is included, but no template was found.", fSettings->GetSpeciesName(currentSpecies)), __func__, ClassName());
				continue;
			}

			// Create a projection.
			TH1F* projection = (TH1F*)currentTemplate->ProjectionX(Form("TOFProj_%s", fSettings->GetSpeciesName(currentSpecies)), fFitRangeMinBinTPC, fFitRangeMaxBinTPC);
			projection->SetLineColor(fSettings->GetSpeciesColor(currentSpecies));
			projection->Scale(fFitParameters[iSpecies]);
			
			legend->AddEntry(projection->DrawCopy("same"), fSettings->GetSpeciesName(currentSpecies));
			nLegendEntries += 1.;

			// Add to the sum.
			if (!sumProj) {
				sumProj = new TH1F(*projection);
				sumProj->SetDirectory(0);
				sumProj->SetTitle("TOFtotal");
				sumProj->SetLineColor(kViolet);
			} else {
				sumProj->Add(projection);
			}

		}
	
	}

	sumProj->Add(mismatchProj);

	legend->AddEntry(sumProj->DrawCopy("same"), "Total Fit");
	legend->SetY1(0.9 - nLegendEntries * 0.04);
	legend->Draw();	

	return canvas;

}

// -----------------------------------------------------------------------
TCanvas* AliTemplateFitterDiHadronPID::DrawTPCProjection(TCanvas* cvs) {

	/**
	 * Draws a TPC projection of the fit.
	 * \param cvs If a canvas is provided, then it is used, otherwise a new canvas is created.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	if (cvs) {cvs->Clear();}
	TCanvas* canvas = (cvs ? cvs : TCanvas::MakeDefCanvas());
	canvas->SetLogy();

	// Make a Legend.
	TLegend* legend = new TLegend(0.75,0.70,0.92,0.92);
	Double_t nLegendEntries = 3.;

	// Data projection.
	TH1F* dataProj = (TH1F*)fData->ProjectionY("TPCdata", fFitRangeMinBinTOF, fFitRangeMaxBinTOF);
	dataProj->SetLineColor(kBlack);
	dataProj->SetLineWidth(2);
	dataProj->Sumw2();
	legend->AddEntry(dataProj->DrawCopy(), "Data");

	// Mismatch projection.
	TH1F* mismatchProj = (TH1F*)fMismatchTemplate->ProjectionY("TPCmm", fFitRangeMinBinTOF, fFitRangeMaxBinTOF);
	mismatchProj->SetLineColor(kGray + 2);
	mismatchProj->SetLineWidth(2);
	mismatchProj->Scale(fFitParameters[fSettings->GetNSpecies()]);
	legend->AddEntry(mismatchProj->DrawCopy("same"), "TOF Mismatch");

	TH1F* sumProj = 0x0;

	// Loop over all species, and draw them if they're included in the fit.
	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {
		
		AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
		if (fSettings->IsPeakIncludedInFit(fMassAssumption, currentSpecies, fMeanPt)) {

			TH2F* currentTemplate = (TH2F*)fPeakTemplates->At(iSpecies);
			if (!currentTemplate) {
				AliLogDiHadronPID::Error(Form("%s peak is included, but no template was found.", fSettings->GetSpeciesName(currentSpecies)), __func__, ClassName());
				continue;
			}

			// Create a projection.
			TH1F* projection = (TH1F*)currentTemplate->ProjectionY(Form("TPCProj_%s", fSettings->GetSpeciesName(currentSpecies)), fFitRangeMinBinTOF, fFitRangeMaxBinTOF);
			projection->SetLineColor(fSettings->GetSpeciesColor(currentSpecies));
			projection->Scale(fFitParameters[iSpecies]);

			legend->AddEntry(projection->DrawCopy("same"), fSettings->GetSpeciesName(currentSpecies));
			nLegendEntries += 1.;

			// Add to the sum.
			if (!sumProj) {
				sumProj = new TH1F(*projection);
				sumProj->SetDirectory(0);
				sumProj->SetTitle("TOFtotal");
				sumProj->SetLineColor(kViolet);
			} else {
				sumProj->Add(projection);
			}

		}
	
	}

	sumProj->Add(mismatchProj);

	legend->AddEntry(sumProj->DrawCopy("same"), "Total Fit");
	legend->SetY1(0.9 - nLegendEntries * 0.04);
	legend->Draw();	

	return canvas;

}

// -----------------------------------------------------------------------
TCanvas* AliTemplateFitterDiHadronPID::DrawNLL(TCanvas* cvs) {

	/**
	 * Draws a histogram with the neg LogLikelihood values for each bin.
	 * \param cvs If a canvas is provided, then it is used, otherwise a new canvas is created.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	if (cvs) {cvs->Clear();}
	TCanvas* canvas = (cvs ? cvs : TCanvas::MakeDefCanvas());

	TH2F* hNLL = new TH2F(*fData);
	hNLL->SetDirectory(0);

	for (Int_t iBinTOF = 1; iBinTOF <= fData->GetNbinsX(); iBinTOF++) {
		for (Int_t iBinTPC = 1; iBinTPC <= fData->GetNbinsY(); iBinTPC++) {
			
			Double_t dataval = fData->GetBinContent(iBinTOF, iBinTPC);
			Double_t modelval = 0.;

			// Loop over all species, and draw them if they're included in the fit.
			for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {
		
				AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
				if (fSettings->IsPeakIncludedInFit(fMassAssumption, currentSpecies, fMeanPt)) {

					modelval += fFitParameters[iSpecies] * ((TH2F*)fPeakTemplates->At(iSpecies))->GetBinContent(iBinTOF, iBinTPC);

				}
			
			}

			if (fMismatchTemplate) {modelval += (fFitParameters[fSettings->GetNSpecies()] * (fMismatchTemplate->GetBinContent(iBinTOF, iBinTPC)));}

			Double_t bin_nll = modelval - dataval * ROOT::Math::Util::EvalLog(modelval);

			hNLL->SetBinContent(iBinTOF, iBinTPC, bin_nll);

		}			
	}

	hNLL->Draw("colz");

	return canvas;

}

// -----------------------------------------------------------------------
TCanvas* AliTemplateFitterDiHadronPID::DrawChi2(TCanvas* cvs) {

	/**
	 * Draws a histogram with the chi^2 values for each bin.
	 * \param cvs If a canvas is provided, then it is used, otherwise a new canvas is created.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	if (cvs) {cvs->Clear();}
	TCanvas* canvas = (cvs ? cvs : TCanvas::MakeDefCanvas());

	TH2F* hChi2 = new TH2F(*fData);
	hChi2->SetDirectory(0);

	for (Int_t iBinTOF = 1; iBinTOF <= fData->GetNbinsX(); iBinTOF++) {
		for (Int_t iBinTPC = 1; iBinTPC <= fData->GetNbinsY(); iBinTPC++) {
			
			Double_t dataval = fData->GetBinContent(iBinTOF, iBinTPC);
			if (dataval < fSettings->GetMinEntriesForChi2()) {continue;}

			Double_t modelval = 0.;

			// Loop over all species, and draw them if they're included in the fit.
			for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {
		
				AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
				if (fSettings->IsPeakIncludedInFit(fMassAssumption, currentSpecies, fMeanPt)) {

					modelval += fFitParameters[iSpecies] * ((TH2F*)fPeakTemplates->At(iSpecies))->GetBinContent(iBinTOF, iBinTPC);

				}
			
			}

			if (fMismatchTemplate) {modelval += (fFitParameters[fSettings->GetNSpecies()] * (fMismatchTemplate->GetBinContent(iBinTOF, iBinTPC)));}

			Double_t bin_chi = (modelval - dataval)/TMath::Sqrt(dataval);

			hChi2->SetBinContent(iBinTOF, iBinTPC, bin_chi*bin_chi);

		}			
	}

	hChi2->Draw("colz");

	return canvas;

}

// -----------------------------------------------------------------------
void AliTemplateFitterDiHadronPID::EstimateParameters() {

	/** 
	 * Provide a crude estimate of the fit result. The mismatch yield is estimated by 
	 * searching the "mismatch region", which is specificied in AliSettingsDiHadronPID. If at least 
	 * 10 (arbitrary number!) hits are present in this region, then the mismatch fraction is calculated by
	 * comparing the mismatch template with those 10 or more hits. 
	 *
	 * The peak yields are estimated as follows: for pions, kaons and protons, the maximum of the template
	 * is matched with the corresponding bin content in the data. For other species, 100. is assumed (more
	 * stable, as these species often have very low abundances).
	 */

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	if (!fData) {
		AliLogDiHadronPID::Error(Form("No data histogram present."), __func__, ClassName());
		return;
	}

	// Estimating the mismatch yield. NOTE: For low statistics bins, the method of fixing the template to a 
	// certain region doesn't work very well, since it can happen that there are simply no mismatches in the "search region".
	// In that case it's probably better to take the mismatch fraction from the mixed event fit.
	const TAxis* axisTPC = fData->GetYaxis();

	Int_t minBinTOF = fSettings->GetMismatchRegionTOFminBin(fMassAssumption, fMeanPt, 0.4); // Assume that absAvgEta will not 
	Int_t maxBinTOF = fSettings->GetMismatchRegionTOFmaxBin(fMassAssumption, fMeanPt, 0.4); // change the result significantly.

	//cout << "TOF Bins: " << minBinTOF << " - " << maxBinTOF << endl;
	Double_t DataSum = 0, MismatchSum = 0;
	for (Int_t iBinTOF = minBinTOF; iBinTOF <= maxBinTOF; ++iBinTOF) {
		for (Int_t iBinTPC = 1; iBinTPC <= axisTPC->GetNbins(); ++iBinTPC) {
			DataSum += fData->GetBinContent(iBinTOF, iBinTPC);

			MismatchSum += fMismatchTemplate->GetBinContent(iBinTOF, iBinTPC);
		}
	}

	// Check if there are enough (arbitrarily put to 10) hits in the mismatch search region.
	if (DataSum > 10.) {
		fFitParameters[fSettings->GetNSpecies()] = DataSum / MismatchSum;
		AliLogDiHadronPID::Info(Form("Found %f mismatch hits, estimating mismatch yield to %f", DataSum, (DataSum/MismatchSum)), __func__, ClassName());
		
	}
	else if (fFixedMismatchFraction > 0.) {
		fFitParameters[fSettings->GetNSpecies()] = fFixedMismatchFraction * AliHistToolsDiHadronPID::GetSumHist(fData);
		AliLogDiHadronPID::Info(Form("Not enough hits in mismatch search region: setting mismatch fraction to user defined fraction: %f.", fFixedMismatchFraction), __func__, ClassName());
	} else {
		fFitParameters[fSettings->GetNSpecies()] = 0.;
		AliLogDiHadronPID::Warning(Form("Not enough hits in mismatch search region to determine mismatch fraction. It's best to fix the fraction yourself. Putting mismatches to zero."), __func__, ClassName());
	}

	// Estimating the particle yields, by looking at the template's maximum bin (for pi/ka/pr), or to 100 (other species).
	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {

		AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
		if (!(fSettings->IsPeakIncludedInFit(fMassAssumption, currentSpecies, fMeanPt))) {
			fFitParameters[iSpecies] = 0.;
			continue;
		}

		TH2F* currentTemplate = (TH2F*)fPeakTemplates->At(iSpecies);
		if (!currentTemplate) {
			AliLogDiHadronPID::Error(Form("No template found for: %s, but peak was included in fit...", fSettings->GetSpeciesName(currentSpecies)), __func__, ClassName());
			continue;
		}

		if (currentSpecies == AliSettingsDiHadronPID::kPion ||
			currentSpecies == AliSettingsDiHadronPID::kKaon ||
			currentSpecies == AliSettingsDiHadronPID::kProton) {

			fFitParameters[iSpecies] = fData->GetBinContent(currentTemplate->GetMaximumBin()) / currentTemplate->GetBinContent(currentTemplate->GetMaximumBin());
//cout << "Species: " << iSpecies << " Max bin: " << currentTemplate->GetMaximumBin() << " Content in maximum data bin: " << fData->GetBinContent(currentTemplate->GetMaximumBin()) << " Content in max template bin: " << currentTemplate->GetBinContent(currentTemplate->GetMaximumBin()) << " fit par set to: " << fFitParameters[iSpecies] << endl;
            if (fFitParameters[iSpecies] < 1.) {fFitParameters[iSpecies] = 1./currentTemplate->GetBinContent(currentTemplate->GetMaximumBin());}

		} else {

			fFitParameters[iSpecies] = 100.;

		}
	
	}

}

// -----------------------------------------------------------------------
void AliTemplateFitterDiHadronPID::SetFitParameters(const double* pars) {

	// Note that the number of fit parameters is lower than the number of species.
	if (!fMinimizer) {
		AliLogDiHadronPID::Error(Form("Minimizer was not initialized."), __func__, ClassName());
		return;
	}

	for (Int_t iPar = 0; iPar <= (fSettings->GetNSpecies() + 1); iPar++) {
		fFitParameters[iPar] = pars[iPar];
	}

}

// -----------------------------------------------------------------------
void AliTemplateFitterDiHadronPID::SetFitErrors(const double* errors) {

	// Not safe.
	for (Int_t iPar = 0; iPar < (fSettings->GetNSpecies() + 1); iPar++) {
		fFitErrors[iPar] = errors[iPar];
	}

}

// -----------------------------------------------------------------------
double AliTemplateFitterDiHadronPID::negll(const double* pars) {

	/** 
	 *Calculates the neg log likelihood.
	 */
	SetFitParameters(pars);

	Double_t nll = 0.;

	for (Int_t iBinTOF = fFitRangeMinBinTOF; iBinTOF <= fFitRangeMaxBinTOF; iBinTOF++) {
		for (Int_t iBinTPC = fFitRangeMinBinTPC; iBinTPC <= fFitRangeMaxBinTPC; iBinTPC++) {
			
			Double_t dataval = fData->GetBinContent(iBinTOF, iBinTPC);
			Double_t modelval = 0.;

			// Loop over all species, and draw them if they're included in the fit.
			for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {
		
				AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
				if (fSettings->IsPeakIncludedInFit(fMassAssumption, currentSpecies, fMeanPt)) {

					modelval += fFitParameters[iSpecies] * ((TH2F*)fPeakTemplates->At(iSpecies))->GetBinContent(iBinTOF, iBinTPC);

				}
			
			}

			if (fMismatchTemplate) {modelval += (fFitParameters[fSettings->GetNSpecies()] * (fMismatchTemplate->GetBinContent(iBinTOF, iBinTPC)));}

			nll += modelval - dataval * ROOT::Math::Util::EvalLog(modelval);

		}			
	}
	
	return nll;

}
// -----------------------------------------------------------------------
double AliTemplateFitterDiHadronPID::chi2(const double* pars) {

	/**
	 * Calculates the chi^2.
	 */
	SetFitParameters(pars);

	Double_t chisq = 0.;
	Int_t minEntriesForChi2 = fSettings->GetMinEntriesForChi2();

	for (Int_t iBinTOF = fFitRangeMinBinTOF; iBinTOF <= fFitRangeMaxBinTOF; iBinTOF++) {
		for (Int_t iBinTPC = fFitRangeMinBinTPC; iBinTPC <= fFitRangeMaxBinTPC; iBinTPC++) {

			Double_t dataval = fData->GetBinContent(iBinTOF, iBinTPC);
			if (dataval < minEntriesForChi2) {continue;}
			Double_t modelval = 0.;

			// Loop over all species, and draw them if they're included in the fit.
			for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {
		
				AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
				if (fSettings->IsPeakIncludedInFit(fMassAssumption, currentSpecies, fMeanPt)) {

					modelval += fFitParameters[iSpecies] * ((TH2F*)fPeakTemplates->At(iSpecies))->GetBinContent(iBinTOF, iBinTPC);

				}
			
			}

			if (fMismatchTemplate) {modelval += (fFitParameters[fSettings->GetNSpecies()] * (fMismatchTemplate->GetBinContent(iBinTOF, iBinTPC)));}

			Double_t bin_chi = (modelval - dataval)/TMath::Sqrt(dataval);
			chisq += (bin_chi * bin_chi);

		}			
	}

	return chisq;

}
