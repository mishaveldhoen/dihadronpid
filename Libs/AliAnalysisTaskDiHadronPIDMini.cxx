/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  This analysis task fills histograms with PID information of tracks 
//  associated to a high p_T trigger.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include <iostream>
using std::cout;
using std::endl;

// Basic Includes
#include "TFile.h"
#include "TChain.h"

// Manager/ Handler
#include "AliAnalysisManager.h"
#include "AliInputEventHandler.h"

// PID includes.
#include "AliPIDResponse.h"

// AOD includes.
#include "AliAODEvent.h"
#include "AliAODTrack.h"
#include "AliAODHandler.h"
#include "AliAODVertex.h"
#include "AliAODInputHandler.h"
#include "AliAODMCParticle.h"
#include "AliAODMCHeader.h"
#include "AliMultSelection.h"

// Additional includes.
#include "AliLogDiHadronPID.h"
#include "AliMiniTrackDiHadronPID.h"
#include "AliHistToolsDiHadronPID.h"
#include "AliFunctionsDiHadronPID.h"

// AnalysisTask headers.
#include "AliAnalysisTaskDiHadronPIDMini.h"

ClassImp(AliAnalysisTaskDiHadronPIDMini);

// -----------------------------------------------------------------------
AliAnalysisTaskDiHadronPIDMini::AliAnalysisTaskDiHadronPIDMini():
	AliAnalysisTaskSE(),
	fPIDResponse(0x0),
	fEventTrigger(AliVEvent::kMB),
	fMaxVertexZ(10.),
	fCentralityEstimator("V0M"),
	fMinCentrality(10.),
	fMaxCentrality(0.),
	fCutOnContributorsOrSPDVertex(kFALSE),
	fMinPt(0.),
	fMaxPt(100.),
	fMaxAbsEta(1.),
	fMaxAbsMCY(0.5),
	fFilterMask(0),
	fCurrentAODEvent(0x0),
	fCurrentMiniEvent(0x0),
	fEventTree(0x0),
	fEventQAList(0x0),
	fEventCuts(0x0),
	fPrimVtxType(0x0),
	fOfflineTrigger(0x0),
	fTrackQAList(0x0),
	fNClustersTPC(0x0),
	fNFindableClustersTPC(0x0),
	fNCrossedRowsTPC(0x0),
	fNHitsSPD(0x0),
	fTrackFlags(0x0),
	fNclsTPCvsTPCsignalN(0x0),
	fTOFsignalVsHMPIDnSigma(0x0),
	fMCParticleQAList(0x0),
	fTrackLabelSign(0x0),
	fNMultipleRecoTracks(0x0),
	fBeamType(AliMiniEventDiHadronPID::kPbPb),
	fIsMC(kFALSE),
	fCalculateMismatch(kTRUE),
	fUseMismatchFileFromHomeDir(kFALSE),
	fT0Fill(0x0),
	fLvsEta(0x0),
	fLvsEtaProjections(0x0)	

{

	//
	// Default Constructor.
	//

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

}

// -----------------------------------------------------------------------
AliAnalysisTaskDiHadronPIDMini::AliAnalysisTaskDiHadronPIDMini(const char* name):
	AliAnalysisTaskSE(name),
	fPIDResponse(0x0),
	fEventTrigger(AliVEvent::kMB),
	fMaxVertexZ(10.),
	fCentralityEstimator("V0M"),
	fMinCentrality(10.),
	fMaxCentrality(0.),
	fCutOnContributorsOrSPDVertex(kFALSE),
	fMinPt(0.),
	fMaxPt(100.),
	fMaxAbsEta(1.),
	fMaxAbsMCY(0.5),
	fFilterMask(0),
	fCutOnTOFHit(kFALSE),
	fCutOnTOFHitBelowPt(4.),
	fCurrentAODEvent(0x0),
	fCurrentMiniEvent(0x0),
	fEventTree(0x0),
	fEventQAList(0x0),
	fEventCuts(0x0),
	fPrimVtxType(0x0),
	fOfflineTrigger(0x0),
	fTrackQAList(0x0),
	fNClustersTPC(0x0),
	fNFindableClustersTPC(0x0),
	fNCrossedRowsTPC(0x0),
	fNHitsSPD(0x0),
	fTrackFlags(0x0),
	fNclsTPCvsTPCsignalN(0x0),
	fTOFsignalVsHMPIDnSigma(0x0),	
	fMCParticleQAList(0x0),
	fTrackLabelSign(0x0),
	fNMultipleRecoTracks(0x0),	
	fBeamType(AliMiniEventDiHadronPID::kPbPb),
	fIsMC(kFALSE),
	fCalculateMismatch(kTRUE),
	fUseMismatchFileFromHomeDir(kFALSE),
	fT0Fill(0x0),
	fLvsEta(0x0),
	fLvsEtaProjections(0x0)

{

	//
	// Named Constructor.
	//

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	DefineOutput(1, TTree::Class());
	DefineOutput(2, TList::Class());
	DefineOutput(3, TList::Class());
	DefineOutput(4, TList::Class());


}

// -----------------------------------------------------------------------
AliAnalysisTaskDiHadronPIDMini::~AliAnalysisTaskDiHadronPIDMini() {

	//
	// Destructor.
	//

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	if (fEventTree) {delete fEventTree; fEventTree = 0x0;}
	if (fCurrentMiniEvent) {delete fCurrentMiniEvent; fCurrentMiniEvent = 0x0;}

}

// -----------------------------------------------------------------------
void AliAnalysisTaskDiHadronPIDMini::UserCreateOutputObjects() {

	//
	// Create Output objects.
	//

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	AliAnalysisManager* manager = AliAnalysisManager::GetAnalysisManager();
	if (!manager) {AliFatal("Could not obtain analysis manager.");}	
	AliInputEventHandler* inputHandler = dynamic_cast<AliInputEventHandler*> (manager->GetInputEventHandler());
	if (!inputHandler) {AliFatal("Could not obtain input handler.");}	

	// Getting the pointer to the PID response object.
	fPIDResponse = inputHandler->GetPIDResponse();	
	if (!fPIDResponse) {AliFatal("Could not obtain PID response.");}
	if (fIsMC) {
		cout << endl;
		cout << " ************************************************* " << endl;
		cout << "  PID Response is" << (fPIDResponse->IsTunedOnData() ? " " : " NOT ") << "tuned on Data" << endl;
		cout << " ************************************************* " << endl;
		cout << endl;	
	}

	// Open the output file for the TTree container, so that baskets can be written to file instead of stored in memory.
	OpenFile(1);

	// Creating the event buffer and event tree.
	fEventTree = new TTree("eventTree", "eventTree");

	// Creating a branch also creates an instance of the mini event, which is pointed to by fCurrentMiniEvent.
	fCurrentMiniEvent = 0x0;
	fEventTree->Branch("events", "AliMiniEventDiHadronPID", &fCurrentMiniEvent);
	PostData(1, fEventTree);

	// Create a TList for the Event QA histograms. These histograms are made of parameters that
	// we don't save in the event tree, i.e., the information that's lost.
	fEventQAList = CreateEventQAHistos();
	PostData(2, fEventQAList);

	fTrackQAList = CreateTrackQAHistos();
	PostData(3, fTrackQAList);

	if (fIsMC) {
		fMCParticleQAList = CreateMCParticleQAHistos();
		PostData(4, fMCParticleQAList);
	}

	// Load external TOF histograms if flag is set.
	if (fCalculateMismatch) {LoadExtMismatchHistos();}


}

// -----------------------------------------------------------------------
void AliAnalysisTaskDiHadronPIDMini::UserExec(Option_t*) {

	//
	// Main Loop.
	//

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	// Input Current Event.
	fCurrentAODEvent = dynamic_cast<AliAODEvent*>(InputEvent());
	if (!fCurrentAODEvent) AliFatal("No Event Found!");

	// Check if the event is selected.
	if (!IsEventSelected(fCurrentAODEvent)) {return;}

	// Reset the event buffer from the tree (i.e., call delete on the tracks array).
	fCurrentMiniEvent->ResetEvent();

	// Fill the event buffer with the parameters from the AOD event.
	fCurrentMiniEvent->CopyPars(fCurrentAODEvent, fBeamType, fCentralityEstimator.Data());

	// Check the number of mini events and mini tracks on memory. Note that this
	// should be 1 event (created by the tree un UserCreateOutputObjects()), and 0 tracks.
	//cout << " NMiniEvents: " << AliMiniEventDiHadronPID::GetNInstances() << " NMiniTracks: " << AliMiniTrackDiHadronPID::GetNInstances() << endl; 

	// Fill data
	if (fIsMC) {UserExecMC();}
	else {UserExecData();}

	// Tell the current mini event that we're done (shrink to minimum size).
	fCurrentMiniEvent->Done();

	// Update the tree.
	fEventTree->Fill();

	// See whether the tree baskets are on disc or in memory.
	// fEventTree->Print(); 

	PostData(1, fEventTree);
	PostData(2, fEventQAList);
	PostData(3, fTrackQAList);
	if (fIsMC) {PostData(4, fMCParticleQAList);}

}

// -----------------------------------------------------------------------
void AliAnalysisTaskDiHadronPIDMini::UserExecData() {

	// Loop over tracks.
	for (Int_t iTrack = 0; iTrack < fCurrentAODEvent->GetNumberOfTracks(); iTrack++) {

		// Get a ptr to the current AOD track, and check if it's selected.
		AliAODTrack* currentAODTrack = (AliAODTrack*)fCurrentAODEvent->GetTrack(iTrack);

		if (!IsTrackSelected(currentAODTrack)) {continue;}

		// Get a ptr to the curent mini track.
		AliMiniTrackDiHadronPID* currentMiniTrack = fCurrentMiniEvent->NextParticle();

		// Add a random hit to the track (for TOF mismatches).
		Double_t rndHitTime = -1.e21;
		if (fCalculateMismatch) {rndHitTime = GenerateRandomHit(currentAODTrack->Eta());}

		// Copy the relevant parameters from the aodTrack to the miniTrack.
		currentMiniTrack->CopyPars(currentAODTrack, fCurrentAODEvent, fPIDResponse, fIsMC, fCalculateMismatch, rndHitTime);

	}

}

// -----------------------------------------------------------------------
void AliAnalysisTaskDiHadronPIDMini::UserExecMC() {

	// 	Create MCArray and Objectarray;
	TClonesArray* mcArray = 0x0;
	TObjArray* mcArrayLink = 0x0;

	mcArray = dynamic_cast<TClonesArray*>(fCurrentAODEvent->FindListObject(AliAODMCParticle::StdBranchName()));
	mcArrayLink = new TObjArray(mcArray->GetEntriesFast());

	// Loop over MC particles.
	for (Int_t iParticle = 0; iParticle < mcArray->GetEntriesFast(); iParticle++) {

		// Get the MCParticle object.
		AliAODMCParticle* currentAODMCParticle = (AliAODMCParticle*) mcArray->At(iParticle);
		
		// Check for very broad MC cuts (against bloating with unnecessary tracks).
		if (!IsMCParticleSelected(currentAODMCParticle)) {continue;}
		
		// Construct miniTrack in the Clones array to store the MC info at.
		AliMiniTrackDiHadronPID* currentMiniTrackMC = fCurrentMiniEvent->NextParticle();

		// Get mother from array as we need this for secondary information for the track.
		AliAODMCParticle* currentAODMCMother = 0x0;
		if (currentAODMCParticle->GetMother() >= 0) {currentAODMCMother = (AliAODMCParticle*)mcArray->At(TMath::Abs(currentAODMCParticle->GetMother()));}

		// Fill minitrack with MC info.
		currentMiniTrackMC->CopyPars(currentAODMCParticle, currentAODMCMother);

		// Fill linker array.
		mcArrayLink->AddAt(currentMiniTrackMC, iParticle);
	}

	// Loop over reconstructed tracks.
	for (Int_t iTrack = 0; iTrack < fCurrentAODEvent->GetNumberOfTracks(); iTrack++) {

		// Get a ptr to the current AOD track.
		AliAODTrack* currentAODTrack = (AliAODTrack*)fCurrentAODEvent->GetTrack(iTrack);

		// Check the filtermask of the reconstructed track.
		if (!(currentAODTrack->GetFilterMap() & fFilterMask)) {continue;}		

		// Check the sign of the label. A negative label means that the track was reconstructed with several points, but there was another track with more points.
		Int_t currentTrackLabel = currentAODTrack->GetLabel();
		if (currentTrackLabel < 0) {fTrackLabelSign->Fill(0);} 
		else {fTrackLabelSign->Fill(1);}
		Int_t absCurrentTrackLabel = TMath::Abs(currentTrackLabel);

		// Get the associated minitrack pointer from the linkarray.
		AliMiniTrackDiHadronPID* currentMiniTrack = (AliMiniTrackDiHadronPID*)mcArrayLink->At(absCurrentTrackLabel);
		
		// If this exists, minitrack passed MC particle cuts on pt, eta, y.
		if (currentMiniTrack) { 

			// Check whether the track already had reconstructed info.
			if (currentMiniTrack->Pt() > 0.) {
				AliLogDiHadronPID::Error(Form("Tryig to overwrite reco info."), __func__, ClassName());
				fNMultipleRecoTracks->Fill(0);
			}

			// Add a random hit to the track (for TOF mismatches).
			Double_t rndHitTime = -1.e21;
			if (fCalculateMismatch) {rndHitTime = GenerateRandomHit(currentAODTrack->Eta());}

			// Copy the relevant parameters from the aodTrack to the miniTrack.
			currentMiniTrack->CopyPars(currentAODTrack, fCurrentAODEvent, fPIDResponse, fIsMC, fCalculateMismatch, rndHitTime);

		// If htere is no MC part available but the track still passes reconstructed track cuts, add it to the event.
		} else {

			if (!IsTrackSelected(currentAODTrack)) {continue;}

			currentMiniTrack = fCurrentMiniEvent->NextParticle();

			Double_t rndHitTime = -1.e21;
			if (fCalculateMismatch) {rndHitTime = GenerateRandomHit(currentAODTrack->Eta());}
			
			// Copy the relevant MC parameters of these..
			AliAODMCParticle* currentAODMCParticle = (AliAODMCParticle*)mcArray->At(absCurrentTrackLabel);
			AliAODMCParticle* currentAODMCMother = 0x0;
			if (currentAODMCParticle->GetMother() >= 0) {currentAODMCMother = (AliAODMCParticle*)mcArray->At(TMath::Abs(currentAODMCParticle->GetMother()));}

			// Fill minitrack with MC info.
			currentMiniTrack->CopyPars(currentAODMCParticle, currentAODMCMother);

			// Copy the relevant parameters from the aodTrack to the miniTrack.
			currentMiniTrack->CopyPars(currentAODTrack, fCurrentAODEvent, fPIDResponse, fIsMC, fCalculateMismatch, rndHitTime);			
			// currentMiniTrack->PrintTrack();			

		}
	}

	delete mcArrayLink;

}

// -----------------------------------------------------------------------
Bool_t AliAnalysisTaskDiHadronPIDMini::IsEventSelected(AliAODEvent* event) {

	// Checks if an event is selected, and fills QA histograms.
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	// Get ptr to the input event handler.
	AliInputEventHandler* InputHandler = (AliInputEventHandler*)((AliAnalysisManager::GetAnalysisManager())->GetInputEventHandler());
	if (!InputHandler) {
		AliLogDiHadronPID::Error(Form("Cannot get ptr to event handler..."), __func__, ClassName());
		return kFALSE;
	}

	fEventCuts->Fill(0);

	// Check the event trigger.
	UInt_t eventTrigger = InputHandler->IsEventSelected();	// This UInt_t indicates the type of trigger (i.e., AliVEvent::kMB, kCentral, kSemiCentral, kINT7)
	if (!(eventTrigger & fEventTrigger)) {return kFALSE;}	// Reject if desired event trigger does not match
	
	fEventCuts->Fill(1);

	// Check the Primary Event Vertex.
	AliAODVertex* primaryVertex = event->GetPrimaryVertex();
    if (!primaryVertex) {return kFALSE;}

    fEventCuts->Fill(2);

	if (fBeamType == AliMiniEventDiHadronPID::kpp) {
		if (primaryVertex->GetNContributors() < 1) {
			if (primaryVertex->GetType() == AliAODVertex::kMainSPD) {
				fPrimVtxType->Fill(0);
			} else {
				return kFALSE;
			}
		} else {
			fPrimVtxType->Fill(1);
		}
	} 

	fEventCuts->Fill(3);

    if (TMath::Abs(primaryVertex->GetZ()) > fMaxVertexZ) {return kFALSE;}

    fEventCuts->Fill(4);

    // Check the centrality.
    if ((fBeamType == AliMiniEventDiHadronPID::kPbPb) || (fBeamType == AliMiniEventDiHadronPID::kpPb)) {

    	// TO CHECK: check on event centrality quality maybe not desired on MC...
       
        // First try MultSelection
        AliMultSelection *MultSelection = 0x0; 
	MultSelection = (AliMultSelection * ) event->FindListObject("MultSelection");
	if( MultSelection) {
            Float_t currentPercentile = MultSelection->GetMultiplicityPercentile(fCentralityEstimator.Data());
            // TODO: add check for centrality validity; maybe centrality -1 is a flag for invalid centrality?
    	    fEventCuts->Fill(5);
	    if ((currentPercentile < fMaxCentrality) || (currentPercentile > fMinCentrality)) {return kFALSE;}
	    fEventCuts->Fill(6);
	} else { // Use AliCentrality

    	AliCentrality* currentCentrality = event->GetCentrality();
    	if (currentCentrality->GetQuality()) {return kFALSE;} 

    	fEventCuts->Fill(5);

	    Float_t currentPercentile = currentCentrality->GetCentralityPercentile(fCentralityEstimator.Data());
	    if ((currentPercentile < fMaxCentrality) || (currentPercentile > fMinCentrality)) {return kFALSE;}

	    fEventCuts->Fill(6);
	}


    }

    fEventCuts->Fill(7);

    // Fill QA histograms.
    if (eventTrigger & AliVEvent::kMB) {fOfflineTrigger->Fill(0);}
    if (eventTrigger & AliVEvent::kINT7) {fOfflineTrigger->Fill(1);}

    return kTRUE;

}

// -----------------------------------------------------------------------
void AliAnalysisTaskDiHadronPIDMini::SetCentrality(Double_t min, Double_t max) {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	if (min < 0 || min > 100. || max < 0. || max > 100.) {
		AliLogDiHadronPID::Error(Form("Centrality has to be between 0 and 100."), __func__, ClassName());
		return;
	}

	// Max centrality is always the lowest number.
	if (min < max) {fMaxCentrality = min; fMinCentrality = max;}
	else {fMaxCentrality = max; fMinCentrality = min;}


}

// -----------------------------------------------------------------------
Bool_t AliAnalysisTaskDiHadronPIDMini::IsTrackSelected(AliAODTrack* track) {

	// Checks if a track is selected, and fills QA histograms.
	AliLogDiHadronPID::Debug4(Form("Called."), __func__, ClassName());

	// Check Pt.
	if ((track->Pt() < fMinPt) || (track->Pt() > fMaxPt)) {return kFALSE;}

	// Check abs value of eta.
	if (TMath::Abs(track->Eta()) > fMaxAbsEta) {return kFALSE;}

	// Check the filter map. Note that if at least one of the requested filterbits is found, then the track is accepted.
	if (!(track->GetFilterMap() & fFilterMask)) {return kFALSE;}

	// Cut on kTOFout & kTIME if requested.
	ULong_t flags = track->GetFlags();
	if (fCutOnTOFHit) {
		if (track->Pt() < fCutOnTOFHitBelowPt) {
			if (!(flags & AliVTrack::kTOFout)) {return kFALSE;}
			if (!(flags & AliVTrack::kTIME)) {return kFALSE;}
		}	
	}

	// Fill QA histograms.
	fNClustersTPC->Fill(track->GetTPCNcls());
	fNFindableClustersTPC->Fill(track->GetTPCNclsF());
	fNCrossedRowsTPC->Fill(track->GetTPCNCrossedRows());
	fNclsTPCvsTPCsignalN->Fill(track->GetTPCNcls(), track->GetTPCsignalN());	

	// Fill TOF vs HMPID histogram for 1.5 < pT < 2.0.
	if (track->Pt() > 1.5 && track->Pt() < 2.0) {
		if (flags & AliVTrack::kHMPIDpid) {
			
			Double_t TOFsignalExpPion = 0.;
			fPIDResponse->GetSignalDelta(AliPIDResponse::kTOF, track, AliPID::kPion, TOFsignalExpPion, kFALSE);

			Double_t HMPIDsignalExpPion = 0.;
			fPIDResponse->GetSignalDelta(AliPIDResponse::kHMPID, track, AliPID::kPion, HMPIDsignalExpPion, kFALSE);

			Double_t HMPIDnSigmaExpPion = fPIDResponse->NumberOfSigmasHMPID(track, AliPID::kPion);
			cout << "Expected signal: " << HMPIDnSigmaExpPion << " signal delta: " << HMPIDsignalExpPion << endl;

			fTOFsignalVsHMPIDnSigma->Fill(TOFsignalExpPion, HMPIDnSigmaExpPion);
		}
		
	}

	Int_t tmp = 0;
	if (track->HasPointOnITSLayer(0)) {tmp++;}
	if (track->HasPointOnITSLayer(1)) {tmp++;}
	fNHitsSPD->Fill(tmp);

	fTrackFlags->Fill(0);

	if (flags & AliVTrack::kITSin) {fTrackFlags->Fill(1);}
	if (flags & AliVTrack::kITSout) {fTrackFlags->Fill(2);}
	if (flags & AliVTrack::kITSrefit) {fTrackFlags->Fill(3);}
	if (flags & AliVTrack::kITSpid) {fTrackFlags->Fill(4);}

	if (flags & AliVTrack::kTPCin) {fTrackFlags->Fill(5);}
	if (flags & AliVTrack::kTPCout) {fTrackFlags->Fill(6);}
	if (flags & AliVTrack::kTPCrefit) {fTrackFlags->Fill(7);}
	if (flags & AliVTrack::kTPCpid) {fTrackFlags->Fill(8);}

	if (flags & AliVTrack::kTOFin) {fTrackFlags->Fill(9);}
	if (flags & AliVTrack::kTOFout) {fTrackFlags->Fill(10);}
	if (flags & AliVTrack::kTOFrefit) {fTrackFlags->Fill(11);}
	if (flags & AliVTrack::kTOFpid) {fTrackFlags->Fill(12);}

	if (flags & AliVTrack::kTIME) {fTrackFlags->Fill(13);}

	return kTRUE;
}

// -----------------------------------------------------------------------
Bool_t AliAnalysisTaskDiHadronPIDMini::IsMCParticleSelected(AliAODMCParticle* particle) {

	// Checks if an MC particle is selected, and fills QA histograms.
	AliLogDiHadronPID::Debug4(Form("Called."), __func__, ClassName());	

	// Throw out neutral particles.
	if (particle->Charge() == 0) {return kFALSE;}

	// 0.8 GeV < MCPt < 10 GeV
	if ((particle->Pt() < fMinPt) || (particle->Pt() > fMaxPt)) {return kFALSE;}

	// |eta| < 1.0 OR |y| < 0.6
	if ((TMath::Abs(particle->Eta()) > fMaxAbsEta) && (TMath::Abs(particle->Y()) > fMaxAbsMCY)) {return kFALSE;}

	return kTRUE;
}

// -----------------------------------------------------------------------
Bool_t AliAnalysisTaskDiHadronPIDMini::LoadExtMismatchHistos() {

	//
	// Attempting to load a root file containing information needed
	// to generate random TOF hits.
 	//

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Opening external TOF file.
	TFile* fin = 0x0;
	
	// The default is that the file TOFmismatchHistos.root is taken from the /rootfiles/ directory, 
	// and this works fine when running on the train. When the user submits the jobs himself, he can
	// choose to not take the file from the home dir, but upload one along with the source code.
	// If in this case the file is not found, the program tries to get the file from the root directory
	// anyway.
	if (fUseMismatchFileFromHomeDir == kFALSE) {
		fin = TFile::Open("TOFmismatchHistos.root");
		if (!fin) {AliWarning("Tried to open uploaded TOFmismatchHistos.root, but failed");}
	}

	if (!fin) {fin = TFile::Open("alien:///alice/cern.ch/user/m/mveldhoe/rootfiles/TOFmismatchHistos.root");}
	
	if (!fin) {
		AliWarning("Couln't open TOFmismatchHistos.root, will not calculate mismatches...");
		fCalculateMismatch = kFALSE;
		return kFALSE;
	} else {
		AliInfo("Sucessfully loaded TOFmismatchHistos.root");
	}

	// Check if the required histograms are present.
	TH1F* tmp1 = (TH1F*)fin->Get("hNewT0Fill");
	if (!tmp1) {
		AliWarning("Couln't find hNewT0Fill, will not calculate mismatches...");
		fCalculateMismatch = kFALSE;
		return kFALSE;	
	}
	TH2F* tmp2 = (TH2F*)fin->Get("hLvsEta");
	if (!tmp2) {
		AliWarning("Couln't find hLvsEta, will not calculate mismatches...");
		fCalculateMismatch = kFALSE;
		return kFALSE;	
	}	

	// Make a deep copy of the files in the histogram.
	fT0Fill = (TH1F*)tmp1->Clone("fT0Fill");
	fLvsEta = (TH2F*)tmp2->Clone("fLvsEta");

	// Close the external file.
	AliInfo("Closing external file.");
	fin->Close();

	// Creating a TObjArray for LvsEta projections.
	const Int_t nbinseta = fLvsEta->GetNbinsX();
	fLvsEtaProjections = new TObjArray(nbinseta);
	fLvsEtaProjections->SetOwner(kTRUE);

	// Making the projections needed (excluding underflow/ overflow).
	for (Int_t iEtaBin = 1; iEtaBin < (nbinseta + 1); iEtaBin++) {
		TH1F* tmp = (TH1F*)fLvsEta->ProjectionY(Form("LvsEtaProjection_%i",iEtaBin),iEtaBin,iEtaBin);
		tmp->SetDirectory(0);
		fLvsEtaProjections->AddAt(tmp,iEtaBin - 1);
	}

	return kTRUE;

}

// -----------------------------------------------------------------------
Double_t AliAnalysisTaskDiHadronPIDMini::GenerateRandomHit(Double_t eta) {

	//
	// Returns a random TOF time.
	//

	AliLogDiHadronPID::Debug4(Form("Called for eta = %f", eta), __func__, ClassName());

	// Default (error) value:
	Double_t rndHitTime = -1.e21;

	// TOF mismatch flag is not turned on.
	if (!fCalculateMismatch) {
		AliFatal("Called GenerateRandomHit() method, but flag fCalculateMismatch not set.");
		return rndHitTime;
	}

	// TOF doesn't extend much further than 0.8.
	if (TMath::Abs(eta) > 0.8) {
		if (fDebug) {AliInfo("Tried to get a random hit for a track with eta > 0.8.");}
		return rndHitTime;
	}

	// Finding the bin of the eta.
	TAxis* etaAxis = fLvsEta->GetXaxis();
	Int_t etaBin = etaAxis->FindBin(eta);
	if (etaBin == 0 || (etaBin == etaAxis->GetNbins() + 1)) {return rndHitTime;}

	const TH1F* lengthDistribution = (const TH1F*)fLvsEtaProjections->At(etaBin - 1);

	if (!lengthDistribution) {
		AliFatal("length Distribution not found.");
		return rndHitTime;
	}

	Double_t currentRndLength = lengthDistribution->GetRandom(); // in cm.

	// Similar to Roberto's code.
	Double_t currentRndTime = currentRndLength / (TMath::C() * 1.e2 / 1.e12);
	Double_t t0fill = -1.26416e+04;
	rndHitTime = fT0Fill->GetRandom() - t0fill + currentRndTime;

	return rndHitTime;

}

// -----------------------------------------------------------------------
TList* AliAnalysisTaskDiHadronPIDMini::CreateEventQAHistos() {

	// This method creates a TList of event QA histograms, owned by the list.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	TList* list = new TList();
	list->SetOwner(kTRUE);
	list->SetName("eventQAHistos");

	fEventCuts = new TH1F("fEventCuts", "Event Cuts;;N_{event}", 8, -0.5, 7.5);
	(fEventCuts->GetXaxis())->SetBinLabel(1, "Before Cuts");
	(fEventCuts->GetXaxis())->SetBinLabel(2, "Event Trigger");
	(fEventCuts->GetXaxis())->SetBinLabel(3, "Pr Vtx Exists");
	(fEventCuts->GetXaxis())->SetBinLabel(4, "Good Pr Vtx");
	(fEventCuts->GetXaxis())->SetBinLabel(5, "VtxZ cut");
	(fEventCuts->GetXaxis())->SetBinLabel(6, "Cent Quality");
	(fEventCuts->GetXaxis())->SetBinLabel(7, "Cent Cut");
	(fEventCuts->GetXaxis())->SetBinLabel(8, "After Cuts");
	list->Add(fEventCuts);

	fPrimVtxType = new TH1F("fPrimVtxType", "Primary Vertex Type (Selected Events)", 2, -0.5, 1.5);
	(fPrimVtxType->GetXaxis())->SetBinLabel(1, "kMainSPD");
	(fPrimVtxType->GetXaxis())->SetBinLabel(2, "Other");
	list->Add(fPrimVtxType);

	fOfflineTrigger = new TH1F("fOfflineTrigger", "Offline Trigger (Selected Events);;N_{event}",2,-0.5,1.5);
	(fOfflineTrigger->GetXaxis())->SetBinLabel(1, "kMB");
	(fOfflineTrigger->GetXaxis())->SetBinLabel(2, "kINT7");
	list->Add(fOfflineTrigger);

	return list;

}

// -----------------------------------------------------------------------
TList* AliAnalysisTaskDiHadronPIDMini::CreateTrackQAHistos() {

	// This method creates a TList of track QA histograms, owned by the list.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	TList* list = new TList();
	list->SetOwner(kTRUE);
	list->SetName("trackQAHistos");

	fNClustersTPC = new TH1F("fNClustersTPC", "Number of TPC Clusters for Tracking;N_{TPC};N_{ch}",160,0,159);
	list->Add(fNClustersTPC);

	fNFindableClustersTPC = new TH1F("fNFindableClustersTPC", "Number of Findable TPC Clusters for Tracking;N_{TPC};N_{ch}",160,0,159);
	list->Add(fNFindableClustersTPC);

	fNCrossedRowsTPC = new TH1F("fNCrossedRowsTPC", "Number of crossed TPC rows;N_{TPC};N_{ch}",160,0,159);
	list->Add(fNCrossedRowsTPC);

	fNHitsSPD = new TH1F("fNHitsSPD", "Number of hits in the SPD;N_{SPD hits};N_{ch}",3,-0.5,2.5);
	list->Add(fNHitsSPD);

	fTrackFlags = new TH1F("fTrackFlags", "Track Status Flags;;N_{ch}",14,-0.5,13.5);
	(fTrackFlags->GetXaxis())->SetBinLabel(1, "All Accepted");

	(fTrackFlags->GetXaxis())->SetBinLabel(2, "kITSin");
	(fTrackFlags->GetXaxis())->SetBinLabel(3, "kITSout");
	(fTrackFlags->GetXaxis())->SetBinLabel(4, "kITSrefit");
	(fTrackFlags->GetXaxis())->SetBinLabel(5, "kITSpid");

	(fTrackFlags->GetXaxis())->SetBinLabel(6, "kTPCin");
	(fTrackFlags->GetXaxis())->SetBinLabel(7, "kTPCout");
	(fTrackFlags->GetXaxis())->SetBinLabel(8, "kTPCrefit");
	(fTrackFlags->GetXaxis())->SetBinLabel(9, "kTPCpid");

	(fTrackFlags->GetXaxis())->SetBinLabel(10, "kTOFin");
	(fTrackFlags->GetXaxis())->SetBinLabel(11, "kTOFout");
	(fTrackFlags->GetXaxis())->SetBinLabel(12, "kTOFrefit");
	(fTrackFlags->GetXaxis())->SetBinLabel(13, "kTOFpid");

	(fTrackFlags->GetXaxis())->SetBinLabel(14, "kTIME");

	list->Add(fTrackFlags);

	fNclsTPCvsTPCsignalN = new TH2F("fNclsTPCvsTPCsignalN","fNclsTPCvsTPCsignalN;N_{tracking};N_{dE/dx}",160,-0.5,159.5,160,-0.5,159.5);
	list->Add(fNclsTPCvsTPCsignalN);

	fTOFsignalVsHMPIDnSigma = new TH2F("fTOFsignalVsHMPIDnSigma", "fTOFsignalVsHMPIDnSigma;TOF;HMPID", 200 , -1000., 8000., 40, -30, 30);
	list->Add(fTOFsignalVsHMPIDnSigma);

	return list;

}

// -----------------------------------------------------------------------
TList* AliAnalysisTaskDiHadronPIDMini::CreateMCParticleQAHistos() {

	// This method creates a TList of MC particle QA histograms, owned by the list.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	TList* list = new TList();
	list->SetOwner(kTRUE);
	list->SetName("mcParticleQAHistos");

	fTrackLabelSign = new TH1F("fTrackLabelSign", "fTrackLabelSign;Sign", 2, -0.5, 1.5);
	(fTrackLabelSign->GetXaxis())->SetBinLabel(1, "Neg");
	(fTrackLabelSign->GetXaxis())->SetBinLabel(2, "Pos");

	list->Add(fTrackLabelSign);

	fNMultipleRecoTracks = new TH1F("fNMultipleRecoTracks", "fNMultipleRecoTracks",1,-0.5,0.5);
	list->Add(fNMultipleRecoTracks);

	return list;

}
