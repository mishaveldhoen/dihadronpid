#ifndef ALIFITTEMPLATESDIHADRONPID_H
#define ALIFITTEMPLATESDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TNamed.h"
#include "TAxis.h"
#include "TH2F.h"
#include "AliFitTemplatesDiHadronPID.h"
#include "AliPtEtaSpectrumDiHadronPID.h"
#include "AliSettingsDiHadronPID.h"
#include "AliObjectArray2DDiHadronPID.h"

class AliFitTemplatesDiHadronPID : public TNamed {
	
public:
	AliFitTemplatesDiHadronPID();
	AliFitTemplatesDiHadronPID(const char* name, const char* title, const TAxis* ptAxis, const TH2F* etaDEtaMap, AliSettingsDiHadronPID::Species massAssumption);
	AliFitTemplatesDiHadronPID(const AliFitTemplatesDiHadronPID& other);

	// AliFitTemplatesDiHadronPID& operator=(const AliFitTemplatesDiHadronPID& other);

	~AliFitTemplatesDiHadronPID();

	Bool_t							GenerateTemplates(AliPtEtaSpectrumDiHadronPID* spectrum);

	Int_t 							GetMassAssumption() const {return fMassAssumption;} 
	TH2F*							GetPionTemplate(Int_t pTbin, Int_t dEtaBin) {return (TH2F*)fPionTemplates->GetAt(pTbin - 1, dEtaBin - 1);}
	TH2F*							GetKaonTemplate(Int_t pTbin, Int_t dEtaBin) {return (TH2F*)fKaonTemplates->GetAt(pTbin - 1, dEtaBin - 1);}
	TH2F*							GetProtonTemplate(Int_t pTbin, Int_t dEtaBin) {return (TH2F*)fProtonTemplates->GetAt(pTbin - 1, dEtaBin - 1);}
	// TH2F*							GetDeuteronTemplate(Int_t pTbin, Int_t dEtaBin) {return (TH2F*)fDeuteronTemplates->GetAt(pTbin - 1, dEtaBin - 1);}
	// TH2F*							GetMuonTemplate(Int_t pTbin, Int_t dEtaBin) {return (TH2F*)fMuonTemplates->GetAt(pTbin - 1, dEtaBin - 1);}
	TH2F*							GetElectronTemplate(Int_t pTbin, Int_t dEtaBin) {return (TH2F*)fElectronTemplates->GetAt(pTbin - 1, dEtaBin - 1);}
	TH2F*							GetPeakTemplate(Int_t pTbin, Int_t dEtaBin, AliSettingsDiHadronPID::Species species);
	TH2F*							GetMismatchTemplate(Int_t pTbin, Int_t dEtaBin) {return (TH2F*)fMismatchTemplates->GetAt(pTbin - 1, dEtaBin - 1);}

	TH2F*							GetEtaDEtaMapClone() const {return (fDEtaEtaMap ? (TH2F*)(fDEtaEtaMap->Clone()) : 0x0);}

	void 							Print(Option_t*) const;

	void							NormalizeEtaDEtaMap();

private:
	Bool_t							InitTemplates(AliPtEtaSpectrumDiHadronPID* spectrum);

	void							NormalizeTemplate(TH2F* temp);

private:
	AliSettingsDiHadronPID*			fSettings; //!	
	AliSettingsDiHadronPID::Species	fMassAssumption;

	TAxis*							fPtAxis;

	// Correlation between eta and DEta.% The single most important way to enhance ones understanding is to visualize data or scientific concepts. 
	TH2F*							fDEtaEtaMap;

	// FIXME: Seems like the Streamer() gets into trouble when there are 32 bins in DEta.
	// Arrays in (pT,DEta) (FIXME: Literal mention of the six particle species.)
	AliObjectArray2DDiHadronPID*	fPionTemplates;
	AliObjectArray2DDiHadronPID*	fKaonTemplates;
	AliObjectArray2DDiHadronPID*	fProtonTemplates;
	// AliObjectArray2DDiHadronPID*	fDeuteronTemplates;
	AliObjectArray2DDiHadronPID*	fElectronTemplates;
	// AliObjectArray2DDiHadronPID*	fMuonTemplates;

	AliObjectArray2DDiHadronPID*	fMismatchTemplates;

	ClassDef(AliFitTemplatesDiHadronPID, 2);

};

#endif
