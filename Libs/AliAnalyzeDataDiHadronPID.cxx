/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Process Data from AliAnalysisTaskDiHadronPIDMini
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include <iostream>
#include <TThread.h>
#include "TSystem.h"
#include "TKey.h"
#include "TDirectoryFile.h"
#include "TObjString.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TFileMerger.h"
#include "THn.h"
#include "TDirectory.h"
#include "TRandom3.h"
#include "AliHistToolsDiHadronPID.h"
#include "AliMiscToolsDiHadronPID.h"
#include "AliMTFileMergerDiHadronPID.h"

#include "AliPtEtaSpectrumDiHadronPID.h"
#include "AliFitTemplatesDiHadronPID.h"

#include "AliLogDiHadronPID.h"

#include "AliAnalyzeDataDiHadronPID.h"

using namespace std;

ClassImp(AliAnalyzeDataDiHadronPID);

// -----------------------------------------------------------------------
AliAnalyzeDataDiHadronPID::AliAnalyzeDataDiHadronPID(const char* name, const char* title) :
	TNamed(name, title),
	fSettings(AliSettingsDiHadronPID::Instance()),
	fInputFileList(0x0),
	fCurrentFileIndex(0),
	fCurrentFile(0x0),
	fCurrentEventBranch(0x0),
	fCurrentEventIndex(0),
	fCurrentEvent(0x0),
	fTotalEventIndex(0),
	fStopwatch(0x0),
	fNProcessedEvents(0),
	fRerunUntilEvent(-1),
	fRerunInProgress(kFALSE),
	fOutputFile(0x0)

{

	// Default Constructor
	AliLogDiHadronPID::Debug2(Form("Default constructor called"), __func__, ClassName());

	// Error message when settings singleton is not found!
	if (!fSettings) {AliLogDiHadronPID::Error(Form("Settings Singleton not found!"), __func__, ClassName());}

	// Making sure that histograms are not associated to the current directory.
	TH1::AddDirectory(kFALSE);

	fDataDir = fSettings->GetDataPath();
	fInputFileName = "AnalysisResults.root";
	fOutputDir = fSettings->GetWorkingDirectory();
	fOutputFileName = "output.root";

	// Generate a list of files that match the inputFileName.
	if (GenerateFileList()) {CheckFileList();}

}

// -----------------------------------------------------------------------
AliAnalyzeDataDiHadronPID::~AliAnalyzeDataDiHadronPID() {

	// Destructor.
	AliLogDiHadronPID::Debug2(Form("Destructor called."), __func__, ClassName());

	if (fInputFileList) {delete fInputFileList; fInputFileList = 0x0;}
	if (fCurrentFile) {fCurrentFile->Close(); delete fCurrentFile; fCurrentFile = 0x0;}
	if (fCurrentEvent) {delete fCurrentEvent; fCurrentEvent = 0x0;}
	if (fStopwatch) {delete fStopwatch; fStopwatch = 0x0;}

}

// -----------------------------------------------------------------------
void AliAnalyzeDataDiHadronPID::Exec(Int_t maxNevents) {

	// This method is responsible for looping over all events, and executing the user
	// defined code (creating output, analysing data, storing output).
	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	// Start the timer.
	if (!fStopwatch) {fStopwatch = new TStopwatch();}
	fStopwatch->Start();

    // Sanity check max number of events (if no valid option is chosen, then set to inf)
    if (maxNevents < 0) {maxNevents = 1410065407;}

	// Reset event counter.
	fNProcessedEvents = 0;
	fRerunInProgress = kFALSE;
	fRerunUntilEvent = -1;

	// Create user defined output.
	AliLogDiHadronPID::Info(Form("Creating output objects."), __func__, ClassName());
	UserCreateOutput();

	// Open the output file.
	fOutputFile = new TFile(Form("%s/%s", fOutputDir.Data(), fOutputFileName.Data()), "recreate");
	if (!fOutputFile) {
		AliLogDiHadronPID::Error(Form("Could not create output file..."), __func__, ClassName());
		return;
	}

	// Reset to the first file, first event.
	if (!NextFile(kTRUE)) {return;}

	// Loop over events.
	Int_t memGraphIndex = 0;
	TVectorF vNEvents(100);
	TVectorF vRealMem(100);
	TVectorF vVirtualMem(100);

	while (GetEvent() && fNProcessedEvents < maxNevents) {

		fNProcessedEvents++;

		// Printout per 1000 events.
		if (fNProcessedEvents % 1000 == 0) {
			cout << "Processing Event: " << fNProcessedEvents/* << " index: "<< fTotalEventIndex*/ << endl;

			// Add entry in the mem Graph.
			ProcInfo_t procInfo;
			gSystem->GetProcInfo(&procInfo);
			vNEvents[memGraphIndex] = fNProcessedEvents;
			vRealMem[memGraphIndex] = (procInfo.fMemResident) / 1024;
			vVirtualMem[memGraphIndex] = (procInfo.fMemVirtual) / 1024;

			// Increase  size if needed.
			memGraphIndex++;
			if (vNEvents.GetNoElements() == memGraphIndex) {
				vNEvents.ResizeTo(memGraphIndex + 10);
				vRealMem.ResizeTo(memGraphIndex + 10);
				vVirtualMem.ResizeTo(memGraphIndex + 10);
			}

		}

		// The code from the user analysing the event. -> Problem, what if we want to start again!
		UserExec();

	}

	// TODO: Avoid code duplication.
	// Perform rerun if requested.
	if (fRerunUntilEvent > 0) {
		
		AliLogDiHadronPID::Info(Form("Rerun requested until event %i", fRerunUntilEvent), __func__, ClassName());
		fRerunInProgress = kTRUE;

		// Reset to the first file, first event.
		if (!NextFile(kTRUE)) {return;}

		Int_t nRerunEvents = 0;

		// Loop over events.
		while (GetEvent() && (nRerunEvents < fRerunUntilEvent)) {

			nRerunEvents++;
			
			// Printout per 1000 events.
			if (nRerunEvents % 1000 == 0) {cout << "Processing Event: " << nRerunEvents/* << " index: "<< fTotalEventIndex*/ << endl;}

			// The code from the user analysing the event. -> Problem, what if we want to start again!
			UserExec();

		}	
	}

	// Apply possible final analysis steps.
	UserFinalizeAnalysis();

	AliLogDiHadronPID::Info(Form("Saving output objects."), __func__, ClassName());
	fOutputFile->cd();

	// Store mem info graphs.
	vNEvents.ResizeTo(memGraphIndex);
	vRealMem.ResizeTo(memGraphIndex);
	vVirtualMem.ResizeTo(memGraphIndex);

	TGraph gRealMemUsage(vNEvents, vRealMem);
	gRealMemUsage.SetName("realMemUsage");
	gRealMemUsage.SetTitle("realMemUsage");
	gRealMemUsage.GetXaxis()->SetTitle("N_{events}");
	gRealMemUsage.GetYaxis()->SetTitle("Real Mem (MB)");
	gRealMemUsage.Write();

	TGraph gVirtualMemUsage(vNEvents, vVirtualMem);
	gVirtualMemUsage.SetName("virtualMemUsage");
	gVirtualMemUsage.SetTitle("virtualMemUsage");
	gVirtualMemUsage.GetXaxis()->SetTitle("N_{events}");
	gVirtualMemUsage.GetYaxis()->SetTitle("Virtual Mem (MB)");
	gVirtualMemUsage.Write();

	// Save user defined output.
	UserSaveOutput();	

	// Give analysis summary.
	cout << "Processed " << fNProcessedEvents << " events." << endl;
	cout << "Final event index was: " << fTotalEventIndex << endl;
	cout << "Analysis time: " << AliMiscToolsDiHadronPID::FormatTime(fStopwatch->RealTime()).Data() << endl;

	// Close the output file.
	fOutputFile->Close();
	delete fOutputFile; 
	fOutputFile = 0x0;

}

// -----------------------------------------------------------------------
Bool_t AliAnalyzeDataDiHadronPID::GenerateFileList() {

	// Creates a list of all files in a certain directory tree.
	// the -follow option ensures that symlinks are followed.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	// Create a pipe to the unix find command.
	FILE* pipe = gSystem->OpenPipe(Form("find %s -name %s -follow",fDataDir.Data(), fInputFileName.Data()),"r");
	
	// Create a list of file names.
	fInputFileList = new TObjArray(100);
	fInputFileList->SetOwner(kTRUE);

	TString tmpFileName;
	while (tmpFileName.Gets(pipe)) {	

		//cout<<tmpFileName.Data()<<endl;
		fInputFileList->AddLast(new TObjString(tmpFileName.Data()));

	}	

	// Check if a sensible list has been created.
	if (fInputFileList->GetEntriesFast()) {return kTRUE;}
	else {
		cout<<"WARNING: File list not generated, no matching files found..."<<endl;
		delete fInputFileList; fInputFileList = 0x0;
		return kFALSE;
	}

}

// -----------------------------------------------------------------------
void AliAnalyzeDataDiHadronPID::CheckFileList() {

	// This method tries to open and close all files in the file list, 
	// to check if they're accessible.
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	for (Int_t iFile = 0; iFile < fInputFileList->GetEntriesFast(); iFile++) {
		TObjString* tmpFileName = (TObjString*)fInputFileList->At(iFile);
		cout << (tmpFileName->GetString()).Data() << " -> ";
		TFile* tmpFile = new TFile((tmpFileName->GetString()).Data());
		if (tmpFile->IsZombie()) {
			cout << "ERROR!" << endl;
			fInputFileList->RemoveAt(iFile);
			//fInputFileList->Remove(tmpFile);
		} else {cout << "OK!" << endl;}

		tmpFile->Close();
		delete tmpFile;

	}

}


// -----------------------------------------------------------------------
Bool_t AliAnalyzeDataDiHadronPID::NextFile(Bool_t reset) {

	// Opens the first file in the InputFileList, and loads the first event.
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	// Closes current file and deletes objects.
	if (fCurrentFile) {
		fCurrentFile->Close();
		fCurrentFile = 0x0;
	}

	// Reset event branch and index.
	fCurrentEventBranch = 0x0;	// I don't think we need to delete this.
	fCurrentEventIndex = 0;

	// If reset is called then we start at the first file, otherwise we go
	// to the next one.
	if (reset) {fCurrentFileIndex = 0; fTotalEventIndex = 0;}
	else {fCurrentFileIndex++;}

	// Check if there are more files in the list.
	if (fCurrentFileIndex == fInputFileList->GetEntriesFast()) {
		//cout << "No more files in the list!" << endl;
		return kFALSE; 
	}

	// Look up file name.
	TObjString* tmpFileName = (TObjString*)fInputFileList->At(fCurrentFileIndex);	
	while (!tmpFileName) {

		// If the last file in the list is missing, then we're done.
		if (fCurrentFileIndex + 1 == fInputFileList->GetEntriesFast()) {return kFALSE;}

		// If not, try to go to the next file.
		cout << "File missing in the list, skipping to the next one!" << endl;
		fCurrentFileIndex++; 
		tmpFileName = (TObjString*)fInputFileList->At(fCurrentFileIndex);
	}

	// Load file.
	fCurrentFile = new TFile((tmpFileName->GetString()).Data());
	if (fCurrentFile->IsZombie()) {
		cout << "ERROR: could not open: "<< (tmpFileName->GetString()).Data() << endl;
		return kFALSE;
	} else {
		cout << " Opening file: "<<fCurrentFileIndex + 1 <<"/"<< fInputFileList->GetEntriesFast()<<": "<< (tmpFileName->GetString()).Data() << endl;
	}

	// Read the event tree in the file.
	TDirectoryFile* tmpDir = (TDirectoryFile*)fCurrentFile->Get("PWGCF_DiHadronPIDMini");
	if (!tmpDir) {
		cout << "ERROR: could not read PWGCF_DiHadronPIDMini" << endl;
		return kFALSE;
	}


	TTree* tmpEventTree = (TTree*)tmpDir->Get("eventTree");
	if (!tmpEventTree) {
		cout << "ERROR: could not read eventTree" << endl;
		return kFALSE;
	}

	// Let fCurrentEvent point to the first event.
	fCurrentEventBranch = tmpEventTree->GetBranch("events");
	fCurrentEventBranch->SetAddress(&fCurrentEvent);
	
	return kTRUE;

}

// -----------------------------------------------------------------------
Bool_t AliAnalyzeDataDiHadronPID::GetEvent() {

	// Get the next available event.
	AliLogDiHadronPID::Debug4(Form("Called."), __func__, ClassName());

	// Check if we're at the end of the file.
	if (fCurrentEventIndex == fCurrentEventBranch->GetEntries()) {
		
		// Check if there's a net file available, if not, then return false.
		if (!NextFile()) {return kFALSE;}
		else {return GetEvent();}	// Forces the loop to start over (protects against empty files).
	}

	// Get the event located at the current index.
	fCurrentEventBranch->GetEntry(fCurrentEventIndex);

	// Print info.
	// if (fVerboseLevel > 1) cout << "  Event: " << fCurrentEventIndex + 1 << "/" << fCurrentEventBranch->GetEntries() << endl;
	
	// Increment the index.
	fCurrentEventIndex++;
	fTotalEventIndex++;

	return kTRUE;

}

