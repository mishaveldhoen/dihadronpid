#ifndef ALIANALYSYSTASKDIHADRONPIDMINI_H
#define ALIANALYSYSTASKDIHADRONPIDMINI_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TString.h"
#include "TObjArray.h"
#include "TFormula.h"

#include "AliAnalysisTaskSE.h"
#include "AliMiniEventDiHadronPID.h"

class AliAnalysisTaskDiHadronPIDMini : public AliAnalysisTaskSE {

public:
	// Constructors/Destructors.
	AliAnalysisTaskDiHadronPIDMini();
	AliAnalysisTaskDiHadronPIDMini(const char* name);
	virtual ~AliAnalysisTaskDiHadronPIDMini();

private:
	AliAnalysisTaskDiHadronPIDMini(const AliAnalysisTaskDiHadronPIDMini&);
	AliAnalysisTaskDiHadronPIDMini& operator=(const AliAnalysisTaskDiHadronPIDMini&);

public:
	// Analysis.
	void 							UserCreateOutputObjects();
	void 							UserExec(Option_t*);
	void 							UserExecData();
	void 							UserExecMC();
	
	// Event Cuts.
	Bool_t 							IsEventSelected(AliAODEvent* event);
	void 							SetEventTrigger(UInt_t eventTrigger) {fEventTrigger = eventTrigger;} // i.e., AliVEvent::kMB, etc. 
	void							SetCentrality(Double_t min, Double_t max);
	void							SetCentralityEstimator(TString centralityEstimator) {fCentralityEstimator = centralityEstimator;}
	void							SetMaxVertexZ(Double_t maxVtxZ) {fMaxVertexZ = maxVtxZ;}

	// Optional Event Cuts.
	void							SetCutOnContributorsOrSPDVertex() {fCutOnContributorsOrSPDVertex = kTRUE;}

	// Track Cuts.
	Bool_t							IsTrackSelected(AliAODTrack* track);
	void							SetPtRange(Double_t minPt, Double_t maxPt) {fMinPt = minPt; fMaxPt = maxPt;}
	void							SetMaxAbsEta(Double_t maxAbsEta) {fMaxAbsEta = maxAbsEta;}
	void							SetFilterMask(UInt_t mask) {fFilterMask = mask;}

	// Optional Track Cuts.
	void							SetCutOnTOFHitBelowPt(Double_t pt) {fCutOnTOFHit = kTRUE; fCutOnTOFHitBelowPt = pt;}

	// MC Particle Cuts.
	Bool_t							IsMCParticleSelected(AliAODMCParticle* particle);
	void							SetMaxAbsMCY(Double_t maxAbsMCY) {fMaxAbsMCY = maxAbsMCY;}

	// Misc Setters.
	void 							SetCalculateMismatch(Bool_t calcmismatch = kTRUE) {fCalculateMismatch = calcmismatch;}
	void 							SetUseMismatchFileFromGridHomeDir(Bool_t usefromhomedir = kTRUE) {fUseMismatchFileFromHomeDir = usefromhomedir;}
	void 							SetIsMC(Bool_t isMC = kTRUE) {fIsMC = isMC;}
	void 							SetBeamType(AliMiniEventDiHadronPID::BeamType beamType) {fBeamType = beamType;}

	// Misc Getters.
	Bool_t 							GetCalculateMismatch() const {return fCalculateMismatch;}

private:
	
	Bool_t 							LoadExtMismatchHistos();
	Double_t 						GenerateRandomHit(Double_t eta);
	AliMiniEventDiHadronPID::BeamType GetBeamType() const {return fBeamType;}

	// Creating QA histograms.
	TList*							CreateEventQAHistos();
	TList*							CreateTrackQAHistos();
	TList*							CreateMCParticleQAHistos();

private:

	// PID Response Object.
	AliPIDResponse*					fPIDResponse;					//! PID Response.

	// Mandatory Event Cuts.
	UInt_t 							fEventTrigger;					// AliVEvent::kMB, kCentral, kINT7, etc.
	Float_t 						fMaxVertexZ;
	TString 						fCentralityEstimator;
	Float_t 						fMinCentrality;
	Float_t 						fMaxCentrality;

	// Optional Event Cuts.
	Bool_t							fCutOnContributorsOrSPDVertex;

	// Mandatory Track Cuts.
	Double_t						fMinPt;
	Double_t						fMaxPt;
	Double_t 						fMaxAbsEta;
	Double_t						fMaxAbsMCY;
	UInt_t 							fFilterMask;

	// Optional Track Cuts.
	Bool_t							fCutOnTOFHit;					// Remove tracks with no TOFout & kTIME below
	Double_t						fCutOnTOFHitBelowPt;            // this pt.
	
	// The following info is stored in the mini tracks, and cuts need to be done in the processing of the mini tracks.
	// Int_t						fMinimumNumberOfTPCClusters; 	// NCls of TPC detector.
	// UInt_t 						fMinSPDHitsForPtDeptDCAcut;		// Required number of SPD hits for performing Pt-Dept DCA cut.
	// TFormula* 					fPtDeptDCAxyCutFormula;			// Formula for the Pt-Dept DCA cut.
	// Double_t						fDCAzCut;

	// Current Event.
	AliAODEvent*					fCurrentAODEvent;			//! Current AOD Event.
	AliMiniEventDiHadronPID*		fCurrentMiniEvent;			//!

	// Output Containers.
	TTree*							fEventTree;					//!

	TList*							fEventQAList;				//!
	TH1F* 							fEventCuts;					//!
	TH1F*							fPrimVtxType;				//!
	TH1F*							fOfflineTrigger;			//!

	TList*							fTrackQAList;				//!
	TH1F*							fNClustersTPC;				//!
	TH1F* 							fNFindableClustersTPC;		//!
	TH1F*							fNCrossedRowsTPC;			//!
	TH1F*							fNHitsSPD;					//!
	TH1F*							fTrackFlags;				//!
	TH2F*							fNclsTPCvsTPCsignalN;		//!
	TH2F*							fTOFsignalVsHMPIDnSigma;	//!

	TList*							fMCParticleQAList;			//!
	TH1F*							fTrackLabelSign;			//!
	TH1F*							fNMultipleRecoTracks; 		//!

	// Settings.
	AliMiniEventDiHadronPID::BeamType	fBeamType;					// Enum Type
	Bool_t								fIsMC;						//
	Bool_t								fCalculateMismatch;			//
	Bool_t							fUseMismatchFileFromHomeDir;//

	// TOF mismatch stuff.
	TH1F*							fT0Fill;					//!
	TH2F*							fLvsEta;					//!
	TObjArray*						fLvsEtaProjections;			//!		

	ClassDef(AliAnalysisTaskDiHadronPIDMini, 2);

};

#endif
