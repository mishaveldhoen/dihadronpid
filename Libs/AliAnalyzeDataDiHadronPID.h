#ifndef ALIANALYZEDATADIHADRONPID_H
#define ALIANALYZEDATADIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TNamed.h"
#include "TObjArray.h"
#include "TBranch.h"
#include "TFile.h"
#include "TDirectory.h"
#include "TStopwatch.h"

#include "AliSettingsDiHadronPID.h"
#include "AliMiniEventDiHadronPID.h"

class AliAnalyzeDataDiHadronPID : public TNamed {

public:
	AliAnalyzeDataDiHadronPID(const char* name, const char* title);
	~AliAnalyzeDataDiHadronPID();
	
	// Analysis Methods.
	void		 					Exec(Int_t maxNevents = 1410065407);		
	void 							SetDataDir(TString str) {fDataDir = str;}
	void 							SetInputFileName(TString str) {fInputFileName = str;}
	void 							SetOutputDir(TString str) {fOutputDir = str;}
	void 							SetOutputFileName(TString str) {fOutputFileName = str;}

private:
	// Method should be overridden by the user.
	virtual void					UserCreateOutput() = 0;		// User creates output objects.
	virtual void					UserExec() = 0; 			// Analysis on every event.
	virtual void					UserFinalizeAnalysis() = 0;	// Any possible final operations to be performed on the output.
	virtual void					UserSaveOutput() = 0;		// Explicitly call Write() on all object that need to be stored.

protected:
	// Event iteration methods, needed for any type of analysis.
	Bool_t							GenerateFileList();
	void 							CheckFileList();
	Bool_t							NextFile(Bool_t reset = kFALSE);	// Go to the
	Bool_t							GetEvent();

	// Interface for the user task.
	AliMiniEventDiHadronPID*		GetCurrentEvent() {return fCurrentEvent;}
	void							RequestRerunUntilCurrentEvent() {fRerunUntilEvent = fNProcessedEvents;}
	Bool_t							RerunIsInProgress() const {return fRerunInProgress;}

protected:
	// Analysis Settings Singleton.
	AliSettingsDiHadronPID*			fSettings;				//! Not Stored

	// General settings.
	TString 						fDataDir;				// Given by the settings.
	TString 						fInputFileName;			// Standard.
	TString 						fOutputDir;				// Should be given by the settings.
	TString							fOutputFileName;		// user defined, but standard something like: analysisResult.root

	// Pointers to current events/files, etc.
	TObjArray*						fInputFileList;			//!
	Int_t							fCurrentFileIndex;		//! Is reset when new file is opened.
	TFile*							fCurrentFile;			//!
	TBranch*						fCurrentEventBranch;	//!
	Int_t							fCurrentEventIndex;		//!
	AliMiniEventDiHadronPID*		fCurrentEvent;			//!

	Int_t							fTotalEventIndex;		//! Is NOT reset when new file is opened (i.e., this uniquely defines the event.)

	// Quantifying analysis size / payload.
	TStopwatch*						fStopwatch;				//!
	Int_t 							fNProcessedEvents;		//!

	// Rerun variables - Certain analyses require that a certain number of events is processed twice,
	// for example when creating mixed events. Event mixing only starts when the event pool has reached
	// it's full size, so the events up to that point are not used for mixed events if no rewind takes place.
	Int_t							fRerunUntilEvent;		//
	Bool_t							fRerunInProgress;		//

	TFile*							fOutputFile;			//! Output file

	ClassDef(AliAnalyzeDataDiHadronPID, 1);

};

#endif
