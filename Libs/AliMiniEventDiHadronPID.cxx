/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Minimal Event Implementation for DiHadronPID Analysis.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include <iostream>
#include "AliLogDiHadronPID.h"
#include "AliCentrality.h"
#include "AliAODVertex.h"
#include "AliMiniEventDiHadronPID.h"

using namespace std;

Int_t AliMiniEventDiHadronPID::fNInstances = 0;

ClassImp(AliMiniEventDiHadronPID);

// -----------------------------------------------------------------------
AliMiniEventDiHadronPID::AliMiniEventDiHadronPID(): 
	TObject(), 
	fVtxZ(-999),
	fVtxZIsMainSPD(0),
	fVtxZNCont(-1),	 
	fCent(-999),
	fCentIsBad(1),
	fTrackIndex(0),
	fTracks(0x0)

{

	fNInstances++;
	Init(); // Doesn't this cause a leak?? Seems like the root example does it too.
			// Without creating an array, it seems like the TBrowser cannot read the tracks
			// in the event.

} 

// -----------------------------------------------------------------------
AliMiniEventDiHadronPID::~AliMiniEventDiHadronPID() {

	if (fTracks) {delete fTracks; fTracks = 0x0;}

	fNInstances--;
}

// -----------------------------------------------------------------------
void AliMiniEventDiHadronPID::Init() {

	// This method initializes the track array in the event. 
	fTracks = new TClonesArray("AliMiniTrackDiHadronPID",1000);
	fTracks->SetOwner(kTRUE);

}

// -----------------------------------------------------------------------
AliMiniTrackDiHadronPID* AliMiniEventDiHadronPID::NextParticle() {

	// Lazy instantiation of the track array as soon as one tries to add a particle.
	if (!fTracks) {Init();}

	// Create a new particle, and update the counter.
	AliMiniTrackDiHadronPID* newtrack = (AliMiniTrackDiHadronPID*)fTracks->ConstructedAt(fTrackIndex);
	fTrackIndex++;

	return newtrack;	

}

// -----------------------------------------------------------------------
void AliMiniEventDiHadronPID::Done() {

	// Call this when the event is done. (Not sure if this actually makes a difference)
	if (fTracks) fTracks->Expand(fTrackIndex);

}

// -----------------------------------------------------------------------
void AliMiniEventDiHadronPID::ResetEvent() {

	if (fTracks) {fTracks->Delete();}
	else {Init();}
	fTrackIndex = 0;
	fVtxZ = -999.;
	fCent = -999.;

}

// -----------------------------------------------------------------------
void AliMiniEventDiHadronPID::CopyPars(AliAODEvent* event, BeamType beamType, const char* centralityEstimator) {

	// Copy relevant parameters from and AOD event.
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	// Set vtxZ of the current event.
	AliAODVertex* currentPrimaryVertex = event->GetPrimaryVertex();
	fVtxZ = currentPrimaryVertex->GetZ();
	fVtxZIsMainSPD = (currentPrimaryVertex->GetType() == AliAODVertex::kMainSPD);
	fVtxZNCont = currentPrimaryVertex->GetNContributors();

	// Set Centrality of the current event.
	if ((beamType == AliMiniEventDiHadronPID::kPbPb) || (beamType == AliMiniEventDiHadronPID::kpPb)) {
		AliCentrality* currentCentrality = event->GetCentrality();
		fCent = currentCentrality->GetCentralityPercentile(centralityEstimator);
		fCentIsBad = (Bool_t)currentCentrality->GetQuality(); //AliCentrality::GetQuality returns 0 if the centrality is OK
	}

}
