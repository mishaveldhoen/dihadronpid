#ifndef ALITEMPLATEFITTERDIHADRONPID_H
#define ALITEMPLATEFITTERDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TH2F.h"
#include "TCanvas.h"
#include "TObject.h"
#include "TObjArray.h"
#include "Math/Minimizer.h"
// #include "Minuit2/Minuit2Minimizer.h"
#include "AliSettingsDiHadronPID.h"

class AliTemplateFitterDiHadronPID : public TObject {
	
public:
	AliTemplateFitterDiHadronPID();
	// AliTemplateFitterDiHadronPID(AliSettingsDiHadronPID::Species massAssumption, Double_t meanPt, 
		// const TH2F* data, const TObjArray* peakTemplates, const TH2F* mismatchTemplate = 0x0, Double_t fixedMismatchFraction = -1);
	~AliTemplateFitterDiHadronPID();

private:
	Bool_t 								Init();
	void								UpdateFitRegion();

public:
	Int_t								Fit();
	void								Set(AliSettingsDiHadronPID::Species massAssumption, Double_t meanPt, 
		const TH2F* data, const TObjArray* peakTemplates, const TH2F* mismatchTemplate = 0x0, Double_t fixedMismatchFraction = -1);

	// Setters.

	// Getters.
	double*								GetFitParameters() {return fFitParameters;}
	double*								GetFitErrors() {return fFitErrors;}
	Int_t 								GetFitStatus() {return fFitStatus;}

	// Vizualisation methods.
	TCanvas*							DrawFitData(TCanvas* cvs = 0x0);
	TCanvas*							DrawTOFProjection(TCanvas* cvs = 0x0);
	TCanvas*							DrawTPCProjection(TCanvas* cvs = 0x0);
	TCanvas*							DrawNLL(TCanvas* cvs = 0x0);
	TCanvas*							DrawChi2(TCanvas* cvs = 0x0);

	// Verbose methods.
	void 								Print(Option_t*) const;
	void								PrintHessian() const;

private:
	void								EstimateParameters();
	void 								SetFitParameters(const double* pars);
	void								SetFitErrors(const double* errors);
	double								negll(const double* pars);
	double								chi2(const double* pars);

private:
	const TH2F*							fData; //!
	const TObjArray*					fPeakTemplates; //!
	const TH2F*							fMismatchTemplate; //!

	Bool_t 								fReady; //!

	AliSettingsDiHadronPID*				fSettings; //!
	ROOT::Math::Minimizer*				fMinimizer; //!

	AliSettingsDiHadronPID::Species		fMassAssumption;
	Double_t							fMeanPt;
	Double_t 							fFixedMismatchFraction; // Default: -1, i.e., not used.
	Int_t								fFitRangeMinBinTOF;
	Int_t 								fFitRangeMaxBinTOF;
	Int_t								fFitRangeMinBinTPC;
	Int_t								fFitRangeMaxBinTPC;
	Int_t								fFitStatus;
	double*								fFitParameters; //! Cannot be streamed, since length is unknown a priori.
	double*								fFitErrors; //! id.

	ClassDef(AliTemplateFitterDiHadronPID, 1);

};

#endif
