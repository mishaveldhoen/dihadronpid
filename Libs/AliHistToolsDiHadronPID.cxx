/************************************************************************* 
 * Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Tools for drawing/ manipulating histograms. (NEEDS CLEANUP!)
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "AliHistToolsDiHadronPID.h"
#include "AliFunctionsDiHadronPID.h"
#include "AliMiscToolsDiHadronPID.h"
#include "AliLogDiHadronPID.h"

#include <iostream>
#include <fstream>
using namespace std;

#include "TKey.h"
#include "TRandom3.h"
#include "TList.h"
#include "TLegend.h"
#include "TMath.h"
#include "TF1.h"
#include "TObjArray.h"
#include "TStyle.h"
#include "TArray.h"
#include "TString.h"
#include "TGraphAsymmErrors.h"

// -----------------------------------------------------------------------
TH1F* AliHistToolsDiHadronPID::RebinVariableBinning(
	const TH1F* histIn, const Double_t* binsx, Int_t Nbinsx, Bool_t density) {

	// Rebins a histogram (hin) with a variable binning to a histogram
	// with another variable binning (binsx). If the "density" flag is set,
	// then it is expected that the bins are per unit x-axis, otherwise an
	// absolute count is assumed.
	//
	// TODO: determine over/under-flow bins.

	// Gather info from the original histogram.
	const TArrayD* binsxIn = histIn->GetXaxis()->GetXbins();
	TArrayD* binsxOut = new TArrayD(Nbinsx + 1,binsx);
	Int_t NbinsxIn = binsxIn->GetSize() - 1;
	const char* nameIn = histIn->GetName();
	const char* titleIn = histIn->GetTitle();
	const char* xAxisTitleIn = histIn->GetXaxis()->GetTitle(); 
	const char* yAxisTitleIn = histIn->GetYaxis()->GetTitle();

	// Gather info from the output histogram and create it.
	Int_t NbinsxOut = binsxOut->GetSize() - 1;
	const char* nameOut = Form("%s (rebinned)",nameIn);
	const char* titleOut = Form("%s_rebinned;%s;%s",titleIn,xAxisTitleIn,yAxisTitleIn);
	TH1F* histOut = new TH1F(nameOut,titleOut,NbinsxOut,binsxOut->GetArray());
	//histOut->Sumw2();

	// Filling the bins.
	for (Int_t iBinOut = 0; iBinOut < NbinsxOut; iBinOut++) {

		Double_t binOutLowEdge = binsxOut->At(iBinOut);
		Double_t binOutHighEdge = binsxOut->At(iBinOut+1);
		Double_t binOutWidth = binOutHighEdge - binOutLowEdge;
		//Double_t binOutCenter = binOutHighEdge - binOutWidth/2.;

		// Setting all errors of the outgoing histogram to zero (needed later):
		histOut->SetBinError(iBinOut+1,0.);

		for (Int_t iBinIn = 0; iBinIn < NbinsxIn; iBinIn++) {
			
			Double_t binInLowEdge = binsxIn->At(iBinIn);
			Double_t binInHighEdge = binsxIn->At(iBinIn+1);
			Double_t binInWidth = binInHighEdge - binInLowEdge;
			//Double_t binInCenter = binInHighEdge - binInWidth/2.;
			Double_t binInContent = histIn->GetBinContent(iBinIn+1);
			Double_t binInError2 = histIn->GetBinError(iBinIn+1)*histIn->GetBinError(iBinIn+1);

			/* -- Determining what percentage of the in-bin is included 
			   in the current out-bin -- */
			Double_t PercentageIncluded = 0.;

			// Position of the low edge of the in-bin:
			// In  1   2   3            
			// Out   |   |
			Int_t binInLowEdgePos = 0;
			if (binInLowEdge < binOutLowEdge) binInLowEdgePos = 1;
			else if (binInLowEdge > binOutHighEdge) binInLowEdgePos = 3;
			else binInLowEdgePos = 2;

			// Same for the high edge of the in-bin:
			Int_t binInHighEdgePos = 0;
			if (binInHighEdge < binOutLowEdge) binInHighEdgePos = 1;
			else if (binInHighEdge > binOutHighEdge) binInHighEdgePos = 3;
			else binInHighEdgePos = 2;

			// In-bin not included.
			// In        | |     or     | |
			// Out |   |                    |    |
			if ( binInLowEdgePos == 3 || binInHighEdgePos == 1 ) {
				//cout<<"In-bin not included."<<endl;
				PercentageIncluded = 0.;
			}

			// In-bin partially included (left side).
			// In    |    |
			// Out |  |
			else if ( binInLowEdgePos == 2 && binInHighEdgePos == 3 ) {
				//cout<<"In-bin partially included (left side)."<<endl;
				PercentageIncluded = (binOutHighEdge - binInLowEdge)/(binInHighEdge - binInLowEdge);
			}

			// In-bin partially included (middle).
			// In    |     |
			// Out     |  |
			else if ( binInLowEdgePos == 1 && binInHighEdgePos == 3 ) {
				//cout<<"In-bin partially included (middle)."<<endl;
				PercentageIncluded = (binOutHighEdge - binOutLowEdge)/(binInHighEdge - binInLowEdge);
			}

			// In-bin partially included (right side).
			// In    |     |
			// Out        |  |
			else if ( binInLowEdgePos == 1 && binInHighEdgePos == 2 ) {
				//cout<<"In-bin partially included (right side)."<<endl;
				PercentageIncluded = (binInHighEdge - binOutLowEdge)/(binInHighEdge - binInLowEdge);
			}

			// In-bin completely included.
			// In    |  |
			// Out  |    |
			else if ( binInLowEdgePos == 2 && binInHighEdgePos == 2 ) {
				//cout<<"In-bin completely included."<<endl;
				PercentageIncluded = 1.;
			}

			// Give some status:
			//cout<<"Bin Out: "<<iBinOut+1<<" ("<<binOutLowEdge<<","<<binOutHighEdge<<")  Bin In: "<<iBinIn+1<<" ("<<binInLowEdge<<","<<binInHighEdge<<"), content: "<<binInContent<<" +/- "<<TMath::Sqrt(binInError2)<<". Bin in Pos: "<<binInLowEdgePos<<" "<<binInHighEdgePos<<" pct: "<<PercentageIncluded<<endl; 

			//else cout<<"Something weird just happened."<<endl;

			Double_t binOutAddedError2 = 0.;
			binOutAddedError2 = binInError2 * PercentageIncluded * PercentageIncluded;
			Double_t binOutAddedContent = 0.;
			binOutAddedContent = binInContent * PercentageIncluded;

			if (density) {
				binOutAddedContent*=binInWidth;
			}
			
			//histOut->Fill(binOutCenter,binOutAddedContent);
			histOut->SetBinContent(iBinOut+1,histOut->GetBinContent(iBinOut+1)+binOutAddedContent);

			//cout<<histOut->GetBinError(iBinOut+1)<<endl;
			//cout<<binOutAddedError2<<endl;
			histOut->SetBinError(iBinOut+1,histOut->GetBinError(iBinOut+1)+binOutAddedError2);
			//cout<<histOut->GetBinError(iBinOut+1)<<endl;
		}

		// Once a certain binOut has been filled all the way we have to scale
		// it if the "density" flag is set.

		histOut->SetBinContent(iBinOut + 1,histOut->GetBinContent(iBinOut+1)/binOutWidth);
		histOut->SetBinError(iBinOut + 1,TMath::Sqrt(histOut->GetBinError(iBinOut+1)));
		//cout<<histOut->GetBinError(iBinOut+1)<<endl;
	}

	return histOut;

}

// -----------------------------------------------------------------------
TH1F* AliHistToolsDiHadronPID::RebinVariableBinning(const TH1F* histIn, const TH1F* histAxis, Bool_t density) {

	// Rebins histogram histIn to the x-axis of histAxis
	Int_t nbinsx = histAxis->GetXaxis()->GetNbins();
	const Double_t* binsx = (histAxis->GetXaxis()->GetXbins())->GetArray();
	return RebinVariableBinning(histIn, const_cast<Double_t*>(binsx), nbinsx, density);

}

// -----------------------------------------------------------------------
TH1F* AliHistToolsDiHadronPID::RebinVariableBinning(const TH1F* histIn, const TAxis* xaxis, Bool_t density) {

	// Rebins histogram histIn to the x-axis of histAxis
	Int_t nbinsx = xaxis->GetNbins();
	const Double_t* binsx = (xaxis->GetXbins())->GetArray();
	return RebinVariableBinning(histIn, const_cast<Double_t*>(binsx), nbinsx, density);

}

// -----------------------------------------------------------------------
TH1F* AliHistToolsDiHadronPID::TrimHisto(const TH1F* histo, Int_t firstbin, Int_t lastbin) {

	const char* name = histo->GetName();
	const char* title = histo->GetTitle();
	if (firstbin < 0) return 0x0;
	if (lastbin > histo->GetNbinsX()) return 0x0;
	if (firstbin > lastbin) return 0x0;

	Int_t Nbins = lastbin - firstbin + 1;
	// cout<<firstbin<<" "<<lastbin<<" "<<Nbins<<endl;
	//const Int_t NbinsC = Nbins;
	Double_t newaxis[41];

	for (Int_t ii = firstbin; ii < lastbin+1; ii++) {
		newaxis[ii - firstbin] = histo->GetBinLowEdge(ii); 
		//cout<<ii-firstbin<<" "<<newaxis[ii - firstbin]<<endl;
	}
	newaxis[Nbins] = histo->GetBinLowEdge(lastbin+1);

	TH1F* hout = new TH1F("hout","hout",Nbins,newaxis);
	hout->SetDirectory(0);
	hout->SetTitle(title);
	hout->SetName(name);

	for (Int_t ii = 1; ii <= Nbins; ii++) {
		hout->SetBinContent(ii, histo->GetBinContent(firstbin + (ii - 1)));
		hout->SetBinError(ii, histo->GetBinError(firstbin + (ii - 1)));
	} 

	return hout;

}

// -----------------------------------------------------------------------
TH1F* AliHistToolsDiHadronPID::ShiftHistogram(const TH1F* hist, Int_t newFirstBin) {

	// This method creates creates a copy of the old histogram, but shifted,
	// assuming rotational symmetry. Useful for example in histograms as a function of phi.
	if (newFirstBin < 1 || newFirstBin > hist->GetNbinsX()) {
		cout << "ERROR: New first bin is: " << newFirstBin << endl;
		return 0x0;
	}
	// if ((hist->GetXaxis())->IsVariableBinSize()) {
	// 	cout << "ERROR: Not implemented for variable bin sizes." << endl;		
	// 	return 0x0;
	// }

	TH1F* hout = new TH1F(Form("%s_shifted", hist->GetName()), hist->GetTitle(), hist->GetXaxis()->GetNbins(), hist->GetXaxis()->GetBinLowEdge(newFirstBin), hist->GetXaxis()->GetBinLowEdge(newFirstBin) + hist->GetXaxis()->GetXmax() - hist->GetXaxis()->GetXmin());
	hout->SetDirectory(0);
	hout->GetXaxis()->SetTitle(hist->GetXaxis()->GetTitle());

	for (Int_t iBin = 1; iBin <= hist->GetNbinsX(); iBin++) {
		
		hout->SetBinContent(iBin, hist->GetBinContent(newFirstBin));
		hout->SetBinError(iBin, hist->GetBinError(newFirstBin));

		if (newFirstBin == hist->GetNbinsX()) {newFirstBin = 1;}
		else {newFirstBin++;}

	}

	return hout;

}

// -----------------------------------------------------------------------
TH1F* AliHistToolsDiHadronPID::MakeHistFromVector(const char* name, const char* title, const TVectorF &vector, Int_t domainInSigma, Int_t nBins) {

    /**
     * This method creates a histogram based on a TVector. It first determines the mean and 
     * RMS of the data set, and creates a histogram with a domain of mean +/- n*rms, where
     * n = domainInSigma.
     */

    // Create the histogram.
    Double_t mean = TMath::Mean(vector.GetNoElements(), &vector[0]);
    Double_t rms = TMath::RMS(vector.GetNoElements(), &vector[0]);
    TH1F* hist = new TH1F(name, title, nBins, mean - domainInSigma * rms, mean + domainInSigma * rms);

    // Fill the histogram.
    for (Int_t iElement = 0; iElement < vector.GetNoElements(); ++iElement) {hist->Fill(vector[iElement]);}

    return hist;

}

// -----------------------------------------------------------------------
TH1* AliHistToolsDiHadronPID::MakeHist(const char* name, const char* title, const char* type, const TAxis* xAxis, const TAxis* yAxis, const TAxis* zAxis) {

	TH1* hout = 0x0;

	// Type is for example Int_t, Float_t, etc.
	TString typeString(type);
	typeString.ToLower();

	// Add titles of axes.
	TString titleString(title);
	if (xAxis) {
		titleString.Append(Form(";%s", xAxis->GetTitle()));
		if (yAxis) {
			titleString.Append(Form(";%s", yAxis->GetTitle()));
			if (zAxis) {
				titleString.Append(Form(";%s", zAxis->GetTitle()));
			} 
		} 
	}

	// If a TAxis is constructed with the constructor TAxis(nbins, min, max), then
	// the method GetXbins() return an empty array. 
	const Double_t* xAxisArray;
	if (xAxis) {
		if ( !((xAxis->GetXbins())->GetSize()) ) {xAxisArray = CreateAxis(xAxis->GetNbins(), xAxis->GetXmin(), xAxis->GetXmax());}
		else {xAxisArray = (xAxis->GetXbins())->GetArray();}
	} else {xAxisArray = 0x0;}

	const Double_t* yAxisArray;
	if (yAxis) {
		if ( !((yAxis->GetXbins())->GetSize()) ) {yAxisArray = CreateAxis(yAxis->GetNbins(), yAxis->GetXmin(), yAxis->GetXmax());}
		else {yAxisArray = (yAxis->GetXbins())->GetArray();}
	} else {yAxisArray = 0x0;}

	const Double_t* zAxisArray;
	if (zAxis) {
		if ( !((zAxis->GetXbins())->GetSize()) ) {zAxisArray = CreateAxis(zAxis->GetNbins(), zAxis->GetXmin(), zAxis->GetXmax());}
		else {zAxisArray = (zAxis->GetXbins())->GetArray();}
	} else {zAxisArray = 0x0;}

	// Create the histogram.
	if (typeString.Contains("i")) {

		if (xAxis && yAxis && zAxis) {
			hout = new TH3I(name, titleString.Data(), 
				xAxis->GetNbins(), xAxisArray,
				yAxis->GetNbins(), yAxisArray,
				zAxis->GetNbins(), zAxisArray);
		} else if (xAxis && yAxis) {
			hout = new TH2I(name, titleString.Data(), 
				xAxis->GetNbins(), xAxisArray,
				yAxis->GetNbins(), yAxisArray);
		} else if (xAxis) {
			hout = new TH1I(name, titleString.Data(), 
				xAxis->GetNbins(), xAxisArray);
		} else {
			cout << "No axes specified..." << endl;
		}

	} else if (typeString.Contains("f")) {

		if (xAxis && yAxis && zAxis) {
			hout = new TH3F(name, titleString.Data(), 
				xAxis->GetNbins(), xAxisArray,
				yAxis->GetNbins(), yAxisArray,
				zAxis->GetNbins(), zAxisArray);
		} else if (xAxis && yAxis) {
			hout = new TH2F(name, titleString.Data(), 
				xAxis->GetNbins(), xAxisArray,
				yAxis->GetNbins(), yAxisArray);
		} else if (xAxis) {
			hout = new TH1F(name, titleString.Data(), 
				xAxis->GetNbins(), xAxisArray);
		} else {
			cout << "No axes specified..." << endl;
		}

	} else if (typeString.Contains("d")) {

		if (xAxis && yAxis && zAxis) {
			hout = new TH3D(name, titleString.Data(), 
				xAxis->GetNbins(), xAxisArray,
				yAxis->GetNbins(), yAxisArray,
				zAxis->GetNbins(), zAxisArray);
		} else if (xAxis && yAxis) {
			hout = new TH2D(name, titleString.Data(), 
				xAxis->GetNbins(), xAxisArray,
				yAxis->GetNbins(), yAxisArray);
		} else if (xAxis) {
			hout = new TH1D(name, titleString.Data(), 
				xAxis->GetNbins(), xAxisArray);
		} else {
			cout << "No axes specified..." << endl;
		}

	} else {
		cout << "Type not implemented..." << endl;
	}

	if (hout) {hout->SetDirectory(0);}

	return hout;

}

// -----------------------------------------------------------------------
THn* AliHistToolsDiHadronPID::MakeHist4D(const char* name, const char* title, const char* type, const TAxis* axis0, const TAxis* axis1, const TAxis* axis2, const TAxis* axis3) {

	// This method creates a 4D histogram of the desired type. At the moment,
	// only a histogram with floats can be chosen.
	THn* hout = 0x0;

	// The THn's constructor requires array's with min/max values for each dimension,
	// and the number of bins. As far as I know, the axes are not allowed to be variable.
	if (axis0) {
		if (axis0->IsVariableBinSize()) {
			AliLogDiHadronPID::Error(Form("axis0 has variable bin size."), __func__, "AliHistToolsDiHadronPID");
			return 0x0;
		}
	} else {
		AliLogDiHadronPID::Error(Form("axis0 points to 0x0."), __func__, "AliHistToolsDiHadronPID");
		return 0x0;
	}

	// Check axis1
	if (axis1) {
		if (axis1->IsVariableBinSize()) {
			AliLogDiHadronPID::Error(Form("axis1 has variable bin size."), __func__, "AliHistToolsDiHadronPID");
			return 0x0;
		}
	} else {
		AliLogDiHadronPID::Error(Form("axis1 points to 0x0."), __func__, "AliHistToolsDiHadronPID");
		return 0x0;
	}

	// Check axis2
	if (axis2) {
		if (axis2->IsVariableBinSize()) {
			AliLogDiHadronPID::Error(Form("axis2 has variable bin size."), __func__, "AliHistToolsDiHadronPID");
			return 0x0;
		}
	} else {
		AliLogDiHadronPID::Error(Form("axis2 points to 0x0."), __func__, "AliHistToolsDiHadronPID");
		return 0x0;
	}

	// Check axis3
	if (axis3) {
		if (axis3->IsVariableBinSize()) {
			AliLogDiHadronPID::Error(Form("axis3 has variable bin size."), __func__, "AliHistToolsDiHadronPID");
			return 0x0;
		}
	} else {
		AliLogDiHadronPID::Error(Form("axis3 points to 0x0."), __func__, "AliHistToolsDiHadronPID");
		return 0x0;
	}

	const Int_t nbins[4] = {axis0->GetNbins(), axis1->GetNbins(), axis2->GetNbins(), axis3->GetNbins()};
	const Double_t min[4] = {axis0->GetXmin(), axis1->GetXmin(), axis2->GetXmin(), axis3->GetXmin()};
	const Double_t max[4] = {axis0->GetXmax(), axis1->GetXmax(), axis2->GetXmax(), axis3->GetXmax()};

	// Check the type of the histogram, and create an instance.
	TString typeString(type);
	typeString.ToLower();

	if (typeString.Contains("f")) {

		hout = new THnF(name, title, 4, nbins, min, max);
	}

	// Put the correct axis titles.
	(hout->GetAxis(0))->SetTitle(axis0->GetTitle());
	(hout->GetAxis(1))->SetTitle(axis1->GetTitle());
	(hout->GetAxis(2))->SetTitle(axis2->GetTitle());
	(hout->GetAxis(3))->SetTitle(axis3->GetTitle());

	return hout;

}

// -----------------------------------------------------------------------
Double_t AliHistToolsDiHadronPID::GetSumHist(const TH1* hist) {

	Double_t count = 0.;
	for (Int_t iX = 1; iX <= hist->GetNbinsX(); iX++) {
		for (Int_t iY = 1; iY <= hist->GetNbinsY(); iY++) {
			for (Int_t iZ = 1; iZ <= hist->GetNbinsZ(); iZ++) {
				count += hist->GetBinContent(iX, iY, iZ);
			}		
		}			
	}

	return count;

}
// -----------------------------------------------------------------------
void AliHistToolsDiHadronPID::ScaleHistPt(TH1* hist) {
//
//  Multiply the bin content for each bin by the value on the x axis
//      Also scale error
//      Used for going from 1/pt dN/dpt to dN/dpt
//
	for (Int_t iX = 1; iX <= hist->GetNbinsX(); iX++) {
          Double_t err = hist->GetBinError(iX)*hist->GetBinCenter(iX);
          hist->SetBinContent(iX, hist->GetBinContent(iX)*hist->GetBinCenter(iX));
          hist->SetBinError(iX,err);
        }
}
// -----------------------------------------------------------------------
void AliHistToolsDiHadronPID::NormalizeHist(TH1* hist, Double_t value) {

	// Compute the integral of the histogram. Note that even 1D histograms
	// have three axes.

	Double_t count = 0.;
	for (Int_t iX = 1; iX <= hist->GetNbinsX(); iX++) {
		for (Int_t iY = 1; iY <= hist->GetNbinsY(); iY++) {
			for (Int_t iZ = 1; iZ <= hist->GetNbinsZ(); iZ++) {
				count += hist->GetBinContent(iX, iY, iZ);
			}		
		}			
	}

	if (count > 1.e-100) {hist->Scale(value / count);}

}

// -----------------------------------------------------------------------
TH1F* AliHistToolsDiHadronPID::MakeHist1D(const char* name, const char* title, const TAxis* xaxis) {

	TH1F* fout = new TH1F(name, title, 
		xaxis->GetNbins(), (xaxis->GetXbins())->GetArray());
	fout->SetDirectory(0);
	return fout;
	
}

// -----------------------------------------------------------------------
TH2F* AliHistToolsDiHadronPID::MakeHist2D(const char* name, const char* title, const TAxis* xaxis, const TAxis* yaxis) {

	TH2F* fout = new TH2F(name, title, 
		xaxis->GetNbins(), (xaxis->GetXbins())->GetArray(), 
		yaxis->GetNbins(), (yaxis->GetXbins())->GetArray());
	fout->SetDirectory(0);
	return fout;

}

// -----------------------------------------------------------------------
TH3F* AliHistToolsDiHadronPID::MakeHist3D(const char* name, const char* title, const TAxis* xaxis, const TAxis* yaxis, const TAxis* zaxis) {

	TH3F* fout = new TH3F(name, title, 
		xaxis->GetNbins(), (xaxis->GetXbins())->GetArray(), 
		yaxis->GetNbins(), (yaxis->GetXbins())->GetArray(), 
		zaxis->GetNbins(), (zaxis->GetXbins())->GetArray());
	fout->SetDirectory(0);
	return fout;
	
}

// -----------------------------------------------------------------------
TH3F* AliHistToolsDiHadronPID::MakeHist3D(const char* name, const char* title, 
	Int_t nbinsX, Double_t minX, Double_t maxX,
	Int_t nbinsY, Double_t minY, Double_t maxY,
	Int_t nbinsZ, const Double_t* zaxis) {

	const Double_t* xaxis = const_cast<Double_t*>(CreateAxis(nbinsX,minX,maxX));
	const Double_t* yaxis = const_cast<Double_t*>(CreateAxis(nbinsY,minY,maxY));

	TH3F* hout = new TH3F(name,title,nbinsX,xaxis,nbinsY,yaxis,nbinsZ,zaxis);

	return hout;

}


// -----------------------------------------------------------------------
TH2F* AliHistToolsDiHadronPID::Function2DToHist2D(const TF2* function, const TH2* grid) {

	// Creates a 2D histogram of "function" using the binning of 
	// the histogram "grid". 
	//  - We assume "grid" to have fixed binwidth.
	//  - Bins are only filled if they are within the domain of the function.
	//  - Histogram takes over the color of the function.

	// Gathering info about the axes.
	Int_t NbinsX = grid->GetXaxis()->GetNbins();
	Int_t NbinsY = grid->GetYaxis()->GetNbins();

	// Determining function range.
	Double_t Xmin = 0.; 
	Double_t Xmax = 0.; 
	Double_t Ymin = 0.; 
	Double_t Ymax = 0.;
	function->GetRange(Xmin, Ymin, Xmax, Ymax);

	// Creating the histogram.
	TH2F* hout = new TH2F(Form("%s_hist", function->GetName()),
		Form("%s (Hist);%s;%s", function->GetTitle(), grid->GetXaxis()->GetTitle(), grid->GetYaxis()->GetTitle()),
		NbinsX, grid->GetXaxis()->GetBinLowEdge(1), grid->GetXaxis()->GetBinUpEdge(NbinsX),
		NbinsY, grid->GetYaxis()->GetBinLowEdge(1), grid->GetYaxis()->GetBinUpEdge(NbinsY));
	hout->SetLineColor(function->GetLineColor());
	hout->SetMarkerColor(function->GetLineColor());
	hout->SetDirectory(0);

	// Filling the histogram.
	for (Int_t iBinX = 1; iBinX < (NbinsX + 1); iBinX++) {
		for (Int_t iBinY = 1; iBinY < (NbinsY + 1); iBinY++) {
			Double_t CoordX = grid->GetXaxis()->GetBinCenter(iBinX);
			Double_t CoordY = grid->GetYaxis()->GetBinCenter(iBinY);

			if ( (CoordX < Xmin) || (CoordX > Xmax) || (CoordY < Ymin) || (CoordY > Ymax) ) {continue;}

			hout->SetBinContent(iBinX, iBinY, function->Eval(CoordX, CoordY));
		}		
	}

	return hout;

}

// -----------------------------------------------------------------------
Double_t* AliHistToolsDiHadronPID::CreateAxis(Int_t nbins, Double_t min, Double_t max) {

	if (nbins <= 0) return 0x0;
	if (max < min) return 0x0;

	Double_t* axis = new Double_t[nbins + 1];
	Double_t binsize = (max - min)/((Double_t)nbins);
	for (Int_t iBin = 0; iBin < nbins + 1; iBin++) {
		axis[iBin] = min + ((Double_t)iBin) * binsize;
	}

	return axis;
}

// -----------------------------------------------------------------------
TAxis* AliHistToolsDiHadronPID::CreateTAxis(Int_t nbins, Double_t min, Double_t max) {

	Double_t* axis = CreateAxis(nbins, min, max);
	if (axis) {
		TAxis* aout = new TAxis(nbins, axis);
		delete axis;
		return aout;
	} 

	return 0x0;

}

// -----------------------------------------------------------------------
TArrayD* AliHistToolsDiHadronPID::CreateTArrayD(Int_t size, Double_t min, Double_t max) {

	Double_t* axis = CreateAxis(size - 1, min, max);
	if (axis) {
		TArrayD* aout = new TArrayD(size, axis);
		delete axis;
		return aout;
	}

	return 0x0;

}

// -----------------------------------------------------------------------
void AliHistToolsDiHadronPID::PrintTAxis(const TAxis* axis) {

	if (axis->IsVariableBinSize()) {
		for (Int_t iBin = 1; iBin <= axis->GetNbins(); iBin++) {
			cout << axis->GetBinLowEdge(iBin) << ", ";
		}

		cout << axis->GetBinUpEdge(axis->GetNbins()) << endl;
	} else {
		cout << axis->GetNbins() << " bins, (" << axis->GetXmin() << ", " << axis->GetXmax() << ")." << endl;
	}

}

// -----------------------------------------------------------------------
TH1F* AliHistToolsDiHadronPID::SplineToHist(const TSpline* spline, const TAxis* axis) {

	// This method takes a spline, and fills a histogram using the provided axis.
	// Bin content is determined by taking an integral under the spline.
	TH1F* hOut = (TH1F*)MakeHist(spline->GetName(), spline->GetTitle(), "F", axis);
	for (Int_t iBin = 1; iBin <= hOut->GetNbinsX(); iBin++) {

		// cout << axis->GetBinLowEdge(iBin) << " " << axis->GetBinUpEdge(iBin) << endl;
		hOut->SetBinContent(iBin, AliMiscToolsDiHadronPID::SplineIntegral(spline, axis->GetBinLowEdge(iBin), axis->GetBinUpEdge(iBin), 100));
		hOut->SetBinError(iBin, 0.);
	}

	hOut->Scale(1., "width");

	return hOut;

}

// -----------------------------------------------------------------------
void AliHistToolsDiHadronPID::DivideThroughSpline(TH1F* hist, const TSpline* spline) {

	// hist / spline.
	TH1F* hSpline = SplineToHist(spline, hist);
	hist->Divide(hSpline);

	delete hSpline;

}

// -----------------------------------------------------------------------
TGraphAsymmErrors* AliHistToolsDiHadronPID::AddSystematics(const TH1F* histo, const TGraphAsymmErrors* grSyst) {
   // Add systematic errors to a TH1F. Returns a TGraphAsymmErrors with the systematics
   // The input grSyst has points with y=1 and errro up and down are relative
   
   TGraphAsymmErrors *retGr = new TGraphAsymmErrors;
   if (grSyst->GetN() !=  histo->GetNbinsX()) {
      cout << "ERROR in " << __PRETTY_FUNCTION__ << " number of bins does not match: histo " << histo->GetNbinsX() << " graph " << grSyst->GetN() << endl;
      return 0;
   }
   for (Int_t iBin = 1; iBin <= histo->GetNbinsX(); iBin++) {
     Double_t x, yval;
     grSyst->GetPoint(iBin-1, x, yval);
     if (fabs(histo->GetBinCenter(iBin)/x-1) > 1e-6) {
        cout << "ERROR in " << __PRETTY_FUNCTION__ << " bin " << iBin << " center hist bin " << histo->GetBinCenter(iBin) << " xval " << x << endl;
        continue;
     }
     retGr->SetPoint(iBin-1,histo->GetBinCenter(iBin), histo->GetBinContent(iBin));
     retGr->SetPointEXlow(iBin-1,histo->GetBinCenter(iBin)-histo->GetBinLowEdge(iBin));
     retGr->SetPointEXhigh(iBin-1,histo->GetXaxis()->GetBinUpEdge(iBin)-histo->GetBinCenter(iBin));
     retGr->SetPointEYhigh(iBin-1,grSyst->GetErrorYhigh(iBin-1)*histo->GetBinContent(iBin));
     retGr->SetPointEYlow(iBin-1,grSyst->GetErrorYlow(iBin-1)*histo->GetBinContent(iBin));
   }
   return retGr;
}

// -----------------------------------------------------------------------
void AliHistToolsDiHadronPID::ConstFit(const TH1* hist, Double_t& fit, Double_t& fiterr) {

	// For the classes TH1F, TH1D, etc. you can call GetArray(), and in principle
	// this could be passed to ConstFit(). This array contains under and overflow however,
	// so we need to create a new array.

	// Create new arrays.
	Int_t nBins = hist->GetNbinsX() * hist->GetNbinsY() * hist->GetNbinsZ();
	//cout << nBins << endl;
	Double_t data[nBins];
	Double_t errors[nBins];
	for (Int_t iBinX = 1; iBinX <= hist->GetNbinsX(); iBinX++) {
		for (Int_t iBinY = 1; iBinY <= hist->GetNbinsY(); iBinY++) {
			for (Int_t iBinZ = 1; iBinZ <= hist->GetNbinsZ(); iBinZ++) {
			
				Int_t index = (iBinZ - 1) + hist->GetNbinsZ() * (iBinY - 1) + hist->GetNbinsZ() * hist->GetNbinsY() * (iBinX - 1);
				//cout << index << endl;
				data[index] = hist->GetBinContent(iBinX, iBinY, iBinZ);
				errors[index] = hist->GetBinError(iBinX, iBinY, iBinZ);

			}
		}
	}

	AliFunctionsDiHadronPID::ConstFit(data, errors, nBins, fit, fiterr);

}

// -----------------------------------------------------------------------
void AliHistToolsDiHadronPID::ConstMinusHist(TH1F* hist, Float_t cc) {

	// h -> (c-h)
	Int_t nbins = hist->GetNbinsX();
	for (Int_t iBin = 0; iBin < (nbins + 1); iBin++) {
		Float_t bincontent = hist->GetBinContent(iBin);
		hist->SetBinContent(iBin,(cc - bincontent));
	}

}

// -----------------------------------------------------------------------
void AliHistToolsDiHadronPID::NormaliseMixedEvents(TH2* hist, TString option) {

    /**
	 * This method normalises the mixed event histogram. One can choose one 
     * of the three following options:
     *  - ORIGIN: Set the mixed events to 1 at the origin.
     *  - STRIP:  Set the average within the strip around DEta = 0 to 1.
     *  - ALL:    Scale the mixed events with 2<ME>, where <> denotes the 
     *            average value.
     *
     *  Note that if the mixed events are a perfect triangle, then all three
     *  methods should give exactly the same result (run TestNormaliseMixedEvents.C
     *  to check this).
     *
     *  The preferred option is "ALL", since that way the trigger efficiency
     *  completely drops out, and we can simply scale any final results with
     *  the average single-particle efficiency. Furthermore, the ALL method
     *  does not suffer from the additional ~1/Nbins error in the correction factor
     *  that is caused by the discretisation of the triangle.
     *
     *  This method assumes X=DPhi, Y=DEta.
     */

    // Argument parsing.
    option.ToLower();
    Bool_t selectedOption[3] = {option.Contains("origin"), option.Contains("strip"), option.Contains("all")};
    if (!selectedOption[0] && !selectedOption[1] && !selectedOption[2]) {
        AliLogDiHadronPID::Error("No valid option given, choose: ORIGIN, STRIP, or ALL.", __func__, "AliHistToolsDiHadronPID");
        return;
    }

    if ((selectedOption[0] && selectedOption[1]) ||
        (selectedOption[0] && selectedOption[2]) ||
        (selectedOption[1] && selectedOption[2])) {
        AliLogDiHadronPID::Error("More than one valid option chosen.", __func__, "AliHistToolsDiHadronPID");
        return;
    }

    if (!hist) {
        AliLogDiHadronPID::Error("Don't give me hist=0x0.", __func__, "AliHistToolsDiHadronPID");
        return;
    }

	// Get a ptr to the Y axis.
#ifdef __CLING__
        const TAxis* aDPhi = hist->GetXaxis();
        const TAxis* aDEta = hist->GetYaxis();
#else
        TAxis* aDPhi = hist->GetXaxis();
        TAxis* aDEta = hist->GetYaxis();
#endif

	TString axisTitle(aDEta->GetTitle());
	axisTitle.ToLower();
	if (axisTitle.Contains("phi")) {
		AliLogDiHadronPID::Error(Form("It seems like the Y axis is the DPhi axis..."), __func__, "AliHistToolsDiHadronPID");
		return;
	}

	// Find the bin range in DEta.
	Int_t minBinDEta = aDEta->FindBin(-1.e-4);
	Int_t maxBinDEta = aDEta->FindBin(1.e-4);
	Int_t minBinDPhi = aDPhi->FindBin(-1.e-4);
	Int_t maxBinDPhi = aDPhi->FindBin(1.e-4);
	if (option.Contains("strip") || option.Contains("all")) {
		minBinDPhi = 1;
		maxBinDPhi = aDPhi->GetNbins();
	}
    if (option.Contains("all")) {
        minBinDEta = 1;
        maxBinDEta = aDEta->GetNbins();
    }

	// Calculate the sum over the strip.
	Double_t sum = 0.;
	Double_t nBins = 0.;
	for (Int_t iDPhiBin = minBinDPhi; iDPhiBin <= maxBinDPhi; iDPhiBin++) {
		for (Int_t iDEtaBin = minBinDEta; iDEtaBin <= maxBinDEta; iDEtaBin++) {
			sum += hist->GetBinContent(iDPhiBin, iDEtaBin);
			nBins++;
		}
	}

	// Perform the scaling.
	sum /= nBins;
	if (sum > 10e-10) {
		hist->Scale(option.Contains("all") ? 1./(2.*sum) : 1./sum);
	} else {
		AliLogDiHadronPID::Warning(Form("Cannot divide through 0..."), __func__, "AliHistToolsDiHadronPID");
	}

}

// -----------------------------------------------------------------------
void AliHistToolsDiHadronPID::RandomizeHist(TH1* hist) {

	// This method takes a histogram, and varies each bin according to their
	// statistical error.  
	TRandom3* rnd = new TRandom3();
	rnd->SetSeed(0);
	
	for (Int_t iBinX = 1; iBinX <= hist->GetNbinsX(); iBinX++) {
		for (Int_t iBinY = 1; iBinY <= hist->GetNbinsY(); iBinY++) {
			for (Int_t iBinZ = 1; iBinZ <= hist->GetNbinsZ(); iBinZ++) {
					
				hist->SetBinContent(iBinX, iBinY, iBinZ,
					rnd->Gaus(hist->GetBinContent(iBinX, iBinY, iBinZ),
						hist->GetBinError(iBinX, iBinY, iBinZ)));
			
			}
		}
	}

	delete rnd;

}

// -----------------------------------------------------------------------
void AliHistToolsDiHadronPID::RandomizeCollectionOfHist(TCollection* collection) {

	// This method randomises all the histograms in the supplied array.
	TIter next(collection);
	TH1* hist = 0x0;

	while ( (hist = (TH1*)next()) ) {
		RandomizeHist(hist);
	}

}

// -----------------------------------------------------------------------
/* ONGOING WORK
Bool_t AliHistToolsDiHadronPID::OperateWithTObjArg(TObject* hist1, const TObject* hist2, Operation operation, const TString& options) {

	// This function is meant to call more specialized functions that can deal with the
	// types presented. For example, if hist1 is a THn, and hist2 is a TH1, then the
	// user is redirected to a function specialised in dealing with this.
	// Operation has to be specified (kMult, kDiv, kSub, kAdd).
	// options is a void pointer, and may or may not be required, depending on the 
	// specific objects hist1, and hist2.

	TString cl1((hist1->Class())->GetName());
	TString cl2((hist2->Class())->GetName());

	// Redirecting to specialized functions.
	if (cl1.BeginsWith("THn") && cl2.BeginsWith("TH") && (!cl2.BeginsWith("THn"))) {
		return OperateWithTH1onTHn((THn*)hist1, (const TH1*)hist2, operation, options);
	}
	
	cout << Form("%s::%s -> Operation of type %s(%s) not valid or not implemented.", __FILE__, __func__, cl2.Data(), cl1.Data()) << endl;
	return kFALSE;

}
*/
// -----------------------------------------------------------------------
TCanvas* AliHistToolsDiHadronPID::CreateSpectraComparison(const char* name, 
	const char* title, const TH1F* h1, const TH1F* h2, Int_t markerstyle, Bool_t logy) {

	// - Creates a window comparing two histograms h1, and h2.
	// - Returns an array of pointers to the objects created
	//   in this function.

	//Int_t optstat = gStyle->GetOptStat();
	gStyle->SetOptStat(0);
	//c->UseCurrentStyle();
	//gStyle->SetOptStat(optstat);

	TH1F* spectra[2];
	TH1F* division;
	Int_t spectracolor[2] = {1,2};
	Int_t divisioncolor = 4;

	// Cloning and configuring spectra.
	spectra[0] = (TH1F*)h1->Clone();
	//spectra[0]->SetDirectory(0);
	spectra[1] = (TH1F*)h2->Clone();
	//spactra[1]->SetDirectory(0);	
	for (Int_t iSpectra = 0; iSpectra < 2; iSpectra++) {
		spectra[iSpectra]->Sumw2();
		spectra[iSpectra]->SetMarkerStyle(markerstyle);
		spectra[iSpectra]->SetLineColor(spectracolor[iSpectra]);
		spectra[iSpectra]->SetMarkerColor(spectracolor[iSpectra]);
	}

	// Creating the division.
	division = (TH1F*)spectra[0]->Clone("hDivision");
	division->Divide(spectra[1]);
	division->SetLineColor(divisioncolor);
	division->SetMarkerColor(divisioncolor);
	division->GetYaxis()->SetTitle("Ratio");

	// Creating window
	//TCanvas* c = new TCanvas(name,title,0,0,400,400);
	TCanvas* c = TCanvas::MakeDefCanvas();
	c->SetName(name);
	c->SetTitle(title);
	TPad* p1 = new TPad("p1","pad1",0,0.25,1,1);
	p1->SetRightMargin(0.05);
	p1->SetBottomMargin(0.);
	TPad* p2 = new TPad("p2","pad2",0,0,1,0.25);
	p2->SetTopMargin(0.);
	p2->SetRightMargin(0.05);
	p2->SetBottomMargin(0.3);
	p2->SetFillStyle(4000);
	p1->Draw();
	p2->Draw();

	// Determining plotting range.
	Double_t max = TMath::Max(spectra[0]->GetMaximum(),spectra[1]->GetMaximum());
	Double_t min = TMath::Min(spectra[0]->GetMinimum(),spectra[1]->GetMinimum());
	Double_t range = max-min;
	spectra[0]->SetMinimum(min-0.05*range);
	spectra[0]->SetMaximum(max+0.05*range);

	// Drawing
	p1->cd();
	if (logy) {p1->SetLogy();}
	spectra[0]->Draw("e");
	spectra[0]->GetXaxis()->SetLabelSize(0);
	spectra[0]->GetYaxis()->SetLabelSize(0.05);
	spectra[0]->GetYaxis()->SetTitleSize(0.05);
	spectra[0]->GetYaxis()->SetTitleOffset(0.8);
	//Double_t labelsize = spectra[0]->GetYaxis()->GetLabelSize();
	spectra[1]->Draw("same e");
	p2->cd();
	division->SetTitle("");
	division->GetXaxis()->SetLabelSize(0.1);
	division->GetXaxis()->SetTitleSize(0.15);
	division->GetXaxis()->SetTitleOffset(0.6);
	division->GetYaxis()->SetLabelSize(0.1);
	division->GetYaxis()->SetTitleSize(0.12);
	division->GetYaxis()->SetTitleOffset(0.25);
	division->SetMinimum(0.8);
	division->SetMaximum(1.2);
	division->Draw("e");

	//c->UseCurrentStyle();

	//gStyle->SetOptStat(optstat);

	TLegend* legend = new TLegend(0.75,0.8,0.95,0.95);
	legend->AddEntry(spectra[0],h1->GetTitle(),"lpe");
	legend->AddEntry(spectra[1],h2->GetTitle(),"lpe");
	p1->cd();
	legend->Draw("same");

/*
	// returning the created objects.
	TObjArray* returnobjects = new TObjArray(6);
	returnobjects->AddLast(c);
	returnobjects->AddLast(p1);
	returnobjects->AddLast(p2);
	returnobjects->AddLast(spectra[0]);
	returnobjects->AddLast(spectra[1]);
	returnobjects->AddLast(division);
*/

	return c;

}

// -----------------------------------------------------------------------
void AliHistToolsDiHadronPID::FixPlotRange(TCanvas* cvs) {

	// This method looks through the list of primitives in the canvas, 
	// and makes sure the minimum and maximum are set such that every
	// object is visible.

	// Get a ptr to the first object that was plotted in the canvas.
	// This object determines the plot range on the y-axis.
	TObject* firstObj = (cvs->GetListOfPrimitives())->First();
	if (!TString(firstObj->ClassName()).Contains("TH1")) {
		AliLogDiHadronPID::Error(Form("This method only works if the first object plotted derives from TH1."), __func__, "AliHistToolsDiHadronPID");
		return;
	}

	// Record the original minimum and maximum.
	TH1* firstHist = (TH1*)firstObj;
	Double_t plotMin = firstHist->GetMinimum();
	Double_t plotMax = firstHist->GetMaximum();

	// Create iterator.
	TList* listOfPrimitivesCpy = new TList();	
	TIter next(cvs->GetListOfPrimitives());
	while (TObject* obj = next()) {

		listOfPrimitivesCpy->AddLast(obj);

		if (TString(obj->ClassName()).Contains("TH1")) {
			plotMin = TMath::Min(((TH1*)obj)->GetMinimum(), plotMin);
			plotMax = TMath::Max(((TH1*)obj)->GetMaximum(), plotMax);			
		} else if (TString(obj->ClassName()).Contains("TF1")) {
			plotMin = TMath::Min(((TF1*)obj)->GetMinimum(), plotMin);
			plotMax = TMath::Max(((TF1*)obj)->GetMaximum(), plotMax);	
		}

	}
	
	// Make sure log scales are not set below 1.
	if (cvs->GetLogy()) {
		if (plotMin < 1.) {plotMin = 1.;}
		plotMin *= 0.5; 
		plotMax *= 2;
	} else {
		Double_t plotRange = plotMax - plotMin;
		plotMin -= plotRange * 0.1;
		plotMax += plotRange * 0.1;		
	}

	firstHist->SetMinimum(plotMin);
	firstHist->SetMaximum(plotMax);

	// Redraw.
	cvs->cd();
	TIter next2(listOfPrimitivesCpy);

	while (TObject* obj = next2()) {	
		obj->Draw((obj == listOfPrimitivesCpy->First()) ? "" : "same");
	}

	delete listOfPrimitivesCpy;

}

// -----------------------------------------------------------------------
void AliHistToolsDiHadronPID::ConvertAllTH1sToCMacro(const char* fileNameIn, const char* fileNameOut) {

	/**
	 * Convert all TH1's in a file to a C file with methods that return instances of TGraphAsymmErrors.
	 * \param fileNameIn ROOT file.
	 * \param fileNameOut C file.
	 */

	TFile* fIn = TFile::Open(fileNameIn);
	if (fIn) {
		TIter next(fIn->GetListOfKeys());
		while (TKey* currentKey = (TKey*)next()) {
			if ( TString(currentKey->GetClassName()).Contains("TH1") ) {
				TH1* currentHist = (TH1*)currentKey->ReadObj();
			
				// Write the beginning of the method.
				ofstream outStream1;
				outStream1.open(fileNameOut, ios::out | ios::app); // ios::app specifies that we want to append to the file.
				if (outStream1.is_open()) {
					outStream1 << "// -----------------------------------------------------------------------\n";
					outStream1 << Form("TGraphAsymmErrors* %s() {\n\n", currentHist->GetName());
				}
				outStream1.close();

				// Write the histogram data.
				ConvertHistogramToTextFile(currentHist, fileNameOut, "double");

				// Write the end of the method.
				ofstream outStream2;
				outStream2.open(fileNameOut, ios::out | ios::app); // ios::app specifies that we want to append to the file.
				if (outStream2.is_open()) {
					outStream2 << "\n";
					outStream2 << Form("\tint numpoints = %i;\n", currentHist->GetNbinsX());
					outStream2 << "\tTGraphAsymmErrors* graph = new TGraphAsymmErrors(numpoints, xval, yval, xerrminus, xerrplus, yerrminus, yerrplus);\n";
					outStream2 << "\tMakeUpGraph(graph, 21, kBlue);\n";
  					outStream2 << "\treturn graph;\n\n";
  					outStream2 << "}\n\n";

				}
				outStream2.close();

			}	
		}
	}

	return;

}

// -----------------------------------------------------------------------
void AliHistToolsDiHadronPID::ConvertHistogramToTextFile(const TH1* hist, const char* fileName, const char* type, Bool_t indent) {

	/**
	 * This method takes a histogram, and converts it to a text file, where the 
	 * values are written in standard C array format, i.e.: 
	 * \code 
	 * type arrayName[nBins] = { x1, x2, ...}
	 * \endcode
 	 *
	 * \param hist Pointer to the histogram
	 * \param fileName Name of output file
	 * \param type The type of the values, i.e., double, Float_t, etc.
	 */

	if (!hist) {return;}
	const Int_t nBinsX = hist->GetXaxis()->GetNbins();
	 
	Double_t xx[nBinsX];
	Double_t xx_edown[nBinsX];
	Double_t xx_eup[nBinsX];

	Double_t yy[nBinsX];
	Double_t yy_edown[nBinsX];
	Double_t yy_eup[nBinsX];

	// Loop over bins.
	for (Int_t iBinX = 1; iBinX <= nBinsX; iBinX++) {
		xx[iBinX - 1] = hist->GetXaxis()->GetBinCenter(iBinX);
		xx_edown[iBinX - 1] = hist->GetXaxis()->GetBinCenter(iBinX) - hist->GetXaxis()->GetBinLowEdge(iBinX);
		xx_eup[iBinX - 1] = hist->GetXaxis()->GetBinUpEdge(iBinX) - hist->GetXaxis()->GetBinCenter(iBinX);

		yy[iBinX - 1] = hist->GetBinContent(iBinX);
		yy_edown[iBinX - 1] = hist->GetBinError(iBinX);
		yy_eup[iBinX - 1] = hist->GetBinError(iBinX);

	}

	WriteArrayToFile("xval", xx, nBinsX, fileName, type, indent);
	WriteArrayToFile("xerrplus", xx_edown, nBinsX, fileName, type, indent);
	WriteArrayToFile("xerrminus", xx_eup, nBinsX, fileName, type, indent);

	WriteArrayToFile("yval", yy, nBinsX, fileName, type, indent);
	WriteArrayToFile("yerrplus", yy_edown, nBinsX, fileName, type, indent);
	WriteArrayToFile("yerrminus", yy_eup, nBinsX, fileName, type, indent);

	return;

}

// -----------------------------------------------------------------------
void AliHistToolsDiHadronPID::WriteArrayToFile(const char* arrayName, const Double_t* array, Int_t nBins, const char* fileName, const char* type, Bool_t indent) {

	/**
	 * Private method that writes an array to a file in the format:
	 * \code 
	 * type arrayName[nBins] = { x1, x2, ...}
	 * \endcode
	 */

	ofstream outStream;
	outStream.open(fileName, ios::out | ios::app); // ios::app specifies that we want to append to the file.

	if (outStream.is_open()) {

		outStream << (indent ? "\t" : "") << type << " " << arrayName << "[] = { ";
		for (Int_t iBin = 1; iBin <= nBins; iBin++) {
			outStream << Form("%e", array[iBin - 1]);
			if (iBin != nBins) {outStream << ", ";}
		} 
		outStream << "};\n";

	}

	outStream.close();

	return;

}

// -----------------------------------------------------------------------
Bool_t AliHistToolsDiHadronPID::DoAxesMatch(const TAxis* axis1, const TAxis* axis2) {

	// If a quick check is requested, then only the range of the axis is
	// compared. The full check will check the range of every bin.
	const Double_t eps = 10e-10;
	if (axis1->GetNbins() != axis2->GetNbins()) {return kFALSE;}

	for (Int_t iBin = 1; iBin <= (axis1->GetNbins() + 1); ++iBin) {
		if ((axis1->GetBinLowEdge(iBin) + eps) < axis2->GetBinLowEdge(iBin) ||
			(axis1->GetBinLowEdge(iBin) - eps) > axis2->GetBinLowEdge(iBin)) {return kFALSE;}
	}

	return kTRUE;

}

// -----------------------------------------------------------------------
void AliHistToolsDiHadronPID::AverageX(const TH1* hist, Double_t& avg, Double_t& error) {

	// This method calculates sum(x_i*y_i) / sum(y_i), where the sum loops over all bins,
	// i.e., the average x, weighted with the bin content.

	// The stat. uncertainty is propagated in the usual way, leading to:
	// e_sum^2 = ((1+avg)/sumY)^2 * sum(x_i^2 * ey_i^2) 

	Double_t sumY = 0.;
	Double_t error2 = 0.;

	// Calculate average.
	for (Int_t iBin = 1; iBin <= hist->GetNbinsX(); iBin++) {
			
		Double_t xx = hist->GetXaxis()->GetBinCenter(iBin);
		Double_t yy = hist->GetBinContent(iBin);
		Double_t ey = hist->GetBinError(iBin);

		avg += (xx * yy);
		sumY += (yy);
		error2 += (xx * xx * ey * ey);
	
	}

	avg = (avg / sumY);

	error2 *= ((1+avg)*(1+avg)/(sumY * sumY));
	error = TMath::Sqrt(error2);

}

// -----------------------------------------------------------------------
void AliHistToolsDiHadronPID::RMS(const TH1* hist, Double_t& rms, Double_t& error) {

	// Calculate the variance of a histogram (RMS), i.e., sum(p_i(x_i - <x>)^2),
	// where p_i == y_i/y_tot (i.e. the probability).

	// The error propagation is done as folows:
	//

	Double_t rms2 = 0.;
	Double_t sumY = 0.;
	Double_t firstMom = 0.;

	Double_t avg = 0.;
	Double_t avgError = 0.;

	AverageX(hist, avg, avgError);

	for (Int_t iBin = 1; iBin <= hist->GetNbinsX(); iBin++) {
			
		Double_t xx = hist->GetXaxis()->GetBinCenter(iBin);
		Double_t yy = hist->GetBinContent(iBin);

		// Calculate 0th, 1st and 2nd moments.
		sumY += yy;
		firstMom += yy * (xx - avg);
		rms2 += yy * TMath::Power((xx - avg), 2);

	}

	// Make sure that the sum of probabilities (y coord) is 1.
	firstMom /= sumY;
	rms2 /= sumY; 

	rms = TMath::Sqrt(rms2);

	Double_t error2 = 0.;
	for (Int_t iBin = 1; iBin <= hist->GetNbinsX(); iBin++) {

		Double_t xx = hist->GetXaxis()->GetBinCenter(iBin);
		Double_t ey = hist->GetBinError(iBin);

		Double_t derivative = -rms / sumY 
			+ (xx - avg) * (xx - avg) / sumY 
			- 2 * firstMom * (xx - avg) / sumY;

		error2 += TMath::Power(derivative * ey, 2);

	}

	error = TMath::Sqrt(error2);

}

// -----------------------------------------------------------------------
void AliHistToolsDiHadronPID::CountInEllips(const TH2* hist, Double_t rX, Double_t rY, Double_t& count, Double_t& error, Bool_t isDensity) {

	/**
	 * Calculates the yield in an ellips around \f$(0,0)\f$ in a 2D histogram.
	 *
	 * \param hist 2D input histogram.
	 * \param rX Radius in x-direction.
	 * \param rY Radius in y-direction.
	 * \param count The counted number
	 * \param error Uncertainty on the count (assuming no correlation between bins).
	 * \param isDensity If true, then the result (also uncertainty) is scaled with the binsize,
	 * to get a dimensionless quantity. 
	 */

	// Some sanity checks.
	if (!hist) {
		AliLogDiHadronPID::Error("No histogram provided.", __func__, "AliHistToolsDiHadronPID");
		return;
	}

	// if (hist->GetXaxis()->IsVariableBinSize() || hist->GetYaxis()->IsVariableBinSize()) {
	// 	AliLogDiHadronPID::Error("Probably not good to use this method on a histogram with variable bin size.", __func__, "AliHistToolsDiHadronPID");
	// 	return;
	// }

	// Loop over all bins that fall within the jet radius.
	count = 0.;
	error = 0.;

	for (Int_t iBinX = 1; iBinX <= hist->GetNbinsX(); iBinX++) {
		
		Double_t binCenterX = hist->GetXaxis()->GetBinCenter(iBinX);
		for (Int_t iBinY = 1; iBinY <= hist->GetNbinsY(); iBinY++) {
		
			Double_t binCenterY = hist->GetYaxis()->GetBinCenter(iBinY);

			if (TMath::Power(binCenterX / rX, 2) + 
				TMath::Power(binCenterY / rY, 2) > 1.) {continue;}

			count += hist->GetBinContent(iBinX, iBinY);
			error += (hist->GetBinError(iBinX, iBinY) * hist->GetBinError(iBinX, iBinY));

		}
	}

	error = TMath::Sqrt(error);

	// Convert density into counts.
	if (isDensity) {
		Double_t binSurface = hist->GetXaxis()->GetBinWidth(1) * hist->GetYaxis()->GetBinWidth(1);
		count *= binSurface;
		error *= binSurface;
	}

}

// -----------------------------------------------------------------------
TH1F* AliHistToolsDiHadronPID::ProjectionY_RMSError(const TH2F* hist, Int_t minBinX, Int_t maxBinX) {

	// This method makes a projection onto the x or y axis, where the error
	// is defined as the RMS of the points. I.e., it makes sense to use this 
	// method if all points are expected to be the same.

	TH1F* hout = (TH1F*)MakeHist(Form("%s_ProjY", hist->GetName()), Form("%s ProjY", hist->GetTitle()), "F", hist->GetYaxis());

	if (minBinX < 0) {minBinX = 1;}
	if (maxBinX < 0) {maxBinX = hist->GetNbinsX();}

	for (Int_t iBinY = 1; iBinY <= hist->GetNbinsX(); iBinY++) {			

		// Calculate Average
		Double_t avg = 0.;
		Double_t nBins = 0.;
		for (Int_t iBinX = minBinX; iBinX <= maxBinX; iBinX++) {
			avg += hist->GetBinContent(iBinX, iBinY);
			nBins++;
		}
		avg /= nBins;
		hout->SetBinContent(iBinY, avg);

		// Calculate the RMS.
		Double_t rms = 0.;
		for (Int_t iBinX = minBinX; iBinX <= maxBinX; iBinX++) {
			rms += TMath::Power(avg - hist->GetBinContent(iBinX, iBinY), 2.);
		}
		rms /= nBins;
		hout->SetBinError(iBinY, rms);

	} 

	return hout;

}

// -----------------------------------------------------------------------
TH1F* AliHistToolsDiHadronPID::Ratio2DProjectX(const TH2F* h1, const TH2F* h2) {

	// This method creates a ratio of h1 and h2, and projects this onto the X axis.
	// It is assumed that the two histogams are correlated, and the reported error
	// is the RMS of the deviations.

	// Create projected histogram.
	TH1F* hout = (TH1F*)MakeHist("test","test","F",h1->GetXaxis());

	for (Int_t iBinX = 1; iBinX <= h1->GetNbinsX(); iBinX++) {

		Double_t avg = 0.;
		Double_t rms = 0.;

		// Calculate the average ratio.
		for (Int_t iBinY = 1; iBinY <= h1->GetNbinsY(); iBinY++) {
			avg += (h1->GetBinContent(iBinX, iBinY) / h2->GetBinContent(iBinX, iBinY));
		}
		avg /= h1->GetNbinsY();
		hout->SetBinContent(iBinX, avg);

		// Calculate the RMS.
		for (Int_t iBinY = 1; iBinY <= h1->GetNbinsY(); iBinY++) {
			Double_t tmp = ((h1->GetBinContent(iBinX, iBinY) / h2->GetBinContent(iBinX, iBinY)) - avg);
			rms += (tmp * tmp);
		}
		rms /= h1->GetNbinsY();
		hout->SetBinError(iBinX, TMath::Sqrt(rms));

	}

	return hout;
	
}

// -----------------------------------------------------------------------
TH1F* AliHistToolsDiHadronPID::CutOutJetAndProjectOnDPhi(const TH2F* hist, Double_t maxDEta, Double_t DPhiJet, Double_t DEtaJet, Bool_t useConstFit, Bool_t yieldIsPerTrigger) {

    /**
	 * This method creates a projection on the DPhi axis, but removes all bins
	 * which have a bin center within an ellipse (DPhiJet, DEtaJet) from the origin.
	 *
	 * Note that if useConstFit is TRUE, then we assume that all bins in DEta direction which are included 
     * are fully correlated, that is, we do a constant fit. For this reason, the jet size should be chosen not
     * too small.
     *
     * Note also that the unit on the y axis is the average dN/dDeltaPhi in a certain range of DEta (maxDEta). 
     * This changed on 7/7/2015, previously it was the average per bin, but this is not a nice quantity...
     */

	if (maxDEta < DEtaJet) {
		AliLogDiHadronPID::Error(Form("maxDEta needs to be larger than DEtaJet."), __func__, "AliHistToolsDiHadronPID");
		return 0x0;
	}
	Int_t minBinDEta = hist->GetYaxis()->FindBin(-maxDEta + 0.001);
	Int_t maxBinDEta = hist->GetYaxis()->FindBin(maxDEta - 0.001);

	TH1F* hout = new TH1F(Form("%s_proj_minusJet", hist->GetName()), hist->GetTitle(), hist->GetXaxis()->GetNbins(), hist->GetXaxis()->GetXmin(), hist->GetXaxis()->GetXmax());
	hout->SetXTitle(hist->GetXaxis()->GetTitle());
    hout->SetYTitle(yieldIsPerTrigger ? Form("#frac{1}{N_{trig}} #LT#frac{dN}{d#Delta#phi}#GT_{|#Delta#eta| < %3.1f}", maxDEta) : "");
	hout->SetDirectory(0);

	// Int_t maxNPointsIncluded = 0;
	if (useConstFit) {

		// Use a constant fit to the points instead of a sum.
		for (Int_t iDPhiBin = 1; iDPhiBin <= hist->GetXaxis()->GetNbins(); iDPhiBin++) {

			Int_t nPointsIncluded = 0;

			// Create arrays that need to be fitted.
			Double_t points[hist->GetYaxis()->GetNbins()];
			Double_t errors[hist->GetYaxis()->GetNbins()];
			for (Int_t iDEtaBin = 1; iDEtaBin <= hist->GetYaxis()->GetNbins(); iDEtaBin++) {
				
				points[iDEtaBin] = 0.;
				errors[iDEtaBin] = 0.;

			}

			Double_t dPhiScaled2 = TMath::Power(hist->GetXaxis()->GetBinCenter(iDPhiBin) / DPhiJet, 2);

			// Fill the arrays that need to be fitted.
			for (Int_t iDEtaBin = minBinDEta; iDEtaBin <= maxBinDEta; iDEtaBin++) {

				Double_t dEtaScaled2 = TMath::Power(hist->GetYaxis()->GetBinCenter(iDEtaBin) / DEtaJet, 2);

				// Continue if the bin center is within the jet region (note that we don't need to put a square root here
				// because of the normalisation).
				if ((dPhiScaled2 + dEtaScaled2) < 1.) {continue;}

				points[nPointsIncluded] = hist->GetBinContent(iDPhiBin, iDEtaBin);
				errors[nPointsIncluded] = hist->GetBinError(iDPhiBin, iDEtaBin);
				nPointsIncluded++;

			}		

			Double_t fitval = 0.;
			Double_t fiterr = 0.;

			AliFunctionsDiHadronPID::ConstFit(points, errors, nPointsIncluded, fitval, fiterr);

			hout->SetBinContent(iDPhiBin, fitval);
			hout->SetBinError(iDPhiBin, fiterr);

			// maxNPointsIncluded = TMath::Max(maxNPointsIncluded, nPointsIncluded);

		}

	} else {

		// Use ordinary sum.
		for (Int_t iDPhiBin = 1; iDPhiBin <= hist->GetXaxis()->GetNbins(); iDPhiBin++) {

			Double_t dPhiScaled2 = TMath::Power(hist->GetXaxis()->GetBinCenter(iDPhiBin) / DPhiJet, 2);

			Double_t sum = 0.;
			Double_t errorSquared = 0.;
			Double_t nPointsIncluded = 0.;

			for (Int_t iDEtaBin = minBinDEta; iDEtaBin <= maxBinDEta; iDEtaBin++) {

				Double_t dEtaScaled2 = TMath::Power(hist->GetYaxis()->GetBinCenter(iDEtaBin) / DEtaJet, 2);

				if ((dPhiScaled2 + dEtaScaled2) < 1.) {continue;}		

				sum += hist->GetBinContent(iDPhiBin, iDEtaBin);
				errorSquared += (hist->GetBinError(iDPhiBin, iDEtaBin) * hist->GetBinError(iDPhiBin, iDEtaBin));
				nPointsIncluded++;

			}

			hout->SetBinContent(iDPhiBin, sum / nPointsIncluded);
			hout->SetBinError(iDPhiBin, TMath::Sqrt(errorSquared) / nPointsIncluded);
			// cout << "Npoints included: " << nPointsIncluded << endl;
		
		}

	}

	// hout->Scale((Double_t)maxNPointsIncluded);

	return hout;

}

// -----------------------------------------------------------------------
TH1F* AliHistToolsDiHadronPID::DCT(const char* name, const TH1* data, Int_t DSTtype) {

	// Performs a DCT of type DSTtype = 1,2,3..., as defined in for example
	// http://en.wikipedia.org/wiki/Discrete_cosine_transform
	//
	// The input is a data histogram, the output is a histogram with the
	// transformation values. Stat. errors on the transformed values are calculated.
	//
	// It is assumed that the input data histogram contains only one period, and
	// is continued in an odd fashion. For example in two-particle correlations, 
	// data between DPhi = 0, and DPhi = pi is expected. Symmetrizing data is the responsibility
	// of the user.

	if (DSTtype != 2) {
		cout << "Only DST-II is implemented." << endl;
		return 0x0;
	}

	// Create an output histogram.
	TH1F* hOut = new TH1F(name, Form("%s (DST %i);n;X_{n}", name, DSTtype), data->GetNbinsX(), -0.5, data->GetNbinsX() - 0.5);

	for (Int_t iCoefficient = 0; iCoefficient < data->GetNbinsX(); iCoefficient++) {
		
		Double_t value = 0.;
		Double_t error2 = 0.;

		for (Int_t iDataPoint = 0; iDataPoint < data->GetNbinsX(); iDataPoint++) {
				
			Double_t matrixElement = 0.;

			switch (DSTtype) {
				case 2:
					matrixElement = TMath::Cos(TMath::Pi() * (Double_t)iCoefficient * (0.5 + (Double_t)iDataPoint) / (Double_t)data->GetNbinsX());
					break;
				default:
					break;
			}

			value += (data->GetBinContent(iDataPoint + 1) * matrixElement);
			error2 += (data->GetBinError(iDataPoint + 1) * matrixElement * data->GetBinError(iDataPoint + 1) * matrixElement);

		}

		hOut->SetBinContent(iCoefficient + 1, value);
		hOut->SetBinError(iCoefficient + 1, TMath::Sqrt(error2));

	}

	return hOut;

}

// -----------------------------------------------------------------------
TF1* AliHistToolsDiHadronPID::CreateFlowFuncFromDCT(const char* name, const TH1* data, const TH1* dct, Int_t minOrder, Int_t maxOrder) {

	// This method takes the (unscaled) DCT values, and returns a function that 
	// should describe the data. Note that the "data" histogram does not need to
	// be symmetrised here.

	// Create a flow function with the right amount of orders.
	TString* func = new TString("[0]*(1");
	for (Int_t iCoefficient = 0; iCoefficient < dct->GetNbinsX(); iCoefficient++) {
		func->Append(Form("+2*[%i]*cos(%i*x)", iCoefficient + 1, iCoefficient + 1));
	}
	func->Append(")");

	// Create a function with the correct range.
	TF1* fout = new TF1(name, func->Data(), (data->GetXaxis()->GetXmin()), (data->GetXaxis()->GetXmax()));

	// Scale the yield with the number of bins in the data histogram.
	fout->SetParameter(0, dct->GetBinContent(1) / data->GetNbinsX());
	fout->SetParName(0, "baseLine");

	for (Int_t iCoefficient = 1; iCoefficient <  dct->GetNbinsX(); iCoefficient++) {
		if (iCoefficient > maxOrder || iCoefficient < minOrder) {
			fout->SetParameter(iCoefficient, 0.);
		} else {
			fout->SetParameter(iCoefficient, dct->GetBinContent(iCoefficient + 1) / dct->GetBinContent(1));
		}

		fout->SetParName(iCoefficient, Form("v%i#Delta", iCoefficient + 1));
	}

	return fout;

}

// -----------------------------------------------------------------------
TF1* AliHistToolsDiHadronPID::CreateFlowFuncErrorFromDCT(const char* name, const TH1* data, const TH1* dct, Int_t minOrder, Int_t maxOrder) {

	// Create a flow error function with the right amount of orders.
	TString* func = new TString("TMath::Sqrt([0]*[0]");
	for (Int_t iCoefficient = 0; iCoefficient < dct->GetNbinsX(); iCoefficient++) {
		func->Append(Form("+4*[%i]*[%i]*cos(%i*x)*cos(%i*x)", iCoefficient + 1, iCoefficient + 1, iCoefficient + 1, iCoefficient + 1));
	}
	func->Append(")");

	// Create a function with the correct range.
	TF1* fout = new TF1(name, func->Data(), (data->GetXaxis()->GetXmin()), (data->GetXaxis()->GetXmax()));

	// Scale the yield with the number of bins in the data histogram.
	fout->SetParameter(0, dct->GetBinError(1) / data->GetNbinsX());
	fout->SetParName(0, "baseLine");

	for (Int_t iCoefficient = 1; iCoefficient < dct->GetNbinsX(); iCoefficient++) {
		if (iCoefficient > maxOrder || iCoefficient < minOrder) {
			fout->SetParameter(iCoefficient, 0.);
		} else {
			fout->SetParameter(iCoefficient, dct->GetBinError(iCoefficient + 1) / (data->GetNbinsX()));
		}

		fout->SetParName(iCoefficient, Form("#epsilon_{v%i#Delta}", iCoefficient + 1));
	
	}
	return fout;

}

// -----------------------------------------------------------------------
TGraphErrors* AliHistToolsDiHadronPID::CreateFlowFuncErrorsFromDCT(const TH1* data, const TH1* symmData, Int_t minOrder, Int_t maxOrder) {

	// This method applies a DCT transform on the data, and generates a graph representing the result
	// and the error band.
	TH1F* dct = DCT("dct", symmData, 2);
	TF1* dctFunc = CreateFlowFuncFromDCT("dctfunc", data, dct, minOrder, maxOrder);
	TF1* dctErrorFunc = CreateFlowFuncErrorFromDCT("dctfunc", data, dct, minOrder, maxOrder);

	// for (Int_t iPar = 0; iPar < dctErrorFunc->GetNpar(); iPar++) {
	// 	cout << " par "<< iPar << " = " << dctErrorFunc->GetParameter(iPar) << endl;
	// }

	Double_t delta = (data->GetXaxis()->GetXmax() - data->GetXaxis()->GetXmin()) / 1000.;
	TGraphErrors* errors = new TGraphErrors(1000);
	for (Int_t iPoint = 0; iPoint < errors->GetN(); iPoint++) {

		Double_t xx = (0.5 + iPoint) * delta + data->GetXaxis()->GetXmin(); 

		errors->SetPoint(iPoint, xx, dctFunc->Eval(xx));
		errors->SetPointError(iPoint, errors->GetErrorX(iPoint), dctErrorFunc->Eval(xx));
	
	}

	delete dct;
	delete dctFunc;
	delete dctErrorFunc;

	errors->SetFillColor(4);
	errors->SetFillStyle(3010);

	return errors;

}

// -----------------------------------------------------------------------
TH1F* AliHistToolsDiHadronPID::CreateSymmetrisedHist(const char* name, const TH1* hist) {

	// This method maps a histogram with the range -Pi/2 .. 3Pi/2 to a histogram
	// from 0 .. Pi, assuming that the data is anti-symmetric around 0 and Pi.
	// Errors are added in quadrature.

	// The method assumes that there are bin edges at -pi/2,0,pi/2,pi, and 3pi/2.
	// This means that nbins nod 4 = 0.
	if (hist->GetNbinsX() % 4) {
		cout << "This only works on a histogram with nBins = 4*Int" << endl;
		return 0x0;
	} 

	// Create an output histogram.
	TH1F* hOut = new TH1F(name, name, hist->GetNbinsX()/2, 0., TMath::Pi());
	hOut->GetXaxis()->SetTitle(hist->GetXaxis()->GetTitle());
	hOut->GetYaxis()->SetTitle(hist->GetYaxis()->GetTitle());

	// Have to copy axis, since the "hist" has a const qualifier.
	TAxis* aTmpAxis = new TAxis(*hist->GetXaxis());

	// Loop over bins of the outgoing histogram.
	for (Int_t iBin = 1; iBin <= hOut->GetNbinsX(); iBin++) {
			
		// Calculate the two corresponding bins in the unmirrored histogram.
		Int_t iBin1 = aTmpAxis->FindBin(0.00001) + iBin - 1; 
		Int_t iBin2 = aTmpAxis->FindBin(0.00001) - iBin;

		if (iBin2 < 1) {iBin2 += hist->GetNbinsX();}

		// cout << "bins: " << iBin << " -> " << iBin1 << "," << iBin2 << endl;

		hOut->SetBinContent(iBin, hist->GetBinContent(iBin1) + hist->GetBinContent(iBin2));
		hOut->SetBinError(iBin, TMath::Sqrt(hist->GetBinError(iBin1) * hist->GetBinError(iBin1) +
			hist->GetBinError(iBin2) * hist->GetBinError(iBin2)));

	}

	delete aTmpAxis;

	return hOut;

}

// -----------------------------------------------------------------------
TCanvas* AliHistToolsDiHadronPID::ShowSlices(const TH2F* hist, Bool_t parallelToX, Int_t minBin, Int_t maxBin) {

	// This method makes slices of every bin in the desired range, and plots
	// them in one canvas. A ptr to the canvas is returned.

	if (!hist) {return 0x0;}
	if (minBin < 1) {minBin = 1;}

	// The default value of maxBin is -1, which means that all bins are taken.
	Int_t nBins = (parallelToX ? hist->GetNbinsY() : hist->GetNbinsX());
	if (maxBin > nBins || maxBin < 0) {maxBin = nBins;}

	// Create a new canvas.
	TCanvas* canvas = new TCanvas(Form("%s_Slices", hist->GetName()),
		Form("%s Slices", hist->GetTitle()), 0, 0, 500, 500);

	TLegend* legend = new TLegend(0.4,0.5,0.6,0.9);

	TH1F* firsthist = 0x0;
	Double_t maxVal = 0.;
	Double_t minVal = 0.;

	// Loop over bins.
	for (Int_t iBin = minBin; iBin <= maxBin; iBin++) {
		
		cout << "Bin: " << iBin << endl;
		TH1F* projection = (TH1F*)(parallelToX ? 
			hist->ProjectionX(Form("%s_Bin%s%i", hist->GetName(), "Y", iBin), iBin, iBin):
			hist->ProjectionY(Form("%s_Bin%s%i", hist->GetName(), "X", iBin), iBin, iBin));
		projection->SetDirectory(0);
		Int_t color = (iBin == 10 ? 50 : iBin);
		projection->SetLineColor(color);
		projection->SetMarkerColor(color);
		projection->SetLineWidth(2);
		if (iBin == minBin) {
			projection->Draw();
			firsthist = projection;
			minVal = firsthist->GetMinimum();
			maxVal = firsthist->GetMaximum();
		}
		else {
			projection->Draw("same");
			minVal = TMath::Min(minVal, projection->GetMinimum());
			maxVal = TMath::Max(maxVal, projection->GetMaximum());
		}

		legend->AddEntry(projection, Form("Bin%i", iBin),"L");

	}

	firsthist->SetMinimum(minVal);
	firsthist->SetMaximum(maxVal);

	legend->Draw("same");

	return canvas;

}

// -----------------------------------------------------------------------
void AliHistToolsDiHadronPID::DeleteCanvasAndPrimitives(TCanvas* cvs) {

	// Transfer ownership of the primitives in the canvas to us.
	TObjArray* primitivesToBeDeleted = new TObjArray();
	primitivesToBeDeleted->SetOwner(kTRUE);

	TList* primitivesFromCVS = cvs->GetListOfPrimitives();

	for (Int_t iPrimitive = 0; iPrimitive < primitivesFromCVS->GetSize(); iPrimitive++) {
		
		// Read primitives, for histo's set directory to 0.	
		TObject* primitive = primitivesFromCVS->At(iPrimitive);
		if (primitive) {

			TString className(primitive->ClassName());
			cout << "Found: " << className.Data() << endl;

			// Delete histograms.
			if (className.Contains("TH1") || className.Contains("TH2") || className.Contains("TH3")) {
				cout << "Found histogram at: " << primitive << "in directory: " << ((TH1*)primitive)->GetDirectory() << endl;
				((TH1*)primitive)->SetDirectory(0);
			
				primitivesToBeDeleted->AddLast(primitive);

			}

			

		}
	
	}

	delete cvs;
	delete primitivesToBeDeleted;

}

// -----------------------------------------------------------------------
/* ONGOING WORK
Bool_t AliHistToolsDiHadronPID::OperateWithTH1onTHn(THn* hist1, const TH1* hist2, Operation operation, const TString& options) {

	// Operates on hist1 with hist2. The option argument is expected to be of type:
	// "x0y1z2", meaning that axis 0 of hist1 is identified with axis x of hist 2, etc.
	// if "e" is added to the option, then error propagation will be done. The sumw2
	// structure will be created if it isn't available yet.
	// Example: A 4D THn needs to be devided by a TH2F (mixed events or so), and 
	// we want errors to be propagated. The function call would then be:
	//
	//   OperateWithTObjArg(name1, name2, kDiv, "x0y1e");

	// WORK IN PROGRESS!!

	// Not safe for arguments like x1y1, etc.
	TArrayI map(hist2->GetDimension());

	for (Int_t iDim = 0; iDim < hist2->GetDimension(); ++iDim) {

		char axisl = 120 + iDim;
		if (options.Contains(axisl)) {
			TString car(options[options.Index(axisl) + 1]);
			if (car.IsDigit()) {
				map[0] = car.Atoi()
			} else {
				cout << Form("%s::%s -> an integer is expected next to x in the options argument.", __FILE__, __func__) << endl;	
				return kFALSE;
			}
		} else {
			cout << Form("%s::%s -> x not specified in option.", __FILE__, __func__) << endl;	
			return kFALSE;
		}


	}





	return kFALSE;

}

*/




