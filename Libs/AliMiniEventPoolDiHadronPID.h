#ifndef ALIMINIEVENTPOOLDIHADRONPID_H
#define ALIMINIEVENTPOOLDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TObject.h"
#include "TObjArray.h"
#include "TAxis.h"
#include "TVectorF.h"
#include "TH1I.h"
#include "AliMiniEventDiHadronPID.h"
#include "AliSettingsDiHadronPID.h"

class AliMiniEventPoolDiHadronPID : public TObject {
	
public:
	AliMiniEventPoolDiHadronPID();
	~AliMiniEventPoolDiHadronPID();

	void 							AddEventTriggers(const AliMiniEventDiHadronPID* miniEvent, TObjArray* triggers, Int_t eventIndex);
	void							GetMixTriggers(const AliMiniEventDiHadronPID* miniEvent, TObjArray* triggers);

	void							SetCheckOutgoingEventFrequency() {fOutgoingEventFrequency = new TVectorF(1000);}

	Bool_t							IsReady() const;
	void 							Print(Option_t*) const;
	TH1I*							GetOutgoingEventFrequency() const;

private:
	AliSettingsDiHadronPID*			fSettings;			//!
	mutable Bool_t					fIsReady;			// Is true if we have the minimum number of events in every bin.

	TAxis*							fCentralityAxis;	//! Unfortunately FindBin() which we often use is marked as non-const... Therefore we need explicit copies of the axes.
	const Int_t						fNCentralityBins;	//
	TAxis*							fVtxZAxis;			//!
	const Int_t						fNVtxZBins;			//

	Int_t**							fCounter; 			//!
	TObjArray***					fPool; 				//!
	Int_t***						fPoolEventNumbers;	//!

	TVectorF*						fOutgoingEventFrequency; //!

	ClassDef(AliMiniEventPoolDiHadronPID, 1);

};

#endif
