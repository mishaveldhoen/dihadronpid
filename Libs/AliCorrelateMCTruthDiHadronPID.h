#ifndef ALICORRELATEMCTRUTHDIHADRONPID_H
#define ALICORRELATEMCTRUTHDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TAxis.h"
#include "TH1D.h"
#include "TH2D.h"
#include "THn.h"
#include "TVectorF.h"
#include "AliAnalyzeDataDiHadronPID.h"
#include "AliMiniEventPoolDiHadronPID.h"

class AliCorrelateMCTruthDiHadronPID : public AliAnalyzeDataDiHadronPID {
	
public:
	AliCorrelateMCTruthDiHadronPID(const char* name, const char* title);
	~AliCorrelateMCTruthDiHadronPID();

private:
	void							UserCreateOutput();
	void 							UserExec();
    void                            CorrelateAssociatedTrack(const AliMiniTrackDiHadronPID* triggerTrack, 
                                        Int_t iTriggerTrack, Int_t iAssocTrack, Bool_t isMixedEvents = kFALSE);
	void							UserFinalizeAnalysis();
	void							UserSaveOutput();

public:	
	void							MakeHistogramOfEventIndicesWithTriggers() {fEventsWithTriggers = new TVectorF(1000); fPool->SetCheckOutgoingEventFrequency();}
	void							SetMassAssumption(Int_t massAssumption) {fMassAssumption = ((massAssumption > -1 && massAssumption < 3) ? massAssumption : -1);}
	void 							Print(Option_t*) const;

private:
	AliMiniEventPoolDiHadronPID*	fPool;								//!
	TVectorF*						fEventsWithTriggers;	 			//! Keeps track of event indices that have triggers.

	// Settings.
	Int_t							fMassAssumption;					// If < 0 then no PID correlations are made.

	// Axes. 
	// In principle we would have liked to only talk to the axes through AliSettingsDiHadronPID::GetPtAxis() etc.,
	// however since that method returns a const TAxis*, and TAxis::FindBin is non-const, we need deep-copies...
	TAxis*							fPtAxis;							//!
	TAxis*							fEtaAxis;							//!
    TAxis*                          fDPhiAxis;                          //!
    TAxis*                          fDEtaAxis;                          //!
	TAxis*							fCentralityAxis;					//!
	TAxis*							fVtxZAxis;							//!

	// QA histograms.
	TH1D*							fNTotTriggersTruth;                 //!
	TH1D*							fPtHistogramTruth; 					//!

	TH1D*							fNTotTriggersRec;					//!
	TH1D*							fPtHistogramRec; 					//!
   
    // These histograms are used for calculating PID and matching efficiency.
    TH1D*                           fPtHistogramPrimTruth;              //!
    TH1D*                           fPtHistogramPrimRec;                //!
    TH1D*                           fPtHistogramPrimRecSelForPID;       //!
    TH1D*                           fPtHistogramPrimRecMatched;         //!

    // MC Truth correlation histograms (DPhi, DEta), [pt].              //!
    TH2D**                          fMCPIDCorrelationsPrimTruth;        //!
    TH2D**                          fMCPIDCorrelationsWeakTruth;        //!
    TH2D**                          fMCPIDCorrelationsMatTruth;         //!

    TH2D**                          fMCPIDMixedPrimTruth;               //!
    TH2D**                          fMCPIDMixedWeakTruth;               //!
    TH2D**                          fMCPIDMixedMatTruth;                //!

    // MC Truth correlation histograms (DPhi, DEta), [pt].              //!
    TH2D**                          fMCPIDCorrelationsPrimRec;          //!
    TH2D**                          fMCPIDCorrelationsWeakRec;          //!
    TH2D**                          fMCPIDCorrelationsMatRec;           //!

    TH2D**                          fMCPIDMixedPrimRec;                 //!
    TH2D**                          fMCPIDMixedWeakRec;                 //!
    TH2D**                          fMCPIDMixedMatRec;                  //!

    // These two histograms are filled with pairs of a trigger particle and a 
    // reconstructed associated particle which was selected for PID, or 
    // matched with the correct TOF hit. They can be used for an efficiency
    // calculation.
    TH2D**                          fMCPIDCorrelationsPrimRecSelForPID; //!
    TH2D**                          fMCPIDCorrelationsPrimRecMatched;   //!

    TH2D**                          fMCPIDMixedPrimRecSelForPID;        //!
    TH2D**                          fMCPIDMixedPrimRecMatched;          //!

    // Efficiency histograms, calculated by the UserFinalizeAnalysis() method.
    TH1D*                           fSPEffTracking;                     //!
    TH1D*                           fSPEffSelForPID;                    //!
    TH1D*                           fSPEffMatched;                      //!

    TH1D*                           fCorrEffTracking;                   //!
    TH1D*                           fCorrEffSelForPID;                  //!
    TH1D*                           fCorrEffMatched;                    //!

    TH1D*                           fCorrEffTrackingDPhiStrip;          //!
    TH1D*                           fCorrEffSelForPIDDPhiStrip;         //!
    TH1D*                           fCorrEffMatchedDPhiStrip;           //!

    TH1D*                           fCorrEffTrackingFullDPhiDEta;       //!
    TH1D*                           fCorrEffSelForPIDFullDPhiDEta;      //!
    TH1D*                           fCorrEffMatchedFullDPhiDEta;        //!

	ClassDef(AliCorrelateMCTruthDiHadronPID, 1);

};

#endif
