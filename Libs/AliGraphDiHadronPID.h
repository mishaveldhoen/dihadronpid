#ifndef ALIGRAPHDIHADRONPID_H
#define ALIGRAPHDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TObjArray.h"
#include "TNamed.h"
#include "TVectorF.h"
#include "TPad.h"
#include "TLegend.h"
#include "TH1.h"
#include "AliSystUncertaintyGenDiHadronPID.h"

class AliGraphDiHadronPID : public TNamed {
	
public:
	AliGraphDiHadronPID();
	AliGraphDiHadronPID(const char* name, const char* title);
	AliGraphDiHadronPID(const TH1* hist);
	AliGraphDiHadronPID(const AliGraphDiHadronPID& other);
	~AliGraphDiHadronPID();

	enum SystErrorStyle {kBand, kOpenBlock, kSolidBlock};

	// Basics.
	void						Init(Int_t nPoints);
	void						Reset();

	// Measurement.
	void						ImportMeasurement(const TH1* hist);
	void						SetSystErr(Double_t pct);
	void						SetSystErr(const AliSystUncertaintyGenDiHadronPID* systErr);

	// Looks (setters).
	void						SetColor(Color_t color) {fColor = color;}
	void						SetMarkerStyle(Style_t style) {fMarkerStyle = style;}
	void						SetSystErrorStyle(SystErrorStyle style) {fSystErrorStyle = style;}
	void						SetSpecies() {;}
	void						SetMinX(Double_t min) {fMinX = min;}
	void						SetMaxX(Double_t max) {fMaxX = max;}
	void						SetMinY(Double_t min) {fMinY = min;}
	void						SetMaxY(Double_t max) {fMaxY = max;}
	void						SetXAxisTitle(TString title) {fXAxisTitle = title;}
	void						SetYAxisTitle(TString title) {fYAxisTitle = title;}
	static void					SetMultiPlotDisplacement(Double_t displacement) {fMultiPlotDisplacement = displacement;}

	// Looks (getters).
	Double_t					GetMinX() const {return fMinX;}
	Double_t					GetMaxX() const {return fMaxX;}
	const char*					GetTitleX() {return fXAxisTitle.Data();}

	Double_t					GetMinY(Bool_t useLog = kFALSE) const {return (useLog ? fMinYLog : fMinY);}
	Double_t					GetMaxY(Bool_t useLog = kFALSE) const {return (useLog ? fMaxYLog : fMaxY);}
	const char*					GetTitleY() {return fYAxisTitle.Data();}

	// Operations.
	void						MergeBins(Int_t minBin, Int_t maxBin, Int_t groupSize, Bool_t takeAvg = kFALSE);
	void						Scale(Double_t scaleFactor) {fY *= scaleFactor; fSystErrorUp *= scaleFactor; fSystErrorDown *= scaleFactor;}

	// Show.
	void						Show(TPad* pad = 0x0, Int_t collectionSize = 1, Int_t position = 0, Bool_t showStat = kTRUE, Bool_t showSyst = kTRUE);
	void						ShowStat(TPad* pad = 0x0, Int_t collectionSize = 1, Int_t position = 0) {Show(pad, collectionSize, position, kTRUE, kFALSE);}
	void						ShowSyst(TPad* pad = 0x0, Int_t collectionSize = 1, Int_t position = 0) {Show(pad, collectionSize, position, kFALSE, kTRUE);}
	void						UpdateLegend(TLegend* legend);

	void 						Print(Option_t*) const;

private:
	Int_t						fNPoints;

	// The measurement.
	TVectorF					fX;
	TVectorF					fY;
	TVectorF					fBinWidth;
	TVectorF					fStatErrorVert;	
	TVectorF					fSystErrorUp;
	TVectorF					fSystErrorDown;

	// The looks.
	Double_t 					fSystErrorMargin;
	SystErrorStyle				fSystErrorStyle;
	static Double_t				fMultiPlotDisplacement;	
	Double_t					fMinX;
	Double_t					fMaxX;
	Double_t					fMinY;
	Double_t					fMinYLog;
	Double_t					fMaxY;
	Double_t					fMaxYLog;
	Color_t						fColor;
	Style_t						fMarkerStyle;
	Int_t						fLineWidth;
	TString						fXAxisTitle;
	TString						fYAxisTitle;

	TObjArray*					fOwnedObjects;

	ClassDef(AliGraphDiHadronPID, 1);

};

#endif
