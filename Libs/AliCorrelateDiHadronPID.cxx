/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Class description
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "AliPtEtaSpectrumDiHadronPID.h"
#include "AliCorrelateDiHadronPID.h"
#include "AliHistToolsDiHadronPID.h"
#include "AliLogDiHadronPID.h"

#include "AliMiniEventDiHadronPID.h"
#include "AliMiniTrackDiHadronPID.h"

ClassImp(AliCorrelateDiHadronPID);

// -----------------------------------------------------------------------
AliCorrelateDiHadronPID::AliCorrelateDiHadronPID(const char* name, const char* title):
	AliAnalyzeDataDiHadronPID(name, title),
	fPool(new AliMiniEventPoolDiHadronPID()),
	fEventsWithTriggers(0x0),
	fMassAssumption(-1),
	fPtAxis(0x0),
	fEtaAxis(0x0),
	fCentralityAxis(0x0),
	fVtxZAxis(0x0),
	fNTotTriggers(0x0),
    fTriggersPhiEta(0x0),
	fPtHistogram(0x0),
	fNSamePairs(0x0),
	fNMixedPairs(0x0),
	fNMixedOverSamePairs(0x0),
	fNSameTriggers(0x0),
	fNMixedTriggers(0x0),
	fNMixedOverSameTriggers(0x0),
	fUICorrelations(0x0),
	fUIMixedEvents(0x0),
	fUICorrected(0x0),
	fUICorrelationsSumBeforeMECorr(0x0),
	fUIMixedEventsSumBeforeMECorr(0x0),
	fUICorrectedSumBeforeMECorr(0x0),
	fUICorrectedSumAfterMECorr(0x0),
	fPIDCorrelations(0x0),
	fPIDMixedEvents(0x0)

{
	
	// Default Constructor
	AliLogDiHadronPID::Debug2("Default constructor called.", __func__, ClassName());

}

// -----------------------------------------------------------------------
AliCorrelateDiHadronPID::~AliCorrelateDiHadronPID() {
	
	// Destructor
	AliLogDiHadronPID::Debug2("Destructor called.", __func__, ClassName());

	// Delete ME pool.
	if (fPool) {delete fPool; fPool = 0x0;}

	if (fEventsWithTriggers) {delete fEventsWithTriggers; fEventsWithTriggers = 0x0;}

	// Delete Axes.
	if (fPtAxis) {delete fPtAxis; fPtAxis = 0x0;}
	if (fEtaAxis) {delete fEtaAxis; fEtaAxis = 0x0;}
	if (fCentralityAxis) {delete fCentralityAxis; fCentralityAxis = 0x0;}
	if (fVtxZAxis) {delete fVtxZAxis; fVtxZAxis = 0x0;}

	// Delete QA histograms.
	if (fNTotTriggers) {delete fNTotTriggers; fNTotTriggers = 0x0;}
    if (fTriggersPhiEta) {delete fTriggersPhiEta; fTriggersPhiEta = 0x0;}
	if (fPtHistogram) {delete fPtHistogram; fPtHistogram = 0x0;}

	if (fNSamePairs) {delete fNSamePairs; fNSamePairs = 0x0;}
	if (fNMixedPairs) {delete fNMixedPairs; fNMixedPairs = 0x0;}
	if (fNMixedOverSamePairs) {delete fNMixedOverSamePairs; fNMixedOverSamePairs = 0x0;}

	if (fNSameTriggers) {delete fNSameTriggers; fNSameTriggers = 0x0;}
	if (fNMixedTriggers) {delete fNMixedTriggers; fNMixedTriggers = 0x0;}
	if (fNMixedOverSameTriggers) {delete fNMixedOverSameTriggers; fNMixedOverSameTriggers = 0x0;}

	for (Int_t iPtBin = 1; iPtBin <= fSettings->GetPtAxis()->GetNbins(); iPtBin++) {

		// Delete UI Correlation Histogarms (reverse order from creation).	
		if(fUICorrelationsSumBeforeMECorr[iPtBin-1]) {delete fUICorrelationsSumBeforeMECorr[iPtBin-1]; fUICorrelationsSumBeforeMECorr[iPtBin-1] = 0x0;}
		if(fUIMixedEventsSumBeforeMECorr[iPtBin-1]) {delete fUIMixedEventsSumBeforeMECorr[iPtBin-1]; fUIMixedEventsSumBeforeMECorr[iPtBin-1] = 0x0;}
		if(fUICorrectedSumBeforeMECorr[iPtBin-1]) {delete fUICorrectedSumBeforeMECorr[iPtBin-1]; fUICorrectedSumBeforeMECorr[iPtBin-1] = 0x0;}
	
		if(fUICorrectedSumAfterMECorr[iPtBin-1]) {delete fUICorrectedSumAfterMECorr[iPtBin-1]; fUICorrectedSumAfterMECorr[iPtBin-1] = 0x0;}

		for (Int_t iCentralityBin = 1; iCentralityBin <= fSettings->GetCentralityAxis()->GetNbins(); iCentralityBin++) {
				
			for (Int_t iVtxZBin = 1; iVtxZBin <= fSettings->GetVtxZAxis()->GetNbins(); iVtxZBin++) {
						
				if(fUICorrelations[iPtBin-1][iCentralityBin-1][iVtxZBin-1]) {delete fUICorrelations[iPtBin-1][iCentralityBin-1][iVtxZBin-1]; fUICorrelations[iPtBin-1][iCentralityBin-1][iVtxZBin-1] = 0x0;}
				if(fUIMixedEvents[iPtBin-1][iCentralityBin-1][iVtxZBin-1]) {delete fUIMixedEvents[iPtBin-1][iCentralityBin-1][iVtxZBin-1]; fUIMixedEvents[iPtBin-1][iCentralityBin-1][iVtxZBin-1] = 0x0;}
				if(fUICorrected[iPtBin-1][iCentralityBin-1][iVtxZBin-1]) {delete fUICorrected[iPtBin-1][iCentralityBin-1][iVtxZBin-1]; fUICorrected[iPtBin-1][iCentralityBin-1][iVtxZBin-1] = 0x0;}
			
			}

			if(fUICorrelations[iPtBin-1][iCentralityBin-1]) {delete fUICorrelations[iPtBin-1][iCentralityBin-1]; fUICorrelations[iPtBin-1][iCentralityBin-1] = 0x0;}
			if(fUIMixedEvents[iPtBin-1][iCentralityBin-1]) {delete fUIMixedEvents[iPtBin-1][iCentralityBin-1]; fUIMixedEvents[iPtBin-1][iCentralityBin-1] = 0x0;}
			if(fUICorrected[iPtBin-1][iCentralityBin-1]) {delete fUICorrected[iPtBin-1][iCentralityBin-1]; fUICorrected[iPtBin-1][iCentralityBin-1] = 0x0;}

		}
	
		if(fUICorrelations[iPtBin-1]) {delete fUICorrelations[iPtBin-1]; fUICorrelations[iPtBin-1] = 0x0;}
		if(fUIMixedEvents[iPtBin-1]) {delete fUIMixedEvents[iPtBin-1]; fUIMixedEvents[iPtBin-1] = 0x0;}
		if(fUICorrected[iPtBin-1]) {delete fUICorrected[iPtBin-1]; fUICorrected[iPtBin-1] = 0x0;}

		// Delete PID Correlation Histogarms.
		if(fPIDCorrelations) {
			if(fPIDCorrelations[iPtBin-1]) {delete fPIDCorrelations[iPtBin-1]; fPIDCorrelations[iPtBin-1] = 0x0;}
			if(fPIDMixedEvents[iPtBin-1]) {delete fPIDMixedEvents[iPtBin-1]; fPIDMixedEvents[iPtBin-1] = 0x0;}
		}

	}

	if (fPIDCorrelations) {delete fPIDCorrelations; fPIDCorrelations = 0x0;}
	if (fPIDMixedEvents) {delete fPIDMixedEvents; fPIDMixedEvents = 0x0;}

	if(fUICorrelationsSumBeforeMECorr) {delete fUICorrelationsSumBeforeMECorr; fUICorrelationsSumBeforeMECorr = 0x0;}
	if(fUIMixedEventsSumBeforeMECorr) {delete fUIMixedEventsSumBeforeMECorr; fUIMixedEventsSumBeforeMECorr = 0x0;}
	if(fUICorrectedSumBeforeMECorr) {delete fUICorrectedSumBeforeMECorr; fUICorrectedSumBeforeMECorr = 0x0;}
	if(fUICorrectedSumAfterMECorr) {delete fUICorrectedSumAfterMECorr; fUICorrectedSumAfterMECorr = 0x0;}

	if(fUICorrelations) {delete fUICorrelations; fUICorrelations = 0x0;}
	if(fUIMixedEvents) {delete fUIMixedEvents; fUIMixedEvents = 0x0;}
	if(fUICorrected) {delete fUICorrected; fUICorrected = 0x0;}

	// Delete inclusive histograms if they exist.
	if (fSignalInclusive) {
		for (Int_t iEtaBin = 0; iEtaBin < fSettings->GetEtaAxis()->GetNbins(); ++iEtaBin) {
			for (Int_t iPtBin = 0; iPtBin < fSettings->GetPtAxis()->GetNbins(); ++iPtBin) {

				if (fSignalInclusive[iEtaBin][iPtBin]) {delete fSignalInclusive[iEtaBin][iPtBin]; fSignalInclusive[iEtaBin][iPtBin] = 0x0;}
				if (fMismatchInclusive[iEtaBin][iPtBin]) {delete fMismatchInclusive[iEtaBin][iPtBin]; fMismatchInclusive[iEtaBin][iPtBin] = 0x0;}

			}

			if (fSignalInclusive[iEtaBin]) {delete fSignalInclusive[iEtaBin]; fSignalInclusive[iEtaBin] = 0x0;}
			if (fMismatchInclusive[iEtaBin]) {delete fMismatchInclusive[iEtaBin]; fMismatchInclusive[iEtaBin] = 0x0;}

		}

		if (fSignalInclusive) {delete fSignalInclusive; fSignalInclusive = 0x0;}
		if (fMismatchInclusive) {delete fMismatchInclusive; fMismatchInclusive = 0x0;}

	}

}

// -----------------------------------------------------------------------
void AliCorrelateDiHadronPID::UserCreateOutput() {

	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	// Change output file name.
	if (fMassAssumption > -1) {fOutputFileName = fSettings->GetPIDCorrelationsFileNameForSpecies(fMassAssumption);}

	// Obtain deep copies of the axes (we want to be able to use the non-const method TAxis::FindBin())
	fPtAxis = new TAxis(*fSettings->GetPtAxis());
	fEtaAxis = new TAxis(*fSettings->GetEtaAxis());
	fCentralityAxis = new TAxis(*fSettings->GetCentralityAxis());
	fVtxZAxis = new TAxis(*fSettings->GetVtxZAxis());

	// Create QA histograms.
	fNTotTriggers = new TH1D("triggerHist", "triggerHist", 1, fSettings->GetMinTriggerPt(), fSettings->GetMaxTriggerPt());
    fTriggersPhiEta = (TH2D*)AliHistToolsDiHadronPID::MakeHist("triggerHistPhiEta", "triggerHistPhiEta;#phi,#eta", "D", fSettings->GetPhiAxis(), fSettings->GetEtaAxis());
	fPtHistogram = (TH1D*)AliHistToolsDiHadronPID::MakeHist("hPtDist", "p_{T} distribution", "D", fSettings->GetPtAxis());

	fNSamePairs = (TH2D*)AliHistToolsDiHadronPID::MakeHist("hNSamePairs", "Same Pair Distribution", "D", fSettings->GetCentralityAxis(), fSettings->GetVtxZAxis());
	fNMixedPairs = (TH2D*)AliHistToolsDiHadronPID::MakeHist("hNMixedPairs", "Mixed Pair Distribution", "D", fSettings->GetCentralityAxis(), fSettings->GetVtxZAxis());
	fNMixedOverSamePairs = (TH2D*)AliHistToolsDiHadronPID::MakeHist("hNSameOverMixedPairs", "Same/Mixed Pair Distribution", "D", fSettings->GetCentralityAxis(), fSettings->GetVtxZAxis());
	
	fNSameTriggers = (TH2D*)AliHistToolsDiHadronPID::MakeHist("hNSameTriggers", "Same Trigger Distribution", "D", fSettings->GetCentralityAxis(), fSettings->GetVtxZAxis());
	fNMixedTriggers = (TH2D*)AliHistToolsDiHadronPID::MakeHist("hNMixedTriggers", "Mixed Trigger Distribution", "D", fSettings->GetCentralityAxis(), fSettings->GetVtxZAxis());
	fNMixedOverSameTriggers = (TH2D*)AliHistToolsDiHadronPID::MakeHist("hNSameOverMixedTriggers", "Same/Mixed Trigger Distribution", "D", fSettings->GetCentralityAxis(), fSettings->GetVtxZAxis());
	
	// Create UI Correlation histograms.
	fUICorrelationsSumBeforeMECorr = new TH2D*[fSettings->GetPtAxis()->GetNbins()];
	fUIMixedEventsSumBeforeMECorr = new TH2D*[fSettings->GetPtAxis()->GetNbins()];
	fUICorrectedSumBeforeMECorr = new TH2D*[fSettings->GetPtAxis()->GetNbins()];
	fUICorrectedSumAfterMECorr = new TH2D*[fSettings->GetPtAxis()->GetNbins()];

	fUICorrelations = new TH2D***[fSettings->GetPtAxis()->GetNbins()];
	fUIMixedEvents = new TH2D***[fSettings->GetPtAxis()->GetNbins()];
	fUICorrected = new TH2D***[fSettings->GetPtAxis()->GetNbins()];

	for (Int_t iPtBin = 1; iPtBin <= fSettings->GetPtAxis()->GetNbins(); iPtBin++) {
		
		fUICorrelationsSumBeforeMECorr[iPtBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fUICorrelationsSumBeforeMECorr_PtBin%i", iPtBin), Form("fUICorrelationsSumBeforeMECorr_PtBin%i", iPtBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());
		fUIMixedEventsSumBeforeMECorr[iPtBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fUIMixedEventsSumBeforeMECorr_PtBin%i", iPtBin), Form("fUIMixedEventsSumBeforeMECorr_PtBin%i", iPtBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());
		fUICorrectedSumBeforeMECorr[iPtBin-1] = 0x0;
	
		fUICorrectedSumAfterMECorr[iPtBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fUICorrectedSumAfterMECorr_PtBin%i", iPtBin), Form("fUICorrectedSumAfterMECorr_PtBin%i", iPtBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());

		fUICorrelations[iPtBin-1] = new TH2D**[fSettings->GetCentralityAxis()->GetNbins()];
		fUIMixedEvents[iPtBin-1] = new TH2D**[fSettings->GetCentralityAxis()->GetNbins()];
		fUICorrected[iPtBin-1] = new TH2D**[fSettings->GetCentralityAxis()->GetNbins()];

		for (Int_t iCentralityBin = 1; iCentralityBin <= fSettings->GetCentralityAxis()->GetNbins(); iCentralityBin++) {
				
			fUICorrelations[iPtBin-1][iCentralityBin-1] = new TH2D*[fSettings->GetVtxZAxis()->GetNbins()];
			fUIMixedEvents[iPtBin-1][iCentralityBin-1] = new TH2D*[fSettings->GetVtxZAxis()->GetNbins()];
			fUICorrected[iPtBin-1][iCentralityBin-1] = new TH2D*[fSettings->GetVtxZAxis()->GetNbins()];
	
			for (Int_t iVtxZBin = 1; iVtxZBin <= fSettings->GetVtxZAxis()->GetNbins(); iVtxZBin++) {
						
				fUICorrelations[iPtBin-1][iCentralityBin-1][iVtxZBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fUICorrelations_PtBin%i_CentBin%i_VtxZBin%i", iPtBin, iCentralityBin, iVtxZBin), Form("fUICorrelations_PtBin%i_CentBin%i_VtxZBin%i", iPtBin, iCentralityBin, iVtxZBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());
				fUIMixedEvents[iPtBin-1][iCentralityBin-1][iVtxZBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fUIMixedEvents_PtBin%i_CentBin%i_VtxZBin%i", iPtBin, iCentralityBin, iVtxZBin), Form("fUIMixedEvents_PtBin%i_CentBin%i_VtxZBin%i", iPtBin, iCentralityBin, iVtxZBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());
				fUICorrected[iPtBin-1][iCentralityBin-1][iVtxZBin-1] = 0x0; 
			
			}

		}
	
	}

	// Create PID histograms if requested.
	if (fMassAssumption > -1) {

		const TAxis* dphiAxis = fSettings->GetDPhiAxis();
		const TAxis* detaAxis = fSettings->GetDEtaAxis();
		const TAxis* tofAxis = fSettings->GetTOFAxis((AliSettingsDiHadronPID::Species)fMassAssumption);
		const TAxis* tpcAxis = fSettings->GetTPCAxis((AliSettingsDiHadronPID::Species)fMassAssumption);
		Int_t dim[4] 		= {dphiAxis->GetNbins(), detaAxis->GetNbins(), tofAxis->GetNbins(), tpcAxis->GetNbins()};
		Double_t min[4]		= {dphiAxis->GetXmin(), detaAxis->GetXmin(), tofAxis->GetXmin(), tpcAxis->GetXmin()};
		Double_t max[4]		= {dphiAxis->GetXmax(), detaAxis->GetXmax(), tofAxis->GetXmax(), tpcAxis->GetXmax()};

		fPIDCorrelations = new THnF*[fSettings->GetPtAxis()->GetNbins()];
		fPIDMixedEvents = new THnF*[fSettings->GetPtAxis()->GetNbins()];

		for (Int_t iPtBin = 1; iPtBin <= fSettings->GetPtAxis()->GetNbins(); iPtBin++) {

			fPIDCorrelations[iPtBin-1] = new THnF(Form("CorrelationsPID_pt%i", iPtBin), Form("CorrelationsPID_pt%i;#Delta#phi;#Delta#eta", iPtBin), 4, dim, min, max);
			fPIDMixedEvents[iPtBin-1] = new THnF(Form("MixedEventsPID_pt%i", iPtBin), Form("MixedEventsPID_pt%i;#Delta#phi;#Delta#eta", iPtBin), 4, dim, min, max);

		}

		// Create inclusive histograms, to cross check with the di-hadron results.
		const TAxis* aEta = fSettings->GetEtaAxis();
		const TAxis* aPt = fSettings->GetPtAxis();
		fSignalInclusive = new TH2F**[aEta->GetNbins()];
		fMismatchInclusive = new TH2F**[aEta->GetNbins()];

		for (Int_t iEtaBin = 0; iEtaBin < aEta->GetNbins(); ++iEtaBin) {

			fSignalInclusive[iEtaBin] = new TH2F*[aPt->GetNbins()];
			fMismatchInclusive[iEtaBin] = new TH2F*[aPt->GetNbins()];

			for (Int_t iPtBin = 0; iPtBin < aPt->GetNbins(); ++iPtBin) {

				// Create histograms for the signal.
				fSignalInclusive[iEtaBin][iPtBin] = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("hist_eta%i_pt_%i",iEtaBin,iPtBin),
					Form("hist_eta%i_pt_%i",iEtaBin,iPtBin),"F",fSettings->GetTOFAxis((AliSettingsDiHadronPID::Species)fMassAssumption), fSettings->GetTPCAxis((AliSettingsDiHadronPID::Species)fMassAssumption));

				// Create a set of histograms for the mismatches.
				fMismatchInclusive[iEtaBin][iPtBin] = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("histmm_eta%i_pt_%i",iEtaBin,iPtBin),
					Form("histmm_eta%i_pt_%i",iEtaBin,iPtBin),"F",fSettings->GetTOFAxis((AliSettingsDiHadronPID::Species)fMassAssumption), fSettings->GetTPCAxis((AliSettingsDiHadronPID::Species)fMassAssumption));

			}
		}
		
	} else {
		fPIDCorrelations = 0x0;
		fPIDMixedEvents = 0x0;
		fSignalInclusive = 0x0;
		fMismatchInclusive = 0x0;
	}


}

// -----------------------------------------------------------------------
void AliCorrelateDiHadronPID::UserExec() {

	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	// Get a ptr to the current event.
	AliMiniEventDiHadronPID* currentEvent = GetCurrentEvent();

	Int_t centBin = fCentralityAxis->FindBin(currentEvent->GetCentrality());
	if (centBin < 1 || centBin > fCentralityAxis->GetNbins()) {return;}

	Int_t vtxzBin = fVtxZAxis->FindBin(currentEvent->GetVtxZ());
	if (vtxzBin < 1 || vtxzBin > fVtxZAxis->GetNbins()) {return;}	

	// Create an array for the triggers we will add to the pool, this array owns the triggers,
	// and should therefore only contain copies.
	TObjArray* donatedTriggers = new TObjArray(10);
	donatedTriggers->SetOwner(kTRUE);

	// Identify pairs in the same event.
	for (Int_t iTriggerTrack = 0; iTriggerTrack < currentEvent->GetNTracks(); ++iTriggerTrack) { 
		
		AliMiniTrackDiHadronPID* currentTriggerTrack = currentEvent->GetTrack(iTriggerTrack);
		
		// Check track filtermap against settings filtermask.
		if (fSettings->GetFilterMask()) {if (!(currentTriggerTrack->GetFilterMap() & fSettings->GetFilterMask())) {continue;}}
		
		// Check the pT of the trigger.
		if ((currentTriggerTrack->Pt() < fSettings->GetMinTriggerPt()) ||
			(currentTriggerTrack->Pt() > fSettings->GetMaxTriggerPt())) {continue;}
		
		// Create a copy of the current track, and add to the array of donated triggers.
		AliMiniTrackDiHadronPID* donatedTrigger = new AliMiniTrackDiHadronPID(*currentTriggerTrack);
		donatedTriggers->AddLast(donatedTrigger);

		// Loop over associateds (but only when we're not doing a rerun!)
		if (RerunIsInProgress()) {continue;}

		// Fill the pT distribution.
		fPtHistogram->Fill(currentTriggerTrack->Pt());
        
        // Bugfix: Note also that these histograms should no longer be filled when a rerun is in progress!
		fNTotTriggers->Fill(currentTriggerTrack->Pt());
        fTriggersPhiEta->Fill(currentTriggerTrack->Phi(), currentTriggerTrack->Eta());
        fNSameTriggers->Fill(currentEvent->GetCentrality(), currentEvent->GetVtxZ());

		for (Int_t iAssocTrack = 0; iAssocTrack < fCurrentEvent->GetNTracks(); ++iAssocTrack) {
			
			// Load Associated Track and perform PID cut.
			AliMiniTrackDiHadronPID* currentAssocTrack = fCurrentEvent->GetTrack(iAssocTrack);
			if (fSettings->GetFilterMask()) {if (!(currentAssocTrack->GetFilterMap() & fSettings->GetFilterMask())) {continue;}}
			if (!(fSettings->IsTrackSelectedForPID(currentAssocTrack))) {continue;}

			// Check the Charge selection.
			if (!(fSettings->IsTrackSelectedForCharge(currentAssocTrack))) {continue;}

			// Check the pT of the associated.
			if ((currentAssocTrack->Pt() < fSettings->GetMinAssociatedPt()) ||
				(currentAssocTrack->Pt() > fSettings->GetMaxAssociatedPt())) {continue;}

			// Protect against making auto correlations.
			if (iTriggerTrack == iAssocTrack) {AliLogDiHadronPID::Error(Form("Same track is used as trigger and associated..."), __func__, ClassName());}

			// Fill the number of same pairs histogram.								
			fNSamePairs->Fill(currentEvent->GetCentrality(), currentEvent->GetVtxZ());

			Double_t deltaPhi = currentTriggerTrack->Phi() - currentAssocTrack->Phi();
			if (deltaPhi < -TMath::Pi()/2.) {deltaPhi += 2.*TMath::Pi();}
			else if (deltaPhi > 3.*TMath::Pi()/2.) {deltaPhi -= 2.*TMath::Pi();}
			Double_t deltaEta = currentTriggerTrack->Eta() - currentAssocTrack->Eta();

			// Fill Correlation histograms.
			Int_t ptBin = fPtAxis->FindBin(currentAssocTrack->Pt());
			if ((ptBin < 1) || (ptBin > fPtAxis->GetNbins())) {continue;}
			Int_t etaBin = fEtaAxis->FindBin(currentAssocTrack->Eta());
			if ((etaBin < 1) || (etaBin > fEtaAxis->GetNbins())) {continue;}

			fUICorrelations[ptBin-1][centBin-1][vtxzBin-1]->Fill(deltaPhi, deltaEta);

			if (fMassAssumption > -1) {
				Double_t filling[4] = {deltaPhi, deltaEta, currentAssocTrack->GetTOFsignalMinusExpected(fMassAssumption), currentAssocTrack->GetTPCsignalMinusExpected(fMassAssumption)};
				fPIDCorrelations[ptBin-1]->Fill(filling);

				// Fill inclusive histograms.
				fSignalInclusive[etaBin-1][ptBin-1]->Fill(currentAssocTrack->GetTOFsignalMinusExpected(fMassAssumption), currentAssocTrack->GetTPCsignalMinusExpected(fMassAssumption));
				fMismatchInclusive[etaBin-1][ptBin-1]->Fill(currentAssocTrack->GetTOFRandom(fMassAssumption), currentAssocTrack->GetTPCsignalMinusExpected(fMassAssumption));
		
			}
	
		}

	}

	// If No rerun is in progress, and the event yielded triggers, then mark the event as an event with triggers.
	if (fEventsWithTriggers) {
		if (!RerunIsInProgress() && donatedTriggers->GetEntries()) {

			if (fEventsWithTriggers->GetNoElements() <= fTotalEventIndex) {
				fEventsWithTriggers->ResizeTo(fTotalEventIndex + 1000);
			}

			((*fEventsWithTriggers)[fTotalEventIndex])++;
		}
	}

	// Mix events if the pool is ready, and the current event contains triggers.
	if (fPool->IsReady() && donatedTriggers->GetEntries()) {

		// Create an array that the pool can use to provide us with triggers. Since the pool is owner of the triggers,
		// this array does not own anything.
		TObjArray* receivedTriggers = new TObjArray(10);

		// Receive triggers appropriate for the current event.
		fPool->GetMixTriggers(currentEvent, receivedTriggers);
		// cout << "Mixing with " << receivedTriggers->GetEntriesFast() << " triggers. " << endl;

		// Loop over the received triggers.
		for (Int_t iTriggerTrack = 0; iTriggerTrack < receivedTriggers->GetEntriesFast(); ++iTriggerTrack) { 

			AliMiniTrackDiHadronPID* currentTriggerTrack = (AliMiniTrackDiHadronPID*)receivedTriggers->At(iTriggerTrack);
			fNMixedTriggers->Fill(currentEvent->GetCentrality(), currentEvent->GetVtxZ());
		
			// Loop over this events associated tracks.
			for (Int_t iAssocTrack = 0; iAssocTrack < fCurrentEvent->GetNTracks(); ++iAssocTrack) {
				
				// Load Associated Track and perform PID cut.
				AliMiniTrackDiHadronPID* currentAssocTrack = fCurrentEvent->GetTrack(iAssocTrack);
				if (fSettings->GetFilterMask()) {if (!(currentAssocTrack->GetFilterMap() & fSettings->GetFilterMask())) {continue;}}
				if (!(fSettings->IsTrackSelectedForPID(currentAssocTrack))) {continue;}

				// Check the Charge selection.
				if (!(fSettings->IsTrackSelectedForCharge(currentAssocTrack))) {continue;}

				// Check the pT of the associated.
				if ((currentAssocTrack->Pt() < fSettings->GetMinAssociatedPt()) ||
					(currentAssocTrack->Pt() > fSettings->GetMaxAssociatedPt())) {continue;}

				// Fill the number of same pairs histogram.								
				fNMixedPairs->Fill(currentEvent->GetCentrality(), currentEvent->GetVtxZ());

				Double_t deltaPhi = currentTriggerTrack->Phi() - currentAssocTrack->Phi();
				if (deltaPhi < -TMath::Pi()/2.) {deltaPhi += 2.*TMath::Pi();}
				else if (deltaPhi > 3.*TMath::Pi()/2.) {deltaPhi -= 2.*TMath::Pi();}
				Double_t deltaEta = currentTriggerTrack->Eta() - currentAssocTrack->Eta();

				// Fill Mixed Event histograms.
				Int_t ptBin = fPtAxis->FindBin(currentAssocTrack->Pt());
				if ((ptBin < 1) || (ptBin > fPtAxis->GetNbins())) {continue;}

				fUIMixedEvents[ptBin-1][centBin-1][vtxzBin-1]->Fill(deltaPhi, deltaEta);
			
				if (fMassAssumption > -1) {
					Double_t filling[4] = {deltaPhi, deltaEta, currentAssocTrack->GetTOFsignalMinusExpected(fMassAssumption), currentAssocTrack->GetTPCsignalMinusExpected(fMassAssumption)};
					fPIDMixedEvents[ptBin-1]->Fill(filling);
				}
			}

		}

		delete receivedTriggers; receivedTriggers = 0x0;

	}

	// Supply new triggers to the pool, but of course only if there are triggers to donate.
	// If not, then delete the array.
	//
	// The method AddEventTriggers(...) releases ownership over the array to the pool, so we 
	// shouldn't delete it ourselves!
	if (donatedTriggers->GetEntries()) {

		// Pool status before
		Bool_t poolIsReady = fPool->IsReady();
		fPool->AddEventTriggers(currentEvent, donatedTriggers, fTotalEventIndex);

		// If the pool status went to ready in this event, then request
		if (poolIsReady == kFALSE) {
			if (fPool->IsReady()) {
				RequestRerunUntilCurrentEvent();
				AliLogDiHadronPID::Info(Form("Pool is ready now, requesting rerun up to this point."), __func__, ClassName());
				fPool->Print(0);
			}
		}

	} else {
		if (donatedTriggers) {delete donatedTriggers; donatedTriggers = 0x0;}
	}

	// fPool->Print(0);

}

// -----------------------------------------------------------------------
void AliCorrelateDiHadronPID::UserFinalizeAnalysis() {

	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	// Create the histogram of Nbins
	for (Int_t iBinX = 1; iBinX <= fNMixedOverSamePairs->GetNbinsX(); iBinX++) {
		for (Int_t iBinY = 1; iBinY <= fNMixedOverSamePairs->GetNbinsY(); iBinY++) {			
			fNMixedOverSamePairs->SetBinContent(iBinX, iBinY, fNMixedPairs->GetBinContent(iBinX, iBinY) / fNSamePairs->GetBinContent(iBinX, iBinY));
			fNMixedOverSameTriggers->SetBinContent(iBinX, iBinY, fNMixedTriggers->GetBinContent(iBinX, iBinY) / fNSameTriggers->GetBinContent(iBinX, iBinY));
		}
	}

	// Correct the correlations, per centrality and vtxz bin:
	for (Int_t iPtBin = 1; iPtBin <= fSettings->GetPtAxis()->GetNbins(); iPtBin++) {
		for (Int_t iCentralityBin = 1; iCentralityBin <= fSettings->GetCentralityAxis()->GetNbins(); iCentralityBin++) {
			for (Int_t iVtxZBin = 1; iVtxZBin <= fSettings->GetVtxZAxis()->GetNbins(); iVtxZBin++) {

				// Call Sumw2 on all histograms that were filled.
				fUICorrelations[iPtBin-1][iCentralityBin-1][iVtxZBin-1]->Sumw2();
				fUIMixedEvents[iPtBin-1][iCentralityBin-1][iVtxZBin-1]->Sumw2();

				// Add correlations and mixed events (before)
				fUICorrelationsSumBeforeMECorr[iPtBin-1]->Add(fUICorrelations[iPtBin-1][iCentralityBin-1][iVtxZBin-1]);
				fUIMixedEventsSumBeforeMECorr[iPtBin-1]->Add(fUIMixedEvents[iPtBin-1][iCentralityBin-1][iVtxZBin-1]);

				fUICorrected[iPtBin-1][iCentralityBin-1][iVtxZBin-1] = (TH2D*)fUICorrelations[iPtBin-1][iCentralityBin-1][iVtxZBin-1]->Clone(Form("fUICorrected_PtBin%i_CentBin%i_VtxZBin%i", iPtBin, iCentralityBin, iVtxZBin));
				fUICorrected[iPtBin-1][iCentralityBin-1][iVtxZBin-1]->SetTitle(Form("fUICorrected_PtBin%i_CentBin%i_VtxZBin%i", iPtBin, iCentralityBin, iVtxZBin));
				AliHistToolsDiHadronPID::NormaliseMixedEvents(fUIMixedEvents[iPtBin-1][iCentralityBin-1][iVtxZBin-1], kTRUE); // Flag true means: use only bins around (0,0)
				fUICorrected[iPtBin-1][iCentralityBin-1][iVtxZBin-1]->Divide(fUIMixedEvents[iPtBin-1][iCentralityBin-1][iVtxZBin-1]);

				// Add corrected result
				fUICorrectedSumAfterMECorr[iPtBin-1]->Add(fUICorrected[iPtBin-1][iCentralityBin-1][iVtxZBin-1]);

			}
		}

		fUICorrectedSumBeforeMECorr[iPtBin-1] = (TH2D*)fUICorrelationsSumBeforeMECorr[iPtBin-1]->Clone(Form("fUICorrectedSumBeforeMECorr_PtBin%i", iPtBin));
		fUICorrectedSumBeforeMECorr[iPtBin-1]->SetTitle(Form("fUICorrectedSumBeforeMECorr_PtBin%i", iPtBin));
		AliHistToolsDiHadronPID::NormaliseMixedEvents(fUIMixedEventsSumBeforeMECorr[iPtBin-1], kTRUE); // Flag true means: use only bins around (0,0)
		fUICorrectedSumBeforeMECorr[iPtBin-1]->Divide(fUIMixedEventsSumBeforeMECorr[iPtBin-1]);

	}
}

// -----------------------------------------------------------------------
void AliCorrelateDiHadronPID::UserSaveOutput() {

	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	fNTotTriggers->Write();
	fPtHistogram->Write();

	fNSamePairs->Write();
	fNMixedPairs->Write();
	fNMixedOverSamePairs->Write();

	fNSameTriggers->Write();
	fNMixedTriggers->Write();
	fNMixedOverSameTriggers->Write();

	// Save UI stuff.
	for (Int_t iPtBin = 1; iPtBin <= fSettings->GetPtAxis()->GetNbins(); iPtBin++) {
		fUICorrelationsSumBeforeMECorr[iPtBin-1]->Write();
		fUIMixedEventsSumBeforeMECorr[iPtBin-1]->Write();
		fUICorrectedSumBeforeMECorr[iPtBin-1]->Write();
		fUICorrectedSumAfterMECorr[iPtBin-1]->Write();

		TH2D* wings = (TH2D*)fUICorrectedSumBeforeMECorr[iPtBin-1]->Clone(Form("SumBeforeMixingOverSumAfterMixing_PtBin%i", iPtBin));
		wings->SetTitle(Form("SumBeforeMixing / SumAfterMixing (PtBin%i)", iPtBin));
		wings->Divide(fUICorrectedSumAfterMECorr[iPtBin-1]);
		wings->Write();
		delete wings;
	}

	// Save PID stuff.
	if (fMassAssumption > -1) {

		// Also save histogram with the largest bins in the PID mixed event histograms.
		TH1D* histMaxBinPIDME = (TH1D*)AliHistToolsDiHadronPID::MakeHist("histMaxBinPIDME", "histMaxBinPIDME;p_{T}", "D", fSettings->GetPtAxis());

		for (Int_t iPtBin = 1; iPtBin <= fSettings->GetPtAxis()->GetNbins(); iPtBin++) {
			fPIDCorrelations[iPtBin-1]->Write();
			fPIDMixedEvents[iPtBin-1]->Write();

			// Find the maximum bin in the ME histogram.
			Double_t maxVal = 0.;
			Int_t bin[4] = {0,0,0,0};
			for (bin[0] = 1; bin[0] <= fSettings->GetDPhiAxis()->GetNbins(); bin[0]++) {
				for (bin[1] = 1; bin[1] <= fSettings->GetDEtaAxis()->GetNbins(); bin[1]++) {
					for (bin[2] = 1; bin[2] <= fSettings->GetTOFAxis((AliSettingsDiHadronPID::Species)fMassAssumption)->GetNbins(); bin[2]++) {
						for (bin[3] = 1; bin[3] <= fSettings->GetTPCAxis((AliSettingsDiHadronPID::Species)fMassAssumption)->GetNbins(); bin[3]++) {
							maxVal = TMath::Max(maxVal, fPIDMixedEvents[iPtBin-1]->GetBinContent(bin));
						}
					}
				}
			}

			histMaxBinPIDME->SetBinContent(iPtBin, maxVal);
}

		histMaxBinPIDME->Write();
		delete histMaxBinPIDME;

		// Save inclusive histogram.
		AliPtEtaSpectrumDiHadronPID* spectrum = new AliPtEtaSpectrumDiHadronPID(Form("Spec_%i", fMassAssumption), Form("Spec_%i", fMassAssumption), (AliSettingsDiHadronPID::Species)fMassAssumption);

		for (Int_t iEtaBin = 1; iEtaBin <= fSettings->GetEtaAxis()->GetNbins(); ++iEtaBin) {
			for (Int_t iPtBin = 1; iPtBin <= fSettings->GetPtAxis()->GetNbins(); ++iPtBin) {
				spectrum->ImportPIDDataForBin(fSignalInclusive[iEtaBin - 1][iPtBin - 1], fMismatchInclusive[iEtaBin - 1][iPtBin - 1], iPtBin, iEtaBin);
			}
		}

		spectrum->Write();
		delete spectrum;

	}

	// If available, save the information about the frequency with which every event is used in 
	// event mixing.
	TH1I* poolOutgoingEventFrequency = fPool->GetOutgoingEventFrequency();

	if (poolOutgoingEventFrequency) {
		poolOutgoingEventFrequency->Write();
		delete poolOutgoingEventFrequency;
	}

	if (fEventsWithTriggers) {
		TH1I* hEventsWithTriggers = new TH1I("hEventsWithTriggers","Events With Triggers;EventIndex;Y/N",fEventsWithTriggers->GetNoElements(), -0.5, fEventsWithTriggers->GetNoElements() - 0.5);
		for (Int_t iEventIndex = 0; iEventIndex < fEventsWithTriggers->GetNoElements(); iEventIndex++) {
			hEventsWithTriggers->SetBinContent(iEventIndex+1,(*fEventsWithTriggers)[iEventIndex]);	
		}
		hEventsWithTriggers->SetLineColor(kRed);
		hEventsWithTriggers->Write();
		delete hEventsWithTriggers; hEventsWithTriggers = 0x0;
	}

}

// -----------------------------------------------------------------------
void AliCorrelateDiHadronPID::Print(Option_t*) const {
	
	// Print info about the class.
	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

}
