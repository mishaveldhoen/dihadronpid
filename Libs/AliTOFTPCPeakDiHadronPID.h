#ifndef ALITOFTPCPEAKDIHADRONPID_H
#define ALITOFTPCPEAKDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TObject.h"
#include "AliSettingsDiHadronPID.h"

class AliTOFTPCPeakDiHadronPID : public TObject {

public:
	enum Parameter {kYield, kMuTOF, kSigmaTOF, kTailTOF, kMuTPC, kSigmaTPC, kTailTPC};

public:
	AliTOFTPCPeakDiHadronPID();
	AliTOFTPCPeakDiHadronPID(AliSettingsDiHadronPID::Species massAssumption, AliSettingsDiHadronPID::Species peakSpecies, Double_t meanPt, Double_t absMeanEta, Double_t tofBinWidth, Double_t tpcBinWidth);
	AliTOFTPCPeakDiHadronPID(const AliTOFTPCPeakDiHadronPID& other);
	AliTOFTPCPeakDiHadronPID& operator=(const AliTOFTPCPeakDiHadronPID &other);
	~AliTOFTPCPeakDiHadronPID();

	double 								operator()(double* xy, double*) {return operator()(xy);}
	double 								operator()(double* xy) {return operator()(xy[0], xy[1]);}
	Double_t							operator()(Double_t tof, Double_t tpc);
	Double_t							Integral(Double_t tofMin, Double_t tofMax, Double_t tpcMin, Double_t tpcMax);
	Double_t							Norm();
	void								FillHist(TH2F* hist);
	void                                                            FillHistdEdxRatio(TH2F* hist, Double_t expVal);

	void								ResetParameters(Bool_t setToExpectedValues = kTRUE);

private:
	void								SetParameters(const Double_t* parameters); // NOT SAFE.
	void								SetParameterErrors(const Double_t* parerrors); // NOT SAFE.
	void								SetParameterStepSize(const Double_t* parstepsize); // NOT SAFE.
	void								SetParameterIsFixed(const Bool_t* parisfixed); // NOT SAFE.

public:
	void								SetUseInFit(Bool_t use) {fIsUsedInFit = use;}
	void								SetParValue(Parameter parameter, Double_t value) {fParameters[parameter] = value;}
	void								SetParError(Parameter parameter, Double_t error) {fParameterErrors[parameter] = error;}
	void 								SetParStepSize(Parameter parameter, Double_t stepsize) {fParameterStepSize[parameter] = stepsize;}
	void								FixParameter(Parameter parameter) {fParameterIsFixed[parameter] = kTRUE;}
	void								ReleaseParameter(Parameter parameter) {fParameterIsFixed[parameter] = kFALSE;}

	void								SetPeakSpecies(AliSettingsDiHadronPID::Species species) {fPeakSpecies = species;} // Used in exceptional cases (when converting pion to lepton peak).

	AliSettingsDiHadronPID::Species 	GetPeakSpecies() const {return fPeakSpecies;}
	const char*							GetPeakSpeciesName() const {return (AliSettingsDiHadronPID::GetSpeciesName(fPeakSpecies));}

	Double_t							GetParValue(Parameter parameter) const {return fParameters[parameter];}
	Double_t							GetParError(Parameter parameter) const {return fParameterErrors[parameter];}
	Double_t							GetParStepSize(Parameter parameter) const {return fParameterStepSize[parameter];}
	Bool_t								GetParIsFixed(Parameter parameter) const {return fParameterIsFixed[parameter];}
	Double_t							ParameterIsFixed(Parameter parameter) const {return fParameterIsFixed[parameter];}

	static Int_t						GetNParameters() {return fNParameters;}
	static TString	 					GetParName(Parameter parameter) {return fParameterNames[parameter];}
	TString	 							GetParNameWithSpecies(Parameter parameter) const; 
	static TString	 					GetParNameWithSpecies(Parameter parameter, AliSettingsDiHadronPID::Species species);

	Bool_t								IsUsedInFit() const {return fIsUsedInFit;}
	Bool_t								TPCTailIsUsed() const {return fUseTPCTail;}

	void 								Print(Option_t*) const;
	static Int_t						GetNInstances() {return fNInstances;}

private:
	// Static Data.
	static Int_t						fNInstances;
	static const Int_t					fNParameters; 
	static const char*					fParameterNames[7];
	static const Double_t				fParameterStdStepSizes[7];

	AliSettingsDiHadronPID* 			fSettings; //!

	AliSettingsDiHadronPID::Species 	fMassAssumption;
	AliSettingsDiHadronPID::Species		fPeakSpecies;

	Double_t							fMeanPt;
	Double_t							fAbsMeanEta;	
	Double_t							fParameters[7];
	Double_t							fParameterErrors[7];
	Double_t							fParameterStepSize[7];
	Bool_t								fParameterIsFixed[7];

	Double_t							fTOFbinWidth;
	Double_t							fTPCbinWidth;

	Bool_t								fIsUsedInFit;
	Bool_t								fUseTPCTail;

	ClassDef(AliTOFTPCPeakDiHadronPID, 1);

};

#endif
