/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Process Data from AliAnalysisTaskDiHadronPIDMini
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include <iostream>
#include <TThread.h>
#include "TSystem.h"
#include "TKey.h"
#include "TDirectoryFile.h"
#include "TObjString.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TFileMerger.h"
#include "THn.h"
#include "TDirectory.h"
#include "TRandom3.h"
#include "AliHistToolsDiHadronPID.h"
#include "AliMiscToolsDiHadronPID.h"
#include "AliMTFileMergerDiHadronPID.h"

#include "AliPtEtaSpectrumDiHadronPID.h"
#include "AliFitTemplatesDiHadronPID.h"

#include "AliLogDiHadronPID.h"

#include "AliProcessDataDiHadronPIDMini.h"

using namespace std;

ClassImp(AliProcessDataDiHadronPIDMini);

// -----------------------------------------------------------------------
AliProcessDataDiHadronPIDMini::AliProcessDataDiHadronPIDMini(Int_t massAssumption) :
	TObject(),
	fSettings(AliSettingsDiHadronPID::Instance()),
	fMaxNThreads(1),
	fInputFileList(0x0),
	fCurrentFileIndex(0),
	fCurrentFile(0x0),
	fCurrentEventBranch(0x0),
	fCurrentEventIndex(0),
	fCurrentEvent(0x0),
	fCurrentExists(kFALSE),
	fCurrentTriggerHist(0x0),
	fCurrentCorrPID(0x0),
	fCurrentMixedPID(0x0),
	fCurrentCorrUI(0x0),
	fCurrentMixedUI(0x0),
	fCurrentCorrMCPion(0x0),
	fCurrentMixedMCPion(0x0),
	fCurrentCorrMCKaon(0x0),
	fCurrentMixedMCKaon(0x0),
	fCurrentCorrMCProton(0x0),
	fCurrentMixedMCProton(0x0),
	//fCurrentFitTemplates(0x0),
	fTriggerPool(0x0),
	fPoolIsFilled(kFALSE),
	fStartedMixingAtFile(0),
	fStartedMixingAtEvent(0),
	fPoolWriteIndex(0),
	fNbinsDPhi(16),
	fNbinsDEta(16),
	fNbinsTOF(400),
	fNbinsTPC(150),
	fMassAssumption((AliSettingsDiHadronPID::Species)massAssumption),
	fDebugLevel(0),
	fVerboseLevel(1)

{

	// Default Constructor
	AliLogDiHadronPID::Debug2(Form("Default constructor called, m.a.: %i", massAssumption), __func__, ClassName());

	// Error message when settings singleton is not found!
	if (!fSettings) {AliLogDiHadronPID::Error(Form("Settings Singleton not found!"), __func__, ClassName());}

	fDataDir = fSettings->GetDataPath();
	fInputFileName = "AnalysisResults.root";
	fWorkDir = fSettings->GetWorkingDirectory();
	fOutputDir = "output";
	fOutputFileName = fSettings->GetTemplatesFileNameForSpecies(massAssumption);
	fCorrExtension = "Corr";

	// Generate a list of files that match the inputFileName.
	if (GenerateFileList()) {CheckFileList();}

	// Obtain Axes from the settings.
	fVertexZAxis = new TAxis(*(fSettings->GetVtxZAxis()));;
	fCentralityAxis = new TAxis(*(fSettings->GetCentralityAxis()));
	fPtAxis	= new TAxis(*(fSettings->GetPtAxis()));
	fEtaAxis = new TAxis(*(fSettings->GetEtaAxis()));
	fDEtaAxis = new TAxis(*(fSettings->GetDEtaAxis()));
	fDPhiAxis = new TAxis(*(fSettings->GetDPhiAxis()));
	fTOFAxis = new TAxis(*(fSettings->GetTOFAxis(fMassAssumption)));
	fTPCAxis = new TAxis(*(fSettings->GetTPCAxis(fMassAssumption)));

	// Create an instance of AliMiniEventDiHadronPID, to be filled when reading the tree.
	fCurrentEvent 		= new AliMiniEventDiHadronPID();

	// Create a trigger pool.
	fTriggerPool		= new TObjArray(fSettings->GetNTriggersToMix());

	// Creating the histograms.
	CreateBuffer();

	// Set some static properties.
	TH1::AddDirectory(kFALSE);

}

// -----------------------------------------------------------------------
AliProcessDataDiHadronPIDMini::~AliProcessDataDiHadronPIDMini() {

	// Destructor.
	AliLogDiHadronPID::Debug2(Form("Destructor called."), __func__, ClassName());

	if (fInputFileList) {delete fInputFileList; fInputFileList = 0x0;}
	if (fCurrentEvent) {delete fCurrentEvent; fCurrentEvent = 0x0;}
	if (fCurrentFile) {fCurrentFile->Close(); delete fCurrentFile; fCurrentFile = 0x0;}
	if (fTriggerPool) {delete fTriggerPool; fTriggerPool = 0x0;}

	if (fCurrentTriggerHist) {delete fCurrentTriggerHist; fCurrentTriggerHist = 0x0;}
	if (fCurrentCorrPID) {delete fCurrentCorrPID; fCurrentCorrPID = 0x0;}
	if (fCurrentMixedPID) {delete fCurrentMixedPID; fCurrentMixedPID = 0x0;}
	if (fCurrentCorrUI) {delete fCurrentCorrUI; fCurrentCorrUI = 0x0;}
	if (fCurrentMixedUI) {delete fCurrentMixedUI; fCurrentMixedUI = 0x0;}

	if (fCurrentCorrMCPion) {delete fCurrentCorrMCPion; fCurrentCorrMCPion = 0x0;}
	if (fCurrentMixedMCPion) {delete fCurrentMixedMCPion; fCurrentMixedMCPion = 0x0;}
	if (fCurrentCorrMCKaon) {delete fCurrentCorrMCKaon; fCurrentCorrMCKaon = 0x0;}
	if (fCurrentMixedMCKaon) {delete fCurrentMixedMCKaon; fCurrentMixedMCKaon = 0x0;}
	if (fCurrentCorrMCProton) {delete fCurrentCorrMCProton; fCurrentCorrMCProton = 0x0;}
	if (fCurrentMixedMCProton) {delete fCurrentMixedMCProton; fCurrentMixedMCProton = 0x0;}

	// Delete axes.
	if (fVertexZAxis) {delete fVertexZAxis; fVertexZAxis = 0x0;}
	if (fCentralityAxis) {delete fCentralityAxis; fCentralityAxis = 0x0;}
	if (fPtAxis) {delete fPtAxis; fPtAxis = 0x0;}
	if (fEtaAxis) {delete fEtaAxis; fEtaAxis = 0x0;}
	if (fDEtaAxis) {delete fDEtaAxis; fDEtaAxis = 0x0;}
	if (fDPhiAxis) {delete fDPhiAxis; fDPhiAxis = 0x0;}

}

// -----------------------------------------------------------------------
void AliProcessDataDiHadronPIDMini::MakeQAHistos() {

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	if (!fInputFileList) {cout << __func__ << " -> No input file list available..." << endl; return;}
	if (!(fInputFileList->GetEntriesFast())) {cout << __func__ << " -> Input file list is empty..." << endl; return;}

	TFile* fout = new TFile("QAhistos.root", "recreate");

	// Start the timer.
	TStopwatch sw;
	sw.Start();	

	// Histogram of TPC signal v.s. Nclusters dE/dx (use fMassAssumption)
	TH2F* hTPCvsNClus = new TH2F("hTPCvsNClus","hTPCvsNClus;TPC;Nclus_dEdx",fTPCAxis->GetNbins(), fTPCAxis->GetXmin(), fTPCAxis->GetXmax(), 32, 0, 160);
	TH2F* hTOFvsNClus = new TH2F("hTOFvsNClus","hTOFvsNClus;TOF;Nclus_dEdx",fTOFAxis->GetNbins(), fTOFAxis->GetXmin(), fTOFAxis->GetXmax(), 32, 0, 160);
	
	TH2F* hPhivsNClus = new TH2F("hPhivsNClus","hPhivsNClus;#phi;Nclus_dEdx",720,0.,2.*TMath::Pi(), 160, 0, 160);
	TH3F* hTOFTPCvsNClus = new TH3F("hTOFTPCvsNClus","hTOFTPCvsNClus;TOF;TPC;Nclus_dEdx",fTOFAxis->GetNbins(), fTOFAxis->GetXmin(), fTOFAxis->GetXmax(),
		fTPCAxis->GetNbins(), fTPCAxis->GetXmin(), fTPCAxis->GetXmax(), 32, 0, 160);

	TAxis* nClusAxis = new TAxis(32, 0, 160);
	TH3F* hEtaPtvsNClus = (TH3F*)AliHistToolsDiHadronPID::MakeHist("hEtaPtvsNClus","hEtaPtvsNClus","F",fEtaAxis,fPtAxis,nClusAxis);

	// Histogram of the TOF signal of tracks passing the DCA cut, and all tracks, for a low pt range (1.0 < pt < 1.1).
	TH1F* hTOFwithDCAcut = new TH1F("hTOFwithDCAcut", "hTOFwithDCAcut",fTOFAxis->GetNbins(), fTOFAxis->GetXmin(), fTOFAxis->GetXmax());
	TH1F* hTOFwithoutDCAcut = new TH1F("hTOFwithoutDCAcut", "hTOFwithoutDCAcut",fTOFAxis->GetNbins(), fTOFAxis->GetXmin(), fTOFAxis->GetXmax());

	TH1F* hTPCwithDCAcut = new TH1F("hTPCwithDCAcut", "hTPCwithDCAcut",fTPCAxis->GetNbins(), fTPCAxis->GetXmin(), fTPCAxis->GetXmax());
	TH1F* hTPCwithoutDCAcut = new TH1F("hTPCwithoutDCAcut", "hTPCwithoutDCAcut",fTPCAxis->GetNbins(), fTPCAxis->GetXmin(), fTPCAxis->GetXmax());

	TH2F* hDCAWithCut = new TH2F("hDCAWithCut","hDCAWithCut;xy;z",300,-3.,3.,100,-5.,5.);
	TH2F* hDCAWithoutCut = new TH2F("hDCAWithoutCut","hDCAWithoutCut;xy;z",300,-3.,3.,100,-5.,5.);

	// If available, these histograms will be filled with DCA info of tracks with filterbit 16 and 32.
	TH2F* hDCABit16 = new TH2F("hDCABit16","hDCABit16;xy;z",300,-3.,3.,100,-5.,5.);
	TH2F* hDCABit32 = new TH2F("hDCABit32","hDCABit32;xy;z",300,-3.,3.,100,-5.,5.);

	TH2F* hSPDHitVsFilterMask = new TH2F("hSPDHitVsFilterMask", "hSPDHitVsFilterMask;Filter Mask;SPD hit",10,-0.5,9.5,2,-0.5,1.5);
	for (Int_t iFilterMask = 0; iFilterMask <= 9; iFilterMask++) {
		(hSPDHitVsFilterMask->GetXaxis())->SetBinLabel(iFilterMask + 1, Form("%i", (1<<iFilterMask)));
	}
	(hSPDHitVsFilterMask->GetYaxis())->SetBinLabel(1, "NO");
	(hSPDHitVsFilterMask->GetYaxis())->SetBinLabel(2, "YES");

	// TOF histograms, with species separated using truth info.
	TH1F* hTOFtruthPion = new TH1F("hTOFtruthPion", "hTOFtruthPion",fTOFAxis->GetNbins(), fTOFAxis->GetXmin(), fTOFAxis->GetXmax());
	TH1F* hTOFtruthKaon = new TH1F("hTOFtruthKaon", "hTOFtruthKaon",fTOFAxis->GetNbins(), fTOFAxis->GetXmin(), fTOFAxis->GetXmax());
	TH1F* hTOFtruthProton = new TH1F("hTOFtruthProton", "hTOFtruthProton",fTOFAxis->GetNbins(), fTOFAxis->GetXmin(), fTOFAxis->GetXmax());
	TH1F* hTOFtruthMismatch = new TH1F("hTOFtruthMismatch", "hTOFtruthMismatch",fTOFAxis->GetNbins(), fTOFAxis->GetXmin(), fTOFAxis->GetXmax());
	TH1F* hTOFtruthOther = new TH1F("hTOFtruthOther", "hTOFtruthOther",fTOFAxis->GetNbins(), fTOFAxis->GetXmin(), fTOFAxis->GetXmax());

	// Check the eta distribution of events with and without trigger.
	TH2F* hEtaDistributionVsNTrigger = new TH2F("hEtaDistributionVsNTrigger", "hEtaDistributionVsNTrigger;#eta;N_{trig}", 64, -0.8, 0.8, 11, -0.5, 10.5);
	TH1F* hNEventsVsNTriggers = new TH1F("hNEventsVsNTriggers", "hNEventsVsNTriggers;N_{trig};N_{evt}", 11, -0.5, 10.5);

	// Reset to the first file, first event.
	if (!NextFile(kTRUE)) {return;}

	AliMiniTrackDiHadronPID* currentTrack = 0x0;

	Int_t nEvents = 0;
	Bool_t mcFound = kFALSE;
	
	// Loop over events.
	while (GetEvent()) {
		
		nEvents++;
		Double_t nTriggersFound = 0.; // In the regular triger range.

		for (Int_t iTrack = 0; iTrack < fCurrentEvent->GetNTracks(); ++iTrack) {

			currentTrack = fCurrentEvent->GetTrack(iTrack);

			// Check track filtermap against settings filtermask.
			if (fSettings->GetFilterMask()) {

				// Fill histogram that checks SPD hits VS filter mask.
				for (Int_t iFilterMask = 0; iFilterMask <= 9; iFilterMask++) {
						
					if (currentTrack->GetFilterMap() & (1<<iFilterMask)) {
						hSPDHitVsFilterMask->Fill(iFilterMask, currentTrack->HasSPDHit());

						if (iFilterMask == 4 && !(currentTrack->GetFilterMap() & 16)) {cout << "Track should not have entered the histogram." << endl;}
						if (iFilterMask == 5 && !(currentTrack->GetFilterMap() & 32)) {cout << "Track should not have entered the histogram." << endl;}
					}

				}

				if (currentTrack->GetFilterMap() & 16) {hDCABit16->Fill(currentTrack->GetDCAxy(), currentTrack->GetDCAz());}
				if (currentTrack->GetFilterMap() & 32) {hDCABit32->Fill(currentTrack->GetDCAxy(), currentTrack->GetDCAz());}

				if (!(currentTrack->GetFilterMap() & fSettings->GetFilterMask())) {
					continue;
				}

			}

			//hAssoc_Pt_Eta->Fill(currentTrack->Pt(), currentTrack->Eta());
			hPhivsNClus->Fill(currentTrack->Phi(), currentTrack->GetTPCSignalN());
			hEtaPtvsNClus->Fill(currentTrack->Eta(), currentTrack->Pt(), currentTrack->GetTPCSignalN());
			hTPCvsNClus->Fill(currentTrack->GetTPCsignalMinusExpected(fMassAssumption), currentTrack->GetTPCSignalN());
			hTOFvsNClus->Fill(currentTrack->GetTOFsignalMinusExpected(fMassAssumption), currentTrack->GetTPCSignalN());
			hTOFTPCvsNClus->Fill(currentTrack->GetTOFsignalMinusExpected(fMassAssumption), currentTrack->GetTPCsignalMinusExpected(fMassAssumption), currentTrack->GetTPCSignalN());

			if (currentTrack->Pt() > fSettings->GetMinTriggerPt() && currentTrack->Pt() < fSettings->GetMaxTriggerPt()) {nTriggersFound++;}

			// Select a narrow p_T and eta range.
			if ((currentTrack->Eta() < 0.7) || (currentTrack->Eta() > 0.8)) {continue;}
			if ((currentTrack->Pt() > 1.1) || (currentTrack->Pt() < 1.0)) {continue;}
		
			hTOFwithoutDCAcut->Fill(currentTrack->GetTOFsignalMinusExpected(fMassAssumption));
			hTPCwithoutDCAcut->Fill(currentTrack->GetTPCsignalMinusExpected(fMassAssumption));
			hDCAWithoutCut->Fill(currentTrack->GetDCAxy(), currentTrack->GetDCAz());

			if (fSettings->IsTrackSelectedForPID(currentTrack)) {

				hTOFwithDCAcut->Fill(currentTrack->GetTOFsignalMinusExpected(fMassAssumption));
				hTPCwithDCAcut->Fill(currentTrack->GetTPCsignalMinusExpected(fMassAssumption));
				hDCAWithCut->Fill(currentTrack->GetDCAxy(), currentTrack->GetDCAz());
			
			}

			// If MC, then also fill TOF histograms of particles with their true identity.
			if (currentTrack->IsMC()) {
				mcFound = kTRUE;

				if (currentTrack->IsTOFMatch()) {
					if (currentTrack->IsMCPion()) {hTOFtruthPion->Fill(currentTrack->GetTOFsignalMinusExpected(fMassAssumption));}
					else if (currentTrack->IsMCKaon()) {hTOFtruthKaon->Fill(currentTrack->GetTOFsignalMinusExpected(fMassAssumption));}
					else if (currentTrack->IsMCProton()) {hTOFtruthProton->Fill(currentTrack->GetTOFsignalMinusExpected(fMassAssumption));}
					else {hTOFtruthOther->Fill(currentTrack->GetTOFsignalMinusExpected(fMassAssumption));}
				} else {
					hTOFtruthMismatch->Fill(currentTrack->GetTOFsignalMinusExpected(fMassAssumption));
				}
			}
		}

		hNEventsVsNTriggers->Fill(nTriggersFound);

		for (Int_t iTrack = 0; iTrack < fCurrentEvent->GetNTracks(); ++iTrack) {

			currentTrack = fCurrentEvent->GetTrack(iTrack);
			hEtaDistributionVsNTrigger->Fill(currentTrack->Eta(), nTriggersFound);

		}

	}

	// Stop the timer.
	cout << "Analysis time: " << AliMiscToolsDiHadronPID::FormatTime(sw.RealTime()).Data() << endl;
	cout << "Processed events: " << nEvents << endl;

	fout->cd();
	hPhivsNClus->Write();
	hTPCvsNClus->Write();
	hTOFvsNClus->Write();
	hTOFTPCvsNClus->Write();
	hEtaPtvsNClus->Write();

	hTOFwithoutDCAcut->Write();
	hTOFwithDCAcut->Write();	

	hTPCwithoutDCAcut->Write();
	hTPCwithDCAcut->Write();

	hDCAWithoutCut->Write();
	hDCAWithCut->Write();
	hDCABit16->Write();
	hDCABit32->Write();

	hSPDHitVsFilterMask->Write();

	hNEventsVsNTriggers->Write();
	hEtaDistributionVsNTrigger->Write();

	if (mcFound) {
		hTOFtruthPion->Write();
		hTOFtruthKaon->Write();
		hTOFtruthProton->Write();
		hTOFtruthMismatch->Write();
		hTOFtruthOther->Write();

	}

	fout->Close();

}

// -----------------------------------------------------------------------
void AliProcessDataDiHadronPIDMini::MakeUISpectrum(const char* fileName) {

	// Creates unidentified spectra (needed to count triggers).
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());
	
	if (!fInputFileList) {cout << __func__ << " -> No input file list available..." << endl; return;}
	if (!(fInputFileList->GetEntriesFast())) {cout << __func__ << " -> Input file list is empty..." << endl; return;}

	// Start the timer.
	TStopwatch sw;
	sw.Start();

	// Create a file with a directory structure. Create histograms.
	TFile* fout = new TFile(fileName, "recreate");

	TH1F* hout = new TH1F("UIPtSpectrum", "UI p_{T} spectrum;p_{T} (GeV/c); Counts", 30, 0, 15);

	// Reset to the first file, first event.
	if (!NextFile(kTRUE)) {return;}
	AliMiniTrackDiHadronPID* currentTrack = 0x0;

	Int_t nEvents = 0;

	while (GetEvent()) {
		
		nEvents++;

		// Printout per 1000 events.
		if (nEvents % 1000 == 0) {
			cout << "Processing Event: " << nEvents << endl;
		}

		// Loop over tracks.
		for (Int_t iTrack = 0; iTrack < fCurrentEvent->GetNTracks(); ++iTrack) {

			currentTrack = fCurrentEvent->GetTrack(iTrack);

			// Check track filtermap against settings filtermask.
			if (!(currentTrack->GetFilterMap() & fSettings->GetFilterMask())) {continue;}

			hout->Fill(currentTrack->Pt());

		}

	}


	// Stop the timer.
	cout << "Analysis time: " << AliMiscToolsDiHadronPID::FormatTime(sw.RealTime()).Data() << endl;
	cout << "Processed events: " << nEvents << endl;

	fout->cd();

	hout->Write();
	fout->Close();

}

// -----------------------------------------------------------------------
void AliProcessDataDiHadronPIDMini::MakePtEtaSpectra(const char* fileName) {

	/**
	 * Method creates \f$(p_T, \eta)\f$ spectra objects. If the data is MC, then also "truth"
	 * (pT, eta) spectra are created.
	 *
	 * NOTES for MC analysis: 
	 *  - Several cuts are done on the data regarding PID. Those cuts are also
	 *    done on the truth (pt, eta) spectrum. This means that this method isn't suitable
	 *    to calculate efficiencies, but is only for comparing fitted v.s. truth.
	 *  - In the "truth" spectra we have removed TOF mismatches, since those are also
	 *    not counted in a fit.
	 */
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	if (!fInputFileList) {cout << __func__ << " -> No input file list available..." << endl; return;}
	if (!(fInputFileList->GetEntriesFast())) {cout << __func__ << " -> Input file list is empty..." << endl; return;}

	// Start the timer.
	TStopwatch sw;
	sw.Start();

	// Create a file with a directory structure. Create histograms.
	TFile* fout = new TFile(fileName, "recreate");

	TH2F* 		hist[fSettings->GetNMassAssumptions()][fEtaAxis->GetNbins()][fPtAxis->GetNbins()];
	TH2F*		histmm[fSettings->GetNMassAssumptions()][fEtaAxis->GetNbins()][fPtAxis->GetNbins()];
	TH2F*		histptetamc[fSettings->GetNMassAssumptions()];
	TH2F*		histptetamcMismatch[fSettings->GetNMassAssumptions()];

	TH2F*		histmmMC[fSettings->GetNMassAssumptions()][fEtaAxis->GetNbins()][fPtAxis->GetNbins()];
	TH1I*		histnevents = new TH1I("nEvents","nEvents",2,-0.5,1.5);

	(histnevents->GetXaxis())->SetBinLabel(1,"all");
	(histnevents->GetXaxis())->SetBinLabel(2,"selected");

	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNMassAssumptions(); ++iSpecies) {
		for (Int_t iEtaBin = 0; iEtaBin < fEtaAxis->GetNbins(); ++iEtaBin) {
			for (Int_t iPtBin = 0; iPtBin < fPtAxis->GetNbins(); ++iPtBin) {

				AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;

				// Create histograms for the signal.
				hist[iSpecies][iEtaBin][iPtBin] = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("hist_sp%i_eta%i_pt_%i",iSpecies,iEtaBin,iPtBin),
					Form("hist_sp%i_eta%i_pt_%i",iSpecies,iEtaBin,iPtBin),"F",fSettings->GetTOFAxis(currentSpecies), fSettings->GetTPCAxis(currentSpecies));

				// Create a set of histograms for the mismatches.
				histmm[iSpecies][iEtaBin][iPtBin] = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("histmm_sp%i_eta%i_pt_%i",iSpecies,iEtaBin,iPtBin),
					Form("histmm_sp%i_eta%i_pt_%i",iSpecies,iEtaBin,iPtBin),"F",fSettings->GetTOFAxis(currentSpecies), fSettings->GetTPCAxis(currentSpecies));

				// If MC, then make a set of histograms for TRUTH mismatches.
				histmmMC[iSpecies][iEtaBin][iPtBin] = (fSettings->GetIsMC() ? (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("histmmMC_sp%i_eta%i_pt_%i",iSpecies,iEtaBin,iPtBin),
					Form("histmmMC_sp%i_eta%i_pt_%i",iSpecies,iEtaBin,iPtBin),"F",fSettings->GetTOFAxis(currentSpecies), fSettings->GetTPCAxis(currentSpecies)) : 0x0);

			}
		}

		// Create set of histgrams for MC truth data
		histptetamc[iSpecies] = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("MCPtEtaSpec_%i",iSpecies), Form("MCPtEtaSpec_%i",iSpecies), "F", fPtAxis, fEtaAxis);
		histptetamcMismatch[iSpecies] = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("MCPtEtaMismatch_%i",iSpecies), Form("MCPtEtaMismatch_%i",iSpecies), "F", fPtAxis, fEtaAxis);
		
	}

	// Reset to the first file, first event.
	if (!NextFile(kTRUE)) {return;}
	AliMiniTrackDiHadronPID* currentTrack = 0x0;

	Bool_t mcFound = kFALSE;

	Int_t nEvents = 0;

	while (GetEvent()) {
		
		histnevents->Fill(0);

		// Take all events that pass the vtxZ and centrality selection.
		if (!IsSelected(fCurrentEvent)) {continue;}

		nEvents++;
		histnevents->Fill(1);

		// Printout per 1000 events.
		if (nEvents % 1000 == 0) {
			cout << "Processing Event: " << nEvents << endl;
		}

		// Loop over tracks.
		for (Int_t iTrack = 0; iTrack < fCurrentEvent->GetNTracks(); ++iTrack) {

			currentTrack = fCurrentEvent->GetTrack(iTrack);

			// Check track filtermap against settings filtermask.
			if (fSettings->GetFilterMask()) {if (!(currentTrack->GetFilterMap() & fSettings->GetFilterMask())) {continue;}}

			// Check PID selection.
			if (!(fSettings->IsTrackSelectedForPID(currentTrack))) {continue;}

			// Check the Charge selection.
			if (!(fSettings->IsTrackSelectedForCharge(currentTrack))) {continue;}

			// Determine the pt, eta bin of the current track.
			Int_t iEtaBin = fEtaAxis->FindBin(currentTrack->Eta());
			Int_t iPtBin = fPtAxis->FindBin(currentTrack->Pt());

			// Loop over all species.
			for (Int_t iSpecies = 0; iSpecies < fSettings->GetNMassAssumptions(); ++iSpecies) {

				if ( (iEtaBin > 0) && (iPtBin > 0) && (iEtaBin <= fEtaAxis->GetNbins()) && (iPtBin <= fPtAxis->GetNbins()) )  {

					// Fill signal and mismatch histograms.
					hist[iSpecies][iEtaBin - 1][iPtBin - 1]->Fill(currentTrack->GetTOFsignalMinusExpected(iSpecies), currentTrack->GetTPCsignalMinusExpected(iSpecies));
					histmm[iSpecies][iEtaBin - 1][iPtBin - 1]->Fill(currentTrack->GetTOFRandom(iSpecies), currentTrack->GetTPCsignalMinusExpected(iSpecies));

					if (fSettings->GetIsMC() && (!currentTrack->IsTOFMatch())) {
						histmmMC[iSpecies][iEtaBin - 1][iPtBin - 1]->Fill(currentTrack->GetTOFsignalMinusExpected(iSpecies), currentTrack->GetTPCsignalMinusExpected(iSpecies));
					}

				}

			}

			// Fill mc histograms
			if (currentTrack->IsMC()) {
				mcFound = kTRUE;

				// Cut away the mismatches, since they're also cut out in the fit, only if mismatch info is available in the MC production.
				if (fSettings->GetIsTOFMismatchInfoAvailableMC()) {
					if (currentTrack->IsTOFMatch()) {
						if (currentTrack->IsMCPion()){histptetamc[0]->Fill(currentTrack->Pt(), currentTrack->Eta());}
						if (currentTrack->IsMCKaon()){histptetamc[1]->Fill(currentTrack->Pt(), currentTrack->Eta());}
						if (currentTrack->IsMCProton()){histptetamc[2]->Fill(currentTrack->Pt(), currentTrack->Eta());}
					} else {
						if (currentTrack->IsMCPion()){histptetamcMismatch[0]->Fill(currentTrack->Pt(), currentTrack->Eta());}
						if (currentTrack->IsMCKaon()){histptetamcMismatch[1]->Fill(currentTrack->Pt(), currentTrack->Eta());}
						if (currentTrack->IsMCProton()){histptetamcMismatch[2]->Fill(currentTrack->Pt(), currentTrack->Eta());}
					}
				} else {
					if (currentTrack->IsMCPion()){histptetamc[0]->Fill(currentTrack->Pt(), currentTrack->Eta());}
					if (currentTrack->IsMCKaon()){histptetamc[1]->Fill(currentTrack->Pt(), currentTrack->Eta());}
					if (currentTrack->IsMCProton()){histptetamc[2]->Fill(currentTrack->Pt(), currentTrack->Eta());}
				}
			}

		}

	}

	// Write nevents.
	fout->cd();
	histnevents->Write();

	// Create (Pt, Eta) spectra for all species.
	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNMassAssumptions(); iSpecies++) {
			
		AliPtEtaSpectrumDiHadronPID* spectrum = new AliPtEtaSpectrumDiHadronPID(Form("Spec_%i", iSpecies), Form("Spec_%i", iSpecies), (AliSettingsDiHadronPID::Species)iSpecies);

		for (Int_t iEtaBin = 1; iEtaBin <= fEtaAxis->GetNbins(); ++iEtaBin) {
			for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); ++iPtBin) {
				if (fSettings->GetIsMC() && fSettings->IsMCLabelUsedForMismatches()) {
					spectrum->ImportPIDDataForBin(hist[iSpecies][iEtaBin - 1][iPtBin - 1], histmmMC[iSpecies][iEtaBin - 1][iPtBin - 1], iPtBin, iEtaBin);
				} else {
					spectrum->ImportPIDDataForBin(hist[iSpecies][iEtaBin - 1][iPtBin - 1], histmm[iSpecies][iEtaBin - 1][iPtBin - 1], iPtBin, iEtaBin);
				}
			}
		}

		fout->cd();
		spectrum->Write();
		if (mcFound) {
			histptetamc[iSpecies]->Write();
			histptetamcMismatch[iSpecies]->Write();
		} 
	}

	// Write number of events histogram.

	// Stop the timer.
	cout << "Analysis time: " << AliMiscToolsDiHadronPID::FormatTime(sw.RealTime()).Data() << endl;
	cout << "Processed events: " << nEvents << endl;
	fout->Close();

}

// -----------------------------------------------------------------------
void AliProcessDataDiHadronPIDMini::MakeTemplates(const char* fileName) {

	// The idea is that we loop over all triggers, and generate for each trigger
	// a number of associateds, and then fill a (dEta, eta) histogram. In this way
	// the trigger efficiency is taken into account, wheras the associated (identified)
	// efficiency must be taken from the fits.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	if (!fInputFileList) {cout << __func__ << " -> No input file list available..." << endl; return;}
	if (!(fInputFileList->GetEntriesFast())) {cout << __func__ << " -> Input file list is empty..." << endl; return;}

	// Start the timer.
	TStopwatch sw;
	sw.Start();

	// Create random number generator.
	TRandom3 rnd;

	// Create a histogram.
	TH2F* hist = (TH2F*)AliHistToolsDiHadronPID::MakeHist("etaDEtaCorr", "etaDEtaCorr", "F", fDEtaAxis, fEtaAxis);

	// Create template objects.
	//AliFitTemplatesDiHadronPID* pionMassAssumptionTemplates = new AliFitTemplatesDiHadronPID(fPtAxis, fEtaAxis, fDEtaAxis, 0);

	// Reset to the first file, first event.
	if (!NextFile(kTRUE)) {return;}
	AliMiniTrackDiHadronPID* currentTriggerTrack = 0x0;

	// Loop over events.
	Int_t nEvents = 0;
	while (GetEvent()) {
		
		nEvents++;

		// Loop over trigger tracks.
		for (Int_t iTrack = 0; iTrack < fCurrentEvent->GetNTracks(); iTrack++) {
				
			currentTriggerTrack = fCurrentEvent->GetTrack(iTrack);

			// Check track filtermap against settings filtermask.
			if (fSettings->GetFilterMask()) {if (!(currentTriggerTrack->GetFilterMap() & fSettings->GetFilterMask())) {continue;}}

			// Check the pT of the trigger.
			if ((currentTriggerTrack->Pt() < fSettings->GetMinTriggerPt()) ||
				(currentTriggerTrack->Pt() > fSettings->GetMaxTriggerPt())) {continue;}	

			// Generate 1000 tracks with a flat distribution in eta. Note that it's okay to generate the tracks according
			// to a flat distribution, since when creating the templates the contibutions from each (pT, eta) bin is weighted
			// with the fitted yield.
			for (Int_t iAssoc = 0; iAssoc < 1000; iAssoc++) {
					
				Double_t etaAssoc = (rnd.Rndm() * 1.6) - 0.8;
				Double_t deltaEta = currentTriggerTrack->Eta() - etaAssoc;				
			
				hist->Fill(deltaEta, etaAssoc);

			}

		}

	}

	// Create a templates object, one for every mass assumption.
	AliFitTemplatesDiHadronPID* pionTemplates = new AliFitTemplatesDiHadronPID("pionTemplates", "pionTemplates", fPtAxis, hist, AliSettingsDiHadronPID::kPion);
	AliFitTemplatesDiHadronPID* kaonTemplates = new AliFitTemplatesDiHadronPID("kaonTemplates", "kaonTemplates", fPtAxis, hist, AliSettingsDiHadronPID::kKaon);
	AliFitTemplatesDiHadronPID* protonTemplates = new AliFitTemplatesDiHadronPID("protonTemplates", "protonTemplates", fPtAxis, hist, AliSettingsDiHadronPID::kProton);
	
	delete hist; hist = 0x0;

	// Create file.
	TFile* fout = new TFile(fileName, "recreate");

	fout->cd();
	pionTemplates->Write();
	delete pionTemplates; pionTemplates = 0x0;

	kaonTemplates->Write();
	delete kaonTemplates; kaonTemplates = 0x0;

	protonTemplates->Write();
	delete protonTemplates; protonTemplates = 0x0;

	// Stop the timer.
	cout << "Analysis time: " << AliMiscToolsDiHadronPID::FormatTime(sw.RealTime()).Data() << endl;
	cout << "Processed events: " << nEvents << endl;

	fout->Close();

}

// -----------------------------------------------------------------------
void AliProcessDataDiHadronPIDMini::Correlate(Int_t vtxZbin, Int_t centBin) {

	AliLogDiHadronPID::Debug2(Form("Called for vtxZbin: %i, centBin: %i", vtxZbin, centBin), __func__, ClassName());

	// Performs identified correlations for one of the (vtx-z, centrality) bins.
	if (!fInputFileList) {AliLogDiHadronPID::Error(Form("No input file list available..."), __func__, ClassName()); return;}
	if (!(fInputFileList->GetEntriesFast())) {AliLogDiHadronPID::Error(Form("Imput file list is empty..."), __func__, ClassName()); return;}

	// Start the timer.
	TStopwatch sw;
	sw.Start();

	// Reset the mixing pool.
	ResetTriggerPool();

	// Clear the buffer.
	ClearBuffer();

	// Reset to the first file, first event.
	if (!NextFile(kTRUE)) {return;}
	AliMiniTrackDiHadronPID* currentTriggerTrack = 0x0;
	AliMiniTrackDiHadronPID* currentAssocTrack = 0x0;
	
	// Create an array that temporarily stores triggers.
	TObjArray* triggerArray = new TObjArray(10);
	triggerArray->SetOwner(kFALSE);

	Int_t nEvents = 0;

	// Loop over events.
	while (GetEvent()) {
		
		nEvents++;

		// Printout per 1000 events.
		if (nEvents % 1000 == 0) {
			cout << "Processing Event: " << nEvents << endl;
		}

		// Take only events in the right (vtx-z, centrality) bin.
		if (!IsSelected(fCurrentEvent, vtxZbin, centBin)) {continue;}
		
		// Reset the trigger buffer.
		triggerArray->Clear();

		// Loop over triggers.
		for (Int_t iTrigger = 0; iTrigger < fCurrentEvent->GetNTracks(); ++iTrigger) {

			currentTriggerTrack = fCurrentEvent->GetTrack(iTrigger);
			
			// Check track filtermap against settings filtermask.
			if (fSettings->GetFilterMask()) {if (!(currentTriggerTrack->GetFilterMap() & fSettings->GetFilterMask())) {continue;}}

			// Check the pT of the trigger.
			if ((currentTriggerTrack->Pt() < fSettings->GetMinTriggerPt()) ||
				(currentTriggerTrack->Pt() > fSettings->GetMaxTriggerPt())) {continue;}

			// Add trigger to the trigger array.			
			triggerArray->Add(currentTriggerTrack);	
			fCurrentTriggerHist->Fill(currentTriggerTrack->Pt());		

			// Loop over associateds.
			for (Int_t iAssoc = 0; iAssoc < fCurrentEvent->GetNTracks(); ++iAssoc) {
				
				// Load Associated Track and perform PID cut.
				currentAssocTrack = fCurrentEvent->GetTrack(iAssoc);
				if (fSettings->GetFilterMask()) {if (!(currentAssocTrack->GetFilterMap() & fSettings->GetFilterMask())) {continue;}}
				if (!(fSettings->IsTrackSelectedForPID(currentAssocTrack))) {continue;}

				// Check the Charge selection.
				if (!(fSettings->IsTrackSelectedForCharge(currentAssocTrack))) {continue;}

				// Check the pT of the associated.
				if ((currentAssocTrack->Pt() < fSettings->GetMinAssociatedPt()) ||
					(currentAssocTrack->Pt() > fSettings->GetMaxAssociatedPt())) {continue;}

				// Protect against making auto correlations.
				if (iTrigger == iAssoc) {AliLogDiHadronPID::Error(Form("Same track is used as trigger and associated..."), __func__, ClassName());}

				Double_t deltaPhi = currentTriggerTrack->Phi() - currentAssocTrack->Phi();
				if (deltaPhi < -TMath::Pi()/2.) {deltaPhi += 2.*TMath::Pi();}
				else if (deltaPhi > 3.*TMath::Pi()/2.) {deltaPhi -= 2.*TMath::Pi();}
				Double_t deltaEta = currentTriggerTrack->Eta() - currentAssocTrack->Eta();

				// Fill Correlation histograms.
				Int_t ptBin = fPtAxis->FindBin(currentAssocTrack->Pt());

				if ((ptBin < 1) || (ptBin > fPtAxis->GetNbins())) {continue;}

				((TH2F*)fCurrentCorrUI->At(ptBin - 1))->Fill(deltaPhi, deltaEta);
				
				Double_t filling[4] = {deltaPhi, deltaEta, currentAssocTrack->GetTOFsignalMinusExpected(fMassAssumption), currentAssocTrack->GetTPCsignalMinusExpected(fMassAssumption)};
				((THnF*)fCurrentCorrPID->At(ptBin - 1))->Fill(filling);

				// Fill MC Correlation histograms if requested.
				if (fSettings->GetIsMC()) {

					// If MC info is available, then only add matched tracks.
					if ((fSettings->GetIsTOFMismatchInfoAvailableMC() && currentAssocTrack->IsTOFMatch()) || 
						(!(fSettings->GetIsTOFMismatchInfoAvailableMC()))) {

						if (currentAssocTrack->IsMCPion()) {((TH2F*)fCurrentCorrMCPion->At(ptBin - 1))->Fill(deltaPhi, deltaEta);}
						if (currentAssocTrack->IsMCKaon()) {((TH2F*)fCurrentCorrMCKaon->At(ptBin - 1))->Fill(deltaPhi, deltaEta);}
						if (currentAssocTrack->IsMCProton()) {((TH2F*)fCurrentCorrMCProton->At(ptBin - 1))->Fill(deltaPhi, deltaEta);}

					}


				}
				
			}

		}

		// Mix events if the pool is full.
		if (fPoolIsFilled) {

			for (Int_t iMixTrigger = 0; iMixTrigger < fTriggerPool->GetEntriesFast(); ++iMixTrigger) {

				// Tracks in the pool fall within the required pT range, no need to check.
				currentTriggerTrack = (AliMiniTrackDiHadronPID*)fTriggerPool->At(iMixTrigger);

				// Loop over associateds.
				for (Int_t iAssoc = 0; iAssoc < fCurrentEvent->GetNTracks(); ++iAssoc) {

					// Load associated track and do PID cut.
					currentAssocTrack = fCurrentEvent->GetTrack(iAssoc);
					if (!(fSettings->IsTrackSelectedForPID(currentAssocTrack))) {continue;}

					// Check the Charge selection.
					if (!(fSettings->IsTrackSelectedForCharge(currentAssocTrack))) {continue;}


					// Check the pT of the associated.
					if ((currentAssocTrack->Pt() < fSettings->GetMinAssociatedPt()) ||
						(currentAssocTrack->Pt() > fSettings->GetMaxAssociatedPt())) {continue;}

					Double_t deltaPhi = currentTriggerTrack->Phi() - currentAssocTrack->Phi();
					if (deltaPhi < -TMath::Pi()/2.) {deltaPhi += 2.*TMath::Pi();}
					else if (deltaPhi > 3.*TMath::Pi()/2.) {deltaPhi -= 2.*TMath::Pi();}
					Double_t deltaEta = currentTriggerTrack->Eta() - currentAssocTrack->Eta();

					// Fill Mixed event histograms.
					Int_t ptBin = fPtAxis->FindBin(currentAssocTrack->Pt());

					if ((ptBin < 1) || (ptBin > fPtAxis->GetNbins())) {continue;}

					((TH2F*)fCurrentMixedUI->At(ptBin - 1))->Fill(deltaPhi, deltaEta);

					Double_t filling[4] = {deltaPhi, deltaEta, currentAssocTrack->GetTOFsignalMinusExpected(fMassAssumption), currentAssocTrack->GetTPCsignalMinusExpected(fMassAssumption)};
					((THnF*)fCurrentMixedPID->At(ptBin - 1))->Fill(filling);		

					// Fill MC Mixed Event histograms if requested.
					if (fSettings->GetIsMC()) {

						// If MC info is available, then only add matched tracks.
						if ((fSettings->GetIsTOFMismatchInfoAvailableMC() && currentAssocTrack->IsTOFMatch()) || 
							(!(fSettings->GetIsTOFMismatchInfoAvailableMC()))) {

							if (currentAssocTrack->IsMCPion()) {((TH2F*)fCurrentMixedMCPion->At(ptBin - 1))->Fill(deltaPhi, deltaEta);}
							if (currentAssocTrack->IsMCKaon()) {((TH2F*)fCurrentMixedMCKaon->At(ptBin - 1))->Fill(deltaPhi, deltaEta);}
							if (currentAssocTrack->IsMCProton()) {((TH2F*)fCurrentMixedMCProton->At(ptBin - 1))->Fill(deltaPhi, deltaEta);}

						}

					}

				}

			}

		}

		// Fill the pool, and check whether the pool is filled.
		for (Int_t iTrigger = 0; iTrigger < triggerArray->GetEntriesFast(); ++iTrigger) {

			// Check if there's already a track at the index where we intend to write.
			if (fTriggerPool->At(fPoolWriteIndex)) {
				AliLogDiHadronPID::Debug4(Form("Removing trigger at index: %i", fPoolWriteIndex), __func__, ClassName());
				AliMiniTrackDiHadronPID* oldtrig = (AliMiniTrackDiHadronPID*)fTriggerPool->At(fPoolWriteIndex);
				fTriggerPool->RemoveAt(fPoolWriteIndex);
				delete oldtrig; oldtrig = 0x0;
			}

			// Add new trigger.
			AliMiniTrackDiHadronPID* newtrig = new AliMiniTrackDiHadronPID(*((AliMiniTrackDiHadronPID*)triggerArray->At(iTrigger)));
			fTriggerPool->AddAt(newtrig, fPoolWriteIndex);
			if (fVerboseLevel > 1) {cout << "Add trigger at index: " << fPoolWriteIndex << ", pool size: " << fTriggerPool->GetSize() << endl;}
			if (fVerboseLevel > 2) {PrintPoolPointers();}

			// Increase the index, and check if the pool is full.
			fPoolWriteIndex++;
			if (fPoolWriteIndex == fTriggerPool->GetSize()) {
				fPoolWriteIndex = 0;

				// If this is the first time that we reach the end of the pool, then
				// set a flag that the pool is full, and save the index of the event.
				if (!fPoolIsFilled) {
					fPoolIsFilled = kTRUE;
					fStartedMixingAtFile = fCurrentFileIndex;
					fStartedMixingAtEvent = fCurrentEventIndex;
					if (fVerboseLevel > 0) {cout << "Event pool has been filled, start mixing now!" << endl;}

				}
			
			}
		
		}

	} // End loop over events.

	//  - TODO: Minimize Code Copying -
	// Mix the event that were not yet mixed (i.e., the ones that came before the mixing started).
	// Note that the pool should keep being refresed.
	if (fPoolIsFilled) {

		// Start from the beginning.
		NextFile(kTRUE);

		while (GetEvent()) {

			nEvents++;

			// Take only events in the right (vtx-z, centrality) bin.
			if (!IsSelected(fCurrentEvent, vtxZbin, centBin)) {continue;}

			if (fCurrentFileIndex == fStartedMixingAtFile && fCurrentEventIndex == fStartedMixingAtEvent) {break;}

			// Mix events.
			for (Int_t iMixTrigger = 0; iMixTrigger < fTriggerPool->GetEntriesFast(); ++iMixTrigger) {

				currentTriggerTrack = (AliMiniTrackDiHadronPID*)fTriggerPool->At(iMixTrigger);

				// Loop over associateds.
				for (Int_t iAssoc = 0; iAssoc < fCurrentEvent->GetNTracks(); ++iAssoc) {

					// Load Associated track, and do PID cut.
					currentAssocTrack = fCurrentEvent->GetTrack(iAssoc);
					if (!(fSettings->IsTrackSelectedForPID(currentAssocTrack))) {continue;}

					// Check the Charge selection.
					if (!(fSettings->IsTrackSelectedForCharge(currentAssocTrack))) {continue;}

					// Check the pT of the associated.
					if ((currentAssocTrack->Pt() < fSettings->GetMinAssociatedPt()) ||
						(currentAssocTrack->Pt() > fSettings->GetMaxAssociatedPt())) {continue;}

					Double_t deltaPhi = currentTriggerTrack->Phi() - currentAssocTrack->Phi();
					if (deltaPhi < -TMath::Pi()/2.) {deltaPhi += 2.*TMath::Pi();}
					else if (deltaPhi > 3.*TMath::Pi()/2.) {deltaPhi -= 2.*TMath::Pi();}
					Double_t deltaEta = currentTriggerTrack->Eta() - currentAssocTrack->Eta();

					// Fill mixed event histograms.
					Int_t ptBin = fPtAxis->FindBin(currentAssocTrack->Pt());

					if ((ptBin < 1) || (ptBin > fPtAxis->GetNbins())) {continue;}

					((TH2F*)fCurrentMixedUI->At(ptBin - 1))->Fill(deltaPhi, deltaEta);

					Double_t filling[4] = {deltaPhi, deltaEta, currentAssocTrack->GetTOFsignalMinusExpected(fMassAssumption), currentAssocTrack->GetTPCsignalMinusExpected(fMassAssumption)};
					((THnF*)fCurrentMixedPID->At(ptBin - 1))->Fill(filling);		

					// Fill MC Mixed Event histograms if requested.
					if (fSettings->GetIsMC()) {

						// If MC info is available, then only add matched tracks.
						if ((fSettings->GetIsTOFMismatchInfoAvailableMC() && currentAssocTrack->IsTOFMatch()) || 
							(!(fSettings->GetIsTOFMismatchInfoAvailableMC()))) {

							if (currentAssocTrack->IsMCPion()) {((TH2F*)fCurrentMixedMCPion->At(ptBin - 1))->Fill(deltaPhi, deltaEta);}
							if (currentAssocTrack->IsMCKaon()) {((TH2F*)fCurrentMixedMCKaon->At(ptBin - 1))->Fill(deltaPhi, deltaEta);}
							if (currentAssocTrack->IsMCProton()) {((TH2F*)fCurrentMixedMCProton->At(ptBin - 1))->Fill(deltaPhi, deltaEta);}

						}

					}

				}

			}

			// Update pool contents.
			for (Int_t iTrigger = 0; iTrigger < fCurrentEvent->GetNTracks(); ++iTrigger) {

				// Add new trigger.
				currentTriggerTrack = fCurrentEvent->GetTrack(iTrigger);

				// Check the pT of the trigger.
				if ((currentTriggerTrack->Pt() < fSettings->GetMinTriggerPt()) ||
					(currentTriggerTrack->Pt() > fSettings->GetMaxTriggerPt())) {continue;}

				// Check if there's already a track at the index where we intend to write.
				if (fTriggerPool->At(fPoolWriteIndex)) {
					AliLogDiHadronPID::Debug4(Form("Removing trigger at index: %i", fPoolWriteIndex), __func__, ClassName());
					AliMiniTrackDiHadronPID* oldtrig = (AliMiniTrackDiHadronPID*)fTriggerPool->At(fPoolWriteIndex);
					fTriggerPool->RemoveAt(fPoolWriteIndex);
					delete oldtrig; oldtrig = 0x0;
				}

				AliMiniTrackDiHadronPID* newtrig = new AliMiniTrackDiHadronPID(*currentTriggerTrack);
				fTriggerPool->AddAt(newtrig, fPoolWriteIndex);
				AliLogDiHadronPID::Debug4(Form("Add trigger at index: %i, pool size: %i.", fPoolWriteIndex, fTriggerPool->GetSize()), __func__, ClassName());
				//PrintPoolPointers();

				// Increase the index, and check if the pool is full.
				fPoolWriteIndex++;
				if (fPoolWriteIndex == fTriggerPool->GetSize()) {fPoolWriteIndex = 0;}

			}

		}

	}

	// Stop the timer.
	cout << "Analysis time: " << AliMiscToolsDiHadronPID::FormatTime(sw.RealTime()).Data() << endl;
	cout << "Processed events: " << nEvents << endl;

}

/*
// -----------------------------------------------------------------------
void AliProcessDataDiHadronPIDMini::CorrelateAll() {

	// 1) Perform correlations of all the bins in vtx-z and centraltiy.
	// 2) Perform mixed event correction.
	// 3) Save output to files.
	// 4) Merge resulting files.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	// Create output directory
	TString currentdir = (gSystem->pwd());
	cout << "Current directory: " << currentdir << endl;
	if (!gSystem->cd(fWorkDir.Data())) {
		if (gSystem->mkdir(fWorkDir.Data(), kTRUE) < 0) {cout << __func__ << " : ERROR: Could not create working directory: " << currentdir.Data() << "/" << fWorkDir.Data() << endl; return;}
	} else {
		cout << __func__ << " : INFO: Working directory already exists: " << currentdir.Data() << "/" << fWorkDir.Data() << endl;	
		gSystem->cd(currentdir.Data());
	}

	if (!gSystem->cd(fOutputDir.Data())) {
		if (gSystem->mkdir(fOutputDir.Data(), kTRUE) < 0) {cout << __func__ << " : ERROR: Could not create output directory: " << fOutputDir.Data() << endl; return;}
	} else {
		cout << __func__ << " : INFO: Output directory already exists: " << fOutputDir.Data() << endl;
		gSystem->cd(currentdir.Data());
	}

	// Start the time.
	TThread* mixingThread = 0x0;
	TStopwatch sw;
	sw.Start();

	// Pointer to the mixing function.
	TThread::VoidRtnFunc_t mixingfunc = AliProcessDataDiHadronPIDMini::CorrectForMixedEvent;

	for (Int_t iVtxZ = 1; iVtxZ <= fVertexZAxis->GetNbins(); ++iVtxZ) {
		for (Int_t iCent = 1; iCent <= fCentralityAxis->GetNbins(); ++iCent) {
			cout << "-----------------------------------------------------------------------"<<endl;
			cout << __func__ << ": Performing analysis for vtxZ = "<<iVtxZ<<" and cent = "<<iCent<<endl;  
			cout << "-----------------------------------------------------------------------"<<endl<<endl;
			gSystem->mkdir(Form("%s/vtxz%icent%i",fWorkDir.Data(), iVtxZ, iCent), kTRUE);
			Correlate(iVtxZ, iCent);
			cout << __func__ << ": Analysis done!" << endl;


			TString foutname(Form("%s/vtxz%icent%i/%s", fWorkDir.Data(), iVtxZ, iCent, fOutputFileName.Data()));
			SaveBuffer(foutname.Data());

			cout << __func__ << ": Wrote buffer to: " << foutname.Data() << endl;

			// Wait for the previous mixing thread to complete.
			if (mixingThread) {
				mixingThread->Join();
				cout << __func__ << ": Previous mixing thread is joined..." << endl; 
				delete mixingThread; mixingThread = 0x0;
			}

			// Create an array on the heap with (inputfile, outputfile). Cleaning up of this array
			// is the responsibility of the CorrectForMixedEvent thread.
			TString foutnameCorr(foutname);
			foutnameCorr.Insert(foutname.Last('.'), fCorrExtension);
			TObjArray* fnames = new TObjArray(2);
			fnames->SetOwner(kTRUE);
			fnames->AddLast(new TObjString(foutname.Data()));
			fnames->AddLast(new TObjString(foutnameCorr.Data()));

			// Start the correction thread. Note that it's important that the pointer points to something on the heap!
			// Before we tried foutname.Data(), which is a pointer, but the owner of that pointer (the foutname TString) was on the stack,
			// hence it ran out of scope.
			// TThread::VoidRtnFunc_t rf = AliProcessDataDiHadronPIDMini::CorrectForMixedEvent;
			mixingThread = new TThread(Form("mixingThread_vtxz%icent%i", iVtxZ, iCent), mixingfunc, (void*)fnames);
			mixingThread->Run();
			cout << __func__ << ": New mixing thread started..." << endl;
		}
	}

	if (mixingThread) {
		mixingThread->Join();
		cout << __func__ << ": Last mixing thread is joined..." << endl; 
		delete mixingThread; mixingThread = 0x0;		
	}

	// Correlations are done.
	cout << __func__ << ": Analysis time: " << AliMiscToolsDiHadronPID::FormatTime(sw.RealTime()).Data() << endl;
	sw.Start();

	// Initalize mergers for merging the corrected and uncorrected files.
	TFileMerger* merger = new TFileMerger();				// Merger for uncorrected files.
	merger->OutputFile(Form("%s/%s",fOutputDir.Data(), fOutputFileName.Data()));

	TFileMerger* mergerCorr = new TFileMerger();			// Merger for corrected files.
	TString outputFileNameCorr(fOutputFileName);
	outputFileNameCorr.Insert(outputFileNameCorr.Last('.'), fCorrExtension);
	mergerCorr->OutputFile(Form("%s/%s",fOutputDir.Data(), outputFileNameCorr.Data()));

	for (Int_t iVtxZ = 1; iVtxZ <= fVertexZAxis->GetNbins(); ++iVtxZ) {
		for (Int_t iCent = 1; iCent <= fCentralityAxis->GetNbins(); ++iCent) {

			TString foutname(Form("%s/vtxz%icent%i/%s", fWorkDir.Data(), iVtxZ, iCent, fOutputFileName.Data()));			
			merger->AddFile(foutname.Data());

			TString foutnameCorr(foutname);
			foutnameCorr.Insert(foutnameCorr.Last('.'), fCorrExtension);
			mergerCorr->AddFile(foutnameCorr.Data());

		}
	}

	// Getting a pointer to the Merge function.
	TThread::VoidRtnFunc_t mergerfunc = AliProcessDataDiHadronPIDMini::Merge;

	// Merge the uncorrected files.
	TThread mergerThread("mergerThread", mergerfunc, merger);
	cout << __func__ << Form(": Opening thread to merge uncorrected files (%s).", mergerThread.GetName()) << endl;
	mergerThread.Run();

	// Merge the corected files.
	TThread mergerCorrThread("mergerCorrThread", mergerfunc, mergerCorr);
	cout << __func__ << Form(": Opening thread to merger corrected files (%s).", mergerCorrThread.GetName()) << endl;
	mergerCorrThread.Run();

	// Thread that merges uncorrected results is done.
	mergerThread.Join();
	cout << __func__ << Form(": Thread: %s is done.", mergerCorrThread.GetName()) << endl;

	// Once the uncorrected merger thread is done, we can start performing mixed events correction on it.
	TObjArray* fnames = new TObjArray(2);
	fnames->SetOwner(2);
	fnames->AddLast(new TObjString(Form("%s/%s", fOutputDir.Data(), fOutputFileName.Data())));
	TString outputFileNameCorrRet(outputFileNameCorr);
	outputFileNameCorrRet.Insert(outputFileNameCorr.Last('.'), "Ret");
	fnames->AddLast(new TObjString(Form("%s/%s", fOutputDir.Data(), outputFileNameCorrRet.Data())));
	TThread finalMixingThread("finalMergerThread", mixingfunc, fnames);
	cout << __func__ << Form(": Opening thread to correct the merged uncorrected output (%s).", finalMixingThread.GetName()) << endl;
	finalMixingThread.Run();

	// Thread that mergers corrected results is done.
	mergerCorrThread.Join();
	cout << __func__ << Form(": Thread: %s is done.", mergerCorrThread.GetName()) << endl;
	cout << __func__ << ": Merging time: " << AliMiscToolsDiHadronPID::FormatTime(sw.RealTime()).Data() << endl;

	// Final mxing thread is done.
	finalMixingThread.Join();
	cout << __func__ << Form(": Thread: %s is done.", finalMixingThread.GetName()) << endl;

}
*/

// -----------------------------------------------------------------------
void AliProcessDataDiHadronPIDMini::MakeEfficiencies(const char* fileName) {

    /**
     * This method calculates the efficiencies based on a MC production. It calculates the efficiencies
     * per particle species and charge for:
     *  - Tracking (Reconstructed/Generated)
     *  - TPC cut efficiency (TPC cluster cut/Reconstructed) (USED FOR QA)
     *  - TOF cut efficiency (kTOFout+kTIME/Reconstructed) (USED FOR QA)
     *  - TOF matching efficiency (kTOFout+kTIME+TOF match/Reconstructed) (USED FOR QA)
     *  - Total PID efficiency (TPC cluster cut+kTOFout+kTIME+TOF match/Reconstructed) (THIS ONE SHOULD BE USED FOR ANALYSIS)
     * 
     * Two important points:
     *  - This function already takes care of the G3/F correction whenever appropriate (for 2010 pp and PbPb data).
     *    The efficiencies that are produced can therefore directly be used and compared with other analyses.
     *  - If the user has set a rapidity cut in config.C, then the efficiency will be calculated only in the region |y| < 0.5.
     *    Note that it is important to get this correct, as the sensitivity of the TOF detector is generally better at smaller
     *    rapidities.
     */

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	if (!fInputFileList) {cout << __func__ << " -> No input file list available..." << endl; return;}
	if (!(fInputFileList->GetEntriesFast())) {cout << __func__ << " -> Input file list is empty..." << endl; return;}

	// Check if MC.
	if (!fSettings->GetIsMC()) {cout << __func__ << " -> Dataset isn't MC..." << endl; return;}

	// Check if rapidity cut is demanded for inclusive pectra.
	if (fSettings->GetRapidityCutForInclusiveSpectra()) { cout << __func__ << " -> Rapidity cut found, make inclusive spectra efficiencies." << endl;} 
	else { cout << __func__ << " -> No rapidity cut, make correlation efficiencies." << endl;}

	// Start the timer.
	TStopwatch sw;
	sw.Start();

	const Bool_t isForSpectra = fSettings->GetRapidityCutForInclusiveSpectra();
	const Int_t nCharges = 2;
    AliLogDiHadronPID::Info(Form("The efficiencies that are calculated have %s rapidity cut.", isForSpectra ? "a" : "no"), __func__, ClassName());

	// Create a file, histograms.
	TFile* fout = new TFile(fileName, "recreate");

    // Reconstructed tracks.
	TH1F*		hRecPt[fSettings->GetNMassAssumptions()][nCharges];
	TH2F*		hRecPtEta[fSettings->GetNMassAssumptions()][nCharges];

    // TOF PID Efficiency - Only meant for comparison with TOF analysis.
	TH1F*		hRecSelTOFPt[fSettings->GetNMassAssumptions()][nCharges];
	TH2F*		hRecSelTOFPtEta[fSettings->GetNMassAssumptions()][nCharges];
	TH1F*		hRecSelTOFMatchPt[fSettings->GetNMassAssumptions()][nCharges];
	TH2F*		hRecSelTOFMatchPtEta[fSettings->GetNMassAssumptions()][nCharges];

    // TPC PID Efficiency - Only meant for comparison with TPC analysis.
	TH1F*		hRecSelTPCPt[fSettings->GetNMassAssumptions()][nCharges];
	TH2F*		hRecSelTPCPtEta[fSettings->GetNMassAssumptions()][nCharges];

    // Total PID Efficiency - Use this for analysis. 
	TH1F*		hRecSelPIDPt[fSettings->GetNMassAssumptions()][nCharges];
	TH2F*		hRecSelPIDPtEta[fSettings->GetNMassAssumptions()][nCharges];
	TH1F*		hRecSelPIDMatchPt[fSettings->GetNMassAssumptions()][nCharges];
	TH2F*		hRecSelPIDMatchPtEta[fSettings->GetNMassAssumptions()][nCharges];

    // Generated Particles (Truth).
	TH1F*		hGenPrimPt[fSettings->GetNMassAssumptions()][nCharges];
	TH2F*		hGenPrimPtEta[fSettings->GetNMassAssumptions()][nCharges];
	TH1F*		hGenSecPt[fSettings->GetNMassAssumptions()][nCharges];
	TH2F*		hGenSecPtEta[fSettings->GetNMassAssumptions()][nCharges];

    // Create the histograms and call sumw2().
	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNMassAssumptions(); iSpecies++) {		
		for (Int_t iCharge = 0; iCharge < nCharges; iCharge++) {

			hRecPt[iSpecies][iCharge] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("hrecpt_sp%i_ch%i",iSpecies, iCharge),Form("hrecpt_sp%i_ch%i",iSpecies, iCharge),"F",fPtAxis);
			hRecPtEta[iSpecies][iCharge] = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("hrecpteta_sp%i_ch%i",iSpecies, iCharge),Form("hrecpteta_sp%i_ch%i",iSpecies, iCharge),"F",fPtAxis ,fEtaAxis);

			hRecSelTOFPt[iSpecies][iCharge] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("hrecselTOFpt_sp%i_ch%i",iSpecies, iCharge),Form("hrecselTOFpt_sp%i_ch%i",iSpecies, iCharge),"F",fPtAxis);
			hRecSelTOFPtEta[iSpecies][iCharge] = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("hrecselTOFpteta_sp%i_ch%i",iSpecies, iCharge),Form("hrecselTOFpteta_sp%i_ch%i",iSpecies, iCharge),"F",fPtAxis ,fEtaAxis);
			hRecSelTOFMatchPt[iSpecies][iCharge] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("hrecselTOFmatchpt_sp%i_ch%i",iSpecies, iCharge),Form("hrecselTOFmatchpt_sp%i_ch%i",iSpecies, iCharge),"F",fPtAxis);
			hRecSelTOFMatchPtEta[iSpecies][iCharge] = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("hrecselTOFmatchpteta_sp%i_ch%i",iSpecies, iCharge),Form("hrecselTOFmatchpteta_sp%i_ch%i",iSpecies, iCharge),"F",fPtAxis ,fEtaAxis);
			
			hRecSelTPCPt[iSpecies][iCharge] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("hrecselTPCpt_sp%i_ch%i",iSpecies, iCharge),Form("hrecselTPCpt_sp%i_ch%i",iSpecies, iCharge),"F",fPtAxis);
			hRecSelTPCPtEta[iSpecies][iCharge] = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("hrecselTPCpteta_sp%i_ch%i",iSpecies, iCharge),Form("hrecselTPCpteta_sp%i_ch%i",iSpecies, iCharge),"F",fPtAxis ,fEtaAxis);

            // Total PID Efficiency - Use this for analysis. 
			hRecSelPIDPt[iSpecies][iCharge] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("hrecselPIDpt_sp%i_ch%i",iSpecies, iCharge),Form("hrecselPIDpt_sp%i_ch%i",iSpecies, iCharge),"F",fPtAxis);
			hRecSelPIDPtEta[iSpecies][iCharge] = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("hrecselPIDpteta_sp%i_ch%i",iSpecies, iCharge),Form("hrecselPIDpteta_sp%i_ch%i",iSpecies, iCharge),"F",fPtAxis ,fEtaAxis);
			hRecSelPIDMatchPt[iSpecies][iCharge] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("hrecselPIDpt_sp%i_ch%i",iSpecies, iCharge),Form("hrecselPIDpt_sp%i_ch%i",iSpecies, iCharge),"F",fPtAxis);
			hRecSelPIDMatchPtEta[iSpecies][iCharge] = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("hrecselPIDpteta_sp%i_ch%i",iSpecies, iCharge),Form("hrecselPIDpteta_sp%i_ch%i",iSpecies, iCharge),"F",fPtAxis ,fEtaAxis);

			hGenPrimPt[iSpecies][iCharge] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("hgenprimpt_sp%i_ch%i",iSpecies, iCharge),Form("hgenprimpt_sp%i_ch%i",iSpecies, iCharge),"F",fPtAxis);
			hGenPrimPtEta[iSpecies][iCharge] = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("hgenprimpteta_sp%i_ch%i",iSpecies, iCharge),Form("hgenprimpteta_sp%i_ch%i",iSpecies, iCharge),"F",fPtAxis ,fEtaAxis);
			hGenSecPt[iSpecies][iCharge] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("hgensecpt_sp%i_ch%i",iSpecies, iCharge),Form("hgensecpt_sp%i_ch%i",iSpecies, iCharge),"F",fPtAxis);
			hGenSecPtEta[iSpecies][iCharge] = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("hgensecpteta_sp%i_ch%i",iSpecies, iCharge),Form("hgensecpteta_sp%i_ch%i",iSpecies, iCharge),"F",fPtAxis ,fEtaAxis);
		
			hRecPt[iSpecies][iCharge]->Sumw2();
			hRecPtEta[iSpecies][iCharge]->Sumw2();

			hRecSelTOFPt[iSpecies][iCharge]->Sumw2();
			hRecSelTOFPtEta[iSpecies][iCharge]->Sumw2();
			hRecSelTOFMatchPt[iSpecies][iCharge]->Sumw2();
			hRecSelTOFMatchPtEta[iSpecies][iCharge]->Sumw2();
			
			hRecSelTOFPt[iSpecies][iCharge]->Sumw2();
			hRecSelTOFPtEta[iSpecies][iCharge]->Sumw2();

			hRecSelPIDPt[iSpecies][iCharge]->Sumw2();
			hRecSelPIDPtEta[iSpecies][iCharge]->Sumw2();
			hRecSelPIDMatchPt[iSpecies][iCharge]->Sumw2();
			hRecSelPIDMatchPtEta[iSpecies][iCharge]->Sumw2();

			hGenPrimPt[iSpecies][iCharge]->Sumw2();
			hGenPrimPtEta[iSpecies][iCharge]->Sumw2();
			hGenSecPt[iSpecies][iCharge]->Sumw2();
			hGenSecPtEta[iSpecies][iCharge]->Sumw2();

		}
	}

	// Reset to the first file, first event.
	if (!NextFile(kTRUE)) {return;}
	AliMiniTrackDiHadronPID* currentTrack = 0x0;

	Int_t nEvents = 0;

	while (GetEvent()) {
		
		// Take events passing the centrality, vtxZ cuts.
		if (!IsSelected(fCurrentEvent)) {continue;}

		nEvents++;

		// Printout per 1000 events.
		if (nEvents % 1000 == 0) {
			cout << "Processing Event: " << nEvents << endl;
		}

		// Loop over tracks.
		for (Int_t iTrack = 0; iTrack < fCurrentEvent->GetNTracks(); ++iTrack) {

			currentTrack = fCurrentEvent->GetTrack(iTrack);
		
			// currentTrack->PrintTrack();

			// Pions.
			if (currentTrack->IsMCPion()) {
							
				if (currentTrack->IsPhysicalPrimary()) {
					
					// Generated primary.
					if (!isForSpectra) {hGenPrimPt[0][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
					hGenPrimPtEta[0][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());

					// Reconstructed.
					if (currentTrack->Pt() < -900.) {continue;} // Default value for RECpT -> not reconstructed.
					if (TMath::Abs(currentTrack->MCEta()) > 0.8) {continue;} // Eta cut.

					// if (TMath::Abs(currentTrack->MCEta()) > 0.8) {cout << "ERROR: Track has MCeta > 0.8! " << endl;}
					// if (TMath::Abs(currentTrack->Eta()) > 0.8) {cout << "ERROR: Track has RECeta > 0.8! " << endl;}

					// Check track filtermap against settings filtermask (i.e., check if the particle is reconstructed).
					if (fSettings->GetFilterMask()) {if (!(currentTrack->GetFilterMap() & fSettings->GetFilterMask())) {continue;}}

					hRecPt[0][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());
					hRecPtEta[0][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());

                    // Is Selected for TOF PID (only kTOFout, kTIME, no TPC requirement).
					if (fSettings->IsTrackSelectedForTOFPID(currentTrack)) {
						if (!isForSpectra) {hRecSelTOFPt[0][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
						hRecSelTOFPtEta[0][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());
                        
						// TOF matched (if flag is set in MC).
						if (fSettings->GetIsTOFMismatchInfoAvailableMC() && currentTrack->IsTOFMatch()) { 
							if (!isForSpectra) {hRecSelTOFMatchPt[0][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
							hRecSelTOFMatchPtEta[0][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());
						} 
					}

                    // Is Selected for TPC PID (only TPC dE/dx cluster cut).
					if (fSettings->IsTrackSelectedForTPCPID(currentTrack)) {
						if (!isForSpectra) {hRecSelTPCPt[0][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
						hRecSelTPCPtEta[0][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());
					}

					// Selected for PID.
					if (fSettings->IsTrackSelectedForPID(currentTrack)) {
						if (!isForSpectra) {hRecSelPIDPt[0][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
						hRecSelPIDPtEta[0][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());

						// TOF matched (if flag is set in MC).
						if (fSettings->GetIsTOFMismatchInfoAvailableMC() && currentTrack->IsTOFMatch()) { 
							if (!isForSpectra) {hRecSelPIDMatchPt[0][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
							hRecSelPIDMatchPtEta[0][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());
						} 
					}

				} else if (currentTrack->IsSecondaryFromWeakDecay() || currentTrack->IsSecondaryFromMaterialDecay()) {

					// Only reconstructed secondaries and in filtermask.
					if (currentTrack->Pt() < -900.) {continue;}
					if (TMath::Abs(currentTrack->MCEta()) > 0.8) {continue;} // Eta cut.

					if (fSettings->GetFilterMask()) {if (!(currentTrack->GetFilterMap() & fSettings->GetFilterMask())) {continue;}}

					// Generated secondarary.
					if (!isForSpectra) {hGenSecPt[0][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
					hGenSecPtEta[0][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());
				}
			}

			// Kaons.
			if (currentTrack->IsMCKaon()) {
							
				if (currentTrack->IsPhysicalPrimary()) {
					
					// Generated primary.
					if (!isForSpectra) {hGenPrimPt[1][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
					hGenPrimPtEta[1][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());

					// Reconstructed.
					if (currentTrack->Pt() < -900.) {continue;}
					if (TMath::Abs(currentTrack->MCEta()) > 0.8) {continue;} // Eta cut.

					// Check track filtermap against settings filtermask.
					if (fSettings->GetFilterMask()) {if (!(currentTrack->GetFilterMap() & fSettings->GetFilterMask())) {continue;}}

					hRecPt[1][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());
					hRecPtEta[1][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());

                    // Is Selected for TOF PID (only kTOFout, kTIME, no TPC requirement).
					if (fSettings->IsTrackSelectedForTOFPID(currentTrack)) {
						if (!isForSpectra) {hRecSelTOFPt[1][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
						hRecSelTOFPtEta[1][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());

						// TOF matched (if flag is set in MC).
						if (fSettings->GetIsTOFMismatchInfoAvailableMC() && currentTrack->IsTOFMatch()) { 
							if (!isForSpectra) {hRecSelTOFMatchPt[1][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
							hRecSelTOFMatchPtEta[1][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());
						} 
					}

                    // Is Selected for TPC PID (only TPC dE/dx cluster cut).
					if (fSettings->IsTrackSelectedForTPCPID(currentTrack)) {
						if (!isForSpectra) {hRecSelTPCPt[1][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
						hRecSelTPCPtEta[1][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());
					}

					// Selected for PID.
					if (fSettings->IsTrackSelectedForPID(currentTrack)) {
						if (!isForSpectra) {hRecSelPIDPt[1][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
						hRecSelPIDPtEta[1][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());

						// TOF matched (if flag is set in MC).
						if (fSettings->GetIsTOFMismatchInfoAvailableMC() && currentTrack->IsTOFMatch()) { 
							if (!isForSpectra) {hRecSelPIDMatchPt[1][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
							hRecSelPIDMatchPtEta[1][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());
						} 
					}

				} else if (currentTrack->IsSecondaryFromWeakDecay() || currentTrack->IsSecondaryFromMaterialDecay()) {

					// Only reconstructed secondaries and in filtermask.
					if (currentTrack->Pt() < -900.) {continue;}
					if (TMath::Abs(currentTrack->MCEta()) > 0.8) {continue;} // Eta cut.

					if (fSettings->GetFilterMask()) {if (!(currentTrack->GetFilterMap() & fSettings->GetFilterMask())) {continue;}}

					// Generated secondarary.
					if (!isForSpectra) {hGenSecPt[1][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
					hGenSecPtEta[1][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());
				}
			}

			// Protons.
			if (currentTrack->IsMCProton()) {
							
				if (currentTrack->IsPhysicalPrimary()) {
					
					// Generated primary.
					if (!isForSpectra) {hGenPrimPt[2][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
					hGenPrimPtEta[2][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());

					// Reconstructed.
					if (currentTrack->Pt() < -900.) {continue;}
					if (TMath::Abs(currentTrack->MCEta()) > 0.8) {continue;} // Eta cut.

					// Check track filtermap against settings filtermask.
					if (fSettings->GetFilterMask()) {if (!(currentTrack->GetFilterMap() & fSettings->GetFilterMask())) {continue;}}

					hRecPt[2][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());
					hRecPtEta[2][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());

                    // Is Selected for TOF PID (only kTOFout, kTIME, no TPC requirement).
					if (fSettings->IsTrackSelectedForTOFPID(currentTrack)) {
						if (!isForSpectra) {hRecSelTOFPt[2][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
						hRecSelTOFPtEta[2][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());

						// TOF matched (if flag is set in MC).
						if (fSettings->GetIsTOFMismatchInfoAvailableMC() && currentTrack->IsTOFMatch()) { 
							if (!isForSpectra) {hRecSelTOFMatchPt[2][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
							hRecSelTOFMatchPtEta[2][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());
						} 
					}

                    // Is Selected for TPC PID (only TPC dE/dx cluster cut).
					if (fSettings->IsTrackSelectedForTPCPID(currentTrack)) {
						if (!isForSpectra) {hRecSelTPCPt[2][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
						hRecSelTPCPtEta[2][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());
					}

					// Selected for PID.
					if (fSettings->IsTrackSelectedForPID(currentTrack)) {
						if (!isForSpectra) {hRecSelPIDPt[2][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
						hRecSelPIDPtEta[2][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());

						// TOF matched (if flag is set in MC).
						if (fSettings->GetIsTOFMismatchInfoAvailableMC() && currentTrack->IsTOFMatch()) { 
							if (!isForSpectra) {hRecSelPIDMatchPt[2][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
							hRecSelPIDMatchPtEta[2][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());
						} 
					}

				} else if (currentTrack->IsSecondaryFromWeakDecay() || currentTrack->IsSecondaryFromMaterialDecay()) {

					// Only reconstructed secondaries and in filtermask.
					if (currentTrack->Pt() < -900.) {continue;}
					if (TMath::Abs(currentTrack->MCEta()) > 0.8) {continue;} // Eta cut.

					if (fSettings->GetFilterMask()) {if (!(currentTrack->GetFilterMap() & fSettings->GetFilterMask())) {continue;}}

					// Generated secondarary.
					if (!isForSpectra) {hGenSecPt[2][currentTrack->IsPositive()]->Fill(currentTrack->MCPt());}
					hGenSecPtEta[2][currentTrack->IsPositive()]->Fill(currentTrack->MCPt(), currentTrack->MCEta());
				}
			}
		}
	}	

	fout->cd();	

	TObjArray* rawHistos = new TObjArray();
	rawHistos->SetOwner(kTRUE);
	rawHistos->SetName("RawHistos");

	// Save rawhistos to file. 
	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNMassAssumptions(); iSpecies++) {

		// Mask only needed for spectra efficiencies.
		TH2F* ptetamask = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("PtEtaMask_sp%i",iSpecies), Form("PtEtaMask_sp%i",iSpecies), "F", fPtAxis, fEtaAxis);
		AliMiscToolsDiHadronPID::MakePtEtaMask(ptetamask, iSpecies, 1000);

		// Print it.
		TCanvas* cvs = TCanvas::MakeDefCanvas();
		ptetamask->Draw();
		ptetamask->SetDrawOption("surf1");
		cvs->SaveAs(Form("PtEtaMask_%i.pdf",iSpecies));

		for (Int_t iCharge = 0; iCharge < nCharges; iCharge++) {
			
			// For spectra efficiencies, use the mask to create pt histograms.
			if (isForSpectra) {

				// hRecPtEta[iSpecies][iCharge]->Multiply(hRecPtEta[iSpecies][iCharge], ptetamask[iSpecies], 1, 1, "B");
				// hRecPt[iSpecies][iCharge] = (TH1F*)(hRecPtEta[iSpecies][iCharge]->ProjectionX(Form("hrecpt_sp%i_ch%i",iSpecies, iCharge),1, fEtaAxis->GetNbins()));
				// hRecSelPIDPtEta[iSpecies][iCharge]->Multiply(hRecSelPIDPtEta[iSpecies][iCharge], ptetamask[iSpecies], 1, 1, "B");
				// hRecSelPIDPt[iSpecies][iCharge] = (TH1F*)(hRecSelPIDPtEta[iSpecies][iCharge]->ProjectionX(Form("hrecselpt_sp%i_ch%i",iSpecies, iCharge),1, fEtaAxis->GetNbins()));
				// hRecSelTOFMatchPtEta[iSpecies][iCharge]->Multiply(hRecSelTOFMatchPtEta[iSpecies][iCharge], ptetamask[iSpecies], 1, 1, "B");
				// hRecSelTOFMatchPt[iSpecies][iCharge] = (TH1F*)(hRecSelTOFMatchPtEta[iSpecies][iCharge]->ProjectionX(Form("hrecselmatchpt_sp%i_ch%i",iSpecies, iCharge),1, fEtaAxis->GetNbins()));
				// hGenPrimPtEta[iSpecies][iCharge]->Multiply(hGenPrimPtEta[iSpecies][iCharge], ptetamask[iSpecies], 1, 1, "B");
				// hGenPrimPt[iSpecies][iCharge] = (TH1F*)(hGenPrimPtEta[iSpecies][iCharge]->ProjectionX(Form("hgenprimpt_sp%i_ch%i",iSpecies, iCharge),1, fEtaAxis->GetNbins()));
				// hGenSecPtEta[iSpecies][iCharge]->Multiply(hGenSecPtEta[iSpecies][iCharge], ptetamask[iSpecies], 1, 1, "B");
				// hGenSecPt[iSpecies][iCharge] = (TH1F*)(hGenSecPtEta[iSpecies][iCharge]->ProjectionX(Form("hgensecpt_sp%i_ch%i",iSpecies, iCharge),1, fEtaAxis->GetNbins()));

				for (Int_t iEtaBin = 1; iEtaBin <= fEtaAxis->GetNbins(); ++iEtaBin) {
					for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); ++iPtBin) {

						hRecPtEta[iSpecies][iCharge]->SetBinContent(iPtBin, iEtaBin, hRecPtEta[iSpecies][iCharge]->GetBinContent(iPtBin, iEtaBin) * ptetamask->GetBinContent(iPtBin, iEtaBin));
						hRecSelTPCPtEta[iSpecies][iCharge]->SetBinContent(iPtBin, iEtaBin, hRecSelTPCPtEta[iSpecies][iCharge]->GetBinContent(iPtBin, iEtaBin) * ptetamask->GetBinContent(iPtBin, iEtaBin));
						hRecSelTOFPtEta[iSpecies][iCharge]->SetBinContent(iPtBin, iEtaBin, hRecSelTOFPtEta[iSpecies][iCharge]->GetBinContent(iPtBin, iEtaBin) * ptetamask->GetBinContent(iPtBin, iEtaBin));
						hRecSelTOFMatchPtEta[iSpecies][iCharge]->SetBinContent(iPtBin, iEtaBin, hRecSelTOFMatchPtEta[iSpecies][iCharge]->GetBinContent(iPtBin, iEtaBin) * ptetamask->GetBinContent(iPtBin, iEtaBin));
						hRecSelPIDPtEta[iSpecies][iCharge]->SetBinContent(iPtBin, iEtaBin, hRecSelPIDPtEta[iSpecies][iCharge]->GetBinContent(iPtBin, iEtaBin) * ptetamask->GetBinContent(iPtBin, iEtaBin));
						hRecSelPIDMatchPtEta[iSpecies][iCharge]->SetBinContent(iPtBin, iEtaBin, hRecSelPIDMatchPtEta[iSpecies][iCharge]->GetBinContent(iPtBin, iEtaBin) * ptetamask->GetBinContent(iPtBin, iEtaBin));
						hGenPrimPtEta[iSpecies][iCharge]->SetBinContent(iPtBin, iEtaBin, hGenPrimPtEta[iSpecies][iCharge]->GetBinContent(iPtBin, iEtaBin) * ptetamask->GetBinContent(iPtBin, iEtaBin));
						hGenSecPtEta[iSpecies][iCharge]->SetBinContent(iPtBin, iEtaBin, hGenSecPtEta[iSpecies][iCharge]->GetBinContent(iPtBin, iEtaBin) * ptetamask->GetBinContent(iPtBin, iEtaBin));
					}
				}

				hRecPt[iSpecies][iCharge] = (TH1F*)(hRecPtEta[iSpecies][iCharge]->ProjectionX(Form("hrecpt_sp%i_ch%i",iSpecies, iCharge),1, fEtaAxis->GetNbins()));

				hRecSelTPCPt[iSpecies][iCharge] = (TH1F*)(hRecSelTPCPtEta[iSpecies][iCharge]->ProjectionX(Form("hrecselTPCpt_sp%i_ch%i",iSpecies, iCharge),1, fEtaAxis->GetNbins()));

				hRecSelTOFPt[iSpecies][iCharge] = (TH1F*)(hRecSelTOFPtEta[iSpecies][iCharge]->ProjectionX(Form("hrecselTOFpt_sp%i_ch%i",iSpecies, iCharge),1, fEtaAxis->GetNbins()));
				hRecSelTOFMatchPt[iSpecies][iCharge] = (TH1F*)(hRecSelTOFMatchPtEta[iSpecies][iCharge]->ProjectionX(Form("hrecselTOFmatchpt_sp%i_ch%i",iSpecies, iCharge),1, fEtaAxis->GetNbins()));

				hRecSelPIDPt[iSpecies][iCharge] = (TH1F*)(hRecSelPIDPtEta[iSpecies][iCharge]->ProjectionX(Form("hrecselPIDpt_sp%i_ch%i",iSpecies, iCharge),1, fEtaAxis->GetNbins()));
				hRecSelPIDMatchPt[iSpecies][iCharge] = (TH1F*)(hRecSelPIDMatchPtEta[iSpecies][iCharge]->ProjectionX(Form("hrecselPIDmatchpt_sp%i_ch%i",iSpecies, iCharge),1, fEtaAxis->GetNbins()));

				hGenPrimPt[iSpecies][iCharge] = (TH1F*)(hGenPrimPtEta[iSpecies][iCharge]->ProjectionX(Form("hgenprimpt_sp%i_ch%i",iSpecies, iCharge),1, fEtaAxis->GetNbins()));
				hGenSecPt[iSpecies][iCharge] = (TH1F*)(hGenSecPtEta[iSpecies][iCharge]->ProjectionX(Form("hgensecpt_sp%i_ch%i",iSpecies, iCharge),1, fEtaAxis->GetNbins()));

			}

			rawHistos->Add(hRecPt[iSpecies][iCharge]);
			rawHistos->Add(hRecPtEta[iSpecies][iCharge]);

			rawHistos->Add(hRecSelTPCPt[iSpecies][iCharge]);
			rawHistos->Add(hRecSelTPCPtEta[iSpecies][iCharge]);

			rawHistos->Add(hRecSelTOFPt[iSpecies][iCharge]);
			rawHistos->Add(hRecSelTOFPtEta[iSpecies][iCharge]);
			rawHistos->Add(hRecSelTOFMatchPt[iSpecies][iCharge]);
			rawHistos->Add(hRecSelTOFMatchPtEta[iSpecies][iCharge]);

			rawHistos->Add(hRecSelPIDPt[iSpecies][iCharge]);
			rawHistos->Add(hRecSelPIDPtEta[iSpecies][iCharge]);
			rawHistos->Add(hRecSelPIDMatchPt[iSpecies][iCharge]);
			rawHistos->Add(hRecSelPIDMatchPtEta[iSpecies][iCharge]);

			rawHistos->Add(hGenPrimPt[iSpecies][iCharge]);
			rawHistos->Add(hGenPrimPtEta[iSpecies][iCharge]);
			rawHistos->Add(hGenSecPt[iSpecies][iCharge]);
			rawHistos->Add(hGenSecPtEta[iSpecies][iCharge]);

		}

		delete ptetamask;
		ptetamask = 0x0;
	}
	rawHistos->Write(0,TObject::kSingleKey);

	// Compute efficiencies and save to file. 
	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNMassAssumptions(); iSpecies++) {
		for (Int_t iCharge = 0; iCharge < nCharges; iCharge++) {

			// Tracking.
			TH1F* trackingpt = (TH1F*)hRecPt[iSpecies][iCharge]->Clone(Form("trackingpt_sp%i_ch%i",iSpecies, iCharge));
			trackingpt->SetTitle(Form("trackingpt_sp%i_ch%i",iSpecies, iCharge));
			trackingpt->Divide(trackingpt, hGenPrimPt[iSpecies][iCharge], 1, 1, "B");
			if (fSettings->GetBeamType() != 1) AliMiscToolsDiHadronPID::ApplyGeantFlukaCorrection(trackingpt, iSpecies, iCharge, kTRUE);
			trackingpt->Write();

			if (!isForSpectra) {
				TH2F* trackingpteta = (TH2F*)hRecPtEta[iSpecies][iCharge]->Clone(Form("trackingpteta_sp%i_ch%i",iSpecies, iCharge));
				trackingpteta->SetTitle(Form("trackingpteta_sp%i_ch%i",iSpecies, iCharge));
				trackingpteta->Divide(trackingpteta, hGenPrimPtEta[iSpecies][iCharge], 1, 1, "B");
				if (fSettings->GetBeamType() != 1) AliMiscToolsDiHadronPID::ApplyGeantFlukaCorrection(trackingpteta, iSpecies, iCharge, kTRUE);
				trackingpteta->Write();
			}

			// TOF selection - Only meant as a comparison with TOF analysis!
			TH1F* tofcutpt = (TH1F*)hRecSelTOFPt[iSpecies][iCharge]->Clone(Form("pidcutTOFpt_sp%i_ch%i",iSpecies, iCharge));
			tofcutpt->SetTitle(Form("tofcutTOFpt_sp%i_ch%i",iSpecies, iCharge));
			tofcutpt->Divide(tofcutpt, hRecPt[iSpecies][iCharge], 1, 1, "B");
			tofcutpt->Write();

			if (!isForSpectra) {
				TH2F* tofcutpteta = (TH2F*)hRecSelTOFPtEta[iSpecies][iCharge]->Clone(Form("tofcutTOFpteta_sp%i_ch%i",iSpecies, iCharge));
				tofcutpteta->SetTitle(Form("tofcutTOFpteta_sp%i_ch%i",iSpecies, iCharge));
				tofcutpteta->Divide(tofcutpteta, hRecPtEta[iSpecies][iCharge], 1, 1, "B");
				tofcutpteta->Write();			
			}

			// TPC selection - Only meant as a comparison with TPC analysis!
			TH1F* tpccutpt = (TH1F*)hRecSelTPCPt[iSpecies][iCharge]->Clone(Form("pidcutTPCpt_sp%i_ch%i",iSpecies, iCharge));
			tpccutpt->SetTitle(Form("tpccutTPCpt_sp%i_ch%i",iSpecies, iCharge));
			tpccutpt->Divide(tpccutpt, hRecPt[iSpecies][iCharge], 1, 1, "B");
			tpccutpt->Write();

			if (!isForSpectra) {
				TH2F* tpccutpteta = (TH2F*)hRecSelTPCPtEta[iSpecies][iCharge]->Clone(Form("tpccutTPCpteta_sp%i_ch%i",iSpecies, iCharge));
				tpccutpteta->SetTitle(Form("tpccutTPCpteta_sp%i_ch%i",iSpecies, iCharge));
				tpccutpteta->Divide(tpccutpteta, hRecPtEta[iSpecies][iCharge], 1, 1, "B");
				tpccutpteta->Write();			
			}

			// PID selection.
			TH1F* pidcutpt = (TH1F*)hRecSelPIDPt[iSpecies][iCharge]->Clone(Form("pidcutpt_sp%i_ch%i",iSpecies, iCharge));
			pidcutpt->SetTitle(Form("pidcutpt_sp%i_ch%i",iSpecies, iCharge));
			pidcutpt->Divide(pidcutpt, hRecPt[iSpecies][iCharge], 1, 1, "B");
			pidcutpt->Write();

			if (!isForSpectra) {
				TH2F* pidcutpteta = (TH2F*)hRecSelPIDPtEta[iSpecies][iCharge]->Clone(Form("pidcutpteta_sp%i_ch%i",iSpecies, iCharge));
				pidcutpteta->SetTitle(Form("pidcutpteta_sp%i_ch%i",iSpecies, iCharge));
				pidcutpteta->Divide(pidcutpteta, hRecPtEta[iSpecies][iCharge], 1, 1, "B");
				pidcutpteta->Write();			
			}

			// Primaries.
			TH1F* primfracpt = (TH1F*)hRecPt[iSpecies][iCharge]->Clone(Form("primfracpt_sp%i_ch%i",iSpecies, iCharge));
			primfracpt->SetTitle(Form("primfracpt_sp%i_ch%i",iSpecies, iCharge));
			TH1F* histgentotalpt = (TH1F*)hRecPt[iSpecies][iCharge]->Clone(Form("hprimtotalpt_sp%i_ch%i",iSpecies, iCharge));
			histgentotalpt->Add(hGenSecPt[iSpecies][iCharge]);
			primfracpt->Divide(primfracpt, histgentotalpt, 1, 1, "B");
			primfracpt->Write();

			if (!isForSpectra) {
				TH2F* primfracpteta = (TH2F*)hRecPtEta[iSpecies][iCharge]->Clone(Form("primfracpteta_sp%i_ch%i",iSpecies, iCharge));
				primfracpteta->SetTitle(Form("primfracpteta_sp%i_ch%i",iSpecies, iCharge));
				TH2F* histgentotalpteta = (TH2F*)hRecPtEta[iSpecies][iCharge]->Clone(Form("hprimtotalpteta_sp%i_ch%i",iSpecies, iCharge));
				histgentotalpteta->Add(hGenSecPtEta[iSpecies][iCharge]);
				primfracpteta->Divide(primfracpteta, histgentotalpteta, 1, 1, "B");
				primfracpteta->Write();
			}

			// Matching (IGNORE TPC).
			if (!(fSettings->GetIsTOFMismatchInfoAvailableMC())) {continue;}
			TH1F* matchingTOFpt = (TH1F*)hRecSelTOFMatchPt[iSpecies][iCharge]->Clone(Form("matchingTOFpt_sp%i_ch%i",iSpecies, iCharge));
			matchingTOFpt->SetTitle(Form("matchingTOFpt_sp%i_ch%i",iSpecies, iCharge));
			matchingTOFpt->Divide(matchingTOFpt, hRecSelTOFPt[iSpecies][iCharge], 1, 1, "B");
			if (fSettings->GetBeamType() != 1) AliMiscToolsDiHadronPID::ApplyGeantFlukaCorrection(matchingTOFpt, iSpecies, iCharge, kFALSE);
			matchingTOFpt->Write();		

			if (!isForSpectra) {
				TH2F* matchingTOFpteta = (TH2F*)hRecSelTOFMatchPtEta[iSpecies][iCharge]->Clone(Form("matchingTOFpteta_sp%i_ch%i",iSpecies, iCharge));
				matchingTOFpteta->SetTitle(Form("matchingTOFpteta_sp%i_ch%i",iSpecies, iCharge));
				matchingTOFpteta->Divide(matchingTOFpteta, hRecSelTOFPtEta[iSpecies][iCharge], 1, 1, "B");
				if (fSettings->GetBeamType() != 1) AliMiscToolsDiHadronPID::ApplyGeantFlukaCorrection(matchingTOFpteta, iSpecies, iCharge, kFALSE);
				matchingTOFpteta->Write();		
			}

			// "Good Match" = PID x Match (IGNORE TPC).
			if (!(fSettings->GetIsTOFMismatchInfoAvailableMC())) {continue;}
			TH1F* goodmatchingTOFpt = (TH1F*)hRecSelTOFMatchPt[iSpecies][iCharge]->Clone(Form("goodmatchingTOFpt_sp%i_ch%i",iSpecies, iCharge));
			goodmatchingTOFpt->SetTitle(Form("goodmatchingTOFpt_sp%i_ch%i",iSpecies, iCharge));
			goodmatchingTOFpt->Divide(goodmatchingTOFpt, hRecPt[iSpecies][iCharge], 1, 1, "B");
			if (fSettings->GetBeamType() != 1) AliMiscToolsDiHadronPID::ApplyGeantFlukaCorrection(goodmatchingTOFpt, iSpecies, iCharge, kFALSE);
			goodmatchingTOFpt->Write();		

			if (!isForSpectra) {
				TH2F* goodmatchingTOFpteta = (TH2F*)hRecSelTOFMatchPtEta[iSpecies][iCharge]->Clone(Form("goodmatchingTOFpteta_sp%i_ch%i",iSpecies, iCharge));
				goodmatchingTOFpteta->SetTitle(Form("goodmatchingTOFpteta_sp%i_ch%i",iSpecies, iCharge));
				goodmatchingTOFpteta->Divide(goodmatchingTOFpteta, hRecPtEta[iSpecies][iCharge], 1, 1, "B");
				if (fSettings->GetBeamType() != 1) AliMiscToolsDiHadronPID::ApplyGeantFlukaCorrection(goodmatchingTOFpteta, iSpecies, iCharge, kFALSE);
				goodmatchingTOFpteta->Write();		
			}		

			// Matching (INCLUDE TPC).
			if (!(fSettings->GetIsTOFMismatchInfoAvailableMC())) {continue;}
			TH1F* matchingpt = (TH1F*)hRecSelPIDMatchPt[iSpecies][iCharge]->Clone(Form("matchingpt_sp%i_ch%i",iSpecies, iCharge));
			matchingpt->SetTitle(Form("matchingpt_sp%i_ch%i",iSpecies, iCharge));
			matchingpt->Divide(matchingpt, hRecSelPIDPt[iSpecies][iCharge], 1, 1, "B");
			if (fSettings->GetBeamType() != 1) AliMiscToolsDiHadronPID::ApplyGeantFlukaCorrection(matchingpt, iSpecies, iCharge, kFALSE);
			matchingpt->Write();		

			if (!isForSpectra) {
				TH2F* matchingpteta = (TH2F*)hRecSelPIDMatchPtEta[iSpecies][iCharge]->Clone(Form("matchingpteta_sp%i_ch%i",iSpecies, iCharge));
				matchingpteta->SetTitle(Form("matchingpteta_sp%i_ch%i",iSpecies, iCharge));
				matchingpteta->Divide(matchingpteta, hRecSelPIDPtEta[iSpecies][iCharge], 1, 1, "B");
				if (fSettings->GetBeamType() != 1) AliMiscToolsDiHadronPID::ApplyGeantFlukaCorrection(matchingpteta, iSpecies, iCharge, kFALSE);
				matchingpteta->Write();		
			}

			// "Good Match" = PID x Match (INCLUDE TPC).
			if (!(fSettings->GetIsTOFMismatchInfoAvailableMC())) {continue;}
			TH1F* goodmatchingpt = (TH1F*)hRecSelPIDMatchPt[iSpecies][iCharge]->Clone(Form("goodmatchingpt_sp%i_ch%i",iSpecies, iCharge));
			goodmatchingpt->SetTitle(Form("goodmatchingpt_sp%i_ch%i",iSpecies, iCharge));
			goodmatchingpt->Divide(goodmatchingpt, hRecPt[iSpecies][iCharge], 1, 1, "B");
			if (fSettings->GetBeamType() != 1) AliMiscToolsDiHadronPID::ApplyGeantFlukaCorrection(goodmatchingpt, iSpecies, iCharge, kFALSE);
			goodmatchingpt->Write();		

			if (!isForSpectra) {
				TH2F* goodmatchingpteta = (TH2F*)hRecSelPIDMatchPtEta[iSpecies][iCharge]->Clone(Form("goodmatchingpteta_sp%i_ch%i",iSpecies, iCharge));
				goodmatchingpteta->SetTitle(Form("goodmatchingpteta_sp%i_ch%i",iSpecies, iCharge));
				goodmatchingpteta->Divide(goodmatchingpteta, hRecPtEta[iSpecies][iCharge], 1, 1, "B");
				if (fSettings->GetBeamType() != 1) AliMiscToolsDiHadronPID::ApplyGeantFlukaCorrection(goodmatchingpteta, iSpecies, iCharge, kFALSE);
				goodmatchingpteta->Write();		
			}		

			// Total Efficiencies, exclude matching for pp.
			TH1F* totalpt;
			if (!(fSettings->GetBeamType() == 2)) {
				totalpt = (TH1F*)hRecSelPIDMatchPt[iSpecies][iCharge]->Clone(Form("totalpt_sp%i_ch%i",iSpecies, iCharge));
			} else {
				totalpt = (TH1F*)hRecSelPIDPt[iSpecies][iCharge]->Clone(Form("totalpt_sp%i_ch%i",iSpecies, iCharge));
			}
			totalpt->SetTitle(Form("totalpt_sp%i_ch%i",iSpecies, iCharge));
			totalpt->Divide(totalpt, hGenPrimPt[iSpecies][iCharge], 1, 1, "B");
			if (fSettings->GetBeamType() != 1) AliMiscToolsDiHadronPID::ApplyGeantFlukaCorrection(totalpt, iSpecies, iCharge, kTRUE);
			if (fSettings->GetBeamType() != 1) AliMiscToolsDiHadronPID::ApplyGeantFlukaCorrection(totalpt, iSpecies, iCharge, kFALSE);
			totalpt->Write();

			if (!isForSpectra) {
				TH2F* totalpteta;
				if (!(fSettings->GetBeamType() == 2)) {
					totalpteta = (TH2F*)hRecSelTOFMatchPtEta[iSpecies][iCharge]->Clone(Form("totalpteta_sp%i_ch%i",iSpecies, iCharge));
				} else {
					totalpteta = (TH2F*)hRecSelPIDPtEta[iSpecies][iCharge]->Clone(Form("totalpteta_sp%i_ch%i",iSpecies, iCharge));
				}
				totalpteta->SetTitle(Form("totalpteta_sp%i_ch%i",iSpecies, iCharge));
				totalpteta->Divide(totalpteta, hGenPrimPtEta[iSpecies][iCharge], 1, 1, "B");
				if (fSettings->GetBeamType() != 1) AliMiscToolsDiHadronPID::ApplyGeantFlukaCorrection(totalpteta, iSpecies, iCharge, kTRUE);
				if (fSettings->GetBeamType() != 1) AliMiscToolsDiHadronPID::ApplyGeantFlukaCorrection(totalpteta, iSpecies, iCharge, kFALSE);
				totalpteta->Write();
			}

		}
	}

	fout->Close();

	// Stop the timer.
	cout << "Analysis time: " << AliMiscToolsDiHadronPID::FormatTime(sw.RealTime()).Data() << endl;
	cout << "Processed events: " << nEvents << endl;
	
}

// -----------------------------------------------------------------------
void AliProcessDataDiHadronPIDMini::MakeDCATemplates(const char* fileName) {

	// Open Spectra, check if MC or data, loop over tracks, fill 1 or 3 DCA histograms, write to file.

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	if (!fInputFileList) {cout << __func__ << " -> No input file list available..." << endl; return;}
	if (!(fInputFileList->GetEntriesFast())) {cout << __func__ << " -> Input file list is empty..." << endl; return;}

	if (!fSettings->GetDCAAxis()) {cout << __func__ << " -> DCA axis isn't set in config file..." << endl; return;}

	// Start the timer.
	TStopwatch sw;
	sw.Start();

	// Check if MC.
	if (!fSettings->GetIsMC()) {
		cout << __func__ << " -> Data found." << endl;
	} else {
		cout << __func__ << " -> MC found." << endl;
	}

	// Get open DCA filtermask from config file.
	if (!fSettings->GetFilterMaskForDCATemplates()) {
		cout << __func__ << " -> No Filter Mask in settings for open DCA_{xy}..." << endl; return;
	}

	const Double_t maxSigma = 1.0; // TODO: put in settings.
	const Int_t nCharges = 2;

	// DCA histos.
	TH2F*		histDCA_Data[fSettings->GetNMassAssumptions()][nCharges];
	TH2F*		histDCA_MCprim[fSettings->GetNMassAssumptions()][nCharges];
	TH2F*		histDCA_MCweak[fSettings->GetNMassAssumptions()][nCharges];
	TH2F*		histDCA_MCmat[fSettings->GetNMassAssumptions()][nCharges];

	// These histograms indicate what percentage of particles in the template survives the
	// tighter DCA cut:
	TH1F*		histTightDCA_data[fSettings->GetNMassAssumptions()][nCharges];
	TH1F*		histTightDCA_MCprim[fSettings->GetNMassAssumptions()][nCharges];
	TH1F*		histTightDCA_MCweak[fSettings->GetNMassAssumptions()][nCharges];
	TH1F*		histTightDCA_MCmat[fSettings->GetNMassAssumptions()][nCharges];

	TH1F*		histLooseDCA_data[fSettings->GetNMassAssumptions()][nCharges];
	TH1F*		histLooseDCA_MCprim[fSettings->GetNMassAssumptions()][nCharges];
	TH1F*		histLooseDCA_MCweak[fSettings->GetNMassAssumptions()][nCharges];
	TH1F*		histLooseDCA_MCmat[fSettings->GetNMassAssumptions()][nCharges];

	TH1F*		histPctSurvivedDCACut_data[fSettings->GetNMassAssumptions()][nCharges];
	TH1F*		histPctSurvivedDCACut_MCprim[fSettings->GetNMassAssumptions()][nCharges];
	TH1F*		histPctSurvivedDCACut_MCweak[fSettings->GetNMassAssumptions()][nCharges];
	TH1F*		histPctSurvivedDCACut_MCmat[fSettings->GetNMassAssumptions()][nCharges];
	
	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNMassAssumptions(); iSpecies++) {		
		for (Int_t iCharge = 0; iCharge < nCharges; iCharge++) {

			histDCA_Data[iSpecies][iCharge] = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("histdcadata_sp%i_ch%i",iSpecies, iCharge),Form("histdcadata_sp%i_ch%i",iSpecies, iCharge), "F", fPtAxis, fSettings->GetDCAAxis());
			histDCA_MCprim[iSpecies][iCharge] = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("histdcamcprim_sp%i_ch%i",iSpecies, iCharge),Form("histdcamcprim_sp%i_ch%i",iSpecies, iCharge), "F", fPtAxis, fSettings->GetDCAAxis());
			histDCA_MCweak[iSpecies][iCharge] = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("histdcamcweak_sp%i_ch%i",iSpecies, iCharge),Form("histdcamcweak_sp%i_ch%i",iSpecies, iCharge), "F", fPtAxis, fSettings->GetDCAAxis());
			histDCA_MCmat[iSpecies][iCharge] = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("histdcamcmat_sp%i_ch%i",iSpecies, iCharge),Form("histdcamcmat_sp%i_ch%i",iSpecies, iCharge), "F", fPtAxis, fSettings->GetDCAAxis());

			histTightDCA_data[iSpecies][iCharge] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("histTightDCA_data_sp%i_ch%i",iSpecies, iCharge),Form("histTightDCA_data_sp%i_ch%i",iSpecies, iCharge), "F", fPtAxis);
			histTightDCA_MCprim[iSpecies][iCharge] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("histTightDCA_MCprim_sp%i_ch%i",iSpecies, iCharge),Form("histTightDCA_MCprim_sp%i_ch%i",iSpecies, iCharge), "F", fPtAxis);
			histTightDCA_MCweak[iSpecies][iCharge] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("histTightDCA_MCweak_sp%i_ch%i",iSpecies, iCharge),Form("histTightDCA_MCweak_sp%i_ch%i",iSpecies, iCharge), "F", fPtAxis);
			histTightDCA_MCmat[iSpecies][iCharge] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("histTightDCA_MCmat_sp%i_ch%i",iSpecies, iCharge),Form("histTightDCA_MCmat_sp%i_ch%i",iSpecies, iCharge), "F", fPtAxis);

			histLooseDCA_data[iSpecies][iCharge] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("histLooseDCA_data_sp%i_ch%i",iSpecies, iCharge),Form("histLooseDCA_data_sp%i_ch%i",iSpecies, iCharge), "F", fPtAxis);
			histLooseDCA_MCprim[iSpecies][iCharge] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("histLooseDCA_MCprim_sp%i_ch%i",iSpecies, iCharge),Form("histLooseDCA_MCprim_sp%i_ch%i",iSpecies, iCharge), "F", fPtAxis);
			histLooseDCA_MCweak[iSpecies][iCharge] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("histLooseDCA_MCweak_sp%i_ch%i",iSpecies, iCharge),Form("histLooseDCA_MCweak_sp%i_ch%i",iSpecies, iCharge), "F", fPtAxis);
			histLooseDCA_MCmat[iSpecies][iCharge] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("histLooseDCA_MCmat_sp%i_ch%i",iSpecies, iCharge),Form("histLooseDCA_MCmat_sp%i_ch%i",iSpecies, iCharge), "F", fPtAxis);

			// histPctSurvivedDCACut_data[iSpecies][iCharge] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("histPctSurvivedDCACut_data_sp%i_ch%i",iSpecies, iCharge),Form("histPctSurvivedDCACut_data_sp%i_ch%i",iSpecies, iCharge), "F", fPtAxis);
			// histPctSurvivedDCACut_MCprim[iSpecies][iCharge] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("histPctSurvivedDCACut_MCprim_sp%i_ch%i",iSpecies, iCharge),Form("histPctSurvivedDCACut_MCprim_sp%i_ch%i",iSpecies, iCharge), "F", fPtAxis);
			// histPctSurvivedDCACut_MCweak[iSpecies][iCharge] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("histPctSurvivedDCACut_MCweak_sp%i_ch%i",iSpecies, iCharge),Form("histPctSurvivedDCACut_MCweak_sp%i_ch%i",iSpecies, iCharge), "F", fPtAxis);
			// histPctSurvivedDCACut_MCmat[iSpecies][iCharge] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("histPctSurvivedDCACut_MCmat_sp%i_ch%i",iSpecies, iCharge),Form("histPctSurvivedDCACut_MCmat_sp%i_ch%i",iSpecies, iCharge), "F", fPtAxis);

			// histdcadata->Scale(1.,"width");
			// histdcamcprim->Scale(1.,"width");
			// histdcamcweak->Scale(1.,"width");
			// histdcamcmat->Scale(1.,"width");

		}
	}

	// Reset to the first file, first event.
	if (!NextFile(kTRUE)) {return;}
	AliMiniTrackDiHadronPID* currentTrack = 0x0;

	Int_t nEvents = 0;

	while (GetEvent()) {
		
		// Take events passing the centrality, and vtxz cut.
		if (!IsSelected(fCurrentEvent)) {continue;}

		nEvents++;

		// Printout per 1000 events.
		if (nEvents % 1000 == 0) {
			cout << "Processing Event: " << nEvents << endl;
		}

		// Loop over tracks.
		for (Int_t iTrack = 0; iTrack < fCurrentEvent->GetNTracks(); ++iTrack) {

			currentTrack = fCurrentEvent->GetTrack(iTrack);

			// Check PID selection.
			if (!(fSettings->IsTrackSelectedForPID(currentTrack))) {continue;}

			// Check track filtermap against DCA filtermask (Loose DCA).
			if (currentTrack->GetFilterMap() & fSettings->GetFilterMaskForDCATemplates()) {

				// MC.
				if (fSettings->GetIsMC()) {

					if (currentTrack->IsPhysicalPrimary()) {

						if (currentTrack->IsMCPion()) {
							histDCA_MCprim[0][currentTrack->IsPositive()]->Fill(currentTrack->Pt(), currentTrack->GetDCAxy());
							histLooseDCA_MCprim[0][currentTrack->IsPositive()]->Fill(currentTrack->Pt());
						} 
						if (currentTrack->IsMCKaon()) {
							histDCA_MCprim[1][currentTrack->IsPositive()]->Fill(currentTrack->Pt(), currentTrack->GetDCAxy());
							histLooseDCA_MCprim[1][currentTrack->IsPositive()]->Fill(currentTrack->Pt());
						} 
						if (currentTrack->IsMCProton()) {
							histDCA_MCprim[2][currentTrack->IsPositive()]->Fill(currentTrack->Pt(), currentTrack->GetDCAxy());
							histLooseDCA_MCprim[2][currentTrack->IsPositive()]->Fill(currentTrack->Pt());
						} 

					}

					if (currentTrack->IsSecondaryFromWeakDecay()) {
						
						if (currentTrack->IsMCPion()) {
							histDCA_MCweak[0][currentTrack->IsPositive()]->Fill(currentTrack->Pt(), currentTrack->GetDCAxy());
							histLooseDCA_MCweak[0][currentTrack->IsPositive()]->Fill(currentTrack->Pt());
						}
						if (currentTrack->IsMCKaon()) {
							histDCA_MCweak[1][currentTrack->IsPositive()]->Fill(currentTrack->Pt(), currentTrack->GetDCAxy());
							histLooseDCA_MCweak[1][currentTrack->IsPositive()]->Fill(currentTrack->Pt());

						}
						if (currentTrack->IsMCProton()) {
							histDCA_MCweak[2][currentTrack->IsPositive()]->Fill(currentTrack->Pt(), currentTrack->GetDCAxy());
							histLooseDCA_MCweak[2][currentTrack->IsPositive()]->Fill(currentTrack->Pt());
						} 

					}

					if (currentTrack->IsSecondaryFromMaterialDecay()) {

						if (currentTrack->IsMCPion()) {
							histDCA_MCmat[0][currentTrack->IsPositive()]->Fill(currentTrack->Pt(), currentTrack->GetDCAxy());
							histLooseDCA_MCmat[0][currentTrack->IsPositive()]->Fill(currentTrack->Pt());							
						} 
						if (currentTrack->IsMCKaon()) {
							histDCA_MCmat[1][currentTrack->IsPositive()]->Fill(currentTrack->Pt(), currentTrack->GetDCAxy());
							histLooseDCA_MCmat[1][currentTrack->IsPositive()]->Fill(currentTrack->Pt());
						} 
						if (currentTrack->IsMCProton()) {
							histDCA_MCmat[2][currentTrack->IsPositive()]->Fill(currentTrack->Pt(), currentTrack->GetDCAxy());
							histLooseDCA_MCmat[2][currentTrack->IsPositive()]->Fill(currentTrack->Pt());
						} 

					}

				}

				// Data, take pure sample by getting one sigma in TPC and TOF and fill each species.
				else {

					for (Int_t iSpecies = 0; iSpecies < fSettings->GetNMassAssumptions(); iSpecies++) {	

						if (TMath::Sqrt( currentTrack->GetTOFnSigma(iSpecies) * currentTrack->GetTOFnSigma(iSpecies) + currentTrack->GetTPCnSigma(iSpecies) * currentTrack->GetTPCnSigma(iSpecies)) < maxSigma ) {
							histDCA_Data[iSpecies][currentTrack->IsPositive()]->Fill(currentTrack->Pt(), currentTrack->GetDCAxy());
							histLooseDCA_data[iSpecies][currentTrack->IsPositive()]->Fill(currentTrack->Pt());
						}
					}
				}
			} 

			// Check track filtermap agains DCA filtermask (Tight DCA / Regular Track Cuts).
			if (currentTrack->GetFilterMap() & fSettings->GetFilterMask()) { 
			
				// MC.
				if (fSettings->GetIsMC()) {

					if (currentTrack->IsPhysicalPrimary()) {

						if (currentTrack->IsMCPion()) {histTightDCA_MCprim[0][currentTrack->IsPositive()]->Fill(currentTrack->Pt());}
						if (currentTrack->IsMCKaon()) {histTightDCA_MCprim[1][currentTrack->IsPositive()]->Fill(currentTrack->Pt());}
						if (currentTrack->IsMCProton()) {histTightDCA_MCprim[2][currentTrack->IsPositive()]->Fill(currentTrack->Pt());}

					}

					if (currentTrack->IsSecondaryFromWeakDecay()) {
						
						if (currentTrack->IsMCPion()) {histTightDCA_MCweak[0][currentTrack->IsPositive()]->Fill(currentTrack->Pt());}
						if (currentTrack->IsMCKaon()) {histTightDCA_MCweak[1][currentTrack->IsPositive()]->Fill(currentTrack->Pt());}
						if (currentTrack->IsMCProton()) {histTightDCA_MCweak[2][currentTrack->IsPositive()]->Fill(currentTrack->Pt());}

					}

					if (currentTrack->IsSecondaryFromMaterialDecay()) {

						if (currentTrack->IsMCPion()) {histTightDCA_MCmat[0][currentTrack->IsPositive()]->Fill(currentTrack->Pt());							}
						if (currentTrack->IsMCKaon()) {histTightDCA_MCmat[1][currentTrack->IsPositive()]->Fill(currentTrack->Pt());}
						if (currentTrack->IsMCProton()) {histTightDCA_MCmat[2][currentTrack->IsPositive()]->Fill(currentTrack->Pt());}

					}

				}

				// Data, take pure sample by getting one sigma in TPC and TOF and fill each species.
				else {

					for (Int_t iSpecies = 0; iSpecies < fSettings->GetNMassAssumptions(); iSpecies++) {	

						if (TMath::Sqrt( currentTrack->GetTOFnSigma(iSpecies) * currentTrack->GetTOFnSigma(iSpecies) + currentTrack->GetTPCnSigma(iSpecies) * currentTrack->GetTPCnSigma(iSpecies)) < maxSigma ) {
							histTightDCA_data[iSpecies][currentTrack->IsPositive()]->Fill(currentTrack->Pt());
						}
					}
				}
			}
		}
	}

	// Determine fractions of particles that are removed.
	TFile* fout = new TFile(fileName, "recreate");

	// fout->mkdir("SpectraNormalCuts");
	// fout->mkdir("SpectraDCATemplateCuts");
	// fout->mkdir("PctSurvivingDCACut");

	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNMassAssumptions(); iSpecies++) {		
		for (Int_t iCharge = 0; iCharge < nCharges; iCharge++) {
			if (fSettings->GetIsMC()) {

				// fout->cd();
				histDCA_MCprim[iSpecies][iCharge]->Write();
				delete histDCA_MCprim[iSpecies][iCharge]; histDCA_MCprim[iSpecies][iCharge] = 0x0;
				histDCA_MCweak[iSpecies][iCharge]->Write();
				delete histDCA_MCweak[iSpecies][iCharge]; histDCA_MCweak[iSpecies][iCharge] = 0x0;
				histDCA_MCmat[iSpecies][iCharge]->Write();
				delete histDCA_MCmat[iSpecies][iCharge]; histDCA_MCmat[iSpecies][iCharge] = 0x0;

				// fout->cd("PctSurvivingDCACut");
				// for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {
					// histPctSurvivedDCACut_MCprim[iSpecies][iCharge]->SetBinContent(iPtBin, (histLooseDCA_MCprim[iSpecies][iCharge]->GetBinContent(iPtBin) > 0. ? histTightDCA_MCprim[iSpecies][iCharge]->GetBinContent(iPtBin) / histLooseDCA_MCprim[iSpecies][iCharge]->GetBinContent(iPtBin) : -1));
					// histPctSurvivedDCACut_MCweak[iSpecies][iCharge]->SetBinContent(iPtBin, (histLooseDCA_MCweak[iSpecies][iCharge]->GetBinContent(iPtBin) > 0. ? histTightDCA_MCweak[iSpecies][iCharge]->GetBinContent(iPtBin) / histLooseDCA_MCweak[iSpecies][iCharge]->GetBinContent(iPtBin) : -1));
					// histPctSurvivedDCACut_MCmat[iSpecies][iCharge]->SetBinContent(iPtBin, (histLooseDCA_MCmat[iSpecies][iCharge]->GetBinContent(iPtBin) > 0. ? histTightDCA_MCmat[iSpecies][iCharge]->GetBinContent(iPtBin) / histLooseDCA_MCmat[iSpecies][iCharge]->GetBinContent(iPtBin) : -1));
				// }

				// Calulate fraction of tracks that are cut away with the tight DCA cut (Binomial Errors)
				histPctSurvivedDCACut_MCprim[iSpecies][iCharge] = (TH1F*)(histTightDCA_MCprim[iSpecies][iCharge])->Clone(Form("histPctSurvivedDCACut_MCprim_sp%i_ch%i",iSpecies, iCharge));
				histPctSurvivedDCACut_MCprim[iSpecies][iCharge]->Sumw2(); // Need to do this before dividing!
				histPctSurvivedDCACut_MCprim[iSpecies][iCharge]->SetTitle(Form("histPctSurvivedDCACut_MCprim_sp%i_ch%i",iSpecies, iCharge));
				histPctSurvivedDCACut_MCprim[iSpecies][iCharge]->Divide(histPctSurvivedDCACut_MCprim[iSpecies][iCharge], histLooseDCA_MCprim[iSpecies][iCharge],1,1,"B");
				histPctSurvivedDCACut_MCprim[iSpecies][iCharge]->Write();
				delete histPctSurvivedDCACut_MCprim[iSpecies][iCharge]; histPctSurvivedDCACut_MCprim[iSpecies][iCharge] = 0x0;
				
				histPctSurvivedDCACut_MCweak[iSpecies][iCharge] = (TH1F*)(histTightDCA_MCweak[iSpecies][iCharge])->Clone(Form("histPctSurvivedDCACut_MCweak_sp%i_ch%i",iSpecies, iCharge));
				histPctSurvivedDCACut_MCweak[iSpecies][iCharge]->Sumw2();
				histPctSurvivedDCACut_MCweak[iSpecies][iCharge]->SetTitle(Form("histPctSurvivedDCACut_MCweak_sp%i_ch%i",iSpecies, iCharge));
				histPctSurvivedDCACut_MCweak[iSpecies][iCharge]->Divide(histPctSurvivedDCACut_MCweak[iSpecies][iCharge], histLooseDCA_MCweak[iSpecies][iCharge],1,1,"B");
				histPctSurvivedDCACut_MCweak[iSpecies][iCharge]->Write();
				delete histPctSurvivedDCACut_MCweak[iSpecies][iCharge]; histPctSurvivedDCACut_MCweak[iSpecies][iCharge] = 0x0;
				
				histPctSurvivedDCACut_MCmat[iSpecies][iCharge] = (TH1F*)(histTightDCA_MCmat[iSpecies][iCharge])->Clone(Form("histPctSurvivedDCACut_MCmat_sp%i_ch%i",iSpecies, iCharge));
				histPctSurvivedDCACut_MCmat[iSpecies][iCharge]->Sumw2();
				histPctSurvivedDCACut_MCmat[iSpecies][iCharge]->SetTitle(Form("histPctSurvivedDCACut_MCmat_sp%i_ch%i",iSpecies, iCharge));
				histPctSurvivedDCACut_MCmat[iSpecies][iCharge]->Divide(histPctSurvivedDCACut_MCmat[iSpecies][iCharge], histLooseDCA_MCmat[iSpecies][iCharge],1,1,"B");
				histPctSurvivedDCACut_MCmat[iSpecies][iCharge]->Write();
				delete histPctSurvivedDCACut_MCmat[iSpecies][iCharge]; histPctSurvivedDCACut_MCmat[iSpecies][iCharge] = 0x0;
				
				// fout->cd("SpectraNormalCuts");
				histTightDCA_MCprim[iSpecies][iCharge]->Write();
				delete histTightDCA_MCprim[iSpecies][iCharge]; histTightDCA_MCprim[iSpecies][iCharge] = 0x0;
				histTightDCA_MCweak[iSpecies][iCharge]->Write();
				delete histTightDCA_MCweak[iSpecies][iCharge]; histTightDCA_MCweak[iSpecies][iCharge] = 0x0;
				histTightDCA_MCmat[iSpecies][iCharge]->Write();
				delete histTightDCA_MCmat[iSpecies][iCharge]; histTightDCA_MCmat[iSpecies][iCharge] = 0x0;

				// fout->cd("SpectraDCATemplateCuts");
				histLooseDCA_MCprim[iSpecies][iCharge]->Write();
				delete histLooseDCA_MCprim[iSpecies][iCharge]; histLooseDCA_MCprim[iSpecies][iCharge] = 0x0;
				histLooseDCA_MCweak[iSpecies][iCharge]->Write();
				delete histLooseDCA_MCweak[iSpecies][iCharge]; histLooseDCA_MCweak[iSpecies][iCharge] = 0x0;
				histLooseDCA_MCmat[iSpecies][iCharge]->Write();
				delete histLooseDCA_MCmat[iSpecies][iCharge]; histLooseDCA_MCmat[iSpecies][iCharge] = 0x0;

			} else {

				// fout->cd();
				histDCA_Data[iSpecies][iCharge]->Write();
				delete histDCA_Data[iSpecies][iCharge]; histDCA_Data[iSpecies][iCharge] = 0x0;

				// fout->cd("PctSurvivingDCACut");
				// for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {
				// 	histPctSurvivedDCACut_data[iSpecies][iCharge]->SetBinContent(iPtBin, (histLooseDCA_data[iSpecies][iCharge]->GetBinContent(iPtBin) > 0. ? histTightDCA_data[iSpecies][iCharge]->GetBinContent(iPtBin) / histLooseDCA_data[iSpecies][iCharge]->GetBinContent(iPtBin) : -1));
				// }
				// histPctSurvivedDCACut_data[iSpecies][iCharge]->Write();
				// delete histPctSurvivedDCACut_data[iSpecies][iCharge]; histPctSurvivedDCACut_data[iSpecies][iCharge] = 0x0;

				histPctSurvivedDCACut_data[iSpecies][iCharge] = (TH1F*)(histTightDCA_data[iSpecies][iCharge])->Clone(Form("histPctSurvivedDCACut_data_sp%i_ch%i",iSpecies, iCharge));
				histPctSurvivedDCACut_data[iSpecies][iCharge]->Sumw2();
				histPctSurvivedDCACut_data[iSpecies][iCharge]->SetTitle(Form("histPctSurvivedDCACut_data_sp%i_ch%i",iSpecies, iCharge));
				histPctSurvivedDCACut_data[iSpecies][iCharge]->Divide(histPctSurvivedDCACut_data[iSpecies][iCharge], histLooseDCA_data[iSpecies][iCharge],1,1,"B");
				histPctSurvivedDCACut_data[iSpecies][iCharge]->Write();
				delete histPctSurvivedDCACut_data[iSpecies][iCharge]; histPctSurvivedDCACut_data[iSpecies][iCharge] = 0x0;

				// fout->cd("SpectraNormalCuts");
				histTightDCA_data[iSpecies][iCharge]->Write();
				delete histTightDCA_data[iSpecies][iCharge]; histTightDCA_data[iSpecies][iCharge] = 0x0;

				// fout->cd("SpectraDCATemplateCuts");
				histLooseDCA_data[iSpecies][iCharge]->Write();
				delete histLooseDCA_data[iSpecies][iCharge]; histLooseDCA_data[iSpecies][iCharge] = 0x0;


			}
		}
	}

	fout->Close();

	// Stop the timer.
	cout << "Analysis time: " << AliMiscToolsDiHadronPID::FormatTime(sw.RealTime()).Data() << endl;
	cout << "Processed events: " << nEvents << endl;

}

// -----------------------------------------------------------------------
Bool_t AliProcessDataDiHadronPIDMini::SaveBuffer(TString filename) {

	// Saves the fBufferCorrPID and fBufferCorrUI objects to file.
	AliLogDiHadronPID::Debug2(Form("Called with filename: %s", filename.Data()), __func__, ClassName());

	if (fVerboseLevel > 0) {cout << "Saving Buffer To: "<<filename.Data() << endl;}
	TFile* fout = new TFile(filename.Data(), "recreate");
	if (!fout) {return kFALSE;}

	fCurrentTriggerHist->Write();

	fCurrentCorrPID->Write();
	fCurrentCorrUI->Write();
	fCurrentMixedPID->Write();
	fCurrentMixedUI->Write();
	//fCurrentFitTemplates->Write();

	if (fSettings->GetIsMC()) {
		fCurrentCorrMCPion->Write();
		fCurrentMixedMCPion->Write();
		
		fCurrentCorrMCKaon->Write();
		fCurrentMixedMCKaon->Write();
		
		fCurrentCorrMCProton->Write();
		fCurrentMixedMCProton->Write();

	}

	fout->Close();

	return kTRUE;

}

// -----------------------------------------------------------------------
Bool_t AliProcessDataDiHadronPIDMini::GenerateFileList() {

	// Creates a list of all files in a certain directory tree.
	// the -follow option ensures that symlinks are followed.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	FILE* pipe = gSystem->OpenPipe(Form("find %s -name %s -follow",fDataDir.Data(), fInputFileName.Data()),"r");
	
	cout << fDataDir.Data() << endl;

	fInputFileList = new TObjArray(100);
	fInputFileList->SetOwner(kTRUE);

	TString tmpFileName;
	while (tmpFileName.Gets(pipe)) {	

		//cout<<tmpFileName.Data()<<endl;
		fInputFileList->AddLast(new TObjString(tmpFileName.Data()));

	}	

	// Check if a sensible list has been created.
	if (fInputFileList->GetEntriesFast()) {return kTRUE;}
	else {
		cout<<"WARNING: File list not generated, no matching files found..."<<endl;
		delete fInputFileList; fInputFileList = 0x0;
		return kFALSE;
	}

}

// -----------------------------------------------------------------------
void AliProcessDataDiHadronPIDMini::CheckFileList() {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	for (Int_t iFile = 0; iFile < fInputFileList->GetEntriesFast(); iFile++) {
		TObjString* tmpFileName = (TObjString*)fInputFileList->At(iFile);
		cout << (tmpFileName->GetString()).Data() << " -> ";
		TFile* tmpFile = new TFile((tmpFileName->GetString()).Data());
		if (tmpFile->IsZombie()) {
			cout << "ERROR!" << endl;
			fInputFileList->RemoveAt(iFile);
			//fInputFileList->Remove(tmpFile);
		} else {cout << "OK!" << endl;}

		tmpFile->Close();
		delete tmpFile;

	}

}

// -----------------------------------------------------------------------
Bool_t AliProcessDataDiHadronPIDMini::WriteObj(TObject* obj, TDirectory* dir) {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	Bool_t success = dir->cd();
	if (!success) {return success;}

	obj->Write();

	return success;

}

// -----------------------------------------------------------------------
Bool_t AliProcessDataDiHadronPIDMini::WriteObjAndDelete(TObject* obj, TDirectory* dir) {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	Bool_t success = WriteObj(obj, dir);

	delete obj;
	obj = 0x0;

	return success;

}

// -----------------------------------------------------------------------
Double_t AliProcessDataDiHadronPIDMini::MixedEventNorm(const TH2F* hist) {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, "AliProcessDataDiHadronPIDMini");

	// Assuming the DPhi axis is the X axis, and DEta is the Y axis.
	Int_t nBinsDPhi = (hist->GetXaxis())->GetNbins();
	Int_t nBinsDEta = (hist->GetYaxis())->GetNbins();

	// Check if even, if so then take two rows, else take one.
	if (nBinsDEta % 2) {
		return (hist->Integral(1, nBinsDPhi, nBinsDEta / 2, nBinsDEta / 2 + 1) / (2. * nBinsDPhi));
	} else {
		return (hist->Integral(1, nBinsDPhi, nBinsDEta / 2, nBinsDEta / 2) / (nBinsDPhi));
	}

}

// -----------------------------------------------------------------------
Bool_t AliProcessDataDiHadronPIDMini::CreateBuffer() {

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	// Create trigger histogram.
	fCurrentTriggerHist = new TH1F("triggerHist", "triggerHist", 1, fSettings->GetMinTriggerPt(), fSettings->GetMaxTriggerPt());

	// Create object arrays.
	fCurrentCorrPID		= new TObjArray(fPtAxis->GetNbins());
	fCurrentMixedPID	= new TObjArray(fPtAxis->GetNbins());

	fCurrentCorrPID->SetOwner(kTRUE);
	fCurrentMixedPID->SetOwner(kTRUE);

	fCurrentCorrUI		= new TObjArray(fPtAxis->GetNbins());
	fCurrentMixedUI 	= new TObjArray(fPtAxis->GetNbins());
	
	fCurrentCorrUI->SetOwner(kTRUE);
	fCurrentMixedUI->SetOwner(kTRUE);

	// Create buffer for MC.
	if (fSettings->GetIsMC()) {

		fCurrentCorrMCPion = new TObjArray(fPtAxis->GetNbins());
		fCurrentMixedMCPion = new TObjArray(fPtAxis->GetNbins());

		fCurrentCorrMCPion->SetOwner(kTRUE);
		fCurrentMixedMCPion->SetOwner(kTRUE);

		fCurrentCorrMCKaon = new TObjArray(fPtAxis->GetNbins());
		fCurrentMixedMCKaon = new TObjArray(fPtAxis->GetNbins());

		fCurrentCorrMCKaon->SetOwner(kTRUE);
		fCurrentMixedMCKaon->SetOwner(kTRUE);

		fCurrentCorrMCProton = new TObjArray(fPtAxis->GetNbins());
		fCurrentMixedMCProton = new TObjArray(fPtAxis->GetNbins());

		fCurrentCorrMCProton->SetOwner(kTRUE);
		fCurrentMixedMCProton->SetOwner(kTRUE);

	}

	//fCurrentFitTemplates = new AliFitTemplatesDiHadronPID(fPtAxis, fEtaAxis, fDEtaAxis, fMassAssumption);

	// {DPhi, DEta, TOF, TPC} // FIXME: PID Ranges!!
	Int_t dim[4] 		= {fDPhiAxis->GetNbins(), fDEtaAxis->GetNbins(), fTOFAxis->GetNbins(), fTPCAxis->GetNbins()};
	Double_t min[4]		= {fDPhiAxis->GetXmin(), fDEtaAxis->GetXmin(), fTOFAxis->GetXmin(), fTPCAxis->GetXmin()};
	Double_t max[4]		= {fDPhiAxis->GetXmax(), fDEtaAxis->GetXmax(), fTOFAxis->GetXmax(), fTPCAxis->GetXmax()};

	for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); ++iPtBin) {
		
		// Create histograms with PID info. TODO: Separate constructor with const TAxis arguments.
		THnF* tmpCurrentCorrPID = new THnF(Form("CorrelationsPID_pt%i", iPtBin), Form("CorrelationsPID_pt%i;#Delta#phi;#Delta#eta", iPtBin), 4, dim, min, max);
		fCurrentCorrPID->AddAt(tmpCurrentCorrPID, iPtBin - 1);

		THnF* tmpCurrentMixedPID = new THnF(Form("MixedEventsPID_pt%i", iPtBin), Form("MixedEventsPID_pt%i;#Delta#phi;#Delta#eta", iPtBin), 4, dim, min, max);
		fCurrentMixedPID->AddAt(tmpCurrentMixedPID, iPtBin - 1);

		// Creating histos without PID info.
		TH2F* tmpCurrentCorrUI	= new TH2F(Form("CorrelationsUI_pt%i", iPtBin), Form("CorrelationsUI_pt%i;#Delta#phi;#Delta#eta", iPtBin), dim[0], min[0], max[0], dim[1], min[1], max[1]);
		fCurrentCorrUI->AddAt(tmpCurrentCorrUI, iPtBin - 1);

		TH2F* tmpCurrentMixedUI	= new TH2F(Form("MixedEventsUI_pt%i", iPtBin), Form("MixedEventsUI_pt%i;#Delta#phi;#Delta#eta", iPtBin), dim[0], min[0], max[0], dim[1], min[1], max[1]);
		fCurrentMixedUI->AddAt(tmpCurrentMixedUI, iPtBin - 1);

		// Creating MC Truth histograms.
		if (fSettings->GetIsMC()) {

			TH2F* tmpCurrentCorrMCPion = new TH2F(Form("CorrelationsMCPion_pt%i", iPtBin), Form("CorrelationsMCPion_pt%i;#Delta#phi;#Delta#eta", iPtBin), dim[0], min[0], max[0], dim[1], min[1], max[1]);
			fCurrentCorrMCPion->AddAt(tmpCurrentCorrMCPion, iPtBin - 1);

			TH2F* tmpCurrentMixedMCPion	= new TH2F(Form("MixedEventsMCPion_pt%i", iPtBin), Form("MixedEventsMCPion_pt%i;#Delta#phi;#Delta#eta", iPtBin), dim[0], min[0], max[0], dim[1], min[1], max[1]);
			fCurrentMixedMCPion->AddAt(tmpCurrentMixedMCPion, iPtBin - 1);

			TH2F* tmpCurrentCorrMCKaon = new TH2F(Form("CorrelationsMCKaon_pt%i", iPtBin), Form("CorrelationsMCKaon_pt%i;#Delta#phi;#Delta#eta", iPtBin), dim[0], min[0], max[0], dim[1], min[1], max[1]);
			fCurrentCorrMCKaon->AddAt(tmpCurrentCorrMCKaon, iPtBin - 1);

			TH2F* tmpCurrentMixedMCKaon	= new TH2F(Form("MixedEventsMCKaon_pt%i", iPtBin), Form("MixedEventsMCKaon_pt%i;#Delta#phi;#Delta#eta", iPtBin), dim[0], min[0], max[0], dim[1], min[1], max[1]);
			fCurrentMixedMCKaon->AddAt(tmpCurrentMixedMCKaon, iPtBin - 1);

			TH2F* tmpCurrentCorrMCProton = new TH2F(Form("CorrelationsMCProton_pt%i", iPtBin), Form("CorrelationsMCProton_pt%i;#Delta#phi;#Delta#eta", iPtBin), dim[0], min[0], max[0], dim[1], min[1], max[1]);
			fCurrentCorrMCProton->AddAt(tmpCurrentCorrMCProton, iPtBin - 1);

			TH2F* tmpCurrentMixedMCProton	= new TH2F(Form("MixedEventsMCProton_pt%i", iPtBin), Form("MixedEventsMCProton_pt%i;#Delta#phi;#Delta#eta", iPtBin), dim[0], min[0], max[0], dim[1], min[1], max[1]);
			fCurrentMixedMCProton->AddAt(tmpCurrentMixedMCProton, iPtBin - 1);

		}
	
	}

	return kTRUE;

} 

// -----------------------------------------------------------------------
void AliProcessDataDiHadronPIDMini::ClearBuffer() {

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	// Sets all the buffer histograms to zero.
	if (fVerboseLevel > 0) {cout << "Resetting Buffer..." << endl;}

	for (Int_t iPt = 0; iPt < fPtAxis->GetNbins(); ++iPt) {

		((THnF*)fCurrentCorrPID->At(iPt))->Reset();
		((THnF*)fCurrentMixedPID->At(iPt))->Reset();
		((TH2F*)fCurrentCorrUI->At(iPt))->Reset();
		((TH2F*)fCurrentMixedUI->At(iPt))->Reset();

		if (fSettings->GetIsMC()) {		
			((TH2F*)fCurrentCorrMCPion->At(iPt))->Reset();
			((TH2F*)fCurrentMixedMCPion->At(iPt))->Reset();
			
			((TH2F*)fCurrentCorrMCKaon->At(iPt))->Reset();
			((TH2F*)fCurrentMixedMCKaon->At(iPt))->Reset();			
		
			((TH2F*)fCurrentCorrMCProton->At(iPt))->Reset();
			((TH2F*)fCurrentMixedMCProton->At(iPt))->Reset();
		
		}

	}

}

// -----------------------------------------------------------------------
Bool_t AliProcessDataDiHadronPIDMini::NextFile(Bool_t reset) {

	// Opens the first file in the InputFileList, and loads the first event.
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	// Closes current file and deletes objects.
	if (fCurrentFile) {
		fCurrentFile->Close();
		fCurrentFile = 0x0;
	}

	// Reset event branch and index.
	fCurrentEventBranch = 0x0;	// I don't think we need to delete this.
	fCurrentEventIndex = 0;

	// If reset is called then we start at the first file, otherwise we go
	// to the next one.
	if (reset) {fCurrentFileIndex = 0;}
	else {fCurrentFileIndex++;}

	// Check if there are more files in the list.
	if (fCurrentFileIndex == fInputFileList->GetEntriesFast()) {
		//cout << "No more files in the list!" << endl;
		return kFALSE; 
	}

	// Look up file name.
	TObjString* tmpFileName = (TObjString*)fInputFileList->At(fCurrentFileIndex);	
	while (!tmpFileName) {

		// If the last file in the list is missing, then we're done.
		if (fCurrentFileIndex + 1 == fInputFileList->GetEntriesFast()) {return kFALSE;}

		// If not, try to go to the next file.
		cout << "File missing in the list, skipping to the next one!" << endl;
		fCurrentFileIndex++; 
		tmpFileName = (TObjString*)fInputFileList->At(fCurrentFileIndex);
	}

	// Load file.
	fCurrentFile = new TFile((tmpFileName->GetString()).Data());
	if (fCurrentFile->IsZombie()) {
		cout << "ERROR: could not open: "<< (tmpFileName->GetString()).Data() << endl;
		return kFALSE;
	} else {
		if (fVerboseLevel > 1) cout << endl;
		if (fVerboseLevel > 1) cout << "---------------------------------------------------------------" << endl;
		if (fVerboseLevel > 0) cout << " Opening file: "<<fCurrentFileIndex + 1 <<"/"<< fInputFileList->GetEntriesFast()<<": "<< (tmpFileName->GetString()).Data() << endl;
		if (fVerboseLevel > 1) cout << "---------------------------------------------------------------" << endl;
	}

	// Read the event tree in the file.
	TDirectoryFile* tmpDir = (TDirectoryFile*)fCurrentFile->Get("PWGCF_DiHadronPIDMini");
	if (!tmpDir) {
		cout << "ERROR: could not read PWGCF_DiHadronPIDMini" << endl;
		return kFALSE;
	}


	TTree* tmpEventTree = (TTree*)tmpDir->Get("eventTree");
	if (!tmpEventTree) {
		cout << "ERROR: could not read eventTree" << endl;
		return kFALSE;
	}

	// Let fCurrentEvent point to the first event.
	fCurrentEventBranch = tmpEventTree->GetBranch("events");
	fCurrentEventBranch->SetAddress(&fCurrentEvent);
	
	return kTRUE;

}

// -----------------------------------------------------------------------
Bool_t AliProcessDataDiHadronPIDMini::GetEvent() {

	// Get the next available event.
	AliLogDiHadronPID::Debug4(Form("Called."), __func__, ClassName());

	// Check if we're at the end of the file.
	if (fCurrentEventIndex == fCurrentEventBranch->GetEntries()) {
		
		// Check if there's a net file available, if not, then return false.
		if (!NextFile()) {return kFALSE;}
		else {return GetEvent();}	// Forces the loop to start over (protects against empty files).
	}

	// Get the event located at the current index.
	fCurrentEventBranch->GetEntry(fCurrentEventIndex);

	// Print info.
	if (fVerboseLevel > 1) cout << "  Event: " << fCurrentEventIndex + 1 << "/" << fCurrentEventBranch->GetEntries() << endl;
	
	// Increment the index.
	fCurrentEventIndex++;

	return kTRUE;

}

// -----------------------------------------------------------------------
Bool_t AliProcessDataDiHadronPIDMini::IsSelected(const AliMiniEventDiHadronPID* event, Int_t vtxZbin, Int_t centBin) const {

	//cout << "vtxz: " << event->GetVtxZ() << " bin: "<< (fVertexZAxis->FindBin(event->GetVtxZ()))<<endl;
	//cout << "cent: " << event->GetCentrality() << " bin: " << (fCentralityAxis->FindBin(event->GetCentrality())) << endl;

	AliLogDiHadronPID::Debug4(Form("Called."), __func__, ClassName());

	if ((fVertexZAxis->FindBin(event->GetVtxZ()) == vtxZbin) &&
		(fCentralityAxis->FindBin(event->GetCentrality()) == centBin)) {return kTRUE;}
	else {return kFALSE;}

}

// -----------------------------------------------------------------------
Bool_t AliProcessDataDiHadronPIDMini::IsSelected(const AliMiniEventDiHadronPID* event) const {

	AliLogDiHadronPID::Debug4(Form("Called."), __func__, ClassName());

	if ((fVertexZAxis->GetXmin() < event->GetVtxZ()) &&
		(fVertexZAxis->GetXmax() > event->GetVtxZ()) &&
		(fCentralityAxis->GetXmin() < event->GetCentrality()) &&
		(fCentralityAxis->GetXmax() > event->GetCentrality())) {
		return kTRUE;}
	else {
		return kFALSE;
	}

}

// -----------------------------------------------------------------------
Bool_t AliProcessDataDiHadronPIDMini::IsTrackInPool(const AliMiniTrackDiHadronPID* track) const {

	// While this is not a water-proof method, it is quite unlikely 
	// that two tracks are this similar, and not the same track.
	AliLogDiHadronPID::Debug4(Form("Called."), __func__, ClassName());

	Bool_t inpool = kFALSE;
	cout << "entries: "<< fTriggerPool->GetEntries() << endl;
	for (Int_t iTrack = 0; iTrack < fTriggerPool->GetEntries(); ++iTrack) {
		if (track->IsVerySimilarTo((AliMiniTrackDiHadronPID*)fTriggerPool->At(iTrack))) { 
			cout << __func__ << ": track is probably in pool, at pos: " << iTrack << endl;
			//track->PrintTrack();
			//((AliMiniTrackDiHadronPID*)fTriggerPool->At(iTrack))->PrintTrack();
			inpool = kTRUE;
		}
	}

	return inpool;

}

// -----------------------------------------------------------------------
void AliProcessDataDiHadronPIDMini::PrintPoolPointers() const {

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	for (Int_t iTrack = 0; iTrack < fTriggerPool->GetEntries(); ++iTrack) {
		AliMiniTrackDiHadronPID* tr = (AliMiniTrackDiHadronPID*)fTriggerPool->At(iTrack);
		cout << iTrack << " -> " << tr << " pt: "<< tr->Pt() << endl;
	}

}

// -----------------------------------------------------------------------
Int_t AliProcessDataDiHadronPIDMini::GetPoolArrayIndex(Int_t vtxZBin, Int_t centBin) const {

	AliLogDiHadronPID::Debug4(Form("Called."), __func__, ClassName());

	if (vtxZBin < 0 || vtxZBin > fVertexZAxis->GetNbins() ||
		centBin < 0 || centBin > fCentralityAxis->GetNbins()) {return -999;}

	else {return ((vtxZBin - 1) * fCentralityAxis->GetNbins() + (centBin - 1));} 	

}

// -----------------------------------------------------------------------
Int_t AliProcessDataDiHadronPIDMini::GetPoolArrayIndex(Double_t vtxZ, Double_t cent) const {

	AliLogDiHadronPID::Debug4(Form("Called."), __func__, ClassName());

	return GetPoolArrayIndex(fVertexZAxis->FindBin(vtxZ), fCentralityAxis->FindBin(cent));

}

// -----------------------------------------------------------------------
void AliProcessDataDiHadronPIDMini::ResetTriggerPool() {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	// Emptying the trigger pool.
	if (fVerboseLevel > 0) {cout << "Clearing Trigger Buffer..." << endl;}

	fTriggerPool->Clear();	// In principle this is not necessary...
	fPoolWriteIndex = 0;
	fPoolIsFilled = kFALSE;
	fStartedMixingAtEvent = 0;
	fStartedMixingAtFile = 0;

}

// -----------------------------------------------------------------------
void* AliProcessDataDiHadronPIDMini::CorrectForMixedEvent(void* fnames) {

	// Perform mixed event correction on a file containing correlation and
	// mixed event results. Method is designed to run in a parallel thread,
	// so that it can be called when another correlation analysis is ran.

	// filename should be a TObjArray holding TObjStrings for:
	//  [0] input file name;
	//  [1] output file name.
	// This method will take responsibility for deleting the array.
	TObjArray* filenames = (TObjArray*)fnames;
	TString finNameStr = ((TObjString*)filenames->At(0))->String(); // Make a copy! Note that the array
	TString foutNameStr = ((TObjString*)filenames->At(1))->String(); // could in principle be deleted here.

	// Read input file.
	TFile* fin = new TFile(finNameStr.Data());
	if (fin->IsZombie()) {cout << Form("%s -> Could not open file: %s", __func__, finNameStr.Data()) << endl; return 0;}

	// Create output file.
	TFile* fout = new TFile(foutNameStr.Data(), "recreate");
	if (fin->IsZombie()) {cout << Form("%s -> Could not create file: %s", __func__, foutNameStr.Data()) << endl; return 0;}

	Int_t ptbin = 0;

	TH2F* uisignal = 0x0;
	fin->GetObject(Form("CorrelationsUI_pt%i", ptbin), uisignal);
	if (!uisignal) {
		cout << __func__ << ": Could not find first UI signal histogram." << endl; 
		fin->ls();
		return 0;
	} 

	TH2F* uimixed = 0x0;
	fin->GetObject(Form("MixedEventsUI_pt%i", ptbin), uimixed);
	if (!uimixed) {
		cout << __func__ << ": Could not find first UI mixed event histogram." << endl; 
		fin->ls();
		return 0;
	} 

	// Loop over unidentified histograms.
	while(uisignal && uimixed) {
		
		//cout << __func__ << ": Mixing UI correlations, pt bin: " << ptbin << endl;
		TH2F* uicorr = (TH2F*)uisignal->Clone(Form("CorrelationsUI_pt%i", ptbin));
		uicorr->SetDirectory(0);
		uicorr->Sumw2();

		TH2F* uimixednorm = (TH2F*)uimixed->Clone(Form("MixedEventsUINormalized_pt%i", ptbin));
		uimixednorm->SetDirectory(0);
		uimixednorm->Sumw2();


		uimixednorm->Scale(1./MixedEventNorm(uimixed));

		uicorr->Divide(uimixednorm);

		// Write to the output file.
		fout->cd();
		uicorr->Write();
		uimixednorm->Write();

		// Should be OK to do this...
		delete uisignal; uisignal = 0x0;
		delete uimixed; uimixed = 0x0;
		delete uicorr; uicorr = 0x0;
		delete uimixednorm; uimixednorm = 0x0;

		// Load the next UI histograms.
		ptbin++;
		fin->GetObject(Form("CorrelationsUI_pt%i", ptbin), uisignal); 
		fin->GetObject(Form("MixedEventsUI_pt%i", ptbin), uimixed);

	}

	ptbin = 0;
	Int_t spbin = 0;

	THnF* pidsignal = 0x0;
	fin->GetObject(Form("CorrelationsPID_sp%i_pt%i", spbin, ptbin), pidsignal);
	if (!pidsignal) {cout << __func__ << ": Could not find first PID signal histogram: " << endl; return 0;} 

	TH2F* pidmixed = 0x0;
	fin->GetObject(Form("MixedEventsPID_sp%i_pt%i", spbin, ptbin), pidmixed);
	if (!pidmixed) {cout << __func__ << ": Could not find first PID mixed event histogram." << endl; return 0;} 

	// Loop over identified histograms.
	while(pidsignal && pidmixed) {

		//cout << __func__ << ": Mixing PID correlations, species: "<<spbin << ", pt bin: " << ptbin << endl;
		THnF* pidcorr = (THnF*)pidsignal->Clone(Form("CorrelationsPID_sp_%i_pt%i", spbin, ptbin));
		pidcorr->Sumw2();

		TH2F* pidmixednorm = (TH2F*)pidmixed->Clone(Form("MixedEventsPIDNormalized_sp%i_pt%i", spbin, ptbin));
		pidmixednorm->SetDirectory(0);
		pidmixednorm->Sumw2();
		pidmixednorm->Scale(1./MixedEventNorm(pidmixed));

		// Check if the axes seem compatible.
		TAxis* dPhiAxisNum = pidcorr->GetAxis(0);
		TAxis* dEtaAxisNum = pidcorr->GetAxis(1);
		TAxis* dPhiAxisDen = pidmixed->GetXaxis();
		TAxis* dEtaAxisDen = pidmixed->GetYaxis();
		if (AliHistToolsDiHadronPID::DoAxesMatch(dPhiAxisNum, dPhiAxisDen) &&
			AliHistToolsDiHadronPID::DoAxesMatch(dEtaAxisNum, dEtaAxisDen)) {

			Int_t coord4D[4] = {0, 0, 0, 0};
			Int_t nZeros = 0;

			// Division
			for (Int_t iDeltaPhi = 1; iDeltaPhi <= dPhiAxisNum->GetNbins(); ++iDeltaPhi) {
				coord4D[0] = iDeltaPhi;

				for (Int_t iDeltaEta = 1; iDeltaEta <= dEtaAxisNum->GetNbins(); ++iDeltaEta) {
					coord4D[1] = iDeltaEta;

					Float_t denvalnorm = pidmixednorm->GetBinContent(iDeltaPhi, iDeltaEta);
					Float_t denerrnorm = pidmixednorm->GetBinError(iDeltaPhi, iDeltaEta);
					Float_t denval = pidmixed->GetBinContent(iDeltaPhi, iDeltaEta);

					// Look at the non-normalized mixed events for zero-bins...
					if (denval < 0.1) {nZeros++;}

					// Loop over the PID axes (also overflow!)
					for (Int_t iPID1 = 0; iPID1 <= ((pidcorr->GetAxis(2))->GetNbins() + 1); ++iPID1) {
						coord4D[2] = iPID1;

						for (Int_t iPID2 = 0; iPID2 <= ((pidcorr->GetAxis(2))->GetNbins() + 1); ++iPID2) {
							coord4D[3] = iPID2;

							// Note that this is safe; if the denominator is < 0.1 it must be 0.;
							if (denval > 0.1) {
								Float_t numval = pidcorr->GetBinContent(coord4D);
								pidcorr->SetBinContent(coord4D, numval / denvalnorm);
								
								Float_t numerr = pidcorr->GetBinError(coord4D);
								Float_t newerr = (numerr*numerr*denvalnorm*denvalnorm + denerrnorm*denerrnorm*numval*numval) / (denvalnorm*denvalnorm*denvalnorm*denvalnorm);
								pidcorr->SetBinError(coord4D, newerr);

							} else {
								pidcorr->SetBinContent(coord4D, 0.);
								pidcorr->SetBinError(coord4D, 0.);
							}
						}
					}
				}
			}

			if (nZeros) {
				cout << __func__ << ": WARNING: encountered " << nZeros << " zeros in the mixed event histogram... " << endl;
			}

		} else {
			cout << __func__ << ": ERROR: Axes of numerator and denominator do not match!" << endl;
		}

		// Write to file.
		fout->cd();
		pidcorr->Write();

		// Clean up memory.
		delete pidsignal; pidsignal = 0x0;
		delete pidmixed; pidmixed = 0x0;
		delete pidcorr; pidcorr = 0x0;		

		// Load the next histogram (not very nice to use magic number 3)
		spbin++;
		if (spbin == 3) {
			spbin = 0;
			ptbin++;
		}
		fin->GetObject(Form("CorrelationsPID_sp%i_pt%i", spbin, ptbin), pidsignal); 
		fin->GetObject(Form("MixedEventsPID_sp%i_pt%i", spbin, ptbin), pidmixed);

	} 

	// Close input file.
	fin->Close();
	delete fin;
	fin = 0x0;

	// Close output file.
	fout->Close();
	delete fout;
	fout = 0x0;

	// Delete file name pointer (we're the only ones still using it!)
	filenames->SetOwner(kTRUE);
	delete filenames;
	filenames = 0x0;

	return 0;

}

// -----------------------------------------------------------------------
void* AliProcessDataDiHadronPIDMini::Merge(void* merger) {

	// Performs a merge, and cleans up the file merger.
	TFileMerger* mgr = (TFileMerger*)merger;
	mgr->Merge();
	delete mgr;
	mgr = 0x0;

	return 0;

}
