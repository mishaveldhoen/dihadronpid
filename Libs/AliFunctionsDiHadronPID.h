#ifndef ALIFUNCTIONSDIHADRONPID_H
#define ALIFUNCTIONSDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TCanvas.h"
#include "TMath.h"

class AliFunctionsDiHadronPID {

public:
	AliFunctionsDiHadronPID();

protected:
	~AliFunctionsDiHadronPID();

public:
	// Math.
	static Int_t Power(Int_t base, Int_t power);
	static Double_t Sqrt2Pi() {return 2.50662827463100024;}

	// Natural constants.
	static Double_t Charge() {return 1.60217646e-19;}	// (C)
	static Double_t C() {return 2.99792458e+8;}			// (m/s)
	static Double_t Mpion() {return 0.13957018;}		// (GeV/c^2)
	static Double_t Mkaon() {return 0.493667;}			// (GeV/c^2)
	static Double_t Mproton() {return 0.938272046;}		// (GeV/c^2)
	static Double_t Mdeuteron() {return 2.01410178*GeVperu();}	// (GeV/c^2)
	static Double_t Melectron() {return 0.000510998910;} // (GeV/c^2)
	static Double_t Mmuon() {return 0.1056583715;} // (GeV/c^2)
	static Double_t M(Int_t species) {
		switch(species) {
			case 0:return Mpion();
			case 1:return Mkaon();
			case 2:return Mproton();
			case 3:return Mdeuteron();
			case 4:return Melectron();
			case 5:return Mmuon();
			default:return -999.; 
		}
	}

	// Conversions.
	static Double_t GeVperu() {return 0.931494061;}		// (GeV/c^2) per u
	static Double_t GeVperkg() {return 5.608524e+26;} 	// (GeV/c^2) per kg

	// Detector paramters.
	static Double_t RTOF() {return 385.;}				// Radius of TOF (cm).
	static Double_t BTPC() {return 0.5;}				// Magnetic field in TPC (T = kg C^-1 s^-1).

	// Fit Functions (these are basically riemann sums, i.e, f(x)*dx... It's better to use the integrals defined as below).
	static Double_t Gaussian1D(Double_t xx, Double_t integral, Double_t mu, Double_t sigma, Double_t binwidth = 1.);
	static Double_t Gaussian1DTail(Double_t xx, Double_t integral, Double_t mu, Double_t sigma, Double_t tail, Double_t binwidth = 1.);

	static Double_t Gaussian2D(Double_t xx, Double_t yy, Double_t integral, 
		Double_t mux, Double_t muy, Double_t sigmax, Double_t sigmay, 
		Double_t binwidthx = 1., Double_t binwidthy = 1.);

	static Double_t Gaussian2DTailX(Double_t xx, Double_t yy, Double_t integral, 
		Double_t mux, Double_t muy, Double_t sigmax, Double_t sigmay, 
		Double_t tailx, Double_t binwidthx = 1., Double_t binwidthy = 1.);

	static Double_t Gaussian2DTailY(Double_t xx, Double_t yy, Double_t integral, 
		Double_t mux, Double_t muy, Double_t sigmax, Double_t sigmay, 
		Double_t taily, Double_t binwidthx = 1., Double_t binwidthy = 1.);

	static Double_t Gaussian2DTailXY(Double_t xx, Double_t yy, Double_t integral, 
		Double_t mux, Double_t muy, Double_t sigmax, Double_t sigmay, 
		Double_t tailx, Double_t taily, Double_t binwidthx = 1., Double_t binwidthy = 1.);

	// Integrals of fit functions. The integral and the norm are defined separately for efficiency, since every ll or chi^2
	// call only needs to calculate the norm once.
	static Double_t IntegralGaussian1D(Double_t xMin, Double_t xMax, Double_t mu, Double_t sigma);
	static Double_t NormGaussian1D(Double_t totIntegral) {return totIntegral;}

	static Double_t IntegralGaussian1DTail(Double_t xMin, Double_t xMax, Double_t mu, Double_t sigma, Double_t lambda);
	static Double_t NormGaussian1DTail(Double_t totIntegral, Double_t sigma, Double_t lambda);

	static Double_t IntegralGaussian2D(Double_t xMin, Double_t xMax, Double_t yMin, Double_t yMax,
		Double_t mux, Double_t muy, Double_t sigmax, Double_t sigmay) {return (IntegralGaussian1D(xMin, xMax, mux, sigmax) * IntegralGaussian1D(yMin, yMax, muy, sigmay));}
	static Double_t NormGaussian2D(Double_t totIntegral) {return totIntegral;}

	static Double_t IntegralGaussian2DTailX(Double_t xMin, Double_t xMax, Double_t yMin, Double_t yMax,
		Double_t mux, Double_t muy, Double_t sigmax, Double_t sigmay, Double_t lambdax) {return (IntegralGaussian1DTail(xMin, xMax, mux, sigmax, lambdax) * IntegralGaussian1D(yMin, yMax, muy, sigmay));}
	static Double_t NormGaussian2DTailX(Double_t totIntegral, Double_t sigmax, Double_t lambdax) {return NormGaussian1DTail(totIntegral, sigmax, lambdax);}

	static Double_t IntegralGaussian2DTailY(Double_t xMin, Double_t xMax, Double_t yMin, Double_t yMax,
		Double_t mux, Double_t muy, Double_t sigmax, Double_t sigmay, Double_t lambday) {return (IntegralGaussian1D(xMin, xMax, mux, sigmax) * IntegralGaussian1DTail(yMin, yMax, muy, sigmay, lambday));}
	static Double_t NormGaussian2DTailY(Double_t totIntegral, Double_t sigmay, Double_t lambday) {return NormGaussian1DTail(totIntegral, sigmay, lambday);}

	static Double_t IntegralGaussian2DTailXY(Double_t xMin, Double_t xMax, Double_t yMin, Double_t yMax,
		Double_t mux, Double_t muy, Double_t sigmax, Double_t sigmay, Double_t lambdax, Double_t lambday) {return (IntegralGaussian1DTail(xMin, xMax, mux, sigmax, lambdax) * IntegralGaussian1DTail(yMin, yMax, muy, sigmay, lambday));}
	static Double_t NormGaussian2DTailXY(Double_t totIntegral, Double_t sigmax, Double_t sigmay, Double_t lambdax, Double_t lambday) {return (NormGaussian1DTail(totIntegral, sigmax, lambdax) * NormGaussian1DTail(totIntegral, sigmay, lambday) / totIntegral);}

	// Penalty Functions.
	static Double_t PolyPenalty(Double_t xx, Double_t center, Double_t flatwidth, Int_t polyorder);
	static TCanvas* TestPolyPenalty(Double_t range = 3., Double_t center = 1., Double_t flatwidth = 1., Int_t polyorder = 3);

	// Misc Functions.
	static Double_t GetAbsAvgEta(Double_t mineta, Double_t maxeta);
	static Double_t Eta(Int_t species, Double_t pT, Double_t Y);
	
	// Fits to a constant.
	static void ConstFit(Double_t* data, Double_t* errors, Int_t nDim, Double_t& fit, Double_t& fiterr);

	// PID Expected signal functions.
	static Double_t TOFExpTime(Double_t pT, Double_t eta, Double_t mass);
	static Double_t TPCExpdEdX(Double_t pT, Double_t eta, Double_t mass);

	// Estimated peak positions.
	static Double_t EstimateTOFPosition(Double_t avgPt, Double_t absAvgEta, Double_t assumedMass, Double_t peakMass) {return (TOFExpTime(avgPt, absAvgEta, peakMass) - TOFExpTime(avgPt, absAvgEta, assumedMass));}
	static Double_t EstimateTOFPosition(Double_t avgPt, Double_t absAvgEta, Int_t massAssumption, Int_t peakSpecies) {return EstimateTOFPosition(avgPt, absAvgEta, M(massAssumption), M(peakSpecies));}
        // TPC peak position in the variable delta = dE/dx - <dE/dx>_expected (default analysis)
	static Double_t EstimateTPCPosition(Double_t avgPt, Double_t absAvgEta, Double_t assumedMass, Double_t peakMass) {return (TPCExpdEdX(avgPt, absAvgEta, peakMass) - TPCExpdEdX(avgPt, absAvgEta, assumedMass));}
	static Double_t EstimateTPCPosition(Double_t avgPt, Double_t absAvgEta, Int_t massAssumption, Int_t peakSpecies) {return EstimateTPCPosition(avgPt, absAvgEta, M(massAssumption), M(peakSpecies));}
        // TPC peak position in the ratio variable r = dE/dx / <dE/dx>_expected (default analysis)
	static Double_t EstimateTPCPositionRatio(Double_t avgPt, Double_t absAvgEta, Double_t assumedMass, Double_t peakMass) {return (TPCExpdEdX(avgPt, absAvgEta, peakMass) / TPCExpdEdX(avgPt, absAvgEta, assumedMass));}
	static Double_t EstimateTPCPositionRatio(Double_t avgPt, Double_t absAvgEta, Int_t massAssumption, Int_t peakSpecies) {return EstimateTPCPositionRatio(avgPt, absAvgEta, M(massAssumption), M(peakSpecies));}

	// Standard Functions.
	static Double_t SafeExp(Double_t power) {/*return ((power < -500.) ? TMath::Exp(-500.) : TMath::Exp(power));*/
		if (power < -500.) {return TMath::Exp(-500.);}
		else if (power > 500.) {return TMath::Exp(500.);}
		else {return TMath::Exp(power);}
	}
	static Double_t Exponent(Double_t xx, Int_t sign, Double_t p0, Double_t p1) {return (sign*TMath::Exp(p0 + xx*p1));}
	static Double_t Poly1(Double_t xx, Double_t p0, Double_t p1) {return (p0 + p1*xx);}
	static Double_t Poly2(Double_t xx, Double_t p0, Double_t p1, Double_t p2) {return (p0 + p1*xx + p2*xx*xx);}
	static Double_t Poly3(Double_t xx, Double_t p0, Double_t p1, Double_t p2, Double_t p3) {return (p0 + p1*xx + p2*xx*xx + p3*xx*xx*xx);}
	static Double_t Poly4(Double_t xx, Double_t p0, Double_t p1, Double_t p2, Double_t p3, Double_t p4) {return (p0 + p1*xx + p2*xx*xx + p3*xx*xx*xx + p4*xx*xx*xx*xx);}
	static Double_t Poly5(Double_t xx, Double_t p0, Double_t p1, Double_t p2, Double_t p3, Double_t p4, Double_t p5) {return (p0 + p1*xx + p2*xx*xx + p3*xx*xx*xx + p4*xx*xx*xx*xx + p5*xx*xx*xx*xx*xx);}				

	// Error Propagation.
	static Double_t ErrorRatio(Double_t x1, Double_t e1, Double_t x2, Double_t e2);

};

#endif
