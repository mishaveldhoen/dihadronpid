/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Minimal Track Implementation for DiHadronPID Analysis.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include <iostream>
#include "TString.h"

#include "AliAODTrack.h"
#include "AliAODMCParticle.h"
#include "AliPIDResponse.h"
#include "AliAODEvent.h"

#include "AliLogDiHadronPID.h"
#include "AliMiniTrackDiHadronPID.h"

using namespace std;

Int_t AliMiniTrackDiHadronPID::fNInstances = 0;
Int_t AliMiniTrackDiHadronPID::fNSpecies = 3; // pi/ka/pr/de

ClassImp(AliMiniTrackDiHadronPID);

// -----------------------------------------------------------------------
AliMiniTrackDiHadronPID::AliMiniTrackDiHadronPID(): 
	TObject(),
	fMCInfo(0x0)

{
	
	ResetTrack();
	fNInstances++;

}

// -----------------------------------------------------------------------
AliMiniTrackDiHadronPID::AliMiniTrackDiHadronPID(const AliMiniTrackDiHadronPID& other):
	TObject(other),
	fFlags(other.fFlags),
    fFilterMap(other.fFilterMap),
	fPt(other.fPt),
	fEta(other.fEta),
	fPhi(other.fPhi),
	fDCAxy(other.fDCAxy),
	fDCAz(other.fDCAz),
	fTPCSignalN(other.fTPCSignalN),
	fStartTimeMask(other.fStartTimeMask),
	fHasSPDHit(other.fHasSPDHit),
	fMCInfo(0x0)

{
	for (Int_t iSpecies = 0; iSpecies < fNSpecies; ++iSpecies) {
		fTOFsignalMinusExpected[iSpecies] 	= other.fTOFsignalMinusExpected[iSpecies];
		fTOFRandom[iSpecies]				= other.fTOFRandom[iSpecies];
		fTPCsignalMinusExpected[iSpecies] 	= other.fTPCsignalMinusExpected[iSpecies];
		fTOFnSigma[iSpecies] 				= other.fTOFnSigma[iSpecies];
		fTOFnSigmaRandom[iSpecies]			= other.fTOFnSigmaRandom[iSpecies];
		fTPCnSigma[iSpecies] 				= other.fTPCnSigma[iSpecies];
	}

	if (other.IsMC()) {
		
		// Kinematics.
		SetMCPt(other.MCPt());
		SetMCEta(other.MCEta());
		SetMCY(other.MCY());
		SetMCPhi(other.MCPhi());

		// Flags.
		if (other.IsMCPion()) {SetIsMCPion();}
		if (other.IsMCKaon()) {SetIsMCKaon();}
		if (other.IsMCProton()) {SetIsMCProton();}
		if (other.IsMCDeuterium()) {SetIsMCDeuterium();}
		if (other.IsTOFMatch()) {SetIsTOFMatch();}
		if (other.IsPhysicalPrimary()) {SetIsPhysicalPrimary();}
		if (other.IsSecondaryFromWeakDecay()) {SetIsSecondaryFromWeakDecay();}
		if (other.IsSecondaryFromMaterialDecay()) {SetIsSecondaryFromMaterialDecay();}
	}

	fNInstances++;

}

// -----------------------------------------------------------------------
AliMiniTrackDiHadronPID::~AliMiniTrackDiHadronPID() {

	if (fMCInfo) { delete fMCInfo; fMCInfo = 0x0;}

	fNInstances--;

}

// -----------------------------------------------------------------------
Bool_t AliMiniTrackDiHadronPID::IsValidSpecies(Int_t species) const {

	if ((species >= 0) && (species < fNSpecies)) {return kTRUE;}
	else {return kFALSE;}

}

// -----------------------------------------------------------------------
void AliMiniTrackDiHadronPID::CopyPars(const AliMiniTrackDiHadronPID* miniTrack) {

	// Copy all parameters from another mini track.

	fFlags = miniTrack->GetFlags();
	fFilterMap = miniTrack->GetFilterMap();
	fPt = miniTrack->Pt();
	fEta = miniTrack->Eta();
	fPhi = miniTrack->Phi();
	fDCAxy = miniTrack->GetDCAxy();
	fDCAz = miniTrack->GetDCAz();
	fTPCSignalN = miniTrack->GetTPCSignalN();
	fStartTimeMask = miniTrack->GetStartTimeMask();
	fHasSPDHit = miniTrack->HasSPDHit();
	for (Int_t iSpecies = 0; iSpecies < fNSpecies; ++iSpecies) {
		fTOFsignalMinusExpected[iSpecies] = miniTrack->GetTOFsignalMinusExpected(iSpecies);
		fTOFRandom[iSpecies] = miniTrack->GetTOFRandom(iSpecies);
		fTPCsignalMinusExpected[iSpecies] = miniTrack->GetTPCsignalMinusExpected(iSpecies);
		fTOFnSigma[iSpecies] = miniTrack->GetTOFnSigma(iSpecies);
		fTOFnSigmaRandom[iSpecies] = miniTrack->GetTOFnSigmaRandom(iSpecies);
		fTPCnSigma[iSpecies] = miniTrack->GetTPCnSigma(iSpecies);
	}

	// MC pars.
	if (miniTrack->IsMC()) {

		// Kinematics.
		SetMCPt(miniTrack->MCPt());
		SetMCEta(miniTrack->MCEta());
		SetMCY(miniTrack->MCY());
		SetMCPhi(miniTrack->MCPhi());

		// Flags.
		if (miniTrack->IsMCPion()) {SetIsMCPion();}
		if (miniTrack->IsMCKaon()) {SetIsMCKaon();}
		if (miniTrack->IsMCProton()) {SetIsMCProton();}
		if (miniTrack->IsMCDeuterium()) {SetIsMCDeuterium();}
		if (miniTrack->IsTOFMatch()) {SetIsTOFMatch();}
		if (miniTrack->IsPhysicalPrimary()) {SetIsPhysicalPrimary();}
		if (miniTrack->IsSecondaryFromWeakDecay()) {SetIsSecondaryFromWeakDecay();}
		if (miniTrack->IsSecondaryFromMaterialDecay()) {SetIsSecondaryFromMaterialDecay();}
	}

}

// -----------------------------------------------------------------------
void AliMiniTrackDiHadronPID::CopyPars(AliAODTrack* aodTrack, const AliAODEvent* aodEvent, AliPIDResponse* pidResponse, Bool_t isMC, Bool_t addMismatch, Double_t rndHitTime) {


	// Copy relevant parameters from an AOD track.


	// Copy info about the charge.
	if (aodTrack->Charge() == 1) {SetIsPositive();}
	else if (aodTrack->Charge() == -1) {SetIsNegative();}
	else {AliLogDiHadronPID::Error(Form("Found a track with incorrect charge."), __func__, ClassName());}

	// Check the other flags.
	ULong_t flags = aodTrack->GetFlags();
	if (flags & AliVTrack::kTOFout) {AddFlag(AliMiniTrackDiHadronPID::kTOFout);}
	if (flags & AliVTrack::kTIME) {AddFlag(AliMiniTrackDiHadronPID::kTIME);}
	if (flags & AliVTrack::kTPCout) {AddFlag(AliMiniTrackDiHadronPID::kTPCout);}

	//cout << flags << " " << AliVTrack::kTOFout << " " << AliVTrack::kTIME << " " << AliVTrack::kTPCout << " " << (Int_t)GetFlags() << endl;

	// Copy the filtermap.
	fFilterMap = aodTrack->GetFilterMap();

	// Copy kinematical info.
	fPt = aodTrack->Pt();
	fEta = aodTrack->Eta();
	fPhi = aodTrack->Phi();
	fTPCSignalN = aodTrack->GetTPCsignalN();

	// Copy start time mask.
	fStartTimeMask = pidResponse->GetTOFResponse().GetStartTimeMask(aodTrack->P());

	// Copy SPD hits.
	if (aodTrack->HasPointOnITSLayer(0) || aodTrack->HasPointOnITSLayer(1)) {
		fHasSPDHit = kTRUE;
	}

	// DCA info.
	Double_t posAtDCA[2] = {-999,-999};
    Double_t covar[3] = {-999,-999,-999};
    Bool_t propagate = aodTrack->PropagateToDCA(aodEvent->GetPrimaryVertex(),aodEvent->GetMagneticField(),100.,posAtDCA,covar);
    	
    if (propagate) {
    	fDCAxy = posAtDCA[0];
    	fDCAz = posAtDCA[1];
    }

	AliPID::EParticleType speciesCodes[4] = {AliPID::kPion, AliPID::kKaon, AliPID::kProton, AliPID::kDeuteron};

	// Copy TPC PID info - Note that the info is copied regardless of the flags!
	Double_t tmp = 0.;
	for (Int_t iSpecies = 0; iSpecies < fNSpecies; iSpecies++) {
		
		pidResponse->GetSignalDelta(AliPIDResponse::kTPC, aodTrack, speciesCodes[iSpecies], tmp, kFALSE);
		fTPCsignalMinusExpected[iSpecies] = tmp;
		fTPCnSigma[iSpecies] = pidResponse->NumberOfSigmasTPC(aodTrack, speciesCodes[iSpecies]);
	
	}

	// Copy TOF PID info - Note that the info is copied regarless of the flags!
	for (Int_t iSpecies = 0; iSpecies < fNSpecies; iSpecies++) {
		
		pidResponse->GetSignalDelta(AliPIDResponse::kTOF, aodTrack, speciesCodes[iSpecies], tmp, kFALSE);
		fTOFsignalMinusExpected[iSpecies] = tmp;
		fTOFnSigma[iSpecies] = pidResponse->NumberOfSigmasTOF(aodTrack, speciesCodes[iSpecies]);
	
	}

	// Copy TOF matching status -> This is MCInfo, since it can only be done on reconstructed MC tracks.
	if (isMC) {
		Int_t absTrackLabel = TMath::Abs(aodTrack->GetLabel());
		Int_t tofLabel[3] = {0, 0, 0};
		aodTrack->GetTOFLabel(tofLabel);
		if (tofLabel[0] == absTrackLabel || tofLabel[1] == absTrackLabel || tofLabel[2] == absTrackLabel) {SetIsTOFMatch();}
	}

	// Calculate mismatch times when requested.
	if (addMismatch) {	
		
		Double_t tofRawSignal = aodTrack->GetTOFsignal();
		for (Int_t iSpecies = 0; iSpecies < fNSpecies; ++iSpecies) {
			
			// Subtract the expected time for iSpecies from the random time.
			Double_t tofExpectedSignal = tofRawSignal - fTOFsignalMinusExpected[iSpecies];
			fTOFRandom[iSpecies] = rndHitTime - tofExpectedSignal;

			// Calculate the expected TOF sigma by dividing the signal delta over nSigma.

			Double_t tofExpectedSigma = -999.;
			if (fTOFnSigma[iSpecies] > 10e-30) {tofExpectedSigma = (fTOFsignalMinusExpected[iSpecies] / fTOFnSigma[iSpecies]);}

			// Check expected sigma and calculate mismatch nSigma
			fTOFnSigmaRandom[iSpecies] = -999.;
			if (tofExpectedSigma > 10e-30) {fTOFnSigmaRandom[iSpecies] = (fTOFRandom[iSpecies] / tofExpectedSigma);}

		}

	}

}

// -----------------------------------------------------------------------
void AliMiniTrackDiHadronPID::CopyPars(const AliAODMCParticle* aodMCParticle, const AliAODMCParticle* aodMCMother) {

	// Copy relevant parameters from an AOD MC particle.

	// Copy charge.
	if (aodMCParticle->Charge() > 0) {SetIsPositive();}
	else if (aodMCParticle->Charge() < 0) {SetIsNegative();}
	else {AliLogDiHadronPID::Error(Form("Found a neutral particle..."), __func__, ClassName());}

	// Copy kinematic variables.
	SetMCPt(aodMCParticle->Pt());
	SetMCEta(aodMCParticle->Eta());
	SetMCY(aodMCParticle->Y());
	SetMCPhi(aodMCParticle->Phi());

	// Copy MC Truth.
	switch (aodMCParticle->GetPdgCode()) {
		case -211:
		case 211: {SetIsMCPion(); break;}
		case -321:
		case 321: {SetIsMCKaon(); break;}
		case -2212:
		case 2212: {SetIsMCProton(); break;}
		case -1000010020:
		case 1000010020: {SetIsMCDeuterium(); break;}
	}

	// Copy primary, secondary info.
	if (aodMCParticle->IsPhysicalPrimary()) {

		SetIsPhysicalPrimary();

	} else {
		
		if (aodMCMother) {

			Int_t motherPDG = -999;
			Int_t firstInt = -999; 

			motherPDG = TMath::Abs(aodMCMother->GetPdgCode());
			firstInt = Int_t (motherPDG/ TMath::Power(10, Int_t(TMath::Log10(motherPDG))));
			
			if ( firstInt == 3) {
				SetIsSecondaryFromWeakDecay();
			} else {
				SetIsSecondaryFromMaterialDecay();
			}

		}

	}

}

// -----------------------------------------------------------------------
Bool_t AliMiniTrackDiHadronPID::IsVerySimilarTo(const AliMiniTrackDiHadronPID* other) const {

	// While this is not a water-proof method, it is quite unlikely 
	// that two tracks are this similar, and not the same track.
	const Double_t eps = 10e-10;
	if (fFlags != other->GetFlags()) {return kFALSE;}
	if (fPt > (other->Pt() + eps) || fPt < (other->Pt() - eps)) {return kFALSE;}
	if (fEta > (other->Eta() + eps) || fEta < (other->Eta() - eps)) {return kFALSE;}
	if (fPhi > (other->Phi() + eps) || fPhi < (other->Phi() - eps)) {return kFALSE;}

	for (Int_t iSpecies = 0; iSpecies < fNSpecies; ++iSpecies) {
		if (fTOFsignalMinusExpected[iSpecies] > (other->GetTOFsignalMinusExpected(iSpecies) + eps) || fTOFsignalMinusExpected[iSpecies] < (other->GetTOFsignalMinusExpected(iSpecies) - eps)) {return kFALSE;}
		if (fTPCsignalMinusExpected[iSpecies] > (other->GetTPCsignalMinusExpected(iSpecies) + eps) || fTPCsignalMinusExpected[iSpecies] < (other->GetTPCsignalMinusExpected(iSpecies) - eps)) {return kFALSE;}
	}

	return kTRUE;

}

// -----------------------------------------------------------------------
void AliMiniTrackDiHadronPID::ResetTrack() {

	fFlags = 0;
	fFilterMap = 0;
	fPt = -999.;
	fEta = -999.;
	fPhi = -999.;
	fDCAxy = -999.;
	fDCAz = -999.;
	fTPCSignalN = -1;
	fStartTimeMask = -1;
	fHasSPDHit = kFALSE;
	if (fMCInfo) {delete fMCInfo;}
	fMCInfo = 0x0;

	for (Int_t iSpecies = 0; iSpecies < fNSpecies; ++iSpecies) {
		fTOFsignalMinusExpected[iSpecies] = -999.;
		fTOFRandom[iSpecies] = -999.;
		fTPCsignalMinusExpected[iSpecies] = -999.;
		fTOFnSigma[iSpecies] = -999.;
		fTOFnSigmaRandom[iSpecies] = -999.;
		fTPCnSigma[iSpecies] = -999.;
	}

}

// -----------------------------------------------------------------------
void AliMiniTrackDiHadronPID::PrintTrack() const {

	const char* speciesNames[4] = {"pi", "ka", "pr", "de"};

	cout << "----------------------------" << endl;
	cout << " Track Info" << endl;
	cout << "----------------------------" << endl;
	cout << " Pt: " << fPt << endl;
	cout << " Eta: " << fEta << endl;
	cout << " Phi: " << fPhi << endl;
	cout << " DCAxy: " << fDCAxy << endl;
	cout << " DCAz: " << fDCAz << endl;
	for (Int_t iSpecies = 0; iSpecies < fNSpecies; iSpecies++) {
		cout << Form(" TOF %s: ", speciesNames[iSpecies]) << fTOFsignalMinusExpected[iSpecies] << endl; 
	}
	for (Int_t iSpecies = 0; iSpecies < fNSpecies; iSpecies++) {
		cout << Form(" TPC %s: ", speciesNames[iSpecies]) << fTPCsignalMinusExpected[iSpecies] << endl; 
	}
	cout << " N clusters for dE/dx determintion: " << fTPCSignalN << endl;
	cout << " Start time mask: " << (Int_t)fStartTimeMask << endl;
	cout << " Has SPD hit: " << ((HasSPDHit()) ? "YES" : "NO") << endl;
	cout << " Charge: " << (IsPositive() ? "Positive" : "Negative") << endl; 
	cout << " Track flags: " << (IsFlagSet(kTOFout) ? "kTOFout " : "") 
		<< (IsFlagSet(kTIME) ? "kTIME " : "")
		<< (IsFlagSet(kTPCout) ? "kTPCout " : "") << endl;
	cout << endl;
    cout << " Filter Bits Passed: ";
    for (Int_t iFilterBit = 1; iFilterBit < 12; iFilterBit++) {
        if (fFilterMap & (1 << iFilterBit)) {cout << iFilterBit << " ";}
    }
    cout << endl;
	cout << " IsMC: "<< (IsMC() ? "YES" : "NO") << endl;
	if (IsMC()) {
		cout << " MCPt: " << MCPt() << endl;
		cout << " MCEta: " << MCEta() << endl;
		cout << " MCY: " << MCY() << endl;
		cout << " MCPhi: " << MCPhi() << endl;
		cout << endl;
		cout << " IsMCPion: " << (IsMCPion() ? "YES" : "NO") << endl; 
		cout << " IsMCKaon: " << (IsMCKaon() ? "YES" : "NO") << endl; 
		cout << " IsMCProton: " << (IsMCProton() ? "YES" : "NO") << endl; 
		cout << " IsMCDeuterium: " << (IsMCDeuterium() ? "YES" : "NO") << endl; 
		cout << " IsTOFMatch: " << (IsTOFMatch() ? "YES" : "NO") << endl; 
		cout << " IsPhysicalPrimary: " << (IsPhysicalPrimary() ? "YES" : "NO") << endl; 
		cout << " IsSecondaryFromWeakDecay: " << (IsSecondaryFromWeakDecay() ? "YES" : "NO") << endl; 
		cout << " IsSecondaryFromMaterialDecay: " << (IsSecondaryFromMaterialDecay() ? "YES" : "NO") << endl; 
	}
	cout << "----------------------------" << endl;
	cout << endl;
}
