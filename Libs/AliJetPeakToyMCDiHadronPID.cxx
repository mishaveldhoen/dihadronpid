/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Class description
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "TRandom.h"
#include "TFile.h"
#include "TStopwatch.h"
#include "TH2F.h"
#include "TH1F.h"

#include "AliJetPeakToyMCDiHadronPID.h"
#include "AliSettingsDiHadronPID.h"
#include "AliMiscToolsDiHadronPID.h"
#include "AliJetPeakToyMCDiHadronPID.h"
#include "AliHistToolsDiHadronPID.h"
#include "AliAssociatedYieldDiHadronPID.h"
#include "AliLogDiHadronPID.h"

ClassImp(AliJetPeakToyMCDiHadronPID);

// -----------------------------------------------------------------------
AliJetPeakToyMCDiHadronPID::AliJetPeakToyMCDiHadronPID():
	TObject(),
	fOutputDirectory("."),
	fFileSuffix(""),
	fNRepetitions(2000),
    fNTriggers(1.),
	fBgdDensity(10e4),
	fJetYield(5000.),
	fJetSTD(0.1),
	fPeakAlpha(3.0),	
	fBackgroundCircle(1.0),
	fMaxDEta(1.4),
	fFlowCoefficients(8),
	fMinFlowCoefficientSubtracted(1),
	fMaxFlowCoefficientSubtracted(4),
	fUseWideJet(kFALSE),
	fWideJetSTD(-1.),
	fWideJetYieldPct(0.)	

{
	
	/** 
	 * Default Constructor
	 */
	AliLogDiHadronPID::Debug2("Default constructor called.", __func__, ClassName());

}

// -----------------------------------------------------------------------
void AliJetPeakToyMCDiHadronPID::RunSimulation() {

	/**
	 * Run the simulation given the current parameters. The implementation
	 * of this method is basically the same as the original test macro,
	 * Libs/TestMacros/TestAssociatedYield.C
	 *
	 * \todo Cleanup this method.
	 */

	TH1::AddDirectory(0);

	Double_t peakRadiusSignal = fPeakAlpha * fJetSTD;
	Double_t peakRadiusBgd = fPeakAlpha * fJetSTD;

	gRandom->SetSeed(0);

	// Start the timer.
	TStopwatch sw;
	sw.Start();

	// Create and configure an instance of the logger.
	AliLogDiHadronPID* logger = AliLogDiHadronPID::Instance();
	logger->SetVerboseLevel(1);
	logger->SetDebugLevel(0);

	// Create and configure an instance of the analysis settings, if it doesn't exist yet.
	AliSettingsDiHadronPID* settings = AliSettingsDiHadronPID::Instance(0);	// Bug: this will give an [ERROR] from AliSettings, which can be ignored.
	if (!settings) {
		settings = AliSettingsDiHadronPID::Instance(1);
		settings->SetBeamType(0);
	}
	
	settings->SetDEtaAxis(32, -1.6, 1.6);
	settings->SetDPhiAxis(32);

	// Histograms testing what happens if we repeat the measurement a large number of times.
    TVectorF vNSYieldCounts(fNRepetitions);
    TVectorF vNSYieldDEtaGaussFit(fNRepetitions);

    TVectorF vNSYieldCountsOverExp(fNRepetitions);
    TVectorF vNSYieldDEtaGaussFitOverExp(fNRepetitions);

    TVectorF vNSYieldCountsVsSigma(fNRepetitions);
    TVectorF vNSYieldDEtaGaussFitVsSigma(fNRepetitions);

    TVectorF vSumFlowCoefficients(fNRepetitions);
    TVectorF vDCTBgdYieldMinusTrueBgdYield(fNRepetitions);
    TVectorF vDCTBgdYieldOverTrueBgdYield(fNRepetitions);

    Double_t flowPadding = 0.1 * (fFlowCoefficients.Max() - fFlowCoefficients.Min()); // Histogram range is min...max with 10% padding.
    if (flowPadding < 1.e-4) {flowPadding = 1.e-4;} 
	TH2F* hFlowCoefficientsDCT = new TH2F("hFlowCoefficientsDCT", "hFlowCoefficientsDCT;n;V_{n#Delta}", 8, 0.5, 0.5+fFlowCoefficients.GetNoElements(), 100, fFlowCoefficients.Min() - flowPadding, fFlowCoefficients.Max() + flowPadding);
	TH2F* hFlowCoefficientsDCTSubtracted = new TH2F("hFlowCoefficientsDCTSubtracted", "hFlowCoefficientsDCTSubtracted;n;V_{n#Delta}", 8, 0.5, 0.5+fFlowCoefficients.GetNoElements(), 100, fFlowCoefficients.Min() - flowPadding, fFlowCoefficients.Max() + flowPadding);

	TH2F* hAvgDCTDVsDEta = new TH2F("hAvgDCTDVsDEta", "hAvgDCTDVsDEta;n;#Delta#eta;#braket{V_{n#Delta}}", 8, 0.5, 0.5+fFlowCoefficients.GetNoElements(), 
		settings->GetDEtaAxis()->GetNbins(), settings->GetDEtaAxis()->GetXmin(), settings->GetDEtaAxis()->GetXmax());

	// Create an empty 2D histogram, and AliAssociatedYieldDiHadronPID instance (uses copy constructor, so both objects are independent!)
	TH2F* signalHist = 0x0;
	TH2F* bgdHist = 0x0;
	AliAssociatedYieldDiHadronPID* testAssociatedYield = 0x0;

	TH2F* firstSignalHistogram = 0x0;
	TH2F* firstBackgroundHistogram  = 0x0;
	TH2F* firstTotalHistogram = 0x0;

	for (Int_t iRep = 0; iRep < fNRepetitions; iRep++) {

		if (!(iRep % 10)) {cout << "Repetition: " << iRep << endl;}

		// TODO: Would be nicer if AliAssociatedYieldDiHadronPID had a constructor (name, title, xAxis, yAxis).
		// Unfortunately this is not so easy, as TH2F does not have such a constructor either.
		signalHist = (TH2F*)AliHistToolsDiHadronPID::MakeHist("signalHist", "signalHist", "F", settings->GetDPhiAxis(), settings->GetDEtaAxis());
		signalHist->SetZTitle("#frac{1}{N_{trig}} #frac{d^{2}N}{d#Delta#phi#Delta#eta}");
		bgdHist = (TH2F*)AliHistToolsDiHadronPID::MakeHist("bgdHist", "bgdHist", "F", settings->GetDPhiAxis(), settings->GetDEtaAxis());	
		bgdHist->SetZTitle("#frac{1}{N_{trig}} #frac{d^{2}N}{d#Delta#phi#Delta#eta}");

		testAssociatedYield = new AliAssociatedYieldDiHadronPID(*signalHist);

		// Fill the associated yield randomly.
		testAssociatedYield->FillRandom(fNTriggers, fBgdDensity, fJetYield, fJetSTD, fFlowCoefficients.GetNoElements(), fFlowCoefficients.GetMatrixArray(), signalHist, bgdHist);

		// Determine the number of signal hits (should be found by the analysis!)
		Double_t nsTrueIntegral = signalHist->Integral("width");
		// cout << nsTrueIntegral << endl;
		Double_t bgdTrueIntegralPeakRegion = 0.;
		Double_t bgdTrueIntegralPeakRegionError = 0.;
		AliHistToolsDiHadronPID::CountInEllips(bgdHist, peakRadiusSignal, peakRadiusSignal, bgdTrueIntegralPeakRegion, bgdTrueIntegralPeakRegionError, kTRUE);

		// Perform the analysis.
		testAssociatedYield->PerformDPhiAnalysis(fMaxDEta, peakRadiusBgd, fMinFlowCoefficientSubtracted, fMaxFlowCoefficientSubtracted, kFALSE);
		testAssociatedYield->PerformDEtaAnalysis(peakRadiusSignal);

		Double_t yield = 0.;
		Double_t error = 0.;
		Double_t bgdYieldDCT[2] = {0., 0.};

		// Measure peak yield in two ways.
		testAssociatedYield->GetNSJetYield(yield, error, peakRadiusSignal, bgdYieldDCT);

		vNSYieldCounts[iRep] = yield;
		vNSYieldCountsOverExp[iRep] = yield/nsTrueIntegral;
		if (error < 10e-9) {cout << "TestAssociatedYield: WARNING: NSJetYield error is very small!" << endl; error = 10e-9;}
		vNSYieldCountsVsSigma[iRep] = ((yield - nsTrueIntegral) / error);

		testAssociatedYield->GetNSJetYieldDEtaFit(yield, error);
		vNSYieldDEtaGaussFit[iRep] = yield;
		vNSYieldDEtaGaussFitOverExp[iRep] = yield/nsTrueIntegral;
		if (error < 10e-9) {cout << "TestAssociatedYield: WARNING: NSJetYieldDEtaFit error is very small! Fit probably failed." << endl; error = 10e-9;}
		vNSYieldDEtaGaussFitVsSigma[iRep] = ((yield - nsTrueIntegral) / error);

		// Fill histogram DCT background minus true background.
		vDCTBgdYieldMinusTrueBgdYield[iRep] = (bgdYieldDCT[0] - bgdTrueIntegralPeakRegion);
		vDCTBgdYieldOverTrueBgdYield[iRep] = (bgdYieldDCT[0] / bgdTrueIntegralPeakRegion);

		// Fill histogram VnD(n).
		for (Int_t iFlowCoefficient = 1; iFlowCoefficient <= fFlowCoefficients.GetNoElements(); iFlowCoefficient++) {
				
			Double_t flowCoefficient = 0.;
			Double_t flowCoefficientError = 0.;

			testAssociatedYield->GetFlowCoefficient(iFlowCoefficient, flowCoefficient, flowCoefficientError);
			hFlowCoefficientsDCT->Fill(iFlowCoefficient, flowCoefficient);

			if (iFlowCoefficient <= fFlowCoefficients.GetNoElements()) {
				hFlowCoefficientsDCTSubtracted->Fill(iFlowCoefficient, flowCoefficient - fFlowCoefficients[iFlowCoefficient-1]);
			} else {
				hFlowCoefficientsDCTSubtracted->Fill(iFlowCoefficient, flowCoefficient);
			}

		}

		// Fill histogram N(sum VnD).
		Double_t sumFlowCoefficientsSubtracted = 0.;
		for (Int_t iFlowCoefficient = fMinFlowCoefficientSubtracted; iFlowCoefficient <= fMaxFlowCoefficientSubtracted; iFlowCoefficient++) {
			Double_t flowCoefficient = 0.;
			Double_t flowCoefficientError = 0.;		
			testAssociatedYield->GetFlowCoefficient(iFlowCoefficient, flowCoefficient, flowCoefficientError);
			sumFlowCoefficientsSubtracted += flowCoefficient;
		}
		vSumFlowCoefficients[iRep] = sumFlowCoefficientsSubtracted;

		// Fill histogram <VnD>(n,DEta).	
		for (Int_t iBinDEta = 1; iBinDEta <= bgdHist->GetNbinsY(); iBinDEta++) {
			
			// Get projection of one DEta bin.
			TH1F* projectionOneDEtaBin = (TH1F*)bgdHist->ProjectionX("projectionOneDEtaBin", iBinDEta, iBinDEta);

			// Symmetrize
			TH1F* projectionOneDEtaBinSymm = AliHistToolsDiHadronPID::CreateSymmetrisedHist("projectionOneDEtaBinSymm", projectionOneDEtaBin);

			// Create a DCT.
			TH1F* dctOneDEtaBin = AliHistToolsDiHadronPID::DCT("dctOneDEtaBin", projectionOneDEtaBinSymm);

			// Fill the histogram. 
			for (Int_t nn = 1; nn <= fFlowCoefficients.GetNoElements(); nn++) {
				hAvgDCTDVsDEta->Fill(nn, bgdHist->GetYaxis()->GetBinCenter(iBinDEta), dctOneDEtaBin->GetBinContent(nn + 1) / dctOneDEtaBin->GetBinContent(1));
			}

			delete projectionOneDEtaBin;
			delete projectionOneDEtaBinSymm;
			delete dctOneDEtaBin;

		}

		// On the first repetition, keep a copy of the random associated yield distribution.
		if (iRep == 0) {

			firstTotalHistogram = new TH2F(*(TH2F*)testAssociatedYield);
			firstSignalHistogram = new TH2F(*signalHist);
			firstBackgroundHistogram = new TH2F(*bgdHist);

			// Make Additional Histograms.
			TCanvas* cvs = TCanvas::MakeDefCanvas();
			testAssociatedYield->Draw("lego2");	
			cvs->SaveAs(Form("%s/toyMC_AssocDistribution%s.pdf", fOutputDirectory.Data(), fFileSuffix.Data()));
			delete cvs;

			// Save the fourier transformation (plus error band) as a pdf.
			cvs = testAssociatedYield->PlotBackgroundDPhiErrorBand();	
			cvs->SaveAs(Form("%s/toyMC_DPhiDistributionDCTAndError%s.pdf", fOutputDirectory.Data(), fFileSuffix.Data()));
			delete cvs;				

			// Save the fourier transformation (decomposed) as a pdf.
			cvs = testAssociatedYield->PlotBackgroundDPhiDecomposed();	
			cvs->SaveAs(Form("%s/toyMC_DPhiDistributionDCTDecomposed%s.pdf", fOutputDirectory.Data(), fFileSuffix.Data()));
			delete cvs;			

			// Save the DEta peak (SG fit).
			cvs = testAssociatedYield->PlotDEtaFit();	
			cvs->SaveAs(Form("%s/toyMC_DEtaFitGauss%s.pdf", fOutputDirectory.Data(), fFileSuffix.Data()));
			delete cvs;	

			// Save the DEta peak (DG fit).
			cvs = testAssociatedYield->PlotDEtaFitDG();	
			cvs->SaveAs(Form("%s/toyMC_DEtaFitDoubleGauss%s.pdf", fOutputDirectory.Data(), fFileSuffix.Data()));
			delete cvs;

			// Save the DPhi peak width plot.
			cvs = testAssociatedYield->PlotDPhiFit();	
			cvs->SaveAs(Form("%s/toyMC_DPhiFitGauss%s.pdf", fOutputDirectory.Data(), fFileSuffix.Data()));
			delete cvs;			

			// Save the Flow in a plot.
			cvs = TCanvas::MakeDefCanvas();			
			testAssociatedYield->GetCloneDCTCoefficients()->Draw();	
			cvs->SaveAs(Form("%s/toyMC_VnDelta%s.pdf", fOutputDirectory.Data(), fFileSuffix.Data()));
			delete cvs;	

		}

		delete signalHist;
		delete bgdHist;
		delete testAssociatedYield;

	}

    // Use measurement vectors to create histograms and fill them.
	TH1F* hNSYieldCounts = AliHistToolsDiHadronPID::MakeHistFromVector("hNSYieldCounts", "hNSYieldCounts;N", vNSYieldCounts, 5, 100);
	TH1F* hNSYieldDEtaGaussFit = AliHistToolsDiHadronPID::MakeHistFromVector("hNSYieldDEtaGaussFit", "hNSYieldDEtaGaussFit;N", vNSYieldDEtaGaussFit, 5, 100);

	TH1F* hNSYieldCountsOverExp = AliHistToolsDiHadronPID::MakeHistFromVector("hNSYieldCountsOverExp", "hNSYieldCountsOverExp;N/N_{true}", vNSYieldCountsOverExp, 5, 100);
	TH1F* hNSYieldDEtaGaussFitOverExp = AliHistToolsDiHadronPID::MakeHistFromVector("hNSYieldDEtaGaussFitOverExp", "hNSYieldDEtaGaussFitOverExp;N/N_{true}", vNSYieldDEtaGaussFitOverExp, 5, 100);

	TH1F* hNSYieldCountsVsSigma = AliHistToolsDiHadronPID::MakeHistFromVector("hNSYieldCountsVsSigma", "hNSYieldCountsVsSigma;(N-N_{true})/#sigma", vNSYieldCountsVsSigma, 5, 100);
	TH1F* hNSYieldDEtaGaussFitVsSigma = AliHistToolsDiHadronPID::MakeHistFromVector("hNSYieldDEtaGaussFitVsSigma", "hNSYieldDEtaGaussFitVsSigma;(N-N_{true})/#sigma", vNSYieldDEtaGaussFitVsSigma, 5, 100);

	TH1F* hDCTBgdYieldMinusTrueBgdYield = AliHistToolsDiHadronPID::MakeHistFromVector("hDCTBgdYieldMinusTrueBgdYield", "hDCTBgdYieldMinusTrueBgdYield;D-D_{True}", vDCTBgdYieldMinusTrueBgdYield, 5, 100);
	TH1F* hDCTBgdYieldOverTrueBgdYield = AliHistToolsDiHadronPID::MakeHistFromVector("hDCTBgdYieldOverTrueBgdYield", "hDCTBgdYieldOverTrueBgdYield;D/D_{True}", vDCTBgdYieldOverTrueBgdYield, 5, 100);

	TH1F* hSumFlowCoefficients = AliHistToolsDiHadronPID::MakeHistFromVector("hSumFlowCoefficients", "hSumFlowCoefficients;#sum_nV_{n#Delta};N", vSumFlowCoefficients, 5, 100);

	// Perform fits on the collected measurements, as an alternative way to determine mean and sigma.
	cout << endl << "Fitting: NS Yield By Counting" << endl;
	hNSYieldCounts->Fit("gaus");
	cout << endl << "Fitting: NS Yield By Counting minus Truth" << endl;
	hNSYieldCountsOverExp->Fit("gaus");
	cout << endl << "Fitting: NS Yield By Counting minus Truth over Sigma" << endl;	
	hNSYieldCountsVsSigma->Fit("gaus");
	cout << endl << "Fitting: NS Yield By Fitting DEta" << endl;	
	hNSYieldDEtaGaussFit->Fit("gaus");
	cout << endl << "Fitting: NS Yield By Fitting DEta minus Truth" << endl;	
	hNSYieldDEtaGaussFitOverExp->Fit("gaus");
	cout << endl << "Fitting: NS Yield By Fitting DEta minus Truth over Sigma" << endl;		
	hNSYieldDEtaGaussFitVsSigma->Fit("gaus");
	cout << endl << "Fitting: Sum Flow Coefficients" << endl;		
	hSumFlowCoefficients->Fit("gaus");	
	cout << endl << "Fitting: DCT bgd yield minus true bgd yield." << endl;		
	hDCTBgdYieldMinusTrueBgdYield->Fit("gaus");	
	cout << endl << "Fitting: DCT bgd yield minus true bgd yield." << endl;		
	hDCTBgdYieldOverTrueBgdYield->Fit("gaus");	

	// Store result to file.
	TFile* fOut = TFile::Open(Form("%s/toyMC%s.root", fOutputDirectory.Data(), fFileSuffix.Data()), "recreate");
	hNSYieldCounts->Write();
	delete hNSYieldCounts;
	hNSYieldCountsOverExp->Write();
	delete hNSYieldCountsOverExp;
	hNSYieldCountsVsSigma->Write();
	delete  hNSYieldCountsVsSigma;
	hNSYieldDEtaGaussFit->Write();
	delete hNSYieldDEtaGaussFit;
	hNSYieldDEtaGaussFitOverExp->Write();
	delete hNSYieldDEtaGaussFitOverExp;
	hNSYieldDEtaGaussFitVsSigma->Write();
	delete hNSYieldDEtaGaussFitVsSigma;
	
	hFlowCoefficientsDCT->Write();
	delete hFlowCoefficientsDCT;
	hFlowCoefficientsDCTSubtracted->Write();
	delete hFlowCoefficientsDCTSubtracted;
	hSumFlowCoefficients->Write();
	delete hSumFlowCoefficients;
	hDCTBgdYieldMinusTrueBgdYield->Write();
	delete hDCTBgdYieldMinusTrueBgdYield;
	hDCTBgdYieldOverTrueBgdYield->Write();
	delete hDCTBgdYieldOverTrueBgdYield;

	hAvgDCTDVsDEta->Scale(1./fNRepetitions); // Average over the number of repetitions.
	hAvgDCTDVsDEta->Write();
	delete hAvgDCTDVsDEta;
	
	firstTotalHistogram->Write();
	delete firstTotalHistogram;
	firstSignalHistogram->Write();
	delete firstSignalHistogram;
	firstBackgroundHistogram->Write();
	delete firstBackgroundHistogram;

    // Store also the original vectors.
	vNSYieldCounts.Write("vNSYieldCounts");
	vNSYieldCountsOverExp.Write("vNSYieldCountsOverExp");
	vNSYieldCountsVsSigma.Write("vNSYieldCountsVsSigma");
	vNSYieldDEtaGaussFit.Write("vNSYieldDEtaGaussFit");
	vNSYieldDEtaGaussFitOverExp.Write("vNSYieldDEtaGaussFitOverExp");
	vNSYieldDEtaGaussFitVsSigma.Write("vNSYieldDEtaGaussFitVsSigma");
	vSumFlowCoefficients.Write("vSumFlowCoefficients");
	vDCTBgdYieldMinusTrueBgdYield.Write("vDCTBgdYieldMinusTrueBgdYield");
	vDCTBgdYieldOverTrueBgdYield.Write("vDCTBgdYieldOverTrueBgdYield");

	fOut->Close();

	// Show Analysis time.
	cout << "Analysis time: " << AliMiscToolsDiHadronPID::FormatTime(sw.RealTime()).Data() << endl;	

}

// -----------------------------------------------------------------------
void AliJetPeakToyMCDiHadronPID::SetOutputDirectory(const char* outputDir) {

	fOutputDirectory = outputDir;

}

// -----------------------------------------------------------------------
void AliJetPeakToyMCDiHadronPID::SetOutputFileSuffix(const char* fileSuffix) {

	fFileSuffix = fileSuffix;

}

// -----------------------------------------------------------------------
void AliJetPeakToyMCDiHadronPID::SetNRepetitions(Int_t nRepetitions) {

	fNRepetitions = nRepetitions;

}

// -----------------------------------------------------------------------
void AliJetPeakToyMCDiHadronPID::SetNTriggers(Double_t nTriggers) {

	fNTriggers = nTriggers;

}

// -----------------------------------------------------------------------
void AliJetPeakToyMCDiHadronPID::SetBgdDensity(Double_t bgdDensity) {

	fBgdDensity = bgdDensity;

}

// -----------------------------------------------------------------------
void AliJetPeakToyMCDiHadronPID::SetJetYield(Double_t jetYield) {

	fJetYield = jetYield;

}

// -----------------------------------------------------------------------
void AliJetPeakToyMCDiHadronPID::SetJetSTD(Double_t jetSTD) {

	fJetSTD = jetSTD;

}

// -----------------------------------------------------------------------
void AliJetPeakToyMCDiHadronPID::SetPeakAlpha(Double_t alpha) {

	fPeakAlpha = alpha;

}

// -----------------------------------------------------------------------
void AliJetPeakToyMCDiHadronPID::SetBackgroundCircle(Double_t backgroundCircle) {

	fBackgroundCircle = backgroundCircle;

}

// -----------------------------------------------------------------------
void AliJetPeakToyMCDiHadronPID::SetMaxDEta(Double_t maxDEta) {

	fMaxDEta = maxDEta;

}

// -----------------------------------------------------------------------
void AliJetPeakToyMCDiHadronPID::SetFlowCoefficient(Int_t n, Double_t value) {

	if (n < 1) {
		AliLogDiHadronPID::Error("Minimum flow coefficient is 1.", __func__, ClassName());
		return;
	}

	if (fFlowCoefficients.GetNoElements() < n) {
		fFlowCoefficients.ResizeTo(n);
	}

	fFlowCoefficients[n - 1] = value;
}

// -----------------------------------------------------------------------
void AliJetPeakToyMCDiHadronPID::SetMinMaxFlowCoefficientsSubtracted(Int_t min, Int_t max) {

	/**
	 * The coefficients \f$V_{n\Delta}\f$ are used as background, for \f$min < n < max\f$.
	 */

	fMinFlowCoefficientSubtracted = min;
	fMaxFlowCoefficientSubtracted = max;

}

// -----------------------------------------------------------------------
void AliJetPeakToyMCDiHadronPID::SetUseWideJet(Bool_t useWideJet) {

	fUseWideJet = useWideJet;

}

// -----------------------------------------------------------------------
void AliJetPeakToyMCDiHadronPID::SetWideJetSTD(Double_t wideJetSTD) {

	fWideJetSTD = wideJetSTD;

}

// -----------------------------------------------------------------------
void AliJetPeakToyMCDiHadronPID::SetWideJetYieldPct(Double_t wideJetYieldPct) {

	fWideJetYieldPct = wideJetYieldPct;

}

// -----------------------------------------------------------------------
void AliJetPeakToyMCDiHadronPID::Print(Option_t*) const {
	
	// Print info about the class.
	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	cout << "--------------------------------------------------" << endl;
	cout << " Jet Peak Measurement ToyMC" << endl;
	cout << "--------------------------------------------------" << endl;
	cout << " Output Directory: " << fOutputDirectory.Data() << endl;
	cout << " Output file Suffix: " << fFileSuffix.Data() << endl;
	cout << " Number of Reps: " << fNRepetitions << endl;
	cout << endl;
	cout << " Target Background Density (per trigger): " << fBgdDensity << endl;
	cout << " Target Jet-Peak Yield: " << fJetYield << endl;	
	cout << " Model Flow Coefficients: ";
	for (Int_t iFlowCoefficient = 0; iFlowCoefficient < fFlowCoefficients.GetNoElements(); iFlowCoefficient++) {
		cout << fFlowCoefficients[iFlowCoefficient] << " ";	
	}
	cout << endl << endl;
	cout << " Jet-Peak Sigma: " << fJetSTD << endl;
	cout << " Peak Region (no. Sigma): " << fPeakAlpha << endl;
	cout << endl;
	cout << " VnD subtracted as background: " << fMinFlowCoefficientSubtracted << " <= n <= " << fMaxFlowCoefficientSubtracted << endl;
	cout << endl;
	cout << " Use wide Gauss under regular jet (DG): " << (fUseWideJet ? "YES" : "NO") << endl;
	cout << " Wide Gauss Sigma: " << fWideJetSTD << endl;
	cout << " Pct of yield in Wide Jet: " << fWideJetYieldPct << endl;
	cout << "--------------------------------------------------" << endl;
	cout << endl;

}

// -----------------------------------------------------------------------
TString AliJetPeakToyMCDiHadronPID::GetOutputFileNameAndPath() const {

    return TString::Format("%s/toyMC%s.root", fOutputDirectory.Data(), fFileSuffix.Data());
}
