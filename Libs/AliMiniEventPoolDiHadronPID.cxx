/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  This class stores triggers that can be used later to correlate to 
//  uncorrelated events. The design is as follows:
//   - Store only whole events, i.e., if the maximum pool size is reached,
//     then remove the first event in the pool completely.
//   - Store events as a function of centrality and vtxz.
//  It is important to have the ratio Npairs,same/Npairs,mixed to be the 
//  same for all bins. By making sure that all bins have the same number
//  of events before we start mixing, this should on avarage work out
//  correctly.
//		The user should excplicitly check if the minimum number requested
//  does not exceed the total number of events in the data set of one
//  type (cent, vtxz).
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "AliMiniEventPoolDiHadronPID.h"
#include "AliLogDiHadronPID.h"

ClassImp(AliMiniEventPoolDiHadronPID);

// -----------------------------------------------------------------------
AliMiniEventPoolDiHadronPID::AliMiniEventPoolDiHadronPID():
	TObject(),
	fSettings(AliSettingsDiHadronPID::Instance()),
	fIsReady(kFALSE),
	fCentralityAxis(fSettings ? (TAxis*)(fSettings->GetCentralityAxis())->Clone() : 0x0),
	fNCentralityBins(fSettings ? fSettings->GetCentralityAxis()->GetNbins() : -1),
	fVtxZAxis(fSettings ? (TAxis*)(fSettings->GetVtxZAxis())->Clone() : 0x0),	
	fNVtxZBins(fSettings ? fSettings->GetVtxZAxis()->GetNbins() : -1),
	fOutgoingEventFrequency(0x0)

{
	
	// Default Constructor
	AliLogDiHadronPID::Debug2("Default constructor called.", __func__, ClassName());

	// Error message when settings singleton is not found!
	if (!fSettings) {AliLogDiHadronPID::Error(Form("Settings Singleton not found!"), __func__, ClassName());}

	// Initialise the pool.
	fCounter = new Int_t*[fNCentralityBins];
	fPool = new TObjArray**[fNCentralityBins];
	fPoolEventNumbers = new Int_t**[fNCentralityBins];

	for (Int_t iCent = 0; iCent < fNCentralityBins; iCent++) {

		fCounter[iCent] = new Int_t[fNVtxZBins];
		fPool[iCent] = new TObjArray*[fNVtxZBins];
		fPoolEventNumbers[iCent] = new Int_t*[fNVtxZBins];

		for (Int_t iVtxZ = 0; iVtxZ < fNVtxZBins; iVtxZ++) {
					
			fCounter[iCent][iVtxZ] = 0;
			fPool[iCent][iVtxZ] = new TObjArray(fSettings->GetNTriggersToMix());
			fPool[iCent][iVtxZ]->SetOwner(kTRUE);

			fPoolEventNumbers[iCent][iVtxZ] = new Int_t[fSettings->GetNTriggersToMix()];
		
		}	
	}
}

// -----------------------------------------------------------------------
AliMiniEventPoolDiHadronPID::~AliMiniEventPoolDiHadronPID() {
	
	// Destructor
	AliLogDiHadronPID::Debug2("Destructor called.", __func__, ClassName());

	for (Int_t iCent = 0; iCent < fNCentralityBins; iCent++) {
		for (Int_t iVtxZ = 0; iVtxZ < fNVtxZBins; iVtxZ++) {
			delete fPool[iCent][iVtxZ]; fPool[iCent][iVtxZ] = 0x0;
			delete fPoolEventNumbers[iCent][iVtxZ]; fPoolEventNumbers[iCent][iVtxZ] = 0x0;
		}

		delete fCounter[iCent];
		delete fPool[iCent];
		delete fPoolEventNumbers[iCent];

	}	

	delete fCounter;
	delete fPool;
	delete fPoolEventNumbers;

	if (fOutgoingEventFrequency) {delete fOutgoingEventFrequency;}

}

// -----------------------------------------------------------------------
void AliMiniEventPoolDiHadronPID::AddEventTriggers(const AliMiniEventDiHadronPID* miniEvent, TObjArray* triggers, Int_t eventIndex) {

	// This method adds a new event to the pool. To speed up things somewhat, 
	// it assumes that the user has already created a TObjArray with (clones of)
	// the trigger. Responsibility for these triggers is hereby given to the pool.
	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	if (!triggers) {
		AliLogDiHadronPID::Error(Form("No trigger array provided."), __func__, ClassName());
		return;
	}
	
	// Check the centrality and vtx Z value of the event.
	Int_t centBin = fCentralityAxis->FindBin(miniEvent->GetCentrality());
	Int_t vtxzBin = fVtxZAxis->FindBin(miniEvent->GetVtxZ());

	// Check the current status of the pool.
	Int_t& currentPoolCounter = fCounter[centBin-1][vtxzBin-1];
	TObjArray* currentPoolContent = (TObjArray*)fPool[centBin-1][vtxzBin-1]->At(currentPoolCounter);

	// Clean up any possible pool content.
	if (currentPoolContent) {
		fPool[centBin-1][vtxzBin-1]->RemoveAt(currentPoolCounter); 	// This automatically sets the entry to 0x0, and does nothing if nothing is found.

		// Delete array and its triggers. Note that here we need to own the array contents.
		currentPoolContent->SetOwner(kTRUE);
		delete currentPoolContent;
		currentPoolContent = 0x0;
	}

	fPool[centBin-1][vtxzBin-1]->AddAt(triggers, currentPoolCounter);
	fPoolEventNumbers[centBin-1][vtxzBin-1][currentPoolCounter] = eventIndex;

	currentPoolCounter++;
	if (currentPoolCounter == fSettings->GetNTriggersToMix()) {currentPoolCounter = 0;} 

}

// -----------------------------------------------------------------------
void AliMiniEventPoolDiHadronPID::GetMixTriggers(const AliMiniEventDiHadronPID* miniEvent, TObjArray* triggers) {

	// This method resets the triggers object array, and puts fills it with the appropriate triggers
	// for this centrality bin / vtxZ bin.
	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	if (!triggers) {
		AliLogDiHadronPID::Error(Form("No trigger array provided."), __func__, ClassName());
		return;
	}

	if (!IsReady()) {
		AliLogDiHadronPID::Error(Form("Pool is not ready for mixing."), __func__, ClassName());
		return;
	}

	// Make sure that the array does not own the triggers.
	triggers->SetOwner(kFALSE);
	triggers->Clear();
	// cout << " Entries after clearing: " << triggers->GetEntries() << endl;

	// Check the centrality and vtx Z value of the event.
	Int_t centBin = fCentralityAxis->FindBin(miniEvent->GetCentrality());
	Int_t vtxzBin = fVtxZAxis->FindBin(miniEvent->GetVtxZ());

	// Loop over all the triggers that we have for that bin, and add them to "triggers".
	for (Int_t iPoolIndex = 0; iPoolIndex < fSettings->GetNTriggersToMix(); iPoolIndex++) {
			
		TObjArray* currentEvent = (TObjArray*)fPool[centBin-1][vtxzBin-1]->At(iPoolIndex);
		for (Int_t iEventIndex = 0; iEventIndex < currentEvent->GetEntries(); iEventIndex++) {
				
			triggers->AddLast(currentEvent->At(iEventIndex));
			// cout << "Adding trigger to the pool, entries: " << triggers->GetEntries() << endl;	

		}

		// Record how many times each event index has been given out.
		if (fOutgoingEventFrequency) {
			Int_t currentEventIndex = fPoolEventNumbers[centBin-1][vtxzBin-1][iPoolIndex];
			
			if (fOutgoingEventFrequency->GetNoElements() <= currentEventIndex) {
				fOutgoingEventFrequency->ResizeTo(currentEventIndex + 1000);
			}
			((*fOutgoingEventFrequency)[currentEventIndex])++;
	
		}

	}

}

// -----------------------------------------------------------------------
Bool_t AliMiniEventPoolDiHadronPID::IsReady() const {

	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	// Return ready if this was confirmed before.
	if (fIsReady == kFALSE) {

		// Check the number of entries
		for (Int_t iCent = 0; iCent < fNCentralityBins; iCent++) {			
			for (Int_t iVtxZ = 0; iVtxZ < fNVtxZBins; iVtxZ++) {

				// Return false if one of the bins is found to contain too little events.
				Int_t nEntries = fPool[iCent][iVtxZ]->GetEntries();
				if (nEntries < fSettings->GetNTriggersToMix()) {return kFALSE;}

			}
		}

		// If all bins have the desired size, then we are ready.
		fIsReady = kTRUE;

	}

	return fIsReady;

}

// -----------------------------------------------------------------------
void AliMiniEventPoolDiHadronPID::Print(Option_t*) const {
	
	// Print info about the class.
	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	cout << endl;
	cout << " V Cent | VtxZ ---> " << endl;
	for (Int_t iCent = 0; iCent < fNCentralityBins; iCent++) {
		for (Int_t iVtxZ = 0; iVtxZ < fNVtxZBins; iVtxZ++) {
				
			cout << Form(" %3i(%3i)", fPool[iCent][iVtxZ]->GetEntries(), fCounter[iCent][iVtxZ]); // Important to not use GetEntriesFast(), as this counts the number of fills.
		
		}

		cout << endl;
	}

	cout << "   POOL IS" << (IsReady() ? "" : " NOT") << " READY" <<endl;
	cout << endl;

}

// -----------------------------------------------------------------------
TH1I* AliMiniEventPoolDiHadronPID::GetOutgoingEventFrequency() const {

	if (!fOutgoingEventFrequency) {return 0x0;}

	TH1I* hOut = new TH1I("hPoolOutgoingEventFrequency","Pool Outgoing Event Frequency;EventIndex;Frequency",fOutgoingEventFrequency->GetNoElements(), -0.5, fOutgoingEventFrequency->GetNoElements() - 0.5);
	for (Int_t iEventIndex = 0; iEventIndex < fOutgoingEventFrequency->GetNoElements(); iEventIndex++) {
			
		hOut->SetBinContent(iEventIndex+1,(*fOutgoingEventFrequency)[iEventIndex]);
	
	}

	return hOut;

}
