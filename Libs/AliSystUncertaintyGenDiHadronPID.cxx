/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Class that can generate TF1's representing the systematic uncertainty
//  The class can handle addition of various parameterisation of
//  systematic uncertainty, lines, parabola's, kinked lines, and keeps
//  track of separate uncertainties, up and down.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "AliSystUncertaintyGenDiHadronPID.h"
#include "AliLogDiHadronPID.h"
#include "TMath.h"

#include <iostream>
using namespace std;

ClassImp(AliSystUncertaintyGenDiHadronPID);

// -----------------------------------------------------------------------
AliSystUncertaintyGenDiHadronPID::AliSystUncertaintyGenDiHadronPID(Double_t xMin, Double_t xMax):
	TObject(),
	fXmin(xMin),
	fXmax(xMax),
	fFormulaUp("TMath::Sqrt("),
	fFormulaDown("TMath::Sqrt(")

{
	
	// Default Constructor
	AliLogDiHadronPID::Debug2("Default constructor called.", __func__, ClassName());

}

// -----------------------------------------------------------------------
AliSystUncertaintyGenDiHadronPID::~AliSystUncertaintyGenDiHadronPID() {
	
	// Destructor
	AliLogDiHadronPID::Debug2("Destructor called.", __func__, ClassName());

}

// -----------------------------------------------------------------------
void AliSystUncertaintyGenDiHadronPID::AddLinearTerm(Double_t yMin, Double_t yMax, Bool_t up, Bool_t down) {

	// This method adds a term that is linear from fXmin to fXmax.
	TString a = Form("%1.3f", yMin);
	TString b = Form("(%1.3f-%1.3f)/(%1.3f-%1.3f)*(x-%1.3f)", yMax, yMin, fXmax, fXmin, fXmin);

	TString total = "(";
	total += a;
	total += "+";
	total += b;
	total += ")";

	Add(total, up, down);

}

// -----------------------------------------------------------------------
void AliSystUncertaintyGenDiHadronPID::AddKinkTerm(Double_t yMin, Double_t yMax, Double_t xKink, Bool_t up, Bool_t down) {

	// This method adds a term that is constant from fXmin to xKink, and linear from xKink to fXmax.
	TString a = Form("%1.3f", yMin);
	TString b = Form("(%1.3f-%1.3f)/(%1.3f-%1.3f)*(x-%1.3f)", yMax, yMin, fXmax, xKink, xKink);

	TString total = "(";
	total += a;
	total += "+";
	total += Form("TMath::Floor(x/%1.3f)", xKink);
	total += "*";
	total += b;
	total += ")";

	Add(total, up, down);

}

// -----------------------------------------------------------------------
void AliSystUncertaintyGenDiHadronPID::AddQuadraticTerm(Double_t yMin, Double_t yMax, Bool_t up, Bool_t down) {

	// This method adds a quadratic term, concave up, with the minimum at fXmin.
	TString a = Form("%1.3f", yMin);	
	TString b = Form("(%1.3f-%1.3f)/(%1.3f-%1.3f)/(%1.3f-%1.3f)*(x-%1.3f)*(x-%1.3f)",
		yMax, yMin, fXmax, fXmin, fXmax, fXmin, fXmin, fXmin);

	TString total = "(";
	total += a;
	total += "+";
	total += b;
	total += ")";

	Add(total, up, down);

}

// -----------------------------------------------------------------------
TF1* AliSystUncertaintyGenDiHadronPID::GetTF1(Bool_t up) const {

	TString func = "";
	if (up) {func += fFormulaUp;}
	else {
		func += "-";
		func += fFormulaDown;
	}

	func += ")";

	TF1* fout = new TF1(Form("func%s",up ? "Up" : "Down"), func.Data(), fXmin, fXmax);

	return fout;

}

// -----------------------------------------------------------------------
TGraphAsymmErrors* AliSystUncertaintyGenDiHadronPID::GetGraph(const TH1* hist) const {

	// Check if the histogram's boundaries are beyond fXmin or fXmax.
	if ((hist->GetBinLowEdge(1) + 0.0001) < fXmin ||
		(hist->GetBinLowEdge(hist->GetNbinsX() + 1) - 0.0001) > fXmax) {
		AliLogDiHadronPID::Warning(Form("Domain of the histogram is larger than the domain on which you defined systematic uncertainties."), __func__, ClassName());
	}

	// Create a new graph around the histogram points.
	TGraphAsymmErrors* gOut = new TGraphAsymmErrors(hist);

	// Create functions for up and down.
	TF1* upFunc = GetTF1(kTRUE);
	TF1* downFunc = GetTF1(kFALSE);

	// Set the lower and upper errors on the graph.
	for (Int_t iPoint = 0; iPoint < gOut->GetN(); iPoint++) {
		
		Double_t xx = 0.;
		Double_t yy = 0.;
		gOut->GetPoint(iPoint, xx, yy);
		gOut->SetPointEYlow(iPoint, TMath::Abs(yy * downFunc->Eval(xx)));
		gOut->SetPointEYhigh(iPoint, TMath::Abs(yy * upFunc->Eval(xx)));
	
	}

	// Clean up.
	delete upFunc;
	delete downFunc;

	return gOut;

}

// -----------------------------------------------------------------------
void AliSystUncertaintyGenDiHadronPID::Add(TString &addition, Bool_t up, Bool_t down) {

	if (up) {
		
		// Add plus sign if previous contributions exist.
		if (fFormulaUp[fFormulaUp.Length() - 1] == ')') {fFormulaUp += "+";}

		// Add the error in quadrature.
		fFormulaUp+=addition;
		fFormulaUp+="*";
		fFormulaUp+=addition;

	}

	if (down) {

		// Add plus sign if previous contributions exist.
		if (fFormulaDown[fFormulaDown.Length() - 1] == ')') {fFormulaDown += "+";}

		// Add the error in quadrature.
		fFormulaDown+=addition;
		fFormulaDown+="*";
		fFormulaDown+=addition;

	}

}

// -----------------------------------------------------------------------
void AliSystUncertaintyGenDiHadronPID::Print(Option_t*) const {
	
	// Print info about the class.
	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	cout << "Current formula for the upwards systemtatic error: " << endl << fFormulaUp.Data() << ")." << endl;
	cout << endl;
	cout << "Current formula for the downwards systemtatic error: " << endl <<fFormulaDown.Data() << ")." << endl;

}
