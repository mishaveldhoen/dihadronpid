#ifndef ALIRATIOSDIHADRONPID_H
#define ALIRATIOSDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TNamed.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TGraphErrors.h"
#include "AliSettingsDiHadronPID.h"

class AliRatiosDiHadronPID : public TNamed {
	
public:
	
	// kAll -> The Whole (DPhi, DEta) plane,
	// kPeak1a -> Square: -1.0 < DPhi < 1.0, -0.5 < DEta < 0.5
	// kPeak1b -> Square: -1.0 < DPhi < 1.0, -0.6 < DEta < 0.6
	// kRidge1 -> Square: -1.0 < DPhi < 1.0, 0.8 < DEta < 1.4 (Symmetric)
	enum Region {kAll, kPeak1a, kPeak1b, kPeakOLD, kRidge1, kUnknownRegion, kRidgeOLD};
	enum DPhiBackgroundModel {kNoFlow_AS, kWithFixed_V2, kWithFixed_V2_AS, kWithFixed_V2_V3, kWithFixed_V2_V3_AS, kWithFixed_V2_V3_V4, kWithFixed_V1_V2_V3_V4};
	enum DEtaFitStrategy {kSingleGauss, kDoubleGauss};

	AliRatiosDiHadronPID();	
	AliRatiosDiHadronPID(const char* name, const char* title);
	AliRatiosDiHadronPID(const AliRatiosDiHadronPID& other);
	~AliRatiosDiHadronPID();

	// Plotting Directory Setter.
	void 							SetPlotDirectory(TString plotDir) {fPlotDir = plotDir;}

	// Data I/O.
	void							SetNTriggers(Double_t nTrig) {fNTriggers = nTrig;}
	Double_t 						GetNTriggers() const {return fNTriggers;}

	void							ImportCorrelation(AliSettingsDiHadronPID::Species massAssumption, Int_t ptBin, const TH2F* correlation);
	TH2F*							GetCorrelationClone(AliSettingsDiHadronPID::Species massAssumption, Int_t ptBin) const;
	
	void							ImportMixedEvent(AliSettingsDiHadronPID::Species massAssumption, Int_t ptBin, const TH2F* mixedEvent);
	TH2F*							GetMixedEventClone(AliSettingsDiHadronPID::Species massAssumption, Int_t ptBin) const;
	
	// Just take the ptr, no copy is made.
	void 							ImportTotalEfficiency(AliSettingsDiHadronPID::Species massAssumption, TH1F* effhist) {fEfficiency[massAssumption] = effhist;}
	void							ImportPrimaryFraction(AliSettingsDiHadronPID::Species massAssumption, TF1* primfrac) {fPrimaryFraction[massAssumption] = primfrac;}

	TH2F*							GetCorrectedClone(AliSettingsDiHadronPID::Species massAssumption, Int_t ptBin, Int_t ptBinMax = -1) const;
	TH1F* 							GetHistBulkFitWithConst(AliSettingsDiHadronPID::Species species, Int_t pTbin, Double_t BulkMinAbsDEta, TLatex* dEtaRange = 0x0, Bool_t useSumInsteadOfFit = kFALSE) const;
	TH1F* 							GetHistBulkFitWithConst(const TH2F* hist, Double_t BulkMinAbsDEta, TLatex* dEtaRange = 0x0, Bool_t useSumInsteadOfFit = kFALSE) const;

	TH1F*							GetProjectionPt(AliSettingsDiHadronPID::Species species, Region region) const;

	TH1F*							GetJetYieldClone(AliSettingsDiHadronPID::Species species) const {return (fJetYield[species] ? (TH1F*)(fJetYield[species])->Clone() : 0x0);}
	TH1F*							GetBulkYieldClone(AliSettingsDiHadronPID::Species species) const {return (fBulkYield[species] ? (TH1F*)(fBulkYield[species])->Clone() : 0x0);}

	TH1F*							GetJetPrToPiClone() const {return (fJetPrToPi ? (TH1F*)fJetPrToPi->Clone() : 0x0);}
	TH1F*							GetJetKaToPiClone() const {return (fJetKaToPi ? (TH1F*)fJetKaToPi->Clone() : 0x0);}
	TH1F*							GetBulkPrToPiClone() const {return (fBulkPrToPi ? (TH1F*)fBulkPrToPi->Clone() : 0x0);}
	TH1F*							GetBulkKaToPiClone() const {return (fBulkKaToPi ? (TH1F*)fBulkKaToPi->Clone() : 0x0);}

	// Plotting.
	void 							PlotDPhiDEta(AliSettingsDiHadronPID::Species species, Int_t pTbin) const;	
	void 							PlotRegionYieldVsPt(AliSettingsDiHadronPID::Species species, Region region) const;
	
	// Measuring peak and bulk yields.
	TH1F**							AnalyseDPhiDEta(AliSettingsDiHadronPID::BeamType beamType, AliSettingsDiHadronPID::Species species, 
										DPhiBackgroundModel model, Int_t pTBinMin, Int_t pTBinMax, Double_t PeakRadiusDPhi, Double_t PeakRadiusDEta,
										Double_t* parameters, Double_t* errors, Double_t* measurementOfChoice, Double_t* measurementOfChoiceError, 
										Double_t* peakWidthSG, Double_t* peakWidthSGError, Bool_t useWingCorrection = kFALSE, Bool_t saveFitToPDF = kFALSE) const;
	TF1*							FitDPhiFlowAndASPeak(AliSettingsDiHadronPID::Species species, Int_t pTBin, TH1* hist, TH1* signalHist, const TF1* initialFunc, DPhiBackgroundModel model, Color_t mainColor, Bool_t saveFitToPDF = kFALSE) const;
	
	TGraphErrors*					DCTDPhiFlow(AliSettingsDiHadronPID::BeamType beamType, AliSettingsDiHadronPID::Species species, 
										Int_t pTBin, const TH1* bgdHist, Color_t /*mainColor*/, Double_t& background, Double_t& backgroundError, 
										Int_t minOrder = 1, Int_t maxOrder = 4, Bool_t saveFitToPDF = kFALSE) const;
	TF1*							FitDPhiNSPeak(AliSettingsDiHadronPID::Species species, Int_t pTBin, TH1* hist, TH1* bgdHist, const TF1* initialFunc, DPhiBackgroundModel model, Bool_t useDoubleGauss, Color_t mainColor, Bool_t saveFitToPDF = kFALSE) const;

	void							GetDPhiJetYieldAndErrorByCounting(const TH1* histSignal, const TObject* background, Double_t& yield, Double_t& error) const;
	void 							GetJetYieldAndErrorByCountingInJetRegion(const TH2* histSignal, const TObject* background, Double_t& yield, Double_t& error, Double_t maxDEta = 1.3, Double_t DPhiJet = 1., Double_t DEtaJet = 1.) const;

	void							EstimateBackgroundDEta(TH1* hist, Double_t bulkMinAbsDEta, Double_t& yield, Double_t& error) const;
	TF1* 							FitDEtaPeak(AliSettingsDiHadronPID::Species species, Int_t pTBin, TH1* hist, TH1* bgdHist, const TF1* initialFunc, Bool_t useDoubleGauss, Color_t mainColor, Bool_t saveFitToPDF = kFALSE) const;
	
	// OLD!
	void							RegionsMethod(AliSettingsDiHadronPID::Species species, Int_t pTBin, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta, Double_t* parameters, Double_t* errors, Bool_t saveFitToPDF = kFALSE) const;

	void							FitDPhiWithGauss(AliSettingsDiHadronPID::Species species, Int_t pTBin, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta, Double_t* parameters, Double_t* errors, Bool_t saveFitToPDF = kFALSE) const;
	void							FitDPhiWithDoubleGauss(AliSettingsDiHadronPID::Species species, Int_t pTBin, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta, Double_t* parameters, Double_t* errors, Bool_t saveFitToPDF = kFALSE) const;
	void							FitDPhiWithTrippleGauss(AliSettingsDiHadronPID::Species species, Int_t pTBin, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta, Double_t* parameters, Double_t* errors, Bool_t saveFitToPDF = kFALSE) const;

	void							FitBulkWithConstVn(AliSettingsDiHadronPID::Species species, Int_t pTBin, Double_t BulkMinAbsDEta, Double_t* parameters, Double_t* errors, Bool_t saveFitToPDF = kFALSE) const;

	void 							FitDEtaWithGauss(AliSettingsDiHadronPID::Species species, Int_t pTbin, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta,  Double_t* parameters, Double_t* errors, Bool_t doubleGauss, Bool_t saveFitToPDF = kFALSE) const;
	void							FitDEtaWithParabola(AliSettingsDiHadronPID::Species species, Int_t pTbin, Double_t minDPhi, Double_t maxDPhi, Double_t* parameters, Double_t* errors, Bool_t saveFitToPDF = kFALSE) const;

	// Measurements as function of pT (Working OK)
	void							MeasureJetAndBulkYieldUsingRegions(AliSettingsDiHadronPID::Species species, Region peakRegion, Region bulkRegion);

	void							MeasureJetAndBulkYieldUsingBulkFit(AliSettingsDiHadronPID::Species species, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta, Bool_t saveFitsToFile = kFALSE);

	void 							MeasureJetAndBulkYield(AliSettingsDiHadronPID::BeamType beamType, AliSettingsDiHadronPID::Species species, DPhiBackgroundModel model, Bool_t rebinInPt = kTRUE, Bool_t useWingCorrection = kFALSE, Bool_t saveFitsToFile = kFALSE);

	void							MeasureJetAndBulkYieldUsingDPhiFit(AliSettingsDiHadronPID::Species species, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta, Bool_t saveFitsToFile = kFALSE);
	void							MeasureJetAndBulkYieldUsingDPhiFitDG(AliSettingsDiHadronPID::Species species, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta, Bool_t saveFitsToFile = kFALSE);
	void							MeasureJetAndBulkYieldUsingDPhiFitTGNoBulkSubtraction(AliSettingsDiHadronPID::Species species, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta, Bool_t saveFitsToFile = kFALSE);
	
	void							MeasureJetAndBulkYieldUsingDEtaFit(AliSettingsDiHadronPID::Species species, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta, Bool_t useDoubleGauss, Bool_t saveFitsToFile = kFALSE);
	
	void							MeasureVnUsingBulkFit(AliSettingsDiHadronPID::Species species, Double_t BulkMinAbsDEta, Bool_t saveFitsToFile = kFALSE) const;
	void							MeasureAwaySideFlatnessDEta(AliSettingsDiHadronPID::Species species, Bool_t saveFitsToFile = kFALSE) const;

	// Systematic error due to peak extraction method.
	void							RandomiseCorrelations();
	TH2F*							SystErrDEtaFitVSDPhiFit(AliSettingsDiHadronPID::Species species, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta);

	TH2F*							SystErrBulkFitVSDPhiFit(AliSettingsDiHadronPID::Species species, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta);
	TH2F*							SystErrBulkFitVSDPhiFitDG(AliSettingsDiHadronPID::Species species, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta);

	TH2F*							SystErrBulkFitVSDEtaFit(AliSettingsDiHadronPID::Species species, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta);

	TH2F*							SystErrBulkFitTwoSizes(AliSettingsDiHadronPID::Species species, Double_t PeakMaxDPhi1, Double_t PeakMaxDEta1, Double_t BulkMinAbsDEta1, Double_t PeakMaxDPhi2, Double_t PeakMaxDEta2, Double_t BulkMinAbsDEta2);

	// Systematic error from Template PID - Divide DPhiDEta Measured through Truth, and project on the pT axis.
	TH1F*							SystErrPIDMethod(const AliRatiosDiHadronPID* mcRatios);

	// Final Plots.
	Bool_t							MakeRatios(); // Returns kFALSE if the jet or bulk spectra are not created.
	void							SaveSpectraAndRatios(const char* rootFileName = "results.root");

	// Comparison with other di-hadron analyses.
	void							CompareDPhiDEta(const AliRatiosDiHadronPID* other, AliSettingsDiHadronPID::Species species, Int_t pTbin) const;
	void							CompareRegionYield(const AliRatiosDiHadronPID* other, AliSettingsDiHadronPID::Species species, Region region) const;
	
	void							CompareJetYield(const AliRatiosDiHadronPID* other) const;
	void							CompareBulkYield(const AliRatiosDiHadronPID* other) const;

	void							CompareJetAndBulkRatios(const AliRatiosDiHadronPID* other) const;
	void							CompareJetRatios(const AliRatiosDiHadronPID* other) const; // OLD
	void							CompareBulkRatios(const AliRatiosDiHadronPID* other) const; // OLD

	void 							Print(Option_t*) const;

	// Shapes
	static Int_t					GetNRegions() {return fNRegions;}
	static TString					GetRegionName(Region region) {return fRegionNames[region];}

private:
	TObjArray*						GetCorrelationArrayForMassAssumption(AliSettingsDiHadronPID::Species massAssumption) const;
	TObjArray*						GetMixedEventsArrayForMassAssumption(AliSettingsDiHadronPID::Species massAssumption) const;

	TH2F*							GetCorrelation(AliSettingsDiHadronPID::Species massAssumption, Int_t ptBin) const;
	TH2F*							GetMixedEvents(AliSettingsDiHadronPID::Species massAssumption, Int_t ptBin) const;

	Bool_t							GetIntegralAndError(AliSettingsDiHadronPID::Species species, Region region, Int_t ptBin, Double_t& integral, Double_t& error) const;

	TH1F*							CutOutJetAndProject(const TH2F* hist, Double_t maxDEta = 1.3, Double_t DPhiJet = 1., Double_t DEtaJet = 1., Bool_t useConstFit = kFALSE) const;

private:
	AliSettingsDiHadronPID*			fSettings; //!
	TAxis*							fPtAxis;
	TAxis*							fPtAxisRebinned;
	Double_t						fNTriggers; // Number of unidentified triggers.

	// The directory where plots are stored to.
	TString							fPlotDir;

	// Settings.
	Int_t							fNTrialsForSystematics;

	// These arrays contain fitted 2-particle correlations for every pT bin.
	TObjArray*						fIdentifiedCorrelationsPion;
	TObjArray*						fIdentifiedCorrelationsKaon;
	TObjArray*						fIdentifiedCorrelationsProton;

	TObjArray*						fIdentifiedMixedEventsPion;
	TObjArray*						fIdentifiedMixedEventsKaon;
	TObjArray*						fIdentifiedMixedEventsProton;

	TH1F*							fJetYield[3];
	TH1F*							fJetWidthDEta[3];
	TH1F*							fJetWidthDPhi[3];
	TH1F*							fBulkYield[3];

	TH1F*							fJetPrToPi;
	TH1F*							fJetKaToPi;

	TH1F*							fBulkPrToPi;
	TH1F*							fBulkKaToPi;

	// Efficiency histograms.
	TH1F*							fEfficiency[3];
	TF1*							fPrimaryFraction[3];

	// Systematic Uncertainties.
	TGraphErrors*					fJetYieldSyst[3];
	TGraphErrors*					fBulkYieldSyst[3];

	TGraphErrors*					fJetPrToPiSyst;
	TGraphErrors*					fJetKaToPiSyst;

	TGraphErrors*					fBulkPrToPiSyst;
	TGraphErrors*					fBulkKaToPiSyst;

	static const Int_t				fNRegions;
	static const TString			fRegionNames[4];

	ClassDef(AliRatiosDiHadronPID, 1);

};

#endif
