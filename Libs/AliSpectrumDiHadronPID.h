#ifndef ALISPECTRUMDIHADRONPID_H
#define ALISPECTRUMDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

/*class TString;
class TH1F;
class TVirtualPad;
class TBrowser;
class TCanvas;*/

#include "TNamed.h"
#include "TH1F.h"
#include "TObjArray.h"
#include "TFile.h"
#include "TVirtualPad.h"

class AliSpectrumDiHadronPID : public TNamed {

public:
	AliSpectrumDiHadronPID();
	AliSpectrumDiHadronPID(const char* name, const char* title, Int_t massassumption, const char* useddetectors, Bool_t ispbpb = kTRUE);
	~AliSpectrumDiHadronPID();

private:
	AliSpectrumDiHadronPID(const AliSpectrumDiHadronPID&);
	AliSpectrumDiHadronPID& operator=(const AliSpectrumDiHadronPID&);
	void CleanInputBuffer();
	void CleanOutputHistograms();
	Bool_t InitializeHistograms();
	Bool_t InitializeFitters();		
	//Bool_t CheckFitter() const; 	// Should be removed.

public:
	void ImportPIDHistograms(const TObjArray* pidhistos, const TAxis* ptaxis, Double_t etaMin = -0.8, Double_t etaMax = 0.8);
	void ImportMismatchHistograms(const TObjArray* mismatchhistos);
	void ImportEfficiency(const TH1F* hist);

	// Browse Functionality.
	void Browse(TBrowser* b);
	void BrowseThis(); // *MENU*
	void WriteToFile(const char* filename) const; // *MENU*

	// Fits.
	void FitBin(Int_t PtBin, Int_t strategy = 0, Bool_t applyPenalties = 0); // *MENU*
	void FitBins(Int_t BinMin, Int_t BinMax, Int_t strategy = 0/*, const char* vary_par = "", Int_t vary_species = 0, Bool_t vary_updown = 0*/); // *MENU*
	void FitPtRange(Double_t PtMin, Double_t PtMax/*, Int_t, const char*, Int_t, Bool_t*/);
	void FitAll(Int_t strategy = 0/*, const char* vary_par = "", Int_t vary_species = 0, Bool_t vary_updown = 0*/); // *MENU*

	// Systematics - For every bin for which we have a "good" fit, we vary the fitted parameters with some reasonable values,
	// then fix them, and redo the fits of all yields.
	//
	// We've made this into a separate method from the fit methods, to retain the possibility to judge the quality of a fit
	// by comparing it to the fits in the neighboring p_T bins.
	void DetermineSystematicsForBin(Int_t PtBin, Int_t fitstrategy, Int_t syststrategy = 0); // *MENU*
	void DetermineSystematicsForBins(Int_t BinMin, Int_t BinMax, Int_t fitstrategy, Int_t syststrategy = 0); // *MENU*
	void DetermineSystematicsForPtRange(Double_t PtMin, Double_t PtMax, Int_t fitstrategy, Int_t syststrategy = 0); // *MENU*
	void DetermineSystematicsForAllBins(Int_t fitstrategy, Int_t syststrategy = 0); // *MENU*

	// Getting clones of histograms.
	TH1F* GetCloneRawSpectrum(Bool_t rebinned = kFALSE) const;
	TH1F* GetCloneCorrSpectrum(Bool_t rebinned = kFALSE) const;
	TH1F* GetCloneParHisto(Int_t par) const;
	TObjArray* GetCloneVariedRawSpectra(Bool_t rebinned = kFALSE) const {return (rebinned ? (TObjArray*)fRawSpectrumVarReb->Clone() : (TObjArray*)fRawSpectrumVar->Clone());}	// Deep copy.
	TObjArray* GetCloneVariedCorrSpectra(Bool_t rebinned = kFALSE) const {return (rebinned ? (TObjArray*)fCorrSpectrumVarReb->Clone() : (TObjArray*)fCorrSpectrumVar->Clone());}	// id.
	TObjArray* GetCloneParHistos() const {return (TObjArray*)(fParHistos->Clone());} 				// id.
	TObjArray* GetCloneOfUnIDSpectra() const {return (TObjArray*)(fUnIDSpectra->Clone());}			// id.
	TObjArray* GetClonePIDHistos() const {if (fPIDhistos) {return (TObjArray*)(fPIDhistos->Clone());} else {return 0x0;}}				// id.

	// Mismatch fraction histogram.
	TH1F* GetMismatchFraction() const;

	// Settings.
	Int_t GetDebugLevel() const {return fDebug;}
	Bool_t GetIsPbPb() const {return fIsPbPb;}
	Int_t GetMassAssumption() const {return fMassAssumption;}
	const char* GetUsedDetectors() const {return fUsedDetectors;}

	void SetDebugLevel(Int_t debuglevel); // *MENU*
	void SetPrintHessian(Bool_t printhessian = kTRUE); // *MENU*
	
	void PrintStatus(); // *MENU*
	void PrintPtAxis(); // *MENU*
	void PrintEtaRange(); // *MENU*
	void PrintListOfParameters(); // *MENU*

	// Drawing histograms on current pad.
	void DrawRawSpectrum(); // *MENU*
	void DrawCorrSpectrum(); // *MENU*
	void DrawEfficiency(); // *MENU*
	void DrawFitStatus(); // *MENU*
	void DrawChiSquared(); // *MENU*
	void DrawParHisto(Int_t par); // *MENU*
	void DrawFit(Int_t bin); // *MENU*
	void DrawPull(Int_t bin); // *MENU*
	void DrawTOFProjection(Int_t bin); // *MENU*
	void DrawTPCProjection(Int_t bin); // *MENU*
	void DrawMismatchFraction(); // *MENU*

	// Export PDF's
	void ExportTOFProjection(Int_t bin, Int_t step); // *MENU*
 	void ExportTPCProjection(Int_t bin, Int_t step); // *MENU*

	// Create Corrected spectrum.
private:	
	void ApplyGeantFlukaCorrection();	// TO BE REMOVED
	void ApplyEfficiencyCorrection();	// TO BE REMOVED
public:
	void CreateCorrectedSpectrum(); // *MENU*
	void CreateRebinnedSpectrum(); // *MENU*

private:
	TH1F* Rebin(TH1F* hist);
	TObjArray* Rebin(TObjArray* array);

public:
	// Compare Spectrum with another one.
	void SetCompanionSpectrum(const char* filename); // *MENU* 
	void CompareRawSpectra(); // *MENU*

private:
	void ResetCurrentPad();

private:

	TObjArray*						fFitters;		//! Pointers to the used fitters for every bin (not streamed!)
	TBrowser*						fBrowser;		//! Pointer to the browser.
	TVirtualPad*					fBrowserPad;	//! Pointer to the pad in the browser.
	
	TFile*							fCompanionFile;	//!
	AliSpectrumDiHadronPID*			fCompanion;		//! Pointer to another spectrum to which we want to compare.
	TAxis*							fPtAxis;		// 
	Double_t						fEtaMin;		//
	Double_t						fEtaMax;		//
	TH1F*							fRawSpectrum;	// The current raw spectrum.
	TObjArray*						fRawSpectrumVar;// Raw spectra, where all intensive properties are fixed to the latest fit result, but one (or more) were varied.
	TObjArray*						fUnIDSpectra;	// Several unidentified spectra (total entries (total and in fit region), integral under fit (total and in fit region).
	TH1I*							fFitStatus;		// Status of the fitter as a function of pT (-1 for unfitted bins.)
	TH1F*							fEfficiency;	// Efficiency.
	TH1F*							fCorrSpectrum;	// Corrected spectrum.
	TObjArray*						fCorrSpectrumVar;//
	TH1F*							fChiSquaredDOF;	// Chi^2 per DOF as a function of p_T.
	TObjArray*						fPIDhistos;		// p_{T} ordered PID histograms.
	TObjArray*						fMismatchHistos;//
	TObjArray*						fParHistos;		// Histograms of parameters as a function of p_T
	TObjArray*						fFitSteps;		// Canvases of all fit steps, orderd by p_T bin.

	// Relevant Rebinned Results.
	TH1F*							fRawSpectrumReb;	// The current raw spectrum.
	TObjArray*						fRawSpectrumVarReb;// Raw spectra, where all intensive properties are fixed to the latest fit result, but one (or more) were varied.
	TH1F*							fCorrSpectrumReb;
	TObjArray*						fCorrSpectrumVarReb;

	Bool_t 							fIsInitialized;	// If this flag is true, then the instance is fully initialized.

	Bool_t							fOuputHistosAreCreated;

	Bool_t							fFittersAreInitialized;	//! Since the fitters aren't streamed to file, also this flag shouldn't be streamed.
	Bool_t							fBufferIsFull;	//
	TString							fUsedDetectors;	// INFLUENCES FITTER OBJECT - INFLUENCES SYSTEMATIC CHECKS.
	Int_t							fMassAssumption;// INFLUENCES FITTER OBJECT.
	Int_t							fNbinsDPhiDEta;	// 
	Bool_t							fIsPbPb;		//
	Int_t							fDebug;			// Debug flag.
	Bool_t							fPrintHessian;	//

	ClassDef(AliSpectrumDiHadronPID, 4);

};

#endif
