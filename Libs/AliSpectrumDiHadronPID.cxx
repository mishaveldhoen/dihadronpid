/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Creates identified spectra, and holds also QA histograms of the fits.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "AliSpectrumDiHadronPID.h"

#include <iostream>
using namespace std;

#include "TLegend.h"
#include "TKey.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TH2F.h"
#include "TBrowser.h"
#include "TMath.h"

#include "AliHistToolsDiHadronPID.h"
#include "AliFitterBaseDiHadronPID.h"
#include "AliFitterTOFDiHadronPID.h"
#include "AliFitterTOFTPCDiHadronPID.h"

ClassImp(AliSpectrumDiHadronPID);

// -----------------------------------------------------------------------
AliSpectrumDiHadronPID::AliSpectrumDiHadronPID():
	TNamed(),
	fFitters(0x0),
	fBrowser(0x0),
	fBrowserPad(0x0),
	fCompanionFile(0x0),
	fCompanion(0x0),
	fPtAxis(0x0),
	fEtaMin(-0.8),
	fEtaMax(0.8),
	fRawSpectrum(0x0),
	fRawSpectrumVar(0x0),
	fUnIDSpectra(0x0),
	fFitStatus(0x0),
	fEfficiency(0x0),
	fCorrSpectrum(0x0),
	fCorrSpectrumVar(0x0),
	fChiSquaredDOF(0x0),
	fPIDhistos(0x0),
	fMismatchHistos(0x0),
	fParHistos(0x0),
	fFitSteps(0x0),
	fRawSpectrumReb(0x0),
	fRawSpectrumVarReb(0x0),
	fCorrSpectrumReb(0x0),
	fCorrSpectrumVarReb(0x0),
	fOuputHistosAreCreated(kFALSE),
	fFittersAreInitialized(kFALSE),
	fBufferIsFull(kFALSE),
	fUsedDetectors(""),
	fMassAssumption(-1),
	fNbinsDPhiDEta(0),
	fIsPbPb(kTRUE),
	fDebug(0),
	fPrintHessian(kFALSE)

{

	// Default Constructor - When read from a file, the streamer is called after the
	// instance has been created using the default constructor. This means that we can't
	// initialize the fitter here, since fUsedDetectors and fMassAssumption are still at their default
	// values. Solution: lazy instiantiation when the first fit is performed.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

/*
	cout << Form("%s -> Default constructor called.",__func__) << endl;
	cout << "fUsedDetectors: " << fUsedDetectors << endl;
	cout << "fMassAssumption: " << fMassAssumption << endl; 
*/

}

// -----------------------------------------------------------------------
AliSpectrumDiHadronPID::AliSpectrumDiHadronPID(const char* name, const char* title, Int_t massassumption, const char* useddetectors, Bool_t ispbpb):
	TNamed(name, title),
	fFitters(0x0),
	fBrowser(0x0),
	fBrowserPad(0x0),
	fCompanionFile(0x0),
	fCompanion(0x0),
	fPtAxis(0x0),
	fRawSpectrum(0x0),
	fRawSpectrumVar(0x0),
	fUnIDSpectra(0x0),
	fFitStatus(0x0),
	fEfficiency(0x0),
	fCorrSpectrum(0x0),
	fCorrSpectrumVar(0x0),
	fChiSquaredDOF(0x0),
	fPIDhistos(0x0),
	fMismatchHistos(0x0),
	fParHistos(0x0),
	fFitSteps(0x0),
	fRawSpectrumReb(0x0),
	fRawSpectrumVarReb(0x0),
	fCorrSpectrumReb(0x0),
	fCorrSpectrumVarReb(0x0),
	fOuputHistosAreCreated(kFALSE),
	fFittersAreInitialized(kFALSE),
	fBufferIsFull(kFALSE),
	fUsedDetectors(useddetectors),
	fMassAssumption(massassumption),
	fNbinsDPhiDEta(0),
	fIsPbPb(ispbpb),
	fDebug(0),
	fPrintHessian(kFALSE)

{

	// Named Constructor - User should call ImportPIDHistograms() before fitting.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

}

// -----------------------------------------------------------------------
AliSpectrumDiHadronPID::~AliSpectrumDiHadronPID() {

	// Destructor.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	CleanInputBuffer();
	CleanOutputHistograms();

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::CleanInputBuffer() {

	// Deletes the imported histograms, and the p_T axis. After this
	// action, also the output histograms aren't meaningful anymore,
	// and are also deleted.	
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	if (fPtAxis) {delete fPtAxis; fPtAxis = 0x0;}
	if (fPIDhistos) {delete fPIDhistos; fPIDhistos = 0x0;}
	if (fMismatchHistos) {delete fMismatchHistos; fMismatchHistos = 0x0;}
	fBufferIsFull = kFALSE;

	CleanOutputHistograms();

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::CleanOutputHistograms() {

	// Deletes the output histograms.
	if (fFitters) {delete fFitters; fFitters = 0x0;}
	if (fRawSpectrum) {delete fRawSpectrum; fRawSpectrum = 0x0;}
	if (fRawSpectrumVar) {delete fRawSpectrumVar; fRawSpectrumVar = 0x0;}
	if (fUnIDSpectra) {delete fUnIDSpectra; fUnIDSpectra = 0x0;}
	if (fChiSquaredDOF) {delete fChiSquaredDOF; fChiSquaredDOF = 0x0;}
	if (fParHistos) {delete fParHistos; fParHistos = 0x0;}
	if (fFitSteps) {delete fFitSteps; fFitSteps = 0x0;}
	fOuputHistosAreCreated = kFALSE;

}

// -----------------------------------------------------------------------
Bool_t AliSpectrumDiHadronPID::InitializeHistograms() {

	// Creates member histograms. Need to have fPtAxis set.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (fOuputHistosAreCreated) {cout << Form("%s -> ERROR: It's not possible to reinitialize the histograms.",__func__) << endl; return kFALSE;}
	if (!fFitters)  {cout << Form("%s -> ERROR: Fitters not initialized.",__func__) << endl; return kFALSE;}
	if (!fBufferIsFull) {cout << Form("%s -> ERROR: First call ImportPIDHistograms().",__func__) << endl; return kFALSE;}

	// Create the spectrum histogram.
	const TArrayD* ptarray = fPtAxis->GetXbins();

	//cout<<ptarray->At(0)<<" "<<ptarray->At(1)<<" ... "<<ptarray->At(ptarray->GetSize()-1)<<" "<<ptarray->At(ptarray->GetSize())<<endl;
	fRawSpectrum = new TH1F("fRawSpectrum","Spectrum;p_{T} (GeV/c);Count", (ptarray->GetSize() - 1), ptarray->GetArray());
	fRawSpectrum->SetDirectory(0);

	// Create chi^2 histogram.
	fChiSquaredDOF = new TH1F("fChiSquaredDOF", "#chi^{2}/D.O.F.;p_{T} (GeV/c);#chi^{2}/D.O.F.", (ptarray->GetSize() - 1), ptarray->GetArray());
	fChiSquaredDOF->SetDirectory(0);

	// Create fit status histogram (bins are initialized to -1).
	fFitStatus = new TH1I("fFitStatus", "Fit Status;p_{T};Status", (ptarray->GetSize() - 1), ptarray->GetArray());
	fFitStatus->SetDirectory(0);
	for (Int_t iBin = 1; iBin < ptarray->GetSize(); iBin++) {fFitStatus->SetBinContent(iBin, -1);}

	// Get a pointer to one of the fitters (first one in the array.)
	AliFitterBaseDiHadronPID* fitter = (AliFitterBaseDiHadronPID*)(fFitters->At(0));

	// Create histograms of the parameters as function of pT.
	Int_t nfitparams = fitter->GetNumberOfParams();
	fParHistos = new TObjArray(nfitparams);
	fParHistos->SetOwner(kTRUE);
	fParHistos->SetName("fParHistos");
	const TObjArray* parnames = fitter->GetParNames();

	for (Int_t iPar = 0; iPar < nfitparams; iPar++) {
		const char* parnametmp = ((TString*)parnames->At(iPar))->Data();
		TH1F* hist = new TH1F(parnametmp ,Form("%s;p_{T} (GeV/c);%s", parnametmp, parnametmp),
			(ptarray->GetSize() - 1), ptarray->GetArray());
		hist->SetDirectory(0);
		if (fUsedDetectors=="TOF") {hist->SetLineColor(kRed); hist->SetMarkerColor(kRed);}
		fParHistos->Add(hist);
	}

	fFitSteps = new TObjArray(ptarray->GetSize() - 1);
	fFitSteps->SetOwner(kTRUE);
	fFitSteps->SetName("fFitSteps");
	for (Int_t iBin = 0; iBin < fPtAxis->GetNbins(); iBin++) {
		TObjArray* arr_tmp = new TObjArray(10);
		arr_tmp->SetOwner(kTRUE);
		arr_tmp->SetName(Form("Bin_%i",iBin + 1));
		fFitSteps->AddAtAndExpand(arr_tmp,iBin);
	}

	// Create "varied" raw spectra, for calculating systematic errors.
	TArrayI* allparsforsystematics = fitter->GetAllParsForSystematics();
	fRawSpectrumVar = new TObjArray(2 * allparsforsystematics->GetSize());
	fRawSpectrumVar->SetOwner(kTRUE);
	fRawSpectrumVar->SetName("fRawSpectrumVar");

	for (Int_t iPar = 0; iPar < allparsforsystematics->GetSize(); iPar++) {
		const char* name = ((const TString*)parnames->At(allparsforsystematics->At(iPar)))->Data();

		// Create spectrum with one parameter varied up or down.
		TH1F* histup = new TH1F(Form("%s_up", name),"Spectrum;p_{T} (GeV/c);Count", (ptarray->GetSize() - 1), ptarray->GetArray());
		fRawSpectrumVar->AddAt(histup, 2 * iPar);
		TH1F* histdown = new TH1F(Form("%s_down", name),"Spectrum;p_{T} (GeV/c);Count", (ptarray->GetSize() - 1), ptarray->GetArray());
		fRawSpectrumVar->AddAt(histdown, 2 * iPar + 1);
	}

	// Create array of different unidentified spectra.
	fUnIDSpectra = new TObjArray(4);
	fUnIDSpectra->SetOwner(kTRUE);
	fUnIDSpectra->SetName("fUnIDSpectra");

	// Sum of all TPC/TOF bins (also under/overflow)
	TH1F* hUnIDAllBins = new TH1F("hUnIDAllBins", "Total Count; p_{T} (GeV/c); N_{ch}", (ptarray->GetSize() - 1), ptarray->GetArray());
	hUnIDAllBins->SetDirectory(0);
	fUnIDSpectra->Add(hUnIDAllBins);

	// Integral over fitted function (also mismatch if available)
	TH1F* hUnIDTotalIntegral = new TH1F("hUnIDTotalIntegral", "Integral over fit functions; p_{T} (GeV/c); N_{ch}", (ptarray->GetSize() - 1), ptarray->GetArray());
	hUnIDTotalIntegral->SetDirectory(0);
	fUnIDSpectra->Add(hUnIDTotalIntegral);

	// Sum of TPC/TOF bins in fit region only.
	TH1F* hUnIDBinsInFitRegion = new TH1F("hUnIDBinsInFitRegion", "Count in fit region; p_{T} (GeV/c); N_{ch}", (ptarray->GetSize() - 1), ptarray->GetArray());
	hUnIDBinsInFitRegion->SetDirectory(0);
	fUnIDSpectra->Add(hUnIDBinsInFitRegion);

	// Integral over fitted function (but only within the fit region.)
	TH1F* hUnIDIntegralOverFitRegion = new TH1F("hUnIDIntegralOverFitRegion", "Integral over fit functions (in fit region); p_{T} (GeV/c); N_{ch}", (ptarray->GetSize() - 1), ptarray->GetArray());
	hUnIDIntegralOverFitRegion->SetDirectory(0);
	fUnIDSpectra->Add(hUnIDIntegralOverFitRegion);

	delete allparsforsystematics;

	fOuputHistosAreCreated = kTRUE;
	cout << Form("%s -> INFO: Output histograms are initialized...",__func__) << endl;
	
	return fOuputHistosAreCreated;

}

// -----------------------------------------------------------------------
Bool_t AliSpectrumDiHadronPID::InitializeFitters() {

	// (Re)initializes the fitter.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fUsedDetectors) {cout << Form("%s -> ERROR: Used detectors not set.",__func__) << endl; return kFALSE;}
	if (!fUsedDetectors.Contains("TPC") && !fUsedDetectors.Contains("TOF"))  {cout << Form("%s -> ERROR: Not a valid value for used detectors.",__func__) << endl; return kFALSE;}
	if (fMassAssumption == -1) {cout << Form("%s -> ERROR: Mass assumption not set.",__func__) << endl; return kFALSE;}
	if (!fPtAxis) {cout << Form("%s -> ERROR: Pt axis has not been provided.",__func__) << endl; return kFALSE;}
	if (fFitters) {cout << Form("%s -> ERROR: It's not possible to reinitialize the fitter.",__func__) << endl; return kFALSE;}

	fFitters = new TObjArray(fPtAxis->GetNbins());
	fFitters->SetName("fFitters");
	fFitters->SetOwner(kTRUE);

	for (Int_t iBin = 0; iBin < fPtAxis->GetNbins(); iBin++) {

		// Pointer to base class of fitter.
		AliFitterBaseDiHadronPID* tmp = 0x0;

		if (fUsedDetectors.Contains("TPC")) {
			fUsedDetectors = "TOFTPC"; 
			tmp = new AliFitterTOFTPCDiHadronPID();
		} else {
			fUsedDetectors = "TOF";
			tmp = new AliFitterTOFDiHadronPID();
		} 

		// Break out if creation of one of the fitters fails.
		if (!tmp) {
			cout << Form("%s -> ERROR: Could not create fitter for bin %i.",__func__,iBin) << endl;
			delete fFitters;
			fFitters  = 0x0;
			return kFALSE;
		}

		tmp->SetDebugLevel(fDebug);
		tmp->SetPrintHessian(fPrintHessian);
		tmp->SetMassAssumption(fMassAssumption);
		tmp->SetEtaRange(fEtaMin, fEtaMax);

		fFitters->AddAt(tmp, iBin);

	}	

	if (fFitters) {fFittersAreInitialized = kTRUE;
		cout << Form("%s -> INFO: Fitters are initialized...",__func__) << endl;
	}
	return fFittersAreInitialized;

}

/*
// -----------------------------------------------------------------------
Bool_t AliSpectrumDiHadronPID::CheckFitter() const {

	// Checks whether the fitter was set up correctly.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	
	if (!fFitter) {cout << Form("%s -> ERROR: No fitter was initialized.",__func__) << endl; return kFALSE;}
	// Note that if an instance of fFitter exists, then mass assumption and used detector are necessarily set.

	if (fMassAssumption != fFitter->GetMassAssumption()) {
		cout << Form("%s -> ERROR: Mass assumption does not match.",__func__) << endl; 
		return kFALSE;
	}
	//cout<<fFitter->ClassName()<<" "<<fUsedDetectors.Data()<<endl;

	if (TString(fFitter->ClassName()).Contains("TPC") != fUsedDetectors.Contains("TPC")) {
		cout << Form("%s -> ERROR: Used detectors does not match.",__func__) << endl;
		return kFALSE;
	}

	return kTRUE;

}
*/

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::ImportPIDHistograms(const TObjArray* pidhistos, const TAxis* ptaxis, Double_t etaMin, Double_t etaMax) {

	// Clones a TObjArray with PID histograms, and a matching TAxis object.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!pidhistos) {cout << Form("%s -> ERROR: No array of histograms supplied.",__func__) << endl; return;}
	if (!ptaxis) {cout << Form("%s -> ERROR: No axis supplied.",__func__) << endl; return;}
	if (pidhistos->GetEntriesFast() != ptaxis->GetNbins()) 
		{cout << Form("%s -> ERROR: Number of bins in axis does not match number of histos in array.",__func__) << endl; return;}
	if (fBufferIsFull) {cout << Form("%s -> ERROR: I already have PID histograms.",__func__) << endl; return;}

	fPIDhistos = (TObjArray*)pidhistos->Clone();
	fPIDhistos->SetOwner(kTRUE);
	fPIDhistos->SetName("fPIDhistos");

	fPtAxis = (TAxis*)ptaxis->Clone();

	fEtaMin = etaMin;
	fEtaMax = etaMax;

	if (fPIDhistos && fPtAxis) {
		cout << Form("%s -> INFO: PID histograms and Pt axis cloned...",__func__) << endl;
		fBufferIsFull = kTRUE;
	}
}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::ImportMismatchHistograms(const TObjArray* mismatchhistos) {

	// Clones a TObjArray of mismatch histograms. This method should only be called
	// after the pid histograms have been supplied.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fBufferIsFull) {cout << Form("%s -> ERROR: First call ImportPIDHistograms().",__func__) << endl; return;}
	if (!mismatchhistos) {cout << Form("%s -> ERROR: No array of histograms supplied.",__func__) << endl; return;}	
	if (mismatchhistos->GetEntriesFast() != fPtAxis->GetNbins()) 
		{cout << Form("%s -> ERROR: Number of bins in axis does not match number of histos in array.",__func__) << endl; return;}

	// If there were already mismatch histograms in the buffer, then delete those.
	if (fMismatchHistos) {
		cout << Form("%s -> WARNING: Deleting previously supplied mismatch histograms.",__func__) << endl;
		delete fMismatchHistos; 
		fMismatchHistos = 0x0;
	}

	fMismatchHistos = (TObjArray*)mismatchhistos->Clone();
	fMismatchHistos->SetOwner(kTRUE);
	fMismatchHistos->SetName("fMismatchHistos");

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::ImportEfficiency(const TH1F* hist) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (fEfficiency) {
		cout << Form("%s -> WARNING: Deleting previous efficiency histogram.",__func__) << endl;
		delete fEfficiency;
		fEfficiency = 0x0;
	}

	//cout<<"test: "<<hist<<endl;
	// Efficiency may have a different binning.
	fEfficiency = (TH1F*)hist->Clone("fEfficiency");
	//fEfficiency = AliHistToolsDiHadronPID::RebinVariableBinning(tmp, fRawSpectrum, kFALSE);

	//delete tmp;

	if (fEfficiency) {
		cout << Form("%s -> INFO: Effociency histogram cloned...",__func__) << endl;
	}
}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::Browse(TBrowser* b) {

	// Method called by the TBrowser when you double click on the object.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	if (fCorrSpectrum) {
		DrawCorrSpectrum();
	} else {
		DrawRawSpectrum();
	}

	b->Add(fParHistos);
	b->Add(fRawSpectrumVar);
	b->Add(fPIDhistos);
	b->Add(fMismatchHistos);
	b->Add(fUnIDSpectra);

	if (fRawSpectrumReb) {b->Add(fRawSpectrumReb);}
	if (fRawSpectrumVarReb) {b->Add(fRawSpectrumVarReb);}
	if (fCorrSpectrum) {b->Add(fCorrSpectrum);}
	if (fCorrSpectrumVar) {b->Add(fCorrSpectrumVar);}
	if (fCorrSpectrumReb) {b->Add(fCorrSpectrumReb);}
	if (fCorrSpectrumVarReb) {b->Add(fCorrSpectrumVarReb);}

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::BrowseThis() {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Creates a TBrowser to browse this instance.
	fBrowser = new TBrowser("br", this);

	// Saves a pointer to the TBrowsers pad.
	fBrowserPad = gPad;

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::WriteToFile(const char* filename) const {

	// Updates a file, to replace the current instance
	// with the saved one.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	TFile* fout = TFile::Open(filename, "update");

	// Delete the instance from the file before writing.
	fout->Delete(Form("%s;1", this->GetName()));
	this->Write();

	fout->Close();

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::FitBin(Int_t PtBin, Int_t strategy, Bool_t applyPenalties) {
	
	// Fits one of the histograms in fPIDhistos using the 
	// indicated strategy, and updates spectrum/ QA histograms.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fBufferIsFull) {cout << Form("%s -> ERROR: Buffer has no contents.",__func__) << endl; return;}
	if (PtBin < 1) {cout << Form("%s -> ERROR: Non-positive bin number.",__func__) << endl; return;}
	if (PtBin > fPtAxis->GetNbins()) {cout << Form("%s -> ERROR: Bin number out of range.",__func__) << endl; return;}	

	// If fitters are not available, they are created (for all pT bins).	
	if (!fFittersAreInitialized) {
		Bool_t init = InitializeFitters();
		if (!init) {return;}
	}

	// Based on imported histograms and the fitter, histograms are created.
	if (!fOuputHistosAreCreated) {
		Bool_t init = InitializeHistograms();
		if (!init) {return;}
	}

	// Get a pointer to the desired fitter.
	AliFitterBaseDiHadronPID* currentfitter = (AliFitterBaseDiHadronPID*)(fFitters->At(PtBin - 1));

	currentfitter->SetHistogram((TH1*)fPIDhistos->At(PtBin - 1));
	if (fMismatchHistos) {currentfitter->SetMismatchedBackground((TH1*)fMismatchHistos->At(PtBin - 1));}
	currentfitter->SetPtRange(fPtAxis->GetBinLowEdge(PtBin), fPtAxis->GetBinUpEdge(PtBin));

	// Apply standard peak dev penalty if requested.
	if (applyPenalties) {
		currentfitter->SetPeakDevPenalty(kTRUE,40.,2.);
		currentfitter->SetPeakSigmaPenalty(kTRUE,40.,2.);
	}

	// Perform the fit with desired strategy.
	cout << "Fitting bin: " << PtBin << endl;
	TObjArray* atmp = (TObjArray*)fFitSteps->At(PtBin - 1);
	atmp->Delete();
	if (fIsPbPb) {
		currentfitter->DoFitWithStrategyPbPb(strategy, atmp, fCompanion,0,0,0/*, vary_par, vary_species, vary_updown*/);
	} else {
		currentfitter->DoFitWithStrategypp(strategy, atmp, fCompanion,0,0,0/*, vary_par, vary_species, vary_updown*/);
	}

	// Update the spectrum.
	fRawSpectrum->SetBinContent(PtBin, currentfitter->GetIntegral(currentfitter->GetMassAssumption()));
	fRawSpectrum->SetBinError(PtBin, currentfitter->GetIntegralError(currentfitter->GetMassAssumption()));

	// Update the unidentified spectra (for comparison to identified)
	((TH1F*)(fUnIDSpectra->FindObject("hUnIDAllBins")))->SetBinContent(PtBin, currentfitter->GetSumNChargedAllBins());
	((TH1F*)(fUnIDSpectra->FindObject("hUnIDBinsInFitRegion")))->SetBinContent(PtBin, currentfitter->GetSumNChargedInFitRegion());
	((TH1F*)(fUnIDSpectra->FindObject("hUnIDTotalIntegral")))->SetBinContent(PtBin, currentfitter->GetIntegralOverFitFunction());
	((TH1F*)(fUnIDSpectra->FindObject("hUnIDIntegralOverFitRegion")))->SetBinContent(PtBin, currentfitter->GetIntegralOverFitFunctionInFitRegion());

	// Update the chi^2 histogram.
	fChiSquaredDOF->SetBinContent(PtBin, currentfitter->GetCurrentChiSquaredPerDOF());

	// Updating the fit status histogram.
	fFitStatus->SetBinContent(PtBin, currentfitter->GetFitStatus());

	// Update the QA histograms of all parameters.
	for (Int_t iPar = 0; iPar < currentfitter->GetNumberOfParams(); iPar++) {
		TH1F* hist = (TH1F*)fParHistos->At(iPar);
		//if (!(currentfitter->GetIsParFixed()[iPar])) {
		if ((currentfitter->GetIsParFixed())->At(iPar)) {hist->SetLineColor(kRed); hist->SetMarkerColor(kRed);}
			hist->SetBinContent(PtBin, (currentfitter->GetParValues())->At(iPar));
			hist->SetBinError(PtBin, (currentfitter->GetParErrors())->At(iPar));
		//}
	}  

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::FitBins(Int_t BinMin, Int_t BinMax, Int_t strategy/*,
		const char* vary_par, Int_t vary_species, Bool_t vary_updown*/) {
	
	// Fits a certain number of bins.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fBufferIsFull) {cout << Form("%s -> ERROR: Buffer has no contents.",__func__) << endl; return;}
	if (BinMin > BinMax) {cout << Form("%s -> ERROR: BinMin > BinMax.",__func__) << endl; return;}
	if (BinMin < 1) {cout << Form("%s -> ERROR: Non-positive bin number.",__func__) << endl; return;}
	if (BinMax > fPtAxis->GetNbins()) {cout << Form("%s -> ERROR: Bin number out of range.",__func__) << endl; return;}		

	for (Int_t iBin = BinMin; iBin < (BinMax + 1); iBin++) {FitBin(iBin, strategy/*, vary_par, vary_species, vary_updown*/);}

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::FitPtRange(Double_t PtMin, Double_t PtMax/*, Int_t,
		const char*, Int_t, Bool_t*/) {
	
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fBufferIsFull) {cout << Form("%s -> ERROR: Buffer has no contents.",__func__) << endl; return;}
	if (PtMin > PtMax) {cout << Form("%s -> ERROR: BinMin > BinMax.",__func__) << endl; return;}

	/* TODO */

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::FitAll(Int_t strategy/*,
		const char* vary_par, Int_t vary_species, Bool_t vary_updown*/) {
	
	// Fits all the histograms in fPIDhistos.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fBufferIsFull) {cout << Form("%s -> ERROR: Buffer has no contents.",__func__) << endl; return;}

	for (Int_t iBin = 1; iBin < (fPtAxis->GetNbins() + 1); iBin++) {FitBin(iBin, strategy/*, vary_par, vary_species, vary_updown*/);}

}

// -----------------------------------------------------------------------
TH1F* AliSpectrumDiHadronPID::GetCloneRawSpectrum(Bool_t rebinned) const {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	TH1F* hout;

	if (rebinned) {
		if (!fRawSpectrumReb) {cout << Form("%s -> ERROR: No rebinned raw spectrum known.",__func__) << endl; return 0x0;}
		hout = (TH1F*)fRawSpectrumReb->Clone(Form("RawSpectrumReb_%i",fMassAssumption));
	} else {
		if (!fRawSpectrum) {cout << Form("%s -> ERROR: No raw spectrum known.",__func__) << endl; return 0x0;}	
		hout = (TH1F*)fRawSpectrum->Clone(Form("RawSpectrum_%i",fMassAssumption));
	}

	hout->SetDirectory(0); return hout;
}

// -----------------------------------------------------------------------
TH1F* AliSpectrumDiHadronPID::GetCloneCorrSpectrum(Bool_t rebinned) const {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	TH1F* hout;

	if (rebinned) {
		if (!fCorrSpectrumReb) {cout << Form("%s -> ERROR: No rebinned corrected spectrum known.",__func__) << endl; return 0x0;}
		hout = (TH1F*)fCorrSpectrumReb->Clone(Form("CorSpectrumReb_%i",fMassAssumption));
	} else {
		if (!fCorrSpectrum) {cout << Form("%s -> ERROR: No corrected spectrum known.",__func__) << endl; return 0x0;}	
		hout = (TH1F*)fCorrSpectrum->Clone(Form("CorSpectrum_%i",fMassAssumption));
	}

	hout->SetDirectory(0); return hout;
}

// -----------------------------------------------------------------------
TH1F* AliSpectrumDiHadronPID::GetCloneParHisto(Int_t par) const {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!(fParHistos->At(par))) {cout << Form("%s -> ERROR: No histogram found for par %i.",__func__, par) << endl; return 0x0;}

	return (TH1F*)((fParHistos->At(par))->Clone());
}

// -----------------------------------------------------------------------
TH1F* AliSpectrumDiHadronPID::GetMismatchFraction() const {

	// Returns a histogram with the mismatch fraction. The idea is as follows:
	//  - We have a number of mismatches, from the fit, which also takes into account
	//    the part of PID plot outside of the plot range, since the mismatch histograms
	//    contain over/underflow.
	//  - The "data" histogram doesn't contain over and underflow, so simply doing a bin count
	//    there, wouldn't give the correct number of particles.
	//  - If we assume that the shape of the "data" histogram coincides with the shape of the
	//    PID histogram outside of the plotrange, we can however calculate the mismatch fraction
	//    as follows:
	//
	//     MMfrac = NMM / (Ndata + MMoverflowfrac * NMM)
	//

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	TH1F* hout = GetCloneParHisto(0);
	hout->SetName("hMMfrac");
	hout->SetTitle("Mismatch Fraction");
	hout->SetDirectory(0);

	TH1F* unidentifiedcount = (TH1F*)fUnIDSpectra->At(0);

	for (Int_t iBin = 1; iBin <= hout->GetNbinsX(); ++iBin) {

		Double_t NMM = hout->GetBinContent(iBin);
		Double_t Ndata = unidentifiedcount->GetBinContent(iBin);

		TH2F* MM2Dhist = (TH2F*)fMismatchHistos->At(iBin - 1);  

		Double_t MMcountAll = MM2Dhist->GetEntries();		// Overflow and in range,
		Double_t MMcountInRange = MM2Dhist->Integral();	// only in range.
		Double_t MMoverflowfrac = (MMcountAll - MMcountInRange) / MMcountAll;

		Double_t MMfrac = NMM / (Ndata + MMoverflowfrac * NMM);
		//Double_t MMfrac = NMM/Ndata;

		hout->SetBinContent(iBin, MMfrac);

		//cout << "Bin: " << iBin << " NMM: " << NMM << " Ndata: " << Ndata << " MMcountAll: " << MMcountAll << " MMcountInRange: " << MMcountInRange << " MMoverflowfrac: " << MMoverflowfrac << endl;

	}

	return hout;

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::DetermineSystematicsForBin(Int_t PtBin, Int_t fitstrategy, Int_t syststrategy) {

	// Available strategies for determining systematics described in the function
	// AliFitterBaseDiHadronPID::DetermineYieldAfterVaryingPar

	// Systematic errors will be determined.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fBufferIsFull) {cout << Form("%s -> ERROR: Input buffer has no contents.",__func__) << endl; return;}
	if (!fFittersAreInitialized) {cout << Form("%s -> ERROR: No fitters have been initialized.",__func__) << endl; return;}
	if (!fOuputHistosAreCreated) {cout << Form("%s -> ERROR: No output histograms have been created.",__func__) << endl; return;}
	if (PtBin < 1) {cout << Form("%s -> ERROR: Non-positive bin number.",__func__) << endl; return;}
	if (PtBin > fPtAxis->GetNbins()) {cout << Form("%s -> ERROR: Bin number out of range.",__func__) << endl; return;}	

	// Get a pointer to the desired fitter.
	AliFitterBaseDiHadronPID* currentfitter = (AliFitterBaseDiHadronPID*)(fFitters->At(PtBin - 1));
	if (!currentfitter) {cout << Form("%s -> ERROR: Could not get a pointer to the fitter for pt bin %i.",__func__,PtBin) << endl; return;}

	// Check if the fit for the bin has succeeded. If not, then systematics cannot be determined, and the fit needs to be done first.
	if (currentfitter->GetFitStatus() != 0) {
		if (currentfitter->GetFitStatus() == -1) {cout << Form("%s -> ERROR: Fit for pt bin %i has not been performed (note that fit details are lost when saving).",__func__,PtBin) << endl;} 
		else {cout << Form("%s -> ERROR: Fitter for pt bin %i has status %i.",__func__,PtBin,currentfitter->GetFitStatus()) << endl;}
		return;
	}

	// Obtain a list of parameters that are varied for determining systematic errors.
	TArrayI* parstobevaried = currentfitter->GetUsedParsForSystematics(fitstrategy);

	const TObjArray* parnames = currentfitter->GetParNames();

	cout << endl;
	cout << " Determining systematics for bin " << PtBin << endl;
	cout << "----------------------------------------" << endl;

	// Vary all parameters up and down by an amount determined by the fitter.
	for (Int_t iPar = 0; iPar < parstobevaried->GetSize(); iPar++) {

		const Int_t partovary = parstobevaried->At(iPar);
		const char* nametmp = ((TString*)(parnames->At(partovary)))->Data();

		cout << " -> Varying parameter: " << nametmp << endl;

		// Vary up...
		TH1F* histup = (TH1F*)(fRawSpectrumVar->FindObject(Form("%s_up",nametmp)));
		histup->SetBinContent(PtBin, currentfitter->DetermineYieldAfterVaryingPar(partovary, kTRUE, syststrategy));

		// ...and down.
		TH1F* histdown = (TH1F*)(fRawSpectrumVar->FindObject(Form("%s_down",nametmp)));
		histdown->SetBinContent(PtBin, currentfitter->DetermineYieldAfterVaryingPar(partovary, kFALSE, syststrategy));

	}

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::DetermineSystematicsForBins(Int_t BinMin, Int_t BinMax, Int_t fitstrategy, Int_t syststrategy) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fBufferIsFull) {cout << Form("%s -> ERROR: Buffer has no contents.",__func__) << endl; return;}
	if (BinMin > BinMax) {cout << Form("%s -> ERROR: BinMin > BinMax.",__func__) << endl; return;}
	if (BinMin < 1) {cout << Form("%s -> ERROR: Non-positive bin number.",__func__) << endl; return;}
	if (BinMax > fPtAxis->GetNbins()) {cout << Form("%s -> ERROR: Bin number out of range.",__func__) << endl; return;}		

	for (Int_t iBin = BinMin; iBin < (BinMax + 1); iBin++) {DetermineSystematicsForBin(iBin, fitstrategy, syststrategy);}

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::DetermineSystematicsForPtRange(Double_t /*PtMin*/, Double_t /*PtMax*/, Int_t /*fitstrategy*/, Int_t /*syststrategy*/) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	/* TODO */

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::DetermineSystematicsForAllBins(Int_t fitstrategy, Int_t syststrategy) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	for (Int_t iBin = 1; iBin < (fPtAxis->GetNbins() + 1); iBin++) {DetermineSystematicsForBin(iBin, fitstrategy, syststrategy);}

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::SetDebugLevel(Int_t debuglevel) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	fDebug = debuglevel;
	if (fFitters) {
		TIterator* iter = fFitters->MakeIterator();
		AliFitterBaseDiHadronPID* currentfitter = (AliFitterBaseDiHadronPID*)iter->Next();
		while (currentfitter) {
			currentfitter->SetDebugLevel(debuglevel);
			currentfitter = (AliFitterBaseDiHadronPID*)iter->Next();	
		}

		delete iter;
	}
}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::SetPrintHessian(Bool_t printhessian) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	fPrintHessian = printhessian;
	if (fFitters) {
		TIterator* iter = fFitters->MakeIterator();
		AliFitterBaseDiHadronPID* currentfitter = (AliFitterBaseDiHadronPID*)iter->Next();
		while (currentfitter) {
			currentfitter->SetPrintHessian(printhessian);
			currentfitter = (AliFitterBaseDiHadronPID*)iter->Next();	
		}

		delete iter;
	}
}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::PrintStatus() {

	// Prints the status of the instance.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	cout << " Status" << endl;
	cout << "------------------------------" << endl;
	cout << " Name: " << fName << endl;
	cout << " Title: " << fTitle << endl;
	cout << " Mass Assumption: " << fMassAssumption << endl;
	cout << " Used Detectors: " << fUsedDetectors.Data() << endl;
	cout << " Debug level: " << fDebug << endl;
	cout << " Fitter is" << (fFittersAreInitialized ? " " : " not ") << "initialized." << endl;
	cout << " Output histograms are" << (fOuputHistosAreCreated ? " " : " not ") << "initialized." << endl;
	cout << " Input histograms were" << (fBufferIsFull ? " " : " not ") << "supplied." << endl; 
	cout << "------------------------------" << endl;

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::PrintPtAxis() {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fPtAxis) {cout << Form("%s -> ERROR: No pT axis known...",__func__) << endl; return;}

	for (Int_t iBin = 1; iBin <= fPtAxis->GetNbins(); iBin++) {
		cout<<"Bin: "<<iBin<<", "<<fPtAxis->GetBinLowEdge(iBin)<< " < p_T < "<<fPtAxis->GetBinUpEdge(iBin)<<endl; 
	}

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::PrintEtaRange() {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fPtAxis) {cout << Form("%s -> ERROR: No pT axis known...",__func__) << endl; return;}

	cout << "etaMin: " << fEtaMin << " etaMax: " << fEtaMax << endl;

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::PrintListOfParameters() {

	// Prints a list of all fit parameters.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	/* TODO */ 

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::DrawRawSpectrum() {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fRawSpectrum) {cout << Form("%s -> ERROR: No raw spectrum known.",__func__) << endl; return;}

	if (fRawSpectrum) {
		ResetCurrentPad();		
		fRawSpectrum->DrawClone();
		gPad->Update();
	}

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::DrawCorrSpectrum() {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fCorrSpectrum) {cout << Form("%s -> ERROR: No corrected spectrum known.",__func__) << endl; return;}

	if (fCorrSpectrum) {
		ResetCurrentPad();		
		fCorrSpectrum->DrawClone();
		gPad->Update();
	}

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::DrawEfficiency() {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fEfficiency) {cout << Form("%s -> ERROR: No efficiency known.",__func__) << endl; return;}

	ResetCurrentPad();		
	fEfficiency->DrawClone();
	gPad->Update();

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::DrawFitStatus() {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fFitStatus) {cout << Form("%s -> ERROR: No fit status histogram.",__func__) << endl; return;}

	if (fFitStatus) {
		ResetCurrentPad();		
		fFitStatus->DrawClone();
		gPad->Update();
	}
}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::DrawChiSquared() {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fChiSquaredDOF) {cout << Form("%s -> ERROR: No chi squared histogram known.",__func__) << endl; return;}

	if (fChiSquaredDOF) {
		ResetCurrentPad();		
		fChiSquaredDOF->DrawClone();
		gPad->Update();
	}

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::DrawParHisto(Int_t par) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fParHistos) {cout << Form("%s -> ERROR: No parameter histograms known.",__func__) << endl; return;}

	if (fParHistos->At(par)) {
		ResetCurrentPad();
		((TH1F*)fParHistos->At(par))->DrawClone();
		gPad->Update();
	}

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::DrawFit(Int_t bin) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fFitSteps) {cout << Form("%s -> ERROR: You haven't performed any fits.",__func__) << endl; return;}

	if (fFitSteps->At(bin - 1)) {

		TObjArray* atmp = (TObjArray*)(fFitSteps->At(bin - 1));
		TCanvas* ctmp = (TCanvas*)atmp->FindObject("Fit");

		if (ctmp) {
			ResetCurrentPad();
			ctmp->DrawClonePad();
			gPad->Update();	
		}
	}
}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::DrawPull(Int_t bin) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fFitSteps) {cout << Form("%s -> ERROR: You haven't performed any fits.",__func__) << endl; return;}

	if (fFitSteps->At(bin - 1)) {

		TObjArray* atmp = (TObjArray*)(fFitSteps->At(bin - 1));
		TCanvas* ctmp = (TCanvas*)atmp->FindObject("Pull");

		if (ctmp) {
			ResetCurrentPad();
			ctmp->DrawClonePad();
			gPad->Update();	
		}
	}
}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::DrawTOFProjection(Int_t bin) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fFitSteps) {cout << Form("%s -> ERROR: You haven't performed any fits.",__func__) << endl; return;}

	if (fFitSteps->At(bin - 1)) {

		TObjArray* atmp = (TObjArray*)(fFitSteps->At(bin - 1));
		TCanvas* ctmp = (TCanvas*)atmp->FindObject("TOFProjection");

		if (ctmp) {
			ResetCurrentPad();
			ctmp->DrawClonePad();
		}
	}
}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::DrawTPCProjection(Int_t bin) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fFitSteps) {cout << Form("%s -> ERROR: You haven't performed any fits.",__func__) << endl; return;}

	if (fFitSteps->At(bin - 1)) {

		TObjArray* atmp = (TObjArray*)(fFitSteps->At(bin - 1));
		TCanvas* ctmp = (TCanvas*)atmp->FindObject("TPCProjection");

		if (ctmp) {
			ResetCurrentPad();
			ctmp->DrawClonePad();
			gPad->Update();	
		}
	}
}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::DrawMismatchFraction() {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	ResetCurrentPad();
	GetMismatchFraction()->Draw();
	gPad->Update();

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::ExportTOFProjection(Int_t bin, Int_t step) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fFitSteps) {cout << Form("%s -> ERROR: You haven't performed any fits.",__func__) << endl; return;}

	if (fFitSteps->At(bin - 1)) {

		TObjArray* atmp = (TObjArray*)(fFitSteps->At(bin - 1));
		TCanvas* ctmp = (TCanvas*)atmp->FindObject("TOFProjection");

		if (ctmp) {

			TVirtualPad* ptmp = ctmp->GetPad(step);
			ptmp->SaveAs(Form("TOFprojection_Bin_%i_Step_%i.pdf",bin,step));

		}
	}
}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::ExportTPCProjection(Int_t bin, Int_t step) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fFitSteps) {cout << Form("%s -> ERROR: You haven't performed any fits.",__func__) << endl; return;}

	if (fFitSteps->At(bin - 1)) {

		TObjArray* atmp = (TObjArray*)(fFitSteps->At(bin - 1));
		TCanvas* ctmp = (TCanvas*)atmp->FindObject("TPCProjection");

		if (ctmp) {

			TVirtualPad* ptmp = ctmp->GetPad(step);
			ptmp->SaveAs(Form("TPCprojection_Bin_%i_Step_%i.pdf",bin,step));

		}
	}
}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::ApplyGeantFlukaCorrection() {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	switch (fMassAssumption) {

		case 0:
			cout << Form("%s -> INFO: Pions, not applying GEANT/FLUKA correction.",__func__) << endl;	

		break;

		case 1:
			cout << Form("%s -> INFO: Kaons, applying GEANT/FLUKA correction.",__func__) << endl;	
			/* TODO */ 
		break;

		case 2:
			cout << Form("%s -> INFO: Protons, applying GEANT/FLUKA correction.",__func__) << endl;	
			/* TODO */ 
		break;

		default:
			cout << Form("%s -> ERROR: Mass assumption was set to: %i.",__func__,fMassAssumption) << endl;

	}

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::ApplyEfficiencyCorrection() {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fCorrSpectrum) {cout << Form("%s -> ERROR: Corrected spectrum not available.",__func__) << endl; return;}

	// fEfficiency->Scale(1.,"width");
	fCorrSpectrum->Scale(1.,"width");
	fCorrSpectrum->Divide(fEfficiency);

	cout << Form("%s -> INFO: Spectrum corrected and scaled to binwidth.",__func__) << endl;

}


// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::CreateCorrectedSpectrum() {

	// FIXME: CORRECTION IS VERY AD HOC, ONLY WORKS FOR AXES OF 15 BINS.

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	if (!fEfficiency) {cout << "ERROR: No efficiency histogram known." << endl; return;}

	if (fCorrSpectrum) {delete fCorrSpectrum; fCorrSpectrum = 0x0;}
	if (fCorrSpectrumVar) {delete fCorrSpectrumVar; fCorrSpectrumVar = 0x0;}

	fCorrSpectrum = (TH1F*)fRawSpectrum->Clone("fCorrSpectrum");
	
	fCorrSpectrumVar = (TObjArray*)fRawSpectrumVar->Clone("fCorrSpectrumVar");

	fCorrSpectrumVar->SetOwner(kTRUE);

	cout<< fCorrSpectrum->GetBinContent(1)<<" "<<fEfficiency->GetBinContent(1) << " ";

	fCorrSpectrum->Divide(fEfficiency);

	cout<<fCorrSpectrum->GetNbinsX()<< " " << fEfficiency->GetNbinsX()<<endl;
	for (Int_t i = 1; i < 15; ++i) {
		cout<<fCorrSpectrum->GetBinLowEdge(i)<< " " << fEfficiency->GetBinLowEdge(i)<<endl;
		cout<<fCorrSpectrum->GetBinWidth(i)<< " " << fEfficiency->GetBinWidth(i)<<endl;
	
	}
	//cout<<fCorrSpectrum->GetBinLowEdge(16)<< " " << fEfficiency->GetBinLowEdge(16)<<endl;

	cout<< fCorrSpectrum->GetBinContent(1)<<endl;


	for (Int_t iHist = 0; iHist < fCorrSpectrumVar->GetEntriesFast(); ++iHist) {
		TH1F* histtmp = (TH1F*)fCorrSpectrumVar->At(iHist);
		histtmp->Divide(fEfficiency);
	}

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::CreateRebinnedSpectrum() {

	// FIXME: REBINNING IS VERY AD HOC, ONLY WORKS FOR AXES OF 15 BINS.

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	if (fRawSpectrum) {
		if (fRawSpectrumReb) {delete fRawSpectrumReb; fRawSpectrumReb = 0x0;}
		fRawSpectrumReb = Rebin(fRawSpectrum);
	}

	if (fCorrSpectrum) {
		if (fCorrSpectrumReb) {delete fCorrSpectrumReb; fCorrSpectrumReb = 0x0;}
		fCorrSpectrumReb = Rebin(fCorrSpectrum);
	}

	if (fRawSpectrumVar) {
		if (fRawSpectrumVarReb) {delete fRawSpectrumVarReb; fRawSpectrumVarReb = 0x0;}
		fRawSpectrumVarReb = Rebin(fRawSpectrumVar);
	}

	if (fCorrSpectrumVar) {
		if (fCorrSpectrumVarReb) {delete fCorrSpectrumVarReb; fCorrSpectrumVarReb = 0x0;}
		fCorrSpectrumVarReb = Rebin(fCorrSpectrumVar);
	}

}

// -----------------------------------------------------------------------
TH1F* AliSpectrumDiHadronPID::Rebin(TH1F* hist) {

	// FIXME: REBINNING IS VERY AD HOC, ONLY WORKS FOR AXES OF 15 BINS.

	// Rebins the first 14 bins.
	// Errors added in quadrature.
	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// FIXME!!
	//const TArrayD* oldaxis = (hist->GetXaxis())->GetXbins();
	Float_t newaxis[10] = {2.0,2.2,2.4,2.6,3.0,3.4,3.8,4.2,4.6,5.0};
	TH1F* histout = new TH1F(Form("%s_reb",hist->GetName()),hist->GetTitle(),9,newaxis);

	for (Int_t iBin = 1; iBin <= 6; ++iBin) {

		Int_t oldbin = 2 * (iBin - 1) + 1;
		histout->SetBinContent(iBin,(hist->GetBinContent(oldbin)+ hist->GetBinContent(oldbin + 1)));
		histout->SetBinError(iBin,TMath::Sqrt(hist->GetBinError(oldbin)*hist->GetBinError(oldbin) + 
			hist->GetBinError(oldbin + 1)*hist->GetBinError(oldbin + 1)));

	}

	for (Int_t iBin = 7; iBin <= 9; ++iBin) {
		Int_t oldbin = iBin + 6;
		histout->SetBinContent(iBin,hist->GetBinContent(oldbin));
		histout->SetBinError(iBin,hist->GetBinError(oldbin)); 
	}

	histout->SetDirectory(0);

	return histout;

}

// -----------------------------------------------------------------------
TObjArray* AliSpectrumDiHadronPID::Rebin(TObjArray* array) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	const Int_t entries = array->GetEntriesFast();
	TObjArray* aout = new TObjArray(entries);
	aout->SetOwner(kTRUE);

	for (Int_t iHist = 0; iHist < entries; ++iHist) {

		aout->AddLast(Rebin((TH1F*)array->At(iHist)));

	}

	return aout;

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::SetCompanionSpectrum(const char* filename) {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	if (fCompanionFile) {
		cout << Form("%s -> INFO: Closing file %s.",__func__,fCompanionFile->GetName()) << endl;
		fCompanionFile->Close(); 
		fCompanionFile = 0x0; 
	}

	// Open the file containing the other spectrum.
	fCompanionFile = TFile::Open(filename);
	if (!fCompanionFile) {cout << Form("%s -> ERROR: File could not be opened.",__func__) << endl; return;}
	
	// Search a spectrum object in the file.
	Int_t iter = 0;
	fCompanion = 0x0;
	TList* keys = fCompanionFile->GetListOfKeys();
	keys->Print();
	do {
		TKey* currentkey = (TKey*)(keys->At(iter));
		cout<<"iter: "<<iter<<" currentkeyclassname: "<<currentkey->GetClassName()<<endl;
		if ( TString(currentkey->GetClassName()).Contains("AliSpectrumDiHadronPID") ) {
			fCompanion = (AliSpectrumDiHadronPID*)currentkey->ReadObj();
		}
		iter++;
	} while (!fCompanion || iter < keys->GetEntries());

	if (fCompanion) {
		cout << Form("%s -> INFO: Companion found!",__func__) << endl;
		//fBrowser->Add(fCompanion);
	} 
	else {cout << Form("%s -> INFO: Companion not found!",__func__) << endl;}

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::CompareRawSpectra() {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	if (!fCompanion) {cout << Form("%s -> ERROR: No other spectrum supplied.",__func__) << endl; return;}

	TH1F* thisspectrum = GetCloneRawSpectrum();
	thisspectrum->SetTitle("This Spectrum");
	TH1F* otherspectrum = fCompanion->GetCloneRawSpectrum();
	otherspectrum->SetTitle("Other Spectrum");

	TCanvas* cvs = AliHistToolsDiHadronPID::CreateSpectraComparison("comp","comp",thisspectrum,otherspectrum,8,kFALSE);	
	ResetCurrentPad();
	cvs->DrawClonePad();
	delete cvs;	

}

// -----------------------------------------------------------------------
void AliSpectrumDiHadronPID::ResetCurrentPad() {

	if (fDebug > 0) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	if (fBrowserPad) {fBrowserPad->cd();}

	gPad->Clear();
	gPad->SetLogy(0);
	gPad->SetLogz(0);

}
