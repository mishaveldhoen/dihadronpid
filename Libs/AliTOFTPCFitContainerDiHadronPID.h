#ifndef ALITOFTPCFITCONTAINERDIHADRONPID_H
#define ALITOFTPCFITCONTAINERDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TNamed.h"
#include "TH2F.h"
#include "TObjArray.h"
#include "TCanvas.h"
#include "AliTOFTPCFitFunctionDiHadronPID.h"
#include "AliSettingsDiHadronPID.h"

class AliTOFTPCFitContainerDiHadronPID : public TNamed {
	
public:
	AliTOFTPCFitContainerDiHadronPID();
	AliTOFTPCFitContainerDiHadronPID(const char* name, const char* title, const TH2F* dataHist, const TH2F* mismatchHist, Double_t ptMin, Double_t ptMax, Double_t etaMin, Double_t etaMax, AliSettingsDiHadronPID::Species massAssumption);
	~AliTOFTPCFitContainerDiHadronPID();

// 	// These methods apply to a desired fit stage. Stage < 0 for last stage
	double 								eval(Int_t fitStage, double* xy) const;
	double 								chi2(Int_t fitStage) const;
	double 								negll(Int_t fitStage) const;
	Double_t							CalculatePenaltyFactor(Int_t fitStage) const;	
	Int_t								GetFitStatus(Int_t fitStage) const;
// 	void 								SetFitStatusForFitStage(Int_t status, Int_t fitStage); // REMOVE THIS

// 	// These methods use/change the parameters of the current fit function. The signature is so that
// 	// they can be warpped in TF1's or TMath::Functor's.
// 	double 								operator()(double* xy, double*) const {return eval(fCurrentFitFunction, xy);}
	double 								currentChi2(const double* pars);
	double 								currentNegll(const double* pars);
	Int_t								GetCurrentFitStatus() const {return GetFitStatus(fCurrentFitFunction);}
// 	void								SetCurrentFitStatus(Int_t status) {SetFitStatusForFitStage(status, fCurrentFitFunction);} // REMOVE THIS

// 	void								ReInitialise(AliTOFTPCFitContainerDiHadronPID* otherContainer);
	void								ResetFitStages();
//	void								DetermineTOFAndTPCRange(); // Done by the fit function.
 	AliTOFTPCFitFunctionDiHadronPID*	CurrentFitStage();
	AliTOFTPCFitFunctionDiHadronPID*	LastFitStage() {return (AliTOFTPCFitFunctionDiHadronPID*)(fFitFunctions->Last());}
 	Bool_t								NewFitStage(Bool_t copyPreviousStage = kTRUE);		// Adds a fit stage.

// 	// Getters
 	AliSettingsDiHadronPID::Species 	GetMassAssumption() const {return fMassAssumption;}
// 	//AliTOFTPCFitFunctionDiHadronPID*	GetFitStage(Int_t fitstage);		// Fit result is owned by the container. 
	Int_t								GetNFitStages() const {return fFitFunctions->GetEntries();}
	TH2F*								GetDataHistogram() {return fData;}
	TH2F*								GetMismatchHistogram() {return fMismatch;}
 	TH2F*								GetEmptyPIDHist(const char* name, const char* title);
// 	Int_t								GetMassAssumption() const {return fMassAssumption;}
	Double_t 							GetMeanPt() const {return ((fPtMax + fPtMin) / 2.);}

	// Visualisation Methods
	void								Print(Option_t*) const;

	void								DrawFitStage(Int_t fitstage, TCanvas* canvas = 0x0, Int_t subpad = 0) const;
	void								DrawResiduals(Int_t fitstage, TCanvas* canvas = 0x0, Int_t subpad = 0) const;
	void								DrawTOFProjection(Int_t fitstage, TCanvas* canvas = 0x0, Int_t subpad = 0) const;
	void								DrawTPCProjection(Int_t fitstage, TCanvas* canvas = 0x0, Int_t subpad = 0) const;
	void								DrawTPCSlices(Int_t fitstage, TString directory = "TPCFitSlices") const;

	void 								DrawCurrentFitStage() const {DrawFitStage(fCurrentFitFunction);} // *MENU*
	void								DrawCurrentTOFProjection() const {DrawTOFProjection(fCurrentFitFunction);} // *MENU*
	void								DrawCurrentTPCProjection() const {DrawTPCProjection(fCurrentFitFunction);} // *MENU*
	void								DrawCurrentTPCSlices() const {DrawTPCSlices(fCurrentFitFunction);} // *MENU*

 	void								SaveFitStages(TString directory, Bool_t saveSlices = kFALSE) const;

	void SetMakeLogPlots(Bool_t flag = kTRUE) { fMakeLogPlots = flag; }
// private:
// 	void								GetTOFHist(Int_t fitStage, Int_t species);
// 	void								GetTPCHist(Int_t fitStage, Int_t species);

private:

	// pointer to settings singleton
	AliSettingsDiHadronPID* 			fSettings; //!

	TObjArray*							fFitFunctions;
	Int_t 								fCurrentFitFunction;
	TH2F*								fData;
	TH2F*								fMismatch;

	// Kinematical information.
	AliSettingsDiHadronPID::Species		fMassAssumption;
	Double_t							fPtMin;
	Double_t							fPtMax;
	Double_t							fEtaMin;
	Double_t							fEtaMax;

	Bool_t                             fMakeLogPlots;
	ClassDef(AliTOFTPCFitContainerDiHadronPID, 3);

};

#endif

