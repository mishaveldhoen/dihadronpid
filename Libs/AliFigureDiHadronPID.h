#ifndef ALIFIGUREDIHADRONPID_H
#define ALIFIGUREDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TNamed.h"
#include "TCanvas.h"
#include "TString.h"
#include "AliGraphDiHadronPID.h"
#include "AliGraphCollectionDiHadronPID.h"

class AliFigureDiHadronPID : public TNamed {
	
public:
	AliFigureDiHadronPID();
	AliFigureDiHadronPID(const char* name, const char* title);
	~AliFigureDiHadronPID();

	void 						AddGraph(AliGraphDiHadronPID* graph);
	void 						AddGraphCollection(AliGraphCollectionDiHadronPID* graphCollection) {fGraphCollections->AddLast(graphCollection);}

	TCanvas* 					Show(Bool_t addLegend = kTRUE, Bool_t logScale = kFALSE, Bool_t showSyst = kTRUE, TPad* pad = 0x0);

	// Axis size getters.
	Double_t					GetMinX();
	Double_t					GetMaxX();
	Double_t					GetMinY(Bool_t useLog = kFALSE);
	Double_t					GetMaxY(Bool_t useLog = kFALSE);

    // Operations.
    void                        Scale(Double_t scaleFactor);
    
	// Custom Axis Size.
	void						SetUseAutoRangeAndDomain(Bool_t useARAD = kTRUE) {fUseAutoRangeAndDomain = useARAD;}
	void						SetCustomRangeAndDomain(Double_t domMin, Double_t domMax, Double_t ranMin, Double_t ranMax);

	// Legend Settings.
	void						SetLegendSize(Double_t Xmin, Double_t Ymin, Double_t Xmax, Double_t Ymax);

    // Label Settings.
    void                        SetXTitle(const TString& title) {fXAxisTitle = title;}
    void                        SetYTitle(const TString& title) {fYAxisTitle = title;}
    void                        SetCustomLabelSizeX(const Double_t size) {fCustomLabelSizeX = size;}
    void                        SetCustomLabelSizeY(const Double_t size) {fCustomLabelSizeY = size;}

	void 						Print(Option_t*) const;

private:
	TObjArray*					fGraphCollections;
	TObjArray*					fOwnedObjects;

	Bool_t						fUseAutoRangeAndDomain;
	Double_t					fDomainMin;
	Double_t					fDomainMax;
	Double_t					fRangeMin;
	Double_t					fRangeMax;

	Double_t					fLegendXmin;
	Double_t					fLegendXmax;
	Double_t					fLegendYmin;
	Double_t					fLegendYmax;

    TString                     fXAxisTitle;
    TString                     fYAxisTitle;
    Double_t                    fCustomLabelSizeX;
    Double_t                    fCustomLabelSizeY; 

	ClassDef(AliFigureDiHadronPID, 1);

};

#endif
