#ifndef ALISETTINGSDIHADRONPID_H
#define ALISETTINGSDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "Rtypes.h"
#include "TObject.h"
#include "TAxis.h"
#include "TH2I.h"
#include "TString.h"
#include "AliMiniTrackDiHadronPID.h"
#include "AliMiscToolsDiHadronPID.h"

/**
 * \brief Singleton class, encapsulating analysis settings.
 * 
 * \todo The AliSettingsDiHadronPID class has become somewhat bulky. 
 * It was initially intended to be a lean class encapsulating the global 
 * settings, and therefore only have setters and getters. Over time also 
 * several methods were added that explicitly \a do things for the user, 
 * for example methods like: GetMismatchRegionTOFMinOrMaxBin, 
 * and IsTrackSelectedForPID. Ideally these methods should be moved to 
 * another class. Furthermore, it would also be nicer to move the 
 * enums (Species, Charge, ...) to another class, and to make them into
 * a namespace.
 */
class AliSettingsDiHadronPID : public TObject {

public:
	enum Species {kPion, kKaon, kProton, kDeuteron, kElectron, kMuon, kUnknownSpecies};
	enum Charge {kAllCharged, kPositive, kNegative};
	enum BeamType {kPbPb, kPPb, kPP}; 
	enum FitMethod {kChi2, kLogLikelihood};

private:
	AliSettingsDiHadronPID(Bool_t isConfigured = kFALSE);
	AliSettingsDiHadronPID(const AliSettingsDiHadronPID&);
	AliSettingsDiHadronPID& operator=(const AliSettingsDiHadronPID);
	~AliSettingsDiHadronPID();

public:

	// Instance management.
	static AliSettingsDiHadronPID*				Instance(Bool_t configMacroIsCalling = kFALSE);
	static void									Kill();

	// Output.
	void 										Print(Option_t* = "") const;

	// --------------------------
	//  General Settings.
	// --------------------------

	// Beam Type (should use the enum type)
	void										SetBeamType(Int_t beamType); // Beam type is not really used...
	Int_t 										GetBeamType() const {return fBeamType;}

	void										SetIsMC(Bool_t isMC = kTRUE) {fIsMC = isMC;}
	Bool_t										GetIsMC() const {return fIsMC;}

	void										SetTOFMismatchInfoIsAvalableMC(Bool_t isAvailable = kTRUE) {fTOFMismatchInfoIsAvailableMC = isAvailable;}
	Bool_t										GetIsTOFMismatchInfoAvailableMC() const {return fTOFMismatchInfoIsAvailableMC;}

	void										SetFilterMask(Int_t filtermask) {fFilterMask = filtermask;}
	Int_t										GetFilterMask() const {return fFilterMask;}

        void SetPPEfficiencyFactor(Float_t factor) { fPPEfficiencyFactor = factor; }
        Float_t GetPPEfficiencyFactor() { return fPPEfficiencyFactor; }

	void										SetDataPath(const char* path) {fDataPath = path;}
	const char*									GetDataPath() const {return fDataPath;}

	void										SetWorkingDirectory(const char* dir) {fWorkingDir = dir;}
	const char*									GetWorkingDirectory() const {return fWorkingDir;}

	void										SetEfficienciesDirectory(const char* dir) {fEfficienciesDir = dir;}
	TString										GetEfficienciesDirectory() const {return fEfficienciesDir;}

	void										SetUISpectrumFileName(const char* name) {fUISpectrumFileName = name;}
	TString										GetUISpectrumFileName() const {return fUISpectrumFileName;}

	void										SetSpectraFileName(const char* name) {fSpectraFileName = name;}
	TString										GetSpectraFileName() const {return fSpectraFileName;}
	TString										GetSpectraFileNameForSpecies(Int_t species) const {return AliMiscToolsDiHadronPID::AppendToFileName(fSpectraFileName, TString::Format("_%i", species));}

	void										SetPtSpectraFileName(const char* name) {fPtSpectraFileName = name;}
	TString										GetPtSpectraFileName() const {return fPtSpectraFileName;}
	TString										GetPtSpectraCorrectedFileName() const {return AliMiscToolsDiHadronPID::AppendToFileName(fPtSpectraFileName, "_Corrected");}
	TString										GetPtSpectraComparedFileName() const {return AliMiscToolsDiHadronPID::AppendToFileName(fPtSpectraFileName, "_Compared");}

	void										SetTemplatesFileName(const char* name) {fTemplatesFileName = name;}
	TString										GetTemplatesFileName() const {return fTemplatesFileName;}
	TString										GetTemplatesFileNameForSpecies(Int_t species) const {return AliMiscToolsDiHadronPID::AppendToFileName(fTemplatesFileName, TString::Format("_%i", species));}

	void										SetPIDCorrelationsFileName(const char* name) {fPIDCorrelationsFileName = name;}
	TString										GetPIDCorrelationsFileName() const {return fPIDCorrelationsFileName;}
	TString										GetPIDCorrelationsFileNameForSpecies(Int_t species) const {return AliMiscToolsDiHadronPID::AppendToFileName(fPIDCorrelationsFileName, TString::Format("_%i", species));}
	TString										GetPIDCorrelationsFileNameForSpeciesFitted(Int_t species) const {return AliMiscToolsDiHadronPID::AppendToFileName(fPIDCorrelationsFileName, TString::Format("_Fitted_%i", species));}

	TString										GetEfficienciesFileName() const {return fEfficienciesFileName;}
	// TString										GetEfficienciesPIDCorrelationsFileName() const {return AliMiscToolsDiHadronPID::AppendToFileName(fEfficienciesFileName,"PIDCorrelations");}
	// TString										GetEfficienciesPIDCorrelationsFileNameForSpecies(Int_t species) const {return AliMiscToolsDiHadronPID::AppendToFileName(GetEfficienciesPIDCorrelationsFileName(), TString::Format("_%i", species));}
	TString										GetEfficienciesSpectraFileName() const {return AliMiscToolsDiHadronPID::AppendToFileName(fEfficienciesFileName,"Spectra");}
	// TString										GetEfficienciesSpectraFileNameForSpecies(Int_t species) const {return AliMiscToolsDiHadronPID::AppendToFileName(GetEfficienciesPIDCorrelationsFileName(), TString::Format("_%i", species));}

	void										SetFilterMaskForDCATemplates(Int_t filtermaskdca) {fFilterMaskDCATemplates = filtermaskdca;}
	Int_t										GetFilterMaskForDCATemplates() const {return fFilterMaskDCATemplates;}

	void										SetUseDEDxRatios(Bool_t useRatios) {fUseDEDxRatios = useRatios;}
	Bool_t										GetUseDEDxRatios() const {return fUseDEDxRatios;}


	// --------------------------
	//  PID Cuts.
	// --------------------------
	void										SetMinClustersdEdx(Int_t minClusters) {fMinClustersdEdx = minClusters;}
	Int_t										GetMinClustersdEdx() const {return fMinClustersdEdx;}
	void										SetMaxClustersdEdx(Int_t maxClusters) {fMaxClustersdEdx = maxClusters;}
	Int_t										GetMaxClustersdEdx() const {return fMaxClustersdEdx;}
	void 										SetDemandTOFTPCPIDFlags(Bool_t demandFlags) {fDemandTOFTPCPIDFlags = demandFlags;}
	Bool_t										GetDemandTOFTPCPIDFlags() const {return fDemandTOFTPCPIDFlags;}
	void										SetDemandTightDCACut(Bool_t demandCut) {fDemandTightDCACut = demandCut;}
	Bool_t										GetDemandTightDCACut() const {return fDemandTightDCACut;}
	void										SetDemandT0TOFStartTime(Bool_t demandt0tofstarttime) {fDemandT0TOFStartTime = demandt0tofstarttime;}
	Bool_t										GetDemandT0TOFStartTime() const {return fDemandT0TOFStartTime;}

	void										SetDemandCharge(Charge charge) {fRequestedCharge = charge;}
	Charge 										GetDemandCharge() const {return fRequestedCharge;}
	Bool_t										IsTrackSelectedForCharge(const AliMiniTrackDiHadronPID* track) const;

	Bool_t										IsTrackSelectedForTOFPID(const AliMiniTrackDiHadronPID* track) const;
	Bool_t										IsTrackSelectedForTPCPID(const AliMiniTrackDiHadronPID* track) const;
	Bool_t										IsTrackSelectedForPID(const AliMiniTrackDiHadronPID* track) const;

	void										SetRapidityCutForInclusiveSpectra(Bool_t rapidityCut = kTRUE) {fRapidityCutForInclusiveSpectra = rapidityCut;}
	Bool_t										GetRapidityCutForInclusiveSpectra() const {return fRapidityCutForInclusiveSpectra;}

	// --------------------------
	//  Correlation Settings.
	// --------------------------
	void										SetNTriggersToMix(Int_t nTrig) {fNTriggersToMix = nTrig;}
	Int_t										GetNTriggersToMix() const {return fNTriggersToMix;}

	void										SetTriggerPtRange(Double_t minPt, Double_t maxPt) {fMinPtTrig = minPt; fMaxPtTrig = maxPt;}
	Double_t									GetMinTriggerPt() const {return fMinPtTrig;}
	Double_t									GetMaxTriggerPt() const {return fMaxPtTrig;}
	void										SetAssociatedPtRange(Double_t minPt, Double_t maxPt) {fMinPtAssoc = minPt; fMaxPtAssoc = maxPt;}
	Double_t									GetMinAssociatedPt() const {return fMinPtAssoc;}
	Double_t									GetMaxAssociatedPt() const {return fMaxPtAssoc;}

	// --------------------------
	//  Fit Settings.
	// --------------------------

	void										SetIncludePeakInFit(Species massAssumption, Species peakSpecies, Double_t minPt, Double_t maxPt);

	void										SetStdTOFTail(Species massAssumption, Species peakSpecies, Double_t value); // Just sets the value.
	void										SetFixTOFTail(Species massAssumption, Species peakSpecies, Double_t minPt, Double_t maxPt, Double_t value = -1.); // Fixes the tail, and optionally, sets the value.

	void										SetFixTOFPosition(Species massAssumption, Species peakSpecies) {fMuTOFIsFixed[massAssumption][peakSpecies] = kTRUE;}
	void										SetFixTOFSigma(Species massAssumption, Species peakSpecies) {fSigmaTOFIsFixed[massAssumption][peakSpecies] = kTRUE;}

	void										SetStdTPCTail(Species massAssumption, Species peakSpecies, Double_t value);
	void										SetUseTPCTail(Species massAssumption, Species peakSpecies, Double_t minPt, Double_t maxPt);
	void										SetFixTPCTail(Species massAssumption, Species peakSpecies, Double_t minPt, Double_t maxPt, Double_t value = -1.);

	Bool_t										IsPeakIncludedInFit(Species massAssumption, Species peakSpecies, Double_t meanPt) const;

        Bool_t										IsTOFPositionFixed(Species massAssumption, Species peakSpecies) {return fMuTOFIsFixed[massAssumption][peakSpecies];}
        Bool_t										IsTPCPositionFixed(Species massAssumption, Species peakSpecies) {return fMuTPCIsFixed[massAssumption][peakSpecies];}
        Bool_t										IsTOFSigmaFixed(Species massAssumption, Species peakSpecies) {return fSigmaTOFIsFixed[massAssumption][peakSpecies];}
        Bool_t										IsTPCSigmaFixed(Species massAssumption, Species peakSpecies) {return fSigmaTPCIsFixed[massAssumption][peakSpecies];}


	Double_t 									GetStdTOFTailValue(Species massAssumption, Species peakSpecies) const;
	Bool_t										IsTOFTailFixed(Species massAssumption, Species peakSpecies, Double_t meanPt) const;

	Double_t									GetStdTPCTailValue(Species massAssumption, Species peakSpecies) const;
	Bool_t										IsTPCTailIncluded(Species massAssumption, Species peakSpecies, Double_t meanPt) const;
	Bool_t										IsTPCTailFixed(Species massAssumption, Species peakSpecies, Double_t meanPt) const;

	void										SetFixTPCPosition(Species massAssumption, Species peakSpecies) {fMuTPCIsFixed[massAssumption][peakSpecies] = kTRUE;}
	void										SetFixTPCSigma(Species massAssumption, Species peakSpecies) {fSigmaTPCIsFixed[massAssumption][peakSpecies] = kTRUE;}

	void										SetUseChi2BelowPt(Double_t pt) {fUseChi2BelowPt = pt;}
	Double_t									GetUseChi2BelowPt() const {return fUseChi2BelowPt;}

	void										SetFitRegionTOFmin(Double_t tofMin) {fTOFFitRangeMin = tofMin;} // These values are on top of the square formed by the included peaks.
	void										SetFitRegionTOFmax(Double_t tofMax) {fTOFFitRangeMax = tofMax;}
	void										SetFitRegionTPCmin(Double_t tpcMin) {fTPCFitRangeMin = tpcMin;}
	void										SetFitRegionTPCmax(Double_t tpcMax) {fTPCFitRangeMax = tpcMax;}

	Double_t									GetFitRegionTOFmin() const {return fTOFFitRangeMin;} 
	Double_t									GetFitRegionTOFmax() const {return fTOFFitRangeMax;}
	Double_t									GetFitRegionTPCmin() const {return fTPCFitRangeMin;}
	Double_t									GetFitRegionTPCmax() const {return fTPCFitRangeMax;}

	// The fit penalty will give a penalty to chi2 or ll if the fit parameters become unreasonable.
	// FIXME: At the moment what is "unreasonable" is hard-coded.
	void										UseFitPenalty(Bool_t useFitPenalty = kTRUE) {fUseFitPenalty = useFitPenalty;}
	Bool_t								 		IsFitPenaltyUsed() const {return fUseFitPenalty;}

	// When running over MC tracks, choose whether to use the "data" way of making the TOF mismatch
	// template, or just by using the tracks with a MC not-matched status.
	void										UseMCLabelForMismatchTemplate(Bool_t useMCLabel = kTRUE) {fUseMCLabelForMismatches = useMCLabel;}
	Bool_t										IsMCLabelUsedForMismatches() const {return fUseMCLabelForMismatches;}

	// Mismatches.
	Int_t										GetMismatchRegionTOFminBin(Species massAssumption, Double_t avgPt, Double_t absAvgEta) {return GetMismatchRegionTOFMinOrMaxBin(massAssumption, avgPt, absAvgEta, kTRUE);}
	Int_t										GetMismatchRegionTOFmaxBin(Species massAssumption, Double_t avgPt, Double_t absAvgEta) {return GetMismatchRegionTOFMinOrMaxBin(massAssumption, avgPt, absAvgEta, kFALSE);}
private:
	Int_t										GetMismatchRegionTOFMinOrMaxBin(Species massAssumption, Double_t avgPt, Double_t absAvgEta, Bool_t returnMin);

public:
	void										SetMinDistToDetermineMismatches(Double_t minDist) {fMinDistToDetermineMismatch = minDist;}
	void										SetDistFromPeakForMismatch(Double_t minDist) {fDistFromPeakForMismatch = minDist;}
	
	// Chi2 fit.
	void										SetMinEntriesForChi2(Int_t minEntries) {fMinEntriesForChi2 = minEntries;}
	Int_t										GetMinEntriesForChi2() const {return fMinEntriesForChi2;}

	// Standard is to use Sqrt(N_measured) as an error estimate, however, that in principle generates a bias
	void										SetUseSqrtModelValueForErrorChi2(Bool_t useSqrtMod = kTRUE) {fUseSqrtModelValueForErrorChi2 = useSqrtMod;}
	Bool_t										GetUseSqrtModelValueForErrorChi2() {return fUseSqrtModelValueForErrorChi2;}

	// LL fit.
	void										SetMinEntriesForLL(Int_t minEntries) {fMinEntriesForLL = minEntries;}
	Int_t										GetMinEntriesForLL() const {return fMinEntriesForLL;}

	// Standard TOF and TPC resolution at initialisation. Setters are disabeled, since these resolutions
	// are automatically set when the beam type is chosen (see SetBreamType method).
	void										SetStdTOFResolution(Double_t res) {fStdTOFResolution = res;}
	Double_t									GetStdTOFResolution() const {return fStdTOFResolution;}

	void										SetStdTOFShift(Double_t delta) {fStdTOFShift = delta;}
	Double_t									GetStdTOFShift() const {return fStdTOFShift;}

	void										SetStdTPCResolution(Double_t res) {fStdTPCResolution = res;}
	Double_t									GetStdTPCResolution() const {return fStdTPCResolution;}

	void										SetStdTPCElectronShift(Double_t shift) {fStdTPCElectronShift = shift;}
	Double_t									GetStdTPCElectronShift() const {return fStdTPCElectronShift;}

	void										SetMuTOFTolerance(Double_t delta) {fMuTOFTolerance = delta;}  // TOF tolerance is given as fraction of the proton-kaon peak distance
	Double_t									GetMuTOFTolerance(Double_t pt) const;

	void										SetMuTPCTolerance(Double_t delta) {fMuTPCTolerance = delta;}
	Double_t									GetMuTPCTolerance() const {return fMuTPCTolerance;}

public:

	// --------------------------
	//  Axes Settings.
	// --------------------------

	void										SetCentralityAxis(Int_t nBins, const Double_t* axis);
	const TAxis*								GetCentralityAxis() const;
	void										SetVtxZAxis(Int_t nBins, Double_t min, Double_t max);
	const TAxis*								GetVtxZAxis() const;
	void										SetPtAxis(Int_t nBins, const Double_t* axis);
	const TAxis*								GetPtAxis() const;
	void										SetEtaAxis(Int_t nBins, Double_t min, Double_t max);
	const TAxis*								GetEtaAxis() const;
	void										SetDEtaAxis(Int_t nBins, Double_t min, Double_t max); 
	const TAxis*								GetDEtaAxis() const;
	void										SetPhiAxis(Int_t nBins, Double_t min = -TMath::Pi()/2., Double_t max = 3.*TMath::Pi()/2.);
	const TAxis*								GetPhiAxis() const;
	void										SetDPhiAxis(Int_t nBins, Double_t min = -TMath::Pi()/2., Double_t max = 3.*TMath::Pi()/2.);
	const TAxis*								GetDPhiAxis() const;
	void										SetDCAAxis(Int_t nBins, const Double_t* axis);
	void										SetDCAAxis(Int_t nBins, Double_t min = -2.4, Double_t max = 2.4);
	const TAxis*								GetDCAAxis() const;
	void 										SetTOFAxis(Int_t nBins, Double_t min, Double_t max, Species massAssumption);
	const TAxis*								GetTOFAxis(Species massAssumption) const;
	void 										SetTPCAxis(Int_t nBins, Double_t min, Double_t max, Species massAssumption);
	const TAxis*								GetTPCAxis(Species massAssumption) const;

	// --------------------------
	//  Misc methods.
	// --------------------------

	static Int_t								GetNBeamTypes() {return fNBeamTypes;}
	static const char*							GetBeamTypeName(BeamType beamtype) {return fBeamTypeNames[beamtype];}
	//static const char*							GetBeamTypeName(Int_t beamtype) {return fBeamTypeNames[beamtype];}

	static Int_t								GetNSpecies() {return fNSpecies;}
	static Int_t 								GetNMassAssumptions() {return fNMassAssumptions;}
	static const char*							GetSpeciesName(Species species) {return fSpeciesNames[species];}
	static const char*							GetSpeciesNameShort(Species species) {return fSpeciesNamesShort[species];}
	static const char*							GetSpeciesNameLaTeX(Species species) {return fSpeciesLaTeX[species];}
	static Double_t								GetSpeciesMass(Species species) {return fSpeciesMass[species];}
	static Style_t								GetSpeciesPlotMarker(Species species) {return fSpeciesPlotMarker[species];}
	static Color_t								GetSpeciesColor(Species species) {return fSpeciesColor[species];}

	//static const char*							GetSpeciesName(Int_t species) {return GetSpeciesName(species);}
	//static const char*							GetSpeciesNameShort(Int_t species) {return GetSpeciesNameShort(species);}
	//static const char*							GetSpeciesNameLaTeX(Int_t species) {return GetSpeciesNameLaTeX(species);}
	//static Double_t								GetSpeciesMass(Int_t species) {return GetSpeciesMass(species);}
	//static Style_t								GetSpeciesPlotMarker(Int_t species) {return GetSpeciesPlotMarker(species);}
	//static Color_t								GetSpeciesColor(Int_t species) {return GetSpeciesColor(species);}

private:

	static AliSettingsDiHadronPID* 				fSettings;

	static const Int_t							fNBeamTypes;
	static const char*							fBeamTypeNames[3];

	static const Int_t							fNSpecies;
	static const Int_t							fNMassAssumptions;
	static const char*							fSpeciesNames[7];
	static const char*							fSpeciesNamesShort[7];	
	static const char*							fSpeciesLaTeX[7];
	static const Double_t						fSpeciesMass[7];
	static const Style_t						fSpeciesPlotMarker[7];
	static const Color_t						fSpeciesColor[7];

	// Basic Settings.
	Bool_t										fIsConfigured;
	Int_t										fBeamType; // 0 = PbPb, 1 = pPb, 2 = pp -> TODO: Change to enum type.
	Bool_t										fIsMC;
	Bool_t										fTOFMismatchInfoIsAvailableMC; // This is not the case for old AOD MC productions.
	Int_t 										fFilterMask;
	Int_t										fFilterMaskDCATemplates;
        Bool_t fUseDEDxRatios;  // default use difference dE/dx-expected; can use ratio for comparison to others
        Float_t fPPEfficiencyFactor; // Correction for the fact that the pp MC is only for LHC10d; LHC10e has some TOF channels missing
	// Paths and file settings.
	TString										fDataPath;
	TString										fWorkingDir;
	TString										fEfficienciesDir;
	TString										fUISpectrumFileName;
	TString										fSpectraFileName;
	TString										fPtSpectraFileName;
	TString										fTemplatesFileName;
	TString										fPIDCorrelationsFileName;
	TString										fEfficienciesFileName;

	// PID Cuts (for spectra and associateds).
	Int_t										fMinClustersdEdx;
	Int_t										fMaxClustersdEdx;
	Bool_t										fDemandTOFTPCPIDFlags;
	Bool_t										fDemandTightDCACut;
	Bool_t										fDemandT0TOFStartTime;
	Charge 										fRequestedCharge;

	Bool_t										fRapidityCutForInclusiveSpectra; // Rapidity cut only needed for corrected inclusive spectra, always 0.5

	// Correlation Settings.
	Int_t										fNTriggersToMix;
	Double_t									fMinPtTrig;
	Double_t									fMaxPtTrig;
	Double_t									fMinPtAssoc;
	Double_t									fMaxPtAssoc;

	// General Fit Settings.
	Double_t 									fMinDistToDetermineMismatch;
	Double_t									fDistFromPeakForMismatch;
	
	Int_t										fMinEntriesForChi2;
	Bool_t										fUseSqrtModelValueForErrorChi2;

	Int_t 										fMinEntriesForLL;

	Double_t									fStdTOFResolution;
	Double_t									fStdTOFShift;
	Double_t 									fStdTPCResolution;
	Double_t 									fStdTPCElectronShift;

	Double_t									fUseChi2BelowPt;
	
	Double_t									fTOFFitRangeMin; // The fit region is a square around all included peaks, plus the values below.
	Double_t									fTOFFitRangeMax;
	Double_t									fTPCFitRangeMin;
	Double_t									fTPCFitRangeMax;

	Bool_t										fUseFitPenalty;
	Bool_t										fUseMCLabelForMismatches;

        Double_t fMuTPCTolerance;  // Maximum deviation of TPC position from the eta-trend (dE/dx units)
        Double_t fMuTOFTolerance;  // Maximum deviation of TPC position from the eta-trend (relative to proton-kaon peak distance)

	// Fit Peak Settings as a function of pT [M.A.][Species]
	Bool_t										fPeakIsIncluded[7][7];
	Double_t									fPeakIsIncludedMinPt[7][7];
	Double_t									fPeakIsIncludedMaxPt[7][7];

	Bool_t										fTOFTailIsFixed[7][7];
	Double_t									fTOFTailIsFixedMinPt[7][7];
	Double_t 									fTOFTailIsFixedMaxPt[7][7];
	Double_t									fTOFTailIsFixedValue[7][7];

	Bool_t										fMuTOFIsFixed[7][7];
	Bool_t										fSigmaTOFIsFixed[7][7];

	Bool_t										fTPCTailIsIncluded[7][7];
	Double_t									fTPCTailIsIncludedMinPt[7][7];
	Double_t									fTPCTailIsIncludedMaxPt[7][7];

	Bool_t										fTPCTailIsFixed[7][7];
	Double_t									fTPCTailIsFixedMinPt[7][7];
	Double_t									fTPCTailIsFixedMaxPt[7][7];
	Double_t									fTPCTailIsFixedValue[7][7];

	Bool_t										fMuTPCIsFixed[7][7];
	Bool_t										fSigmaTPCIsFixed[7][7];

	// Axes.
	TAxis*										fCentralityAxis;
	TAxis*										fVtxZAxis;	
	TAxis*										fPtAxis;
	TAxis*										fEtaAxis;
	TAxis*										fDEtaAxis;
	TAxis*										fPhiAxis;
	TAxis*										fDPhiAxis;
	TAxis*										fDCAAxis;

	TAxis*										fTOFAxis[3];
	TAxis*										fTPCAxis[3];

	ClassDef(AliSettingsDiHadronPID, 2);

};

#endif
