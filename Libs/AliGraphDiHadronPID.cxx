/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Graph Class, combining statistical and systematic uncertainties.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "AliGraphDiHadronPID.h"
#include "AliLogDiHadronPID.h"

#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TList.h"
#include "TMath.h"

ClassImp(AliGraphDiHadronPID);

Double_t AliGraphDiHadronPID::fMultiPlotDisplacement = 0.02;

// -----------------------------------------------------------------------
AliGraphDiHadronPID::AliGraphDiHadronPID():
	TNamed(),
	fNPoints(0),
	fX(0),
	fY(0),
	fBinWidth(0),
	fStatErrorVert(0),
	fSystErrorUp(0),
	fSystErrorDown(0),
	fSystErrorMargin(0.),
	fSystErrorStyle(kSolidBlock),
	fMinX(0.),
	fMaxX(0.),	
	fMinY(0.),
	fMinYLog(10e-10),
	fMaxY(1.),
	fMaxYLog(1.),
	fColor(0),
	fMarkerStyle(0),
	fLineWidth(2),
	fXAxisTitle("x"),
	fYAxisTitle("y")

{
	
	// Default Constructor
	AliLogDiHadronPID::Debug2("Default constructor called.", __func__, ClassName());

	fOwnedObjects = new TObjArray();
	fOwnedObjects->SetOwner(kTRUE);

}

// -----------------------------------------------------------------------
AliGraphDiHadronPID::AliGraphDiHadronPID(const char* name, const char* title):
	TNamed(name, title),
	fNPoints(0),
	fX(0),
	fY(0),
	fBinWidth(0),
	fStatErrorVert(0),
	fSystErrorUp(0),
	fSystErrorDown(0),
	fSystErrorMargin(0.),
	fSystErrorStyle(kSolidBlock),
	fMinX(0.),
	fMaxX(0.),	
	fMinY(0.),
	fMinYLog(10e-10),
	fMaxY(1.),
	fMaxYLog(1.),
	fColor(0),
	fMarkerStyle(0),
	fLineWidth(2),
	fXAxisTitle("x"),
	fYAxisTitle("y")		

{
	
	// Default Constructor
	AliLogDiHadronPID::Debug2("Default constructor called.", __func__, ClassName());

	fOwnedObjects = new TObjArray();
	fOwnedObjects->SetOwner(kTRUE);

}

// -----------------------------------------------------------------------
AliGraphDiHadronPID::AliGraphDiHadronPID(const TH1* hist):
	TNamed(),
	fNPoints(0),
	fX(0),
	fY(0),
	fBinWidth(0),
	fStatErrorVert(0),
	fSystErrorUp(0),
	fSystErrorDown(0),
	fSystErrorMargin(0.),
	fSystErrorStyle(kSolidBlock),
	fMinX(0.),
	fMaxX(0.),	
	fMinY(0.),
	fMinYLog(10e-10),
	fMaxY(1.),
	fMaxYLog(1.),
	fColor(0),
	fMarkerStyle(0),
	fLineWidth(2),
	fXAxisTitle("x"),
	fYAxisTitle("y")	

{
	
	// Default Constructor
	AliLogDiHadronPID::Debug2("Default constructor called.", __func__, ClassName());

	fOwnedObjects = new TObjArray();
	fOwnedObjects->SetOwner(kTRUE);

	if (hist) {
		SetName(hist->GetName());
		SetTitle(hist->GetTitle());
		SetColor(hist->GetLineColor());
		SetXAxisTitle((hist->GetXaxis())->GetTitle());
		SetYAxisTitle((hist->GetYaxis())->GetTitle());

		ImportMeasurement(hist);

	} else {
		AliLogDiHadronPID::Error(Form("No histogram supplied."), __func__, ClassName());
	}

}

// -----------------------------------------------------------------------
AliGraphDiHadronPID::AliGraphDiHadronPID(const AliGraphDiHadronPID& other):
	TNamed(other),
	fNPoints(other.fNPoints),
	fX(other.fX),
	fY(other.fY),
	fBinWidth(other.fBinWidth),
	fStatErrorVert(other.fStatErrorVert),
	fSystErrorUp(other.fSystErrorUp),
	fSystErrorDown(other.fSystErrorDown),
	fSystErrorMargin(other.fSystErrorMargin),
	fSystErrorStyle(other.fSystErrorStyle),
	fMinX(other.fMinX),
	fMaxX(other.fMaxX),	
	fMinY(other.fMinY),
	fMinYLog(other.fMinYLog),
	fMaxY(other.fMaxY),
	fMaxYLog(other.fMaxYLog),
	fColor(other.fColor),
	fMarkerStyle(other.fMarkerStyle),
	fLineWidth(other.fLineWidth)


{

	// Copy constructor.
	AliLogDiHadronPID::Debug2("Copy constructor called.", __func__, ClassName());

	fOwnedObjects = new TObjArray();
	fOwnedObjects->SetOwner(kTRUE);

}

// -----------------------------------------------------------------------
AliGraphDiHadronPID::~AliGraphDiHadronPID() {
	
	// Destructor
	AliLogDiHadronPID::Debug2("Destructor called.", __func__, ClassName());

	delete fOwnedObjects; fOwnedObjects = 0x0;

}

// -----------------------------------------------------------------------
void AliGraphDiHadronPID::Init(Int_t nPoints) {

	// Creates empty arrays with nPoints for all histograms.
	if (fNPoints) {
		AliLogDiHadronPID::Warning(Form("It seems like the instace was not empty... Calling Reset()."), __func__, ClassName());
		Reset();
	}

	fNPoints = nPoints;
	fX.ResizeTo(nPoints);
	fY.ResizeTo(nPoints);
	fBinWidth.ResizeTo(nPoints);
	fStatErrorVert.ResizeTo(nPoints);
	fSystErrorUp.ResizeTo(nPoints);
	fSystErrorDown.ResizeTo(nPoints);

}

// -----------------------------------------------------------------------
void AliGraphDiHadronPID::Reset() {

	// Reset the instance.
	fNPoints = 0;

	fX.Zero();
	fY.Zero();
	fBinWidth.Zero();
	fStatErrorVert.Zero();
	fSystErrorUp.Zero();
	fSystErrorDown.Zero();

}

// -----------------------------------------------------------------------
void AliGraphDiHadronPID::ImportMeasurement(const TH1* hist) {

	Init(hist->GetNbinsX());

	for (Int_t iBinX = 1; iBinX <= hist->GetNbinsX(); iBinX++) {
			
		fX[iBinX - 1] = hist->GetBinCenter(iBinX);
		fY[iBinX - 1] = hist->GetBinContent(iBinX);
		fStatErrorVert[iBinX - 1] = hist->GetBinError(iBinX);
		fBinWidth[iBinX - 1] = hist->GetBinWidth(iBinX);
	
	}

	// Set range for the horizontal axis.
	fMinX = (hist->GetXaxis())->GetXmin();
	fMaxX = (hist->GetXaxis())->GetXmax();

	// Set range for the vertical axis.
	Double_t range = fY.Max() - fY.Min();
	fMaxY = fY.Max() + range * 0.1;
	fMinY = fY.Min() - range * 0.1;

	// Make sure that the range of the log plot does not drop below 0.
	if (fY.Min() < 0.) {
		fMinYLog = fY.Max();
		for (Int_t iPoint = 0; iPoint < fY.GetNoElements(); iPoint++) {
				
			if (fY[iPoint] > 0.) {
				fMinYLog = TMath::Min(fMinYLog, fY[iPoint] / 2.);
			}
		
		}
	} else {
		fMinYLog = fY.Min() / 2.;
	}

	fMaxYLog = fY.Max() * 2.;

}

// -----------------------------------------------------------------------
void AliGraphDiHadronPID::SetSystErr(Double_t pct) {

	if (!fNPoints) {
		AliLogDiHadronPID::Error(Form("First Call ImportMeasurement()."), __func__, ClassName());
		return;
	}

	for (Int_t iPoint = 0; iPoint < fNPoints; iPoint++) {
			
		fSystErrorUp[iPoint] = fY[iPoint] * pct;
		fSystErrorDown[iPoint] = fY[iPoint] * pct;
	
	}

}

// -----------------------------------------------------------------------
void AliGraphDiHadronPID::SetSystErr(const AliSystUncertaintyGenDiHadronPID* systErr) {

	if (!fNPoints) {
		AliLogDiHadronPID::Error(Form("First Call ImportMeasurement()."), __func__, ClassName());
		return;
	}

	// Allow for different errros up/down.
	TF1* systUp = systErr->GetTF1(kTRUE);
	TF1* systDown = systErr->GetTF1(kFALSE);

	for (Int_t iPoint = 0; iPoint < fNPoints; iPoint++) {

		// FIXME!
		fSystErrorUp[iPoint] = fY[iPoint] * systUp->Eval(fX[iPoint]);
		fSystErrorDown[iPoint] = fY[iPoint] * systUp->Eval(fX[iPoint]);
	
	}

	delete systUp;
	delete systDown;

}

// -----------------------------------------------------------------------
void AliGraphDiHadronPID::MergeBins(Int_t minBin, Int_t maxBin, Int_t groupSize, Bool_t takeAvg) {

	// This method rebins between minBin and maxBin. The default behavior is that the sum is taken
	// of the group, however, optionally you can choose to take the average of the bins.

	if (maxBin < minBin) {
		AliLogDiHadronPID::Error(Form("maxBin < minBin"), __func__, ClassName());
		return;
	}

	if (minBin < 1) {
		AliLogDiHadronPID::Error(Form("minBin < 1"), __func__, ClassName());
		return;
	}

	if (maxBin > fNPoints) {
		AliLogDiHadronPID::Error(Form("maxBin > nPoints"), __func__, ClassName());
		return;
	}

	if ((maxBin - minBin + 1) % groupSize) {
		AliLogDiHadronPID::Error(Form("The bin range is not an integer multiple of the group size."), __func__, ClassName());
		return;
	}
	
	// Buffer for the rebinned data (same size as the original).
	Int_t newNPoints = 0;
	TVectorF newX(fNPoints);
	TVectorF newY(fNPoints);
	TVectorF newBinWidth(fNPoints);
	TVectorF newStatErrVert(fNPoints);
	TVectorF newSystErrUp(fNPoints);
	TVectorF newSystErrDown(fNPoints);

	// Old point index.
	Int_t iOldPoint = 0;

	while (iOldPoint < fNPoints) {

		// If below minBin, or above maxbin then just copy. Else create a group.
		if (iOldPoint < (minBin - 1) || iOldPoint > (maxBin - 1)) {
			newX[newNPoints] = fX[iOldPoint];
			newY[newNPoints] = fY[iOldPoint];
			newBinWidth[newNPoints] = fBinWidth[iOldPoint];
			newStatErrVert[newNPoints] = fStatErrorVert[iOldPoint];
			newSystErrUp[newNPoints] = fSystErrorUp[iOldPoint];
			newSystErrDown[newNPoints] = fSystErrorDown[iOldPoint];

			iOldPoint++;
			newNPoints++;

		} else {
			for (Int_t iGroupIndex = 0; iGroupIndex < groupSize; iGroupIndex++) {

				newX[newNPoints] += fX[iOldPoint];
				newY[newNPoints] += fY[iOldPoint];
				newBinWidth[newNPoints] += fBinWidth[iOldPoint];
				newStatErrVert[newNPoints] += (fStatErrorVert[iOldPoint] * fStatErrorVert[iOldPoint]);
				newSystErrUp[newNPoints] += fSystErrorUp[iOldPoint];
				newSystErrDown[newNPoints] += fSystErrorDown[iOldPoint];

				iOldPoint++;
			
			}

			// For the bin position, we take the average.
			Float_t flGroupSize = ((Float_t)groupSize);
			newX[newNPoints] /= flGroupSize;

			// For the statistical error, the errors were added in quadrature.
			newStatErrVert[newNPoints] = TMath::Sqrt(newStatErrVert[newNPoints]);

			// Take the average if requested.
			if (takeAvg) {
				newY[newNPoints] /= flGroupSize;
				newStatErrVert[newNPoints] /= flGroupSize;
				newSystErrUp[newNPoints] /= flGroupSize;
				newSystErrDown[newNPoints] /= flGroupSize;

			}

			newNPoints++;

		}

	}

	// Replace the old vectors with the new ones.
	fNPoints = newNPoints;
	fX = newX;
	fY = newY;
	fBinWidth = newBinWidth;
	fStatErrorVert = newStatErrVert;
	fSystErrorUp = newSystErrUp;
	fSystErrorDown = newSystErrDown;

	// Make the new vectors to the right size.
	fX.ResizeTo(newNPoints);
	fY.ResizeTo(newNPoints);
	fBinWidth.ResizeTo(newNPoints);
	fStatErrorVert.ResizeTo(newNPoints);
	fSystErrorUp.ResizeTo(newNPoints);
	fSystErrorDown.ResizeTo(newNPoints);

}

// -----------------------------------------------------------------------
void AliGraphDiHadronPID::Show(TPad* pad, Int_t collectionSize, Int_t position, Bool_t showStat, Bool_t showSyst) {

	// This method draws the histogram into canvas cvs (creates one if needed).
	// By default it assumes that it is not part of a larger collection of graphs that
	// should be shown together.

	if (!fNPoints) {
		AliLogDiHadronPID::Error(Form("First Call ImportMeasurement()."), __func__, ClassName());
		return;
	}	

	if (!pad) {	
		pad = TCanvas::MakeDefCanvas();
		fOwnedObjects->AddLast(pad);
	} else {
		pad->cd();
	}

	// Calculate half bin width.
	TVectorF halfBinWidth(fBinWidth);
	halfBinWidth *= 0.5;

	// Set horizontal statistical uncertainties to half bin width. (Correction: set to 0).
	TVectorF statErrHorLeft(fNPoints);/*(halfBinWidth);*/
	TVectorF statErrHorRight(fNPoints);/*(halfBinWidth);*/
	
	// Set horizontal systematic uncertainties to binwidth * (1 - margin).
	TVectorF systErrHorLeft(halfBinWidth);
	systErrHorLeft *= (1. - fSystErrorMargin);
	TVectorF systErrHorRight(systErrHorLeft);

	// Leave positions unchanged
	TVectorF xx(fX);

	// Make changes to the positions when part of a collection.
	if (collectionSize > 1) {

		Double_t totalDisplacement = (2. * (Double_t)position - ((Double_t)collectionSize - 1.)) * fMultiPlotDisplacement;

		xx += totalDisplacement;
		systErrHorLeft += totalDisplacement;
		systErrHorRight -= totalDisplacement;

		// if (position == 0) {
			
		// }
		// else {
		// 	xx += fMultiPlotDisplacement;
		// 	systErrHorLeft += fMultiPlotDisplacement;
		// 	systErrHorRight -= fMultiPlotDisplacement;			
		// }
	}

	// Create graphs (should keep pointers!)
	if (showSyst) {
		TGraphAsymmErrors* systematic = new TGraphAsymmErrors(xx, fY, systErrHorLeft, systErrHorRight, fSystErrorDown, fSystErrorUp);
		fOwnedObjects->AddLast(systematic);
		systematic->SetMinimum(fMinY);
		systematic->SetMaximum(fMaxY);
		// systematic->SetFillColor(fColor - 10);
		// systematic->SetLineColor(fColor - 2);
		(systematic->GetXaxis())->SetTitle(fXAxisTitle.Data());
		(systematic->GetYaxis())->SetTitle(fYAxisTitle.Data());

		if (fSystErrorStyle == kOpenBlock) {
			systematic->SetFillStyle(0);
			systematic->SetFillColor(0);
			systematic->SetLineColor(fColor - 2);
			systematic->SetMarkerColor(fColor - 2);
		} 
		else {
			systematic->SetFillStyle(1001);
			systematic->SetFillColor(fColor - 10);
			systematic->SetLineColor(fColor - 2);
			systematic->SetMarkerColor(fColor - 2);			
		}

		Int_t systErrType = (fSystErrorStyle == kBand ? 3 : 2); 
		systematic->Draw((pad->GetListOfPrimitives())->GetEntries() ? Form("%i", systErrType) : Form("a%i", systErrType)); // Draw axes in case we're the first primitive.
	
	}

	if (showStat) {
		TGraphAsymmErrors* statistical = new TGraphAsymmErrors(xx, fY, statErrHorLeft, statErrHorRight, fStatErrorVert, fStatErrorVert);
		fOwnedObjects->AddLast(statistical);
		statistical->SetMinimum(fMinY);
		statistical->SetMaximum(fMaxY);
		statistical->SetLineColor(fColor - 2);
		statistical->SetMarkerColor(fColor - 2);
		statistical->SetMarkerStyle(fMarkerStyle);
		statistical->SetLineWidth(fLineWidth);
		(statistical->GetXaxis())->SetTitle(fXAxisTitle.Data());
		(statistical->GetYaxis())->SetTitle(fYAxisTitle.Data());		
		statistical->Draw((pad->GetListOfPrimitives())->GetEntries() ? "p" : "ap");
	}

}

// -----------------------------------------------------------------------
void AliGraphDiHadronPID::UpdateLegend(TLegend* legend) {

	TGraphAsymmErrors* legendEntry = new TGraphAsymmErrors();
	legendEntry->SetMarkerStyle(fMarkerStyle);


	if (fSystErrorStyle == kOpenBlock) {
		legendEntry->SetFillStyle(0);
		legendEntry->SetFillColor(0);
		legendEntry->SetLineColor(fColor - 2);
		legendEntry->SetMarkerColor(fColor - 2);
	} 
	else {
		legendEntry->SetFillStyle(1001);
		legendEntry->SetFillColor(fColor - 10);
		legendEntry->SetLineColor(fColor - 2);
		legendEntry->SetMarkerColor(fColor - 2);
	}

	legendEntry->SetLineWidth(fLineWidth);
	legend->AddEntry(legendEntry, fTitle, "lfp");
	fOwnedObjects->Add(legendEntry);

}

// -----------------------------------------------------------------------
void AliGraphDiHadronPID::Print(Option_t*) const {
	
	// Print info about the class.
	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

}
