/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Fit function for a single peak in (TOF,TPC) space. This class not 
//  only contains the function parameters, but also fit-related 
//  information, such as the parameter step size, and whether the peak 
//  must be used in the fit.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include <iostream>
#include "TString.h"
#include "AliFunctionsDiHadronPID.h"
#include "AliLogDiHadronPID.h"
#include "AliTOFTPCPeakDiHadronPID.h"

using namespace std;

// Initializing Static Data.
Int_t AliTOFTPCPeakDiHadronPID::fNInstances = 0;
const Int_t AliTOFTPCPeakDiHadronPID::fNParameters = 7;
const char* AliTOFTPCPeakDiHadronPID::fParameterNames[7] = {"Yield", "MuTOF", "SigmaTOF", "TailTOF", "MuTPC", "SigmaTPC", "TailTPC"};
const Double_t AliTOFTPCPeakDiHadronPID::fParameterStdStepSizes[7] = {0.1, 0.01, 0.01, 0.000001, 0.001, 0.001, 0.001};

ClassImp(AliTOFTPCPeakDiHadronPID);

// -----------------------------------------------------------------------
AliTOFTPCPeakDiHadronPID::AliTOFTPCPeakDiHadronPID():
	TObject(),
	fSettings(AliSettingsDiHadronPID::Instance()),
	fMassAssumption(AliSettingsDiHadronPID::kUnknownSpecies),
	fPeakSpecies(AliSettingsDiHadronPID::kUnknownSpecies),
	fMeanPt(-1.),
	fAbsMeanEta(-1.),
	fTOFbinWidth(1.),
	fTPCbinWidth(1.),
	fIsUsedInFit(kFALSE),
	fUseTPCTail(kFALSE)

{
	
	// Default Constructor
	AliLogDiHadronPID::Debug2("Default constructor called.", __func__, ClassName());
	ResetParameters(kFALSE); // Do not set to default parameters, other data members are not yet streamed.
	fNInstances++;

}

// -----------------------------------------------------------------------
AliTOFTPCPeakDiHadronPID::AliTOFTPCPeakDiHadronPID(AliSettingsDiHadronPID::Species massAssumption, AliSettingsDiHadronPID::Species peakSpecies, Double_t meanPt, Double_t absMeanEta, Double_t tofBinWidth, Double_t tpcBinWidth):
	TObject(),
	fSettings(AliSettingsDiHadronPID::Instance()),	
	fMassAssumption(massAssumption),
	fPeakSpecies(peakSpecies),
	//fNParametersNonStat(fNParameters),	
	fMeanPt(meanPt),
	fAbsMeanEta(absMeanEta),
	// fParameters(new Double_t[fNParameters]),
	// fParameterErrors(new Double_t[fNParameters]),
	// fParameterStepSize(new Double_t[fNParameters]),
	// fParameterIsFixed(new Bool_t[fNParameters]),	
	fTOFbinWidth(tofBinWidth),
	fTPCbinWidth(tpcBinWidth),
	fIsUsedInFit(fSettings->IsPeakIncludedInFit(massAssumption, peakSpecies, meanPt)),
	fUseTPCTail(fSettings->IsTPCTailIncluded(massAssumption, peakSpecies, meanPt))
	
{	
	// Default Constructor
	AliLogDiHadronPID::Debug2("Constructor called.", __func__, ClassName());
	ResetParameters();
	fNInstances++;

}

// -----------------------------------------------------------------------
AliTOFTPCPeakDiHadronPID::AliTOFTPCPeakDiHadronPID(const AliTOFTPCPeakDiHadronPID& other):
	TObject(other),
	fSettings(AliSettingsDiHadronPID::Instance()),	
	fMassAssumption(other.fMassAssumption),
	fPeakSpecies(other.fPeakSpecies),
	fMeanPt(other.fMeanPt),
	fAbsMeanEta(other.fAbsMeanEta),
	// fParameters(new Double_t[fNParameters]),
	// fParameterErrors(new Double_t[fNParameters]),
	// fParameterStepSize(new Double_t[fNParameters]),
	// fParameterIsFixed(new Bool_t[fNParameters]),	
	fTOFbinWidth(other.fTOFbinWidth),
	fTPCbinWidth(other.fTPCbinWidth),
	fIsUsedInFit(other.fIsUsedInFit),
	fUseTPCTail(other.fUseTPCTail)

{
	// Copy constructor.
	AliLogDiHadronPID::Debug2("Copy constructor called.", __func__, ClassName());
	SetParameters(other.fParameters);
	SetParameterErrors(other.fParameterErrors);
	SetParameterStepSize(other.fParameterStepSize);
	SetParameterIsFixed(other.fParameterIsFixed);
	fNInstances++;

}

// -----------------------------------------------------------------------
AliTOFTPCPeakDiHadronPID& AliTOFTPCPeakDiHadronPID::operator=(const AliTOFTPCPeakDiHadronPID& other) {

	// Assignment operator - probably don't need to overload, except for the extra log message.
	AliLogDiHadronPID::Debug2("Assignment operator called.", __func__, ClassName());
	
	// Protection agains self-assignment.
	if (this != &other) {

		// Assume fSettings is already a good pointer.
		TObject::operator=(other); // Call TObject's assignment operator.
		fMassAssumption = other.fMassAssumption;
		fPeakSpecies = other.fPeakSpecies;
		fMeanPt = other.fMeanPt;
		fAbsMeanEta = other.fAbsMeanEta;
		SetParameters(other.fParameters);
		SetParameterErrors(other.fParameterErrors);
		SetParameterStepSize(other.fParameterStepSize);
		SetParameterIsFixed(other.fParameterIsFixed);
		fTOFbinWidth = other.fTOFbinWidth;
		fTPCbinWidth = other.fTPCbinWidth;
		fIsUsedInFit = other.fIsUsedInFit;
		fUseTPCTail = other.fUseTPCTail;

	}

	return *this;

}

// -----------------------------------------------------------------------
AliTOFTPCPeakDiHadronPID::~AliTOFTPCPeakDiHadronPID() {
	
	// Destructor
	AliLogDiHadronPID::Debug2("Destructor called.", __func__, ClassName());

	// if (fParameters) {delete fParameters; fParameters = 0x0;}
	// if (fParameterErrors) {delete fParameterErrors; fParameterErrors = 0x0;}
	// if (fParameterStepSize) {delete fParameterStepSize; fParameterStepSize = 0x0;}
	// if (fParameterIsFixed) {delete fParameterIsFixed; fParameterIsFixed = 0x0;}

	fNInstances--;

}

// -----------------------------------------------------------------------
Double_t AliTOFTPCPeakDiHadronPID::operator()(Double_t tof, Double_t tpc) {

	// Evaluate the function with the current fit parameters.

	// should really use error functions if used in a fit!!

	Double_t tailPositionTOF = GetParValue(kSigmaTOF) * GetParValue(kSigmaTOF) * GetParValue(kTailTOF);
	
	if (fUseTPCTail) {
		Double_t tailPositionTPC = GetParValue(kSigmaTPC) * GetParValue(kSigmaTPC) * GetParValue(kTailTPC);

		return (AliFunctionsDiHadronPID::Gaussian2DTailXY(tof, tpc, GetParValue(kYield), 
			GetParValue(kMuTOF), GetParValue(kMuTPC), GetParValue(kSigmaTOF), GetParValue(kSigmaTPC), tailPositionTOF, tailPositionTPC, 
			fTOFbinWidth, fTPCbinWidth));

	} else {

		return (AliFunctionsDiHadronPID::Gaussian2DTailX(tof, tpc, GetParValue(kYield), 
			GetParValue(kMuTOF), GetParValue(kMuTPC), GetParValue(kSigmaTOF), GetParValue(kSigmaTPC), tailPositionTOF, 
			fTOFbinWidth, fTPCbinWidth));

	}

}

// -----------------------------------------------------------------------
Double_t AliTOFTPCPeakDiHadronPID::Integral(Double_t tofMin, Double_t tofMax, Double_t tpcMin, Double_t tpcMax) {

	// This method retuns the integral under the fit function in the desired range. Note that this 
	// integral is not normalised. To get the correct function value use Norm * Integral.

	// Note that the new implementation (AliFunctionsDiHadronPID::Integral*) takes the tail slope and not
	// the tail position (like in operator()).

	if (fUseTPCTail) {
		return AliFunctionsDiHadronPID::IntegralGaussian2DTailXY(tofMin, tofMax, tpcMin, tpcMax,
			GetParValue(kMuTOF), GetParValue(kMuTPC), 
			GetParValue(kSigmaTOF), GetParValue(kSigmaTPC),
			GetParValue(kTailTOF), GetParValue(kTailTPC));
	} else {
		return AliFunctionsDiHadronPID::IntegralGaussian2DTailX(tofMin, tofMax, tpcMin, tpcMax,
			GetParValue(kMuTOF), GetParValue(kMuTPC), 
			GetParValue(kSigmaTOF), GetParValue(kSigmaTPC),
			GetParValue(kTailTOF));
	}

}

// -----------------------------------------------------------------------
Double_t AliTOFTPCPeakDiHadronPID::Norm() {

	// This method returns the norm of the peak function.
	if (fUseTPCTail) { 
		return AliFunctionsDiHadronPID::NormGaussian2DTailXY(GetParValue(kYield),
			GetParValue(kSigmaTOF), GetParValue(kSigmaTPC),
			GetParValue(kTailTOF), GetParValue(kTailTPC));
	} else {
		return AliFunctionsDiHadronPID::NormGaussian2DTailX(GetParValue(kYield),
			GetParValue(kSigmaTOF), GetParValue(kTailTOF));
	}

}

// -----------------------------------------------------------------------
void AliTOFTPCPeakDiHadronPID::FillHist(TH2F* hist) {

	// This method assumes a histogram to be supplied, and then fills it accoring
	// to the peak's parameters.

	if(!hist) {return;}

	// Get ptrs to the TOF and TPC axes.
	TAxis* aTOF = hist->GetXaxis();
	TAxis* aTPC = hist->GetYaxis();

	// Calculate the norm.
	Double_t norm = Norm();

	for (Int_t iBinTOF = 1; iBinTOF <= aTOF->GetNbins(); iBinTOF++) {
		for (Int_t iBinTPC = 1; iBinTPC <= aTPC->GetNbins(); iBinTPC++) {
				
			hist->SetBinContent(iBinTOF, iBinTPC, norm * Integral(
				aTOF->GetBinLowEdge(iBinTOF), aTOF->GetBinUpEdge(iBinTOF),
				aTPC->GetBinLowEdge(iBinTPC), aTPC->GetBinUpEdge(iBinTPC)));
				
		}
	}

}


// -----------------------------------------------------------------------
void AliTOFTPCPeakDiHadronPID::FillHistdEdxRatio(TH2F* hist, Double_t expVal) {
	/**
	 * Fill a histogram with a peak transformed from dE/dx - expected to dE/dx over expected (used by Benjamin)
	 * expVal is the expected peak position in dE/dx (absolute units)
	 */
	if(!hist) {return;}

	// Get ptrs to the TOF and TPC axes.
	TAxis* aTOF = hist->GetXaxis();
	TAxis* aTPC = hist->GetYaxis();

	// Calculate the norm.
	Double_t norm = Norm();

	for (Int_t iBinTOF = 1; iBinTOF <= aTOF->GetNbins(); iBinTOF++) {
	  for (Int_t iBinTPC = 1; iBinTPC <= aTPC->GetNbins(); iBinTPC++) {
	
	    Double_t TPCdeltaMin = aTPC->GetBinLowEdge(iBinTPC)*expVal - expVal;
	    Double_t TPCdeltaMax = aTPC->GetBinUpEdge(iBinTPC)*expVal - expVal;
	    hist->SetBinContent(iBinTOF, iBinTPC, norm * 
				Integral(aTOF->GetBinLowEdge(iBinTOF), 
					 aTOF->GetBinUpEdge(iBinTOF),
					 TPCdeltaMin, TPCdeltaMax));
				
	  }
	}
	
}

// -----------------------------------------------------------------------
void AliTOFTPCPeakDiHadronPID::ResetParameters(Bool_t setToExpectedValues) {

	// By default we fix all parameters.
	for (Int_t iPar = 0; iPar < fNParameters; iPar++) {
		fParameters[iPar] = -1.;
		fParameterErrors[iPar] = -1.;
		fParameterStepSize[iPar] = fParameterStdStepSizes[iPar];
		fParameterIsFixed[iPar] = kTRUE;
	}

	if (setToExpectedValues) {

		// Set the mu, sigma depending on the available kinematic info, and info about the species.
		SetParValue(kMuTOF, AliFunctionsDiHadronPID::EstimateTOFPosition(fMeanPt, fAbsMeanEta, fSettings->GetSpeciesMass(fMassAssumption), fSettings->GetSpeciesMass(fPeakSpecies)) + fSettings->GetStdTOFShift());
		SetParValue(kSigmaTOF, fSettings->GetStdTOFResolution());
		SetParValue(kTailTOF, fSettings->GetStdTOFTailValue(fMassAssumption, fPeakSpecies));

                if (fSettings->GetUseDEDxRatios()) {
                  SetParValue(kMuTPC, AliFunctionsDiHadronPID::EstimateTPCPositionRatio(fMeanPt, fAbsMeanEta, fSettings->GetSpeciesMass(fMassAssumption), fSettings->GetSpeciesMass(fPeakSpecies)));
                }
                else 
                  SetParValue(kMuTPC, AliFunctionsDiHadronPID::EstimateTPCPosition(fMeanPt, fAbsMeanEta, fSettings->GetSpeciesMass(fMassAssumption), fSettings->GetSpeciesMass(fPeakSpecies)));
		SetParValue(kSigmaTPC, fSettings->GetStdTPCResolution());
		SetParValue(kTailTPC, fSettings->GetStdTPCTailValue(fMassAssumption, fPeakSpecies));
                if (fPeakSpecies == AliSettingsDiHadronPID::kElectron) {
                  SetParValue(kMuTPC, GetParValue(kMuTPC) + fSettings->GetStdTPCElectronShift());
                }

	}

}

// -----------------------------------------------------------------------
void AliTOFTPCPeakDiHadronPID::SetParameters(const Double_t* parameters) {

	for (Int_t iPar = 0; iPar < fNParameters; iPar++) {
		fParameters[iPar] = parameters[iPar];
	}

}

// -----------------------------------------------------------------------
void AliTOFTPCPeakDiHadronPID::SetParameterErrors(const Double_t* parerrors) {

	for (Int_t iPar = 0; iPar < fNParameters; iPar++) {
		fParameterErrors[iPar] = parerrors[iPar];
	}

}

// -----------------------------------------------------------------------
void AliTOFTPCPeakDiHadronPID::SetParameterStepSize(const Double_t* parstepsize) {

	for (Int_t iPar = 0; iPar < fNParameters; iPar++) {
		fParameterStepSize[iPar] = parstepsize[iPar];
	}

}

// -----------------------------------------------------------------------
void AliTOFTPCPeakDiHadronPID::SetParameterIsFixed(const Bool_t* parisfixed) {

	for (Int_t iPar = 0; iPar < fNParameters; iPar++) {
		fParameterIsFixed[iPar] = parisfixed[iPar];
	}

}

// -----------------------------------------------------------------------
TString AliTOFTPCPeakDiHadronPID::GetParNameWithSpecies(Parameter parameter) const {

	TString parNameWithSpecies = TString(AliSettingsDiHadronPID::GetSpeciesName(fPeakSpecies));
	parNameWithSpecies += " ";
	parNameWithSpecies += GetParName(parameter);

	return (parNameWithSpecies);

}

// -----------------------------------------------------------------------
TString AliTOFTPCPeakDiHadronPID::GetParNameWithSpecies(Parameter parameter, AliSettingsDiHadronPID::Species species) {

	TString parNameWithSpecies = TString(AliSettingsDiHadronPID::GetSpeciesName(species));
	parNameWithSpecies += " ";
	parNameWithSpecies += GetParName(parameter);

	return (parNameWithSpecies);

}

// -----------------------------------------------------------------------
void AliTOFTPCPeakDiHadronPID::Print(Option_t*) const {
	
	// Print info about the class.
	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	cout << "-----------------------------------------------------" << endl;
	cout << " " << AliSettingsDiHadronPID::GetSpeciesName(fPeakSpecies) << " Peak (m.a.: " << AliSettingsDiHadronPID::GetSpeciesName(fMassAssumption) << ")" << (fIsUsedInFit ? "" : " - NOT INCLUDED") << endl;
	cout << "-----------------------------------------------------" << endl;
	if (fParameters) {
		for (Int_t iPar = 0; iPar < fNParameters; iPar++) {
			cout << " " << fParameterNames[iPar] << ": ";
			if (iPar == 6 && fUseTPCTail == kFALSE) {cout << "NOT USED!" << endl;}
			else {cout << fParameters[iPar] << " ± " << fParameterErrors[iPar] << " (step size: " << fParameterStepSize[iPar] << ")" << (fParameterIsFixed[iPar] ? " - FIXED" : " - FREE") << endl;}
		}
		cout << endl;
		cout << " Mean pt: " << fMeanPt << endl;
		cout << " Abs Mean Eta: " << fAbsMeanEta << endl;
		cout << " TOF bin width: " << fTOFbinWidth << endl;
		cout << " TPC bin width: " << fTPCbinWidth << endl; 
	}
	cout << "-----------------------------------------------------" << endl;

}
