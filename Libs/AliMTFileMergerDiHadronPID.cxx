/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  File merger that uses multiple threads.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include <stdio.h>
#include <pthread.h>
#include "TSystem.h"
#include "TThread.h"
#include "TFile.h"
#include "TFileMerger.h"
#include "AliMTFileMergerDiHadronPID.h"

ClassImp(AliMTFileMergerDiHadronPID);

// -----------------------------------------------------------------------
AliMTFileMergerDiHadronPID::AliMTFileMergerDiHadronPID() :
	TObject(),
	fMaxThreads(1),
	fOutputFile("out.root"),
	fFileList()

{

	SetMaxThreads(-1);
	//fFileList.SetOwner(kTRUE);

}

// -----------------------------------------------------------------------
AliMTFileMergerDiHadronPID::~AliMTFileMergerDiHadronPID() {

}

// -----------------------------------------------------------------------
void AliMTFileMergerDiHadronPID::PrintStatus() {

	// Displays the status of the merger.
	printf("\n");
	printf("---------------------------------------------\n");
	printf(" Status\n");
	printf("---------------------------------------------\n");
	printf("Max number of threads: %i.\n", fMaxThreads);
	printf("Output file: %s.\n", fOutputFile.Data());
	if (fFileList.GetEntriesFast()) {
		printf("List of input files:\n");
		for (Int_t iFile = 0; iFile < fFileList.GetEntriesFast(); ++iFile) {
			printf("  %s\n", ((TFile*)fFileList.At(iFile))->GetName());
		}
	} else {
		printf("No input files given.\n");
	}
}

// -----------------------------------------------------------------------
Bool_t AliMTFileMergerDiHadronPID::AddFile(TString filename) {

	// Checks the file's existence, and adds it to the merge list.
	TFile* ftest = new TFile(filename.Data());
	if (ftest->IsZombie()) {
		printf("Could not open file: %s!\n", filename.Data());
		return kFALSE;
	}

	fFileList.AddLast(ftest);
	printf("Added file: %s.\n", filename.Data());

	return kTRUE;

}

// -----------------------------------------------------------------------
Bool_t AliMTFileMergerDiHadronPID::SetOutputFile(TString filename) {

	// Sets the target output file.
	fOutputFile = filename;
	return kTRUE;

}

// -----------------------------------------------------------------------
void AliMTFileMergerDiHadronPID::GenerateFileList(TString filename, TString path) {

	// Searches a certain file name in a directory (and its subdirectories).
	FILE* pipe = gSystem->OpenPipe(Form("find %s -name %s", path.Data(), filename.Data()),"r");
	TString tmpFileName;
	while (tmpFileName.Gets(pipe)) {AddFile(tmpFileName);}	

}

// -----------------------------------------------------------------------
void AliMTFileMergerDiHadronPID::SetMaxThreads(Int_t nThreads) {

	// Sets the maximum number of threads. If argument < 0, then
	// the number is set to the amount of cores.

	// Determine the number of cores available.
	Int_t nCores = -1;
	FILE* pipe = gSystem->OpenPipe("echo $MJ","r");
	TString tmpStr;
	tmpStr.Gets(pipe);

	if (tmpStr.IsDigit()) {

		// Number of cores was determined.
		nCores = tmpStr.Atoi() - 1;
		//printf("The number of cores on this computer is: %i.\n", nCores);

		if (nThreads < 0) {
			fMaxThreads = nCores;
		} else {
			if (nThreads > nCores) {
				printf("WARNING: Number of requested threads exceeds number of cores.\n");
			}
			fMaxThreads = nThreads;
		}

	} else {

		// Number of cores was not determined.
		printf("The number of cores on this computer is unknown.\n");
		
		if (nThreads < 0) {
			printf("Cannot set the max number of threads equal to the number of cores.\n");
			fMaxThreads = 1;
		} else {
			fMaxThreads = nThreads;
		}

	}

	//printf("Max number of threads was set to: %i.\n", fMaxThreads);

}

// -----------------------------------------------------------------------
Bool_t AliMTFileMergerDiHadronPID::Merge() {

	const char* filename = "tmpmerge";
	Int_t index = 0;
	Int_t nFilesToMerge = fFileList.GetEntriesFast();
	printf("%i\n", __LINE__);
	// Check if the number of threads is appropriate for the number of files.
	// TODO: Assumes a max of 4 cores to be used...
	printf("%i\n", __LINE__);
	if (nFilesToMerge < 5 && fMaxThreads > 2) {fMaxThreads = 2;}
	else if (nFilesToMerge < 7 && fMaxThreads > 3) {fMaxThreads = 3;}
	printf("%i\n", __LINE__);
	// Create merge list for the threads.
	TFileMerger*	threadmergers[fMaxThreads];
	TThread*		threads[fMaxThreads];
	printf("%i\n", __LINE__);
	for (Int_t iThread = 0; iThread < fMaxThreads; ++iThread) {
	printf("%i\n", __LINE__);
		printf("%s: Creating Thread: %i.\n", __func__, iThread);
	printf("%i\n", __LINE__);
		threadmergers[iThread] = new TFileMerger();
		threadmergers[iThread]->OutputFile(Form("%s%i.root", filename, iThread));
	printf("%i\n", __LINE__);
		// Determine the number of files for this thread.
		Int_t nFilesForThisThread = nFilesToMerge / fMaxThreads;
		if (nFilesToMerge % fMaxThreads >= (iThread + 1)) {nFilesForThisThread++;}
	printf("%i\n", __LINE__);
		for (Int_t iFile = 0; iFile < nFilesForThisThread; ++iFile) {
			threadmergers[iThread]->AddFile((TFile*)(fFileList.At(iFile + index)));
		}
	printf("%i\n", __LINE__);		
		index += nFilesForThisThread;
	printf("%i\n", __LINE__);		
		// Starting the thread.
		TThread::VoidRtnFunc_t rf = AliMTFileMergerDiHadronPID::merge;	// Needs to be static.
		threads[iThread] = new TThread(Form("thread%i",iThread), rf, (void*)threadmergers[iThread]); 
		threads[iThread]->Run();
	printf("%i\n", __LINE__);		
	}
	printf("%i\n", __LINE__);
	TFileMerger merger;
	merger.OutputFile(fOutputFile.Data());
	printf("%i\n", __LINE__);
	// Wait for all threads to finish, and delete pointers.
	for (Int_t iThread = 0; iThread < fMaxThreads; ++iThread) {
	printf("%i\n", __LINE__);
		// Wait for iThread.
		threads[iThread]->Join();
		printf("%s: Thread %i joined.\n", __func__, iThread);
	printf("%i\n", __LINE__);		
		// Delete objects on the heap used by thread iThread.
		delete threadmergers[iThread]; threadmergers[iThread] = 0x0;
		delete threads[iThread]; threads[iThread] = 0x0;
	printf("%i\n", __LINE__);
		// Add the output of iThread to the final merger.
		merger.AddFile(Form("%s%i.root", filename, iThread));
	printf("%i\n", __LINE__);
	}
	printf("%i\n", __LINE__);
	// Final merge.
	merger.Merge();
	printf("%i\n", __LINE__);
	return 0;

}

// -----------------------------------------------------------------------
void* AliMTFileMergerDiHadronPID::merge(void *merger) {

	// Accepts a TFileMerger.
	((TFileMerger*)merger)->Merge();
	return 0x0;

}
