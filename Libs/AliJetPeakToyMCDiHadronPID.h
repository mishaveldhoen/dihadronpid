#ifndef ALIJETPEAKTOYMCDIHADRONPID_H
#define ALIJETPEAKTOYMCDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TObject.h"
#include "TString.h"
#include "TVectorF.h"

class AliJetPeakToyMCDiHadronPID : public TObject {
	
public:
	AliJetPeakToyMCDiHadronPID();

	void 						RunSimulation();

	// Setters.
	void						SetOutputDirectory(const char* outputDir);
	void						SetOutputFileSuffix(const char* fileSuffix);
	void						SetNRepetitions(Int_t nRepetitions);
    void                        SetNTriggers(Double_t nTriggers);
	void 						SetBgdDensity(Double_t bgdDensity);
	void						SetJetYield(Double_t jetYield);
	void						SetJetSTD(Double_t jetSTD);
	void						SetPeakAlpha(Double_t alpha);
	void						SetBackgroundCircle(Double_t backgroundCircle);
	void						SetMaxDEta(Double_t maxDEta);
	void 						SetFlowCoefficient(Int_t n, Double_t value);
	void 						SetMinMaxFlowCoefficientsSubtracted(Int_t min, Int_t max);

	void						SetUseWideJet(Bool_t useWideJet = kTRUE);
	void						SetWideJetSTD(Double_t wideJetSTD);
	void						SetWideJetYieldPct(Double_t wideJetYieldPct);

	void 						Print(Option_t*) const;

    // Getters.
    TString                     GetOutputFileNameAndPath() const;

private:

	// Default Analysis settings.
	TString						fOutputDirectory;
	TString						fFileSuffix;
	Int_t						fNRepetitions;
    Double_t                    fNTriggers;
	Double_t					fBgdDensity;
	Double_t					fJetYield;
	Double_t					fJetSTD;
	Double_t					fPeakAlpha;
	Double_t					fBackgroundCircle; // Size of the circle with bins that are ignored when determining the background.	
	Double_t 					fMaxDEta;
	TVectorF					fFlowCoefficients;
	Int_t						fMinFlowCoefficientSubtracted;
	Int_t						fMaxFlowCoefficientSubtracted;

	// Systematic Uncertainty.
	Bool_t						fUseWideJet;
	Double_t					fWideJetSTD;
	Double_t					fWideJetYieldPct;

	ClassDef(AliJetPeakToyMCDiHadronPID, 1);

};

#endif
