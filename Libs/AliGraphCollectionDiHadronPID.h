#ifndef ALIGRAPHCOLLECTIONDIHADRONPID_H
#define ALIGRAPHCOLLECTIONDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TObject.h"
#include "TPad.h"
#include "TLegend.h"
#include "AliGraphDiHadronPID.h"

class AliGraphCollectionDiHadronPID : public TNamed {
	
public:
	AliGraphCollectionDiHadronPID();
	AliGraphCollectionDiHadronPID(const char* name, const char* title);
	~AliGraphCollectionDiHadronPID();

	Bool_t						AddGraph(AliGraphDiHadronPID* graph);
	void						Show(TPad* pad = 0x0, Bool_t showStat = kTRUE, Bool_t showSyst = kTRUE);
	void						ShowStat(TPad* pad = 0x0) {Show(pad, kTRUE, kFALSE);}
	void						ShowSyst(TPad* pad = 0x0) {Show(pad, kFALSE, kTRUE);}

    // Getters.
	Double_t					GetMinX();
	Double_t					GetMaxX();
	const char*					GetTitleX() {return (fGraphs->GetEntries() ? ((AliGraphDiHadronPID*)fGraphs->At(0))->GetTitleX() : "");}	
	Double_t					GetMinY(Bool_t useLog = kFALSE);
	Double_t					GetMaxY(Bool_t useLog = kFALSE);
	const char*					GetTitleY() {return (fGraphs->GetEntries() ? ((AliGraphDiHadronPID*)fGraphs->At(0))->GetTitleY() : "");}

    // Operations.
	void						Scale(Double_t scaleFactor);

	void						UpdateLegend(TLegend* legend);

	void 						Print(Option_t*) const;

private:
	TObjArray*					fGraphs;
	TObjArray*					fOwnedObjects;

	static Int_t				fMaxNGraphs;

	ClassDef(AliGraphCollectionDiHadronPID, 1);

};

#endif
