TestGaussianFit() {

	// First load alifunctions.cxx

	TH1F* test = new TH1F("test","test",10,-3.,3.);
	test->FillRandom("gaus",1000);


	TF1* func = new TF1("func","AliFunctionsDiHadronPID::Gaussian1D(x,[0],[1],[2],[3])",-3,3);
	func->SetParNames("Integral","#mu","#sigma","binwidth");
	func->SetParameters(1000,0,1,0.6);

	test->Fit(func);

}
