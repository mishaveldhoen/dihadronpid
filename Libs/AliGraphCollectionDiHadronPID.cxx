/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Graph Collection Class
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "AliGraphCollectionDiHadronPID.h"
#include "TCanvas.h"
#include "TList.h"
#include "AliLogDiHadronPID.h"

Int_t AliGraphCollectionDiHadronPID::fMaxNGraphs = 6;

ClassImp(AliGraphCollectionDiHadronPID);

// -----------------------------------------------------------------------
AliGraphCollectionDiHadronPID::AliGraphCollectionDiHadronPID():
	TNamed(),
	fGraphs(0x0),
	fOwnedObjects(0x0)

{
	
	// Default Constructor
	AliLogDiHadronPID::Debug2("Default constructor called.", __func__, ClassName());

	fGraphs = new TObjArray();
	fGraphs->SetOwner(kTRUE);

	fOwnedObjects = new TObjArray();
	fOwnedObjects->SetOwner(kTRUE);


}

// -----------------------------------------------------------------------
AliGraphCollectionDiHadronPID::AliGraphCollectionDiHadronPID(const char* name, const char* title):
	TNamed(name, title),
	fGraphs(0x0),
	fOwnedObjects(0x0)

{
	
	// Default Constructor
	AliLogDiHadronPID::Debug2("Default constructor called.", __func__, ClassName());

	fGraphs = new TObjArray();
	fGraphs->SetOwner(kTRUE);

	fOwnedObjects = new TObjArray();
	fOwnedObjects->SetOwner(kTRUE);


}

// -----------------------------------------------------------------------
AliGraphCollectionDiHadronPID::~AliGraphCollectionDiHadronPID() {
	
	// Destructor
	AliLogDiHadronPID::Debug2("Destructor called.", __func__, ClassName());

	if (fGraphs) {delete fGraphs; fGraphs = 0x0;}
	if (fOwnedObjects) {delete fOwnedObjects; fOwnedObjects = 0x0;}

}

// -----------------------------------------------------------------------
Bool_t AliGraphCollectionDiHadronPID::AddGraph(AliGraphDiHadronPID* graph) {

	if (fGraphs->GetEntriesFast() == fMaxNGraphs) {
		AliLogDiHadronPID::Error(Form("Max number of graphs (%i in the current implementation) in the collection is reached.", fMaxNGraphs), __func__, ClassName());
		return kFALSE;
	}

	fGraphs->AddLast(graph);
	return kTRUE;

}

// -----------------------------------------------------------------------
void AliGraphCollectionDiHadronPID::Show(TPad* pad, Bool_t showStat, Bool_t showSyst) {

	Int_t nGraphs = fGraphs->GetEntriesFast();
	if (!nGraphs) {
		AliLogDiHadronPID::Error(Form("GraphCollection does not contain any graphs."), __func__, ClassName());
		return;
	}	

	if (!pad) {	
		pad = TCanvas::MakeDefCanvas();
		fOwnedObjects->AddLast(pad);
	} else {
		pad->cd();
	}

	// Draw a frame if the pad doesn't contain any primitives.
	if ((pad->GetListOfPrimitives())->GetEntries() == 0) { 
		TH1F* frame = pad->DrawFrame(GetMinX(), GetMinY(), GetMaxX(), GetMaxY(), GetTitle());
		fOwnedObjects->AddLast(frame);

		// Put the correct labels on the axes.
		frame->SetXTitle(((AliGraphDiHadronPID*)fGraphs->At(0))->GetTitleX());
		frame->SetYTitle(((AliGraphDiHadronPID*)fGraphs->At(0))->GetTitleY());
	}

	for (Int_t iGraph = 0; iGraph < nGraphs; iGraph++) {

		if (showSyst) {
			AliGraphDiHadronPID* currentGraph = (AliGraphDiHadronPID*)fGraphs->At(iGraph);
			if (iGraph == 0) {currentGraph->SetSystErrorStyle(AliGraphDiHadronPID::kSolidBlock);}
			else {currentGraph->SetSystErrorStyle(AliGraphDiHadronPID::kOpenBlock);}
			currentGraph->ShowSyst(pad, nGraphs, iGraph);
		}
		if (showStat) {((AliGraphDiHadronPID*)fGraphs->At(iGraph))->ShowStat(pad, nGraphs, iGraph);}
	}

}

// -----------------------------------------------------------------------
Double_t AliGraphCollectionDiHadronPID::GetMinX() {

	Int_t nGraphs = fGraphs->GetEntriesFast();
	if (!nGraphs) {
		AliLogDiHadronPID::Error(Form("GraphCollection does not contain any graphs."), __func__, ClassName());
		return 0.;
	}	

	Double_t min = ((AliGraphDiHadronPID*)fGraphs->At(0))->GetMinX();
	for (Int_t iGraph = 0; iGraph < nGraphs; iGraph++) {

		min = TMath::Min(min, ((AliGraphDiHadronPID*)fGraphs->At(iGraph))->GetMinX());

	}

	return min;

}

// -----------------------------------------------------------------------
Double_t AliGraphCollectionDiHadronPID::GetMaxX() {

	Int_t nGraphs = fGraphs->GetEntriesFast();
	if (!nGraphs) {
		AliLogDiHadronPID::Error(Form("GraphCollection does not contain any graphs."), __func__, ClassName());
		return 0.;
	}	

	Double_t max = ((AliGraphDiHadronPID*)fGraphs->At(0))->GetMaxX();
	for (Int_t iGraph = 0; iGraph < nGraphs; iGraph++) {

		max = TMath::Max(max, ((AliGraphDiHadronPID*)fGraphs->At(iGraph))->GetMaxX());

	}

	return max;

}

// -----------------------------------------------------------------------
Double_t AliGraphCollectionDiHadronPID::GetMinY(Bool_t useLog) {

	Int_t nGraphs = fGraphs->GetEntriesFast();
	if (!nGraphs) {
		AliLogDiHadronPID::Error(Form("GraphCollection does not contain any graphs."), __func__, ClassName());
		return 0.;
	}	

	Double_t min = ((AliGraphDiHadronPID*)fGraphs->At(0))->GetMinY(useLog);
	for (Int_t iGraph = 0; iGraph < nGraphs; iGraph++) {

		min = TMath::Min(min, ((AliGraphDiHadronPID*)fGraphs->At(iGraph))->GetMinY(useLog));

	}

	return min;

}

// -----------------------------------------------------------------------
Double_t AliGraphCollectionDiHadronPID::GetMaxY(Bool_t useLog) {

	Int_t nGraphs = fGraphs->GetEntriesFast();
	if (!nGraphs) {
		AliLogDiHadronPID::Error(Form("GraphCollection does not contain any graphs."), __func__, ClassName());
		return 0.;
	}	

	Double_t max = ((AliGraphDiHadronPID*)fGraphs->At(0))->GetMaxY(useLog);
	for (Int_t iGraph = 0; iGraph < nGraphs; iGraph++) {

		max = TMath::Max(max, ((AliGraphDiHadronPID*)fGraphs->At(iGraph))->GetMaxY(useLog));

	}

	return max;

}

// -----------------------------------------------------------------------
void AliGraphCollectionDiHadronPID::Scale(Double_t scaleFactor) {

    /** 
     * Scale all currently owned graphs with scaleFactor.
     */

    AliGraphDiHadronPID* currentGraph = 0x0;
    TIterator* iter = fGraphs->MakeIterator();    
    while ((currentGraph = (AliGraphDiHadronPID*)iter->Next())) {
        currentGraph->Scale(scaleFactor);
    }

}

// -----------------------------------------------------------------------
void AliGraphCollectionDiHadronPID::UpdateLegend(TLegend* legend) {

	for (Int_t iGraph = 0; iGraph < fGraphs->GetEntriesFast(); iGraph++) {

		((AliGraphDiHadronPID*)fGraphs->At(iGraph))->UpdateLegend(legend);

	}

}

// -----------------------------------------------------------------------
void AliGraphCollectionDiHadronPID::Print(Option_t*) const {
	
	// Print info about the class.
	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

}
