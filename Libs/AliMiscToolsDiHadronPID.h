#ifndef ALIMISCTOOLSDIHADRONPID_H
#define ALIMISCTOOLSDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TString.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TSpline.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TF1.h"

/**
 * Toolbox containing misc. tools. Class only has static members.
 */
class AliMiscToolsDiHadronPID {
	
private:
	AliMiscToolsDiHadronPID() {;}
	~AliMiscToolsDiHadronPID() {;}

public:
	static Bool_t				CreateDir(TString dirName);
	static TFile*				OpenROOTFile(TString fileName, const char* option = "");
	static void					PDFfyROOTFile(TString fileName);

	// Text formatting tools.
	static TString				FormatTime(Double_t timeInSec);
	static TString				AppendToFileName(TString fileName, TString appendix);
	static TString				GetFlowFormula(Int_t VnMax = 8, Int_t firstParIndex = 0);

	// TSpline tools.
	static Double_t 			SplineIntegral(const TSpline* spline, Double_t xMin, Double_t xMax, Int_t nPoints);

	// TGraph tools.
	static Int_t				FindClosestPoint(const TGraph* graph, Double_t xx);
	static void 				ScaleGraphPoint(TGraph* graph, Int_t iPoint, Double_t cc);
	static void					ScaleGraph(TGraph* graph, Double_t cc);
	static void		                ScaleGraphPt(TGraph* graph);
	static void					DivideGraph(TGraph* graph, const TF1* func);
	static void					AddGraphs(TGraph* graph1, const TGraph* graph2);
	static TGraph*				GetErrorBandGraph(const TGraphErrors* graph, Bool_t up = kTRUE);

	// TF1 tools.
	static TGraphErrors*		GetNSigmaBand(TF1* func, Double_t nSigma = 1., Int_t parIndex = -1);

	// Apply Geant Fluka correction to tracking or matching efficiencies.
	static void					ApplyGeantFlukaCorrection(TH1F* hist, const Int_t species, const Bool_t isPos, const Bool_t isTrackingEff);
	static void					ApplyGeantFlukaCorrection(TH2F* hist, const Int_t species, const Bool_t isPos, const Bool_t isTrackingEff);

	static void					TestGeantFlukaCorrection();

	// Create (pT,Eta) mask for retrieving inclusive pT-spectra from (pT, Eta)-spectra.
	static void 				MakePtEtaMask(TH2F* hist, const Int_t iSpecies, const Int_t nMCHits);

};

#endif
