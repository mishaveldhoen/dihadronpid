#ifndef ALIPROCESSDATADIHADRONPIDMINI_H
#define ALIPROCESSDATADIHADRONPIDMINI_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TObject.h"
#include "TObjArray.h"
#include "TFile.h"
#include "TBranch.h"
#include "TAxis.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "THn.h"
#include "TDirectory.h"
#include "AliObjectArray2DDiHadronPID.h"
#include "AliMiniTrackDiHadronPID.h"
#include "AliFitTemplatesDiHadronPID.h"
#include "AliSettingsDiHadronPID.h"
#include "AliMiniEventDiHadronPID.h"
#include "AliFunctionsDiHadronPID.h"

/** 
 * \brief Implements various analyses that one can perform on the output 
 * of AliAnalysisTaskDiHadronPIDMini.
 *
 * \todo In principle we would like to phase out the this class, as it 
 * has become too bulky and cumbersome to maintain. It will be replaced 
 * by the classes deriving from the much leaner class 
 * AliAnalyzeDataDiHadronPID.
 */

class AliProcessDataDiHadronPIDMini : public TObject {

public:
	AliProcessDataDiHadronPIDMini(Int_t massAssumption = 0);
	~AliProcessDataDiHadronPIDMini();
	
	// Setters.
	void 							SetMaxNThreads(UInt_t maxNThreads) {fMaxNThreads = maxNThreads;}
	void							SetDebugLevel(Int_t lvl) {fDebugLevel = lvl;}
	void							SetVerboseLevel(Int_t lvl) {fVerboseLevel = lvl;}
	void							SetWorkDir(TString dir) {fWorkDir = dir;}
	void							SetOutputDir(TString dir) {fOutputDir = dir;}
	void							SetOutputFileName(TString name) {fOutputFileName = name;}

	// Getters.
	UInt_t							GetMaxNThreads() const {return fMaxNThreads;}	// Different threads on different files?
	Int_t							GetDebugLevel() const {return fDebugLevel;}
	Int_t							GetVerboseLevel() const {return fVerboseLevel;}
	Int_t							GetMassAssumption() const {return fMassAssumption;}

	// Analysis Methods.
	void							MakeQAHistos();
	void							MakeUISpectrum(const char* fileName = "output.root");
	void							MakePtEtaSpectra(const char* fileName = "output.root");
	void							MakeTemplates(const char* fileName = "etaDEtaMap.root");
	void							Correlate(Int_t vtxZbin, Int_t centBin);
	// void							CorrelateAll(); // SHOULD BE CHECKED...
	void							MakeEfficiencies(const char* fileName = "Efficiencies.root");
	void							MakeDCATemplates(const char* fileName = "DCATemplates.root");

	// I/O Operations.
	Bool_t							SaveBuffer(TString filename);

private:
	// Misc. Methods.
	Bool_t							GenerateFileList();
	void 							CheckFileList();
	Bool_t							WriteObj(TObject* obj, TDirectory* dir);
	Bool_t							WriteObjAndDelete(TObject* obj, TDirectory* dir);
	static Double_t					MixedEventNorm(const TH2F* hist);

	// Buffer Operations.
	Bool_t							CreateBuffer();
	void							ClearBuffer();
	//void							PerformMECorr();
	//void							UpdateBuffer();

	// Event iteration methods.
	Bool_t							NextFile(Bool_t reset = kFALSE);
	Bool_t							GetEvent();

	// Event pool methods.
	Bool_t							IsSelected(const AliMiniEventDiHadronPID* event, Int_t vtxZbin, Int_t centBin) const;
	Bool_t							IsSelected(const AliMiniEventDiHadronPID* event) const;
	Bool_t							IsTrackInPool(const AliMiniTrackDiHadronPID* track) const;
	void							PrintPoolPointers() const;
	Int_t							GetPoolArrayIndex(Int_t vtxZBin, Int_t centBin) const; // Obsolete?
	Int_t							GetPoolArrayIndex(Double_t vtxZ, Double_t cent) const; // Obsolete?

	// Trigger Pool methods.
	void							ResetTriggerPool();

	// Methods with the right signature to become a TThread.
	static void*					CorrectForMixedEvent(void* fnames);
	static void*					Merge(void* merger);

private:
	// Analysis Settings Singleton.
	AliSettingsDiHadronPID*			fSettings;				//! Not Stored

	// I/O Settings (OLD!).
	UInt_t 							fMaxNThreads;			// Maximum number of threads used by the task.
	TString							fDataDir;				// REVIEW NAME
	TString							fInputFileName;			// REVIEW NAME
	TString							fWorkDir;				// REVIEW NAME
	TString							fOutputDir;				// REVIEW NAME
	TString							fOutputFileName;		// REVIEW NAME
	TString							fCorrExtension;			//

	// Operation variables.
	TObjArray*						fInputFileList;			//!
	Int_t							fCurrentFileIndex;		//!
	TFile*							fCurrentFile;			//!
	TBranch*						fCurrentEventBranch;	//!
	Int_t							fCurrentEventIndex;		//!
	AliMiniEventDiHadronPID*		fCurrentEvent;			//!

	// Histogram Buffers.
	Bool_t							fCurrentExists;			// RENAME!
	TH1F*							fCurrentTriggerHist;	//!
	TObjArray*						fCurrentCorrPID;		//!
	TObjArray*						fCurrentMixedPID;		//!
	TObjArray*						fCurrentCorrUI;			//!
	TObjArray*						fCurrentMixedUI;		//!
	
	TObjArray*						fCurrentCorrMCPion;		//!
	TObjArray*						fCurrentMixedMCPion;	//!
	TObjArray*						fCurrentCorrMCKaon;		//!
	TObjArray*						fCurrentMixedMCKaon;	//!
	TObjArray*						fCurrentCorrMCProton;	//!
	TObjArray*						fCurrentMixedMCProton;	//!

	//AliFitTemplatesDiHadronPID*		fCurrentFitTemplates;	//!

	// Event mixing.
	TObjArray*						fTriggerPool;			//
	Bool_t							fPoolIsFilled;			// 
	Int_t							fStartedMixingAtFile;	//
	Int_t							fStartedMixingAtEvent;	//
	Int_t							fPoolWriteIndex;		//

	// Analysis Parameters.
	TAxis*							fVertexZAxis;			//-> TAxis has "FindBin" functionality. 
	TAxis*							fCentralityAxis;		//->	
	TAxis*							fPtAxis;				//->
	TAxis*							fEtaAxis;				//->
	TAxis*							fDEtaAxis;				//->
	TAxis*							fDPhiAxis;				//->
	TAxis*							fTOFAxis;				//->
	TAxis*							fTPCAxis;				//->
	Int_t							fNbinsDPhi;				// REDUNDANT
	Int_t							fNbinsDEta;				// REDUNDANT
	Int_t							fNbinsTOF;				//
	Int_t							fNbinsTPC;				//
	AliSettingsDiHadronPID::Species	fMassAssumption;		//

	// Feedback.
	Int_t							fDebugLevel;			//!
	Int_t							fVerboseLevel;			//! max = 2

	ClassDef(AliProcessDataDiHadronPIDMini, 1);

};

#endif
