#ifndef ALILOGDIHADRONPID_H
#define ALILOGDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#ifndef ROOT_Rtypes
#include "Rtypes.h"
#endif

#include <iostream>
#include <fstream>
using namespace std;

class AliLogDiHadronPID {

private:

	// Default: Print errors and warnings.
	AliLogDiHadronPID() : fVerboseLevel(2), fLogLevel(2), fDebugLevel(0), fLogFile(0x0) {;}
	AliLogDiHadronPID(const AliLogDiHadronPID&);
	AliLogDiHadronPID& operator=(const AliLogDiHadronPID&);
	~AliLogDiHadronPID();

public:

	enum msgType {kInfo, kWarning, kError, kDebug1, kDebug2, kDebug3, kDebug4};

	// Instance management.
	static AliLogDiHadronPID* 	Instance();
	static void 				Kill();

	// Verbose level, how much is printed on screen.
	void						SetVerboseLevel(Int_t level) {fVerboseLevel = level;}
	Int_t						GetVerboseLevel() {return fVerboseLevel;}

	// Log level, how much is written in the log file.
	void 						SetLogLevel(Int_t level) {fLogLevel = level;}
	Int_t						GetLogLevel() {return fLogLevel;}

	// Debug level, whether debug messages are put on screen/ written in the log.
	void 						SetDebugLevel(Int_t level) {fDebugLevel = level;}
	Int_t						GetDebugLevel() {return fDebugLevel;}

	// File Output.
	void						OpenLogFile(const char* filename);
	void 						CloseLogFile();

	// Messages.
	static void					Info(const char* msg, const char* funcname = 0x0, const char* clname = 0x0) {Instance()->Message(kInfo, msg, funcname, clname);}
	static void					Warning(const char* msg, const char* funcname = 0x0, const char* clname = 0x0) {Instance()->Message(kWarning, msg, funcname, clname);}
	static void					Error(const char* msg, const char* funcname = 0x0, const char* clname = 0x0) {Instance()->Message(kError, msg, funcname, clname);}

	// Debug messages.
	//	Level 1: Temporary messages.
	//  Level 2: Constructor/ destructor calls.
	//  Level 3: Calls of infrequently called methods, other than the constructor/ destructor.
	//  Level 4: All other fuction calls.
	static void					Debug1(const char* msg, const char* funcname = 0x0, const char* clname = 0x0) {Instance()->Message(kDebug1, msg, funcname, clname);}
	static void					Debug2(const char* msg, const char* funcname = 0x0, const char* clname = 0x0) {Instance()->Message(kDebug2, msg, funcname, clname);}
	static void					Debug3(const char* msg, const char* funcname = 0x0, const char* clname = 0x0) {Instance()->Message(kDebug3, msg, funcname, clname);}
	static void					Debug4(const char* msg, const char* funcname = 0x0, const char* clname = 0x0) {Instance()->Message(kDebug4, msg, funcname, clname);}

	// Introspection.
	void						Print() const;

private:
	void 						Message(msgType type, const char* msg, const char* funcname = 0x0, const char* clname = 0x0);

private:
	
	static AliLogDiHadronPID*	fLogger;
	Int_t						fVerboseLevel;
	Int_t						fLogLevel;
	Int_t						fDebugLevel;
	ofstream*					fLogFile;

};

#endif
