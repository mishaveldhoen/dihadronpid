#ifndef ALITOFTPCFITFUNCTIONDIHADRONPID_H
#define ALITOFTPCFITFUNCTIONDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TObject.h"
#include "TString.h"
#include "TH2F.h"
#include "AliTOFTPCPeakDiHadronPID.h"
#include "AliSettingsDiHadronPID.h"

class AliTOFTPCFitFunctionDiHadronPID : public TObject {

public:
	AliTOFTPCFitFunctionDiHadronPID();
	AliTOFTPCFitFunctionDiHadronPID(AliSettingsDiHadronPID::Species massAssumption, Double_t ptmin, Double_t ptmax, Double_t etamin, Double_t etamax, const TH2F* data = 0x0, const TH2F* mismatch = 0x0);
	AliTOFTPCFitFunctionDiHadronPID(const AliTOFTPCFitFunctionDiHadronPID& other);
	AliTOFTPCFitFunctionDiHadronPID& operator=(const AliTOFTPCFitFunctionDiHadronPID& other);

	~AliTOFTPCFitFunctionDiHadronPID();

	// Public function evaluation methods.
	double 											operator()(double* xy);						// Fit function evaluated with the data members - Doesn't include mismatches!
	// Double_t			EvalSpecies(Double_t TOF, Double_t TPC, Int_t species);
	// Double_t			EvalSpeciesIntOne(Double_t TOF, Double_t TPC, Int_t species);

private:

	// Initialisation.
	void											EstimateParticleYields(const TH2F* data);
	void											EstimateMismatchYield(const TH2F* data, const TH2F* mismatch);

public:
	void											ReInitialiseYields(const TH2F* data, const TH2F* mismatch); // This method is used when fit parameters have changed by another object (i.e., different from the default).

	// Getters
	AliTOFTPCPeakDiHadronPID*						GetPeak(Int_t iSpecies) {return GetPeak((AliSettingsDiHadronPID::Species)iSpecies);}
	const AliTOFTPCPeakDiHadronPID*					GetPeak(Int_t iSpecies) const {return GetPeak((AliSettingsDiHadronPID::Species)iSpecies);}
	AliTOFTPCPeakDiHadronPID*						GetPeak(AliSettingsDiHadronPID::Species species) {return (AliTOFTPCPeakDiHadronPID*)fPeaks->At(species);}
	const AliTOFTPCPeakDiHadronPID*					GetPeak(AliSettingsDiHadronPID::Species species) const {return (AliTOFTPCPeakDiHadronPID*)fPeaks->At(species);}

	Double_t										GetMismatchYield() const {return fMismatchYield;}
	Double_t										GetMismatchYieldError() const {return fMismatchYieldError;}
	Double_t										GetMismatchStepSize() const {return fMismatchYieldStep;}

	// Methods used to initialise the fitter.
	Bool_t											IsParFixedAtIndex(Int_t index) const;
	static TString									GetParNameAtIndex(Int_t index);
	Double_t										GetParValueAtIndex(Int_t index) const;
	Double_t										GetParErrorAtIndex(Int_t index) const;
	Double_t										GetParStepAtIndex(Int_t index) const; 

	Int_t											GetFitStatus() const {return fFitStatus;}

	// Interface for the fit region. The fit region can be calculated from the included peaks, and the settings.
	void											UpdateFitRegion();
	Double_t										GetFitRegionTOFMin() const {return fFitRegionTOFMin;}
	Double_t										GetFitRegionTOFMax() const {return fFitRegionTOFMax;}
	Double_t										GetFitRegionTPCMin() const {return fFitRegionTPCMin;}
	Double_t										GetFitRegionTPCMax() const {return fFitRegionTPCMax;}

	void    SetFitRegion(AliTOFTPCFitFunctionDiHadronPID const *other);  // Set fit region equal to other function; used for deltaChi2 and deltaLL calculations
	// Interface for the mismatch parameters.
	void											SetMismatchYield(Double_t mismatchYield) {fMismatchYield = mismatchYield;}
	void											SetMismatchYieldError(Double_t mismatchYieldError) {fMismatchYieldError = mismatchYieldError;}
	void											SetMismatchStepSize(Double_t mismatchStepSize) {fMismatchYieldStep = mismatchStepSize;}
	void											SetMismatchIsFixed(Bool_t isFixed = kTRUE) {fMismatchYieldIsFixed = isFixed;}

	// It would be nice if we could somehow make a mapping from *pars to the data members of the peaks. That way
	// we could avoid adding an unnecessary amount of fixed parameters.
	void											SetPars(const double* pars); // Fast but unsafe.
	void											SetParErrors(const double* errors); // Id.

	void											SetFitStatus(Int_t fitStatus) {fFitStatus = fitStatus;}

	// Debugging.
	void											Print(Option_t*) const;

	static Int_t									GetNParameters() {return (1 + 6 * AliTOFTPCPeakDiHadronPID::GetNParameters());}
	static Bool_t									IndexExists(Int_t index) {return ((index > -1 && index < GetNParameters()) ? kTRUE : kFALSE);}

	// Mapping between the "index" parameter and the enum's for species and parameter.
	static AliSettingsDiHadronPID::Species			GetSpeciesFromIndex(Int_t index);
	static AliTOFTPCPeakDiHadronPID::Parameter 		GetParameterFromIndex(Int_t index);
	static Int_t									GetIndexFromSpeciesAndParameter(AliSettingsDiHadronPID::Species species, AliTOFTPCPeakDiHadronPID::Parameter parameter);

private:
	AliSettingsDiHadronPID* 						fSettings; //!

	AliSettingsDiHadronPID::Species					fMassAssumption;
	Double_t										fMeanPt;
	Double_t										fAbsMeanEta;
	Double_t										fTOFbinWidth;		// Binwidths are required so that the actual yields can be fitted.
	Double_t										fTPCbinWidth;

	TObjArray*										fPeaks;				// Array holding AliTOFTPCPeakDiHadronPID objects.

	Double_t										fMismatchYield;
	Double_t										fMismatchYieldError;
	Double_t										fMismatchYieldStep;
	Bool_t											fMismatchYieldIsFixed;

	// Fit region -> Should be defined here, and not in the container, since the container may hold
	// different fit stages, each with a different number of peaks included. In that case the fit region
	// is also different per stage.
	Double_t										fFitRegionTOFMin;
	Double_t										fFitRegionTOFMax;
	Double_t										fFitRegionTPCMin;
	Double_t										fFitRegionTPCMax;

	Int_t											fFitStatus;

	ClassDef(AliTOFTPCFitFunctionDiHadronPID, 4);

};

#endif
