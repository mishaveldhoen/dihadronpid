/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Class expects identified 2-particle correlations, and makes peak and
//  bulk ratios and various comparison graphs.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "TCanvas.h"
#include "TLatex.h"
#include "AliRatiosDiHadronPID.h"
#include "AliMiscToolsDiHadronPID.h"
#include "AliHistToolsDiHadronPID.h"
#include "AliFunctionsDiHadronPID.h"
#include "AliLogDiHadronPID.h"

#include "AliGraphDiHadronPID.h"
#include "AliGraphCollectionDiHadronPID.h"
#include "AliFigureDiHadronPID.h"

ClassImp(AliRatiosDiHadronPID);

const Int_t AliRatiosDiHadronPID::fNRegions = 4;
const TString AliRatiosDiHadronPID::fRegionNames[4] = {"All", "Peak 4x4", "Ridge", "Unknown"};

// -----------------------------------------------------------------------
AliRatiosDiHadronPID::AliRatiosDiHadronPID():
	TNamed(),
	fSettings(AliSettingsDiHadronPID::Instance()),
	fPtAxis(0x0),
	fPtAxisRebinned(0x0),
	fNTriggers(-1.),
	fPlotDir("."),
	fNTrialsForSystematics(1000),
	fIdentifiedCorrelationsPion(0x0),
	fIdentifiedCorrelationsKaon(0x0),
	fIdentifiedCorrelationsProton(0x0),
	fIdentifiedMixedEventsPion(0x0),
	fIdentifiedMixedEventsKaon(0x0),
	fIdentifiedMixedEventsProton(0x0),
	fJetPrToPi(0x0),
	fJetKaToPi(0x0),
	fBulkPrToPi(0x0),
	fBulkKaToPi(0x0),
	fJetPrToPiSyst(0x0),
	fJetKaToPiSyst(0x0),
	fBulkPrToPiSyst(0x0),
	fBulkKaToPiSyst(0x0)

{
	
	// Default Constructor
	AliLogDiHadronPID::Debug2("Default constructor called.", __func__, ClassName());

	if (!fSettings) {
		AliLogDiHadronPID::Error(Form("Could not get an instance of the settings."), __func__, ClassName());
		return;
	}

	// Get a copy of the pt Axis is is known in the settings.
	fPtAxis = new TAxis(*(fSettings->GetPtAxis()));

	// Create histogram arrays 
	fIdentifiedCorrelationsPion = new TObjArray(fPtAxis->GetNbins()); fIdentifiedCorrelationsPion->SetOwner(kTRUE);
	fIdentifiedCorrelationsKaon = new TObjArray(fPtAxis->GetNbins()); fIdentifiedCorrelationsKaon->SetOwner(kTRUE);
	fIdentifiedCorrelationsProton = new TObjArray(fPtAxis->GetNbins()); fIdentifiedCorrelationsProton->SetOwner(kTRUE);

	fIdentifiedMixedEventsPion = new TObjArray(fPtAxis->GetNbins()); fIdentifiedMixedEventsPion->SetOwner(kTRUE);
	fIdentifiedMixedEventsKaon = new TObjArray(fPtAxis->GetNbins()); fIdentifiedMixedEventsKaon->SetOwner(kTRUE);
	fIdentifiedMixedEventsProton = new TObjArray(fPtAxis->GetNbins()); fIdentifiedMixedEventsProton->SetOwner(kTRUE);

	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		fJetYield[iSpecies] = 0x0;
		fJetWidthDEta[iSpecies] = 0x0;
		fJetWidthDPhi[iSpecies] = 0x0;
		fBulkYield[iSpecies] = 0x0;

		fJetYieldSyst[iSpecies] = 0x0;
		fBulkYieldSyst[iSpecies] = 0x0;

		fEfficiency[iSpecies] = 0x0;
		fPrimaryFraction[iSpecies] = 0x0;

	}

}

// -----------------------------------------------------------------------
AliRatiosDiHadronPID::AliRatiosDiHadronPID(const char* name, const char* title):
	TNamed(name, title),
	fSettings(AliSettingsDiHadronPID::Instance()),
	fPtAxis(0x0),
	fPtAxisRebinned(0x0),	
	fNTriggers(-1.),	
	fPlotDir("."),	
	fNTrialsForSystematics(1000),	
	fIdentifiedCorrelationsPion(0x0),
	fIdentifiedCorrelationsKaon(0x0),
	fIdentifiedCorrelationsProton(0x0),
	fIdentifiedMixedEventsPion(0x0),
	fIdentifiedMixedEventsKaon(0x0),
	fIdentifiedMixedEventsProton(0x0),
	fJetPrToPi(0x0),
	fJetKaToPi(0x0),
	fBulkPrToPi(0x0),
	fBulkKaToPi(0x0),
	fJetPrToPiSyst(0x0),
	fJetKaToPiSyst(0x0),
	fBulkPrToPiSyst(0x0),
	fBulkKaToPiSyst(0x0)

{
	
	// Default Constructor
	AliLogDiHadronPID::Debug2("Default constructor called.", __func__, ClassName());

	if (!fSettings) {
		AliLogDiHadronPID::Error(Form("Could not get an instance of the settings."), __func__, ClassName());
		return;
	}

	// Get a copy of the pt Axis is is known in the settings.
	fPtAxis = new TAxis(*(fSettings->GetPtAxis()));

	// Create histogram arrays 
	fIdentifiedCorrelationsPion = new TObjArray(fPtAxis->GetNbins()); fIdentifiedCorrelationsPion->SetOwner(kTRUE);
	fIdentifiedCorrelationsKaon = new TObjArray(fPtAxis->GetNbins()); fIdentifiedCorrelationsKaon->SetOwner(kTRUE);
	fIdentifiedCorrelationsProton = new TObjArray(fPtAxis->GetNbins()); fIdentifiedCorrelationsProton->SetOwner(kTRUE);

	fIdentifiedMixedEventsPion = new TObjArray(fPtAxis->GetNbins()); fIdentifiedMixedEventsPion->SetOwner(kTRUE);
	fIdentifiedMixedEventsKaon = new TObjArray(fPtAxis->GetNbins()); fIdentifiedMixedEventsKaon->SetOwner(kTRUE);
	fIdentifiedMixedEventsProton = new TObjArray(fPtAxis->GetNbins()); fIdentifiedMixedEventsProton->SetOwner(kTRUE);

	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		fJetYield[iSpecies] = 0x0;
		fJetWidthDEta[iSpecies] = 0x0;
		fJetWidthDPhi[iSpecies] = 0x0;
		fBulkYield[iSpecies] = 0x0;

		fJetYieldSyst[iSpecies] = 0x0;
		fBulkYieldSyst[iSpecies] = 0x0;

		fEfficiency[iSpecies] = 0x0;
		fPrimaryFraction[iSpecies] = 0x0;

	}

}

// -----------------------------------------------------------------------
AliRatiosDiHadronPID::AliRatiosDiHadronPID(const AliRatiosDiHadronPID& other):
	TNamed(other),
	fSettings(AliSettingsDiHadronPID::Instance()),
	fNTriggers(other.fNTriggers),	
	fPlotDir(other.fPlotDir),
	fNTrialsForSystematics(other.fNTrialsForSystematics)

{

	// Copy constructor
	AliLogDiHadronPID::Debug2(Form("Copy Constructor Called."), __func__, ClassName());
	
	// Copy the pT axis.
	fPtAxis = (other.fPtAxis ? new TAxis(*(other.fPtAxis)) : 0x0);
	fPtAxisRebinned = (other.fPtAxisRebinned ? new TAxis(*(other.fPtAxisRebinned)) : 0x0);

	// Create deep copies of the data.
	fIdentifiedCorrelationsPion = (TObjArray*)(other.fIdentifiedCorrelationsPion)->Clone();
	fIdentifiedCorrelationsKaon = (TObjArray*)(other.fIdentifiedCorrelationsKaon)->Clone();
	fIdentifiedCorrelationsProton = (TObjArray*)(other.fIdentifiedCorrelationsProton)->Clone();

	fIdentifiedMixedEventsPion = (TObjArray*)(other.fIdentifiedMixedEventsPion)->Clone();
	fIdentifiedMixedEventsKaon = (TObjArray*)(other.fIdentifiedMixedEventsKaon)->Clone();
	fIdentifiedMixedEventsProton = (TObjArray*)(other.fIdentifiedMixedEventsProton)->Clone();

	// Create a copy of the jet and bulk yields.
	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {

		if (other.fJetYield[iSpecies]) {
			fJetYield[iSpecies] = (TH1F*)(other.fJetYield[iSpecies])->Clone();
			fJetYield[iSpecies]->SetDirectory(0);
		} else {fJetYield[iSpecies] = 0x0;}

		if (other.fJetWidthDEta[iSpecies]) {
			fJetWidthDEta[iSpecies] = (TH1F*)(other.fJetWidthDEta[iSpecies])->Clone();
			fJetWidthDEta[iSpecies]->SetDirectory(0);
		} else {fJetWidthDEta[iSpecies] = 0x0;}

		if (other.fJetWidthDPhi[iSpecies]) {
			fJetWidthDPhi[iSpecies] = (TH1F*)(other.fJetWidthDPhi[iSpecies])->Clone();
			fJetWidthDPhi[iSpecies]->SetDirectory(0);
		} else {fJetWidthDPhi[iSpecies] = 0x0;}

		if (other.fBulkYield[iSpecies]) {
			fBulkYield[iSpecies] = (TH1F*)(other.fBulkYield[iSpecies])->Clone();
			fBulkYield[iSpecies]->SetDirectory(0);
		} else {fBulkYield[iSpecies] = 0x0;}

		if (other.fJetYieldSyst[iSpecies]) {
			fJetYieldSyst[iSpecies] = (TGraphErrors*)(other.fJetYieldSyst[iSpecies])->Clone();
		} else {fJetYieldSyst[iSpecies] = 0x0;}

		if (other.fBulkYieldSyst[iSpecies]) {
			fBulkYieldSyst[iSpecies] = (TGraphErrors*)(other.fBulkYieldSyst[iSpecies])->Clone();
		} else {fBulkYieldSyst[iSpecies] = 0x0;}

		// Only copy the ptr!
		// cout << __LINE__ << endl;
		fEfficiency[iSpecies] = other.fEfficiency[iSpecies];
		fPrimaryFraction[iSpecies] = other.fPrimaryFraction[iSpecies];
		// cout << __LINE__ << endl;
	}	

	// fPionEfficiency = (other.fPionEfficiency ? (TH1F*)(other.fPionEfficiency)->Clone() : 0x0);
	// fKaonEfficiency = (other.fKaonEfficiency ? (TH1F*)(other.fKaonEfficiency)->Clone() : 0x0);
	// fProtonEfficiency = (other.fProtonEfficiency ? (TH1F*)(other.fProtonEfficiency)->Clone() : 0x0);

	fJetPrToPi = (other.fJetPrToPi ? (TH1F*)(other.fJetPrToPi)->Clone() : 0x0);
	fJetKaToPi = (other.fJetKaToPi ? (TH1F*)(other.fJetKaToPi)->Clone() : 0x0);
	fBulkPrToPi = (other.fBulkPrToPi ? (TH1F*)(other.fBulkPrToPi)->Clone() : 0x0);
	fBulkKaToPi = (other.fBulkKaToPi ? (TH1F*)(other.fBulkKaToPi)->Clone() : 0x0);

	fJetPrToPiSyst = (other.fJetPrToPiSyst ? (TGraphErrors*)(other.fJetPrToPiSyst)->Clone() : 0x0);
	fJetKaToPiSyst = (other.fJetKaToPiSyst ? (TGraphErrors*)(other.fJetKaToPiSyst)->Clone() : 0x0);
	fBulkPrToPiSyst = (other.fBulkPrToPiSyst ? (TGraphErrors*)(other.fBulkPrToPiSyst)->Clone() : 0x0);
	fBulkKaToPiSyst = (other.fBulkKaToPiSyst ? (TGraphErrors*)(other.fBulkKaToPiSyst)->Clone() : 0x0);

}

// -----------------------------------------------------------------------
AliRatiosDiHadronPID::~AliRatiosDiHadronPID() {
	
	// Destructor
	AliLogDiHadronPID::Debug2("Destructor called.", __func__, ClassName());

	if (fPtAxis) {delete fPtAxis; fPtAxis = 0x0;}
	if (fPtAxisRebinned) {delete fPtAxisRebinned; fPtAxisRebinned = 0x0;}

	if (fIdentifiedCorrelationsPion) {delete fIdentifiedCorrelationsPion; fIdentifiedCorrelationsPion = 0x0;}
	if (fIdentifiedCorrelationsKaon) {delete fIdentifiedCorrelationsKaon; fIdentifiedCorrelationsKaon = 0x0;}
	if (fIdentifiedCorrelationsProton) {delete fIdentifiedCorrelationsProton; fIdentifiedCorrelationsProton = 0x0;}

	if (fIdentifiedMixedEventsPion) {delete fIdentifiedMixedEventsPion; fIdentifiedMixedEventsPion = 0x0;}
	if (fIdentifiedMixedEventsKaon) {delete fIdentifiedMixedEventsKaon; fIdentifiedMixedEventsKaon = 0x0;}
	if (fIdentifiedMixedEventsProton) {delete fIdentifiedMixedEventsProton; fIdentifiedMixedEventsProton = 0x0;}

	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		
		if (fJetYield[iSpecies]) {delete fJetYield[iSpecies]; fJetYield[iSpecies] = 0x0;}
		if (fJetWidthDEta[iSpecies]) {delete fJetWidthDEta[iSpecies]; fJetWidthDEta[iSpecies] = 0x0;}
		if (fJetWidthDPhi[iSpecies]) {delete fJetWidthDPhi[iSpecies]; fJetWidthDPhi[iSpecies] = 0x0;}
		if (fBulkYield[iSpecies]) {delete fBulkYield[iSpecies]; fBulkYield[iSpecies] = 0x0;}

		if (fJetYieldSyst[iSpecies]) {delete fJetYieldSyst[iSpecies]; fJetYieldSyst[iSpecies] = 0x0;}
		if (fBulkYieldSyst[iSpecies]) {delete fBulkYieldSyst[iSpecies]; fBulkYieldSyst[iSpecies] = 0x0;}
	
	}

	if (fJetPrToPi) {delete fJetPrToPi; fJetPrToPi = 0x0;}
	if (fJetKaToPi) {delete fJetKaToPi; fJetKaToPi = 0x0;}

	if (fBulkPrToPi) {delete fBulkPrToPi; fBulkPrToPi = 0x0;}
	if (fBulkKaToPi) {delete fBulkKaToPi; fBulkKaToPi = 0x0;}

	// if (fPionEfficiency) {delete fPionEfficiency; fPionEfficiency = 0x0;}
	// if (fKaonEfficiency) {delete fKaonEfficiency; fKaonEfficiency = 0x0;}
	// if (fProtonEfficiency) {delete fProtonEfficiency; fProtonEfficiency = 0x0;}	

	if (fJetPrToPiSyst) {delete fJetPrToPiSyst; fJetPrToPiSyst = 0x0;}
	if (fJetKaToPiSyst) {delete fJetKaToPiSyst; fJetKaToPiSyst = 0x0;}

	if (fBulkPrToPiSyst) {delete fBulkPrToPiSyst; fBulkPrToPiSyst = 0x0;}
	if (fBulkKaToPiSyst) {delete fBulkKaToPiSyst; fBulkKaToPiSyst = 0x0;}

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::ImportCorrelation(AliSettingsDiHadronPID::Species massAssumption, Int_t ptBin, const TH2F* correlation) {

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	// Check the correlation ptr.
	if (!correlation) {
		AliLogDiHadronPID::Error(Form("Correlation ptr is 0x0."), __func__, ClassName());
		return;
	}

	// Get a ptr to the array corresponding to the indicated mass assumption.
	TObjArray* arrayTmp = GetCorrelationArrayForMassAssumption(massAssumption);
	if (!arrayTmp) {return;}

	// Clone the supplied histogram, and add to the array.
	TH2F* corrTmp = (TH2F*)correlation->Clone();

	arrayTmp->AddAt(corrTmp, ptBin - 1);
}

// -----------------------------------------------------------------------
TH2F* AliRatiosDiHadronPID::GetCorrelationClone(AliSettingsDiHadronPID::Species massAssumption, Int_t ptBin) const {

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	TH2F* hTmp = GetCorrelation(massAssumption, ptBin);
	if (!hTmp) {return 0x0;}

	TH2F* hout = (TH2F*)(hTmp->Clone());
	hout->SetDirectory(0);

	return hout;

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::ImportMixedEvent(AliSettingsDiHadronPID::Species massAssumption, Int_t ptBin, const TH2F* mixedEvent) {

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	// Check the mixedEvent ptr.
	if (!mixedEvent) {
		AliLogDiHadronPID::Error(Form("mixedEvent ptr is 0x0."), __func__, ClassName());
		return;
	}

	// Get a ptr to the array corresponding to the indicated mass assumption.
	TObjArray* arrayTmp = GetMixedEventsArrayForMassAssumption(massAssumption);
	if (!arrayTmp) {return;}

	// Clone the supplied histogram, and add to the array.
	TH2F* mixedTmp = (TH2F*)mixedEvent->Clone();

	// Scale the mixed events if needed.
	AliHistToolsDiHadronPID::NormaliseMixedEvents(mixedTmp, kTRUE);

	arrayTmp->AddAt(mixedTmp, ptBin - 1);
}

// -----------------------------------------------------------------------
TH2F* AliRatiosDiHadronPID::GetMixedEventClone(AliSettingsDiHadronPID::Species massAssumption, Int_t ptBin) const {

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	TH2F* hTmp = GetMixedEvents(massAssumption, ptBin);
	if (!hTmp) {return 0x0;}

	TH2F* hout = (TH2F*)(hTmp->Clone());
	hout->SetDirectory(0);

	return hout;

}

// -----------------------------------------------------------------------
TH2F* AliRatiosDiHadronPID::GetCorrectedClone(AliSettingsDiHadronPID::Species massAssumption, Int_t ptBinMin, Int_t ptBinMax) const {

	// This methods corrects the signal with the mixed events, and returns a ptr
	// to the result. User owns the result.
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	TObjArray* arrayDataTmp = GetCorrelationArrayForMassAssumption(massAssumption);
	if (!arrayDataTmp) {return 0x0;} 
	TH2F* histDataTmp = (TH2F*)arrayDataTmp->At(ptBinMin - 1);
	if (!histDataTmp) {return 0x0;}

	TObjArray* arrayMETmp = GetMixedEventsArrayForMassAssumption(massAssumption);
	if (!arrayMETmp) {return 0x0;}
	TH2F* histMETmp = (TH2F*)arrayMETmp->At(ptBinMin - 1);
	if (!histMETmp) {return 0x0;}

	// Create a corrected result.
	AliLogDiHadronPID::Info(Form("Getting data for pT bin %i...", ptBinMin), __func__, ClassName());
	
	TH2F* hOut = (TH2F*)histDataTmp->Clone();
	hOut->SetDirectory(0);
	hOut->Divide(histMETmp);

	// Divide through efficiency - Adding errors in quadrature... perhaps not the best...
	if (fEfficiency[massAssumption]) {
		AliLogDiHadronPID::Info(Form("Doing efficiency correction for pT bin %i...", ptBinMin), __func__, ClassName());

		for (Int_t iBinX = 1; iBinX <= hOut->GetNbinsX(); iBinX++) {
			for (Int_t iBinY = 1; iBinY <= hOut->GetNbinsY(); iBinY++) {
				Double_t newVal = hOut->GetBinContent(iBinX, iBinY) / (fEfficiency[massAssumption]->GetBinContent(ptBinMin));
				Double_t newErr = AliFunctionsDiHadronPID::ErrorRatio(hOut->GetBinContent(iBinX, iBinY), hOut->GetBinError(iBinX, iBinY), fEfficiency[massAssumption]->GetBinContent(ptBinMin), fEfficiency[massAssumption]->GetBinError(ptBinMin));
				
				hOut->SetBinContent(iBinX, iBinY, newVal);
				hOut->SetBinError(iBinX, iBinY, newErr);
			}
		}	

	} else {
		AliLogDiHadronPID::Warning(Form("No efficiency histogram available..."), __func__, ClassName());
	}

	if (ptBinMax > ptBinMin) {
		TH2F* nextPtBin = GetCorrectedClone(massAssumption, ptBinMin + 1, ptBinMax);
		hOut->Add(nextPtBin);
		delete nextPtBin;
	}

	return hOut;

}

// -----------------------------------------------------------------------
TH1F* AliRatiosDiHadronPID::GetHistBulkFitWithConst(AliSettingsDiHadronPID::Species species, Int_t pTbin, Double_t BulkMinAbsDEta, TLatex* dEtaRange, Bool_t useSumInsteadOfFit) const {

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());
	
	// Get a clone of the corrected 2D correlation histogram.
	TH2F* correctedDiHadron = GetCorrectedClone(species, pTbin);

	TH1F* hOut = GetHistBulkFitWithConst(correctedDiHadron, BulkMinAbsDEta, dEtaRange, useSumInsteadOfFit);

	delete correctedDiHadron; correctedDiHadron = 0x0;

	return hOut;

}

// -----------------------------------------------------------------------
TH1F* AliRatiosDiHadronPID::GetHistBulkFitWithConst(const TH2F* hist, Double_t BulkMinAbsDEta, TLatex* dEtaRange, Bool_t useSumInsteadOfFit) const {

/* #ifdef __CLING__
	const TAxis* aDPhi = hist->GetXaxis();
	const TAxis* aDEta = hist->GetYaxis();
#else
	TAxis* aDPhi = hist->GetXaxis();
	TAxis* aDEta = hist->GetYaxis();
#endif */
	TH1F* hSideBand = (TH1F*)AliHistToolsDiHadronPID::MakeHist("hSideBand", "hSideBand", "F", hist->GetXaxis());
	hSideBand->SetDirectory(0);

	// Determine the bin range in DEta for the side band.
	const Int_t BulkMinBin1 = 2;
	const Int_t BulkMaxBin1 = hist->GetYaxis()->FindBin(-BulkMinAbsDEta + 0.001);
	const Int_t BulkMinBin2 = hist->GetYaxis()->FindBin(BulkMinAbsDEta - 0.001);
	const Int_t BulkMaxBin2 = hist->GetYaxis()->GetNbins() - 1;
	const Int_t nBinsBulk = BulkMaxBin1 - BulkMinBin1 + BulkMaxBin2 - BulkMinBin2 + 2;

	cout << "Bulk Max Bin1: " << BulkMaxBin1 << " ends at: " << hist->GetYaxis()->GetBinUpEdge(BulkMaxBin1) << ", Bulk Min Bin2: " << BulkMinBin2 << " begins at: " << hist->GetYaxis()->GetBinLowEdge(BulkMinBin2) << endl;

	// If the user supplied a ptr to a TLatex object, then put text in it.
	if (dEtaRange) {dEtaRange->SetText(0.1, 0.1, Form("%4.2f < |#Delta#eta_{bulk}| < %4.2f", hist->GetYaxis()->GetBinLowEdge(BulkMinBin2), hist->GetYaxis()->GetBinUpEdge(BulkMaxBin2)));}

	// Loop over the bins in DEta.
	for (Int_t iDPhiBin = 1; iDPhiBin <= hist->GetXaxis()->GetNbins(); iDPhiBin++) {
	
		if (useSumInsteadOfFit) {

			Double_t sum = 0.;
			Double_t errorSquared = 0.;
			Double_t nBins = 0.;

			for (Int_t iBinBulk1 = BulkMinBin1; iBinBulk1 <= BulkMaxBin1; iBinBulk1++) {

				sum += hist->GetBinContent(iDPhiBin, iBinBulk1);
				errorSquared += hist->GetBinError(iDPhiBin, iBinBulk1) * hist->GetBinError(iDPhiBin, iBinBulk1);
				nBins++;

			}

			for (Int_t iBinBulk2 = BulkMinBin2; iBinBulk2 <= BulkMaxBin2; iBinBulk2++) {

				sum += hist->GetBinContent(iDPhiBin, iBinBulk2);
				errorSquared += hist->GetBinError(iDPhiBin, iBinBulk2) * hist->GetBinError(iDPhiBin, iBinBulk2);
				nBins++;

			}

			// cout << "Number of included bins: " << nBins << endl;

			hSideBand->SetBinContent(iDPhiBin, sum / nBins);
			hSideBand->SetBinError(iDPhiBin, TMath::Sqrt(errorSquared) / nBins);

		} else {

			// Determine side band value for this DPhi bin by using a constant fit.
			Double_t values[nBinsBulk];
			Double_t valerrors[nBinsBulk];

			Int_t iArray = 0;
			for (Int_t iBinBulk1 = BulkMinBin1; iBinBulk1 <= BulkMaxBin1; iBinBulk1++) {
				values[iArray] = hist->GetBinContent(iDPhiBin, iBinBulk1);
				valerrors[iArray] = hist->GetBinError(iDPhiBin, iBinBulk1);
				iArray++;									
			}

			for (Int_t iBinBulk2 = BulkMinBin2; iBinBulk2 <= BulkMaxBin2; iBinBulk2++) {
				values[iArray] = hist->GetBinContent(iDPhiBin, iBinBulk2);
				valerrors[iArray] = hist->GetBinError(iDPhiBin, iBinBulk2);
				iArray++;									
			}

			// Perform a chi^2 fit with a constant to the side bands to determine their weighted avg.
			Double_t constfit = 0.;
			Double_t constfiterr = 0.;
			AliFunctionsDiHadronPID::ConstFit(values, valerrors, nBinsBulk, constfit, constfiterr);

			hSideBand->SetBinContent(iDPhiBin, constfit);
			hSideBand->SetBinError(iDPhiBin, constfiterr);

		}

	}

	return hSideBand;

}

// -----------------------------------------------------------------------
TH1F* AliRatiosDiHadronPID::GetProjectionPt(AliSettingsDiHadronPID::Species species, Region region) const {

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	TString nameTitle = AliSettingsDiHadronPID::GetSpeciesName(species);
	nameTitle += " ";
	nameTitle += GetRegionName(region);

	TH1F* hout = (TH1F*)AliHistToolsDiHadronPID::MakeHist(nameTitle.Data(), nameTitle.Data(), "F", fPtAxis);
	hout->SetDirectory(0);

	// Loop over pT bins.
	for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {
		
		Double_t integral = 0.;
		Double_t error = 0.;

		if (GetIntegralAndError(species, region, iPtBin, integral, error)) {

			hout->SetBinContent(iPtBin, integral);
			hout->SetBinError(iPtBin, error);
		}
	}

	return hout;

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::PlotDPhiDEta(AliSettingsDiHadronPID::Species species, Int_t pTbin) const {

	// This method presents a number of derived plots.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	TCanvas::MakeDefCanvas();
	GetCorrelationClone(species, pTbin)->Draw("surf1");

	TCanvas::MakeDefCanvas();
	GetMixedEventClone(species, pTbin)->Draw("surf1");

	TCanvas::MakeDefCanvas();
	GetCorrectedClone(species, pTbin)->Draw("surf1");

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::PlotRegionYieldVsPt(AliSettingsDiHadronPID::Species species, Region region) const {

	// This method presents a number of derived plots.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	TCanvas::MakeDefCanvas();
	GetProjectionPt(species, region)->Draw("e");

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::RegionsMethod(AliSettingsDiHadronPID::Species species, Int_t pTBin, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta, Double_t* parameters, Double_t* errors, Bool_t saveFitToPDF) const {

	// This method takes a DPhi,DEta histogram, and analytically fits the bins 
	// between BulkMinAbsDEta and 1.5.
	//
	//  - The output is a background subtracted plot in DPhi. 
	//  - The outcomes are put in: parameters[0] = jet yield, parameters[1] = bulk yield per unit (DEta x DPhi)
	//
	// Important! The Bulk yield is an average of the side band, for how much it is included. If only near side
	// plots are available, then then the background is only calculated for the near side.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());
	
	// Get a clone of the histogram.
	TH2F* correctedDiHadron = GetCorrectedClone(species, pTBin);
/* #ifdef __CLING__
	const TAxis* aDPhi = correctedDiHadron->GetXaxis();
	const TAxis* aDEta = correctedDiHadron->GetYaxis();
#else
	TAxis* aDPhi = correctedDiHadron->GetXaxis();
	TAxis* aDEta = correctedDiHadron->GetYaxis();
#endif */
	// Determine the bin range in DEta for the side band.
	const Int_t BulkMinBin1 = 2;
	const Int_t BulkMaxBin1 = correctedDiHadron->GetYaxis()->FindBin(-BulkMinAbsDEta - 0.001);
	const Int_t BulkMinBin2 = correctedDiHadron->GetYaxis()->FindBin(BulkMinAbsDEta + 0.001);
	const Int_t BulkMaxBin2 = correctedDiHadron->GetYaxis()->GetNbins() - 1;

	const Int_t JetMinBin = correctedDiHadron->GetYaxis()->FindBin(-PeakMaxDEta + 0.001);
	const Int_t JetMaxBin = correctedDiHadron->GetYaxis()->FindBin(PeakMaxDEta - 0.001);
	const Int_t nBinsJet = JetMaxBin - JetMinBin + 1;

	// Check the ranges for overlap.
	if (BulkMaxBin1 >= JetMinBin || BulkMinBin2 <= JetMaxBin) {AliLogDiHadronPID::Warning(Form("Jet and Bulk region overlap!"), __func__, ClassName());}
	AliLogDiHadronPID::Info(Form("Determined range: B(%i,%i) - J(%i,%i) - B(%i,%i).", BulkMinBin1, BulkMaxBin1, JetMinBin, JetMaxBin, BulkMinBin2, BulkMaxBin2), __func__, ClassName());

	// Create output histograms.
	TH1F* hJet = (TH1F*)correctedDiHadron->ProjectionX(Form("jet_%s_ptBin%i", fSettings->GetSpeciesName(species), pTBin), JetMinBin, JetMaxBin);
	hJet->SetTitle(Form("%s Jet (pt bin: %i)", fSettings->GetSpeciesName(species), pTBin));
	hJet->SetDirectory(0);

	//TLatex* dEtaRange = new TLatex();
	TH1F* hSideBand = GetHistBulkFitWithConst(species, pTBin, BulkMinAbsDEta, 0x0);
	hSideBand->Scale(nBinsJet);

	TLegend* legend = 0x0;

	// These parameters indicate the average value on the away side after the side band subtraction.
	// In PbPb, it seems this is systematically shifted up somewhat, which means that jet yields are probably underestimated.
	Double_t sideBandShift = 0.;
	Double_t sideBandShiftErr = 0.;

	// Draw peak and bulk.
	if (saveFitToPDF) {

		TCanvas* cvs = TCanvas::MakeDefCanvas();
		
		legend = new TLegend(0.6,0.68,0.88,0.88);
		legend->SetBorderSize(0);
		legend->SetFillColor(0);

		hJet->SetTitle("");
		hJet->SetLineColor(kBlue);
		hJet->SetMarkerStyle(21);
		hJet->SetMarkerColor(kBlue);
		hJet->Draw();

		hSideBand->SetMarkerStyle(22);
		hSideBand->SetMarkerColor(kRed);
		hSideBand->SetLineColor(kRed);
		hSideBand->Draw("same");

		legend->AddEntry(hJet, Form("|#Delta#eta| < %4.2f", correctedDiHadron->GetYaxis()->GetBinUpEdge(JetMaxBin)));
		legend->AddEntry(hSideBand, Form("|#Delta#eta| > %4.2f", correctedDiHadron->GetYaxis()->GetBinLowEdge(BulkMinBin2)));
		legend->Draw();

		AliMiscToolsDiHadronPID::CreateDir(Form("./%s/Sp%i", fPlotDir.Data(), species));			
		cvs->SaveAs(Form("./%s/Sp%i/PtBinJetAndSB%02i.pdf", fPlotDir.Data(), species, pTBin));

		TH1F* hJetSidebandSubtracted = (TH1F*)hJet->Clone("hJetSidebandSubtracted");
		hJetSidebandSubtracted->SetDirectory(0);
		hJetSidebandSubtracted->Add(hSideBand, -1.);
		hJetSidebandSubtracted->Draw();

		TF1* fConst = new TF1("fConst", "[0]", TMath::Pi()/2., 3.*TMath::Pi()/2.);
		fConst->SetLineColor(kBlack);
		fConst->SetLineStyle(0);

		hJetSidebandSubtracted->Fit(fConst, "RQI0");
		sideBandShift = fConst->GetParameter(0);
		sideBandShiftErr = fConst->GetParError(0);

		TGraphErrors* constFitBand = new TGraphErrors(2);
		constFitBand->SetPoint(0, -TMath::Pi()/2., sideBandShift);
		constFitBand->SetPointError(0, 0, sideBandShiftErr);
		constFitBand->SetPoint(1, 3.*TMath::Pi()/2., sideBandShift);
		constFitBand->SetPointError(1, 0, sideBandShiftErr);

		constFitBand->SetFillColor(kRed);
		constFitBand->SetFillStyle(3005);
		constFitBand->Draw("3");

		// fConst->SetRange(-TMath::Pi()/2., 3.*TMath::Pi()/2.);
		fConst->Draw("same");

		cvs->SaveAs(Form("./%s/Sp%i/PtBinJetSBSub%02i.pdf", fPlotDir.Data(), species, pTBin));

		delete fConst;
		delete constFitBand;
		delete cvs;

	}

	// Determine the jet yield and bulk yield.
	Double_t jetYield = 0.;
	Double_t jetErr2 = 0.;
	Double_t bulkYield = 0.;
	Double_t bulkErr2 = 0.;
	for (Int_t iDPhiBin = correctedDiHadron->GetXaxis()->FindBin(-PeakMaxDPhi); iDPhiBin <= correctedDiHadron->GetXaxis()->FindBin(PeakMaxDPhi); iDPhiBin++) {
		cout << jetYield << endl;

		// Take care of a possible "wing" by considering the side band shift. (seems to be only relevant in PbPb).
		jetYield += (hJet->GetBinContent(iDPhiBin) - (hSideBand->GetBinContent(iDPhiBin) - sideBandShiftErr));
		jetErr2 += (hJet->GetBinError(iDPhiBin) * hJet->GetBinError(iDPhiBin));
		jetErr2 += (hSideBand->GetBinError(iDPhiBin) * hSideBand->GetBinError(iDPhiBin));

	}	

	parameters[0] = jetYield;
	errors[0] = TMath::Sqrt(jetErr2);

	// For pp we take a constant fit to the near side (assuming no flow)
	// FIXME: Significant code duplication in the DPhi fit methods.
	if (fSettings->GetBeamType() == 2) {
		AliLogDiHadronPID::Info(Form("Assuming flat background (chi^2 fit)."), __func__, ClassName());
		
		Int_t BulkMinBinDPhi = correctedDiHadron->GetXaxis()->FindBin(-TMath::Pi()/2. + 0.001);
		Int_t BulkMaxBinDPhi = correctedDiHadron->GetXaxis()->FindBin(TMath::Pi()/2. - 0.001);
		Int_t nBinsDPhi = BulkMaxBinDPhi - BulkMinBinDPhi + 1;

		Double_t values[nBinsDPhi];
		Double_t valerrors[nBinsDPhi];
		for (Int_t iDPhiBin = BulkMinBinDPhi; iDPhiBin <= BulkMaxBinDPhi; iDPhiBin++) { 
			values[iDPhiBin - BulkMinBinDPhi] = hSideBand->GetBinContent(iDPhiBin);
			valerrors[iDPhiBin - BulkMinBinDPhi] = hSideBand->GetBinError(iDPhiBin);
		}

		AliFunctionsDiHadronPID::ConstFit(values, valerrors, nBinsDPhi, parameters[1], errors[1]);

	} else {

		Double_t nBinsInLoop = 0.;
		for (Int_t iDPhiBin = 1; iDPhiBin <= correctedDiHadron->GetXaxis()->GetNbins(); iDPhiBin++) {
			bulkYield += hSideBand->GetBinContent(iDPhiBin);
			bulkErr2 += (hSideBand->GetBinError(iDPhiBin) * hSideBand->GetBinError(iDPhiBin));
			nBinsInLoop++;
		}

		parameters[1] = bulkYield;
		errors[1] = TMath::Sqrt(bulkErr2);

		// Calculate the average yield per bin.
		parameters[1] /= (nBinsInLoop * (Double_t)nBinsJet);
		errors[1] /= (nBinsInLoop * (Double_t)nBinsJet);

	}

	// For the background we return the yield per unit DEta,DPhi
	parameters[1] /= (correctedDiHadron->GetXaxis()->GetBinWidth(1) * correctedDiHadron->GetYaxis()->GetBinWidth(1));
	errors[1] /= (correctedDiHadron->GetXaxis()->GetBinWidth(1) * correctedDiHadron->GetYaxis()->GetBinWidth(1));

	delete hSideBand;
	delete hJet;
	delete correctedDiHadron;

}

// -----------------------------------------------------------------------
TH1F** AliRatiosDiHadronPID::AnalyseDPhiDEta(AliSettingsDiHadronPID::BeamType beamType, AliSettingsDiHadronPID::Species species, 
	DPhiBackgroundModel model, Int_t pTBinMin, Int_t pTBinMax, Double_t PeakRadiusDPhi, Double_t PeakRadiusDEta,
	Double_t* parameters, Double_t* errors, Double_t* measurementOfChoice, Double_t* measurementOfChoiceError, 
	Double_t* peakWidthSG, Double_t* peakWidthSGError, Bool_t useWingCorrection, Bool_t saveFitToPDF) const {

	// This method is supposed to replace all specialised DPhi fit methods. It uses a very general fit function,
	// which allows to fit the NS peak with up to two gaussians, the away side peak with a single gaussian, and
	// four fourier coefficients.
	//
	// The results of the analysis will be stored in a 2D histogram (parameters v.s. pT), in "newPtBin".
	// See for the details about what parameters were used: MeasureJetAndBulkYield()

	const Double_t DEtaSignalMax = 1.5;

	const Double_t DEtaBulkMin = PeakRadiusDEta;
	const Double_t DEtaBulkMax = DEtaSignalMax; 

	// Create a canvas.
	TCanvas* cvs = 0x0;
	TLegend* legend = 0x0;
	if (saveFitToPDF) {
		cvs = TCanvas::MakeDefCanvas();
		AliMiscToolsDiHadronPID::CreateDir(Form("./%s/Sp%i", fPlotDir.Data(), species));
		legend = new TLegend(0.6,0.6,0.88,0.88);
		legend->SetBorderSize(0);
		legend->SetFillColor(0);
	}

	// Get a clone of the histogram.
	TH2F* correctedDiHadron = GetCorrectedClone(species, pTBinMin, pTBinMax);


	TAxis* aDEta = correctedDiHadron->GetYaxis();
	aDEta->SetRange(2, aDEta->GetNbins() - 1); // Cut away the first and last bin in DEta.
/* #ifdef __CLING__
	const TAxis* aDPhi = correctedDiHadron->GetXaxis();
#else
	TAxis* aDPhi = correctedDiHadron->GetXaxis();
#endif */
	// Save (DPhi, DEta) plot.
	if (cvs) {
		cvs->cd();

		correctedDiHadron->Draw("surf1");

		cvs->SaveAs(Form("./%s/Sp%i/DPhiDEtaPtBin%02i.pdf", fPlotDir.Data(), species, pTBinMin));
		cvs->Clear();

	}

	// Histogram collecting the different jet yield measurements.
	TH1F* hJetYieldMeasurements = new TH1F(Form("hJetYieldMeasurementsPtBin%i", pTBinMin), Form("hJetYieldMeasurementsPtBin%i", pTBinMin), 7, 0, 1);
	hJetYieldMeasurements->SetDirectory(0);
	hJetYieldMeasurements->SetMarkerStyle(19);
	TAxis* aJetYieldMeasurements = hJetYieldMeasurements->GetXaxis();

	// Histogram collecting the different bulk measurements.
	TH1F* hBulkYieldMeasurements = new TH1F(Form("hBulkYieldMeasurementsPtBin%i", pTBinMin), Form("hBulkYieldMeasurementsPtBin%i", pTBinMin), 4, 0, 1);
	hBulkYieldMeasurements->SetDirectory(0);
	hBulkYieldMeasurements->SetMarkerStyle(19);
	TAxis* aBulkYieldMeasurements = hBulkYieldMeasurements->GetXaxis();	

	// ***************************
	// * CREATE DETA PROJECTIONS *
	// ***************************

	// Determine bin ranges for NS and AS regions.
	Int_t minBinDPhiNS = 1;
	Int_t maxBinDPhiNS = correctedDiHadron->GetXaxis()->FindBin(TMath::Pi() / 2. - 0.001);
	// Double_t RangeDPhiNS = correctedDiHadron->GetXaxis()->GetBinUpEdge(maxBinDPhiNS) - correctedDiHadron->GetXaxis()->GetBinLowEdge(minBinDPhiNS);

	Int_t minBinDPhiAS = maxBinDPhiNS + 1;
	Int_t maxBinDPhiAS = correctedDiHadron->GetXaxis()->GetNbins();
	// Double_t RangeDPhiAS = correctedDiHadron->GetXaxis()->GetBinUpEdge(maxBinDPhiAS) - correctedDiHadron->GetXaxis()->GetBinLowEdge(minBinDPhiAS);

	TH1F* hDEtaProjNSDPhi = (TH1F*)correctedDiHadron->ProjectionY(Form("dEtaProjNS_%s_ptBin%i", fSettings->GetSpeciesName(species), pTBinMin), minBinDPhiNS, maxBinDPhiNS);
	hDEtaProjNSDPhi->GetXaxis()->SetTitle("#Delta#eta");
	hDEtaProjNSDPhi->Scale(1., "width");
	hDEtaProjNSDPhi->GetYaxis()->SetTitle(Form("#frac{dN_{%s}}{d#Delta#eta}", fSettings->GetSpeciesNameLaTeX(species)));
	hDEtaProjNSDPhi->SetLineColor(kBlack);
	hDEtaProjNSDPhi->SetMarkerStyle(20);
	hDEtaProjNSDPhi->SetMarkerColor(kBlack);	

	TH1F* hDEtaProjASDPhi = (TH1F*)correctedDiHadron->ProjectionY(Form("dEtaProjAS_%s_ptBin%i", fSettings->GetSpeciesName(species), pTBinMin), minBinDPhiAS, maxBinDPhiAS);
	hDEtaProjASDPhi->GetXaxis()->SetTitle("#Delta#eta");	
	hDEtaProjASDPhi->Scale(1., "width");
	hDEtaProjASDPhi->GetYaxis()->SetTitle(Form("#frac{dN_{%s}}{d#Deltaeta}", fSettings->GetSpeciesNameLaTeX(species)));
	hDEtaProjASDPhi->SetLineColor(kRed);
	hDEtaProjASDPhi->SetMarkerStyle(21);
	hDEtaProjASDPhi->SetMarkerColor(kRed);

	// Make a plot of the DEta projections together.
	if (cvs) {

		cvs->cd();
		hDEtaProjNSDPhi->Draw(); 

		hDEtaProjASDPhi->Draw("same"); 

		legend->AddEntry(hDEtaProjNSDPhi, Form("%4.2f#pi < #Delta#phi < %4.2f#pi", correctedDiHadron->GetXaxis()->GetBinLowEdge(minBinDPhiNS)/TMath::Pi(), correctedDiHadron->GetXaxis()->GetBinUpEdge(maxBinDPhiNS)/TMath::Pi()));
		legend->AddEntry(hDEtaProjASDPhi, Form("%4.2f#pi < #Delta#phi < %4.2f#pi", correctedDiHadron->GetXaxis()->GetBinLowEdge(minBinDPhiAS)/TMath::Pi(), correctedDiHadron->GetXaxis()->GetBinUpEdge(maxBinDPhiAS)/TMath::Pi()));
		// legend->AddEntry(hDPhiProjCentralDEtaJetCut, Form("|#Delta#eta| < %4.2f, no Peak", aDEta->GetBinUpEdge(maxBinDEtaBulk)));
		legend->Draw(); 

		cvs->SaveAs(Form("./%s/Sp%i/DEtaProjectionsPtBin%02i.pdf", fPlotDir.Data(), species, pTBinMin));
		cvs->Clear();
		legend->Clear();

	}

	// *************************************
	// * CREATE DETA TEMPLATE FIT FUNCTION *
	// *************************************

	// Create template fit function for a DEta projection. (TODO: Make Standard Available data member)
	TF1* fitFuncDEtaTemplate = new TF1("fitFuncDEtaTemplate", "[0]*(1+[1]*x*x*x*x)+[2]*([3]/(sqrt(2*TMath::Pi())*[5]))*exp(-0.5*(((x-[4])/[5])**2))+(([2]*(1-[3]))/(sqrt(2*TMath::Pi())*[7]))*exp(-0.5*(((x-[6])/[7])**2))", -1.5, 1.5);

	fitFuncDEtaTemplate->SetParName(0, "const");
	fitFuncDEtaTemplate->SetParName(1, "a4");
	fitFuncDEtaTemplate->FixParameter(1, 0.);

	fitFuncDEtaTemplate->SetParName(2, "yieldNS");
	fitFuncDEtaTemplate->SetParName(3, "fraction");
	fitFuncDEtaTemplate->SetParName(4, "muDPhiNS1");
	fitFuncDEtaTemplate->FixParameter(4, 0.);
	fitFuncDEtaTemplate->SetParName(5, "sigmaDPhiNS1");
	fitFuncDEtaTemplate->SetParLimits(5, 0.1, 2.0);

	fitFuncDEtaTemplate->SetParName(6, "muDPhiNS2");
	fitFuncDEtaTemplate->FixParameter(6, 0.);
	fitFuncDEtaTemplate->SetParName(7, "sigmaDPhiNS2");
	fitFuncDEtaTemplate->SetParLimits(7, 0.1, 2.0);

	// ********************
	// * SG PEAK FIT DETA *
	// ********************
	TF1* fitFuncDEtaNSTotalSG = FitDEtaPeak(species, pTBinMin, hDEtaProjNSDPhi, (useWingCorrection ? hDEtaProjASDPhi : 0x0), fitFuncDEtaTemplate, kFALSE, kRed, kFALSE);
	peakWidthSG[1] = fitFuncDEtaNSTotalSG->GetParameter(5);
	peakWidthSGError[1] = fitFuncDEtaNSTotalSG->GetParError(5);

	// aJetYieldMeasurements->SetBinLabel(9, "Fit SG (#Delta#eta)");
	// hJetYieldMeasurements->SetBinContent(9, fitFuncDEtaNSTotalSG->GetParameter(2));
	// hJetYieldMeasurements->SetBinError(9, fitFuncDEtaNSTotalSG->GetParError(2));			

	// Note that this only makes sense in pp!!
	// aBulkYieldMeasurements->SetBinLabel(3, "Fit SG (#Delta#eta)");
	// hBulkYieldMeasurements->SetBinContent(3, fitFuncDEtaNSTotalSG->GetParameter(0) / RangeDPhiNS);
	// hBulkYieldMeasurements->SetBinError(3, fitFuncDEtaNSTotalSG->GetParError(0) / RangeDPhiNS);

	// ********************
	// * DG PEAK FIT DETA *
	// ********************
	TF1* fitFuncDEtaNSTotalDG = FitDEtaPeak(species, pTBinMin, hDEtaProjNSDPhi, (useWingCorrection ? hDEtaProjASDPhi : 0x0), fitFuncDEtaTemplate, kTRUE, kRed, kFALSE);
	
	// aJetYieldMeasurements->SetBinLabel(10, "Fit DG (#Delta#eta)");
	// hJetYieldMeasurements->SetBinContent(10, fitFuncDEtaNSTotalDG->GetParameter(2));
	// hJetYieldMeasurements->SetBinError(10, fitFuncDEtaNSTotalDG->GetParError(2));			

	// Only makes sense in pp!!
	// aBulkYieldMeasurements->SetBinLabel(4, "Fit DG (#Delta#eta)");
	// hBulkYieldMeasurements->SetBinContent(4, fitFuncDEtaNSTotalDG->GetParameter(0) / RangeDPhiNS);
	// hBulkYieldMeasurements->SetBinError(4, fitFuncDEtaNSTotalDG->GetParError(0) / RangeDPhiNS);

	// *************************************************************************
	// * PERFORM WING CORRECTION ON (DPHI,DETA) HISTOGRAM * NOT NEEDED ANYMORE *
	// *************************************************************************

	// if (useWingCorrection) {

	// 	for (Int_t iBinDEta = 1; iBinDEta <= aDEta->GetNbins(); iBinDEta++) {
	
	// 		Double_t xx = aDEta->GetBinCenter(iBinDEta);
	// 		Double_t wingCorrFactor = 1. / (1. + xx * xx * xx * xx * fitFuncDEtaNSTotalSG->GetParameter(1));
	// 		//cout << "Wing correction factor: " << wingCorrFactor << endl;
	// 		for (Int_t iBinDPhi = 1; iBinDPhi <= aDPhi->GetNbins(); iBinDPhi++) {
	// 			correctedDiHadron->SetBinContent(iBinDPhi, iBinDEta, correctedDiHadron->GetBinContent(iBinDPhi, iBinDEta) * wingCorrFactor);
	// 		}
		
	// 	}

	// 	if (cvs) {

	// 		cvs->cd();
	// 		correctedDiHadron->Draw("surf1");

	// 		cvs->SaveAs(Form("./%s/Sp%i/DPhiDEta_WingCorr_PtBin%02i.pdf", fPlotDir.Data(), species, pTBinMin));
	// 		cvs->Clear();

	// 	}

	// }


	// *************************************
	// * CREATE DPHI TEMPLATE FIT FUNCTION *
	// *************************************

	// Create template fit function for a DPhi projection, and fix the position of the jet peaks to 0 and pi (TODO: Make Standard Available data member)
	TF1* fitFuncDPhiTemplate = new TF1("fitFuncDPhiTemplate","[0]*(1+2*[10]*cos(x)+2*[11]*cos(2*x)+2*[12]*cos(3*x)+2*[13]*cos(4*x))+(([1]*[2])/(sqrt(2*TMath::Pi())*[4]))*exp(-0.5*(((x-[3])/[4])**2))+(([1]*[2])/(sqrt(2*TMath::Pi())*[4]))*exp(-0.5*(((x-[3]-2*TMath::Pi())/[4])**2))+(([1]*(1-[2]))/(sqrt(2*TMath::Pi())*[6]))*exp(-0.5*(((x-[5])/[6])**2))+(([1]*(1-[2]))/(sqrt(2*TMath::Pi())*[6]))*exp(-0.5*(((x-[5]-2*TMath::Pi())/[6])**2))+([7]/(sqrt(2*TMath::Pi())*[9]))*exp(-0.5*(((x-[8])/[9])**2))+([7]/(sqrt(2*TMath::Pi())*[9]))*exp(-0.5*(((x-[8]+2*TMath::Pi())/[9])**2))", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
	fitFuncDPhiTemplate->SetLineColor(kBlue);	

	fitFuncDPhiTemplate->SetParName(0, "const");

	fitFuncDPhiTemplate->SetParName(1, "yieldNS");
	fitFuncDPhiTemplate->SetParName(2, "fraction");
	fitFuncDPhiTemplate->SetParName(3, "muDPhiNS1");
	fitFuncDPhiTemplate->FixParameter(3, 0.);
	fitFuncDPhiTemplate->SetParName(4, "sigmaDPhiNS1");
	fitFuncDPhiTemplate->SetParLimits(4, 0.1, 2.0);	

	fitFuncDPhiTemplate->SetParName(5, "muDPhiNS2");
	fitFuncDPhiTemplate->FixParameter(5, 0.);
	fitFuncDPhiTemplate->SetParName(6, "sigmaDPhiNS2");
	fitFuncDPhiTemplate->SetParLimits(6, 0.1, 2.0);		

	fitFuncDPhiTemplate->SetParName(7, "yieldAS");
	fitFuncDPhiTemplate->SetParName(8, "muDPhiAS");
	fitFuncDPhiTemplate->FixParameter(8, TMath::Pi());
	fitFuncDPhiTemplate->SetParName(9, "sigmaDPhiAS");
	fitFuncDPhiTemplate->SetParLimits(9, 0.1, 2.0);		

	fitFuncDPhiTemplate->SetParName(10, "V1D");
	fitFuncDPhiTemplate->SetParName(11, "V2D");
	fitFuncDPhiTemplate->SetParName(12, "V3D");
	fitFuncDPhiTemplate->SetParName(13, "V4D");

	// ***************************
	// * CREATE DPHI PROJECTIONS *
	// ***************************

	Int_t minBinDEtaSignal = aDEta->FindBin(-DEtaSignalMax + 0.001);
	Int_t maxBinDEtaSignal = aDEta->FindBin(DEtaSignalMax - 0.001);
	Int_t nBinsDEtaSignal = maxBinDEtaSignal - minBinDEtaSignal + 1;
	Double_t RangeDEtaSignal = aDEta->GetBinUpEdge(maxBinDEtaSignal) - aDEta->GetBinLowEdge(minBinDEtaSignal);
	
	Int_t minBinDEtaBulk = aDEta->FindBin(DEtaBulkMin + 0.001);
	Int_t maxBinDEtaBulk = aDEta->FindBin(DEtaBulkMax - 0.001);

	// Scale the projections with the bin width, so that integrals yield counts.
	TH1F* hDPhiProjCentralDEta = (TH1F*)correctedDiHadron->ProjectionX(Form("dPhiProj_%s_ptBin%i", fSettings->GetSpeciesName(species), pTBinMin), minBinDEtaSignal, maxBinDEtaSignal);
	hDPhiProjCentralDEta->Scale(1., "width");
	hDPhiProjCentralDEta->GetYaxis()->SetTitle(Form("#frac{dN_{%s}}{d#Delta#phi}", fSettings->GetSpeciesNameLaTeX(species)));
	hDPhiProjCentralDEta->SetLineColor(kBlack);
	hDPhiProjCentralDEta->SetMarkerStyle(20);
	hDPhiProjCentralDEta->SetMarkerColor(kBlack);

	// Same histogram as before, but now with the range (0, 2pi).
	TH1F* hDPhiProjCentralDEtaAwaySideCentered = AliHistToolsDiHadronPID::ShiftHistogram(hDPhiProjCentralDEta, hDPhiProjCentralDEta->FindBin(0.001));

	// All of DEta, but Jet cut out (note that in PbPb you may want to do a "wing" correction).
	TH1F* hDPhiProjCentralDEtaJetCut = CutOutJetAndProject(correctedDiHadron, DEtaBulkMax, PeakRadiusDPhi, PeakRadiusDEta, kTRUE); // true statement for const-fit approach.
	hDPhiProjCentralDEtaJetCut->SetNameTitle("JetCut", "Projection without peak");
	hDPhiProjCentralDEtaJetCut->Scale(nBinsDEtaSignal, "width");
	hDPhiProjCentralDEtaJetCut->GetYaxis()->SetTitle(Form("#frac{dN_{%s}}{d#Delta#phi}", fSettings->GetSpeciesNameLaTeX(species)));
	hDPhiProjCentralDEtaJetCut->SetLineColor(kBlue);
	hDPhiProjCentralDEtaJetCut->SetMarkerStyle(21);
	hDPhiProjCentralDEtaJetCut->SetMarkerColor(kBlue);

	// DEta Gap. Note that this is normalised per DEta bin, so we rescale to the number of bins in the central area.
	TH1F* hDPhiProjGapDEta = GetHistBulkFitWithConst(correctedDiHadron, aDEta->GetBinCenter(minBinDEtaBulk), 0x0, kTRUE); // USE SUM INSTEAD OF CONST FIT!
	hDPhiProjGapDEta->SetNameTitle("DEtaGap", "Large #Delta#eta");
	hDPhiProjGapDEta->Scale(nBinsDEtaSignal, "width");
	hDPhiProjGapDEta->GetYaxis()->SetTitle(Form("#frac{dN_{%s}}{d#Delta#phi}", fSettings->GetSpeciesNameLaTeX(species)));
	hDPhiProjGapDEta->SetLineColor(kRed);
	hDPhiProjGapDEta->SetMarkerStyle(22);
	hDPhiProjGapDEta->SetMarkerColor(kRed);

	// ******************************
	// * BACKGROUND ESTIMATION DPHI *
	// ******************************

	// Estimate non-nearside peak yield with Jet-Cut.
	TF1* fitFuncFlowGapDEta = FitDPhiFlowAndASPeak(species, pTBinMin, hDPhiProjGapDEta, hDPhiProjCentralDEta, fitFuncDPhiTemplate, model, kRed, kTRUE);
	aBulkYieldMeasurements->SetBinLabel(1, "#Delta#eta Gap (BG Fit)");
	hBulkYieldMeasurements->SetBinContent(1, fitFuncFlowGapDEta->GetParameter(0) / RangeDEtaSignal);
	hBulkYieldMeasurements->SetBinError(1, fitFuncFlowGapDEta->GetParError(0) / RangeDEtaSignal);

	TF1* fitFuncFlowJetCut = FitDPhiFlowAndASPeak(species, pTBinMin, hDPhiProjCentralDEtaJetCut, hDPhiProjCentralDEta, fitFuncDPhiTemplate, model, kBlue, kTRUE);
	aBulkYieldMeasurements->SetBinLabel(2, "Jet Cut (BG Fit)");
	hBulkYieldMeasurements->SetBinContent(2, fitFuncFlowJetCut->GetParameter(0) / RangeDEtaSignal);
	hBulkYieldMeasurements->SetBinError(2, fitFuncFlowJetCut->GetParError(0) / RangeDEtaSignal);

	// Estimate flow by performing a DCT. (PbPb only)
	TGraphErrors* dctFuncFlowGapDEta = 0x0; 
	TGraphErrors* dctFuncFlowJetCut = 0x0;
	Double_t dctBackground = 0.;
	Double_t dctBackgroundError = 0.;
	
	if (beamType == AliSettingsDiHadronPID::kPbPb) {
		dctFuncFlowGapDEta = DCTDPhiFlow(beamType, species, pTBinMin, hDPhiProjGapDEta, kGreen, dctBackground, dctBackgroundError, 1, 4, kTRUE);
		dctFuncFlowJetCut = DCTDPhiFlow(beamType, species, pTBinMin, hDPhiProjCentralDEtaJetCut, kGreen, dctBackground, dctBackgroundError, 1, 4, kTRUE);
	}	

	if (beamType == AliSettingsDiHadronPID::kPPb) {
		dctFuncFlowGapDEta = DCTDPhiFlow(beamType, species, pTBinMin, hDPhiProjGapDEta, kGreen, dctBackground, dctBackgroundError, 2, 2, kTRUE);
		dctFuncFlowJetCut = DCTDPhiFlow(beamType, species, pTBinMin, hDPhiProjCentralDEtaJetCut, kGreen, dctBackground, dctBackgroundError, 2, 2, kTRUE);
	}

	// Measurement of the background. Note that the measurement is a density.
	if (beamType == AliSettingsDiHadronPID::kPP) {
		measurementOfChoice[0] = fitFuncFlowJetCut->GetParameter(0) / RangeDEtaSignal;
		measurementOfChoiceError[0] = fitFuncFlowJetCut->GetParError(0) / RangeDEtaSignal;
	} else {
		measurementOfChoice[0] = dctBackground / RangeDEtaSignal;
		measurementOfChoiceError[0] = dctBackgroundError / RangeDEtaSignal;		
	}

	// Make a plot of the DPhi projections together with their fits.
	if (cvs) {

		cvs->cd();

		hDPhiProjCentralDEta->DrawClone(); 
		legend->AddEntry(hDPhiProjCentralDEta, Form("|#Delta#eta| < %4.2f", aDEta->GetBinUpEdge(maxBinDEtaSignal)));

		hDPhiProjCentralDEtaJetCut->Draw("same");
		legend->AddEntry(hDPhiProjCentralDEtaJetCut, Form("|#Delta#eta| < %4.2f, no Peak", aDEta->GetBinUpEdge(maxBinDEtaBulk)));

		if (beamType == AliSettingsDiHadronPID::kPbPb || beamType == AliSettingsDiHadronPID::kPPb) {
			if (dctFuncFlowJetCut) {dctFuncFlowJetCut->Draw("same 3");}
			if (dctFuncFlowJetCut) legend->AddEntry(dctFuncFlowJetCut, Form("|#Delta#eta| < %4.2f, no Peak (DCT)", aDEta->GetBinUpEdge(maxBinDEtaBulk)));
		} else {
			hDPhiProjGapDEta->Draw("same");
			legend->AddEntry(hDPhiProjGapDEta, Form("%4.2f < |#Delta#eta| < %4.2f", aDEta->GetBinLowEdge(minBinDEtaBulk), aDEta->GetBinUpEdge(maxBinDEtaBulk)));
			fitFuncFlowGapDEta->Draw("same");	
			fitFuncFlowJetCut->Draw("same");	
			
		}

		legend->Draw(); 

		cvs->SaveAs(Form("./%s/Sp%i/DPhiProjectionsPtBin%02i.pdf", fPlotDir.Data(), species, pTBinMin));
		cvs->Clear();
		legend->Clear();
		

		TFile* dPhiFile = new TFile(Form("./%s/Sp%i/DPhiProjectionsPtBin%02i.root", fPlotDir.Data(), species, pTBinMin), "recreate");
		hDPhiProjCentralDEta->Write();
		dPhiFile->Close();


	}

	// ******************************
	// * JET YIELD BY COUNTING DPHI *
	// ******************************

	// Double_t jetYield_GapDEtaCount = 0.;
	// Double_t jetYieldError_GapDEtaCount = 0.;
	// GetDPhiJetYieldAndErrorByCounting(hDPhiProjCentralDEta, hDPhiProjGapDEta, jetYield_GapDEtaCount, jetYieldError_GapDEtaCount);
	// aJetYieldMeasurements->SetBinLabel(2, "#Delta#eta Gap (BG Count)");
	// hJetYieldMeasurements->SetBinContent(2, jetYield_GapDEtaCount);
	// hJetYieldMeasurements->SetBinError(2, jetYieldError_GapDEtaCount);

	// Counting in DPhi.
	Double_t jetYield_JetCutFit = 0.;
	Double_t jetYieldError_JetCutFit = 0.;
	GetDPhiJetYieldAndErrorByCounting(hDPhiProjCentralDEta, fitFuncFlowJetCut, jetYield_JetCutFit, jetYieldError_JetCutFit);
	aJetYieldMeasurements->SetBinLabel(1, "Jet Count (BG: Jet Cut)");
	hJetYieldMeasurements->SetBinContent(1, jetYield_JetCutFit);
	hJetYieldMeasurements->SetBinError(1, jetYieldError_JetCutFit);

	// if (beamType == AliSettingsDiHadronPID::kPP || beamType == AliSettingsDiHadronPID::kPPb) {
	// 	measurementOfChoice[1] = jetYield_JetCutFit;
	// 	measurementOfChoiceError[1] = jetYieldError_JetCutFit;
	// }

	Double_t jetYield_GapDEtaFit = 0.;
	Double_t jetYieldError_GapDEtaFit = 0.;
	GetDPhiJetYieldAndErrorByCounting(hDPhiProjCentralDEta, fitFuncFlowGapDEta, jetYield_GapDEtaFit, jetYieldError_GapDEtaFit);
	aJetYieldMeasurements->SetBinLabel(2, "Jet Counts (BG: #Delta#eta Gap)");
	hJetYieldMeasurements->SetBinContent(2, jetYield_GapDEtaFit);
	hJetYieldMeasurements->SetBinError(2, jetYieldError_GapDEtaFit);

	// Counting in (DPhi,DEta) (circular region)
	Double_t jetYield_CircleCount = 0.;
	Double_t jetYieldError_CircleCount = 0.;
	if (beamType == AliSettingsDiHadronPID::kPbPb || beamType == AliSettingsDiHadronPID::kPPb) {
		GetJetYieldAndErrorByCountingInJetRegion(correctedDiHadron, dctFuncFlowJetCut, jetYield_CircleCount, jetYieldError_CircleCount, DEtaBulkMax, PeakRadiusDPhi, PeakRadiusDEta);
	} else {
		GetJetYieldAndErrorByCountingInJetRegion(correctedDiHadron, fitFuncFlowJetCut, jetYield_CircleCount, jetYieldError_CircleCount, DEtaBulkMax, PeakRadiusDPhi, PeakRadiusDEta);
	}
	aJetYieldMeasurements->SetBinLabel(7, "Jet Counts (Circle)");
	hJetYieldMeasurements->SetBinContent(7, jetYield_CircleCount);
	hJetYieldMeasurements->SetBinError(7, jetYieldError_CircleCount);

	// For all systems, the measurement of choice is the same for the jet.
	measurementOfChoice[1] = jetYield_CircleCount;
	measurementOfChoiceError[1] = jetYieldError_CircleCount;

	// Double_t jetYield_JetCutCount = 0.;
	// Double_t jetYieldError_JetCutCount = 0.;
	// GetDPhiJetYieldAndErrorByCounting(hDPhiProjCentralDEta, hDPhiProjCentralDEtaJetCut, jetYield_JetCutCount, jetYieldError_JetCutCount);
	// aJetYieldMeasurements->SetBinLabel(4, "Jet Cut (BG Count)");
	// hJetYieldMeasurements->SetBinContent(4, jetYield_JetCutCount);
	// hJetYieldMeasurements->SetBinError(4, jetYieldError_JetCutCount);

	// ********************
	// * SG PEAK FIT DPHI *
	// ********************
	TF1* fitFuncDPhiTotalSGGapDEta = FitDPhiNSPeak(species, pTBinMin, hDPhiProjCentralDEta, hDPhiProjGapDEta, fitFuncFlowGapDEta, model, kFALSE, kRed, kTRUE);
	aJetYieldMeasurements->SetBinLabel(3, "SG Fit (BG: Jet Cut)");
	hJetYieldMeasurements->SetBinContent(3, fitFuncDPhiTotalSGGapDEta->GetParameter(1));
	hJetYieldMeasurements->SetBinError(3, fitFuncDPhiTotalSGGapDEta->GetParError(1));			

	// if (beamType == AliSettingsDiHadronPID::kPbPb) {
	// 	measurementOfChoice[1] = fitFuncDPhiTotalSGGapDEta->GetParameter(1);
	// 	measurementOfChoiceError[1] = fitFuncDPhiTotalSGGapDEta->GetParError(1);
	// }

	TF1* fitFuncDPhiTotalSGJetCut = FitDPhiNSPeak(species, pTBinMin, hDPhiProjCentralDEta, hDPhiProjCentralDEtaJetCut, fitFuncFlowJetCut, model, kFALSE, kRed, kTRUE);
	aJetYieldMeasurements->SetBinLabel(4, "SG Fit (BG: #Delta#eta Gap)");
	hJetYieldMeasurements->SetBinContent(4, fitFuncDPhiTotalSGJetCut->GetParameter(1));
	hJetYieldMeasurements->SetBinError(4, fitFuncDPhiTotalSGJetCut->GetParError(1));	

	peakWidthSG[0] = fitFuncDPhiTotalSGJetCut->GetParameter(4);
	peakWidthSGError[0] = fitFuncDPhiTotalSGJetCut->GetParError(4);

	aBulkYieldMeasurements->SetBinLabel(3, "SG NS Total Fit");
	hBulkYieldMeasurements->SetBinContent(3, fitFuncDPhiTotalSGJetCut->GetParameter(0) / RangeDEtaSignal);
	hBulkYieldMeasurements->SetBinError(3, fitFuncDPhiTotalSGJetCut->GetParError(0) / RangeDEtaSignal);

	TF1* fitFuncDPhiTotalSGJetCutBGD = new TF1(*fitFuncDPhiTotalSGJetCut);
	fitFuncDPhiTotalSGJetCutBGD->SetParameter(1, 0.);
	fitFuncDPhiTotalSGJetCutBGD->SetParameter(7, 0.);
	fitFuncDPhiTotalSGJetCutBGD->SetLineStyle(2);

	// ********************
	// * DG PEAK FIT DPHI *
	// ********************
	TF1* fitFuncDPhiTotalDGGapDEta = FitDPhiNSPeak(species, pTBinMin, hDPhiProjCentralDEta, hDPhiProjGapDEta, fitFuncFlowGapDEta, model, kTRUE, kRed, kTRUE);
	aJetYieldMeasurements->SetBinLabel(5, "DG Fit (BG: Jet Cut)");
	hJetYieldMeasurements->SetBinContent(5, fitFuncDPhiTotalDGGapDEta->GetParameter(1));
	hJetYieldMeasurements->SetBinError(5, fitFuncDPhiTotalDGGapDEta->GetParError(1));			

	TF1* fitFuncDPhiTotalDGJetCut = FitDPhiNSPeak(species, pTBinMin, hDPhiProjCentralDEta, hDPhiProjCentralDEtaJetCut, fitFuncFlowJetCut, model, kTRUE, kRed, kTRUE);
	aJetYieldMeasurements->SetBinLabel(6, "DG Fit (BG: #Delta#eta Gap)");
	hJetYieldMeasurements->SetBinContent(6, fitFuncDPhiTotalDGJetCut->GetParameter(1));
	hJetYieldMeasurements->SetBinError(6, fitFuncDPhiTotalDGJetCut->GetParError(1));			

	aBulkYieldMeasurements->SetBinLabel(4, "DG NS Total Fit");
	hBulkYieldMeasurements->SetBinContent(4, fitFuncDPhiTotalDGJetCut->GetParameter(0) / RangeDEtaSignal);
	hBulkYieldMeasurements->SetBinError(4, fitFuncDPhiTotalDGJetCut->GetParError(0) / RangeDEtaSignal);

	TF1* fitFuncDPhiTotalDGJetCutBGD = new TF1(*fitFuncDPhiTotalDGJetCut);
	fitFuncDPhiTotalDGJetCutBGD->SetParameter(1, 0.);
	fitFuncDPhiTotalDGJetCutBGD->SetParameter(7, 0.);
	fitFuncDPhiTotalDGJetCutBGD->SetLineStyle(2);

	// ***********************************************************
	// * DG PEAK FIT DETA WITH FIXED BASELINE FROM DPHI ANALYSIS *
	// ***********************************************************

	Double_t bgdEstFromDPhiFit = fitFuncDPhiTotalDGJetCutBGD->Integral(hDEtaProjNSDPhi->GetBinLowEdge(minBinDPhiNS), hDEtaProjNSDPhi->GetBinLowEdge(maxBinDPhiNS + 1));

	TF1* fitFuncDEtaNSTotalDGFixedBG = new TF1(*fitFuncDEtaNSTotalDG);
	fitFuncDEtaNSTotalDGFixedBG->FixParameter(0, bgdEstFromDPhiFit);

	hDEtaProjNSDPhi->Fit(fitFuncDEtaNSTotalDGFixedBG, "RQI0");

	// ****************************************
	// * FIGURE SUMMARISING THE DPHI ANALYSIS *
	// ****************************************

	if (cvs) {

		cvs->cd();
		hDPhiProjCentralDEta->Draw();

		fitFuncFlowJetCut->SetLineColor(kRed);
		fitFuncFlowJetCut->Draw("same");
		
		fitFuncDPhiTotalSGJetCut->SetLineColor(kGreen);		
		fitFuncDPhiTotalSGJetCut->Draw("same");

		fitFuncDPhiTotalSGJetCutBGD->SetLineColor(kGreen);				
		fitFuncDPhiTotalSGJetCutBGD->Draw("same");

		fitFuncDPhiTotalDGJetCut->Draw("same");
		fitFuncDPhiTotalDGJetCutBGD->Draw("same");

		legend->AddEntry(hDPhiProjCentralDEta, Form("|#Delta#eta| < %4.2f", aDEta->GetBinUpEdge(maxBinDEtaSignal)));
		legend->AddEntry(fitFuncFlowJetCut, "Background (Jet Cut)");
		legend->AddEntry(fitFuncDPhiTotalSGJetCut, "NS SG Fit");
		legend->AddEntry(fitFuncDPhiTotalSGJetCutBGD, "Background");
		legend->AddEntry(fitFuncDPhiTotalDGJetCut, "NS DG Fit");
		legend->AddEntry(fitFuncDPhiTotalDGJetCutBGD, "Background");
		legend->Draw();

		cvs->SaveAs(Form("./%s/Sp%i/DPhiAnalysisPtBin%02i.pdf", fPlotDir.Data(), species, pTBinMin));
		cvs->Clear();
		legend->Clear();


	}

	// ****************************************
	// * FIGURE SUMMARISING THE DETA ANALYSIS *
	// ****************************************

	if (cvs) {

		cvs->cd();

		hDEtaProjNSDPhi->Draw(); 
		
		fitFuncDEtaNSTotalSG->Draw("same");
		fitFuncDEtaNSTotalDG->SetLineColor(kBlue);
		fitFuncDEtaNSTotalDG->Draw("same");
		// fitFuncDEtaNSTotalDGFixedBG->SetLineColor(kGreen);
		// fitFuncDEtaNSTotalDGFixedBG->Draw("same");

		// Create helper functions.
		TF1* fitFuncWideGauss = new TF1(*fitFuncDEtaNSTotalDG);
		fitFuncWideGauss->SetLineStyle(2);
		fitFuncWideGauss->SetParameter(2, fitFuncWideGauss->GetParameter(2) * (1 - fitFuncWideGauss->GetParameter(3))); // Set the narrow Gauss to 0.
		fitFuncWideGauss->SetParameter(3, 0.); // Set the fraction of the narrow peak to 0.
		fitFuncWideGauss->Draw("same");

		// TF1* fitFuncWideGaussFixedBG = new TF1(*fitFuncDEtaNSTotalDGFixedBG);
		// fitFuncWideGaussFixedBG->SetLineStyle(2);
		// fitFuncWideGaussFixedBG->SetParameter(2, 0.); // Set the narrow Gauss to 0.
		// fitFuncWideGaussFixedBG->Draw("same");

		legend->AddEntry(hDEtaProjNSDPhi, Form("%4.2f#pi < #Delta#phi < %4.2f#pi", correctedDiHadron->GetXaxis()->GetBinLowEdge(minBinDPhiNS)/TMath::Pi(), correctedDiHadron->GetXaxis()->GetBinUpEdge(maxBinDPhiNS)/TMath::Pi()));
		legend->AddEntry(fitFuncDEtaNSTotalSG, "SG Fit");
		legend->AddEntry(fitFuncDEtaNSTotalDG, "DG Fit");
		legend->AddEntry(fitFuncWideGauss, "Pedestal");
		// legend->AddEntry(fitFuncDEtaNSTotalDGFixedBG, "DG Fit (BG from #Delta#phi fit)");		
		// legend->AddEntry(fitFuncWideGaussFixedBG, "Pedestal");
		legend->Draw(); 

		AliHistToolsDiHadronPID::FixPlotRange(cvs);

		cvs->SaveAs(Form("./%s/Sp%i/DEtaAnalysisPtBin%02i.pdf", fPlotDir.Data(), species, pTBinMin));
		cvs->Clear();
		legend->Clear();

		delete fitFuncWideGauss;

		TFile* dEtaFile = new TFile(Form("./%s/Sp%i/DEtaProjectionsPtBin%02i.root", fPlotDir.Data(), species, pTBinMin), "recreate");
		TH1F* hDEtaProjNSDPhiScaled = (TH1F*)hDEtaProjNSDPhi->Clone();
		hDEtaProjNSDPhiScaled->SetDirectory(0);

		hDEtaProjNSDPhiScaled->SetLineColor(fSettings->GetSpeciesColor(species));
		hDEtaProjNSDPhiScaled->SetMarkerColor(fSettings->GetSpeciesColor(species));

		// Subtract the background and scale to one.
		for (Int_t iBinDEta = 1; iBinDEta <= hDEtaProjNSDPhiScaled->GetNbinsX(); iBinDEta++) {
			hDEtaProjNSDPhiScaled->SetBinContent(iBinDEta, hDEtaProjNSDPhiScaled->GetBinContent(iBinDEta) - fitFuncDEtaNSTotalSG->GetParameter(0));
		}

		hDEtaProjNSDPhiScaled->Scale(1./hDEtaProjNSDPhiScaled->GetBinContent(hDEtaProjNSDPhiScaled->GetMaximumBin()));		
		hDEtaProjNSDPhiScaled->SetMinimum(-0.1);
		hDEtaProjNSDPhiScaled->SetMaximum(1.1);

		hDEtaProjNSDPhiScaled->Write();
		dEtaFile->Close();

		delete hDEtaProjNSDPhiScaled;

	}

	// *******************************************
	// * FIGURE COMPARING MEASUREMENT STRATEGIES *
	// *******************************************

	if (cvs) {
		cvs->cd();

		// Choose different preferred measurements for PbPb and pp/pPb.
		hJetYieldMeasurements->Scale(1. / hJetYieldMeasurements->GetBinContent(beamType == AliSettingsDiHadronPID::kPbPb ? 3 : 1));
		hJetYieldMeasurements->Draw();

		TF1* hConst = new TF1("hConst", "[0]", 0, 1);
		hConst->SetParameter(0,1);
		hConst->SetLineStyle(2);
		hConst->Draw("same");

		cvs->SaveAs(Form("./%s/Sp%i/jetYieldMeasurementsPtBin%02i.pdf", fPlotDir.Data(), species, pTBinMin));
		cvs->Clear();

		cvs->cd();
		hBulkYieldMeasurements->Scale(1. / hBulkYieldMeasurements->GetBinContent(2));
		hBulkYieldMeasurements->Draw();

		hConst->Draw("same");

		cvs->SaveAs(Form("./%s/Sp%i/bulkYieldMeasurementsPtBin%02i.pdf", fPlotDir.Data(), species, pTBinMin));

		cvs->Clear();		

		delete hConst;

	}

	fitFuncDPhiTotalSGGapDEta->GetParameters(parameters);
	parameters[0] /= (2.* DEtaSignalMax); // Scale the background. (maybe no longer necessary!!)
	
	for (Int_t iPar = 0; iPar < 14; ++iPar) {errors[iPar] = fitFuncDPhiTotalSGGapDEta->GetParError(iPar);}	
	errors[0] /= (2.* DEtaSignalMax);  // Scale the background.

	delete fitFuncFlowGapDEta;
	delete fitFuncFlowJetCut;

	delete fitFuncDPhiTotalSGGapDEta;
	delete fitFuncDPhiTotalSGJetCut;
	delete fitFuncDPhiTotalSGJetCutBGD;

	delete fitFuncDPhiTotalDGGapDEta;
	delete fitFuncDPhiTotalDGJetCut;
	delete fitFuncDPhiTotalDGJetCutBGD;

	delete fitFuncDEtaNSTotalSG;
	delete fitFuncDEtaNSTotalDG;
	delete fitFuncDEtaNSTotalDGFixedBG;

	if (correctedDiHadron) {delete correctedDiHadron;}
	if (fitFuncDPhiTemplate) {delete fitFuncDPhiTemplate;}
	if (hDPhiProjCentralDEta) {delete hDPhiProjCentralDEta;}
	if (hDPhiProjCentralDEtaAwaySideCentered) {delete hDPhiProjCentralDEtaAwaySideCentered;}
	if (hDPhiProjGapDEta) {delete hDPhiProjGapDEta;}

	TH1F** hout = new TH1F*[2];
	hout[0] = hJetYieldMeasurements;
	hout[1] = hBulkYieldMeasurements;

	return hout;

}

// -----------------------------------------------------------------------
TF1* AliRatiosDiHadronPID::FitDPhiFlowAndASPeak(AliSettingsDiHadronPID::Species species, Int_t pTBin, TH1* hist, TH1* signalHist, const TF1* initialFunc, DPhiBackgroundModel model, Color_t mainColor, Bool_t saveFitToPDF) const {

	// Helper method for FitDPhi. This method is responsible for fitting hist with the flow components and constant of the fit function.
	// Optionally, plots are made and saved.
	//
	// NOTE: 1) This method assumes that there is no significant short range DEta correlation
	//       present in the histogram!
	//       2) This method will touch most parameters, and set the near side peak parameters to zero.

	cout << endl << "FITTING FLOW BGD ***" << endl;
	
	// Create a clone of initialFunc, and set to the desired color.
	TF1* fitFunc = new TF1(*initialFunc);
	fitFunc->SetLineColor(mainColor);

	// Estimate background by taking the average over the near side.
	Double_t NSBgdEst = 0.;
	Double_t nBinsTmp = 0.;
	for (Int_t iBin = 1; iBin <= hist->FindBin(TMath::Pi()/2.); iBin++) {
		nBinsTmp++;
		NSBgdEst += hist->GetBinContent(iBin);	
	}
	NSBgdEst /= nBinsTmp;

	fitFunc->SetParameter(0, NSBgdEst);

	// Fix all parameters.
	fitFunc->FixParameter(1, 0.0); 							// Yield,NS
	fitFunc->FixParameter(2, 1.0);							// Fraction NS1 vs NS2
	fitFunc->FixParameter(3, fitFunc->GetParameter(3));		// Mu,NS1
	fitFunc->FixParameter(4, 0.3);							// Sigma,NS1

	fitFunc->FixParameter(5, fitFunc->GetParameter(5));		// Mu,NS2
	fitFunc->FixParameter(6, 0.6);							// Sigma,NS2

	fitFunc->FixParameter(7, 0.0);							// Yield,AS
	fitFunc->FixParameter(8, fitFunc->GetParameter(8));		// Mu,AS
	fitFunc->FixParameter(9, 0.6);							

	fitFunc->FixParameter(10, 0.0); 
	fitFunc->FixParameter(11, 0.0);
	fitFunc->FixParameter(12, 0.0);
	fitFunc->FixParameter(13, 0.0);

	// If the away side is considered, then do the initial fit only on the near side.
	if (model == kWithFixed_V2_V3_AS || model == kWithFixed_V2_AS || model == kNoFlow_AS) {
		fitFunc->SetRange(-TMath::Pi()/2., TMath::Pi()/2.);
	}			

	// Include v1 if requested.
	if (model == kWithFixed_V1_V2_V3_V4) {
		fitFunc->ReleaseParameter(10);
		fitFunc->SetParameter(10, 0.001);
	}

	// Include v2 if requested.
	if (model == kWithFixed_V1_V2_V3_V4 ||
		model == kWithFixed_V2 || model == kWithFixed_V2_AS ||
		model == kWithFixed_V2_V3 || model == kWithFixed_V2_V3_AS || 
		model == kWithFixed_V2_V3_V4) {

		fitFunc->ReleaseParameter(11);
		fitFunc->SetParameter(11, 0.01);

		if (model == kWithFixed_V2 || model == kWithFixed_V2_AS) {hist->Fit(fitFunc, "RQI0");}

	}

	// Include v3 if requested.
	if (model == kWithFixed_V1_V2_V3_V4 ||
		model == kWithFixed_V2_V3 || model == kWithFixed_V2_V3_AS || 
		model == kWithFixed_V2_V3_V4) {

		fitFunc->ReleaseParameter(12);				
		fitFunc->SetParameter(12, 0.005);

		// Perform a fit.
		if (model == kWithFixed_V2_V3 || model == kWithFixed_V2_V3_AS) {hist->Fit(fitFunc, "RQI0");}
	}

	// Include v4 if requested.
	if (model == kWithFixed_V1_V2_V3_V4 ||
		model == kWithFixed_V2_V3_V4) {

		fitFunc->ReleaseParameter(13);				
		fitFunc->SetParameter(13, 0.001);

		// Perform a fit.
		hist->Fit(fitFunc, "RQI0");

	}

	// Include AS peak if requested.
	if (model == kNoFlow_AS || model == kWithFixed_V2_AS || model == kWithFixed_V2_V3_AS) {

		// Reset the fit range to the full DPhi range.
		fitFunc->SetRange(-TMath::Pi()/2., 3.*TMath::Pi()/2.);

		fitFunc->FixParameter(0, fitFunc->GetParameter(0));
		fitFunc->FixParameter(10, fitFunc->GetParameter(10));
		fitFunc->FixParameter(11, fitFunc->GetParameter(11));
		fitFunc->FixParameter(12, fitFunc->GetParameter(12));
		fitFunc->FixParameter(13, fitFunc->GetParameter(13));
		fitFunc->ReleaseParameter(7);
		fitFunc->ReleaseParameter(9);

		// Get a reasonable estimate for the yield under the AS peak.
		Double_t tmp = 0.;
		Double_t PeakASest = hist->IntegralAndError(hist->FindBin(TMath::Pi() - 1.), hist->FindBin(TMath::Pi() + 1.), tmp, "width");
		Double_t BgdEst = (hist->GetBinContent(hist->FindBin(TMath::Pi()/2.)) + hist->GetBinContent(hist->GetNbinsX())) * hist->GetBinWidth(1);
		Double_t nBins = hist->FindBin(TMath::Pi() + 1.) - hist->FindBin(TMath::Pi() - 1.) + 1.;

		// Assume that half the AS yield is due to the AS peak.
		BgdEst *= (nBins / 2.);
		cout << " AS Peak Estimate (" << hist->GetName() << "): " << PeakASest << " - " << BgdEst << " = " << PeakASest - BgdEst << " Fit: " << fitFunc->GetParameter(7) << endl;
	
		// Note that this estimate does not work very well if the first flow fits aren't very good.
		fitFunc->SetParameter(7, PeakASest - BgdEst);			
		
		// Perform an initial fit.
		hist->Fit(fitFunc, "RQI0");
		
		// Perform a second fit, with baseline parameters released.
		fitFunc->ReleaseParameter(0);

		hist->Fit(fitFunc, "RQI0");

		// if (model == kWithFixed_V2_AS || model == kWithFixed_V2_V3_AS) {fitFunc->ReleaseParameter(11);}
		// if (model == kWithFixed_V2_V3_AS) {fitFunc->ReleaseParameter(12);}

		// hist->Fit(fitFunc, "RQI0");
		
	}

	// Refit with flow coefficients (only in case of v2 now...)
	if (model == kWithFixed_V1_V2_V3_V4 ||
		model == kWithFixed_V2 || model == kWithFixed_V2_AS ||
		model == kWithFixed_V2_V3 || model == kWithFixed_V2_V3_AS || 
		model == kWithFixed_V2_V3_V4) {

		fitFunc->ReleaseParameter(11);

		if (model == kWithFixed_V2 || model == kWithFixed_V2_AS) {hist->Fit(fitFunc, "RQI0");}

	}

	// Save pics.
	if (saveFitToPDF) {

		TCanvas* cvs = TCanvas::MakeDefCanvas();

		// Make a plot of the fourier decomposition of the fit to the large DEta gap fit.
		if (signalHist) {
			signalHist->Draw();
			hist->Draw("same");
		} else {
			hist->Draw();
		}

		fitFunc->Draw("same");

		TF1* funcConst = new TF1("funcConst", "[0]", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
		funcConst->SetLineStyle(2);
		funcConst->SetParameter(0, fitFunc->GetParameter(0));
		funcConst->SetLineColor(mainColor - 2);
		funcConst->Draw("same");

		TF1* funcV1D = 0x0;
		if (model == kWithFixed_V1_V2_V3_V4) {

			funcV1D = new TF1("funcV1D", "[0]*(1+2*[1]*cos(x))", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
			funcV1D->SetLineStyle(2);		
			funcV1D->SetParameters(fitFunc->GetParameter(0), fitFunc->GetParameter(10));
			funcV1D->SetLineColor(mainColor - 2);
			funcV1D->Draw("same");

		}

		TF1* funcV2D = 0x0;
		if (model == kWithFixed_V1_V2_V3_V4 ||
			model == kWithFixed_V2 || model == kWithFixed_V2_AS ||
			model == kWithFixed_V2_V3 || model == kWithFixed_V2_V3_AS || 
			model == kWithFixed_V2_V3_V4) {

			funcV2D = new TF1("funcV2D", "[0]*(1+2*[1]*cos(2*x))", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
			funcV2D->SetLineStyle(2);		
			funcV2D->SetParameters(fitFunc->GetParameter(0), fitFunc->GetParameter(11));
			funcV2D->SetLineColor(mainColor - 4);
			funcV2D->Draw("same");

		}

		TF1* funcV3D = 0x0;
		if (model == kWithFixed_V1_V2_V3_V4 ||
			model == kWithFixed_V2_V3 || model == kWithFixed_V2_V3_AS || 
			model == kWithFixed_V2_V3_V4) {

			funcV3D = new TF1("funcV3D", "[0]*(1+2*[1]*cos(3*x))", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
			funcV3D->SetLineStyle(2);	
			funcV3D->SetParameters(fitFunc->GetParameter(0), fitFunc->GetParameter(12));
			funcV3D->SetLineColor(mainColor - 6);
			funcV3D->Draw("same");
		}

		TF1* funcV4D = 0x0;
		if (model == kWithFixed_V1_V2_V3_V4 || 
			model == kWithFixed_V2_V3_V4) {

			funcV4D = new TF1("funcV4D", "[0]*(1+2*[1]*cos(4*x))", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
			funcV4D->SetLineStyle(2);	
			funcV4D->SetParameters(fitFunc->GetParameter(0), fitFunc->GetParameter(13));
			funcV4D->SetLineColor(mainColor - 8);
			funcV4D->Draw("same");

		}

		TF1* funcSingleGauss = 0x0;
		if (model == kNoFlow_AS || model == kWithFixed_V2_AS || model == kWithFixed_V2_V3_AS) {

			funcSingleGauss = new TF1("funcSingleGauss", "[0]+([1]/(sqrt(2*TMath::Pi())*[3]))*exp(-0.5*(((x-[2])/[3])**2))+([1]/(sqrt(2*TMath::Pi())*[3]))*exp(-0.5*(((x-[2]+2*TMath::Pi())/[3])**2))+([1]/(sqrt(2*TMath::Pi())*[3]))*exp(-0.5*(((x-[2]-2*TMath::Pi())/[3])**2))", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
			funcSingleGauss->SetLineColor(kBlack);
			funcSingleGauss->SetParameters(fitFunc->GetParameter(0), fitFunc->GetParameter(7), fitFunc->GetParameter(8), fitFunc->GetParameter(9));
			funcSingleGauss->Draw("same");

		}

		// Save fit to PDF.
		cvs->SaveAs(Form("./%s/Sp%i/BackgroundDecomposition_%s_PtBin%02i.pdf", fPlotDir.Data(), species, hist->GetName(), pTBin));
		cvs->Clear();

		// Save fit with the uncertainty bar.
		// fitFunc->Print();
		TGraphErrors* errorBandFitFunc = AliMiscToolsDiHadronPID::GetNSigmaBand(fitFunc);
		if (signalHist) {
			signalHist->Draw();
			hist->Draw("same");
		} else {
			hist->Draw();
		}

		fitFunc->Draw("same");	
		errorBandFitFunc->Draw("3");
		cvs->SaveAs(Form("./%s/Sp%i/BackgroundDecompositionErrorBand_%s_PtBin%02i.pdf", fPlotDir.Data(), species, hist->GetName(), pTBin));
		cvs->Clear();


		if (funcConst) {delete funcConst;}
		if (funcV1D) {delete funcV1D;}
		if (funcV2D) {delete funcV2D;}
		if (funcV3D) {delete funcV3D;}
		if (funcV4D) {delete funcV4D;}
		if (funcSingleGauss) {delete funcSingleGauss;}

	}

	return fitFunc;

}

// -----------------------------------------------------------------------
TGraphErrors* AliRatiosDiHadronPID::DCTDPhiFlow(AliSettingsDiHadronPID::BeamType beamType, AliSettingsDiHadronPID::Species species, Int_t pTBin, 
	const TH1* bgdHist, Color_t mainColor, Double_t& background, Double_t& backgroundError, Int_t minOrder, Int_t maxOrder, Bool_t saveFitToPDF) const {

	// Helper method for the DPhi fit. It was designed to perform a DCT on the background. As described
	// in the analysis note, the average yield is equal to X_0/N, while the higher Vn components
	// are equal to X_n/X_0.
	
	// Crate a mirrored histogram for the DCT.
	TH1F* symmBgd = 0x0;
	if (beamType == AliSettingsDiHadronPID::kPbPb) {
		symmBgd = AliHistToolsDiHadronPID::CreateSymmetrisedHist(Form("test_sp%i_pt%i", species, pTBin), (const TH1F*)bgdHist);
	} else if (beamType == AliSettingsDiHadronPID::kPPb) {
		
		// Not very efficient to create a new histogram instance...
		const TH1F* nearSideBgd = AliHistToolsDiHadronPID::TrimHisto((const TH1F*)bgdHist, 1, bgdHist->GetXaxis()->FindBin(TMath::Pi()/2. - 0.0001));
		symmBgd = AliHistToolsDiHadronPID::ShiftHistogram(nearSideBgd, nearSideBgd->GetXaxis()->FindBin(0.0001));
		delete nearSideBgd;
	} else {
		AliLogDiHadronPID::Error(Form("Cannot use DCT on pp data."), __func__, ClassName());
		return 0x0;
	}

	TH1F* DCT = AliHistToolsDiHadronPID::DCT(Form("DCT_sp%i_pt%i", species, pTBin), symmBgd, 2);
	
	// Measure the background level using the DCT.
	background = DCT->GetBinContent(1) / bgdHist->GetNbinsX();
	backgroundError = DCT->GetBinError(1) / bgdHist->GetNbinsX();

	// Create a function and error band for the mirorred result.
	TF1* measuredFlowMirror = AliHistToolsDiHadronPID::CreateFlowFuncFromDCT("test", symmBgd, DCT, minOrder, maxOrder);
	TGraphErrors* errorBandFlowMirror = AliHistToolsDiHadronPID::CreateFlowFuncErrorsFromDCT(symmBgd, symmBgd, minOrder, maxOrder);

	TF1* measuredFlow = AliHistToolsDiHadronPID::CreateFlowFuncFromDCT("test", bgdHist, DCT, minOrder, maxOrder);
	TGraphErrors* errorBandFlow = AliHistToolsDiHadronPID::CreateFlowFuncErrorsFromDCT(bgdHist, symmBgd, minOrder, maxOrder);

	// In case of p-Pb, the symmetrised histogram is not double the data, i.e., the results need to be 
	// Scaled by 2.
	if (beamType == AliSettingsDiHadronPID::kPPb) {
		measuredFlow->SetParameter(0, measuredFlow->GetParameter(0) * 2.);
		AliMiscToolsDiHadronPID::ScaleGraph(errorBandFlow, 2.);
	}

	if (saveFitToPDF) {

		TCanvas* cvs = TCanvas::MakeDefCanvas();

		// Draw the background plus the DCT transform.
		// TF1* measuredFlow = AliHistToolsDiHadronPID::CreateFlowFuncFromDCT("test", bgdHist, DCT, maxOrder);
		// TGraphErrors* errorBandFlow = AliHistToolsDiHadronPID::CreateFlowFuncErrorsFromDCT(bgdHist, symmBgd, maxOrder);
	
		bgdHist->DrawCopy(); // Get the axis right.
		errorBandFlow->Draw("3");
		bgdHist->DrawCopy("same");
		measuredFlow->Draw("same");	
		cvs->SaveAs(Form("./%s/Sp%i/BgdDCT_PtBin%02i.pdf", fPlotDir.Data(), species, pTBin));
		cvs->Clear();

		// Draw the brackground symmetrised, plus the DCT transform.
		
		symmBgd->Draw(); // Get the axis right.
		errorBandFlowMirror->Draw("3");
		symmBgd->Draw("same");
		measuredFlowMirror->Draw("same");	
		cvs->SaveAs(Form("./%s/Sp%i/BgdSymmDCT_PtBin%02i.pdf", fPlotDir.Data(), species, pTBin));
		cvs->Clear();

		// Draw DCT transform coefficients.
		DCT->Draw();
		DCT->GetXaxis()->SetRange(2, DCT->GetNbinsX());
		cvs->SaveAs(Form("./%s/Sp%i/DCT_PtBin%02i.pdf", fPlotDir.Data(), species, pTBin));
		cvs->Clear();

		DCT->Scale(1. / DCT->GetBinContent(1));

		// Draw Scaled DCT transform coefficients.
		DCT->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/DCTScaled_PtBin%02i.pdf", fPlotDir.Data(), species, pTBin));
		cvs->Clear();

	}

	delete measuredFlowMirror;
	delete errorBandFlowMirror;
	delete measuredFlow;

	return errorBandFlow;

}

// -----------------------------------------------------------------------
TF1* AliRatiosDiHadronPID::FitDPhiNSPeak(AliSettingsDiHadronPID::Species species, Int_t pTBin, TH1* signalHist, TH1* bgdHist, const TF1* initialFunc, DPhiBackgroundModel model, Bool_t useDoubleGauss, Color_t /*mainColor*/, Bool_t saveFitToPDF) const {

	// Helper method for the DPhi fit. It assumes that the fit parameters of the background 
	// are already fitted properly in the initialFit.
	//
	// This method creates a clone of the initial fit function, releases parameters for the near side peak,
	// fixes 

	cout << endl << "FITTING NS PEAK ***" << endl;

	TF1* fitFunc = new TF1(*initialFunc);
			
	// Get a reasonable estimate for the yield under the NS peak. (TODO: Can the counting method be used?)
	Double_t tmp = 0.;
	Double_t histIntegral = signalHist->IntegralAndError(signalHist->FindBin(-TMath::Pi()/2. + 0.001), signalHist->FindBin(TMath::Pi()/2. - 0.001), tmp, "width");
	// Double_t flowHistIntegral = bgdHist->IntegralAndError(signalHist->FindBin(-TMath::Pi()/2. + 0.001), signalHist->FindBin(TMath::Pi()/2. - 0.001), tmp, "width");
	Double_t flowFitIntegral = fitFunc->Integral(signalHist->GetBinLowEdge(signalHist->FindBin(-TMath::Pi()/2. + 0.001)), signalHist->GetBinLowEdge(signalHist->FindBin(TMath::Pi()/2. - 0.001) + 1));

	cout << " NS Yield Estimate: " << histIntegral << " - " << flowFitIntegral << " (flow fit) = " << histIntegral - flowFitIntegral << endl;
	// cout << " NS Yield Estimate: " << histIntegral << " - " << flowHistIntegral << " (flow hist) = " << histIntegral - flowHistIntegral << endl;

	// Sloppy fix for PbPb
	if (histIntegral - flowFitIntegral < 0) {
		histIntegral = signalHist->IntegralAndError(signalHist->FindBin(-0.5 + 0.001), signalHist->FindBin(0.5 - 0.001), tmp, "width");
		flowFitIntegral = fitFunc->Integral(signalHist->GetBinLowEdge(signalHist->FindBin(-0.5 + 0.001)), signalHist->GetBinLowEdge(signalHist->FindBin(0.5 - 0.001) + 1));
	}

	// Fix the background parameter.
	fitFunc->FixParameter(0, fitFunc->GetParameter(0));

	// Fix the AS peak parameters.
	fitFunc->FixParameter(7, fitFunc->GetParameter(7));
	fitFunc->FixParameter(9, fitFunc->GetParameter(9));

	// Fix the flow parameters.
	fitFunc->FixParameter(10, fitFunc->GetParameter(10));
	fitFunc->FixParameter(11, fitFunc->GetParameter(11));
	fitFunc->FixParameter(12, fitFunc->GetParameter(12));
	fitFunc->FixParameter(13, fitFunc->GetParameter(13));			

	// Release the NS peak parameters, but fix the f parameter to 1.;
	fitFunc->ReleaseParameter(1);
	fitFunc->SetParameter(1, histIntegral - flowFitIntegral);
	
	fitFunc->FixParameter(2, 1.);

	fitFunc->SetParLimits(4, 0.1, 1.0);
	fitFunc->SetParameter(4, 0.3);

	// Fit SG NS peak on top of the previously determined background.
	signalHist->Fit(fitFunc, "RQI0");

	// Release more parameters... Should think about this, since we actually want to rely mostly on the 
	// counts. I.e., the fit is just there to compare with the counts, and possibly for a peak width measurement.

	// PROBLEM! Counts at large DEta are not really the background since there's still part of the peak left...
	// We will have to release all parameters actually.

	// Release background level (should become somewhat lower after including the NS peak)

	// if (model == kWithFixed_V1_V2_V3_V4) {
	// 	fitFunc->ReleaseParameter(0);
	// 	fitFunc->ReleaseParameter(10);
	// 	fitFunc->ReleaseParameter(11);
	// 	fitFunc->ReleaseParameter(12);
	// 	fitFunc->ReleaseParameter(13);	
	// 	signalHist->Fit(fitFunc, "RQI0");
	// 	fitFunc->FixParameter(0, fitFunc->GetParameter(0));
	// 	fitFunc->FixParameter(10, fitFunc->GetParameter(10));
	// 	fitFunc->FixParameter(11, fitFunc->GetParameter(11));
	// 	fitFunc->FixParameter(12, fitFunc->GetParameter(12));
	// 	fitFunc->FixParameter(13, fitFunc->GetParameter(13));	
	// }

	// Release AS peak if it was part of the background model.
	if (model == kNoFlow_AS || model == kWithFixed_V2_AS || model == kWithFixed_V2_V3_AS) {
		fitFunc->ReleaseParameter(0);
		fitFunc->ReleaseParameter(7);
		fitFunc->ReleaseParameter(9);
		signalHist->Fit(fitFunc, "RQI0");

	}


	if (useDoubleGauss) {

		// Start by getting an estimate of the ratio, but keep the yield in the NS peak constant.
		// fitFunc->FixParameter(1, fitFunc->GetParameter(1)); 	// Total Jet Yield of NS

		// Fix the background again.
		fitFunc->FixParameter(0, fitFunc->GetParameter(0));

		// Release ratio.
		fitFunc->ReleaseParameter(2);							// Fraction NS1 vs NS2
		fitFunc->SetParameter(2, 0.9);  						

		fitFunc->SetParLimits(4, 0.1, 1.0);						// Limit of the NS1 width
		fitFunc->SetParameter(6, 0.6);
		fitFunc->SetParLimits(6, fitFunc->GetParameter(4), 2.0);// Limit the NS2 width
		
		signalHist->Fit(fitFunc, "RQI0");

		// Release background (not for PbPb!).
		if (model == kNoFlow_AS || model == kWithFixed_V2_AS || model == kWithFixed_V2_V3_AS) {
			fitFunc->ReleaseParameter(0);
			signalHist->Fit(fitFunc, "RQI0");

		}
	}

	// Save fit to PDF.
	if (saveFitToPDF) {

		TCanvas* cvs = TCanvas::MakeDefCanvas();

		signalHist->DrawClone();
		bgdHist->DrawClone("same");
		TList* lof = bgdHist->GetListOfFunctions();
		if (lof->At(0)) {
			TF1* bgdFit = ((TF1*)lof->At(0));
			bgdFit->SetLineStyle(2);
			bgdFit->Draw("same");
		}
		fitFunc->Draw("same");

		cvs->SaveAs(Form("./%s/Sp%i/TotalFit_%s_%s_PtBin%02i.pdf", fPlotDir.Data(), species, bgdHist->GetName(), (useDoubleGauss ? "DG" : "SG"), pTBin));
		cvs->Clear();

	}

	return fitFunc;

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::GetDPhiJetYieldAndErrorByCounting(const TH1* histSignal, const TObject* background, Double_t& yield, Double_t& error) const {

	// This method takes a signal histogram, and an object estimating the background (TH1* or TF1*).
	// It sets the parameters yield and error.

	// Find the bin range for the near-side (protection against bin edges exactly at +/- pi/2).
/* #ifdef __CLING__
	const TAxis* axis = histSignal->GetXaxis();
#else 
	TAxis* axis = histSignal->GetXaxis();
#endif */
	Int_t integralMinBin = histSignal->GetXaxis()->FindBin(-TMath::Pi()/2. + 0.001);
	Int_t integralMaxBin = histSignal->GetXaxis()->FindBin(TMath::Pi()/2. - 0.001);
	Double_t integralMin = histSignal->GetXaxis()->GetBinLowEdge(integralMinBin);
	Double_t integralMax = histSignal->GetXaxis()->GetBinLowEdge(integralMaxBin + 1);
	AliLogDiHadronPID::Info(Form("Bin range: (%i,%i), corresponding real range: (%4.2f,%4.2f)", integralMinBin, integralMaxBin, integralMin, integralMax), __func__, ClassName());
	
	// Calculate the near-Side integral under the histogram.
	Double_t signalIntegralError = 0.;
	Double_t signalIntegral = histSignal->IntegralAndError(integralMinBin, integralMaxBin, signalIntegralError, "width");
	
	Double_t bgdIntegralError = 0.;
	Double_t bgdIntegral = 0.;

	// Check the identity of the background object.
	TString className = TString(background->ClassName());

	if (className.Contains("TF1")) {

		bgdIntegral = ((TF1*)background)->Integral(integralMin, integralMax);
		bgdIntegralError = ((TF1*)background)->IntegralError(integralMin, integralMax);
	
	} else if (className.Contains("TH1")) {

		bgdIntegral = ((TH1*)background)->IntegralAndError(integralMinBin, integralMaxBin, bgdIntegralError, "width");
	
	} else {
		AliLogDiHadronPID::Error(Form("Background object is not of type TH1 or TF1."), __func__, ClassName());
		yield = 0.;
		error = 0.;
		return;
	}

	yield = signalIntegral - bgdIntegral;
	error = TMath::Sqrt(signalIntegralError * signalIntegralError + bgdIntegralError * bgdIntegralError);

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::GetJetYieldAndErrorByCountingInJetRegion(const TH2* histSignal, const TObject* background, Double_t& yield, Double_t& error, Double_t maxDEta, Double_t DPhiJet, Double_t DEtaJet) const {

	// Measure the jet yield by counting the yield in the "peak region" circle,
	// and subtract the integral of the background in that same region. We assume that

	// For now, ignore the stat. uncertainty due to the flow coefficients and background.
	if (maxDEta < DEtaJet) {
		AliLogDiHadronPID::Error(Form("maxDEta needs to be larger than DEtaJet."), __func__, ClassName());
		return;
	}

/* #ifdef __CLING__
	const TAxis* aDPhi = histSignal->GetXaxis();
	const TAxis* aDEta = histSignal->GetYaxis();
#else
	TAxis* aDPhi = histSignal->GetXaxis();
	TAxis* aDEta = histSignal->GetYaxis();
#endif */
	Int_t minBinDEta =histSignal->GetYaxis()->FindBin(-maxDEta + 0.001);
	Int_t maxBinDEta =histSignal->GetYaxis()->FindBin(maxDEta - 0.001);
	Double_t nBinsDEta = maxBinDEta;
	nBinsDEta -= minBinDEta;
	nBinsDEta++;

	TH1F* hCircleCutOutDPhi = new TH1F(Form("%s_proj_minusJet", histSignal->GetName()), histSignal->GetTitle(), histSignal->GetXaxis()->GetNbins(), histSignal->GetXaxis()->GetXmin(), histSignal->GetXaxis()->GetXmax());
	hCircleCutOutDPhi->GetXaxis()->SetTitle(histSignal->GetXaxis()->GetTitle());
	hCircleCutOutDPhi->SetDirectory(0);

	// For the moment we assume that the background is described by flow!
	Double_t YieldCircle = 0.;
	Double_t YieldCircleError2 = 0.;	

	Double_t YieldBackgroundCircle = 0.;
	Double_t YieldBackgroundCirclePlusOneSigma = 0.;
	Double_t YieldBackgroundCircleMinusOneSigma = 0.;

	// Determine the type of background object.
	TGraphErrors* backgroundGraph = 0x0;
	TF1* backgroundFunc = 0x0;
	TString className = TString(background->ClassName());
	if (className.Contains("TGraphErrors")) {
		backgroundGraph = (TGraphErrors*)background;
	} else if (className.Contains("TF1")) {
		backgroundFunc = (TF1*)background;
	} else {
		AliLogDiHadronPID::Error(Form("Background type not supported."), __func__, ClassName());
	}

	// Count the entries in the peak region.
	for (Int_t iDPhiBin = 1; iDPhiBin <= histSignal->GetXaxis()->GetNbins(); iDPhiBin++) {

		Double_t dPhiBinCenter = histSignal->GetXaxis()->GetBinCenter(iDPhiBin);
		Double_t dPhiScaled2 = TMath::Power(dPhiBinCenter / DPhiJet, 2);
		for (Int_t iDEtaBin = minBinDEta; iDEtaBin <= maxBinDEta; iDEtaBin++) {

			Double_t dEtaBinCenter =histSignal->GetYaxis()->GetBinCenter(iDEtaBin);
			Double_t dEtaScaled2 = TMath::Power(dEtaBinCenter / DEtaJet, 2);

			// If the bin is in the 
			if ((dPhiScaled2 + dEtaScaled2) < 1.) {

				// Add yield to the circle yield.
				YieldCircle += histSignal->GetBinContent(iDPhiBin, iDEtaBin);
				YieldCircleError2 += (histSignal->GetBinError(iDPhiBin, iDEtaBin) * histSignal->GetBinError(iDPhiBin, iDEtaBin));
				
				// Bgd yield must be divided through the number of included bins, and multiplied
				// by the bin width in DPhi
				
				// If the background is described by a graph.
				if (backgroundGraph) {

					Double_t bgdYield = backgroundGraph->Eval(dEtaBinCenter) / nBinsDEta * (histSignal->GetXaxis()->GetBinWidth(1));
					YieldBackgroundCircle += bgdYield;

					// Determine the yield plus one sigma.
					Int_t closestPoint = AliMiscToolsDiHadronPID::FindClosestPoint(backgroundGraph, dEtaBinCenter);
					Double_t bgdYieldOneSigma = backgroundGraph->GetEY()[closestPoint] / nBinsDEta * (histSignal->GetXaxis()->GetBinWidth(1));

					YieldBackgroundCirclePlusOneSigma += (bgdYield + bgdYieldOneSigma);
					YieldBackgroundCircleMinusOneSigma += (bgdYield - bgdYieldOneSigma);

				} else if (backgroundFunc) {
	
					// Background is described by a TF1.
					Double_t bgdYield = backgroundFunc->Eval(dEtaBinCenter) / nBinsDEta * (histSignal->GetXaxis()->GetBinWidth(1));
					YieldBackgroundCircle += bgdYield;

					// TODO: Determine 1 sigma deviation.
					Double_t bgdYieldOneSigma = backgroundFunc->GetParError(0) / nBinsDEta * (histSignal->GetXaxis()->GetBinWidth(1));

					YieldBackgroundCirclePlusOneSigma += (bgdYield + bgdYieldOneSigma);
					YieldBackgroundCircleMinusOneSigma += (bgdYield - bgdYieldOneSigma);
				}
			} 
		}	
	}

	yield = YieldCircle - YieldBackgroundCircle;

	// Calculate the total error on the yield (yield and background).
	Double_t YieldBackgroundCircleError2 = (YieldBackgroundCirclePlusOneSigma - YieldBackgroundCircle) * (YieldBackgroundCirclePlusOneSigma - YieldBackgroundCircle);
	error = TMath::Sqrt(YieldCircleError2 + YieldBackgroundCircleError2);

	cout << "Jet Yield circle count: " << yield << " +/- " << error << "(Circle: +/- " << TMath::Sqrt(YieldCircleError2) <<  ", Bgd: +/- " << TMath::Sqrt(YieldBackgroundCircleError2) << ")" << endl;

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::EstimateBackgroundDEta(TH1* hist, Double_t bulkMinAbsDEta, Double_t& yield, Double_t& error) const {

	// Make an estimate of the background by fitting points at large DEta with a constant.
	Double_t values[hist->GetNbinsX()];
	Double_t valerrors[hist->GetNbinsX()];
	for (Int_t iBinDEta = 1; iBinDEta <= hist->GetNbinsX(); iBinDEta++) {
		values[iBinDEta - 1] = 0.;
		valerrors[iBinDEta - 1] = 0.;
	} 

	Int_t nBinsSideBand = 0;
	Int_t nBinsPeakEst = 0;
	Double_t peakYieldEst = 0.;
	for (Int_t iBinDEta = 2; iBinDEta <= hist->GetNbinsX() - 1; iBinDEta++) {

		Double_t center = hist->GetBinCenter(iBinDEta);
		if (center < -bulkMinAbsDEta || center > bulkMinAbsDEta) {
			values[nBinsSideBand] = hist->GetBinContent(iBinDEta);
			valerrors[nBinsSideBand] = hist->GetBinError(iBinDEta);
			nBinsSideBand++;
		} else {
			peakYieldEst += hist->GetBinContent(iBinDEta);
			nBinsPeakEst++;
		}

	}

	AliFunctionsDiHadronPID::ConstFit(values, valerrors, nBinsSideBand, yield, error);

}

// -----------------------------------------------------------------------
TF1* AliRatiosDiHadronPID::FitDEtaPeak(AliSettingsDiHadronPID::Species species, Int_t pTBin, TH1* signalHist, TH1* bgdHist, const TF1* initialFunc, Bool_t useDoubleGauss, Color_t /*mainColor*/, Bool_t saveFitToPDF) const {

	// Estimate the background for the signal histogram.
	Double_t bgdEstSignal = 0.;
	Double_t bgdEstSignalErr = 0.;
	EstimateBackgroundDEta(signalHist, 1.0, bgdEstSignal, bgdEstSignalErr);

	TCanvas* cvs = 0x0;
	if(saveFitToPDF) {cvs = TCanvas::MakeDefCanvas();}

	// If a background histogram is provided (likely the AS region), then fit it with c + x^2.
	Double_t a4Est = 0.;
	if (bgdHist) {
		cout << " DETERMINING a4 coefficient! " << endl;
		TF1* fitFuncA2 = new TF1(*initialFunc);
		fitFuncA2->SetParameter(0, bgdEstSignal);
		fitFuncA2->ReleaseParameter(1);
		fitFuncA2->SetParameter(1, 0.001); // a4
		fitFuncA2->FixParameter(2, 0.0);   // Peak Yield
		fitFuncA2->FixParameter(3, 1.0);   // Fraction
		fitFuncA2->FixParameter(5, 0.3);   // Sigma,1
		fitFuncA2->FixParameter(7, 0.6);   // Sigma,2

		bgdHist->Fit(fitFuncA2, "RQI0");

		a4Est = fitFuncA2->GetParameter(1);

		if (cvs) {

			cvs->cd();

			bgdHist->Draw();
			fitFuncA2->Draw("same");

			cvs->SaveAs(Form("./%s/Sp%i/BackgroundDEta_%s_%s_PtBin%02i.pdf", fPlotDir.Data(), species, signalHist->GetName(), (useDoubleGauss ? "DG" : "SG"), pTBin));
			cvs->Clear();

		}

	}

	cout << "Estimated a4 = " << a4Est << endl;

	// Make an estimate of the peak yield by integrating the histogram.
	Double_t tmp = 0.;
	Double_t histIntegral = signalHist->IntegralAndError(2, signalHist->GetNbinsX() - 1, tmp, "width");
	Double_t constFitIntegral = bgdEstSignal * signalHist->GetBinWidth(1) * (Double_t)(signalHist->GetNbinsX() - 2);
	cout << " DEta Jet Yield Estimate: " << histIntegral << " - " << constFitIntegral << " = " << histIntegral - constFitIntegral << endl;

	// Create a new fit function, and initialise parameters.
	TF1* fitFunc = new TF1(*initialFunc);
	fitFunc->SetParameter(0, bgdEstSignal);

	if (pTBin < 8) {
		// fitFunc->ReleaseParameter(1);
		fitFunc->SetParameter(1, a4Est);
	}

	fitFunc->SetParameter(2, histIntegral - constFitIntegral); // Total yield
	fitFunc->FixParameter(3, 1.); // Fraction of the SG.
	fitFunc->SetParameter(5, 0.4); // Sigma,1
	fitFunc->FixParameter(7, 0.6); // Sigma,2

	signalHist->Fit(fitFunc, "RQI0");

	if (useDoubleGauss) {

		// Fix the a4 parameter.
		fitFunc->FixParameter(1, fitFunc->GetParameter(1));

		// Set the fraction of the two Gaussians to 0.9.
		fitFunc->ReleaseParameter(3);
		fitFunc->SetParameter(3, 0.9);

		signalHist->Fit(fitFunc, "RQI0");

		// Release the sigma of the second Gaussian.
		fitFunc->ReleaseParameter(7);

		signalHist->Fit(fitFunc, "RQI0");

	}

	if(cvs) {

		cvs->cd();

		signalHist->Draw();
		fitFunc->Draw("same");

		cvs->SaveAs(Form("./%s/Sp%i/TotalFitDEta_%s_%s_PtBin%02i.pdf", fPlotDir.Data(), species, signalHist->GetName(), (useDoubleGauss ? "DG" : "SG"), pTBin));
		cvs->Clear();

	}

	return fitFunc;

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::FitDPhiWithGauss(AliSettingsDiHadronPID::Species species, Int_t pTBin, Double_t, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta, Double_t* parameters, Double_t* errors, Bool_t saveFitToPDF) const {

	// This method takes the result from the previous method, but now it subtracts the background from
	// the peak yield, and tries to fit the remaining Gaussian.
	// Parameters: 0 = peak yield, 1 = bulk yield, 2 = peak width
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());
	
	// Get a clone of the histogram.
	TH2F* correctedDiHadron = GetCorrectedClone(species, pTBin);
/* #ifdef __CLING__
	const TAxis* aDEta = correctedDiHadron->GetYaxis();
	const TAxis* aDPhi = correctedDiHadron->GetXaxis();
#else
	TAxis* aDEta = correctedDiHadron->GetYaxis();
	TAxis* aDPhi = correctedDiHadron->GetXaxis();
#endif */

	// Determine the bin range in DEta for the side band.
	const Int_t BulkMinBin1 = 2;
	const Int_t BulkMaxBin1 = correctedDiHadron->GetYaxis()->FindBin(-BulkMinAbsDEta);
	const Int_t BulkMinBin2 = correctedDiHadron->GetYaxis()->FindBin(BulkMinAbsDEta);
	const Int_t BulkMaxBin2 = correctedDiHadron->GetYaxis()->GetNbins() - 1;

	const Int_t JetMinBin = correctedDiHadron->GetYaxis()->FindBin(-PeakMaxDEta);
	const Int_t JetMaxBin = correctedDiHadron->GetYaxis()->FindBin(PeakMaxDEta);
	const Int_t nBinsJet = JetMaxBin - JetMinBin + 1;

	// Check the ranges for overlap.
	if (BulkMaxBin1 >= JetMinBin || BulkMinBin2 <= JetMaxBin) {AliLogDiHadronPID::Warning(Form("Jet and Bulk region overlap!"), __func__, ClassName());}
	AliLogDiHadronPID::Info(Form("Determined range: B(%i,%i) - J(%i,%i) - B(%i,%i).", BulkMinBin1, BulkMaxBin1, JetMinBin, JetMaxBin, BulkMinBin2, BulkMaxBin2), __func__, ClassName());

	// Create output histograms.
	TH1F* hJet = (TH1F*)correctedDiHadron->ProjectionX(Form("jet_%s_ptBin%i", fSettings->GetSpeciesName(species), pTBin), JetMinBin, JetMaxBin); 
	hJet->SetTitle(Form("%s Jet (pt bin: %i)", fSettings->GetSpeciesName(species), pTBin));
	hJet->SetDirectory(0);

	TH1F* hSideBand = GetHistBulkFitWithConst(species, pTBin, BulkMinAbsDEta);
	hSideBand->Scale(nBinsJet);

	hJet->Add(hSideBand, -1.);
	
	// Create fit function (const + Gauss).
	TF1* fitFunc = new TF1("fitFunc","[0]+([1]/(sqrt(2*TMath::Pi())*[3]))*exp(-0.5*(((x-[2])/[3])**2))", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
	fitFunc->SetParNames("const", "yield", "muDphi", "sDphi");
	fitFunc->SetParameters(hJet->GetBinContent(1), 1000., 0.0, 0.3);

	// Fix the center of the Gaussian.
	// fitFunc->FixParameter(0,0.);
	fitFunc->FixParameter(2,0.);
	// fitFunc->SetParLimits(3,0.,3.);

	// Create fit function (const)
	TF1* constFunc = new TF1("constFunc", "[0]", TMath::Pi()/2., 3.*TMath::Pi()/2.);
	constFunc->SetParNames("const");
	constFunc->SetParameter(0, 0.);
	constFunc->SetLineColor(kBlack);

	if (saveFitToPDF) {

		TCanvas* cvs = TCanvas::MakeDefCanvas();
		AliMiscToolsDiHadronPID::CreateDir(Form("./%s/Sp%i", fPlotDir.Data(), species));			
		hJet->Fit(fitFunc, "RQI");
		hJet->Fit(constFunc, "RQI+");

		TLatex* lDEtaRangeBulk = new TLatex(0.45, 0.8, Form("%4.2f < |#Delta#eta_{bulk}| < %4.2f", correctedDiHadron->GetYaxis()->GetBinLowEdge(BulkMinBin2), correctedDiHadron->GetYaxis()->GetBinUpEdge(BulkMaxBin2)));
		lDEtaRangeBulk->SetNDC();
		lDEtaRangeBulk->Draw();

		TLatex* lDEtaRangeJet = new TLatex(0.45, 0.72, Form("%4.2f < #Delta#eta_{peak} < %4.2f", correctedDiHadron->GetYaxis()->GetBinLowEdge(JetMinBin), correctedDiHadron->GetYaxis()->GetBinUpEdge(JetMaxBin)));
		lDEtaRangeJet->SetNDC();
		lDEtaRangeJet->Draw();

		cvs->SaveAs(Form("./%s/Sp%i/PtBin%02i.pdf", fPlotDir.Data(), species, pTBin));
		delete cvs;

		delete lDEtaRangeJet;
		delete lDEtaRangeBulk;

	} else {
		hJet->Fit(fitFunc, "RQI0");
	}

	// Rescale peak yield with bin width so that they are counts.
	parameters[0] = fitFunc->GetParameter(1) / hJet->GetBinWidth(1);
	errors[0] = fitFunc->GetParError(1) / hJet->GetBinWidth(1);

	// For pp we assume a flat background (chi2 fit), while for pPb and PbPb we add up the whole near side.
	// FIXME: CODE COPY FROM AliRatiosDiHadronPID::FitBulkWithConst
	if (fSettings->GetBeamType() == 2) {
		AliLogDiHadronPID::Info(Form("Assuming flat background (chi^2 fit)."), __func__, ClassName());
		
		Double_t values[correctedDiHadron->GetXaxis()->GetNbins()];
		Double_t valerrors[correctedDiHadron->GetXaxis()->GetNbins()];
		for (Int_t iDPhiBin = 1; iDPhiBin <= correctedDiHadron->GetXaxis()->GetNbins(); iDPhiBin++) { 
			values[iDPhiBin - 1] = hSideBand->GetBinContent(iDPhiBin);
			valerrors[iDPhiBin - 1] = hSideBand->GetBinError(iDPhiBin);
		}

		AliFunctionsDiHadronPID::ConstFit(values, valerrors, correctedDiHadron->GetXaxis()->GetNbins(), parameters[1], errors[1]);


	} else {

		Double_t bulkYield = 0.;
		Double_t bulkErr2 = 0.;
		
		Double_t nBinsInLoop = 0.;		
		for (Int_t iDPhiBin = 1; iDPhiBin <= correctedDiHadron->GetXaxis()->GetNbins(); iDPhiBin++) {
			bulkYield += hSideBand->GetBinContent(iDPhiBin);
			bulkErr2 += (hSideBand->GetBinError(iDPhiBin) * hSideBand->GetBinError(iDPhiBin));
			nBinsInLoop++;
		}

		parameters[1] = bulkYield;
		errors[1] = TMath::Sqrt(bulkErr2);

		// Calculate the average yield per bin.
		parameters[1] /= (nBinsInLoop * (Double_t)nBinsJet);
		errors[1] /= (nBinsInLoop * (Double_t)nBinsJet);

	}

	// For the background we return the yield per unit DEta,DPhi
	parameters[1] /= (correctedDiHadron->GetXaxis()->GetBinWidth(1) * correctedDiHadron->GetYaxis()->GetBinWidth(1));
	errors[1] /= (correctedDiHadron->GetXaxis()->GetBinWidth(1) * correctedDiHadron->GetYaxis()->GetBinWidth(1));

	parameters[2] = fitFunc->GetParameter(3);
	errors[2] = fitFunc->GetParError(3);

	delete hSideBand;
	delete hJet;
	delete correctedDiHadron;
	delete fitFunc;

}
// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::FitDPhiWithDoubleGauss(AliSettingsDiHadronPID::Species species, Int_t pTBin, Double_t, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta, Double_t* parameters, Double_t* errors, Bool_t saveFitToPDF) const {

	// This method takes the result from the previous method, but now it subtracts the background from
	// the peak yield, and tries to fit the remaining Gaussian.
	// Parameters: 0 = peak yield, 1 = bulk yield, 2 = peak width
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());
	
	// Get a clone of the histogram.
	TH2F* correctedDiHadron = GetCorrectedClone(species, pTBin);
/* #ifdef __CLING__
	const TAxis* aDEta = correctedDiHadron->GetYaxis();
	const TAxis* aDPhi = correctedDiHadron->GetXaxis();
#else
	TAxis* aDEta = correctedDiHadron->GetYaxis();
	TAxis* aDPhi = correctedDiHadron->GetXaxis();
#endif */

	// Determine the bin range in DEta for the side band.
	const Int_t BulkMinBin1 = 2;
	const Int_t BulkMaxBin1 = correctedDiHadron->GetYaxis()->FindBin(-BulkMinAbsDEta);
	const Int_t BulkMinBin2 = correctedDiHadron->GetYaxis()->FindBin(BulkMinAbsDEta);
	const Int_t BulkMaxBin2 = correctedDiHadron->GetYaxis()->GetNbins() - 1;

	const Int_t JetMinBin = correctedDiHadron->GetYaxis()->FindBin(-PeakMaxDEta);
	const Int_t JetMaxBin = correctedDiHadron->GetYaxis()->FindBin(PeakMaxDEta);
	const Int_t nBinsJet = JetMaxBin - JetMinBin + 1;

	// Check the ranges for overlap.
	if (BulkMaxBin1 >= JetMinBin || BulkMinBin2 <= JetMaxBin) {AliLogDiHadronPID::Warning(Form("Jet and Bulk region overlap!"), __func__, ClassName());}
	AliLogDiHadronPID::Info(Form("Determined range: B(%i,%i) - J(%i,%i) - B(%i,%i).", BulkMinBin1, BulkMaxBin1, JetMinBin, JetMaxBin, BulkMinBin2, BulkMaxBin2), __func__, ClassName());

	// Create output histograms.
	TH1F* hJet = (TH1F*)correctedDiHadron->ProjectionX(Form("jet_%s_ptBin%i", fSettings->GetSpeciesName(species), pTBin), JetMinBin, JetMaxBin); 
	hJet->SetTitle(Form("%s Jet (pt bin: %i)", fSettings->GetSpeciesName(species), pTBin));
	hJet->SetDirectory(0);

	TH1F* hSideBand = GetHistBulkFitWithConst(species, pTBin, BulkMinAbsDEta);
	hSideBand->Scale(nBinsJet);

	hJet->Add(hSideBand, -1.);
	
	// Create fit function (const + Gauss).
	TF1* fitFunc = new TF1("fitFunc","[0]+([1]/(sqrt(2*TMath::Pi())*[3]))*exp(-0.5*(((x-[2])/[3])**2))+([4]/(sqrt(2*TMath::Pi())*[6]))*exp(-0.5*(((x-[5])/[6])**2))", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
	fitFunc->SetParNames("const", "yield1", "muDphi1", "sDphi1", "yield1", "muDphi1", "sDphi1");
	fitFunc->SetParameters(hJet->GetBinContent(1), 1000., 0.0, 0.3, 0.0, 0.0, 0.6);

	// Fix the center of the Gaussian.
	fitFunc->FixParameter(2,0.0);
	fitFunc->FixParameter(4,0.0);
	fitFunc->FixParameter(5,0.0);
	fitFunc->FixParameter(6,0.6);

	// Create fit function (const)
	TF1* constFunc = new TF1("constFunc", "[0]", TMath::Pi()/2., 3.*TMath::Pi()/2.);
	constFunc->SetParNames("const");
	constFunc->SetParameter(0, 0.);
	constFunc->SetLineColor(kBlack);

	TF1* fitFunc2 = 0x0;
	TF1* fitFunc3 = 0x0;

	if (saveFitToPDF) {

		TCanvas* cvs = TCanvas::MakeDefCanvas();
		AliMiscToolsDiHadronPID::CreateDir(Form("./%s/Sp%i", fPlotDir.Data(), species));			
		
		hJet->Fit(fitFunc, "RQI");
		
		fitFunc2 = new TF1(*fitFunc);
		fitFunc2->FixParameter(0, fitFunc->GetParameter(0));
		fitFunc2->FixParameter(1, fitFunc->GetParameter(1));
		fitFunc2->FixParameter(2, fitFunc->GetParameter(2));
		fitFunc2->FixParameter(3, fitFunc->GetParameter(3));
		fitFunc2->ReleaseParameter(4);
		fitFunc2->SetParameter(4, fitFunc->GetParameter(1) / 10.);
		fitFunc2->ReleaseParameter(6);
		fitFunc2->SetLineColor(kBlue);
		hJet->Fit(fitFunc2,"RQI+");

		fitFunc3 = new TF1(*fitFunc2);
		fitFunc3->ReleaseParameter(0);
		fitFunc3->SetParameter(0, fitFunc2->GetParameter(0));
		fitFunc3->ReleaseParameter(1);
		fitFunc3->SetParameter(1, fitFunc2->GetParameter(1));
		fitFunc3->ReleaseParameter(3);
		fitFunc3->SetParameter(3, fitFunc2->GetParameter(3));
		fitFunc3->ReleaseParameter(6);
		fitFunc3->SetLineColor(kGreen);
		hJet->Fit(fitFunc3,"RQI+");

		cout << " ------------------------------------------" << endl;
		cout << " Yield difference (DG/G): " << (fitFunc3->GetParameter(1) + fitFunc3->GetParameter(4)) / (fitFunc->GetParameter(1)) << endl;
		cout << " ------------------------------------------" << endl;
		cout << endl;
		hJet->Fit(constFunc, "RQI+");

		TLatex* lDEtaRangeBulk = new TLatex(0.45, 0.8, Form("%4.2f < |#Delta#eta_{bulk}| < %4.2f", correctedDiHadron->GetYaxis()->GetBinLowEdge(BulkMinBin2), correctedDiHadron->GetYaxis()->GetBinUpEdge(BulkMaxBin2)));
		lDEtaRangeBulk->SetNDC();
		lDEtaRangeBulk->Draw();

		TLatex* lDEtaRangeJet = new TLatex(0.45, 0.72, Form("%4.2f < #Delta#eta_{peak} < %4.2f", correctedDiHadron->GetYaxis()->GetBinLowEdge(JetMinBin), correctedDiHadron->GetYaxis()->GetBinUpEdge(JetMaxBin)));
		lDEtaRangeJet->SetNDC();
		lDEtaRangeJet->Draw();

		cvs->SaveAs(Form("./%s/Sp%i/PtBin%02i.pdf", fPlotDir.Data(), species, pTBin));
		delete cvs;

		delete lDEtaRangeJet;
		delete lDEtaRangeBulk;

	} else {
		hJet->Fit(fitFunc, "RQI0");

		fitFunc2 = new TF1(*fitFunc);
		fitFunc2->FixParameter(0, fitFunc->GetParameter(0));
		fitFunc2->FixParameter(1, fitFunc->GetParameter(1));
		fitFunc2->FixParameter(2, fitFunc->GetParameter(2));
		fitFunc2->FixParameter(3, fitFunc->GetParameter(3));
		fitFunc2->ReleaseParameter(4);
		fitFunc2->SetParameter(4, fitFunc->GetParameter(1) / 10.);
		fitFunc2->ReleaseParameter(6);
		fitFunc2->SetLineColor(kBlue);
		hJet->Fit(fitFunc2,"RQI+");

		fitFunc3 = new TF1(*fitFunc2);
		fitFunc3->ReleaseParameter(0);
		fitFunc3->SetParameter(0, fitFunc2->GetParameter(0));
		fitFunc3->ReleaseParameter(1);
		fitFunc3->SetParameter(1, fitFunc2->GetParameter(1));
		fitFunc3->ReleaseParameter(3);
		fitFunc3->SetParameter(3, fitFunc2->GetParameter(3));
		fitFunc3->ReleaseParameter(6);
		fitFunc3->SetLineColor(kGreen);
		hJet->Fit(fitFunc3,"RQI+");
	}

	// Rescale peak yield with bin width so that they are counts.
	parameters[0] = (fitFunc3->GetParameter(1) + fitFunc3->GetParameter(4)) / hJet->GetBinWidth(1);
	errors[0] = TMath::Sqrt(fitFunc3->GetParError(1) * fitFunc3->GetParError(1) + fitFunc3->GetParError(4) * fitFunc3->GetParError(4)) / hJet->GetBinWidth(1);

	// For pp we assume a flat background (chi2 fit), while for pPb and PbPb we add up the whole near side.
	// FIXME: CODE COPY FROM AliRatiosDiHadronPID::FitBulkWithConst
	if (fSettings->GetBeamType() == 2) {
		AliLogDiHadronPID::Info(Form("Assuming flat background (chi^2 fit)."), __func__, ClassName());
		
		Double_t values[correctedDiHadron->GetXaxis()->GetNbins()];
		Double_t valerrors[correctedDiHadron->GetXaxis()->GetNbins()];
		for (Int_t iDPhiBin = 1; iDPhiBin <= correctedDiHadron->GetXaxis()->GetNbins(); iDPhiBin++) { 
			values[iDPhiBin - 1] = hSideBand->GetBinContent(iDPhiBin);
			valerrors[iDPhiBin - 1] = hSideBand->GetBinError(iDPhiBin);
		}

		AliFunctionsDiHadronPID::ConstFit(values, valerrors, correctedDiHadron->GetXaxis()->GetNbins(), parameters[1], errors[1]);


	} else {

		Double_t bulkYield = 0.;
		Double_t bulkErr2 = 0.;
		
		Double_t nBinsInLoop = 0.;		
		for (Int_t iDPhiBin = 1; iDPhiBin <= correctedDiHadron->GetXaxis()->GetNbins(); iDPhiBin++) {
			bulkYield += hSideBand->GetBinContent(iDPhiBin);
			bulkErr2 += (hSideBand->GetBinError(iDPhiBin) * hSideBand->GetBinError(iDPhiBin));
			nBinsInLoop++;
		}

		parameters[1] = bulkYield;
		errors[1] = TMath::Sqrt(bulkErr2);

		// Calculate the average yield per bin.
		parameters[1] /= (nBinsInLoop * (Double_t)nBinsJet);
		errors[1] /= (nBinsInLoop * (Double_t)nBinsJet);

	}

	// For the background we return the yield per unit DEta,DPhi
	parameters[1] /= (correctedDiHadron->GetXaxis()->GetBinWidth(1) * correctedDiHadron->GetYaxis()->GetBinWidth(1));
	errors[1] /= (correctedDiHadron->GetXaxis()->GetBinWidth(1) * correctedDiHadron->GetYaxis()->GetBinWidth(1));

	parameters[2] = fitFunc3->GetParameter(3);
	errors[2] = fitFunc3->GetParError(3);	

	parameters[3] = fitFunc3->GetParameter(6);
	errors[3] = fitFunc3->GetParError(6);

	delete hSideBand;
	delete hJet;
	delete correctedDiHadron;
	delete fitFunc;
	delete fitFunc2;
	delete fitFunc3;

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::FitDPhiWithTrippleGauss(AliSettingsDiHadronPID::Species species, Int_t pTBin, Double_t /*PeakMaxDPhi*/, Double_t PeakMaxDEta, Double_t /*BulkMinAbsDEta*/, Double_t* parameters, Double_t* errors, Bool_t saveFitToPDF) const {

	// Fit with double gauss on the near-side, and single gauss on the away side.
	// No background subtraction.
	// Parameters: (const, jetNSyield, jetNSwidth, jetASyield, jetASwidth)

	// TODO:
	//  - Rename to DPhi fit,
	//  - pp: No flow, just a tripple gauss
	//  - pPb: Flow + Tripple Gauss, Flow not measured in sideband.
	//  - PbPb: Flow measured in side band and fixed. Tripple gauss fit.

	// Get a clone of the histogram.
	TH2F* correctedDiHadron = GetCorrectedClone(species, pTBin);
/*#ifdef __CLING__
	const TAxis* aDEta = correctedDiHadron->GetYaxis();
	const TAxis* aDPhi = correctedDiHadron->GetXaxis();
#else
	TAxis* aDEta = correctedDiHadron->GetYaxis();
	TAxis* aDPhi = correctedDiHadron->GetXaxis();
#endif */

	const Int_t JetMinBin = correctedDiHadron->GetYaxis()->FindBin(-PeakMaxDEta);
	const Int_t JetMaxBin = correctedDiHadron->GetYaxis()->FindBin(PeakMaxDEta);
	// const Int_t nBinsJet = JetMaxBin - JetMinBin + 1;

	// Create output histograms.
	TH1F* hJet = (TH1F*)correctedDiHadron->ProjectionX(Form("jet_%s_ptBin%i", fSettings->GetSpeciesName(species), pTBin), JetMinBin, JetMaxBin); 
	hJet->SetTitle(Form("%s Jet (pt bin: %i)", fSettings->GetSpeciesName(species), pTBin));
	hJet->SetDirectory(0);
	
	// Create fit function (const + 3x Gauss).
	TF1* fitFunc = new TF1("fitFunc","[0]*(1+2*[10]*cos(x)+2*[11]*cos(2*x)+2*[12]*cos(3*x)+2*[13]*cos(4*x))+([1]/(sqrt(2*TMath::Pi())*[3]))*exp(-0.5*(((x-[2])/[3])**2))+([1]/(sqrt(2*TMath::Pi())*[3]))*exp(-0.5*(((x-[2]-2*TMath::Pi())/[3])**2))+([4]/(sqrt(2*TMath::Pi())*[6]))*exp(-0.5*(((x-[5])/[6])**2))+([4]/(sqrt(2*TMath::Pi())*[6]))*exp(-0.5*(((x-[5]-2*TMath::Pi())/[6])**2))+([7]/(sqrt(2*TMath::Pi())*[9]))*exp(-0.5*(((x-[8])/[9])**2))+([7]/(sqrt(2*TMath::Pi())*[9]))*exp(-0.5*(((x-[8]+2*TMath::Pi())/[9])**2))", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
	fitFunc->SetParName(0, "const");
	fitFunc->SetParName(1, "yieldNS1");
	fitFunc->SetParName(2, "muDPhiNS1");
	fitFunc->SetParName(3, "sigmaDPhiNS1");
	fitFunc->SetParName(4, "yieldNS2");
	fitFunc->SetParName(5, "muDPhiNS2");
	fitFunc->SetParName(6, "sigmaDPhiNS2");
	fitFunc->SetParName(7, "yieldAS");
	fitFunc->SetParName(8, "muDPhiAS");
	fitFunc->SetParName(9, "sigmaDPhiAS");
	fitFunc->SetParName(10, "vD1");
	fitFunc->SetParName(11, "vD2");
	fitFunc->SetParName(12, "vD3");
	fitFunc->SetParName(13, "vD4");

	// Estimate yield background.
	Double_t bgEstimate = 0.;
	Double_t counter = 0.;
	for (Int_t iBin = 1; iBin < correctedDiHadron->GetYaxis()->FindBin(-1); iBin++) {
		bgEstimate += hJet->GetBinContent(iBin);
		counter++;
	}
	bgEstimate /= counter;

	// Estimate yield near side peak.
	Double_t nsEstimate = 0.;
	counter = 0.;
	for (Int_t iBin = correctedDiHadron->GetYaxis()->FindBin(-1); iBin <= correctedDiHadron->GetYaxis()->FindBin(1); iBin++) {
		nsEstimate += hJet->GetBinContent(iBin);
		counter++;
	}	
	nsEstimate -= (counter * bgEstimate);
	nsEstimate *= correctedDiHadron->GetXaxis()->GetBinWidth(1);

	// Estimate yield away side peak.
	Double_t asEstimate = 0.;
	counter = 0.;
	for (Int_t iBin = correctedDiHadron->GetYaxis()->FindBin(TMath::Pi()/2.); iBin <= correctedDiHadron->GetYaxis()->FindBin(3.*TMath::Pi()/2.); iBin++) {
		asEstimate += hJet->GetBinContent(iBin);
		counter++;
	}	
	asEstimate -= (counter * bgEstimate);
	asEstimate *= correctedDiHadron->GetXaxis()->GetBinWidth(1);
	
	// Base Line
	fitFunc->SetParameter(0, bgEstimate);

	// NS Peak 1
	fitFunc->SetParameter(1, nsEstimate);
	fitFunc->FixParameter(2, 0.0);
	fitFunc->SetParameter(3, 0.3);

	// NS Peak 2
	fitFunc->FixParameter(4, 0.0);
	fitFunc->FixParameter(5, 0.0);
	fitFunc->FixParameter(6, 0.6);

	// AS Peak (pp and pPb)
	// fitFunc->SetParameter(7, asEstimate);
	// fitFunc->FixParameter(8, TMath::Pi());
	// fitFunc->SetParameter(9, 0.6);

	// AS Peak (PbPb)
	fitFunc->SetParameter(7, 0.);
	fitFunc->FixParameter(8, TMath::Pi());
	fitFunc->FixParameter(9, 0.6);

	// Flow settings (pPb)
	fitFunc->FixParameter(10, 0.);
	fitFunc->SetParameter(11, 0.01);
	fitFunc->SetParameter(12, 0.01);
	fitFunc->FixParameter(13, 0.);

	// Flow settings (PbPb)
	// fitFunc->FixParameter(10, 0.);
	// fitFunc->SetParameter(11, 0.01);
	// fitFunc->SetParameter(12, 0.01);
	// fitFunc->FixParameter(13, 0.);

	TF1* fitFunc2 = 0x0;

	if (saveFitToPDF) {

		TCanvas* cvs = TCanvas::MakeDefCanvas();
		AliMiscToolsDiHadronPID::CreateDir(Form("./%s/Sp%i", fPlotDir.Data(), species));			
		
		hJet->Fit(fitFunc, "RQI");
		
		// fitFunc2 = new TF1(*fitFunc);
		// fitFunc2->ReleaseParameter(4);
		// fitFunc2->SetParameter(4, fitFunc->GetParameter(1) / 10.);
		// fitFunc2->ReleaseParameter(6);
		// fitFunc2->SetLineColor(kBlue);
		// hJet->Fit(fitFunc2,"RI+");
 
		// Draw Flow coeficients seperately.
		TF1* CC = new TF1("fitConst", "[0]", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
		CC->SetParameter(0, fitFunc->GetParameter(0));
		CC->SetLineColor(kPink);
		CC->SetLineStyle(2);
		CC->Draw("same");

		TF1* vDelta2fit = new TF1("fitvDelta2", "[0]*(1+2*[1]*cos(2*x))", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
		vDelta2fit->SetParameters(fitFunc->GetParameter(0), fitFunc->GetParameter(11));
		vDelta2fit->SetLineColor(kPink - 4);
		vDelta2fit->SetLineStyle(2);		
		vDelta2fit->Draw("same");

		TF1* vDelta3fit = new TF1("fitvDelta3", "[0]*(1+2*[1]*cos(3*x))", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
		vDelta3fit->SetParameters(fitFunc->GetParameter(0), fitFunc->GetParameter(12));
		vDelta3fit->SetLineColor(kPink - 6);
		vDelta3fit->SetLineStyle(2);		
		vDelta3fit->Draw("same");

		TF1* vFlowTotal = new TF1("flowTotal", "[0]*(1+2*[1]*cos(2*x)+2*[2]*cos(3*x))", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
		vFlowTotal->SetParameters(fitFunc->GetParameter(0), fitFunc->GetParameter(11), fitFunc->GetParameter(12));
		vFlowTotal->SetLineColor(kPink);
		// vFlowTotal->SetLineStyle(2);		
		vFlowTotal->Draw("same");

		TLatex* lDEtaRangeJet = new TLatex(0.45, 0.72, Form("%4.2f < #Delta#eta < %4.2f", correctedDiHadron->GetYaxis()->GetBinLowEdge(JetMinBin), correctedDiHadron->GetYaxis()->GetBinUpEdge(JetMaxBin)));
		lDEtaRangeJet->SetNDC();
		lDEtaRangeJet->Draw();

		TLatex* lChi2_TwoGauss = new TLatex(0.45, 0.64, Form("#chi^{2}/D.O.F. = %4.2f", fitFunc->GetChisquare() / (hJet->GetNbinsX() - 5)));
		lChi2_TwoGauss->SetTextColor(kRed);
		lChi2_TwoGauss->SetNDC();
		lChi2_TwoGauss->Draw();		

		// TLatex* lChi2_ThreeGauss = new TLatex(0.45, 0.56, Form("#chi^{2}/D.O.F. = %4.2f", fitFunc2->GetChisquare() / (hJet->GetNbinsX() - 7)));
		// lChi2_ThreeGauss->SetTextColor(kBlue);
		// lChi2_ThreeGauss->SetNDC();
		// lChi2_ThreeGauss->Draw();

		cvs->SaveAs(Form("./%s/Sp%i/PtBin%02i.pdf", fPlotDir.Data(), species, pTBin));
		delete cvs;

		delete lDEtaRangeJet;

	} else {
		hJet->Fit(fitFunc, "RQI0");
	}

	parameters[0] = fitFunc->GetParameter(0);
	errors[0] = fitFunc->GetParError(0);

	parameters[1] = fitFunc->GetParameter(1);
	errors[1] = fitFunc->GetParError(1);

	parameters[2] = fitFunc->GetParameter(3);
	errors[2] = fitFunc->GetParError(3);

	parameters[3] = fitFunc->GetParameter(7);
	errors[3] = fitFunc->GetParError(7);

	parameters[4] = fitFunc->GetParameter(9);
	errors[4] = fitFunc->GetParError(9);

	delete hJet;
	delete correctedDiHadron;
	delete fitFunc;
	if (fitFunc2) {delete fitFunc2;}

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::FitBulkWithConstVn(AliSettingsDiHadronPID::Species species, Int_t pTBin, Double_t BulkMinAbsDEta, Double_t* parameters, Double_t* errors, Bool_t saveFitToPDF) const {

	// Project the near-side peak onto the DPhi axis, and fit with 
	// a 1D Gaussian + cos + const. This method returns a ptr to the canvas
	// with the fit result.
	//
	// This method expects "parameters" and "errors" to be properly initialized 8D arrays.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());
	
	// Create the projection on the DPhi axis.
	TH1F* hEtaGap = GetHistBulkFitWithConst(species, pTBin, BulkMinAbsDEta);

	TF1* fitFunc = new TF1("fitFunc","[0]*(1+2*[1]*cos(x)+2*[2]*cos(2*x)+2*[3]*cos(3*x)+2*[4]*cos(4*x))+([5]/(sqrt(2*TMath::Pi())*[7]))*exp(-0.5*(((x-[6])/[7])**2))", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
	fitFunc->SetParNames("const", "V1D", "V2D", "V3D","V4D", "GaussYield", "GaussMu", "GaussSigma");
	fitFunc->SetParameters(hEtaGap->GetBinContent(1), 0.01, 0.01, 0.01, 0., 0., TMath::Pi(), 0.6);
	fitFunc->FixParameter(1, 0.); // V1D
	//fitFunc->FixParameter(3, 0.); 
	//fitFunc->FixParameter(4, 0.);
	fitFunc->FixParameter(4, 0.);

	// Gaussian.
	fitFunc->FixParameter(5, 0.);
	fitFunc->FixParameter(6, TMath::Pi());
	fitFunc->FixParameter(7, 0.6);

	// Fit the function (R = use function range);
	if (saveFitToPDF) {

		fitFunc->SetLineColor(kBlue);
		fitFunc->SetLineWidth(3);

		TCanvas* cvs = TCanvas::MakeDefCanvas();
		AliMiscToolsDiHadronPID::CreateDir(Form("./%s/Sp%i", fPlotDir.Data(), species));			
		hEtaGap->Fit(fitFunc, "RQI");

		// Draw Flow coeficients seperately.
		TF1* CC = new TF1("fitConst", "[0]", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
		CC->SetParameter(0, fitFunc->GetParameter(0));
		CC->SetLineColor(kPink);
		CC->SetLineStyle(2);
		CC->Draw("same");

		TF1* vDelta1fit = new TF1("fitvDelta1", "[0]*(1+2*[1]*cos(x))", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
		vDelta1fit->SetParameters(fitFunc->GetParameter(0), fitFunc->GetParameter(1));
		vDelta1fit->SetLineColor(kPink - 2);
		vDelta1fit->SetLineStyle(2);		
		vDelta1fit->Draw("same");

		TF1* vDelta2fit = new TF1("fitvDelta2", "[0]*(1+2*[1]*cos(2*x))", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
		vDelta2fit->SetParameters(fitFunc->GetParameter(0), fitFunc->GetParameter(2));
		vDelta2fit->SetLineColor(kPink - 4);
		vDelta2fit->SetLineStyle(2);		
		vDelta2fit->Draw("same");

		TF1* vDelta3fit = new TF1("fitvDelta3", "[0]*(1+2*[1]*cos(3*x))", -TMath::Pi()/2., 3.*TMath::Pi()/2.);
		vDelta3fit->SetParameters(fitFunc->GetParameter(0), fitFunc->GetParameter(3));
		vDelta3fit->SetLineColor(kPink - 6 );
		vDelta3fit->SetLineStyle(2);		
		vDelta3fit->Draw("same");

		cvs->SaveAs(Form("./%s/Sp%i/PtBin%02i.pdf", fPlotDir.Data(), species, pTBin));
		delete cvs;

		delete CC;
		delete vDelta1fit;
		delete vDelta2fit;
		delete vDelta3fit;

	} else {
		hEtaGap->Fit(fitFunc, "RQI0");
	}

	fitFunc->GetParameters(parameters);
	
	// TF1::GetParErrors(Double* errors) is not implemented...
	const Double_t* errors_tmp = fitFunc->GetParErrors();
	for (Int_t iPar = 0; iPar < 5; ++iPar) {errors[iPar] = errors_tmp[iPar];}

	delete fitFunc;
	delete hEtaGap;

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::FitDEtaWithGauss(AliSettingsDiHadronPID::Species species, Int_t pTbin, Double_t PeakMaxDPhi, Double_t, Double_t BulkMinAbsDEta, Double_t* parameters, Double_t* errors, Bool_t doubleGauss, Bool_t saveFitToPDF) const {

	// Project the near-side peak onto the DEta axis, and fit with 
	// a 1D Gaussian (or double gauss) + const. This method returns a ptr to the canvas
	// with the fit result.
	//
	// This method expects "parameters" and "errors" to be properly initialized 7D arrays (bgd yield, peak yield1, peak mu1, peak width1, peak yield2, peak mu2, peak width2).
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());
	
	TH2F* correctedDiHadron = GetCorrectedClone(species, pTbin);

	// Determine the min and max bin.
/* #ifdef __CLING__
	const TAxis* aDPhi = correctedDiHadron->GetXaxis();
	const TAxis* aDEta = correctedDiHadron->GetYaxis();
#else
	TAxis* aDPhi = correctedDiHadron->GetXaxis();
	TAxis* aDEta = correctedDiHadron->GetYaxis();
#endif */

	// Determine the bin range in DEta for the side band.
	// const Int_t BulkMinBin1 = 2;
	// const Int_t BulkMaxBin1 = correctedDiHadron->GetYaxis()->FindBin(-BulkMinAbsDEta);
	const Int_t BulkMinBin2 = correctedDiHadron->GetYaxis()->FindBin(BulkMinAbsDEta);
	const Int_t BulkMaxBin2 = correctedDiHadron->GetYaxis()->GetNbins() - 1;

	Int_t minDPhiBin = correctedDiHadron->GetXaxis()->FindBin(-PeakMaxDPhi + 1.e-4);
	Int_t maxDPhiBin = correctedDiHadron->GetXaxis()->FindBin(PeakMaxDPhi - 1.e-4);
	Int_t nBinsDPhi = maxDPhiBin - minDPhiBin + 1;
	AliLogDiHadronPID::Info(Form("DPhi bin range: (%i, %i)", minDPhiBin, maxDPhiBin), __func__, ClassName());

	if (correctedDiHadron->GetXaxis()->FindBin(-PeakMaxDPhi- 1.e-4) != correctedDiHadron->GetXaxis()->FindBin(-PeakMaxDPhi+ 1.e-4)) {AliLogDiHadronPID::Warning(Form("The value of minDPhi is very close to a bin edge..."), __func__, ClassName());}
	if (correctedDiHadron->GetXaxis()->FindBin(PeakMaxDPhi - 1.e-4) != correctedDiHadron->GetXaxis()->FindBin(PeakMaxDPhi + 1.e-4)) {AliLogDiHadronPID::Warning(Form("The value of PeakMaxDPhi is very close to a bin edge..."), __func__, ClassName());}

	// Create the projection on the DEta axis that we want to extract the yield from.
	TH1F* correctedDiHadron1D = (TH1F*)correctedDiHadron->ProjectionY(Form("test_%s_ptBin%i", fSettings->GetSpeciesName(species), pTbin), minDPhiBin, maxDPhiBin); 
	correctedDiHadron1D->SetTitle(Form("%s (pt bin: %i)", fSettings->GetSpeciesName(species), pTbin));
	correctedDiHadron1D->SetDirectory(0);

	// Symmetrize the histogram (only works for even bins.)
	// for (Int_t iBin = 1; iBin <= correctedDiHadron1D->GetNbinsX() / 2; iBin++) {
	// 	Int_t binRight = correctedDiHadron1D->GetNbinsX() + 1 - iBin;

	// 	Double_t avgContent = (correctedDiHadron1D->GetBinContent(iBin) + correctedDiHadron1D->GetBinContent(binRight)) / 2.;
	// 	Double_t avgError = TMath::Sqrt(correctedDiHadron1D->GetBinError(iBin)*correctedDiHadron1D->GetBinError(iBin) 
	// 		+ correctedDiHadron1D->GetBinError(binRight)*correctedDiHadron1D->GetBinError(binRight)) / 2.;

	// 	correctedDiHadron1D->SetBinContent(iBin, avgContent);
	// 	correctedDiHadron1D->SetBinError(iBin, avgError);

	// 	correctedDiHadron1D->SetBinContent(binRight, avgContent);
	// 	correctedDiHadron1D->SetBinError(binRight, avgError);
	
	// }

	// Create fit function.
	TF1* fitFunc = new TF1("fitFunc","[0]+([1]/(sqrt(2*TMath::Pi())*[3]))*exp(-0.5*(((x-[2])/[3])**2))+([4]/(sqrt(2*TMath::Pi())*[6]))*exp(-0.5*(((x-[5])/[6])**2))", -1.5, 1.5);
	fitFunc->SetParNames("const", "yield1", "muDEta1", "sDeta1", "yield2", "muDEta2", "sDeta2");

	// Make a reasonable estimate for the background.
	Double_t values[correctedDiHadron1D->GetNbinsX()];
	Double_t valerrors[correctedDiHadron1D->GetNbinsX()];
	for (Int_t iBinDEta = 1; iBinDEta <= correctedDiHadron->GetYaxis()->GetNbins(); iBinDEta++) {
		values[iBinDEta - 1] = 0.;
		valerrors[iBinDEta - 1] = 0.;
	} 

	Int_t nBinsSideBand = 0;
	Int_t nBinsPeakEst = 0;
	Double_t peakYieldEst = 0.;
	for (Int_t iBinDEta = 2; iBinDEta <= correctedDiHadron->GetYaxis()->GetNbins() - 1; iBinDEta++) {

		Double_t center = correctedDiHadron->GetYaxis()->GetBinCenter(iBinDEta);
		if (center < -BulkMinAbsDEta || center > BulkMinAbsDEta) {
			values[nBinsSideBand] = correctedDiHadron1D->GetBinContent(iBinDEta);
			valerrors[nBinsSideBand] = correctedDiHadron1D->GetBinError(iBinDEta);
			nBinsSideBand++;
		} else {
			peakYieldEst += correctedDiHadron1D->GetBinContent(iBinDEta);
			nBinsPeakEst++;
		}

	}

	Double_t bgdEst = 0.;
	Double_t bgdEstErr = 0.;
	AliFunctionsDiHadronPID::ConstFit(values, valerrors, nBinsSideBand, bgdEst, bgdEstErr);

	peakYieldEst -= (bgdEst * nBinsPeakEst); 
	peakYieldEst *= correctedDiHadron->GetYaxis()->GetBinWidth(1);

	// Set parameters.
	//  - Background is fixed to the estimate at large DEta. 
	//  - Peak yield is estimated at 2x the peak height minus the background.
	fitFunc->FixParameter(0, bgdEst);

	fitFunc->SetParameter(1, peakYieldEst);
	fitFunc->FixParameter(2, 0.);
	fitFunc->SetParameter(3, 0.4);
	fitFunc->SetParLimits(3, 0.,2.);

	fitFunc->FixParameter(4, 0.);
	fitFunc->FixParameter(5, 0.);
	fitFunc->FixParameter(6, 0.6);

	correctedDiHadron1D->Fit(fitFunc ,"RQI0"); //RQ0

	TF1* constFunc = new TF1("constFunc", "[0]",-1.5,1.5);
	constFunc->SetLineColor(kBlack);
	constFunc->SetParameter(0, bgdEst);

	//fitFunc->ReleaseParameter(0);

	TF1* fitFunc2 = 0x0;
	if (saveFitToPDF) {

		TCanvas* cvs = TCanvas::MakeDefCanvas();
		AliMiscToolsDiHadronPID::CreateDir(Form("./%s/Sp%i", fPlotDir.Data(), species));			
		fitFunc->SetLineColor(kRed);
		correctedDiHadron1D->Fit(fitFunc, "RQI");

		if (doubleGauss) {
			fitFunc2 = new TF1(*fitFunc);
			//fitFunc2->ReleaseParameter(4);
			fitFunc2->SetParameter(4, fitFunc->GetParameter(1) * 0.2);
			//fitFunc2->ReleaseParameter(6);
			fitFunc2->SetLineColor(kBlue);
			correctedDiHadron1D->Fit(fitFunc2, "RQI+");
		}

		constFunc->Draw("same");

		TLatex* lDEtaRangeBulk = new TLatex(0.3, 0.22, Form("%4.2f < |#Delta#eta_{bulk}| < %4.2f", correctedDiHadron->GetYaxis()->GetBinLowEdge(BulkMinBin2), correctedDiHadron->GetYaxis()->GetBinUpEdge(BulkMaxBin2)));
		lDEtaRangeBulk->SetNDC();
		lDEtaRangeBulk->SetTextSizePixels(10);
		lDEtaRangeBulk->Draw();

		TLatex* lDPhiRangeJet = new TLatex(0.3, 0.15, Form("%4.2f#pi < #Delta#phi < %4.2f#pi", correctedDiHadron->GetXaxis()->GetBinLowEdge(minDPhiBin)/TMath::Pi(), correctedDiHadron->GetXaxis()->GetBinUpEdge(maxDPhiBin)/TMath::Pi()));
		lDPhiRangeJet->SetNDC();
		lDPhiRangeJet->SetTextSizePixels(10);
		lDPhiRangeJet->Draw();

		TLatex* lChi2_SingleGauss = new TLatex(0.55, 0.84, Form("#chi^{2}/D.O.F. = %4.2f", fitFunc->GetChisquare() / (correctedDiHadron1D->GetNbinsX() - 4)));
		lChi2_SingleGauss->SetTextColor(kRed);
		lChi2_SingleGauss->SetNDC();
		lChi2_SingleGauss->SetTextSizePixels(10);
		lChi2_SingleGauss->Draw();		

		if (fitFunc2) {
			TLatex* lChi2_DoubleGauss = new TLatex(0.55, 0.76, Form("#chi^{2}/D.O.F. = %4.2f", fitFunc2->GetChisquare() / (correctedDiHadron1D->GetNbinsX() - 4)));
			lChi2_DoubleGauss->SetTextColor(kBlue);
			lChi2_DoubleGauss->SetNDC();
			lChi2_DoubleGauss->SetTextSizePixels(10);
			lChi2_DoubleGauss->Draw();
		}

		cvs->SaveAs(Form("./%s/Sp%i/PtBin%02i.pdf", fPlotDir.Data(), species, pTbin));
		delete cvs;

	} else {
		correctedDiHadron1D->Fit(fitFunc ,"RQI0");
		if (doubleGauss) {
			fitFunc2 = new TF1(*fitFunc);
			fitFunc2->ReleaseParameter(4);
			fitFunc2->SetParameter(4, fitFunc->GetParameter(1) * 0.1);
			fitFunc2->ReleaseParameter(6);
			correctedDiHadron1D->Fit(fitFunc2, "RQI0");
		}
	}

	const Double_t* parameters_tmp = 0x0; 
	const Double_t* errors_tmp = 0x0;

	if (fitFunc2) {
		parameters_tmp = fitFunc2->GetParameters();
		errors_tmp = fitFunc2->GetParErrors();
	} else {
		parameters_tmp = fitFunc->GetParameters();
		errors_tmp = fitFunc->GetParErrors();		
	}
	for (Int_t iPar = 0; iPar < 7; ++iPar) {
		parameters[iPar] = parameters_tmp[iPar];
		errors[iPar] = errors_tmp[iPar];
	}

	// The background histogram was the sum over nDPhiBins, scale to per bin:
	parameters[0] /= ((Double_t)nBinsDPhi);
	errors[0] /= ((Double_t)nBinsDPhi);

	// The fitted yield in the bgd is per bin, rescale so that it's per unit DPhi, DEta.
	parameters[0] /= (correctedDiHadron->GetXaxis()->GetBinWidth(1) * correctedDiHadron->GetYaxis()->GetBinWidth(1));
	errors[0] /= (correctedDiHadron->GetXaxis()->GetBinWidth(1) * correctedDiHadron->GetYaxis()->GetBinWidth(1));

	// Renormalise the yields for the peak with the binwidth in DEta, so that they are counts.
	parameters[1] /= correctedDiHadron->GetYaxis()->GetBinWidth(1);
	errors[1] /= correctedDiHadron->GetYaxis()->GetBinWidth(1);

	if (constFunc) {delete constFunc;}
	if (fitFunc) {delete fitFunc;}
	if (fitFunc2) {delete fitFunc2;}
	if (correctedDiHadron) {delete correctedDiHadron;}
	if (correctedDiHadron1D) {delete correctedDiHadron1D;}

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::FitDEtaWithParabola(AliSettingsDiHadronPID::Species species, Int_t pTbin, Double_t minDPhi, Double_t maxDPhi, Double_t* parameters, Double_t* errors, Bool_t saveFitToPDF) const {

	// Make a projection on the DEta axis, and fit with a parabola. Mainly useful to check if 
	// the background is more or less flat.

	//
	// This method expects "parameters" and "errors" to be properly initialized 2D arrays (a0 + a2 * x^2).
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());
	
	TH2F* correctedDiHadron = GetCorrectedClone(species, pTbin);

	// Determine the min and max bin.
/* #ifdef __CLING__
	const TAxis* aDPhi = correctedDiHadron->GetXaxis();
	const TAxis* aDEta = correctedDiHadron->GetYaxis();
#else
	TAxis* aDPhi = correctedDiHadron->GetXaxis();
	TAxis* aDEta = correctedDiHadron->GetYaxis();
#endif */

	Int_t minDPhiBin = correctedDiHadron->GetXaxis()->FindBin(minDPhi + 1.e-4);
	Int_t maxDPhiBin = correctedDiHadron->GetXaxis()->FindBin(maxDPhi - 1.e-4);
	// Int_t nBinsDPhi = maxDPhiBin - minDPhiBin + 1;
	AliLogDiHadronPID::Info(Form("DPhi bin range: (%i, %i)",minDPhiBin, maxDPhiBin), __func__, ClassName());

	if (correctedDiHadron->GetXaxis()->FindBin(minDPhi - 1.e-4) != correctedDiHadron->GetXaxis()->FindBin(minDPhi + 1.e-4)) {AliLogDiHadronPID::Warning(Form("The value of minDPhi is very close to a bin edge..."), __func__, ClassName());}
	if (correctedDiHadron->GetXaxis()->FindBin(maxDPhi - 1.e-4) != correctedDiHadron->GetXaxis()->FindBin(maxDPhi + 1.e-4)) {AliLogDiHadronPID::Warning(Form("The value of maxDPhi is very close to a bin edge..."), __func__, ClassName());}

	// Create the projection on the DEta axis that we want to extract the yield from.
	TH1F* correctedDiHadron1D = (TH1F*)correctedDiHadron->ProjectionY(Form("test_%s_ptBin%i", fSettings->GetSpeciesName(species), pTbin), minDPhiBin, maxDPhiBin); 
	correctedDiHadron1D->SetTitle(Form("%s (pt bin: %i)", fSettings->GetSpeciesName(species), pTbin));
	correctedDiHadron1D->SetDirectory(0);

	// Create fit function.
	TF1* fitFunc = new TF1("fitFunc","[0]*(1+[1]*x*x)", -1.5, 1.5);
	fitFunc->SetParNames("a0", "a2");

	// Make a reasonable estimate for the background.
	Double_t values[correctedDiHadron1D->GetNbinsX()];
	Double_t valerrors[correctedDiHadron1D->GetNbinsX()];
	for (Int_t iBinDEta = 1; iBinDEta <= correctedDiHadron->GetYaxis()->GetNbins(); iBinDEta++) {
		values[iBinDEta - 1] = 0.;
		valerrors[iBinDEta - 1] = 0.;
	} 

	Int_t nBinsSideBand = 0;
	for (Int_t iBinDEta = 1; iBinDEta <= correctedDiHadron->GetYaxis()->GetNbins(); iBinDEta++) {

		Double_t center = correctedDiHadron->GetYaxis()->GetBinCenter(iBinDEta);
		if (center < -1. || center > 1.) {
			values[nBinsSideBand] = correctedDiHadron1D->GetBinContent(iBinDEta);
			valerrors[nBinsSideBand] = correctedDiHadron1D->GetBinError(iBinDEta);
			nBinsSideBand++;
		} 

	}

	Double_t bgdEst = 0.;
	Double_t bgdEstErr = 0.;
	AliFunctionsDiHadronPID::ConstFit(values, valerrors, nBinsSideBand, bgdEst, bgdEstErr);

	// Set parameters.
	//  - Background is fixed to the estimate at large DEta. 
	//  - Peak yield is estimated at 2x the peak height minus the background.
	fitFunc->SetParameter(0, bgdEst);
	fitFunc->SetParameter(1, 0.);

	correctedDiHadron1D->Fit(fitFunc ,"RQI0"); //RQ0

	fitFunc->ReleaseParameter(0);
	if (saveFitToPDF) {

		TCanvas* cvs = TCanvas::MakeDefCanvas();
		AliMiscToolsDiHadronPID::CreateDir(Form("./%s/Sp%i", fPlotDir.Data(), species));			
		correctedDiHadron1D->Fit(fitFunc, "RQ");

		TLatex* lDPhiRangeJet = new TLatex(0.3, 0.15, Form("%4.2f#pi < #Delta#phi < %4.2f#pi", correctedDiHadron->GetXaxis()->GetBinLowEdge(minDPhiBin)/TMath::Pi(), correctedDiHadron->GetXaxis()->GetBinUpEdge(maxDPhiBin)/TMath::Pi()));
		lDPhiRangeJet->SetNDC();
		lDPhiRangeJet->Draw();

		cvs->SaveAs(Form("./%s/Sp%i/PtBin%02i.pdf", fPlotDir.Data(), species, pTbin));
		delete cvs;

	} else {
		correctedDiHadron1D->Fit(fitFunc ,"RQI0"); //RQ0
	}

	fitFunc->GetParameters(parameters);
	
	// TF1::GetParErrors(Double* errors) is not implemented...
	const Double_t* errors_tmp = fitFunc->GetParErrors();
	for (Int_t iPar = 0; iPar < 2; ++iPar) {errors[iPar] = errors_tmp[iPar];}

	delete correctedDiHadron;
	correctedDiHadron = 0x0;


}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::MeasureJetAndBulkYieldUsingRegions(AliSettingsDiHadronPID::Species species, Region peakRegion, Region bulkRegion) {

	// This method determines the bulk and jet yield by subtracting a predefined "bulk" region
	// from the "peak" region.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	TH1F* hJetYield = GetProjectionPt(species, peakRegion);
	hJetYield->SetDirectory(0);
	hJetYield->SetName(Form("jetYield%s", fSettings->GetSpeciesName(species)));
	hJetYield->SetTitle(Form("Jet Yield From Regions %s", fSettings->GetSpeciesName(species)));
	TH1F* hRidgeYield = GetProjectionPt(species, bulkRegion);
	hRidgeYield->SetDirectory(0);
	hJetYield->Add(hRidgeYield, -1.);
	delete hRidgeYield; hRidgeYield = 0x0;

	TH1F* hBulkYield = GetProjectionPt(species, bulkRegion);
	hBulkYield->SetDirectory(0);
	hBulkYield->SetName(Form("bulkYield%s", fSettings->GetSpeciesName(species)));
	hBulkYield->SetTitle(Form("Bulk Yield From 1D Fit %s", fSettings->GetSpeciesName(species)));

	// Setting colors.
	hJetYield->SetLineColor(fSettings->GetSpeciesColor(species));
	hBulkYield->SetLineColor(fSettings->GetSpeciesColor(species));

	// Fixing histogram normalisation.
	if (fNTriggers > 1.) {
		hJetYield->Scale(1./fNTriggers,"width");
		hJetYield->GetYaxis()->SetTitle("#frac{1}{N_{trig}} #frac{dN}{dp_{T}}");
		hBulkYield->Scale(1./fNTriggers,"width");
		hBulkYield->GetYaxis()->SetTitle("#frac{1}{N_{trig} A} #frac{dN}{dp_{T}}");
	} else {
		hJetYield->Scale(1.,"width");
		hJetYield->GetYaxis()->SetTitle("#frac{dN}{dp_{T}}");
		hBulkYield->Scale(1.,"width");
		hBulkYield->GetYaxis()->SetTitle("#frac{1}{A}#frac{dN}{dp_{T}}");	
	}


	// Delete existing jet and bulk histograms.
	if (fJetYield[species]) {delete fJetYield[species]; fJetYield[species] = 0x0;}
	if (fBulkYield[species]) {delete fBulkYield[species]; fBulkYield[species] = 0x0;}

	fJetYield[species] = hJetYield;
	fBulkYield[species] = hBulkYield;

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::MeasureJetAndBulkYieldUsingBulkFit(AliSettingsDiHadronPID::Species species, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta, Bool_t saveFitsToFile) {

	// This method is similar as the method before, however it makes use of the fact 
	// that the background should be constant in DEta (only modulations in DPhi).
	//
	// The procedure is as follows:
	//  1) For every DPhi bin: perform a fit with a constant (analytically) of the bins
	//     as defined by BulkMinAbsDEta. 
	//  2) Create a histogram in DPhi, where the determined bgd is subtracted from 
	//     (sum < PeakMaxDEta).
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	// Create histograms for the jet Yield, Width and Bulk Width.
	TH1F* hJetYield = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetYield%s", fSettings->GetSpeciesName(species)), Form("Jet Yield From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hJetYield->SetDirectory(0);

	TH1F* hBulkYield = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("bulkYield%s", fSettings->GetSpeciesName(species)), Form("Bulk Yield From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hBulkYield->SetDirectory(0);

	for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {

		Double_t fitpars[2] = {0.};
		Double_t fiterrors[2] = {0.};

		RegionsMethod(species, iPtBin, PeakMaxDPhi, PeakMaxDEta, BulkMinAbsDEta, fitpars, fiterrors, saveFitsToFile);

		hJetYield->SetBinContent(iPtBin, fitpars[0]);
		hJetYield->SetBinError(iPtBin, fiterrors[0]);	

		hBulkYield->SetBinContent(iPtBin, fitpars[1]);
		hBulkYield->SetBinError(iPtBin, fiterrors[1]);

	}	

	// Setting names and colors.
	hJetYield->SetLineColor(fSettings->GetSpeciesColor(species));
	hBulkYield->SetLineColor(fSettings->GetSpeciesColor(species));

	// Fixing histogram normalisation.
	if (fNTriggers > 1.) {
		hJetYield->Scale(1./fNTriggers,"width");
		hJetYield->GetYaxis()->SetTitle("#frac{1}{N_{trig}} #frac{dN}{dp_{T}}");
		hBulkYield->Scale(1./fNTriggers,"width");
		hBulkYield->GetYaxis()->SetTitle("#frac{1}{N_{trig} A} #frac{dN}{dp_{T}}");
	} else {
		hJetYield->Scale(1.,"width");
		hJetYield->GetYaxis()->SetTitle("#frac{dN}{dp_{T}}");
		hBulkYield->Scale(1.,"width");
		hBulkYield->GetYaxis()->SetTitle("#frac{1}{A}#frac{dN}{dp_{T}}");	
	}
	
	// Save to file.
	if (saveFitsToFile) {

		AliMiscToolsDiHadronPID::CreateDir(Form("./%s/Sp%i", fPlotDir.Data(), species));
		TFile* fout = new TFile(Form("./%s/Sp%i/FitPars.root", fPlotDir.Data(), species), "recreate");
		hJetYield->Write();
		hBulkYield->Write();
		fout->Close();

	}

	// Delete existing jet and bulk histograms.
	if (fJetYield[species]) {delete fJetYield[species]; fJetYield[species] = 0x0;}
	if (fBulkYield[species]) {delete fBulkYield[species]; fBulkYield[species] = 0x0;}

	fJetYield[species] = hJetYield;
	fBulkYield[species] = hBulkYield;

	// Save to pdf.
	if (saveFitsToFile) {
		TCanvas* cvs = TCanvas::MakeDefCanvas();
		cvs->SetLogy();
		hJetYield->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/JetYield.pdf", fPlotDir.Data(), species));
		hBulkYield->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/BulkYield.pdf", fPlotDir.Data(), species));

		if (cvs) {delete cvs; cvs = 0x0;}

	}

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::MeasureJetAndBulkYield(AliSettingsDiHadronPID::BeamType beamType, AliSettingsDiHadronPID::Species species, DPhiBackgroundModel model, Bool_t rebinInPt, Bool_t useWingCorrection, Bool_t saveFitsToFile) {

	// This method is similar as the method before, however it fits the resulting
	// histogram in Dphi with a Gaussian.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	TAxis* ptAxisUsed = fPtAxis;
	
	// Perform rebinning in pT by adding (DPhi, DEta) histograms.
	Int_t minBins[7] = {1, 5, 9, 13, 17, 19, 21};
	Int_t maxBins[7] = {4, 8, 12, 16, 18, 20, 22};
	Double_t ptArray[8] = {1.0, 1.4, 1.8, 2.2, 2.6, 3.0, 3.4, 4.0};
	
	// Based on the pp radii (roughly 2.5 * fitted sigma of the protons). This contains:
	// Erf(2.5/sqrt(2))^2 ~ 0.975 of the proton data, so it could give a few percent bias.
	Double_t PeakRadius[7] = {1.12, 0.90, 0.75, 0.70, 0.62, 0.58, 0.5};

	if (rebinInPt) {
		ptAxisUsed = new TAxis(7, ptArray);
		ptAxisUsed->SetTitle("p_{T} GeV/c");
	}

	// Create histograms for the jet Yield, Width and Bulk Width.
	TH1F* hBulkYield = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("bulkYield%s", fSettings->GetSpeciesName(species)), Form("Bulk Yield (Best) %s", fSettings->GetSpeciesName(species)), "F", ptAxisUsed);
	hBulkYield->SetDirectory(0);

	TH1F* hJetYield = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetYield%s", fSettings->GetSpeciesName(species)), Form("Jet Yield (Best) %s", fSettings->GetSpeciesName(species)), "F", ptAxisUsed);
	hJetYield->SetDirectory(0);

	TH1F* hJetYieldNS = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetYieldNS%s", fSettings->GetSpeciesName(species)), Form("Jet Yield From 1D Fit %s (NS1)", fSettings->GetSpeciesName(species)), "F", ptAxisUsed);
	hJetYieldNS->SetDirectory(0);
	TH1F* hJetYieldF = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetYieldF%s", fSettings->GetSpeciesName(species)), Form("Jet Yield From 1D Fit %s (NS2)", fSettings->GetSpeciesName(species)), "F", ptAxisUsed);
	hJetYieldF->SetDirectory(0);
	TH1F* hJetWidthNS1 = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetWidthNS1%s", fSettings->GetSpeciesName(species)), Form("Jet Sigma From 1D Fit %s (NS1)", fSettings->GetSpeciesName(species)), "F", ptAxisUsed);
	hJetWidthNS1->SetDirectory(0);
	TH1F* hJetWidthNS2 = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetWidthNS2%s", fSettings->GetSpeciesName(species)), Form("Jet Sigma From 1D Fit %s (NS2)", fSettings->GetSpeciesName(species)), "F", ptAxisUsed);
	hJetWidthNS2->SetDirectory(0);

	// Jet Width Single Gauss
	TH1F* hJetWidthSGDPhi = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetWidth1DDPhi%s", fSettings->GetSpeciesName(species)), Form("Jet Sigma From 1D Fit %s (#Delta#phi)", fSettings->GetSpeciesName(species)), "F", ptAxisUsed);
	hJetWidthSGDPhi->SetDirectory(0);
	TH1F* hJetWidthSGDEta = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetWidth1DDEta%s", fSettings->GetSpeciesName(species)), Form("Jet Sigma From 1D Fit %s (#Delta#eta)", fSettings->GetSpeciesName(species)), "F", ptAxisUsed);
	hJetWidthSGDEta->SetDirectory(0);

	TH1F* hJetYieldAS = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetYieldAS%s", fSettings->GetSpeciesName(species)), Form("Jet Yield From 1D Fit %s (AS1)", fSettings->GetSpeciesName(species)), "F", ptAxisUsed);
	hJetYieldAS->SetDirectory(0);
	TH1F* hJetWidthAS = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetWidthAS%s", fSettings->GetSpeciesName(species)), Form("Jet Sigma From 1D Fit %s (AS1)", fSettings->GetSpeciesName(species)), "F", ptAxisUsed);
	hJetWidthAS->SetDirectory(0);

	TH1F* hV1 = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("v1%s", fSettings->GetSpeciesName(species)), Form("v_{1#Delta} with #Delta#eta gap %s", fSettings->GetSpeciesName(species)), "F", ptAxisUsed);
	hV1->SetDirectory(0);
	TH1F* hV2 = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("v2%s", fSettings->GetSpeciesName(species)), Form("v_{2#Delta} with #Delta#eta gap %s", fSettings->GetSpeciesName(species)), "F", ptAxisUsed);
	hV2->SetDirectory(0);	
	TH1F* hV3 = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("v3%s", fSettings->GetSpeciesName(species)), Form("v_{3#Delta} with #Delta#eta gap %s", fSettings->GetSpeciesName(species)), "F", ptAxisUsed);
	hV3->SetDirectory(0);
	TH1F* hV4 = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("v4%s", fSettings->GetSpeciesName(species)), Form("v_{4#Delta} with #Delta#eta gap %s", fSettings->GetSpeciesName(species)), "F", ptAxisUsed);
	hV4->SetDirectory(0);

	TCanvas* cvs = 0x0;
	TLegend* legend = 0x0;
	if (saveFitsToFile) {
		AliMiscToolsDiHadronPID::CreateDir(Form("./%s/Sp%i", fPlotDir.Data(), species));
		cvs = TCanvas::MakeDefCanvas();
		legend = new TLegend(0.7,0.65,0.88,0.88);
	}

	TObjArray* peakMeasurementHistos = new TObjArray();
	peakMeasurementHistos->SetOwner(kTRUE);
	TObjArray* bulkMeasurementHistos = new TObjArray();
	bulkMeasurementHistos->SetOwner(kTRUE);

	for (Int_t iPtBin = 1; iPtBin <= ptAxisUsed->GetNbins(); iPtBin++) {

		// Expects back a fit result from a DPhi fit!
		Double_t fitpars[14] = {0.};
		Double_t fiterrors[14] = {0.};

		Double_t measurementOfChoice[2] = {0.};
		Double_t measurementOfChoiceError[2] = {0.};

		Double_t peakWidthSG[2] = {0.};
		Double_t peakWidthSGError[2] = {0.};

		TH1F** hMeasurements = AnalyseDPhiDEta(beamType, species, model, minBins[iPtBin - 1], maxBins[iPtBin - 1], PeakRadius[iPtBin - 1], PeakRadius[iPtBin - 1],
			fitpars, fiterrors, measurementOfChoice, measurementOfChoiceError, peakWidthSG, peakWidthSGError, useWingCorrection, saveFitsToFile);

		hBulkYield->SetBinContent(iPtBin, measurementOfChoice[0]);
		hBulkYield->SetBinError(iPtBin, measurementOfChoiceError[0]);

		hJetYield->SetBinContent(iPtBin, measurementOfChoice[1]);
		hJetYield->SetBinError(iPtBin, measurementOfChoiceError[1]);

		hJetYieldNS->SetBinContent(iPtBin, fitpars[1]);
		hJetYieldNS->SetBinError(iPtBin, fiterrors[1]);	
		hJetYieldF->SetBinContent(iPtBin, fitpars[2]);
		hJetYieldF->SetBinError(iPtBin, fiterrors[2]);	
		hJetWidthNS1->SetBinContent(iPtBin, fitpars[4]);
		hJetWidthNS1->SetBinError(iPtBin, fiterrors[4]);		
		hJetWidthNS2->SetBinContent(iPtBin, fitpars[6]);
		hJetWidthNS2->SetBinError(iPtBin, fiterrors[6]);

		hJetWidthSGDPhi->SetBinContent(iPtBin, peakWidthSG[0]);
		hJetWidthSGDPhi->SetBinError(iPtBin, peakWidthSGError[0]);
		hJetWidthSGDEta->SetBinContent(iPtBin, peakWidthSG[1]);
		hJetWidthSGDEta->SetBinError(iPtBin, peakWidthSGError[1]);

		hJetYieldAS->SetBinContent(iPtBin, fitpars[7]);
		hJetYieldAS->SetBinError(iPtBin, fiterrors[7]);	
		hJetWidthAS->SetBinContent(iPtBin, fitpars[9]);
		hJetWidthAS->SetBinError(iPtBin, fiterrors[9]);

		hV1->SetBinContent(iPtBin, fitpars[10]);
		hV1->SetBinError(iPtBin, fiterrors[10]);
		hV2->SetBinContent(iPtBin, fitpars[11]);
		hV2->SetBinError(iPtBin, fiterrors[11]);
		hV3->SetBinContent(iPtBin, fitpars[12]);
		hV3->SetBinError(iPtBin, fiterrors[12]);
		hV4->SetBinContent(iPtBin, fitpars[13]);
		hV4->SetBinError(iPtBin, fiterrors[13]);

		// Make histograms of the different peak measurements as a function of pT.
		TAxis* measurementAxisPeak = hMeasurements[0]->GetXaxis();
		Int_t nMeasurementsPeak = hMeasurements[0]->GetNbinsX();
		hMeasurements[0]->Print();
		
		for (Int_t iPeakMeasurement = 1; iPeakMeasurement <= nMeasurementsPeak; iPeakMeasurement++) {

			if (peakMeasurementHistos->GetEntriesFast() < nMeasurementsPeak) {
				TH1F* tmp_hist = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("%s_%s", measurementAxisPeak->GetBinLabel(iPeakMeasurement), fSettings->GetSpeciesName(species)),
					Form("%s;p_{T}", measurementAxisPeak->GetBinLabel(iPeakMeasurement)), "F", ptAxisUsed);

				tmp_hist->SetLineColor(iPeakMeasurement);
				tmp_hist->SetMarkerColor(iPeakMeasurement);
				tmp_hist->SetMarkerStyle(20 + iPeakMeasurement);

				peakMeasurementHistos->AddLast(tmp_hist);
			}

			TH1F* currentHist = (TH1F*)peakMeasurementHistos->At(iPeakMeasurement - 1);

			currentHist->SetBinContent(iPtBin, (hMeasurements[0])->GetBinContent(iPeakMeasurement));
			currentHist->SetBinError(iPtBin, (hMeasurements[0])->GetBinError(iPeakMeasurement));

		}

		// Make histograms of the different bulk measurements as a function of pT.
		TAxis* measurementAxisBulk = hMeasurements[1]->GetXaxis();
		Int_t nMeasurementsBulk = hMeasurements[1]->GetNbinsX();
		hMeasurements[1]->Print();
		
		for (Int_t iBulkMeasurement = 1; iBulkMeasurement <= nMeasurementsBulk; iBulkMeasurement++) {

			if (bulkMeasurementHistos->GetEntriesFast() < nMeasurementsBulk) {
				TH1F* tmp_hist = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("%s_%s", measurementAxisBulk->GetBinLabel(iBulkMeasurement), fSettings->GetSpeciesName(species)),
					Form("%s;p_{T}", measurementAxisBulk->GetBinLabel(iBulkMeasurement)), "F", ptAxisUsed);

				tmp_hist->SetLineColor(iBulkMeasurement);
				tmp_hist->SetMarkerColor(iBulkMeasurement);
				tmp_hist->SetMarkerStyle(20 + iBulkMeasurement);

				bulkMeasurementHistos->AddLast(tmp_hist);
			}

			TH1F* currentHist = (TH1F*)bulkMeasurementHistos->At(iBulkMeasurement - 1);

			currentHist->SetBinContent(iPtBin, (hMeasurements[1])->GetBinContent(iBulkMeasurement));
			currentHist->SetBinError(iPtBin, (hMeasurements[1])->GetBinError(iBulkMeasurement));

		}

	}	
	
	// Save the different measurements to file.
	if (cvs) {
	
		// Peak
		cvs->cd();
		for (Int_t iPeakMeasurement = 0; iPeakMeasurement < peakMeasurementHistos->GetEntriesFast(); iPeakMeasurement++) {
				
			TH1F* currentHist = (TH1F*)peakMeasurementHistos->At(iPeakMeasurement);

			// Crappy fix for pp (REMOVE ME!)
			if (currentHist->GetMaximum() > 1.2) {currentHist->SetMaximum(1.2);}
			if (currentHist->GetMinimum() < 0.8) {currentHist->SetMinimum(0.8);}

			if (iPeakMeasurement) {currentHist->Draw("same");} 
			else {currentHist->Draw();}
		
			legend->AddEntry(currentHist, currentHist->GetTitle());

		}

		AliHistToolsDiHadronPID::FixPlotRange(cvs);

		legend->Draw();

		cvs->SaveAs(Form("./%s/Sp%i/JetMeasurements.pdf", fPlotDir.Data(), species));	

		cvs->Clear();
		legend->Clear();

		// Bulk
		for (Int_t iBulkMeasurement = 0; iBulkMeasurement < bulkMeasurementHistos->GetEntriesFast(); iBulkMeasurement++) {
				
			TH1F* currentHist = (TH1F*)bulkMeasurementHistos->At(iBulkMeasurement);

			// Crappy fix for pp (REMOVE ME!)
			if (currentHist->GetMaximum() > 1.2) {currentHist->SetMaximum(1.2);}
			if (currentHist->GetMinimum() < 0.8) {currentHist->SetMinimum(0.8);}

			if (iBulkMeasurement) {currentHist->Draw("same");} 
			else {currentHist->Draw();}
		
			legend->AddEntry(currentHist, currentHist->GetTitle());

		}

		AliHistToolsDiHadronPID::FixPlotRange(cvs);

		legend->Draw();

		cvs->SaveAs(Form("./%s/Sp%i/BulkMeasurements.pdf", fPlotDir.Data(), species));	

		cvs->Clear();
		legend->Clear();

	}

	// Apply primary fraction correction on the fitted yields (TODO this could be different for peak and bulk!)
	if ((species == 0 || species == 2) && fPrimaryFraction[species]) {
		for (Int_t iPtBin = 1; iPtBin <= hJetYield->GetNbinsX(); iPtBin++) {
			Double_t currentPrimFrac = fPrimaryFraction[species]->Eval(hJetYield->GetXaxis()->GetBinCenter(iPtBin));
			cout << "Current primary fraction: " << currentPrimFrac << endl;
			hJetYield->SetBinContent(iPtBin, hJetYield->GetBinContent(iPtBin) * currentPrimFrac);
			hBulkYield->SetBinContent(iPtBin, hBulkYield->GetBinContent(iPtBin) * currentPrimFrac);
		}
	}

	// TCanvas* cvs = TCanvas::MakeDefCanvas();
	// hMeasurements->Draw();

	// Setting names and colors.
	hBulkYield->SetLineColor(fSettings->GetSpeciesColor(species));
	hJetYield->SetLineColor(fSettings->GetSpeciesColor(species));
	hJetYieldNS->SetLineColor(fSettings->GetSpeciesColor(species));
	hJetYieldF->SetLineColor(fSettings->GetSpeciesColor(species));
	hJetYieldAS->SetLineColor(fSettings->GetSpeciesColor(species));

	hJetWidthSGDPhi->SetLineColor(fSettings->GetSpeciesColor(species));
	hJetWidthSGDEta->SetLineColor(fSettings->GetSpeciesColor(species));

	hV1->SetLineColor(fSettings->GetSpeciesColor(species));
	hV2->SetLineColor(fSettings->GetSpeciesColor(species));
	hV3->SetLineColor(fSettings->GetSpeciesColor(species));
	hV4->SetLineColor(fSettings->GetSpeciesColor(species));

	// Fixing histogram normalisation.
	if (fNTriggers > 1.) {
		hBulkYield->Scale(1./fNTriggers, "width");
		hBulkYield->GetYaxis()->SetTitle("#frac{1}{N_{trig} A} #frac{dN}{dp_{T}}");

		hJetYield->Scale(1./fNTriggers, "width");
		hJetYield->GetYaxis()->SetTitle("#frac{1}{N_{trig}} #frac{dN}{dp_{T}}");

		hJetYieldNS->Scale(1./fNTriggers, "width");
		hJetYieldNS->GetYaxis()->SetTitle("#frac{1}{N_{trig}} #frac{dN}{dp_{T}}");

		// hJetYieldF->Scale(1./fNTriggers,"width");
		// hJetYieldF->GetYaxis()->SetTitle("#frac{1}{N_{trig}} #frac{dN}{dp_{T}}");

		hJetYieldAS->Scale(1./fNTriggers,"width");
		hJetYieldAS->GetYaxis()->SetTitle("#frac{1}{N_{trig}} #frac{dN}{dp_{T}}");

	} else {
		hBulkYield->Scale(1.,"width");
		hBulkYield->GetYaxis()->SetTitle("#frac{1}{A}#frac{dN}{dp_{T}}");	

		hJetYield->Scale(1.,"width");
		hJetYield->GetYaxis()->SetTitle("#frac{dN}{dp_{T}}");

		hJetYieldNS->Scale(1.,"width");
		hJetYieldNS->GetYaxis()->SetTitle("#frac{dN}{dp_{T}}");

		// hJetYieldF->Scale(1.,"width");
		// hJetYieldF->GetYaxis()->SetTitle("#frac{dN}{dp_{T}}");

		hJetYieldAS->Scale(1.,"width");
		hJetYieldAS->GetYaxis()->SetTitle("#frac{dN}{dp_{T}}");
	}
	
	hJetWidthNS1->SetLineColor(fSettings->GetSpeciesColor(species));
	hJetWidthNS1->GetYaxis()->SetTitle("#sigma_{#Delta#phi}");
	
	hJetWidthNS2->SetLineColor(fSettings->GetSpeciesColor(species));
	hJetWidthNS2->GetYaxis()->SetTitle("#sigma_{#Delta#phi}");	
	
	hJetWidthAS->SetLineColor(fSettings->GetSpeciesColor(species));
	hJetWidthAS->GetYaxis()->SetTitle("#sigma_{#Delta#phi}");

	// Save to file.
	if (saveFitsToFile) {
		TFile* fout = new TFile(Form("./%s/Sp%i/FitPars.root", fPlotDir.Data(), species), "recreate");
		hBulkYield->Write();
		hJetYield->Write();

		hJetYieldNS->Write();
		hJetWidthNS1->Write();
		
		hJetYieldF->Write();
		hJetWidthNS2->Write();
		
		hJetYieldAS->Write();
		hJetWidthAS->Write();

		hJetWidthSGDPhi->Write();
		hJetWidthSGDEta->Write();

		hV1->Write();
		hV2->Write();
		hV3->Write();
		hV4->Write();

		fout->Close();
	}

	// Delete existing jet and bulk histograms.
	if (fJetYield[species]) {delete fJetYield[species]; fJetYield[species] = 0x0;}
	if (fJetWidthDPhi[species]) {delete fJetWidthDPhi[species]; fJetWidthDPhi[species] = 0x0;}
	if (fBulkYield[species]) {delete fBulkYield[species]; fBulkYield[species] = 0x0;}

	fJetYield[species] = hJetYield;
	fJetYield[species]->SetName(Form("jetYield%s", fSettings->GetSpeciesName(species)));

	fJetWidthDPhi[species] = hJetWidthNS1;
	fJetWidthDPhi[species]->SetName(Form("jetWidth%s", fSettings->GetSpeciesName(species)));

	fBulkYield[species] = hBulkYield;

	// Save to pdf.
	if (cvs) {
		cvs->cd();
		fJetWidthDPhi[species]->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/JetWidthDPhi.pdf", fPlotDir.Data(), species));

		cvs->SetLogy();
		fJetYield[species]->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/JetYield.pdf", fPlotDir.Data(), species));
		fBulkYield[species]->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/BulkYield.pdf", fPlotDir.Data(), species));
	}

	if (cvs) {delete cvs; cvs = 0x0;}


}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::MeasureJetAndBulkYieldUsingDPhiFit(AliSettingsDiHadronPID::Species species, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta, Bool_t saveFitsToFile) {

	// This method is similar as the method before, however it fits the resulting
	// histogram in Dphi with a Gaussian.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	// Create histograms for the jet Yield, Width and Bulk Width.
	TH1F* hJetYield = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetYield%s", fSettings->GetSpeciesName(species)), Form("Jet Yield From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hJetYield->SetDirectory(0);

	TH1F* hJetWidth = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetWidth%s", fSettings->GetSpeciesName(species)), Form("Jet Sigma From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hJetWidth->SetDirectory(0);

	TH1F* hBulkYield = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("bulkYield%s", fSettings->GetSpeciesName(species)), Form("Bulk Yield From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hBulkYield->SetDirectory(0);

	if (saveFitsToFile) {AliMiscToolsDiHadronPID::CreateDir(Form("./%s/Sp%i", fPlotDir.Data(), species));}

	TCanvas* cvs = 0x0;

	for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {

		Double_t fitpars[3] = {0.};
		Double_t fiterrors[3] = {0.};

		FitDPhiWithGauss(species, iPtBin, PeakMaxDPhi, PeakMaxDEta, BulkMinAbsDEta, fitpars, fiterrors, saveFitsToFile);

		if (cvs && saveFitsToFile) {
			cvs->SaveAs(Form("./%s/Sp%i/PtBin%02i.pdf", fPlotDir.Data(), species, iPtBin));
			delete cvs;
			cvs = 0x0;
		}	

		hJetYield->SetBinContent(iPtBin, fitpars[0]);
		hJetYield->SetBinError(iPtBin, fiterrors[0]);	

		hBulkYield->SetBinContent(iPtBin, fitpars[1]);
		hBulkYield->SetBinError(iPtBin, fiterrors[1]);

		hJetWidth->SetBinContent(iPtBin, fitpars[2]);
		hJetWidth->SetBinError(iPtBin, fiterrors[2]);

	}	

	// Setting names and colors.
	hJetYield->SetLineColor(fSettings->GetSpeciesColor(species));
	hBulkYield->SetLineColor(fSettings->GetSpeciesColor(species));

	// Fixing histogram normalisation.
	if (fNTriggers > 1.) {
		hJetYield->Scale(1./fNTriggers,"width");
		hJetYield->GetYaxis()->SetTitle("#frac{1}{N_{trig}} #frac{dN}{dp_{T}}");
		hBulkYield->Scale(1./fNTriggers,"width");
		hBulkYield->GetYaxis()->SetTitle("#frac{1}{N_{trig} A} #frac{dN}{dp_{T}}");
	} else {
		hJetYield->Scale(1.,"width");
		hJetYield->GetYaxis()->SetTitle("#frac{dN}{dp_{T}}");
		hBulkYield->Scale(1.,"width");
		hBulkYield->GetYaxis()->SetTitle("#frac{1}{A}#frac{dN}{dp_{T}}");	
	}
	
	hJetWidth->SetLineColor(fSettings->GetSpeciesColor(species));
	hJetWidth->GetYaxis()->SetTitle("#sigma_{#Delta#phi}");

	// Save to file.
	if (saveFitsToFile) {
		TFile* fout = new TFile(Form("./%s/Sp%i/FitPars.root", fPlotDir.Data(), species), "recreate");
		hJetYield->Write();
		hJetWidth->Write();
		hBulkYield->Write();
		fout->Close();
	}

	// Delete existing jet and bulk histograms.
	if (fJetYield[species]) {delete fJetYield[species]; fJetYield[species] = 0x0;}
	if (fJetWidthDPhi[species]) {delete fJetWidthDPhi[species]; fJetWidthDPhi[species] = 0x0;}
	if (fBulkYield[species]) {delete fBulkYield[species]; fBulkYield[species] = 0x0;}

	fJetYield[species] = hJetYield;
	fJetWidthDPhi[species] = hJetWidth;
	fBulkYield[species] = hBulkYield;

	// Save to pdf.
	if (saveFitsToFile) {
		cvs = TCanvas::MakeDefCanvas();
		hJetWidth->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/JetWidthDPhi.pdf", fPlotDir.Data(), species));

		cvs->SetLogy();
		hJetYield->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/JetYield.pdf", fPlotDir.Data(), species));
		hBulkYield->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/BulkYield.pdf", fPlotDir.Data(), species));
	}

	if (cvs) {delete cvs; cvs = 0x0;}

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::MeasureJetAndBulkYieldUsingDPhiFitDG(AliSettingsDiHadronPID::Species species, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta, Bool_t saveFitsToFile) {

	// This method is similar as the method before, however it fits the resulting
	// histogram in Dphi with a Gaussian.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	// Create histograms for the jet Yield, Width and Bulk Width.
	TH1F* hJetYield = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetYield%s", fSettings->GetSpeciesName(species)), Form("Jet Yield From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hJetYield->SetDirectory(0);

	TH1F* hJetWidth = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetWidth%s", fSettings->GetSpeciesName(species)), Form("Jet Sigma From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hJetWidth->SetDirectory(0);

	TH1F* hJetWidth2 = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetWidth2%s", fSettings->GetSpeciesName(species)), Form("Jet Sigma (Wide) From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hJetWidth2->SetDirectory(0);

	TH1F* hBulkYield = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("bulkYield%s", fSettings->GetSpeciesName(species)), Form("Bulk Yield From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hBulkYield->SetDirectory(0);

	if (saveFitsToFile) {AliMiscToolsDiHadronPID::CreateDir(Form("./%s/Sp%i", fPlotDir.Data(), species));}

	TCanvas* cvs = 0x0;

	for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {

		Double_t fitpars[4] = {0.};
		Double_t fiterrors[4] = {0.};

		FitDPhiWithDoubleGauss(species, iPtBin, PeakMaxDPhi, PeakMaxDEta, BulkMinAbsDEta, fitpars, fiterrors, saveFitsToFile);

		if (cvs && saveFitsToFile) {
			cvs->SaveAs(Form("./%s/Sp%i/PtBin%02i.pdf", fPlotDir.Data(), species, iPtBin));
			delete cvs;
			cvs = 0x0;
		}	

		hJetYield->SetBinContent(iPtBin, fitpars[0]);
		hJetYield->SetBinError(iPtBin, fiterrors[0]);	

		hBulkYield->SetBinContent(iPtBin, fitpars[1]);
		hBulkYield->SetBinError(iPtBin, fiterrors[1]);

		hJetWidth->SetBinContent(iPtBin, fitpars[2]);
		hJetWidth->SetBinError(iPtBin, fiterrors[2]);

		hJetWidth2->SetBinContent(iPtBin, fitpars[3]);
		hJetWidth2->SetBinError(iPtBin, fiterrors[3]);

	}	

	// Setting names and colors.
	hJetYield->SetLineColor(fSettings->GetSpeciesColor(species));
	hBulkYield->SetLineColor(fSettings->GetSpeciesColor(species));

	// Fixing histogram normalisation.
	if (fNTriggers > 1.) {
		hJetYield->Scale(1./fNTriggers,"width");
		hJetYield->GetYaxis()->SetTitle("#frac{1}{N_{trig}} #frac{dN}{dp_{T}}");
		hBulkYield->Scale(1./fNTriggers,"width");
		hBulkYield->GetYaxis()->SetTitle("#frac{1}{N_{trig} A} #frac{dN}{dp_{T}}");
	} else {
		hJetYield->Scale(1.,"width");
		hJetYield->GetYaxis()->SetTitle("#frac{dN}{dp_{T}}");
		hBulkYield->Scale(1.,"width");
		hBulkYield->GetYaxis()->SetTitle("#frac{1}{A}#frac{dN}{dp_{T}}");	
	}
	
	hJetWidth->SetLineColor(fSettings->GetSpeciesColor(species));
	hJetWidth->GetYaxis()->SetTitle("#sigma_{#Delta#phi}");

	// Save to file.
	if (saveFitsToFile) {
		TFile* fout = new TFile(Form("./%s/Sp%i/FitPars.root", fPlotDir.Data(), species), "recreate");
		hJetYield->Write();
		hJetWidth->Write();
		hJetWidth2->Write();
		hBulkYield->Write();
		fout->Close();
	}

	// Delete existing jet and bulk histograms.
	if (fJetYield[species]) {delete fJetYield[species]; fJetYield[species] = 0x0;}
	if (fJetWidthDPhi[species]) {delete fJetWidthDPhi[species]; fJetWidthDPhi[species] = 0x0;}
	if (fBulkYield[species]) {delete fBulkYield[species]; fBulkYield[species] = 0x0;}

	fJetYield[species] = hJetYield;
	fJetWidthDPhi[species] = hJetWidth;
	fBulkYield[species] = hBulkYield;

	// Save to pdf.
	if (saveFitsToFile) {
		cvs = TCanvas::MakeDefCanvas();
		hJetWidth->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/JetWidthDPhi.pdf", fPlotDir.Data(), species));
		hJetWidth2->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/JetWidth2DPhi.pdf", fPlotDir.Data(), species));

		cvs->SetLogy();
		hJetYield->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/JetYield.pdf", fPlotDir.Data(), species));
		hBulkYield->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/BulkYield.pdf", fPlotDir.Data(), species));
	}

	if (cvs) {delete cvs; cvs = 0x0;}

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::MeasureJetAndBulkYieldUsingDPhiFitTGNoBulkSubtraction(AliSettingsDiHadronPID::Species species, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta, Bool_t saveFitsToFile) {

	// This measurement takes a region DEta < DEtaJet, and performs a fit with a double gauss on the
	// near-side, and a single gauss on the away side.
	//
	// This method does not look at large delta-eta for background determination, and is therefore not
	// suitable if there are significant large delta-eta correlations (flow).

	// histogram in Dphi with a Gaussian.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	// Create histograms for the jet Yield, Width and Bulk Width.
	TH1F* hJetYieldNS = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetYieldNS%s", fSettings->GetSpeciesName(species)), Form("Jet Yield NS From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hJetYieldNS->SetDirectory(0);

	TH1F* hJetWidthNS = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetWidthNS%s", fSettings->GetSpeciesName(species)), Form("Jet Sigma NS From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hJetWidthNS->SetDirectory(0);

	TH1F* hJetYieldAS = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetYieldAS%s", fSettings->GetSpeciesName(species)), Form("Jet Yield AS From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hJetYieldAS->SetDirectory(0);

	TH1F* hJetWidthAS = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetWidthAS%s", fSettings->GetSpeciesName(species)), Form("Jet Sigma AS From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hJetWidthAS->SetDirectory(0);

	TH1F* hBulkYield = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("bulkYield%s", fSettings->GetSpeciesName(species)), Form("Bulk Yield From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hBulkYield->SetDirectory(0);

	if (saveFitsToFile) {AliMiscToolsDiHadronPID::CreateDir(Form("./%s/Sp%i", fPlotDir.Data(), species));}

	TCanvas* cvs = 0x0;

	for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {

		Double_t fitpars[14] = {0.};
		Double_t fiterrors[14] = {0.};

		FitDPhiWithTrippleGauss(species, iPtBin, PeakMaxDPhi, PeakMaxDEta, BulkMinAbsDEta, fitpars, fiterrors, saveFitsToFile);

		hBulkYield->SetBinContent(iPtBin, fitpars[0]);
		hBulkYield->SetBinError(iPtBin, fiterrors[0]);

		hJetYieldNS->SetBinContent(iPtBin, fitpars[1]);
		hJetYieldNS->SetBinError(iPtBin, fiterrors[1]);

		hJetYieldAS->SetBinContent(iPtBin, fitpars[3]);
		hJetYieldAS->SetBinError(iPtBin, fiterrors[3]);	

		hJetWidthNS->SetBinContent(iPtBin, fitpars[2]);
		hJetWidthNS->SetBinError(iPtBin, fiterrors[2]);

		hJetWidthAS->SetBinContent(iPtBin, fitpars[4]);
		hJetWidthAS->SetBinError(iPtBin, fiterrors[4]);

	}	

	// Setting names and colors.
	hJetYieldNS->SetLineColor(fSettings->GetSpeciesColor(species));
	hJetYieldAS->SetLineColor(fSettings->GetSpeciesColor(species));
	hBulkYield->SetLineColor(fSettings->GetSpeciesColor(species));

	// Fixing histogram normalisation.
	if (fNTriggers > 1.) {
		hJetYieldNS->Scale(1./fNTriggers,"width");
		hJetYieldNS->GetYaxis()->SetTitle("#frac{1}{N_{trig}} #frac{dN}{dp_{T}}");

		hJetYieldAS->Scale(1./fNTriggers,"width");
		hJetYieldAS->GetYaxis()->SetTitle("#frac{1}{N_{trig}} #frac{dN}{dp_{T}}");

		hBulkYield->Scale(1./fNTriggers,"width");
		hBulkYield->GetYaxis()->SetTitle("#frac{1}{N_{trig} A} #frac{dN}{dp_{T}}");
	} else {
		hJetYieldNS->Scale(1.,"width");
		hJetYieldNS->GetYaxis()->SetTitle("#frac{dN}{dp_{T}}");	

		hJetYieldAS->Scale(1.,"width");
		hJetYieldAS->GetYaxis()->SetTitle("#frac{dN}{dp_{T}}");
	
		hBulkYield->Scale(1.,"width");
		hBulkYield->GetYaxis()->SetTitle("#frac{1}{A}#frac{dN}{dp_{T}}");	
	}
	
	hJetWidthNS->SetLineColor(fSettings->GetSpeciesColor(species));
	hJetWidthNS->GetYaxis()->SetTitle("#sigma_{#Delta#phi}");

	hJetWidthAS->SetLineColor(fSettings->GetSpeciesColor(species));
	hJetWidthAS->GetYaxis()->SetTitle("#sigma_{#Delta#phi}");

	// Save to file.
	if (saveFitsToFile) {
		TFile* fout = new TFile(Form("./%s/Sp%i/FitPars.root", fPlotDir.Data(), species), "recreate");
		hJetYieldNS->Write();
		hJetYieldAS->Write();
		hJetWidthNS->Write();
		hJetWidthAS->Write();
		hBulkYield->Write();
		fout->Close();
	}

	// Delete existing jet and bulk histograms.
	if (fJetYield[species]) {delete fJetYield[species]; fJetYield[species] = 0x0;}
	if (fJetWidthDPhi[species]) {delete fJetWidthDPhi[species]; fJetWidthDPhi[species] = 0x0;}
	if (fBulkYield[species]) {delete fBulkYield[species]; fBulkYield[species] = 0x0;}

	fJetYield[species] = hJetYieldNS;
	fJetWidthDPhi[species] = hJetWidthNS;
	fBulkYield[species] = hBulkYield;

	// Save to pdf.
	if (saveFitsToFile) {
		cvs = TCanvas::MakeDefCanvas();
		hJetWidthNS->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/JetWidthNSDPhi.pdf", fPlotDir.Data(), species));
		hJetWidthAS->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/JetWidthASDPhi.pdf", fPlotDir.Data(), species));

		cvs->SetLogy();
		
		hJetYieldNS->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/JetYieldNS.pdf", fPlotDir.Data(), species));
		hJetYieldAS->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/JetYieldAS.pdf", fPlotDir.Data(), species));

		hBulkYield->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/BulkYield.pdf", fPlotDir.Data(), species));
	}

	if (cvs) {delete cvs; cvs = 0x0;}

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::MeasureJetAndBulkYieldUsingDEtaFit(AliSettingsDiHadronPID::Species species, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta, Bool_t useDoubleGauss, Bool_t saveFitsToFile) {

	// This method makes projecitons on DPhi of the near-side peak, and
	// performs a gaussian fit of the result.
	// Optionally, a pdf of the produced fits can be saved.

	if (saveFitsToFile) {AliMiscToolsDiHadronPID::CreateDir(Form("./%s/Sp%i", fPlotDir.Data(), species));}

	// Create histograms for the jet Yield, Width and Bulk Width.
	TH1F* hJetYield = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetYield%s", fSettings->GetSpeciesName(species)), Form("Jet Yield From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hJetYield->SetDirectory(0);

	TH1F* hJetWidth1 = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetWidth%s", fSettings->GetSpeciesName(species)), Form("Jet Sigma 1 From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hJetWidth1->SetDirectory(0);

	TH1F* hJetWidth2 = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetWidth2%s", fSettings->GetSpeciesName(species)), Form("Jet Sigma 2 From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hJetWidth2->SetDirectory(0);

	TH1F* hBulkYield = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("bulkYield%s", fSettings->GetSpeciesName(species)), Form("Bulk Yield From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hBulkYield->SetDirectory(0);

	TCanvas* cvs = 0x0;
	
	for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {
	
		Double_t fitpars[7] = {0.};
		Double_t fiterrors[7] = {0.};
		
	 	FitDEtaWithGauss(species, iPtBin, PeakMaxDPhi, PeakMaxDEta, BulkMinAbsDEta, fitpars, fiterrors, useDoubleGauss, saveFitsToFile);
		
		hJetYield->SetBinContent(iPtBin, fitpars[1]);
		hJetYield->SetBinError(iPtBin, fiterrors[1]);
		
		hJetWidth1->SetBinContent(iPtBin, fitpars[3]);
		hJetWidth1->SetBinError(iPtBin, fiterrors[3]);		

		hJetWidth2->SetBinContent(iPtBin, fitpars[6]);
		hJetWidth2->SetBinError(iPtBin, fiterrors[6]);		

		hBulkYield->SetBinContent(iPtBin, fitpars[0]);
		hBulkYield->SetBinError(iPtBin, fiterrors[0]);
		
	}
	
	// Setting names and colors.
	hJetYield->SetLineColor(fSettings->GetSpeciesColor(species));
	hBulkYield->SetLineColor(fSettings->GetSpeciesColor(species));

	if (fNTriggers > 1.) {
		hJetYield->Scale(1./fNTriggers,"width");
		hJetYield->GetYaxis()->SetTitle("#frac{1}{N_{trig}} #frac{dN}{dp_{T}}");
		hBulkYield->Scale(1./fNTriggers,"width");
		hBulkYield->GetYaxis()->SetTitle("#frac{1}{N_{trig} A} #frac{dN}{dp_{T}}");
	} else {
		hJetYield->Scale(1.,"width");
		hJetYield->GetYaxis()->SetTitle("#frac{dN}{dp_{T}}");
		hBulkYield->Scale(1.,"width");
		hBulkYield->GetYaxis()->SetTitle("#frac{1}{A}#frac{dN}{dp_{T}}");	
	}
	
	hJetWidth1->SetLineColor(fSettings->GetSpeciesColor(species));
	hJetWidth1->GetYaxis()->SetTitle("#sigma_{#Delta#eta}");

	hJetWidth2->SetLineColor(fSettings->GetSpeciesColor(species));
	hJetWidth2->GetYaxis()->SetTitle("#sigma_{#Delta#eta}");

	// Save to file.
	if (saveFitsToFile) {
		TFile* fout = new TFile(Form("./%s/Sp%i/FitPars.root", fPlotDir.Data(), species), "recreate");
		hJetYield->Write();
		hJetWidth1->Write();
		hJetWidth2->Write();
		hBulkYield->Write();
		fout->Close();
	}

	// Delete existing jet and bulk histograms.
	if (fJetYield[species]) {delete fJetYield[species]; fJetYield[species] = 0x0;}
	if (fJetWidthDEta[species]) {delete fJetWidthDEta[species]; fJetWidthDEta[species] = 0x0;} 
	if (fBulkYield[species]) {delete fBulkYield[species]; fBulkYield[species] = 0x0;}

	fJetYield[species] = hJetYield;
	fJetWidthDEta[species] = hJetWidth1;
	fBulkYield[species] = hBulkYield;

	// Save to pdf.
	if (saveFitsToFile) {
		cvs = TCanvas::MakeDefCanvas();
		hJetWidth1->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/JetWidth1DEta.pdf", fPlotDir.Data(), species));
		hJetWidth2->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/JetWidth2DEta.pdf", fPlotDir.Data(), species));
		
		cvs->SetLogy();
		hJetYield->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/JetYield.pdf", fPlotDir.Data(), species));
		hBulkYield->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/BulkYield.pdf", fPlotDir.Data(), species));
	}

	if (cvs) {delete cvs; cvs = 0x0;}
}
	
// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::MeasureVnUsingBulkFit(AliSettingsDiHadronPID::Species species, Double_t BulkMinAbsDEta, Bool_t saveFitsToFile) const {

	if (saveFitsToFile) {AliMiscToolsDiHadronPID::CreateDir(Form("./%s/Sp%i", fPlotDir.Data(), species));}

	TH1F* hBulkYield = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("bulkYield%s", fSettings->GetSpeciesName(species)), Form("bulkYield with #Delta#eta gap %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hBulkYield->SetDirectory(0);

	TH1F* hV1 = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("v1%s", fSettings->GetSpeciesName(species)), Form("v_{1#Delta} with #Delta#eta gap %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hV1->SetDirectory(0);

	TH1F* hV2 = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("v2%s", fSettings->GetSpeciesName(species)), Form("v_{2#Delta} with #Delta#eta gap %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hV2->SetDirectory(0);	

	TH1F* hV3 = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("v3%s", fSettings->GetSpeciesName(species)), Form("v_{3#Delta} with #Delta#eta gap %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hV3->SetDirectory(0);

	TCanvas* cvs = 0x0;

	for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {

		Double_t fitpars[8] = {0.};
		Double_t fiterrors[8] = {0.};

		FitBulkWithConstVn(species, iPtBin, BulkMinAbsDEta, fitpars, fiterrors, saveFitsToFile);

		hBulkYield->SetBinContent(iPtBin, fitpars[0]);
		hBulkYield->SetBinError(iPtBin, fiterrors[0]);	

		hV1->SetBinContent(iPtBin, fitpars[1]);
		hV1->SetBinError(iPtBin, fiterrors[1]);

		hV2->SetBinContent(iPtBin, fitpars[2]);
		hV2->SetBinError(iPtBin, fiterrors[2]);

		hV3->SetBinContent(iPtBin, fitpars[3]);
		hV3->SetBinError(iPtBin, fiterrors[3]);

	}	

	// Setting names and colors.
	hBulkYield->SetLineColor(fSettings->GetSpeciesColor(species));

	if (fNTriggers > 1.) {
		hBulkYield->Scale(fNTriggers, "width");
		hBulkYield->GetYaxis()->SetTitle("#frac{1}{N_{trig} A} #frac{dN}{dp_{T}}");
	} else {
		hBulkYield->Scale(1.,"width");
		hBulkYield->GetYaxis()->SetTitle("#frac{1}{A}#frac{dN}{dp_{T}}");	
	}
	

	hV1->SetLineColor(fSettings->GetSpeciesColor(species));
	hV1->GetYaxis()->SetTitle("v_{1#Delta}");
	
	hV2->SetLineColor(fSettings->GetSpeciesColor(species));
	hV2->GetYaxis()->SetTitle("v_{2#Delta}");
	
	hV3->SetLineColor(fSettings->GetSpeciesColor(species));
	hV3->GetYaxis()->SetTitle("v_{3#Delta}");

	// Save to file.
	if (saveFitsToFile) {
		TFile* fout = new TFile(Form("./%s/Sp%i/FitPars.root", fPlotDir.Data(), species), "recreate");
		hBulkYield->Write();
		hV1->Write();
		hV2->Write();
		hV3->Write();
		fout->Close();
	}

	// Save to pdf.
	if (saveFitsToFile) {
		cvs = TCanvas::MakeDefCanvas();

		cvs->SetLogy(kTRUE);
		hBulkYield->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/BulkYield.pdf", fPlotDir.Data(), species));

		cvs->SetLogy(kFALSE);
		hV1->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/vDelta1.pdf", fPlotDir.Data(), species));

		hV2->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/vDelta2.pdf", fPlotDir.Data(), species));

		hV3->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/vDelta3.pdf", fPlotDir.Data(), species));


		if (cvs) {delete cvs; cvs = 0x0;}

	}


}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::MeasureAwaySideFlatnessDEta(AliSettingsDiHadronPID::Species species, Bool_t saveFitsToFile) const {

	if (saveFitsToFile) {AliMiscToolsDiHadronPID::CreateDir(Form("./%s/Sp%i", fPlotDir.Data(), species));}

	TH1F* hA0 = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("hA0_%s", fSettings->GetSpeciesName(species)), Form("Away side a_{0} %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hA0->SetDirectory(0);

	TH1F* hA2 = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("hA2_%s", fSettings->GetSpeciesName(species)), Form("Away side a_{2} %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hA2->SetDirectory(0);

	TCanvas* cvs = 0x0;

	for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {

		Double_t fitpars[2] = {0.};
		Double_t fiterrors[2] = {0.};

		FitDEtaWithParabola(species, iPtBin, TMath::Pi()/2., 3.*TMath::Pi()/2., fitpars, fiterrors, saveFitsToFile);

		hA0->SetBinContent(iPtBin, fitpars[0]);
		hA0->SetBinError(iPtBin, fiterrors[0]);	

		hA2->SetBinContent(iPtBin, fitpars[1]);
		hA2->SetBinError(iPtBin, fiterrors[1]);

	}	

	// Setting names and colors.
	hA0->SetLineColor(fSettings->GetSpeciesColor(species));
	hA0->GetYaxis()->SetTitle("a_{0}");

	hA2->SetLineColor(fSettings->GetSpeciesColor(species));
	hA2->GetYaxis()->SetTitle("a_{2}");

	// Save to file.
	if (saveFitsToFile) {
		TFile* fout = new TFile(Form("./%s/Sp%i/FitPars.root", fPlotDir.Data(), species), "recreate");
		hA0->Write();
		hA2->Write();
		fout->Close();
	}

	// Save to pdf.
	if (saveFitsToFile) {
		cvs = TCanvas::MakeDefCanvas();

		hA0->Draw();
		cvs->SaveAs(Form("./%s/Sp%i/hA0.pdf", fPlotDir.Data(), species));

		// Create fit function (const)
		TF1* constFunc = new TF1("constFunc", "[0]", 0., 10.);
		constFunc->SetParNames("const");
		constFunc->SetParameter(0, 0.);
		constFunc->SetLineColor(kBlack);

		hA2->Fit(constFunc, "QI");

		cvs->SaveAs(Form("./%s/Sp%i/hA2.pdf", fPlotDir.Data(), species));

		if (cvs) {delete cvs; cvs = 0x0;}

	}

}

// -----------------------------------------------------------------------
/*TH1F* AliRatiosDiHadronPID::MeasureJetAndBulkYieldUsingDPhiFit(AliSettingsDiHadronPID::Species species, Bool_t saveFitsToFile) {

	// This method makes projecitons on DPhi of the near-side peak, and
	// performs a gaussian fit of the result.
	// Optionally, a pdf of the produced fits can be saved.

	if (saveFitsToFile) {AliMiscToolsDiHadronPID::CreateDir(Form("./%s/Sp%i", fPlotDir.Data(), species));}

	// Create histograms for the jet Yield, Width and Bulk Width.
	TH1F* hJetYield = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetYield%s", fSettings->GetSpeciesName(species)), Form("Jet Yield From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hJetYield->SetDirectory(0);

	TH1F* hV1 = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("v1%s", fSettings->GetSpeciesName(species)), Form("v1 From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hV1->SetDirectory(0);

	TH1F* hV2 = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("v2%s", fSettings->GetSpeciesName(species)), Form("v2 From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hV2->SetDirectory(0);

	TH1F* hJetWidth = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetWidth%s", fSettings->GetSpeciesName(species)), Form("Jet Sigma From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hJetWidth->SetDirectory(0);

	TH1F* hBulkYield = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("bulkYield%s", fSettings->GetSpeciesName(species)), Form("Bulk Yield From 1D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hBulkYield->SetDirectory(0);

	TCanvas* cvs = 0x0;
	
	// Determine flow coeficiencts.
	for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {
	
		Double_t fitpars[3] = {0.};
		Double_t fiterrors[3] = {0.};
		
	 	cvs = 0x0;//FitDPhiCorrectedOnlyFlow(species, iPtBin, 0.6, 1.5, fitpars, fiterrors);
		
		if (saveFitsToFile && cvs) {
			cvs->SaveAs(Form("./%s/Sp%i/FlowPtBin%02i.pdf", fPlotDir.Data(), species, iPtBin));
			delete cvs;
			cvs = 0x0;
		}
	}	
		
	for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {
	
		Double_t fitpars[6] = {0.};
		Double_t fiterrors[6] = {0.};
		
	 	cvs = FitDPhiCorrected(species, iPtBin, -0.4, 0.4, fitpars, fiterrors);
		
		if (saveFitsToFile && cvs) {
			cvs->SaveAs(Form("./%s/Sp%i/PtBin%02i.pdf", fPlotDir.Data(), species, iPtBin));
			delete cvs;
			cvs = 0x0;
		}
		
		hBulkYield->SetBinContent(iPtBin, fitpars[0]);
		hBulkYield->SetBinError(iPtBin, fiterrors[0]);

		hJetYield->SetBinContent(iPtBin, fitpars[1]);
		hJetYield->SetBinError(iPtBin, fiterrors[1]);
		
		hJetWidth->SetBinContent(iPtBin, fitpars[2]);
		hJetWidth->SetBinError(iPtBin, fiterrors[2]);		

		hV1->SetBinContent(iPtBin, fitpars[3]);
		hV1->SetBinError(iPtBin, fiterrors[3]);

		hV2->SetBinContent(iPtBin, fitpars[4]);
		hV2->SetBinError(iPtBin, fiterrors[4]);

		
	}
	
	hJetYield->SetLineColor(fSettings->GetSpeciesColor(species));
	hJetYield->Scale(1.,"width");
	hJetYield->GetYaxis()->SetTitle("dN/dp_{T}");

	hV2->SetLineColor(fSettings->GetSpeciesColor(species));
	hV2->GetYaxis()->SetTitle("v_{2}");		

	hJetWidth->SetLineColor(fSettings->GetSpeciesColor(species));
	hJetWidth->GetYaxis()->SetTitle("#sigma_{#Delta#phi}");

	hBulkYield->SetLineColor(fSettings->GetSpeciesColor(species));
	hBulkYield->Scale(1.,"width");
	hBulkYield->GetYaxis()->SetTitle("dN/dp_{T}");		

	// Save to file.
	if (saveFitsToFile) {
		TFile* fout = new TFile(Form("./%s/Sp%i/FitPars.root", fPlotDir.Data(), species), "recreate");
		hJetYield->Write();
		hV1->Write();
		hV2->Write();
		hJetWidth->Write();
		hBulkYield->Write();
		fout->Close();
	}

	// Delete existing jet and bulk histograms.
	if (fJetYield[species]) {delete fJetYield[species]; fJetYield[species] = 0x0;}
	if (fBulkYield[species]) {delete fBulkYield[species]; fBulkYield[species] = 0x0;}

	fJetYield[species] = hJetYield;
	fBulkYield[species] = hBulkYield;

	// delete histograms from other fit parameters.
	if (hJetWidth) {delete hJetWidth; hJetWidth = 0x0;}
	if (hV1) {delete hV1; hV1 = 0x0;}
	if (hV2) {delete hV2; hV2 = 0x0;}

	return hJetYield;
	
}

// -----------------------------------------------------------------------
TH1F* AliRatiosDiHadronPID::MeasureJetAndBulkYieldUsingDPhiDEtaFit(AliSettingsDiHadronPID::Species species) {

	// This method presents a number of derived plots.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	AliMiscToolsDiHadronPID::CreateDir(Form("./%s/Sp%i", fPlotDir.Data(), species));

	// Create histograms for the jet Yield, Width and Bulk Width.
	TH1F* hJetYield = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetYield%s", fSettings->GetSpeciesName(species)), Form("Jet Yield From 2D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hJetYield->SetDirectory(0);

	TH1F* hV2 = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("v2%s", fSettings->GetSpeciesName(species)), Form("v2 From 2D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hV2->SetDirectory(0);

	TH1F* hJetWidthDPhi = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetWidthDPhi%s", fSettings->GetSpeciesName(species)), Form("Jet DPhi Sigma From 2D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hJetWidthDPhi->SetDirectory(0);

	TH1F* hJetWidthDEta = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("jetWidthDEta%s", fSettings->GetSpeciesName(species)), Form("Jet DEta Sigma From 2D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hJetWidthDEta->SetDirectory(0);

	TH1F* hBulkYield = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("bulkYield%s", fSettings->GetSpeciesName(species)), Form("Bulk Yield From 2D Fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis);
	hBulkYield->SetDirectory(0);

	// TCanvas* cvs = 0x0;
	
	for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {
	
		Double_t fitpars[7] = {0.};
		Double_t fiterrors[7] = {0.};
		
		FitDPhiDEtaCorrectedNearSide(species, iPtBin, fitpars, fiterrors);
				
		hJetYield->SetBinContent(iPtBin, fitpars[1]);
		hJetYield->SetBinError(iPtBin, fiterrors[1]);

		hV2->SetBinContent(iPtBin, fitpars[2]);
		hV2->SetBinError(iPtBin, fiterrors[2]);

		hJetWidthDPhi->SetBinContent(iPtBin, fitpars[4]);
		hJetWidthDPhi->SetBinError(iPtBin, fiterrors[4]);

		hJetWidthDEta->SetBinContent(iPtBin, fitpars[6]);
		hJetWidthDEta->SetBinError(iPtBin, fiterrors[6]);		

		hBulkYield->SetBinContent(iPtBin, fitpars[0]);
		hBulkYield->SetBinError(iPtBin, fiterrors[0]);
		
	}
	
	hJetYield->SetLineColor(fSettings->GetSpeciesColor(species));
	hJetYield->Scale(1.,"width");
	hJetYield->GetYaxis()->SetTitle("dN/dp_{T}");

	hV2->SetLineColor(fSettings->GetSpeciesColor(species));
	hV2->GetYaxis()->SetTitle("v_{2}");	

	hJetWidthDPhi->SetLineColor(fSettings->GetSpeciesColor(species));
	hJetWidthDPhi->GetYaxis()->SetTitle("#sigma_{#Delta#phi}");	

	hJetWidthDEta->SetLineColor(fSettings->GetSpeciesColor(species));
	hJetWidthDEta->GetYaxis()->SetTitle("#sigma_{#Delta#eta}");

	hBulkYield->SetLineColor(fSettings->GetSpeciesColor(species));
	hBulkYield->Scale(1.,"width");
	hBulkYield->GetYaxis()->SetTitle("dN/dp_{T}");		

	// Save to file.
	TFile* fout = new TFile(Form("./%s/Sp%i/FitPars.root", fPlotDir.Data(), species), "recreate");
	hJetYield->Write();
	hV2->Write();
	hJetWidthDPhi->Write();
	hJetWidthDEta->Write();
	hBulkYield->Write();
	fout->Close();

	// Delete existing jet and bulk histograms.
	if (fJetYield[species]) {delete fJetYield[species]; fJetYield[species] = 0x0;}
	if (fBulkYield[species]) {delete fBulkYield[species]; fBulkYield[species] = 0x0;}

	fJetYield[species] = hJetYield;
	fBulkYield[species] = hBulkYield;

	// delete histograms from other fit parameters.
	if (hV2) {delete hV2; hV2 = 0x0;}
	if (hJetWidthDPhi) {delete hJetWidthDPhi; hJetWidthDPhi = 0x0;}
	if (hJetWidthDEta) {delete hJetWidthDEta; hJetWidthDEta = 0x0;}

	return hJetYield;

}
*/
// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::RandomiseCorrelations() {

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());	

	AliHistToolsDiHadronPID::RandomizeCollectionOfHist(fIdentifiedCorrelationsPion);
	AliHistToolsDiHadronPID::RandomizeCollectionOfHist(fIdentifiedCorrelationsKaon);
	AliHistToolsDiHadronPID::RandomizeCollectionOfHist(fIdentifiedCorrelationsProton);

	AliHistToolsDiHadronPID::RandomizeCollectionOfHist(fIdentifiedMixedEventsPion);
	AliHistToolsDiHadronPID::RandomizeCollectionOfHist(fIdentifiedMixedEventsKaon);
	AliHistToolsDiHadronPID::RandomizeCollectionOfHist(fIdentifiedMixedEventsProton);

}

// -----------------------------------------------------------------------
TH2F* AliRatiosDiHadronPID::SystErrDEtaFitVSDPhiFit(AliSettingsDiHadronPID::Species species, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta) {

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());	

	// Create output histogram.
	TAxis* ratioAxis = new TAxis(100, 0.5, 1.5);
	ratioAxis->SetTitle("Ratio");
	TH2F* hRatioHist = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("hRatioHist%s", fSettings->GetSpeciesName(species)), Form("DEtaFit vs BulkAndDPhiFit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis, ratioAxis);
	delete ratioAxis;

	// Repeat N times.
	for (Int_t iTrial = 0; iTrial < fNTrialsForSystematics; iTrial++) {
			
		if (iTrial % 100 == 0) {cout << "Trial: " << iTrial << endl;}
			
		// Create a clone of self (want to redo this every time!).
		AliRatiosDiHadronPID* clone = new AliRatiosDiHadronPID(*this);

		// Randomise all the correlations according to stat errors.
		clone->RandomiseCorrelations();

		// Calculate jet yields through dEta fit.
		clone->MeasureJetAndBulkYieldUsingDEtaFit(species, PeakMaxDPhi, PeakMaxDEta, BulkMinAbsDEta, kFALSE, kFALSE);
		TH1F* jetYieldRatio	= clone->GetJetYieldClone(species);
		jetYieldRatio->SetDirectory(0);

		// Calculate the jet yields through side band + dphi fit.
		clone->MeasureJetAndBulkYieldUsingDPhiFit(species, PeakMaxDPhi, PeakMaxDEta, BulkMinAbsDEta, kFALSE);
		TH1F* jetYield2 = clone->GetJetYieldClone(species);

		// Make division, and store into 2D histogram.
		jetYieldRatio->Divide(jetYield2);
		
		delete jetYield2; jetYield2 = 0x0;
		
		for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {
		
			hRatioHist->Fill(fPtAxis->GetBinCenter(iPtBin), jetYieldRatio->GetBinContent(iPtBin));
			
		}
		
		delete jetYieldRatio; jetYieldRatio = 0x0;
		delete clone; clone = 0x0;
	
	}

	TFile* fout = new TFile(Form("DEtaFitvsBulkAndDPhiFit%s.root", fSettings->GetSpeciesName(species)), "recreate");
	hRatioHist->Write();
	fout->Close();

	TCanvas* cvs = TCanvas::MakeDefCanvas();
	hRatioHist->Draw("colz");
	cvs->SaveAs(Form("DEtaFitvsBulkAndDPhiFit%s.pdf", fSettings->GetSpeciesName(species)));
	delete cvs; cvs = 0x0;

	return hRatioHist;

}

// -----------------------------------------------------------------------
TH2F* AliRatiosDiHadronPID::SystErrBulkFitVSDPhiFit(AliSettingsDiHadronPID::Species species, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta) {

	// This method attempts to find the systematic error to the peak
	// yield extraction by comparing the bulk fit and the bulk/dphi fit.
	// To get rid of statistical fluctuations, we take the corrected signal,
	// vary each bin with a random amount, distributed according to the stat.
	// error on that bin.

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());	

	// Create output histogram.
	TAxis* ratioAxis = new TAxis(100, 0.5, 1.5);
	ratioAxis->SetTitle("Ratio");
	TH2F* hRatioHist = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("hRatioHist%s", fSettings->GetSpeciesName(species)), Form("Regions vs BulkAndDPhiFit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis, ratioAxis);
	delete ratioAxis;

	// Repeat N times.
	for (Int_t iTrial = 0; iTrial < fNTrialsForSystematics; iTrial++) {
			
		if (iTrial % 100 == 0) {cout << "Trial: " << iTrial << endl;}
			
		// Create a clone of self (want to redo this every time!).
		AliRatiosDiHadronPID* clone = new AliRatiosDiHadronPID(*this);

		// Randomise all the correlations according to stat errors.
		clone->RandomiseCorrelations();

		// Calculate jet yields through side band fit.
		clone->MeasureJetAndBulkYieldUsingBulkFit(species, PeakMaxDPhi, PeakMaxDEta, BulkMinAbsDEta);
		TH1F* jetYieldRatio	= clone->GetJetYieldClone(species);
		jetYieldRatio->SetDirectory(0);

		// Calculate the jet yields through side band + dphi fit.
		clone->MeasureJetAndBulkYieldUsingDPhiFit(species, PeakMaxDPhi, PeakMaxDEta, BulkMinAbsDEta);
		TH1F* jetYield2 = clone->GetJetYieldClone(species);

		// Make division, and store into 2D histogram.
		jetYieldRatio->Divide(jetYield2);
		
		delete jetYield2; jetYield2 = 0x0;
		
		for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {
		
			hRatioHist->Fill(fPtAxis->GetBinCenter(iPtBin), jetYieldRatio->GetBinContent(iPtBin));
			
		}
		
		delete jetYieldRatio; jetYieldRatio = 0x0;
		delete clone; clone = 0x0;
	
	}

	TFile* fout = new TFile(Form("RegionsvsBulkAndDPhiFit%s.root", fSettings->GetSpeciesName(species)), "recreate");
	hRatioHist->Write();
	fout->Close();

	TCanvas* cvs = TCanvas::MakeDefCanvas();
	hRatioHist->Draw("colz");
	cvs->SaveAs(Form("RegionsvsBulkAndDPhiFit%s.pdf", fSettings->GetSpeciesName(species)));
	delete cvs; cvs = 0x0;


	return hRatioHist;

}// -----------------------------------------------------------------------
TH2F* AliRatiosDiHadronPID::SystErrBulkFitVSDPhiFitDG(AliSettingsDiHadronPID::Species species, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta) {

	// This method attempts to find the systematic error to the peak
	// yield extraction by comparing the bulk fit and the bulk/dphi fit.
	// To get rid of statistical fluctuations, we take the corrected signal,
	// vary each bin with a random amount, distributed according to the stat.
	// error on that bin.

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());	

	// Create output histogram.
	TAxis* ratioAxis = new TAxis(100, 0.5, 1.5);
	ratioAxis->SetTitle("Ratio");
	TH2F* hRatioHist = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("hRatioHist%s", fSettings->GetSpeciesName(species)), Form("Regions vs BulkAndDPhiFit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis, ratioAxis);
	delete ratioAxis;

	// Repeat N times.
	for (Int_t iTrial = 0; iTrial < fNTrialsForSystematics; iTrial++) {
			
		if (iTrial % 100 == 0) {cout << "Trial: " << iTrial << endl;}
			
		// Create a clone of self (want to redo this every time!).
		AliRatiosDiHadronPID* clone = new AliRatiosDiHadronPID(*this);

		// Randomise all the correlations according to stat errors.
		clone->RandomiseCorrelations();

		// Calculate jet yields through side band fit.
		clone->MeasureJetAndBulkYieldUsingBulkFit(species, PeakMaxDPhi, PeakMaxDEta, BulkMinAbsDEta);
		TH1F* jetYieldRatio	= clone->GetJetYieldClone(species);
		jetYieldRatio->SetDirectory(0);

		// Calculate the jet yields through side band + dphi fit.
		clone->MeasureJetAndBulkYieldUsingDPhiFitDG(species, PeakMaxDPhi, PeakMaxDEta, BulkMinAbsDEta);
		TH1F* jetYield2 = clone->GetJetYieldClone(species);

		// Make division, and store into 2D histogram.
		jetYieldRatio->Divide(jetYield2);
		
		delete jetYield2; jetYield2 = 0x0;
		
		for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {
		
			hRatioHist->Fill(fPtAxis->GetBinCenter(iPtBin), jetYieldRatio->GetBinContent(iPtBin));
			
		}
		
		delete jetYieldRatio; jetYieldRatio = 0x0;
		delete clone; clone = 0x0;
	
	}

	TFile* fout = new TFile(Form("RegionsvsBulkAndDPhiFitDG%s.root", fSettings->GetSpeciesName(species)), "recreate");
	hRatioHist->Write();
	fout->Close();

	TCanvas* cvs = TCanvas::MakeDefCanvas();
	hRatioHist->Draw("colz");
	cvs->SaveAs(Form("RegionsvsBulkAndDPhiFitDG%s.pdf", fSettings->GetSpeciesName(species)));
	delete cvs; cvs = 0x0;


	return hRatioHist;

}

// -----------------------------------------------------------------------
TH2F* AliRatiosDiHadronPID::SystErrBulkFitVSDEtaFit(AliSettingsDiHadronPID::Species species, Double_t PeakMaxDPhi, Double_t PeakMaxDEta, Double_t BulkMinAbsDEta) {

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());	

	// Create output histogram.
	TAxis* ratioAxis = new TAxis(100, 0.5, 1.5);
	ratioAxis->SetTitle("Ratio");
	TH2F* hRatioHist = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("hRatioHist%s", fSettings->GetSpeciesName(species)), Form("Regions vs DEta fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis, ratioAxis);
	delete ratioAxis;

	// Repeat N times.
	for (Int_t iTrial = 0; iTrial < fNTrialsForSystematics; iTrial++) {
			
		if (iTrial % 100 == 0) {cout << "Trial: " << iTrial << endl;}

		// Create a clone of self (want to redo this every time!).
		AliRatiosDiHadronPID* clone = new AliRatiosDiHadronPID(*this);

		// Randomise all the correlations according to stat errors.
		clone->RandomiseCorrelations();

		// Calculate jet yields through side band fit.
		clone->MeasureJetAndBulkYieldUsingBulkFit(species, PeakMaxDPhi, PeakMaxDEta, BulkMinAbsDEta);
		TH1F* jetYieldRatio	= clone->GetJetYieldClone(species);
		jetYieldRatio->SetDirectory(0);

		// Calculate the jet yields through side band + dphi fit.
		clone->MeasureJetAndBulkYieldUsingDEtaFit(species, PeakMaxDPhi, PeakMaxDEta, BulkMinAbsDEta, kFALSE, kFALSE);
		TH1F* jetYield2 = clone->GetJetYieldClone(species);

		// Make division, and store into 2D histogram.
		jetYieldRatio->Divide(jetYield2);
		
		delete jetYield2; jetYield2 = 0x0;
		
		for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {
		
			hRatioHist->Fill(fPtAxis->GetBinCenter(iPtBin), jetYieldRatio->GetBinContent(iPtBin));
			
		}
		
		delete jetYieldRatio; jetYieldRatio = 0x0;
		delete clone; clone = 0x0;
	
	}

	TFile* fout = new TFile(Form("RegionsvsDEtaFit%s.root", fSettings->GetSpeciesName(species)), "recreate");
	hRatioHist->Write();
	fout->Close();

	TCanvas* cvs = TCanvas::MakeDefCanvas();
	hRatioHist->Draw("colz");
	cvs->SaveAs(Form("RegionsvsDEtaFit%s.pdf", fSettings->GetSpeciesName(species)));
	delete cvs; cvs = 0x0;

	return hRatioHist;

}

// -----------------------------------------------------------------------
TH2F* AliRatiosDiHadronPID::SystErrBulkFitTwoSizes(AliSettingsDiHadronPID::Species species, Double_t PeakMaxDPhi1, Double_t PeakMaxDEta1, Double_t BulkMinAbsDEta1, Double_t PeakMaxDPhi2, Double_t PeakMaxDEta2, Double_t BulkMinAbsDEta2) {

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());	

	// Create output histogram.
	TAxis* ratioAxis = new TAxis(100, 0.5, 1.5);
	ratioAxis->SetTitle("Ratio");
	TH2F* hRatioHist = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("hRatioHist%s", fSettings->GetSpeciesName(species)), Form("Regions vs DEta fit %s", fSettings->GetSpeciesName(species)), "F", fPtAxis, ratioAxis);

	// Repeat N times.
	for (Int_t iTrial = 0; iTrial < fNTrialsForSystematics; iTrial++) {
			
		if (iTrial % 100 == 0) {cout << "Trial: " << iTrial << endl;}

		// Create a clone of self (want to redo this every time!).
		AliRatiosDiHadronPID* clone = new AliRatiosDiHadronPID(*this);

		// Randomise all the correlations according to stat errors.
		clone->RandomiseCorrelations();

		// Calculate jet yields through side band fit.
		clone->MeasureJetAndBulkYieldUsingBulkFit(species, PeakMaxDPhi1, PeakMaxDEta1, BulkMinAbsDEta1);
		TH1F* jetYieldRatio	= clone->GetJetYieldClone(species);
		jetYieldRatio->SetDirectory(0);

		// Calculate the jet yields through side band + dphi fit.
		clone->MeasureJetAndBulkYieldUsingBulkFit(species, PeakMaxDPhi2, PeakMaxDEta2, BulkMinAbsDEta2);
		TH1F* jetYield2 = clone->GetJetYieldClone(species);

		// Make division, and store into 2D histogram.
		jetYieldRatio->Divide(jetYield2);
		
		delete jetYield2; jetYield2 = 0x0;
		
		for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {
		
			hRatioHist->Fill(fPtAxis->GetBinCenter(iPtBin), jetYieldRatio->GetBinContent(iPtBin));
			
		}
		
		delete jetYieldRatio; jetYieldRatio = 0x0;
		delete clone; clone = 0x0;
	
	}

	TFile* fout = new TFile(Form("BulkFitTwoSizes%s.root", fSettings->GetSpeciesName(species)), "recreate");
	hRatioHist->Write();
	fout->Close();

	TCanvas* cvs = TCanvas::MakeDefCanvas();
	hRatioHist->Draw("colz");
	cvs->SaveAs(Form("BulkFitTwoSizes%s.pdf", fSettings->GetSpeciesName(species)));
	delete cvs; cvs = 0x0;

	return hRatioHist;

}

// -----------------------------------------------------------------------
TH1F* AliRatiosDiHadronPID::SystErrPIDMethod(const AliRatiosDiHadronPID* mcRatios) {

	// This method takes a ratios object with MC truth data, divides the 
	// (DPhi, DEta) regions, and projects onto the pT axis.

	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	// TFile* fout = TFile::Open("DiHadronPIDFitOverTruth.root", "recreate");

	// Loop over species.
	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		
		AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
		TH1F* systErr = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("systErr%s", fSettings->GetSpeciesName(currentSpecies)), Form("systErr%s", fSettings->GetSpeciesName(currentSpecies)), "F", fPtAxis);
		systErr->SetTitle(Form("Fit Over Truth - %s", fSettings->GetSpeciesName(currentSpecies)));
		systErr->SetDirectory(0);

		// fout->

		// Loop over pt bins.
		for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {

			TH2F* thisCorrected = GetCorrectedClone(currentSpecies, iPtBin);
			TH2F* otherCorrected = mcRatios->GetCorrectedClone(currentSpecies, iPtBin);
			thisCorrected->Divide(otherCorrected);
			thisCorrected->SetDirectory(0);

			Double_t fitval = 0.;
			Double_t fiterr = 0.;
			AliHistToolsDiHadronPID::ConstFit(thisCorrected, fitval, fiterr);
			systErr->SetBinContent(iPtBin, fitval);
			systErr->SetBinError(iPtBin, fiterr);

		}

		TCanvas::MakeDefCanvas();
		systErr->Draw();
	
	}

	//thisCorrected->Draw("surf1");	
	return 0x0;

}

// -----------------------------------------------------------------------
Bool_t AliRatiosDiHadronPID::MakeRatios() {

	// This method expects the ptrs fJetYield and fBulkYield to be set,
	// and generates the particle ratios.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
			
		if (!(fJetYield[iSpecies])) {return kFALSE;}
		if (!(fBulkYield[iSpecies])) {return kFALSE;}

	}

	// Make the ratios.
	if (fJetPrToPi) {delete fJetPrToPi; fJetPrToPi = 0x0;}
	fJetPrToPi = (TH1F*)(fJetYield[2])->Clone();
	fJetPrToPi->SetDirectory(0);
	fJetPrToPi->SetName("jetPrToPi");
	fJetPrToPi->SetTitle("p/#pi in Jet");
	fJetPrToPi->GetYaxis()->SetTitle("p/#pi");
	fJetPrToPi->Divide(fJetYield[0]);
	fJetPrToPi->SetMinimum(0.);
	fJetPrToPi->SetMaximum(1.);

	if (fJetKaToPi) {delete fJetKaToPi; fJetKaToPi = 0x0;}
	fJetKaToPi = (TH1F*)(fJetYield[1])->Clone();
	fJetKaToPi->SetDirectory(0);
	fJetKaToPi->SetName("jetKaToPi");
	fJetKaToPi->SetTitle("K/#pi in Jet");
	fJetKaToPi->GetYaxis()->SetTitle("K/#pi");
	fJetKaToPi->Divide(fJetYield[0]);
	fJetKaToPi->SetMinimum(0.);
	fJetKaToPi->SetMaximum(1.);

	if (fBulkPrToPi) {delete fBulkPrToPi; fBulkPrToPi = 0x0;}
	fBulkPrToPi = (TH1F*)(fBulkYield[2])->Clone();
	fBulkPrToPi->SetDirectory(0);
	fBulkPrToPi->SetName("bulkPrToPi");
	fBulkPrToPi->SetTitle("p/#pi in Bulk");
	fBulkPrToPi->GetYaxis()->SetTitle("p/#pi");
	fBulkPrToPi->Divide(fBulkYield[0]);
	fBulkPrToPi->SetMinimum(0.);
	fBulkPrToPi->SetMaximum(1.);

	if (fBulkKaToPi) {delete fBulkKaToPi; fBulkKaToPi = 0x0;}
	fBulkKaToPi = (TH1F*)(fBulkYield[1])->Clone();
	fBulkKaToPi->SetDirectory(0);
	fBulkKaToPi->SetName("bulkKaToPi");
	fBulkKaToPi->SetTitle("K/#pi in Bulk");
	fBulkKaToPi->GetYaxis()->SetTitle("K/#pi");
	fBulkKaToPi->Divide(fBulkYield[0]);
	fBulkKaToPi->SetMinimum(0.);
	fBulkKaToPi->SetMaximum(1.);

	return kTRUE;

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::SaveSpectraAndRatios(const char* rootFileName) {

	// This method opens the file in the argument, and creates a directory
	// in that file (named after fPlotDir), where the plots are stored.
	// 
	// This method replaces the previous method with the same name, which 
	// also was concerned with plot makeup. This is now done in a separate
	// macro.

	TFile* fout = TFile::Open(rootFileName, "update");

	// Recreate the directory in the file.
	fout->rmdir(fPlotDir.Data());
	fout->mkdir(fPlotDir.Data());
	fout->cd(fPlotDir.Data());

	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
			
		if (fJetYield[iSpecies]) {fJetYield[iSpecies]->Write();}
		if (fJetWidthDEta[iSpecies]) {fJetWidthDEta[iSpecies]->Write();}
		if (fJetWidthDPhi[iSpecies]) {fJetWidthDPhi[iSpecies]->Write();}
		if (fBulkYield[iSpecies]) {fBulkYield[iSpecies]->Write();}
	
	}

	if (fJetPrToPi) {fJetPrToPi->Write();}
	if (fJetKaToPi) {fJetKaToPi->Write();}
	if (fBulkPrToPi) {fBulkPrToPi->Write();}
	if (fBulkKaToPi) {fBulkKaToPi->Write();}

	fout->Close();

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::CompareDPhiDEta(const AliRatiosDiHadronPID* other, AliSettingsDiHadronPID::Species species, Int_t pTbin) const {

	// This method presents a number of derived plots.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	TCanvas::MakeDefCanvas();
	TH2F* thisCorrelation = GetCorrelationClone(species, pTbin);
	TH2F* otherCorrelation = other->GetCorrelationClone(species, pTbin);
	thisCorrelation->Divide(otherCorrelation);
	thisCorrelation->Draw("surf1");

	TCanvas::MakeDefCanvas();
	TH2F* thisMixed = GetMixedEventClone(species, pTbin);
	TH2F* otherMixed = other->GetMixedEventClone(species, pTbin);
	thisMixed->Divide(otherMixed);
	thisMixed->Draw("surf1");

	TCanvas::MakeDefCanvas();
	TH2F* thisCorrected = GetCorrectedClone(species, pTbin);
	TH2F* otherCorrected = other->GetCorrectedClone(species, pTbin);
	thisCorrected->Divide(otherCorrected);
	thisCorrected->Draw("surf1");

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::CompareRegionYield(const AliRatiosDiHadronPID* other, AliSettingsDiHadronPID::Species species, Region region) const {

	// This method presents a number of derived plots. (TODO) comparison and ratio.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	TH1F* thisRegion = GetProjectionPt(species, region);
	TH1F* otherRegion = other->GetProjectionPt(species, region);

	TCanvas::MakeDefCanvas();
	thisRegion->DrawCopy();
	otherRegion->DrawCopy("same");

	TCanvas::MakeDefCanvas();
	thisRegion->Divide(otherRegion);
	thisRegion->DrawCopy();

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::CompareJetYield(const AliRatiosDiHadronPID* other) const {

	// This method presents a number of derived plots (TODO) comparison and ratio.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	// Compare Jet yields.
	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
			
		if (!fJetYield[iSpecies]) {continue;}
		TH1F* otherJetYield = other->GetJetYieldClone((AliSettingsDiHadronPID::Species)iSpecies);
		if (!otherJetYield) {continue;}

		TH1F* jetYieldRatio = GetJetYieldClone((AliSettingsDiHadronPID::Species)iSpecies);
		jetYieldRatio->SetDirectory(0);
		jetYieldRatio->Divide(otherJetYield);

		TCanvas::MakeDefCanvas();
		jetYieldRatio->Draw();
	
	}

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::CompareBulkYield(const AliRatiosDiHadronPID* other) const {

	// This method presents a number of derived plots (TODO) comparison and ratio.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	// Compare Bulk yields.
	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
			
		if (!fBulkYield[iSpecies]) {continue;}
		TH1F* otherBulkYield = other->GetBulkYieldClone((AliSettingsDiHadronPID::Species)iSpecies);
		if (!otherBulkYield) {continue;}

		TH1F* bulkYieldRatio = GetBulkYieldClone((AliSettingsDiHadronPID::Species)iSpecies);
		bulkYieldRatio->SetDirectory(0);
		bulkYieldRatio->Divide(otherBulkYield);

		TCanvas::MakeDefCanvas();
		bulkYieldRatio->Draw();
	
	}

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::CompareJetAndBulkRatios(const AliRatiosDiHadronPID* other) const {

	// This Method creates a comparison histogram of the jet- and bulk ratio.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());

	// Compare p/pi ratio.
	if (fJetPrToPi && fBulkPrToPi) {

		TH1F* otherJetPrToPi = other->GetJetPrToPiClone();
		TH1F* otherBulkPrToPi = other->GetBulkPrToPiClone();

		if (otherJetPrToPi && otherBulkPrToPi) {

			AliFigureDiHadronPID* figurePrToPi = new AliFigureDiHadronPID("figurePrToPi", "p/#pi");

			AliGraphDiHadronPID* grThisJetPrToPi = new AliGraphDiHadronPID(fJetPrToPi);
			grThisJetPrToPi->SetColor(kCyan);
			grThisJetPrToPi->SetSystErr(0.1);
			grThisJetPrToPi->SetMinY(0.);
			grThisJetPrToPi->SetMaxY(1.);
			
			AliGraphDiHadronPID* grOtherJetPrToPi = new AliGraphDiHadronPID(otherJetPrToPi);
			grOtherJetPrToPi->SetColor(kMagenta);
			grOtherJetPrToPi->SetSystErr(0.1);			
			grOtherJetPrToPi->SetMinY(0.);
			grOtherJetPrToPi->SetMaxY(1.);			

			AliGraphCollectionDiHadronPID* collJetPrToPi = new AliGraphCollectionDiHadronPID("collJetPrToPi","p/#pi");
			collJetPrToPi->AddGraph(grThisJetPrToPi);
			collJetPrToPi->AddGraph(grOtherJetPrToPi);

			figurePrToPi->AddGraphCollection(collJetPrToPi);

			AliGraphDiHadronPID* grThisBulkPrToPi = new AliGraphDiHadronPID(fBulkPrToPi);
			grThisBulkPrToPi->SetColor(kRed);
			grThisBulkPrToPi->SetSystErr(0.05);				
			grThisBulkPrToPi->SetMinY(0.);
			grThisBulkPrToPi->SetMaxY(1.);				

			AliGraphDiHadronPID* grOtherBulkPrToPi = new AliGraphDiHadronPID(otherBulkPrToPi);
			grOtherBulkPrToPi->SetColor(kBlue);
			grOtherBulkPrToPi->SetSystErr(0.05);				
			grOtherBulkPrToPi->SetMinY(0.);
			grOtherBulkPrToPi->SetMaxY(1.);	

			AliGraphCollectionDiHadronPID* collBulkPrToPi = new AliGraphCollectionDiHadronPID("collBulkPrToPi","p/#pi");
			collBulkPrToPi->AddGraph(grThisBulkPrToPi);
			collBulkPrToPi->AddGraph(grOtherBulkPrToPi);

			figurePrToPi->AddGraphCollection(collBulkPrToPi);

			TCanvas* cvs = figurePrToPi->Show();
			cvs->SaveAs("comparison.pdf");

		}

	}



}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::CompareJetRatios(const AliRatiosDiHadronPID* other) const {

	// This method compares ratios of the jet.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());
	
	// Compare p/pi ratio.
	if (fJetPrToPi) {
		TH1F* otherJetPrToPi = other->GetJetPrToPiClone();
		if (otherJetPrToPi) {
			TH1F* jetRatioPrToPiComp = GetJetPrToPiClone();
			jetRatioPrToPiComp->SetDirectory(0);
			jetRatioPrToPiComp->Divide(otherJetPrToPi);
			jetRatioPrToPiComp->SetMinimum(0.5);
			jetRatioPrToPiComp->SetMaximum(1.3);

			TCanvas::MakeDefCanvas();
			jetRatioPrToPiComp->Draw("HIST");

		}

	}

	// Compare K/pi ratio.
	if (fJetKaToPi) {
		TH1F* otherJetKaToPi = other->GetJetKaToPiClone();
		if (otherJetKaToPi) {
			TH1F* jetRatioKaToPiComp = GetJetKaToPiClone();
			jetRatioKaToPiComp->SetDirectory(0);
			jetRatioKaToPiComp->Divide(otherJetKaToPi);
			jetRatioKaToPiComp->SetMinimum(0.5);
			jetRatioKaToPiComp->SetMaximum(1.3);

			TCanvas::MakeDefCanvas();
			jetRatioKaToPiComp->Draw("HIST");

		}

	}

}

// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::CompareBulkRatios(const AliRatiosDiHadronPID* other) const {

	// This method compares ratios of the bulk.
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());
	
	// Compare p/pi ratio.
	if (fBulkPrToPi) {
		TH1F* otherBulkPrToPi = other->GetBulkPrToPiClone();
		if (otherBulkPrToPi) {
			TH1F* bulkRatioPrToPiComp = GetBulkPrToPiClone();
			bulkRatioPrToPiComp->SetDirectory(0);

			bulkRatioPrToPiComp->Divide(otherBulkPrToPi);
			bulkRatioPrToPiComp->SetMinimum(0.9);
			bulkRatioPrToPiComp->SetMaximum(1.1);

			TCanvas::MakeDefCanvas();
			bulkRatioPrToPiComp->Draw("HIST");

		}

	}

	// Compare K/pi ratio.
	if (fBulkKaToPi) {
		TH1F* otherBulkKaToPi = other->GetBulkKaToPiClone();
		if (otherBulkKaToPi) {
			TH1F* bulkRatioKaToPiComp = GetBulkKaToPiClone();
			bulkRatioKaToPiComp->SetDirectory(0);
			bulkRatioKaToPiComp->Divide(otherBulkKaToPi);
			bulkRatioKaToPiComp->SetMinimum(0.9);
			bulkRatioKaToPiComp->SetMaximum(1.1);			

			TCanvas::MakeDefCanvas();
			bulkRatioKaToPiComp->Draw("HIST");

		}

	}

}
// -----------------------------------------------------------------------
void AliRatiosDiHadronPID::Print(Option_t*) const {
	
	// Print info about the class.
	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	cout << "-----------------------------------------" << endl;
	cout << " AliRatiosDiHadronPID" << endl;
	cout << "-----------------------------------------" << endl;
	cout << " Name: " << GetName() << endl;
	cout << " Title: " << GetTitle() << endl;
	cout << endl;
	cout << " Pion Correlations Histogram: " << (fIdentifiedCorrelationsPion ? "OK" : "N/A") << endl;
	cout << " Kaon Correlations Histogram: " << (fIdentifiedCorrelationsKaon ? "OK" : "N/A") << endl;
	cout << " Proton Correlations Histogram: " << (fIdentifiedCorrelationsProton ? "OK" : "N/A") << endl;
	cout << endl;
	cout << " Pion M.E. Histogram: " << (fIdentifiedMixedEventsPion ? "OK" : "N/A") << endl;
	cout << " Kaon M.E. Histogram: " << (fIdentifiedMixedEventsKaon ? "OK" : "N/A") << endl;
	cout << " Proton M.E. Histogram: " << (fIdentifiedMixedEventsProton ? "OK" : "N/A") << endl;
	cout << endl;
	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		cout << " Jet Yield Calculated for: " << fSettings->GetSpeciesName((AliSettingsDiHadronPID::Species)iSpecies) << ": " << (fJetYield[iSpecies] ? "YES" : "NO") << endl;
		cout << " Bulk Yield Calculated for: " << fSettings->GetSpeciesName((AliSettingsDiHadronPID::Species)iSpecies) << ": " << (fBulkYield[iSpecies] ? "YES" : "NO") << endl;
	}
	cout << " Jet Pr/Pi calculated: " << (fJetPrToPi ? "YES" : "NO") << endl;
	cout << " Jet Ka/Pi calculated: " << (fJetKaToPi ? "YES" : "NO") << endl;
	cout << " Bulk Pr/Pi calculated: " << (fBulkPrToPi ? "YES" : "NO") << endl;
	cout << " Bulk Ka/Pi calculated: " << (fBulkKaToPi ? "YES" : "NO") << endl;
	cout << "-----------------------------------------" << endl;

}

// -----------------------------------------------------------------------
TObjArray* AliRatiosDiHadronPID::GetCorrelationArrayForMassAssumption(AliSettingsDiHadronPID::Species massAssumption) const {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	TObjArray* aout = 0x0;
	switch (massAssumption) {
		case AliSettingsDiHadronPID::kPion:
			aout = fIdentifiedCorrelationsPion;
			break;
		case AliSettingsDiHadronPID::kKaon:
			aout = fIdentifiedCorrelationsKaon;
			break;
		case AliSettingsDiHadronPID::kProton:
			aout = fIdentifiedCorrelationsProton;
			break;
		default:
			break;
	}

	return aout;

}

// -----------------------------------------------------------------------
TObjArray* AliRatiosDiHadronPID::GetMixedEventsArrayForMassAssumption(AliSettingsDiHadronPID::Species massAssumption) const {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	TObjArray* aout = 0x0;
	switch (massAssumption) {
		case AliSettingsDiHadronPID::kPion:
			aout = fIdentifiedMixedEventsPion;
			break;
		case AliSettingsDiHadronPID::kKaon:
			aout = fIdentifiedMixedEventsKaon;
			break;
		case AliSettingsDiHadronPID::kProton:
			aout = fIdentifiedMixedEventsProton;
			break;
		default:
			break;
	}

	return aout;

}

// -----------------------------------------------------------------------
TH2F* AliRatiosDiHadronPID::GetCorrelation(AliSettingsDiHadronPID::Species massAssumption, Int_t ptBin) const {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	// Get a ptr to the array corresponding to the indicated mass assumption.
	TObjArray* arrayTmp = GetCorrelationArrayForMassAssumption(massAssumption);
	if (!arrayTmp) {return 0x0;}

	//
	return (TH2F*)(arrayTmp->At(ptBin - 1));

}

// -----------------------------------------------------------------------
TH2F* AliRatiosDiHadronPID::GetMixedEvents(AliSettingsDiHadronPID::Species massAssumption, Int_t ptBin) const {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	// Get a ptr to the array corresponding to the indicated mass assumption.
	TObjArray* arrayTmp = GetMixedEventsArrayForMassAssumption(massAssumption);
	if (!arrayTmp) {return 0x0;}

	//
	return (TH2F*)(arrayTmp->At(ptBin - 1));


}

// -----------------------------------------------------------------------
Bool_t AliRatiosDiHadronPID::GetIntegralAndError(AliSettingsDiHadronPID::Species species, Region region, Int_t ptBin, Double_t& integral, Double_t& error) const {

	// This method calculates the integral in a certain region in one pT bin.
	// Note: at this moment, the yield per bin is returned, this should become a number
	// that's bin independent, to make a better comparison.
	TH2F* correctedTmp = GetCorrectedClone(species, ptBin);

	// Do the integral.
	integral = 0.;
	error = 0.;
	Double_t errorSq = 0.;

	// Get ptrs to the DPhi and DEta axes.
	TAxis* aDPhi = correctedTmp->GetXaxis();
	TAxis* aDEta = correctedTmp->GetYaxis();

	// Number of bins counter.
	Double_t nBins = 0.;

	// Shapes are defined here.
	switch (region) {

		case kAll:
			for (Int_t iDEtaBin = 1; iDEtaBin <=aDEta->GetNbins(); iDEtaBin++) {
				for (Int_t iDPhiBin = 1; iDPhiBin <= aDPhi->GetNbins(); iDPhiBin++) {
					integral += correctedTmp->GetBinContent(iDPhiBin, iDEtaBin);
					errorSq += (correctedTmp->GetBinError(iDPhiBin, iDEtaBin) * correctedTmp->GetBinError(iDPhiBin, iDEtaBin));				
					nBins += 1.;
				}
			}
			break;

		case kPeak1a:
			for (Int_t iDEtaBin =aDEta->FindBin(-0.5); iDEtaBin <=aDEta->FindBin(0.5); iDEtaBin++) {
				for (Int_t iDPhiBin = aDPhi->FindBin(-1.); iDPhiBin <= aDPhi->FindBin(1.); iDPhiBin++) {
					integral += correctedTmp->GetBinContent(iDPhiBin, iDEtaBin);
					errorSq += (correctedTmp->GetBinError(iDPhiBin, iDEtaBin) * correctedTmp->GetBinError(iDPhiBin, iDEtaBin));				
					nBins += 1.;
				}
			}
			break;

		case kPeak1b:
			for (Int_t iDEtaBin =aDEta->FindBin(-0.6); iDEtaBin <=aDEta->FindBin(0.6); iDEtaBin++) {
				for (Int_t iDPhiBin = aDPhi->FindBin(-1.); iDPhiBin <= aDPhi->FindBin(1.); iDPhiBin++) {
					integral += correctedTmp->GetBinContent(iDPhiBin, iDEtaBin);
					errorSq += (correctedTmp->GetBinError(iDPhiBin, iDEtaBin) * correctedTmp->GetBinError(iDPhiBin, iDEtaBin));				
					nBins += 1.;
				}
			}
			break;

		case kPeakOLD:
			for (Int_t iDEtaBin =aDEta->FindBin(-0.4); iDEtaBin <=aDEta->FindBin(0.4); iDEtaBin++) {
				for (Int_t iDPhiBin = aDPhi->FindBin(-0.52); iDPhiBin <= aDPhi->FindBin(0.52); iDPhiBin++) {
					integral += correctedTmp->GetBinContent(iDPhiBin, iDEtaBin);
					errorSq += (correctedTmp->GetBinError(iDPhiBin, iDEtaBin) * correctedTmp->GetBinError(iDPhiBin, iDEtaBin));				
					nBins += 1.;
				}
			}
			break;

		case kRidge1:
			for (Int_t iDEtaBin =aDEta->FindBin(0.8); iDEtaBin <=aDEta->FindBin(1.4); iDEtaBin++) {
				for (Int_t iDPhiBin = aDPhi->FindBin(-1.); iDPhiBin <= aDPhi->FindBin(1.); iDPhiBin++) {
					integral += correctedTmp->GetBinContent(iDPhiBin, iDEtaBin);
					errorSq += (correctedTmp->GetBinError(iDPhiBin, iDEtaBin) * correctedTmp->GetBinError(iDPhiBin, iDEtaBin));				
					nBins += 1.;
				}
			}

			for (Int_t iDEtaBin =aDEta->FindBin(-1.4); iDEtaBin <=aDEta->FindBin(-0.8); iDEtaBin++) {
				for (Int_t iDPhiBin = aDPhi->FindBin(-1.); iDPhiBin <= aDPhi->FindBin(1.); iDPhiBin++) {
					integral += correctedTmp->GetBinContent(iDPhiBin, iDEtaBin);
					errorSq += (correctedTmp->GetBinError(iDPhiBin, iDEtaBin) * correctedTmp->GetBinError(iDPhiBin, iDEtaBin));				
					nBins += 1.;
				}
			}
			break;

		case kRidgeOLD:
			for (Int_t iDEtaBin =aDEta->FindBin(0.6); iDEtaBin <=aDEta->FindBin(1.5); iDEtaBin++) {
				for (Int_t iDPhiBin = aDPhi->FindBin(-1.); iDPhiBin <= aDPhi->FindBin(1.); iDPhiBin++) {
					integral += correctedTmp->GetBinContent(iDPhiBin, iDEtaBin);
					errorSq += (correctedTmp->GetBinError(iDPhiBin, iDEtaBin) * correctedTmp->GetBinError(iDPhiBin, iDEtaBin));				
					nBins += 1.;
				}
			}

			for (Int_t iDEtaBin =aDEta->FindBin(-1.5); iDEtaBin <=aDEta->FindBin(-0.6); iDEtaBin++) {
				for (Int_t iDPhiBin = aDPhi->FindBin(-0.52); iDPhiBin <= aDPhi->FindBin(0.52); iDPhiBin++) {
					integral += correctedTmp->GetBinContent(iDPhiBin, iDEtaBin);
					errorSq += (correctedTmp->GetBinError(iDPhiBin, iDEtaBin) * correctedTmp->GetBinError(iDPhiBin, iDEtaBin));				
					nBins += 1.;
				}
			}
			break;

		case kUnknownRegion:
		default:
			AliLogDiHadronPID::Warning(Form("That region is not known."), __func__, ClassName());
			break;
	}

	// Take the square root of the error.
	error = TMath::Sqrt(errorSq);

	integral /= nBins;
	error /= nBins;

	if (correctedTmp) {delete correctedTmp; correctedTmp = 0x0;}

	return kTRUE;

}

// -----------------------------------------------------------------------
TH1F* AliRatiosDiHadronPID::CutOutJetAndProject(const TH2F* hist, Double_t maxDEta, Double_t DPhiJet, Double_t DEtaJet, Bool_t useConstFit) const {

	// This method creates a projection on the DPhi axis, but removes all bins
	// which have a bin center within an ellipse (DPhiJet, DEtaJet) from the origin.
	//
	// Note that we assume that all bins in DEta direction which are included are fully correlated,
	// that is, we do a constant fit.
	//
	// Note that this procedure may not be very good if we choose the jet size too small.

	if (maxDEta < DEtaJet) {
		AliLogDiHadronPID::Error(Form("maxDEta needs to be larger than DEtaJet."), __func__, ClassName());
		return 0x0;
	}

/* #ifdef __CLING__
	const TAxis* aDPhi = hist->GetXaxis();
	const TAxis* aDEta = hist->GetYaxis();
#else
	TAxis* aDPhi = hist->GetXaxis();
	TAxis* aDEta = hist->GetYaxis();
#endif */
	Int_t minBinDEta =hist->GetYaxis()->FindBin(-maxDEta + 0.001);
	Int_t maxBinDEta =hist->GetYaxis()->FindBin(maxDEta - 0.001);

	TH1F* hout = new TH1F(Form("%s_proj_minusJet", hist->GetName()), hist->GetTitle(), hist->GetXaxis()->GetNbins(), hist->GetXaxis()->GetXmin(), hist->GetXaxis()->GetXmax());
	hout->GetXaxis()->SetTitle(hist->GetXaxis()->GetTitle());
	hout->SetDirectory(0);

	// Int_t maxNPointsIncluded = 0;
	if (useConstFit) {

		// Use a constant fit to the points instead of a sum.
		for (Int_t iDPhiBin = 1; iDPhiBin <= hist->GetXaxis()->GetNbins(); iDPhiBin++) {

			Int_t nPointsIncluded = 0;

			// Create arrays that need to be fitted.
			Double_t points[hist->GetYaxis()->GetNbins()];
			Double_t errors[hist->GetYaxis()->GetNbins()];
			for (Int_t iDEtaBin = 1; iDEtaBin <=hist->GetYaxis()->GetNbins(); iDEtaBin++) {
				
				points[iDEtaBin] = 0.;
				errors[iDEtaBin] = 0.;

			}

			Double_t dPhiScaled2 = TMath::Power(hist->GetXaxis()->GetBinCenter(iDPhiBin) / DPhiJet, 2);

			// Fill the arrays that need to be fitted.
			for (Int_t iDEtaBin = minBinDEta; iDEtaBin <= maxBinDEta; iDEtaBin++) {

				Double_t dEtaScaled2 = TMath::Power(hist->GetYaxis()->GetBinCenter(iDEtaBin) / DEtaJet, 2);

				// Continue if the bin center is within the jet region (note that we don't need to put a square root here
				// because of the normalisation).
				if ((dPhiScaled2 + dEtaScaled2) < 1.) {continue;}

				points[nPointsIncluded] = hist->GetBinContent(iDPhiBin, iDEtaBin);
				errors[nPointsIncluded] = hist->GetBinError(iDPhiBin, iDEtaBin);
				nPointsIncluded++;

			}		

			Double_t fitval = 0.;
			Double_t fiterr = 0.;

			AliFunctionsDiHadronPID::ConstFit(points, errors, nPointsIncluded, fitval, fiterr);

			hout->SetBinContent(iDPhiBin, fitval);
			hout->SetBinError(iDPhiBin, fiterr);

			// maxNPointsIncluded = TMath::Max(maxNPointsIncluded, nPointsIncluded);

		}

	} else {

		// Use ordinary sum.
		for (Int_t iDPhiBin = 1; iDPhiBin <= hist->GetXaxis()->GetNbins(); iDPhiBin++) {

			Double_t dPhiScaled2 = TMath::Power(hist->GetXaxis()->GetBinCenter(iDPhiBin) / DPhiJet, 2);

			Double_t sum = 0.;
			Double_t errorSquared = 0.;
			Double_t nPointsIncluded = 0.;

			for (Int_t iDEtaBin = minBinDEta; iDEtaBin <= maxBinDEta; iDEtaBin++) {

				Double_t dEtaScaled2 = TMath::Power(hist->GetYaxis()->GetBinCenter(iDEtaBin) / DEtaJet, 2);

				if ((dPhiScaled2 + dEtaScaled2) < 1.) {continue;}		

				sum += hist->GetBinContent(iDPhiBin, iDEtaBin);
				errorSquared += (hist->GetBinError(iDPhiBin, iDEtaBin) * hist->GetBinError(iDPhiBin, iDEtaBin));
				nPointsIncluded++;

			}

			hout->SetBinContent(iDPhiBin, sum / nPointsIncluded);
			hout->SetBinError(iDPhiBin, TMath::Sqrt(errorSquared) / nPointsIncluded);
			// cout << "Npoints included: " << nPointsIncluded << endl;
		
		}

	}

	// hout->Scale((Double_t)maxNPointsIncluded);

	return hout;

}

