#ifndef ALISYSTUNCERTAINTYGENDIHADRONPID_H
#define ALISYSTUNCERTAINTYGENDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TObject.h"
#include "TString.h"
#include "TGraphAsymmErrors.h"
#include "TF1.h"
#include "TH1.h"

class AliSystUncertaintyGenDiHadronPID : public TObject {
	
public:
	AliSystUncertaintyGenDiHadronPID(Double_t xMin, Double_t xMax);
	~AliSystUncertaintyGenDiHadronPID();

	// Adding systematic uncertainty terms.
	void						AddLinearTerm(Double_t yMin, Double_t yMax, Bool_t up = kTRUE, Bool_t down = kTRUE);
	void						AddKinkTerm(Double_t yMin, Double_t yMax, Double_t xKink, Bool_t up = kTRUE, Bool_t down = kTRUE);
	void						AddQuadraticTerm(Double_t yMin, Double_t yMax, Bool_t up = kTRUE, Bool_t down = kTRUE);

	// Creating visual representations of the systematic uncertainty.
	TF1*						GetTF1(Bool_t up) const;
	TGraphAsymmErrors*			GetGraph(const TH1* hist) const;

	// Debug Info.
	void 						Print(Option_t*) const;

private:
	void						Add(TString &addition, Bool_t up, Bool_t down);

private:
	Double_t					fXmin;
	Double_t					fXmax;
	TString						fFormulaUp;
	TString						fFormulaDown;

	ClassDef(AliSystUncertaintyGenDiHadronPID, 1);

};

#endif
