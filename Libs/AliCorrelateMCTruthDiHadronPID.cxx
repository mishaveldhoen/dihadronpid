/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Class description
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "TH1D.h"

#include "AliPtEtaSpectrumDiHadronPID.h"
#include "AliCorrelateMCTruthDiHadronPID.h"
#include "AliHistToolsDiHadronPID.h"
#include "AliLogDiHadronPID.h"

#include "AliMiniEventDiHadronPID.h"
#include "AliMiniTrackDiHadronPID.h"

ClassImp(AliCorrelateMCTruthDiHadronPID);

// -----------------------------------------------------------------------
AliCorrelateMCTruthDiHadronPID::AliCorrelateMCTruthDiHadronPID(const char* name, const char* title):
	AliAnalyzeDataDiHadronPID(name, title),
	fPool(new AliMiniEventPoolDiHadronPID()),
	fEventsWithTriggers(0x0),
    fMassAssumption(0),
	fPtAxis(0x0),
	fEtaAxis(0x0),
    fDPhiAxis(0x0),
    fDEtaAxis(0x0),
	fCentralityAxis(0x0),
	fVtxZAxis(0x0),
	fNTotTriggersTruth(0x0),
	fPtHistogramTruth(0x0),
	fNTotTriggersRec(0x0),
	fPtHistogramRec(0x0),
    fPtHistogramPrimTruth(0x0),
    fPtHistogramPrimRec(0x0),
    fPtHistogramPrimRecSelForPID(0x0),
    fPtHistogramPrimRecMatched(0x0),
    fMCPIDCorrelationsPrimTruth(0x0),
    fMCPIDCorrelationsWeakTruth(0x0),
    fMCPIDCorrelationsMatTruth(0x0), 
    fMCPIDMixedPrimTruth(0x0), 
    fMCPIDMixedWeakTruth(0x0),       
    fMCPIDMixedMatTruth(0x0),
    fMCPIDCorrelationsPrimRec(0x0),
    fMCPIDCorrelationsWeakRec(0x0),
    fMCPIDCorrelationsMatRec(0x0), 
    fMCPIDMixedPrimRec(0x0), 
    fMCPIDMixedWeakRec(0x0),       
    fMCPIDMixedMatRec(0x0),
    fMCPIDCorrelationsPrimRecSelForPID(0x0),
    fMCPIDCorrelationsPrimRecMatched(0x0),
    fMCPIDMixedPrimRecSelForPID(0x0),
    fMCPIDMixedPrimRecMatched(0x0),
    fSPEffTracking(0x0),
    fSPEffSelForPID(0x0),
    fSPEffMatched(0x0),
    fCorrEffTracking(0x0),
    fCorrEffSelForPID(0x0),
    fCorrEffMatched(0x0),
    fCorrEffTrackingDPhiStrip(0x0),
    fCorrEffSelForPIDDPhiStrip(0x0),
    fCorrEffMatchedDPhiStrip(0x0),
    fCorrEffTrackingFullDPhiDEta(0x0),
    fCorrEffSelForPIDFullDPhiDEta(0x0),
    fCorrEffMatchedFullDPhiDEta(0x0)

{
	
	// Default Constructor
	AliLogDiHadronPID::Debug2("Default constructor called.", __func__, ClassName());

}

// -----------------------------------------------------------------------
AliCorrelateMCTruthDiHadronPID::~AliCorrelateMCTruthDiHadronPID() {
	
	// Destructor
	AliLogDiHadronPID::Debug2("Destructor called.", __func__, ClassName());

	// Delete ME pool.
	if (fPool) {delete fPool; fPool = 0x0;}

	if (fEventsWithTriggers) {delete fEventsWithTriggers; fEventsWithTriggers = 0x0;}

	// Delete Axes.
	if (fPtAxis) {delete fPtAxis; fPtAxis = 0x0;}
	if (fEtaAxis) {delete fEtaAxis; fEtaAxis = 0x0;}
	if (fDPhiAxis) {delete fDPhiAxis; fDPhiAxis = 0x0;}
	if (fDEtaAxis) {delete fDEtaAxis; fDEtaAxis = 0x0;}
	if (fCentralityAxis) {delete fCentralityAxis; fCentralityAxis = 0x0;}
	if (fVtxZAxis) {delete fVtxZAxis; fVtxZAxis = 0x0;}

    // Delete 1D histograms.
	if (fNTotTriggersTruth) {delete fNTotTriggersTruth; fNTotTriggersTruth = 0x0;}
	if (fPtHistogramTruth) {delete fPtHistogramTruth; fPtHistogramTruth = 0x0;}

	if (fNTotTriggersRec) {delete fNTotTriggersRec; fNTotTriggersRec = 0x0;}
    if (fPtHistogramRec) {delete fPtHistogramRec; fPtHistogramRec = 0x0;}
   
    // These histograms are used for calculating PID and matching efficiency.
    if (fPtHistogramPrimTruth) {delete fPtHistogramPrimTruth; fPtHistogramPrimTruth = 0x0;}
    if (fPtHistogramPrimRec) {delete fPtHistogramPrimRec; fPtHistogramPrimRec = 0x0;}
    if (fPtHistogramPrimRecSelForPID) {delete fPtHistogramPrimRecSelForPID; fPtHistogramPrimRecSelForPID = 0x0;}
    if (fPtHistogramPrimRecMatched) {delete fPtHistogramPrimRecMatched; fPtHistogramPrimRecMatched = 0x0;}

	for (Int_t iPtBin = 1; iPtBin <= fSettings->GetPtAxis()->GetNbins(); iPtBin++) {

        if (fMCPIDCorrelationsPrimTruth[iPtBin-1]) {delete fMCPIDCorrelationsPrimTruth[iPtBin-1]; fMCPIDCorrelationsPrimTruth[iPtBin-1] = 0x0;}
        if (fMCPIDCorrelationsWeakTruth[iPtBin-1]) {delete fMCPIDCorrelationsWeakTruth[iPtBin-1]; fMCPIDCorrelationsWeakTruth[iPtBin-1] = 0x0;}
        if (fMCPIDCorrelationsMatTruth[iPtBin-1]) {delete fMCPIDCorrelationsMatTruth[iPtBin-1]; fMCPIDCorrelationsMatTruth[iPtBin-1] = 0x0;}

        if (fMCPIDMixedPrimTruth[iPtBin-1]) {delete fMCPIDMixedPrimTruth[iPtBin-1]; fMCPIDMixedPrimTruth[iPtBin-1] = 0x0;}
        if (fMCPIDMixedWeakTruth[iPtBin-1]) {delete fMCPIDMixedWeakTruth[iPtBin-1]; fMCPIDMixedWeakTruth[iPtBin-1] = 0x0;}
        if (fMCPIDMixedMatTruth[iPtBin-1]) {delete fMCPIDMixedMatTruth[iPtBin-1]; fMCPIDMixedMatTruth[iPtBin-1] = 0x0;}

        if (fMCPIDCorrelationsPrimRec[iPtBin-1]) {delete fMCPIDCorrelationsPrimRec[iPtBin-1]; fMCPIDCorrelationsPrimRec[iPtBin-1] = 0x0;}
        if (fMCPIDCorrelationsWeakRec[iPtBin-1]) {delete fMCPIDCorrelationsWeakRec[iPtBin-1]; fMCPIDCorrelationsWeakRec[iPtBin-1] = 0x0;}
        if (fMCPIDCorrelationsMatRec[iPtBin-1]) {delete fMCPIDCorrelationsMatRec[iPtBin-1]; fMCPIDCorrelationsMatRec[iPtBin-1] = 0x0;}

        if (fMCPIDMixedPrimRec[iPtBin-1]) {delete fMCPIDMixedPrimRec[iPtBin-1]; fMCPIDMixedPrimRec[iPtBin-1] = 0x0;}
        if (fMCPIDMixedWeakRec[iPtBin-1]) {delete fMCPIDMixedWeakRec[iPtBin-1]; fMCPIDMixedWeakRec[iPtBin-1] = 0x0;}
        if (fMCPIDMixedMatRec[iPtBin-1]) {delete fMCPIDMixedMatRec[iPtBin-1]; fMCPIDMixedMatRec[iPtBin-1] = 0x0;}

        
        if (fMCPIDCorrelationsPrimRecSelForPID[iPtBin-1]) {delete fMCPIDCorrelationsPrimRecSelForPID[iPtBin-1]; fMCPIDCorrelationsPrimRecSelForPID[iPtBin-1] = 0x0;}
        if (fMCPIDCorrelationsPrimRecMatched[iPtBin-1]) {delete fMCPIDCorrelationsPrimRecMatched[iPtBin-1]; fMCPIDCorrelationsPrimRecMatched[iPtBin-1] = 0x0;}

        if (fMCPIDMixedPrimRecSelForPID[iPtBin-1]) {delete fMCPIDMixedPrimRecSelForPID[iPtBin-1]; fMCPIDMixedPrimRecSelForPID[iPtBin-1] = 0x0;}
        if (fMCPIDMixedPrimRecMatched[iPtBin-1]) {delete fMCPIDMixedPrimRecMatched[iPtBin-1]; fMCPIDMixedPrimRecMatched[iPtBin-1] = 0x0;}

	}

    // Delete the histogram arrays.
    if (fMCPIDCorrelationsPrimTruth) {delete fMCPIDCorrelationsPrimTruth; fMCPIDCorrelationsPrimTruth = 0x0;}
    if (fMCPIDCorrelationsWeakTruth) {delete fMCPIDCorrelationsWeakTruth; fMCPIDCorrelationsWeakTruth = 0x0;}
    if (fMCPIDCorrelationsMatTruth) {delete fMCPIDCorrelationsMatTruth; fMCPIDCorrelationsMatTruth = 0x0;}

    if (fMCPIDMixedPrimTruth) {delete fMCPIDMixedPrimTruth; fMCPIDMixedPrimTruth = 0x0;}
    if (fMCPIDMixedWeakTruth) {delete fMCPIDMixedWeakTruth; fMCPIDMixedWeakTruth = 0x0;}
    if (fMCPIDMixedMatTruth) {delete fMCPIDMixedMatTruth; fMCPIDMixedMatTruth = 0x0;}

    if (fMCPIDCorrelationsPrimRec) {delete fMCPIDCorrelationsPrimRec; fMCPIDCorrelationsPrimRec = 0x0;}
    if (fMCPIDCorrelationsWeakRec) {delete fMCPIDCorrelationsWeakRec; fMCPIDCorrelationsWeakRec = 0x0;}
    if (fMCPIDCorrelationsMatRec) {delete fMCPIDCorrelationsMatRec; fMCPIDCorrelationsMatRec = 0x0;}

    if (fMCPIDMixedPrimRec) {delete fMCPIDMixedPrimRec; fMCPIDMixedPrimRec = 0x0;}
    if (fMCPIDMixedWeakRec) {delete fMCPIDMixedWeakRec; fMCPIDMixedWeakRec = 0x0;}
    if (fMCPIDMixedMatRec) {delete fMCPIDMixedMatRec; fMCPIDMixedMatRec = 0x0;}
        
    if (fMCPIDCorrelationsPrimRecSelForPID) {delete fMCPIDCorrelationsPrimRecSelForPID; fMCPIDCorrelationsPrimRecSelForPID = 0x0;}
    if (fMCPIDCorrelationsPrimRecMatched) {delete fMCPIDCorrelationsPrimRecMatched; fMCPIDCorrelationsPrimRecMatched = 0x0;}

    if (fMCPIDMixedPrimRecSelForPID) {delete fMCPIDMixedPrimRecSelForPID; fMCPIDMixedPrimRecSelForPID = 0x0;}
    if (fMCPIDMixedPrimRecMatched) {delete fMCPIDMixedPrimRecMatched; fMCPIDMixedPrimRecMatched = 0x0;}

    if (fSPEffTracking) {delete fSPEffTracking; fSPEffTracking = 0x0;}
    if (fSPEffSelForPID) {delete fSPEffSelForPID; fSPEffSelForPID = 0x0;}
    if (fSPEffMatched) {delete fSPEffMatched; fSPEffMatched = 0x0;}

    if (fCorrEffTracking) {delete fCorrEffTracking; fCorrEffTracking = 0x0;}
    if (fCorrEffSelForPID) {delete fCorrEffSelForPID; fCorrEffSelForPID = 0x0;}
    if (fCorrEffMatched) {delete fCorrEffMatched; fCorrEffMatched = 0x0;}

    if (fCorrEffTrackingDPhiStrip) {delete fCorrEffTrackingDPhiStrip; fCorrEffTrackingDPhiStrip = 0x0;}
    if (fCorrEffSelForPIDDPhiStrip) {delete fCorrEffSelForPIDDPhiStrip; fCorrEffSelForPIDDPhiStrip = 0x0;}
    if (fCorrEffMatchedDPhiStrip) {delete fCorrEffMatchedDPhiStrip; fCorrEffMatchedDPhiStrip = 0x0;}
    
    if (fCorrEffTrackingFullDPhiDEta) {delete fCorrEffTrackingFullDPhiDEta; fCorrEffTrackingFullDPhiDEta = 0x0;}
    if (fCorrEffSelForPIDFullDPhiDEta) {delete fCorrEffSelForPIDFullDPhiDEta; fCorrEffSelForPIDFullDPhiDEta = 0x0;}
    if (fCorrEffMatchedFullDPhiDEta) {delete fCorrEffMatchedFullDPhiDEta; fCorrEffMatchedFullDPhiDEta = 0x0;}

}

// -----------------------------------------------------------------------
void AliCorrelateMCTruthDiHadronPID::UserCreateOutput() {

	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	// Change output file name.
	fOutputFileName = Form("PIDCorrelations_%i_MCTruth.root", fMassAssumption);

	// Obtain deep copies of the axes (we want to be able to use the non-const method TAxis::FindBin())
	fPtAxis = new TAxis(*fSettings->GetPtAxis());
	fEtaAxis = new TAxis(*fSettings->GetEtaAxis());
	fDPhiAxis = new TAxis(*fSettings->GetDPhiAxis());
	fDEtaAxis = new TAxis(*fSettings->GetDEtaAxis());
	fCentralityAxis = new TAxis(*fSettings->GetCentralityAxis());
	fVtxZAxis = new TAxis(*fSettings->GetVtxZAxis());

	// Create QA histograms.
	fNTotTriggersTruth = new TH1D("triggerHistTruth", "triggerHistTruth", 1, fSettings->GetMinTriggerPt(), fSettings->GetMaxTriggerPt());
	fPtHistogramTruth = (TH1D*)AliHistToolsDiHadronPID::MakeHist("hPtDistTruth", "p_{T} distribution (MC truth)", "D", fSettings->GetPtAxis());
    
	fNTotTriggersRec = new TH1D("triggerHistRec", "triggerHistRec", 1, fSettings->GetMinTriggerPt(), fSettings->GetMaxTriggerPt());
	fPtHistogramRec = (TH1D*)AliHistToolsDiHadronPID::MakeHist("hPtDistRec", "p_{T} distribution (reconstructed)", "D", fSettings->GetPtAxis());
   
    // Create histograms for efficiency calculation.
	fPtHistogramPrimTruth = (TH1D*)AliHistToolsDiHadronPID::MakeHist("hPtDistPrimTruth", "p_{T} distribution (primary MC truth)", "D", fSettings->GetPtAxis());
	fPtHistogramPrimRec = (TH1D*)AliHistToolsDiHadronPID::MakeHist("hPtDistPrimRec", "p_{T} distribution (primary reconstructed)", "D", fSettings->GetPtAxis());
	fPtHistogramPrimRecSelForPID = (TH1D*)AliHistToolsDiHadronPID::MakeHist("hPtPrimRecSelForPID", "p_{T} distribution (primary reconstructed, sel. for PID)", "D", fSettings->GetPtAxis());
	fPtHistogramPrimRecMatched = (TH1D*)AliHistToolsDiHadronPID::MakeHist("hPtDistPrimRecMatched", "p_{T} distribution (primary reconstructed, TOF matched)", "D", fSettings->GetPtAxis());
    
    // Create MC histograms if we run over a MC set.
    fMCPIDCorrelationsPrimTruth = new TH2D*[fSettings->GetPtAxis()->GetNbins()];
    fMCPIDCorrelationsWeakTruth = new TH2D*[fSettings->GetPtAxis()->GetNbins()];
    fMCPIDCorrelationsMatTruth = new TH2D*[fSettings->GetPtAxis()->GetNbins()];

    fMCPIDMixedPrimTruth = new TH2D*[fSettings->GetPtAxis()->GetNbins()];
    fMCPIDMixedWeakTruth = new TH2D*[fSettings->GetPtAxis()->GetNbins()];
    fMCPIDMixedMatTruth = new TH2D*[fSettings->GetPtAxis()->GetNbins()];

    fMCPIDCorrelationsPrimRec = new TH2D*[fSettings->GetPtAxis()->GetNbins()];
    fMCPIDCorrelationsWeakRec = new TH2D*[fSettings->GetPtAxis()->GetNbins()];
    fMCPIDCorrelationsMatRec = new TH2D*[fSettings->GetPtAxis()->GetNbins()];

    fMCPIDMixedPrimRec = new TH2D*[fSettings->GetPtAxis()->GetNbins()];
    fMCPIDMixedWeakRec = new TH2D*[fSettings->GetPtAxis()->GetNbins()];
    fMCPIDMixedMatRec = new TH2D*[fSettings->GetPtAxis()->GetNbins()];

    fMCPIDCorrelationsPrimRecSelForPID = new TH2D*[fSettings->GetPtAxis()->GetNbins()];
    fMCPIDCorrelationsPrimRecMatched = new TH2D*[fSettings->GetPtAxis()->GetNbins()];

    fMCPIDMixedPrimRecSelForPID = new TH2D*[fSettings->GetPtAxis()->GetNbins()];
    fMCPIDMixedPrimRecMatched = new TH2D*[fSettings->GetPtAxis()->GetNbins()];

    // Create the histograms for every pT bin.
    for (Int_t iPtBin = 1; iPtBin <= fSettings->GetPtAxis()->GetNbins(); iPtBin++) {      
        
        fMCPIDCorrelationsPrimTruth[iPtBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fMCPIDCorrelationsPrimTruth_PtBin%i", iPtBin), Form("fMCPIDCorrelationsPrimTruth_PtBin%i", iPtBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());
        fMCPIDCorrelationsWeakTruth[iPtBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fMCPIDCorrelationsWeakTruth_PtBin%i", iPtBin), Form("fMCPIDCorrelationsWeakTruth_PtBin%i", iPtBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());
        fMCPIDCorrelationsMatTruth[iPtBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fMCPIDCorrelationsMatTruth_PtBin%i", iPtBin), Form("fMCPIDCorrelationsMatTruth_PtBin%i", iPtBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());

        fMCPIDMixedPrimTruth[iPtBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fMCPIDMixedPrimTruth_PtBin%i", iPtBin), Form("fMCPIDMixedPrimTruth_PtBin%i", iPtBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());
        fMCPIDMixedWeakTruth[iPtBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fMCPIDMixedWeakTruth_PtBin%i", iPtBin), Form("fMCPIDMixedWeakTruth_PtBin%i", iPtBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());
        fMCPIDMixedMatTruth[iPtBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fMCPIDMixedMatTruth_PtBin%i", iPtBin), Form("fMCPIDMixedMatTruth_PtBin%i", iPtBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());

        fMCPIDCorrelationsPrimRec[iPtBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fMCPIDCorrelationsPrimRec_PtBin%i", iPtBin), Form("fMCPIDCorrelationsPrimRec_PtBin%i", iPtBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());
        fMCPIDCorrelationsWeakRec[iPtBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fMCPIDCorrelationsWeakRec_PtBin%i", iPtBin), Form("fMCPIDCorrelationsWeakRec_PtBin%i", iPtBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());
        fMCPIDCorrelationsMatRec[iPtBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fMCPIDCorrelationsMatRec_PtBin%i", iPtBin), Form("fMCPIDCorrelationsMatRec_PtBin%i", iPtBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());

        fMCPIDMixedPrimRec[iPtBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fMCPIDMixedPrimRec_PtBin%i", iPtBin), Form("fMCPIDMixedPrimRec_PtBin%i", iPtBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());
        fMCPIDMixedWeakRec[iPtBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fMCPIDMixedWeakRec_PtBin%i", iPtBin), Form("fMCPIDMixedWeakRec_PtBin%i", iPtBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());
        fMCPIDMixedMatRec[iPtBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fMCPIDMixedMatRec_PtBin%i", iPtBin), Form("fMCPIDMixedMatRec_PtBin%i", iPtBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());

        fMCPIDCorrelationsPrimRecSelForPID[iPtBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fMCPIDCorrelationsPrimSelForPID_PtBin%i", iPtBin), Form("fMCPIDCorrelationsPrimSelForPID_PtBin%i", iPtBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());
        fMCPIDCorrelationsPrimRecMatched[iPtBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fMCPIDCorrelationsPrimRecMatched_PtBin%i", iPtBin), Form("fMCPIDCorrelationsPrimRecMatched_PtBin%i", iPtBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());

        fMCPIDMixedPrimRecSelForPID[iPtBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fMCPIDMixedPrimSelForPID_PtBin%i", iPtBin), Form("fMCPIDMixedPrimSelForPID_PtBin%i", iPtBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());
        fMCPIDMixedPrimRecMatched[iPtBin-1] = (TH2D*)AliHistToolsDiHadronPID::MakeHist(Form("fMCPIDMixedPrimRecMatched_PtBin%i", iPtBin), Form("fMCPIDMixedPrimRecMatched_PtBin%i", iPtBin), "D", fSettings->GetDPhiAxis(), fSettings->GetDEtaAxis());

    }

}

// -----------------------------------------------------------------------
void AliCorrelateMCTruthDiHadronPID::UserExec() {

	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	// Get a ptr to the current event.
	AliMiniEventDiHadronPID* currentEvent = GetCurrentEvent();
    if (!currentEvent) {
        AliLogDiHadronPID::Error("Could not get event.", __func__, ClassName());
        return;
    }

    // Check if the event falls in the right centrality class.
    Int_t centBin = fCentralityAxis->FindBin(currentEvent->GetCentrality());
	if (centBin < 1 || centBin > fCentralityAxis->GetNbins()) {return;}

	Int_t vtxzBin = fVtxZAxis->FindBin(currentEvent->GetVtxZ());
	if (vtxzBin < 1 || vtxzBin > fVtxZAxis->GetNbins()) {return;}	

	// Create an array for the triggers we will add to the pool, this array owns the triggers,
	// and should therefore only contain copies.
	TObjArray* donatedTriggers = new TObjArray(10);
	donatedTriggers->SetOwner(kTRUE);

	// Identify pairs in the same event.
	for (Int_t iTriggerTrack = 0; iTriggerTrack < currentEvent->GetNTracks(); ++iTriggerTrack) { 
		
		AliMiniTrackDiHadronPID* currentTriggerTrack = currentEvent->GetTrack(iTriggerTrack);
	
        // Only accept pions, kaons and protons as trigger tracks (this should be very close to the "truth").
        if ((!currentTriggerTrack->IsMCPion()) && (!currentTriggerTrack->IsMCKaon()) && 
            (!currentTriggerTrack->IsMCProton())) {continue;}

        // Fill histogram with all truth and reconstructed particles.
		fPtHistogramTruth->Fill(currentTriggerTrack->MCPt());
        if (currentTriggerTrack->GetFilterMap() & fSettings->GetFilterMask()) {
            fPtHistogramRec->Fill(currentTriggerTrack->MCPt());
        }

        // Fill histograms for calculating the single-particle primary efficiency.
        if ((fMassAssumption == 0 && currentTriggerTrack->IsMCPion()) ||
            (fMassAssumption == 1 && currentTriggerTrack->IsMCKaon()) ||
            (fMassAssumption == 2 && currentTriggerTrack->IsMCProton())) {

            if (currentTriggerTrack->IsPhysicalPrimary() && fSettings->IsTrackSelectedForCharge(currentTriggerTrack)) {
                fPtHistogramPrimTruth->Fill(currentTriggerTrack->MCPt());
                if (currentTriggerTrack->GetFilterMap() & fSettings->GetFilterMask()) {
                    fPtHistogramPrimRec->Fill(currentTriggerTrack->MCPt());
                    if (fSettings->IsTrackSelectedForPID(currentTriggerTrack)) {
                        fPtHistogramPrimRecSelForPID->Fill(currentTriggerTrack->MCPt());
                        if (currentTriggerTrack->IsTOFMatch()) {
                            fPtHistogramPrimRecMatched->Fill(currentTriggerTrack->MCPt());
                        }
                    }
                }
            } 
        }
        // Check the pT of the trigger (this cuts away the largest number of tracks).
		if ((currentTriggerTrack->MCPt() < fSettings->GetMinTriggerPt()) ||
			(currentTriggerTrack->MCPt() > fSettings->GetMaxTriggerPt())) {continue;}
		
		// Create a copy of the current trigger track, and add to the array of donated triggers.
        // The donated triggers will later be added to the pool. Note that a donated trigger does not 
        // need to be reconstructed.
		AliMiniTrackDiHadronPID* donatedTrigger = new AliMiniTrackDiHadronPID(*currentTriggerTrack);
		donatedTriggers->AddLast(donatedTrigger);

		// Loop over associated tracks (but only when we're not doing a rerun!)
		if (RerunIsInProgress()) {continue;}

        // Fill histogram with number of MC truth and reconstructed triggers. Note that we still want to continue
        // even if the trigger is not reconstructed (we may want to calculate truth-truth correlations.
        // Bugfix: Note also that these histograms should no longer be filled when a rerun is in progress!
		fNTotTriggersTruth->Fill(currentTriggerTrack->MCPt());
	    if (currentTriggerTrack->GetFilterMap() & fSettings->GetFilterMask()) {
            fNTotTriggersRec->Fill(currentTriggerTrack->MCPt());
        } 

		for (Int_t iAssocTrack = 0; iAssocTrack < fCurrentEvent->GetNTracks(); ++iAssocTrack) {
	
            CorrelateAssociatedTrack(currentTriggerTrack, iTriggerTrack, iAssocTrack);

		}
	}

	// If No rerun is in progress, and the event yielded triggers, then mark the event as an event with triggers.
	if (fEventsWithTriggers) {
		if (!RerunIsInProgress() && donatedTriggers->GetEntries()) {

			if (fEventsWithTriggers->GetNoElements() <= fTotalEventIndex) {
				fEventsWithTriggers->ResizeTo(fTotalEventIndex + 1000);
			}

			((*fEventsWithTriggers)[fTotalEventIndex])++;
		}
	}

	// Mix events if the pool is ready, and the current event contains triggers. Note that this last condition ensures
    // that only events with at least one trigger get mixed with other events. This is especially important in pp and pPb
    // collisions, where the efficiency and eta-distribution of tracks is different for events with or without triggers. In
    // fact, there is probably also a small difference between events with 1,2,3, etc. triggers, however this is ignored here.
	if (fPool->IsReady() && donatedTriggers->GetEntries()) {

		// Create an array that the pool can use to provide us with triggers. Since the pool is owner of the triggers,
		// this array does not own anything.
		TObjArray* receivedTriggers = new TObjArray(10);

		// Receive triggers appropriate for the current event (same vtxZ and centrality).
		fPool->GetMixTriggers(currentEvent, receivedTriggers);
		// cout << "Mixing with " << receivedTriggers->GetEntriesFast() << " triggers. " << endl;

		// Loop over the received triggers.
		for (Int_t iTriggerTrack = 0; iTriggerTrack < receivedTriggers->GetEntriesFast(); ++iTriggerTrack) { 

			AliMiniTrackDiHadronPID* currentTriggerTrack = (AliMiniTrackDiHadronPID*)receivedTriggers->At(iTriggerTrack);

			// Loop over this events associated tracks.
			for (Int_t iAssocTrack = 0; iAssocTrack < fCurrentEvent->GetNTracks(); ++iAssocTrack) {
			
                // Since the associated and trigger track are not taken from the same array, it is no use comparing
                // their indices.
                CorrelateAssociatedTrack(currentTriggerTrack, -1, iAssocTrack, kTRUE);
			
			}
		}

		delete receivedTriggers; receivedTriggers = 0x0;

	}

	// Supply new triggers to the pool, but of course only if there are triggers to donate.
	// If not, then delete the array.
	//
	// The method AddEventTriggers(...) releases ownership over the array to the pool, so we 
	// shouldn't delete it ourselves!
	if (donatedTriggers->GetEntries()) {

		// Pool status before
		Bool_t poolIsReady = fPool->IsReady();
		fPool->AddEventTriggers(currentEvent, donatedTriggers, fTotalEventIndex);

		// If the pool status went to ready in this event, then request
		if (poolIsReady == kFALSE) {
			if (fPool->IsReady()) {
				RequestRerunUntilCurrentEvent();
				AliLogDiHadronPID::Info(Form("Pool is ready now, requesting rerun up to this point."), __func__, ClassName());
				fPool->Print(0);
			}
		}

	} else {
		if (donatedTriggers) {delete donatedTriggers; donatedTriggers = 0x0;}
	}

	// fPool->Print(0);

}

// -----------------------------------------------------------------------
void AliCorrelateMCTruthDiHadronPID::CorrelateAssociatedTrack(const AliMiniTrackDiHadronPID* triggerTrack, Int_t iTriggerTrack, Int_t iAssocTrack, Bool_t isMixedEvents) {

    // Load Associated Track.
    AliMiniTrackDiHadronPID* currentAssocTrack = fCurrentEvent->GetTrack(iAssocTrack);
    if (!currentAssocTrack) {
        AliLogDiHadronPID::Error("Could not load associated track.", __func__, ClassName());
        return;
    }

    // Check the if the associated track has the desired charge (as set in the settings).
    if (!(fSettings->IsTrackSelectedForCharge(currentAssocTrack))) {return;}

    // Check the pT of the associated track against the desired range.
    if ((currentAssocTrack->MCPt() < fSettings->GetMinAssociatedPt()) ||
        (currentAssocTrack->MCPt() > fSettings->GetMaxAssociatedPt())) {return;}

    // Protect against making auto-correlations.
    if (iTriggerTrack == iAssocTrack) {
        AliLogDiHadronPID::Error(Form("Same track is used as trigger and associated."), __func__, ClassName());
        return; 
    }

    // Fill the number of same pairs histogram.								
    Double_t deltaPhi = triggerTrack->MCPhi() - currentAssocTrack->MCPhi();
    if (deltaPhi < -TMath::Pi()/2.) {deltaPhi += 2.*TMath::Pi();}
    else if (deltaPhi > 3.*TMath::Pi()/2.) {deltaPhi -= 2.*TMath::Pi();}
    Double_t deltaEta = triggerTrack->MCEta() - currentAssocTrack->MCEta();

    // Determine the pt and eta bin.
    Int_t ptBin = fPtAxis->FindBin(currentAssocTrack->MCPt());
    if ((ptBin < 1) || (ptBin > fPtAxis->GetNbins())) {return;}
    Int_t etaBin = fEtaAxis->FindBin(currentAssocTrack->MCEta());
    if ((etaBin < 1) || (etaBin > fEtaAxis->GetNbins())) {return;}

    // Check the MC truth species of the associated particle.
    if ((fMassAssumption == 0 && currentAssocTrack->IsMCPion()) ||
        (fMassAssumption == 1 && currentAssocTrack->IsMCKaon()) ||
        (fMassAssumption == 2 && currentAssocTrack->IsMCProton())) {

        // Check the reconstruction status of the trigger and the associated particle.
        Bool_t triggerIsReconstructed = (triggerTrack->GetFilterMap() & fSettings->GetFilterMask());
        Bool_t assocIsReconstructed = (currentAssocTrack->GetFilterMap() & fSettings->GetFilterMask());
        Bool_t assocIsSelectedForPID = (assocIsReconstructed && fSettings->IsTrackSelectedForPID(currentAssocTrack));
        Bool_t assocIsTOFMatched = (assocIsSelectedForPID && currentAssocTrack->IsTOFMatch());

        // Fill the histograms.
        if (currentAssocTrack->IsPhysicalPrimary()) {
            TH2D* hPrimTruth = (isMixedEvents ? fMCPIDMixedPrimTruth[ptBin-1] : fMCPIDCorrelationsPrimTruth[ptBin-1]); 
            hPrimTruth->Fill(deltaPhi, deltaEta);
            if (assocIsReconstructed && triggerIsReconstructed) {
                TH2D* hPrimRec = (isMixedEvents ? fMCPIDMixedPrimRec[ptBin-1] : fMCPIDCorrelationsPrimRec[ptBin-1]); 
                hPrimRec->Fill(deltaPhi, deltaEta);
                if (assocIsSelectedForPID) {
                    TH2D* hPrimRecSelForPID = (isMixedEvents ? fMCPIDMixedPrimRecSelForPID[ptBin-1] : fMCPIDCorrelationsPrimRecSelForPID[ptBin-1]); 
                    hPrimRecSelForPID->Fill(deltaPhi, deltaEta);
                    if (assocIsTOFMatched) {
                        TH2D* hPrimRecMatched = (isMixedEvents ? fMCPIDMixedPrimRecMatched[ptBin-1] : fMCPIDCorrelationsPrimRecMatched[ptBin-1]); 
                        hPrimRecMatched->Fill(deltaPhi, deltaEta);
                    }
                }
            }
        }

        if (currentAssocTrack->IsSecondaryFromWeakDecay()) {
            TH2D* hWeakTruth = (isMixedEvents ? fMCPIDMixedWeakTruth[ptBin-1] : fMCPIDCorrelationsWeakTruth[ptBin-1]); 
            hWeakTruth->Fill(deltaPhi, deltaEta);
            if (assocIsReconstructed && triggerIsReconstructed) {
                TH2D* hWeakRec = (isMixedEvents ? fMCPIDMixedWeakRec[ptBin-1] : fMCPIDCorrelationsWeakRec[ptBin-1]); 
                hWeakRec->Fill(deltaPhi, deltaEta);
            }
        }

        if (currentAssocTrack->IsSecondaryFromMaterialDecay()) {
            TH2D* hMatTruth = (isMixedEvents ? fMCPIDMixedMatTruth[ptBin-1] : fMCPIDCorrelationsMatTruth[ptBin-1]); 
            hMatTruth->Fill(deltaPhi, deltaEta);
            if (assocIsReconstructed && triggerIsReconstructed) {
                TH2D* hMatRec = (isMixedEvents ? fMCPIDMixedMatRec[ptBin-1] : fMCPIDCorrelationsMatRec[ptBin-1]); 
                hMatRec->Fill(deltaPhi, deltaEta);
            }
        }
    }
}

// -----------------------------------------------------------------------
void AliCorrelateMCTruthDiHadronPID::UserFinalizeAnalysis() {

	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

    // Create SP efficiency histograms.
    fSPEffTracking = (TH1D*)fPtHistogramPrimRec->Clone();
    fSPEffTracking->Sumw2();
    fSPEffTracking->Divide(fSPEffTracking, fPtHistogramPrimTruth, 1.,1.,"b");
    fSPEffTracking->SetNameTitle("spEffTracking", "Single Part. Tracking Eff.");

    fSPEffSelForPID = (TH1D*)fPtHistogramPrimRecSelForPID->Clone();
    fSPEffSelForPID->Sumw2();
    fSPEffSelForPID->Divide(fSPEffSelForPID, fPtHistogramPrimRec, 1.,1.,"b");
    fSPEffSelForPID->SetNameTitle("spEffSelForPID", "Single Part. SelForPID Eff.");

    fSPEffMatched = (TH1D*)fPtHistogramPrimRecMatched->Clone();
    fSPEffMatched->Sumw2();
    fSPEffMatched->Divide(fSPEffMatched, fPtHistogramPrimRecSelForPID, 1.,1.,"b");
    fSPEffMatched->SetNameTitle("spEffMatched", "Single Part. Matched Eff.");

    // Create efficiency histograms of the correlations at the origin, i.e.,
    // the convolution of e_trig and e_assoc, divided by <e_trig>.
	fCorrEffTracking = (TH1D*)AliHistToolsDiHadronPID::MakeHist("corrEffTracking", "Corr. Tracking Eff.", "D", fSettings->GetPtAxis());
	fCorrEffSelForPID = (TH1D*)AliHistToolsDiHadronPID::MakeHist("corrEffSelForPID", "Corr. SelForPID Eff.", "D", fSettings->GetPtAxis());
	fCorrEffMatched = (TH1D*)AliHistToolsDiHadronPID::MakeHist("corrEffMatched", "Corr. Matched Eff.", "D", fSettings->GetPtAxis()); 
   
    // Calculate the average tracking efficiency for the trigger particles.
    if (fNTotTriggersTruth->GetBinContent(1) < 1.e-10 || fNTotTriggersRec->GetBinContent(1) < 1.e-10) {
        AliLogDiHadronPID::Error("No truth and/or reconstructed triggers found.", __func__, ClassName());
        return;
    }

    Double_t avgTrigEff = fNTotTriggersRec->GetBinContent(1) / fNTotTriggersTruth->GetBinContent(1);
   
    // Calculate the efficiency at the origin.
    Int_t minBinDPhi = fDPhiAxis->FindBin(-0.00001);
    Int_t maxBinDPhi = fDPhiAxis->FindBin(0.00001);
    Int_t minBinDEta = fDEtaAxis->FindBin(-0.00001);
    Int_t maxBinDEta = fDEtaAxis->FindBin(0.00001);

    for (Int_t iPtBin = 1; iPtBin <= fSettings->GetPtAxis()->GetNbins(); iPtBin++) {
        Double_t truthYield = 0.;
        Double_t recYield = 0.;
        Double_t pidYield = 0.;
        Double_t matchYield = 0.;
        for (Int_t iDPhiBin = minBinDPhi; iDPhiBin <= maxBinDPhi; iDPhiBin++) {
            for (Int_t iDEtaBin = minBinDEta; iDEtaBin <= maxBinDEta; iDEtaBin++) {
                truthYield += fMCPIDCorrelationsPrimTruth[iPtBin-1]->GetBinContent(iDPhiBin, iDEtaBin);
                recYield += fMCPIDCorrelationsPrimRec[iPtBin-1]->GetBinContent(iDPhiBin, iDEtaBin);
                pidYield += fMCPIDCorrelationsPrimRecSelForPID[iPtBin-1]->GetBinContent(iDPhiBin, iDEtaBin);
                matchYield += fMCPIDCorrelationsPrimRecMatched[iPtBin-1]->GetBinContent(iDPhiBin, iDEtaBin); 
            }
        }

        if (truthYield > 1.e-10) {fCorrEffTracking->SetBinContent(iPtBin, recYield / truthYield);}
        else {
            fCorrEffTracking->SetBinContent(iPtBin, -999.);
            AliLogDiHadronPID::Warning(Form("No truth yield in ptBin %i", iPtBin), __func__, ClassName());
        }
        if (recYield > 1.e-10) {fCorrEffSelForPID->SetBinContent(iPtBin, pidYield / recYield);}
        else {
            fCorrEffSelForPID->SetBinContent(iPtBin, -999.);
            AliLogDiHadronPID::Warning(Form("No reconstructed yield in ptBin %i", iPtBin), __func__, ClassName());
        }
        if (pidYield > 1.e-10) {fCorrEffMatched->SetBinContent(iPtBin, matchYield / pidYield);}
        else {
            fCorrEffMatched->SetBinContent(iPtBin, -999.);
            AliLogDiHadronPID::Warning(Form("No yield passing PID cut in ptBin %i", iPtBin), __func__, ClassName());
        }
    
    }
    
    // Correct the pair tracking efficiency for the trigger efficiency.
    fCorrEffTracking->Scale(1. / avgTrigEff);

    // It turns out that the two-particle efficiency, evaluated at the origin does not correspond to the 
    // product of the trigger and associated efficiencies. 
    //
    // The most likely reason is that the efficiency has significant "bumps" in DPhi. We will therefore here
    // repeat the exercise, but integrate over DPhi.
	fCorrEffTrackingDPhiStrip = (TH1D*)AliHistToolsDiHadronPID::MakeHist("corrEffTrackingDPhiStrip", "Corr. Tracking Eff. (#Delta#phi strip)", "D", fSettings->GetPtAxis());
	fCorrEffSelForPIDDPhiStrip = (TH1D*)AliHistToolsDiHadronPID::MakeHist("corrEffSelForPIDDPhiStrip", "Corr. SelForPID Eff. (#Delta#phi strip)", "D", fSettings->GetPtAxis());
	fCorrEffMatchedDPhiStrip = (TH1D*)AliHistToolsDiHadronPID::MakeHist("corrEffMatchedDPhiStrip", "Corr. Matched Eff. (#Delta#phi strip)", "D", fSettings->GetPtAxis()); 

    for (Int_t iPtBin = 1; iPtBin <= fSettings->GetPtAxis()->GetNbins(); iPtBin++) {
        cout << "PtBin: " << iPtBin << endl;
        Double_t truthYield = 0.;
        Double_t recYield = 0.;
        Double_t pidYield = 0.;
        Double_t matchYield = 0.;
        for (Int_t iDPhiBin = 1; iDPhiBin <= fDPhiAxis->GetNbins(); iDPhiBin++) {
            for (Int_t iDEtaBin = minBinDEta; iDEtaBin <= maxBinDEta; iDEtaBin++) {
                truthYield += fMCPIDCorrelationsPrimTruth[iPtBin-1]->GetBinContent(iDPhiBin, iDEtaBin);
                recYield += fMCPIDCorrelationsPrimRec[iPtBin-1]->GetBinContent(iDPhiBin, iDEtaBin);
                pidYield += fMCPIDCorrelationsPrimRecSelForPID[iPtBin-1]->GetBinContent(iDPhiBin, iDEtaBin);
                matchYield += fMCPIDCorrelationsPrimRecMatched[iPtBin-1]->GetBinContent(iDPhiBin, iDEtaBin); 
            }
        }

        if (truthYield > 1.e-10) {fCorrEffTrackingDPhiStrip->SetBinContent(iPtBin, recYield / truthYield);}
        else {
            fCorrEffTrackingDPhiStrip->SetBinContent(iPtBin, -999.);
            AliLogDiHadronPID::Warning(Form("No truth yield in ptBin %i", iPtBin), __func__, ClassName());
        }
        if (recYield > 1.e-10) {fCorrEffSelForPIDDPhiStrip->SetBinContent(iPtBin, pidYield / recYield);}
        else {
            fCorrEffSelForPIDDPhiStrip->SetBinContent(iPtBin, -999.);
            AliLogDiHadronPID::Warning(Form("No reconstructed yield in ptBin %i", iPtBin), __func__, ClassName());
        }
        if (pidYield > 1.e-10) {fCorrEffMatchedDPhiStrip->SetBinContent(iPtBin, matchYield / pidYield);}
        else {
            fCorrEffMatchedDPhiStrip->SetBinContent(iPtBin, -999.);
            AliLogDiHadronPID::Warning(Form("No yield passing PID cut in ptBin %i", iPtBin), __func__, ClassName());
        }
    
    }

    // Correct the pair tracking efficiency for the trigger efficiency.
    fCorrEffTrackingDPhiStrip->Scale(1. / avgTrigEff);

    // Repeat the same, but now integrate over the whole (DPhi, DEta) surface.
	fCorrEffTrackingFullDPhiDEta = (TH1D*)AliHistToolsDiHadronPID::MakeHist("corrEffTrackingFullDPhiDEta", "Corr. Tracking Eff. (Full #Delta#phi, #Delta#eta )", "D", fSettings->GetPtAxis());
	fCorrEffSelForPIDFullDPhiDEta = (TH1D*)AliHistToolsDiHadronPID::MakeHist("corrEffSelForPIDFullDPhiDEta", "Corr. SelForPID Eff. (Full #Delta#phi, #Delta#eta)", "D", fSettings->GetPtAxis());
	fCorrEffMatchedFullDPhiDEta = (TH1D*)AliHistToolsDiHadronPID::MakeHist("corrEffMatchedFullDPhiDEta", "Corr. Matched Eff. (Full #Delta#phi, #Delta#eta )", "D", fSettings->GetPtAxis()); 

    for (Int_t iPtBin = 1; iPtBin <= fSettings->GetPtAxis()->GetNbins(); iPtBin++) {
        cout << "PtBin: " << iPtBin << endl;
        Double_t truthYield = 0.;
        Double_t recYield = 0.;
        Double_t pidYield = 0.;
        Double_t matchYield = 0.;
        for (Int_t iDPhiBin = 1; iDPhiBin <= fDPhiAxis->GetNbins(); iDPhiBin++) {
            for (Int_t iDEtaBin = 1; iDEtaBin <= fDEtaAxis->GetNbins(); iDEtaBin++) {
                truthYield += fMCPIDCorrelationsPrimTruth[iPtBin-1]->GetBinContent(iDPhiBin, iDEtaBin);
                recYield += fMCPIDCorrelationsPrimRec[iPtBin-1]->GetBinContent(iDPhiBin, iDEtaBin);
                pidYield += fMCPIDCorrelationsPrimRecSelForPID[iPtBin-1]->GetBinContent(iDPhiBin, iDEtaBin);
                matchYield += fMCPIDCorrelationsPrimRecMatched[iPtBin-1]->GetBinContent(iDPhiBin, iDEtaBin); 
            }
        }

        if (truthYield > 1.e-10) {fCorrEffTrackingFullDPhiDEta->SetBinContent(iPtBin, recYield / truthYield);}
        else {
            fCorrEffTrackingFullDPhiDEta->SetBinContent(iPtBin, -999.);
            AliLogDiHadronPID::Warning(Form("No truth yield in ptBin %i", iPtBin), __func__, ClassName());
        }
        if (recYield > 1.e-10) {fCorrEffSelForPIDFullDPhiDEta->SetBinContent(iPtBin, pidYield / recYield);}
        else {
            fCorrEffSelForPIDFullDPhiDEta->SetBinContent(iPtBin, -999.);
            AliLogDiHadronPID::Warning(Form("No reconstructed yield in ptBin %i", iPtBin), __func__, ClassName());
        }
        if (pidYield > 1.e-10) {fCorrEffMatchedFullDPhiDEta->SetBinContent(iPtBin, matchYield / pidYield);}
        else {
            fCorrEffMatchedFullDPhiDEta->SetBinContent(iPtBin, -999.);
            AliLogDiHadronPID::Warning(Form("No yield passing PID cut in ptBin %i", iPtBin), __func__, ClassName());
        }
    
    }

    // Correct the pair tracking efficiency for the trigger efficiency.
    fCorrEffTrackingFullDPhiDEta->Scale(1. / avgTrigEff);
}

// -----------------------------------------------------------------------
void AliCorrelateMCTruthDiHadronPID::UserSaveOutput() {

    // TODO: Should check if ptr is nonzero before write call.

	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	fNTotTriggersTruth->Write();
	fPtHistogramTruth->Write();

	fNTotTriggersRec->Write();
	fPtHistogramRec->Write();
   
    fPtHistogramPrimTruth->Write(); 
    fPtHistogramPrimRec->Write();
    fPtHistogramPrimRecSelForPID->Write();
    fPtHistogramPrimRecMatched->Write();

	for (Int_t iPtBin = 1; iPtBin <= fSettings->GetPtAxis()->GetNbins(); iPtBin++) {

        fMCPIDCorrelationsPrimRec[iPtBin-1]->Write();
        fMCPIDCorrelationsWeakRec[iPtBin-1]->Write();
        fMCPIDCorrelationsMatRec[iPtBin-1]->Write();
        
        fMCPIDCorrelationsPrimTruth[iPtBin-1]->Write();
        fMCPIDCorrelationsWeakTruth[iPtBin-1]->Write();
        fMCPIDCorrelationsMatTruth[iPtBin-1]->Write();
        
        fMCPIDMixedPrimRec[iPtBin-1]->Write();
        fMCPIDMixedWeakRec[iPtBin-1]->Write();
        fMCPIDMixedMatRec[iPtBin-1]->Write();
        
        fMCPIDMixedPrimTruth[iPtBin-1]->Write();
        fMCPIDMixedWeakTruth[iPtBin-1]->Write();
        fMCPIDMixedMatTruth[iPtBin-1]->Write();

        fMCPIDCorrelationsPrimRecSelForPID[iPtBin-1]->Write();
        fMCPIDCorrelationsPrimRecMatched[iPtBin-1]->Write();

        fMCPIDMixedPrimRecSelForPID[iPtBin-1]->Write();
        fMCPIDMixedPrimRecMatched[iPtBin-1]->Write();

    }

    // Write the SP efficiency histograms.
    if (fSPEffTracking) {fSPEffTracking->Write();} 
    if (fSPEffSelForPID) {fSPEffSelForPID->Write();} 
    if (fSPEffMatched) {fSPEffMatched->Write();} 

    if (fCorrEffTracking) {fCorrEffTracking->Write();} 
    if (fCorrEffSelForPID) {fCorrEffSelForPID->Write();} 
    if (fCorrEffMatched) {fCorrEffMatched->Write();} 

    if (fCorrEffTrackingDPhiStrip) {fCorrEffTrackingDPhiStrip->Write();} 
    if (fCorrEffSelForPIDDPhiStrip) {fCorrEffSelForPIDDPhiStrip->Write();} 
    if (fCorrEffMatchedDPhiStrip) {fCorrEffMatchedDPhiStrip->Write();} 

    if (fCorrEffTrackingFullDPhiDEta) {fCorrEffTrackingFullDPhiDEta->Write();} 
    if (fCorrEffSelForPIDFullDPhiDEta) {fCorrEffSelForPIDFullDPhiDEta->Write();} 
    if (fCorrEffMatchedFullDPhiDEta) {fCorrEffMatchedFullDPhiDEta->Write();} 

}

// -----------------------------------------------------------------------
void AliCorrelateMCTruthDiHadronPID::Print(Option_t*) const {
	
	// Print info about the class.
	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

}
