#ifndef ALIOBJECTARRAY2DDIHADRONPID_H
#define ALIOBJECTARRAY2DDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TObjArray.h"

class AliObjectArray2DDiHadronPID : public TObject {

public:
	AliObjectArray2DDiHadronPID();
	AliObjectArray2DDiHadronPID(Int_t sizex, Int_t sizey);
	~AliObjectArray2DDiHadronPID();

	Bool_t					SetAt(TObject* obj, Int_t xx, Int_t yy);
	TObject*				GetAt(Int_t xx, Int_t yy) const;
	Int_t					GetSizeX() const {return fSizeX;}
	Int_t					GetSizeY() const {return fSizeY;}
	Int_t					GetSizeArray() const {return fArray->GetSize();}

	void					Print(Option_t*) const; // *MENU*

private:
	Bool_t					BoundsAreOK(Int_t xx, Int_t yy) const;
	Int_t					GetIndex(Int_t xx, Int_t yy) const;

private:
	TObjArray*				fArray; //->
	Int_t					fSizeX;
	Int_t					fSizeY;

	ClassDef(AliObjectArray2DDiHadronPID, 1);

};


#endif
