#ifndef ALIHISTTOOLSDIHADRONPID_H
#define ALIHISTTOOLSDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TAxis.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "THn.h"
#include "TF2.h"
#include "TCanvas.h"
#include "TSpline.h"
#include "TF1.h"
#include "TVector.h"
#include "TGraphErrors.h"
class TGraphAsymmErrors;

/**
 * Toolbox for tools operating on, or generating histograms.
 * \todo Methods that generate histograms should move to a factory class.
 * \todo Thorough cleanup needed.
 */
class AliHistToolsDiHadronPID {

private:
	AliHistToolsDiHadronPID() {;}
	~AliHistToolsDiHadronPID() {;}

public:

	// Histogram Creation (returned instances are owned by the user).
	static TH1F* RebinVariableBinning(const TH1F* histIn, const Double_t* binsx, Int_t Nbinsx, Bool_t density = kTRUE); // OLD
	static TH1F* RebinVariableBinning(const TH1F* histIn, const TH1F* histAxis, Bool_t density = kTRUE); // OLD
	static TH1F* RebinVariableBinning(const TH1F* histIn, const TAxis* xaxis, Bool_t density = kTRUE); // OLD
	static TH1F* TrimHisto(const TH1F* histo, Int_t firstbin, Int_t lastbin);
	static TH1F* ShiftHistogram(const TH1F* hist, Int_t newFirstBin);

    static TH1F* MakeHistFromVector(const char* name, const char* title, const TVectorF& vector, Int_t domainInSigma = 3, Int_t nBins = 100);
	static TH1* MakeHist(const char* name, const char* title, const char* type, const TAxis* xAxis, const TAxis* yAxis = 0x0, const TAxis* zAxis = 0x0); // NEW
	static THn* MakeHist4D(const char* name, const char* title, const char* type, const TAxis* axis0, const TAxis* axis1, const TAxis* axis2, const TAxis* axis3); // NEW

	static void NormalizeHist(TH1* hist, Double_t value = 1.);
	static Double_t GetSumHist(const TH1* hist);
	static TH1F* MakeHist1D(const char* name, const char* title, const TAxis* xaxis); // OLD
	static TH2F* MakeHist2D(const char* name, const char* title, const TAxis* xaxis, const TAxis* yaxis); // OLD
	static TH3F* MakeHist3D(const char* name, const char* title, const TAxis* xaxis, const TAxis* yaxis, const TAxis* zaxis); // OLD
	static TH3F* MakeHist3D(const char* name, const char* title, 
		Int_t nbinsX, Double_t minX, Double_t maxX,
		Int_t nbinsY, Double_t minY, Double_t maxY,
		Int_t nbinsZ, const Double_t* zaxis);
	static TH2F* Function2DToHist2D(const TF2* function, const TH2* grid);
	static Double_t* CreateAxis(Int_t nbins, Double_t min, Double_t max);
	static TAxis* CreateTAxis(Int_t nbins, Double_t min, Double_t max);
	static TArrayD* CreateTArrayD(Int_t size, Double_t min, Double_t max);
	static void PrintTAxis(const TAxis* axis);

	static TH1F* SplineToHist(const TSpline* spline, const TH1* hist) {return SplineToHist(spline, hist->GetXaxis());}
	static TH1F* SplineToHist(const TSpline* spline, const TAxis* axis);
	static void DivideThroughSpline(TH1F* hist, const TSpline* spline);

	static TGraphAsymmErrors* AddSystematics(const TH1F* histo, const TGraphAsymmErrors* grSyst);
	// Histogram Operations (done on the histogram in the argument).
	static void ConstFit(const TH1* hist, Double_t& fit, Double_t& fiterr);
	static void ConstMinusHist(TH1F* hist, Float_t cc = 1); // OLD
        static void ScaleHistPt(TH1* hist);
	static void ScaleHistToMaxBin(TH1* hist, Float_t max = 1.) {hist->Scale(max / hist->GetBinContent(hist->GetMaximumBin()));}
	static void NormaliseMixedEvents(TH2* hist, TString options);
	static void RandomizeHist(TH1* hist);
	static void RandomizeCollectionOfHist(TCollection* collection);

	static TCanvas* ShowSlicesParallelToX(const TH2F* hist, Int_t minBin = 0, Int_t maxBin = -1) {return ShowSlices(hist, 1, minBin, maxBin);}
	static TCanvas* ShowSlicesParallelToY(const TH2F* hist, Int_t minBin = 0, Int_t maxBin = -1) {return ShowSlices(hist, 0, minBin, maxBin);}
	static TCanvas* CreateSpectraComparison(const char* name, const char* title, const TH1F* h1, const TH1F* h2, Int_t markerstyle = 8, Bool_t logy = kTRUE); // OLD
	static void FixPlotRange(TCanvas* cvs);

	// Histogram conversions.
	static void ConvertAllTH1sToCMacro(const char* fileNameIn, const char* fileNameOut = "out.C");
	static void ConvertHistogramToTextFile(const TH1* hist, const char* fileName = "out.txt", const char* type = "double", Bool_t indent = kTRUE);
private:
	static void WriteArrayToFile(const char* arrayName, const Double_t* array, Int_t nBins, const char* fileName, const char* type, Bool_t indent = kTRUE);
public:

	// Checks and properties.
	static Bool_t DoAxesMatch(const TAxis* axis1, const TAxis* axis2);
	static void AverageX(const TH1* hist, Double_t& avg, Double_t& error);
	static void RMS(const TH1* hist, Double_t& rms, Double_t& error);
	static void CountInEllips(const TH2* hist, Double_t rX, Double_t rY, Double_t& count, Double_t& error, Bool_t isDensity = kFALSE);

	// Histogram Deletion.
	static void DeleteCanvasAndPrimitives(TCanvas* cvs);

	// Non-standard projections.
	static TH1F* ProjectionY_RMSError(const TH2F* hist, Int_t minBinX = -1, Int_t maxBinX = -1);
	static TH1F* Ratio2DProjectX(const TH2F* h1, const TH2F* h2);
	static TH1F* CutOutJetAndProjectOnDPhi(const TH2F* hist, Double_t maxDEta, Double_t DPhiJet, Double_t DEtaJet, 
                                           Bool_t useConstFit, Bool_t yieldIsPerTrigger = kTRUE);

	// DCT Transformation.
	static TH1F* DCT(const char* name, const TH1* data, Int_t DSTtype = 2);
	static TF1* CreateFlowFuncFromDCT(const char* name, const TH1* data, const TH1* dct, Int_t minOrder = 1, Int_t maxOrder = 999);
	static TF1* CreateFlowFuncErrorFromDCT(const char* name, const TH1* data, const TH1* dct, Int_t minOrder = 1, Int_t maxOrder = 999);
	static TGraphErrors* CreateFlowFuncErrorsFromDCT(const TH1* data, const TH1* symmData, Int_t minOrder = 1, Int_t maxOrder = 999);

	static TH1F* CreateSymmetrisedHist(const char* name, const TH1* hist);

private:
	static TCanvas* ShowSlices(const TH2F* hist, Bool_t parallelToX, Int_t minBin, Int_t maxBin);

	// Histogram operations (specific implementations).
	//static Bool_t OperateWithTH1onTHn(THn* hist1, const TH1* hist2, Operation operation, const TString& options);

};

#endif
