#ifndef ALIASSOCIATEDYIELDDIHADRONPID_H
#define ALIASSOCIATEDYIELDDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TCanvas.h"
#include "TH2F.h"
#include "TGraphErrors.h"
#include "AliSettingsDiHadronPID.h"

/**
 * This class is meant as a wrapper around a 2D di-hadron histogram. It does not 
 * care about the details of the histogram (species, pT, etc.), and it contains a number of operations that
 * can be applied on the histogram that are helpful in the analysis. This class does 
 * assume that the X-axis of the histogram corresponds to the \f$\Delta\phi\f$ axis, while
 * the Y-axis should correspond to the \f$\Delta\eta\f$ axis.
 *
 * The Plot*() methods are designed in such a way that they represent the analysis that
 * was carried out, but do not add any information. For example, when one has chosen
 * a certain range of flow coefficients, then that range is used in the determination of
 * the jet yield, and also by the Plot*() methods.  
 *
 * \note This method assumes that the histogram that it wraps around is a density, i.e., that
 * it is scaled with its bin width.
 */
class AliAssociatedYieldDiHadronPID : public TH2F {
	
public:
	AliAssociatedYieldDiHadronPID();
	AliAssociatedYieldDiHadronPID(const TH2F& other, Double_t nTriggers = -1.);
	AliAssociatedYieldDiHadronPID(const AliAssociatedYieldDiHadronPID& other);
	~AliAssociatedYieldDiHadronPID();

private:
	const AliAssociatedYieldDiHadronPID& operator=(const AliAssociatedYieldDiHadronPID& other);

// Misc. Functions.
public:
	void					Randomize();
	TH2F*					GetSigmaOverSqrtNDPhiDEta() const;
	void					FillSigmaOverSqrtN(TH1F* hist) const;
	using 					TH2F::FillRandom; // Explicitly tell the compiler that we're not trying to overload the virtual method FillRandom in TH2F.
	void					FillRandom(Double_t nTriggers, Double_t bgdDensityPerTrigger, Double_t peakYield, Double_t peakWidth, 
								Int_t nFlowCoefficients, const Float_t* flowCoefficients = 0x0, TH2F* signalHist = 0x0, TH2F* bgdHist = 0x0);

private:
	void					CleanUp();
	Bool_t 					AnalysisIsDone() const;

// DPhi analysis.
public:
    void                    DivideThroughNTriggers();

	void					PerformDPhiAnalysis(Double_t maxAbsDEta = 1.6, Double_t jetRadiusBG = -1., Int_t minVn = 1, Int_t maxVn = 4, Bool_t useConstFit = kTRUE);
	
	TH1F*					GetCloneDPhiProjection() const;
	TH1F*					GetCloneDCTCoefficients(Bool_t convertToVnDelta = kTRUE) const;
	void					GetFlowCoefficient(Int_t nn, Double_t& vnD, Double_t& error) const;
	void					GetNSPeakWidthDPhi(Double_t& width, Double_t& error) const; 

	TCanvas*				PlotDPhiFit(Color_t dataColor = kBlack, Style_t markerStyle = 21) const;
	TCanvas*				PlotBackgroundDPhiDecomposed(Color_t color = kRed) const;
	TCanvas*				PlotBackgroundDPhiErrorBand(Color_t color = kRed) const;

private:
	TCanvas*				PlotSignalAndBackgroundDPhi(Color_t backgroundColor = kRed, Color_t signalColor = kBlack) const;
	TF1*					GetTotalFlowFunc(Int_t minVn, Int_t maxVn) const;
	TF1*					GetFlowFuncOneHarmonic(Int_t nn = 2) const;

// DEta analysis.
public:
	void					PerformDEtaAnalysis(Double_t maxAbsDPhi = 1., Double_t maxAbsDEta = 1.4);
	TH1F*					GetCloneDEtaProjection() const;
	void					GetNSJetYieldDEtaFit(Double_t& yield, Double_t& error) const;
	void					GetNSPeakWidthDEta(Double_t& width, Double_t& error) const; 
	void 					GetNSPeakDEtaFrac(Double_t& frac, Double_t& error) const;

	void					GetNSPeakWidth1DEta(Double_t& width, Double_t& error) const; 
	void					GetNSPeakWidth2DEta(Double_t& width, Double_t& error) const; 

	TCanvas*				PlotDEtaFit(Color_t dataColor = kBlack, Style_t markerStyle = 21) const;
	TCanvas*				PlotDEtaFitDG(Color_t dataColor = kBlack, Style_t markerStyle = 21) const;

// (DPhi,DEta) analysis.
public:

	void					GetNSJetYield(Double_t& yield, Double_t& error, Double_t jetRadiusSignal, Double_t* bgdYield = 0x0) const; 
	void 					GetBackgroundDensityDPhiDEta(Double_t& density, Double_t& error);
	
    TCanvas*                PlotJetPeak2DStack() const;
    
private:			

	// Misc.
	AliSettingsDiHadronPID*	fSettings; //!
    Double_t                fNTriggers; 
    Bool_t                  fYieldIsPerTrigger;

	// DPhi analysis.
	TH1F*					fDPhiProjection; //! \f$\Delta\phi\f$ projection, with integration range \f$(\Delta\eta_{min}, \Delta\eta_{max})\f$.
	TH1F*					fDPhiProjectionJet; //!
	TH1F*					fDPhiProjectionNoJet; //! \f$\Delta\phi\f$ projection, where bins in a certain jet radius are ignored.
	Double_t				fDPhiProjectionUsedJetRadius; //!
	Double_t				fDPhiProjectionUsedMaxAbsDEta; //!
	Bool_t					fDPhiProjectionUsedConstFit; //!
	
	TF1*					fDPhiProjectionGaussianFit; //!
	Int_t					fDPhiProjectionGaussianFitStatus; //!

	TH1F*					fDPhiProjectionNoJetDCT; //!

	TF1*					fDPhiProjectionTotalFlowFunc; //!
	TGraphErrors*			fDphiProjectionTotalFlowUncertaintyBand; //!
	Int_t					fDPhiProjectionMinVn; //!
	Int_t					fDPhiProjectionMaxVn; //!

	// DEta analysis.
	TH1F*					fDEtaProjection; //!
	Double_t				fDEtaProjectionUsedMaxAbsDPhi; //!
	Double_t				fDEtaProjectionUsedMaxAbsDEta; //!

	TF1*					fDEtaProjectionGaussianFit; //!
	Int_t 					fDEtaProjectionGaussianFitStatus; //!

	TF1*					fDEtaProjectionDoubleGaussianFit; //!
	Int_t 					fDEtaProjectionDoubleGaussianFitStatus; //!

	ClassDef(AliAssociatedYieldDiHadronPID, 1);

};

#endif
