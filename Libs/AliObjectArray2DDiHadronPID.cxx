/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  2D array for objects (wrapper around TObjArray).
//
//   - Warning -
//  The implementation is not at all perfect, i.e., using the inherited
//  methods from TObjArray may give unexpected behavior. This could have 
//  been prevented by private inheritance, however, we wish to retain the 
//  "Write" method.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "AliObjectArray2DDiHadronPID.h"
#include "AliLogDiHadronPID.h"

#include <iostream>
using namespace std;

ClassImp(AliObjectArray2DDiHadronPID);

// -----------------------------------------------------------------------
AliObjectArray2DDiHadronPID::AliObjectArray2DDiHadronPID():
	TObject(),
	fArray(0x0),
	fSizeX(5),
	fSizeY(5)

{
	// Default ctor.
	AliLogDiHadronPID::Debug2(Form("Default constructor called."), __func__, ClassName());
	
}

// -----------------------------------------------------------------------
AliObjectArray2DDiHadronPID::AliObjectArray2DDiHadronPID(Int_t sizex, Int_t sizey):
	TObject(),
	fArray(0x0),
	fSizeX(sizex),
	fSizeY(sizey)

{

	AliLogDiHadronPID::Debug2(Form("Constructor called with sizeX: %i, sizeY: %i", sizex, sizey), __func__, ClassName());
	
	// We always own our content.
	fArray = new TObjArray(fSizeX * fSizeY);
	fArray->SetOwner(kTRUE);

}

// -----------------------------------------------------------------------
AliObjectArray2DDiHadronPID::~AliObjectArray2DDiHadronPID() {

	AliLogDiHadronPID::Debug2(Form("Destructor called."), __func__, ClassName());
	
	if (fArray) {delete fArray; fArray = 0x0;}

}

// -----------------------------------------------------------------------
Bool_t AliObjectArray2DDiHadronPID::SetAt(TObject* obj, Int_t xx, Int_t yy) {
	
	if (!fArray) {fArray = new TObjArray(fSizeX * fSizeY); fArray->SetOwner(kTRUE);}
	
	Int_t index = GetIndex(xx, yy);
	if (index < 0) {return kFALSE;}
	if (index < GetSizeArray()) {
		if (fArray->At(index)) {fArray->RemoveAt(index);}
	}
	fArray->AddAtAndExpand(obj, index);
	return kTRUE;
}

// -----------------------------------------------------------------------
TObject* AliObjectArray2DDiHadronPID::GetAt(Int_t xx, Int_t yy) const {

	// In principle it's not so great that this method has a const qualifier.
	if (!fArray) {return 0x0;}

	Int_t index = GetIndex(xx, yy);
	if (index < 0) {return 0x0;}
	return (fArray->At(index));

}

// -----------------------------------------------------------------------
void AliObjectArray2DDiHadronPID::Print(Option_t*) const {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	cout << "-----------------------------" << endl;
	cout << " AliObjectArray2DDiHadronPID " << endl;
	cout << "-----------------------------" << endl;
	cout << " Size X: " << fSizeX << endl;
	cout << " Size Y: " << fSizeY << endl;
	cout << " Array Size: " << GetSizeArray() << endl;
	cout << "-----------------------------" << endl;

}

// -----------------------------------------------------------------------
Bool_t AliObjectArray2DDiHadronPID::BoundsAreOK(Int_t xx, Int_t yy) const {

	if ((xx >= 0) && (xx < fSizeX) && (yy >= 0) && (yy < fSizeY)) {return kTRUE;}
	else {return kFALSE;}

}

// -----------------------------------------------------------------------
Int_t AliObjectArray2DDiHadronPID::GetIndex(Int_t xx, Int_t yy) const {

	if (BoundsAreOK(xx,yy)) {return (xx + yy*fSizeX);} 
	else {return -999;}

}
