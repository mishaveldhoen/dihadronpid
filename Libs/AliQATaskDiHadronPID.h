#ifndef ALIQATASKDIHADRONPID_H
#define ALIQATASKDIHADRONPID_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TH1D.h"
#include "AliAnalyzeDataDiHadronPID.h"

/**
 * \brief Fill misc. QA histograms, not directly used for the analysis.
 *
 * Currently some histograms are filled that check the \f$p_{T}\f$ and
 * \f$\eta\f$ dependence of various cuts.
 */
class AliQATaskDiHadronPID : public AliAnalyzeDataDiHadronPID {
	
public:
	AliQATaskDiHadronPID(const char* name, const char* title);
	~AliQATaskDiHadronPID();

private:
	void							UserCreateOutput();
	void 							UserExec();
	void							UserFinalizeAnalysis();
	void							UserSaveOutput();

public:
	void 							Print(Option_t*) const;

private:

	// Histograms for unidentified particles.
	TH1D*							fPtSpectrumUITotal;
	TH1D*							fFractionUITOFHit;
	TH1D*							fFractionUISelForPID;
	TH1D*							fFractionUITPCClusterCut;

    TH2D*                           fPtEtaSpectrumUITotal;
    TH2D*                           fPtEtaFractionUITOFHit;
    TH2D*                           fPtEtaFractionUISelForPID;
    TH2D*                           fPtEtaFractionUITPCClusterCut;

	// Histograms for identified particles (tight TOF/TPC cut, or MC info).
	TH1D*							fPtSpectrumPIDTotal[3];
	TH1D*							fFractionPIDTOFHit[3];
	TH1D*							fFractionPIDTPCClusterCut[3];


	ClassDef(AliQATaskDiHadronPID, 1);

};

#endif
