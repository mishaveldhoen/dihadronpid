/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Misc. Tools for the DiHadronPID analysis.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "AliLogDiHadronPID.h"
#include "AliMiscToolsDiHadronPID.h"
#include "AliFunctionsDiHadronPID.h"
#include "TGraphAsymmErrors.h"
#include "TClass.h"
#include "TSystem.h"
#include "TAxis.h"
#include "TKey.h"
#include "TList.h"
#include "TH1.h"

// -----------------------------------------------------------------------
Bool_t AliMiscToolsDiHadronPID::CreateDir(TString dirName) {

	/**
	 * Similar to TSystem::mkdir(), however, mkdir will return -1 both
	 * if it failed to create the directory, but also if the directory already
	 * exists. This method will return 1 if the directory is usable (created or already existing).
	 */

	// Make directory.
	gSystem->mkdir(dirName.Data(), kTRUE);

	// Check if the desired directory exists.
	return (!gSystem->AccessPathName(dirName.Data()));

}

// -----------------------------------------------------------------------
TFile* AliMiscToolsDiHadronPID::OpenROOTFile(TString fileName, const char* option) {

	/**
	 * This method creates a ROOT file, an returns a pointer to the file,
	 * similar to TFile::Open(). It will however also recursively create subdirectories
	 * if they do not yet exist.
	 */

	// Check if there's a directory structure in fileName.
	if (fileName.Last('/') > -1) {
	
		// Check if the directory exists, and otherwise create it.
		TString dirName(fileName);
		dirName.Remove(fileName.Last('/'));
		Bool_t dirIsOK = CreateDir(dirName);
		if (!dirIsOK) {
			AliLogDiHadronPID::Error(Form("Could not create directroy %s", dirName.Data()), __func__, "AliMiscToolsDiHadronPID");
			return 0x0;
		}		
	}

	// Create the actual file.
	TFile* fOut = TFile::Open(fileName.Data(), option);

	return fOut;

}

// -----------------------------------------------------------------------
void AliMiscToolsDiHadronPID::PDFfyROOTFile(TString fileName) {

	/**
	 * This method opens a ROOT file, searches for plottable items.
	 * any that are found are stored as a pdf in a directory filename/histName.pdf
	 */

	TFile* fileIn = TFile::Open(fileName.Data());
	if (!fileIn) {
		AliLogDiHadronPID::Error(Form("Could not open a file named %s", fileName.Data()), __func__, "AliMiscToolsDiHadronPID");
	 	return;
	}

	// Create a directory if it does not yet exist.
	TString dirName = fileName;
	if (dirName.First(".") > 0) {dirName.Remove(dirName.First("."));}
	if (!gSystem->OpenDirectory(dirName.Data())) {
		if (gSystem->MakeDirectory(dirName.Data())) {
			AliLogDiHadronPID::Error(Form("Could not create a directory named %s", dirName.Data()), __func__, "AliMiscToolsDiHadronPID");
			fileIn->Close();
		 	return;
		}
	}

	// Create a canvas.
	TCanvas* cvs = TCanvas::MakeDefCanvas();

	// Iterate over the list of keys (keys are contain meta data about the object I think).
	TIter next(fileIn->GetListOfKeys());
	while (TKey *key = (TKey*)next()) {
		if (TString(key->GetClassName()).Contains("TH1")) {
			TH1* hist = dynamic_cast<TH1*>(key->ReadObj());
			hist->Draw();
			cvs->SaveAs(Form("%s/%s.pdf", dirName.Data(), hist->GetName()));
			cvs->Clear();
		}
	}

	fileIn->Close();
	delete cvs;

}

// -----------------------------------------------------------------------
TString AliMiscToolsDiHadronPID::FormatTime(Double_t timeInSec) {

	/**
	 * Given a time in seconds, this method returns a \c TString in the format hh:mm:ss.
	 *
	 * \param timeInSec Time in seconds.
	 */

	Int_t intsec = (Int_t)timeInSec;
	Int_t inthrs = intsec/3600;
	intsec -= 3600 * inthrs;
	Int_t intmin = intsec/60;
	intsec -= 60 * intmin;
	return (TString(Form("%ih:%02im:%02is",inthrs,intmin,intsec)));

}

// -----------------------------------------------------------------------
TString AliMiscToolsDiHadronPID::AppendToFileName(TString fileName, TString appendix) {

	/**
	 * This method takes a filename (file.extension), and adds to the file-part
	 * another string (appendix).
	 */

	TString fileNameOut(fileName);
	if (fileNameOut.Last('.') == -1) {fileNameOut.Append(appendix);}
	else {fileNameOut.Insert(fileNameOut.Last('.'), appendix);} 

	return fileNameOut;

}

// -----------------------------------------------------------------------
TString AliMiscToolsDiHadronPID::GetFlowFormula(Int_t VnMax, Int_t firstParIndex) {

	/**
	 * Return a TString \f$\left[firstParIndex\right]\times\left(1+2\sum_{n=VnMin}^{VnMax}\left[firstParIndex+n\right]\times\cos(n\Delta\phi)\right)\f$.
	 */

	// Some sanity checks.
	TString sOut("");
	if (VnMax < 0 || firstParIndex < 0) {
		AliLogDiHadronPID::Error("Invalid parameters are supplied, please check.", __func__, "AliMiscToolsDiHadronPID");
	} else if (VnMax == 0) {
		sOut.Append("1");
	} else {
		sOut.Append(Form("[%d]*(1", firstParIndex));
		for (Int_t iFlowCoefficient = 1; iFlowCoefficient <= VnMax; iFlowCoefficient++) {
			sOut.Append(Form("+2*[%d]*cos(%d*x)", firstParIndex + iFlowCoefficient, iFlowCoefficient));
		}
		sOut.Append(")");
	}

	return sOut;

}

// -----------------------------------------------------------------------
Int_t AliMiscToolsDiHadronPID::FindClosestPoint(const TGraph* graph, Double_t xx) {

	/**
	 * This method returns the index of the point that is closest to the desired value xx.
	 * It is safe in the sense that it will never return an invalid (out of bounds) index.
	 */

	// Start by finding the first point next to xx.
	Int_t index = 0;
	while (index < graph->GetN() && graph->GetX()[index] < xx) {index++;}
	if (index == graph->GetN() || index == 0) {
		AliLogDiHadronPID::Warning(Form("It seems like the requested value of x is not within the range."), __func__, "AliMiscToolsDiHadronPID");
		if (index) {index--;} // If the index is equal to the number of points subtract one, to prevent out-of-bounds.
		return index;
	}

	Double_t dist1 = TMath::Abs(xx - graph->GetX()[index]);
	Double_t dist2 = TMath::Abs(xx - graph->GetX()[index - 1]);

	if (dist1 < dist2) {
		return index;
	} else {
		return (index - 1);
	}

}

// -----------------------------------------------------------------------
void AliMiscToolsDiHadronPID::ScaleGraphPoint(TGraph* graph, Int_t iPoint, Double_t cc) {

	/**
	 * This method scales a point (and associated errors) with a factor cc in the y-direction.
	 */
	if (iPoint < 0 || iPoint > graph->GetN()) {return;}

	// Set for all TGraphs
	Double_t* xx = graph->GetX();
	Double_t* yy = graph->GetY();
	
	// Nonzero ptr only for TGraphErrors
	Double_t* ex = graph->GetEX();
	Double_t* ey = graph->GetEY();

	// Nonzero ptr only for TGraphAsymmErrors 
	Double_t* exlow = graph->GetEXlow();
	Double_t* eylow = graph->GetEYlow();
	Double_t* exhigh = graph->GetEXhigh();
	Double_t* eyhigh = graph->GetEYhigh();

	// Loop over points and scale points and errors.
	graph->SetPoint(iPoint, xx[iPoint], yy[iPoint] * cc);
	if (ex) {((TGraphErrors*)graph)->SetPointError(iPoint, ex[iPoint], ey[iPoint] * cc);}
	if (exlow) {((TGraphAsymmErrors*)graph)->SetPointError(iPoint, exlow[iPoint], exhigh[iPoint], eylow[iPoint] * cc, eyhigh[iPoint] * cc);}

}

// -----------------------------------------------------------------------
void AliMiscToolsDiHadronPID::ScaleGraph(TGraph* graph, Double_t cc) {

	/**
	 * Apply AliMiscToolsDiHadronPID::ScaleGraphPoint to all points in the graph.
	 */

	// Set for all TGraphs
	Double_t* xx = graph->GetX();
	Double_t* yy = graph->GetY();
	
	// Nonzero ptr only for TGraphErrors
	Double_t* ex = graph->GetEX();
	Double_t* ey = graph->GetEY();

	// Nonzero ptr only for TGraphAsymmErrors 
	Double_t* exlow = graph->GetEXlow();
	Double_t* eylow = graph->GetEYlow();
	Double_t* exhigh = graph->GetEXhigh();
	Double_t* eyhigh = graph->GetEYhigh();

	// Loop over points and scale points and errors.
	for (Int_t iPoint = 0; iPoint < graph->GetN(); iPoint++) {
			
		graph->SetPoint(iPoint, xx[iPoint], yy[iPoint] * cc);
		if (ex) {((TGraphErrors*)graph)->SetPointError(iPoint, ex[iPoint], ey[iPoint] * cc);}
		if (exlow) {((TGraphAsymmErrors*)graph)->SetPointError(iPoint, exlow[iPoint], exhigh[iPoint], eylow[iPoint] * cc, eyhigh[iPoint] * cc);}
	}

}

// -----------------------------------------------------------------------
void AliMiscToolsDiHadronPID::ScaleGraphPt(TGraph* graph) {
	/**
	 * Scale every point in graph by its x-value; 
         *      used to convert dN/pt/dpt to dN/dpt
	 */
	// Loop over points and scale points and errors.
	for (Int_t iPoint = 0; iPoint < graph->GetN(); iPoint++) {
	  Double_t pt=0, dnptdpt=0;
	  graph->GetPoint(iPoint,pt,dnptdpt);
	  ScaleGraphPoint(graph,iPoint,pt);
	}
}

// -----------------------------------------------------------------------
void AliMiscToolsDiHadronPID::DivideGraph(TGraph* graph, const TF1* func) {

	/** 
     * This method scales the points and error band of the graph with a function.
     */

	// Set for all TGraphs
	Double_t* xx = graph->GetX();
	Double_t* yy = graph->GetY();
	
	// Nonzero ptr only for TGraphErrors
	Double_t* ex = graph->GetEX();
	Double_t* ey = graph->GetEY();

	// Nonzero ptr only for TGraphAsymmErrors 
	Double_t* exlow = graph->GetEXlow();
	Double_t* eylow = graph->GetEYlow();
	Double_t* exhigh = graph->GetEXhigh();
	Double_t* eyhigh = graph->GetEYhigh();

	// Loop over points and scale points and errors.
	for (Int_t iPoint = 0; iPoint < graph->GetN(); iPoint++) {
		
		Double_t cc = 1. / func->Eval(xx[iPoint]);
		graph->SetPoint(iPoint, xx[iPoint], yy[iPoint] * cc);
		if (ex) {((TGraphErrors*)graph)->SetPointError(iPoint, ex[iPoint], ey[iPoint] * cc);}
		if (exlow) {((TGraphAsymmErrors*)graph)->SetPointError(iPoint, exlow[iPoint], exhigh[iPoint], eylow[iPoint] * cc, eyhigh[iPoint] * cc);}
	}

}

// -----------------------------------------------------------------------
void AliMiscToolsDiHadronPID::AddGraphs(TGraph* graph1, const TGraph* graph2) {

	/** 
	 * Given two instances inheriting from TGraph, the second graph is added
	 * to the first. Note that this method does not explicitly check the compatibility
	 * of the X-axes of both graphs, only if the number of points match. Errors in the 
	 * y-direction are added in quadrature.
	 */

	if ((!graph1) || (!graph2)) {return;}

	if (graph1->GetN() != graph2->GetN()) {
		AliLogDiHadronPID::Warning(Form("Graphs do not have the same number of points, hence they cannot be added."), __func__, "AliMiscToolsDiHadronPID");
		return;
	}

	Double_t* upError = graph2->GetEYhigh();
	Double_t* downError = graph2->GetEYlow();	
	if (!upError || !downError) {upError = downError = graph2->GetEY();}

	for (Int_t iPoint = 0; iPoint < graph1->GetN(); iPoint++) {
		graph1->SetPoint(iPoint, graph1->GetX()[iPoint], graph1->GetY()[iPoint] + graph2->GetY()[iPoint]);

		if (TString(graph1->Class()->GetName()).EqualTo("TGraphErrors") && upError) {
			((TGraphErrors*)graph1)->SetPointError(iPoint, graph1->GetErrorX(iPoint), 
				TMath::Sqrt(TMath::Power(graph1->GetErrorY(iPoint), 2) + TMath::Power(upError[iPoint], 2)));
		}

		if (TString(graph1->Class()->GetName()).EqualTo("TGraphAsymmErrors") && upError) {
			((TGraphAsymmErrors*)graph1)->SetPointError(iPoint, graph1->GetErrorXlow(iPoint), graph1->GetErrorXhigh(iPoint),
				TMath::Sqrt(TMath::Power(graph1->GetErrorYlow(iPoint), 2) + TMath::Power(downError[iPoint], 2)),
				TMath::Sqrt(TMath::Power(graph1->GetErrorYhigh(iPoint), 2) + TMath::Power(upError[iPoint], 2)));
		}

	}	

}

// -----------------------------------------------------------------------
TGraph* AliMiscToolsDiHadronPID::GetErrorBandGraph(const TGraphErrors* graph, Bool_t up) {

	/**
	 * Given an instance of TGraphErrors, this method returns a TGraph following the upper (or lower)
	 * edge of the error band.
	 */

	// Create a copy of the original graph.
	TGraph* gOut = new TGraph(graph->GetN());

	// Pointers to the up and down error arrays. Compatible with both TGraphErrors and TGraphAssymErrors.
	Double_t* upError = graph->GetEYhigh();
	Double_t* downError = graph->GetEYlow();
	if (!upError || !downError) {upError = downError = graph->GetEY();}

	// Move all points up (or down) by the magnitude of the error on that point.
	for (Int_t iPoint = 0; iPoint < graph->GetN(); iPoint++) {
		gOut->SetPoint(iPoint, graph->GetX()[iPoint], graph->GetY()[iPoint] + (up ? upError[iPoint] : -downError[iPoint]));
	}

	return gOut;

}

// -----------------------------------------------------------------------
TGraphErrors* AliMiscToolsDiHadronPID::GetNSigmaBand(TF1* func, Double_t nSigma, Int_t parIndex) {

	/** 
	 * This method creates a TGraphErrors, with an error band representing the statistical uncertainty
	 * on the curve. 
	 *
	 * \param func Ptr to the function (assumed to have valid uncertainties on its parameters).
	 * \param nSigma Thickness of the error band. 
	 * \param parIndex If this parameter < 0, then the combined error of all parameters is calculated. If its > 0,
	 * then the calculated band is only due to one parameter.
	 *
 	 * \note Could not make the TF1* argument const, as the method TF1::GradientPar does not allow it.
	 */
	 
	// Check if the parameter of parIndex is OK.
	if (parIndex > func->GetNpar()) {
		AliLogDiHadronPID::Error(Form("Parameter %i does not exist for function %s.", parIndex, func->GetName()), __func__, "AliMiscToolsDiHadronPID");
		return 0x0;
	}

	// Determine the domain.
	Double_t xMin = func->GetXaxis()->GetXmin();
	Double_t xMax = func->GetXaxis()->GetXmax();
	Double_t delta = (xMax - xMin) / func->GetNpx();

	// Create arrays for the xx, yy, ex and ey.
	Double_t xx[func->GetNpx()];
	Double_t yy[func->GetNpx()];
	Double_t ex[func->GetNpx()];
	Double_t ey[func->GetNpx()];

	// Loop over all points.
	for (Int_t iPoint = 0; iPoint < func->GetNpx(); iPoint++) {
			
		// Current x coordinate and uncertainty.
		xx[iPoint] = (0.5 + iPoint) * delta + xMin;
		ex[iPoint] = delta / 2.;
		
		// Current y coordinate and uncertainty.
		yy[iPoint] = func->Eval(xx[iPoint]);
		ey[iPoint] = 0.;
		
		if (parIndex < 0) {

			Double_t ey2 = 0.;
			for (Int_t iPar = 0; iPar < func->GetNpar(); iPar++) {
				
				Double_t parErrorTerm = func->GradientPar(iPar, &xx[iPoint], 0.0001) * func->GetParError(iPar) * nSigma;
				AliLogDiHadronPID::Info(Form("Contribution of parameter %i (%s): %f", iPar, func->GetParName(iPar), parErrorTerm), __func__, "AliMiscToolsDiHadronPID");
				// cout << Form("Contribution of parameter %i (%s): %f", iPar, func->GetParName(iPar), parErrorTerm) << endl;
				ey2 += (parErrorTerm * parErrorTerm);

			}

			ey[iPoint] = TMath::Sqrt(ey2);

		} else {
			ey[iPoint] = func->GradientPar(parIndex, &xx[iPoint], 0.0001) * func->GetParError(parIndex) * nSigma;
			AliLogDiHadronPID::Info(Form("Contribution of parameter %i (%s): %f", parIndex, func->GetParName(parIndex), ey[iPoint]), __func__, "AliMiscToolsDiHadronPID");
			// cout << Form("Contribution of parameter %i (%s): %f", parIndex, func->GetParName(parIndex), ey[iPoint]) << endl;
		}

	}

	// Create a graph with the same resolution as the function.
	TGraphErrors* graph = new TGraphErrors(func->GetNpx(), xx, yy, ex, ey);
	graph->SetFillColor(func->GetLineColor());
	graph->SetFillStyle(1001);

	return graph;

}

// -----------------------------------------------------------------------
Double_t AliMiscToolsDiHadronPID::SplineIntegral(const TSpline* spline, Double_t xMin, Double_t xMax, Int_t nPoints) {

	/**
	 * Take the integral of a class derived from TSpline, in the range (xMin, xMax), using nPoints.
	 * Basically one takes a Riemann sum.
	 */

	Double_t integral = 0.;
	Double_t dX = (xMax - xMin) / nPoints;

	for (Int_t iPoint = 0; iPoint < nPoints; iPoint++) {
		// cout << xMin + dX / 2. + dX * iPoint << endl;
		integral += (spline->Eval(xMin + dX / 2. + dX * iPoint) * dX);
	
	}

	// cout << endl;
	return integral;

}

// -----------------------------------------------------------------------
void AliMiscToolsDiHadronPID::ApplyGeantFlukaCorrection(TH1F* hist, const Int_t species, const Bool_t isPos, const Bool_t isTrackingEff) {

	/**
	 * GEANT3 overestimates the cross sections of negative particles with material, and the produced
	 * efficiencies are therefore too low. By dividing the efficiencies through these numbers, 
	 * the problem is corrected. Note that it should not be applied on newer data sets, as the 
	 * problem with GEANT3 has been resolved at some point.
	 */

	// If not tracking efficiency, then it's matching efficiency.
	if (isPos) {return;}			// No effect on positive particles.
	if (species == 0) {return;}		// No effect on pions.

	// Loop over all p_T bins
	for (Int_t iPtBin = 1; iPtBin <= hist->GetNbinsX(); ++iPtBin) {

		Double_t pTmc = hist->GetBinCenter(iPtBin);

		if (species == 1) {

			// Kaons.
			Float_t ptTPCoutK = pTmc * (1- 3.37297e-03/pTmc/pTmc - 3.26544e-03/pTmc);
			if (isTrackingEff) {
				// cout << __LINE__ << endl;
				Float_t kminusEC = TMath::Min(0.972865 + 0.0117093*pTmc, 1.);
				hist->SetBinContent(iPtBin, hist->GetBinContent(iPtBin)/kminusEC);				
			} else {
				// cout << __LINE__ << endl;
				Float_t kminusEC2 = TMath::Power(0.972865 + 0.0117093*ptTPCoutK,0.07162/0.03471);
				hist->SetBinContent(iPtBin, hist->GetBinContent(iPtBin)/kminusEC2);				
			}

		} else {

			// Protons.
			Float_t ptTPCout=pTmc*(1-6.81059e-01*TMath::Exp(-pTmc*4.20094)); 

			if (isTrackingEff) {
				// cout << __LINE__ << endl;
				Float_t antiprotonEC = 1 - 0.129758 *TMath::Exp(-pTmc*0.679612);
				// cout << "before: "<<hist->GetBinContent(iPtBin)<<" factor: "<< antiprotonEC << " after: ";
				hist->SetBinContent(iPtBin, hist->GetBinContent(iPtBin)/antiprotonEC);
				// cout<<hist->GetBinContent(iPtBin)<<endl;
			} else {
				// cout << __LINE__ << endl;
				Float_t antiprotonEC2 = TMath::Power(1 - 0.129758*TMath::Exp(-ptTPCout*0.679612),0.07162/0.03471);
				hist->SetBinContent(iPtBin, hist->GetBinContent(iPtBin)/antiprotonEC2);
			}
		}
	}
}

// -----------------------------------------------------------------------
void AliMiscToolsDiHadronPID::ApplyGeantFlukaCorrection(TH2F* hist, const Int_t species, const Bool_t isPos, const Bool_t isTrackingEff) {

	/**
	 * GEANT3 overestimates the cross sections of negative particles with material, and the produced
	 * efficiencies are therefore too low. By dividing the efficiencies through these numbers, 
	 * the problem is corrected. Note that it should not be applied on newer data sets, as the 
	 * problem with GEANT3 has been resolved at some point.
	 */

	// If not tracking efficiency, then it's matching efficiency.
	if (isPos) {return;}			// No effect on positive particles.
	if (species == 0) {return;}		// No effect on pions.

	// Loop over all p_T bins and eta bins.
	for (Int_t iEtaBin = 1; iEtaBin <= hist->GetNbinsY(); ++iEtaBin) {
		for (Int_t iPtBin = 1; iPtBin <= hist->GetNbinsX(); ++iPtBin) {

			Double_t pTmc = (hist->GetXaxis())->GetBinCenter(iPtBin);

			if (species == 1) {

				// Kaons.
				Float_t ptTPCoutK = pTmc * (1- 3.37297e-03/pTmc/pTmc - 3.26544e-03/pTmc);
				if (isTrackingEff) {
					Float_t kminusEC = TMath::Min(0.972865 + 0.0117093*pTmc, 1.);
					hist->SetBinContent(iPtBin, iEtaBin, hist->GetBinContent(iPtBin,iEtaBin)/kminusEC);				
				} else {
					Float_t kminusEC2 = TMath::Power(0.972865 + 0.0117093*ptTPCoutK,0.07162/0.03471);
					hist->SetBinContent(iPtBin, iEtaBin, hist->GetBinContent(iPtBin,iEtaBin)/kminusEC2);				
				}

			} else {

				// Protons.
				Float_t ptTPCout=pTmc*(1-6.81059e-01*TMath::Exp(-pTmc*4.20094)); 

				if (isTrackingEff) {
					Float_t antiprotonEC = 1 - 0.129758 *TMath::Exp(-pTmc*0.679612);
					//cout << "before: "<<hist->GetBinContent(iPtBin)<<" factor: "<< antiprotonEC << " after: ";
					hist->SetBinContent(iPtBin, iEtaBin, hist->GetBinContent(iPtBin, iEtaBin)/antiprotonEC);
					//cout<<hist->GetBinContent(iPtBin)<<endl;
				} else {
					Float_t antiprotonEC2 = TMath::Power(1 - 0.129758*TMath::Exp(-ptTPCout*0.679612),0.07162/0.03471);
					hist->SetBinContent(iPtBin, iEtaBin, hist->GetBinContent(iPtBin, iEtaBin)/antiprotonEC2);
				}
			}
		}
	}
}

// -----------------------------------------------------------------------
void AliMiscToolsDiHadronPID::TestGeantFlukaCorrection() {

	/**
	 * This method will test the effect of the implemented GEANT3/FLUKA correction
	 * methods (AliMiscToolsDiHadronPID::ApplyGeantFlukaCorrection) by applying them on trivial histograms.
	 */

	// Create trivial histograms.
	const Int_t nBins = 100;
	const char* species[3] = {"Pion", "Kaon", "Proton"};
	const char* charge[2] = {"Neg", "Pos"};
	const char* type[2] = {"Matching", "Tracking"};
	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		for (Int_t iCharge = 0; iCharge <= 1; iCharge++) {
			for (Int_t iEffType = 0; iEffType <= 1; iEffType++) {
				
				if (iSpecies == 0) {continue;}
				if (iCharge == 1) {continue;}

				TH1F* hist = new TH1F(Form("%s%s%sEff", species[iSpecies], charge[iCharge], type[iEffType]), 
					Form("%s %s %s Efficiency;p_{T}", species[iSpecies], charge[iCharge], type[iEffType]),
					nBins,0,5);
				for (Int_t iBin = 1; iBin <= nBins; iBin++) {hist->SetBinContent(iBin, 1.);}

				ApplyGeantFlukaCorrection(hist, iSpecies, iCharge, iEffType);

				/*TCanvas* cvs = */TCanvas::MakeDefCanvas();
				hist->Draw();
						
			}			
		}	
	}

}

// -----------------------------------------------------------------------
void AliMiscToolsDiHadronPID::MakePtEtaMask(TH2F* hist, const Int_t iSpecies, const Int_t nMCHits) {

	/**
	 * This method operates on a histogram in \f$(p_T,\eta)\f$. It will set all bins with a rapidity 
	 * \f$y>0.5\f$ to zero, and bins on the boundary \f$y=0.5\f$ to the appropriate fraction.
	 *
	 * \todo The rapidity value of 0.5 should not be hardcoded.
	 *
	 * \param hist Original histogram, that is operated on.
	 * \param iSpecies Particle species.
	 * \param nMCHits The fraction in the bins on the \f$y=0.5\f$ boundary is calculated numerically.
	 * nMCHits represents the resolution with which this is done. (technically it's of course not MC!)
	 */

	// Loop over bins.
	for (Int_t iPtBin = 1; iPtBin <= (hist->GetXaxis())->GetNbins(); iPtBin++) {
		for (Int_t iEtaBin = 1; iEtaBin <= (hist->GetYaxis())->GetNbins()/2; iEtaBin++) {

			// Check if bin contains limiting |y| cut.
			Double_t higheta = AliFunctionsDiHadronPID::Eta(iSpecies, (hist->GetXaxis())->GetBinLowEdge(iPtBin), 0.5);
			Double_t loweta = AliFunctionsDiHadronPID::Eta(iSpecies, (hist->GetXaxis())->GetBinUpEdge(iPtBin), 0.5);

			// Contained fully.
			if (TMath::Abs((hist->GetYaxis())->GetBinLowEdge(iEtaBin)) < TMath::Abs(loweta)) {
				hist->SetBinContent(iPtBin, iEtaBin, 1.0);
				hist->SetBinContent(iPtBin, (hist->GetYaxis())->GetNbins() + 1 - iEtaBin, 1.0);
			} 

			// Not contained.
			else if (TMath::Abs((hist->GetYaxis())->GetBinUpEdge(iEtaBin)) > TMath::Abs(higheta)) {
				hist->SetBinContent(iPtBin, iEtaBin, 0.0);
				hist->SetBinContent(iPtBin, (hist->GetYaxis())->GetNbins() + 1 - iEtaBin, 0.0);
			}

			else {

				// Partially contained, fill bin with fraction. 
				Double_t sum = 0.;
				
				for (Double_t iPt = (hist->GetXaxis())->GetBinLowEdge(iPtBin); iPt < (hist->GetXaxis())->GetBinUpEdge(iPtBin); iPt += ((hist->GetXaxis())->GetBinUpEdge(iPtBin) - (hist->GetXaxis())->GetBinLowEdge(iPtBin)) / nMCHits ) {
					for (Double_t iEta = (hist->GetYaxis())->GetBinLowEdge(iEtaBin); iEta < (hist->GetYaxis())->GetBinUpEdge(iEtaBin); iEta += ((hist->GetYaxis())->GetBinUpEdge(iEtaBin) - (hist->GetYaxis())->GetBinLowEdge(iEtaBin)) / nMCHits ) {

						if (TMath::Abs(iEta) < TMath::Abs(AliFunctionsDiHadronPID::Eta(iSpecies, iPt, 0.5))) {
							sum++;
						}
					}
				}

				hist->SetBinContent(iPtBin, iEtaBin, sum / (nMCHits * nMCHits));
				hist->SetBinContent(iPtBin, (hist->GetYaxis())->GetNbins() + 1 - iEtaBin, sum / (nMCHits * nMCHits));

			}
		}
	}	
}
