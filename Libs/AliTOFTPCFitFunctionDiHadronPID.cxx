/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  This class is basically an array of an arbitrary number of instances
//  of AliTOFTPCPeakDiHadronPID. Additionally, it contains the mismatch
//  fit parameters. The class is responsible for evaluating the total fit
//  function, i.e., peaks plus mismatch. It mainly also provides a mapping 
//  between the parameters that the minimizer takes (double*), and the 
//  parameters from all the contained peak objects.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include <iostream>
#include "AliTOFTPCPeakDiHadronPID.h"
#include "AliFunctionsDiHadronPID.h"
#include "AliLogDiHadronPID.h"

#include "AliTOFTPCFitFunctionDiHadronPID.h"

using namespace std;

ClassImp(AliTOFTPCFitFunctionDiHadronPID);

// -----------------------------------------------------------------------
AliTOFTPCFitFunctionDiHadronPID::AliTOFTPCFitFunctionDiHadronPID():
	TObject(),
	fSettings(AliSettingsDiHadronPID::Instance()),
	fMassAssumption(AliSettingsDiHadronPID::kUnknownSpecies),
	fMeanPt(0.),
	fAbsMeanEta(0.),
	fTOFbinWidth(1.),
	fTPCbinWidth(1.),
	fPeaks(new TObjArray(AliSettingsDiHadronPID::GetNSpecies())),
	fMismatchYield(-1.),
	fMismatchYieldError(-1.),
	fMismatchYieldStep(-1),
	fMismatchYieldIsFixed(kTRUE),
	fFitRegionTOFMin(0.),
	fFitRegionTOFMax(0.),
	fFitRegionTPCMin(0.),
	fFitRegionTPCMax(0.),	
	fFitStatus(-1)

{

	// Default constructor.
	AliLogDiHadronPID::Debug2("Default constructor called.", __func__, ClassName());

	// Error message when settings singleton is not found!
	if (!fSettings) {AliLogDiHadronPID::Error(Form("Settings Singleton not found!"), __func__, ClassName());}

	// Let this class own the peaks.
	fPeaks->SetOwner(kTRUE);

	// Call the default constructor of AliTOFTPCPeakDiHadronPID.
	for (Int_t iPeak = 0; iPeak < fPeaks->GetSize(); iPeak++) {
		AliTOFTPCPeakDiHadronPID* peak = new AliTOFTPCPeakDiHadronPID();
		fPeaks->AddLast(peak);
	}

}

// -----------------------------------------------------------------------
AliTOFTPCFitFunctionDiHadronPID::AliTOFTPCFitFunctionDiHadronPID(AliSettingsDiHadronPID::Species massAssumption, Double_t ptmin, Double_t ptmax, Double_t etamin, Double_t etamax, const TH2F* data, const TH2F* mismatch):
	TObject(),
	fSettings(AliSettingsDiHadronPID::Instance()),	
	fMassAssumption(massAssumption),
	fMeanPt((ptmax + ptmin)/2.),
	fAbsMeanEta(AliFunctionsDiHadronPID::GetAbsAvgEta(etamin, etamax)),
	fTOFbinWidth(data ? (data->GetXaxis())->GetBinWidth(1) : 1.),
	fTPCbinWidth(data ? (data->GetYaxis())->GetBinWidth(1) : 1.),
	fPeaks(new TObjArray(AliSettingsDiHadronPID::GetNSpecies())),
	fMismatchYield(-1.),
	fMismatchYieldError(-1.),
	fMismatchYieldStep(-1.),
	fMismatchYieldIsFixed(kTRUE),
	fFitRegionTOFMin(0.),
	fFitRegionTOFMax(0.),
	fFitRegionTPCMin(0.),
	fFitRegionTPCMax(0.),		
	fFitStatus(-1)
{

	// Constructor with kinematic info, and the possibility to supply pointers to the data and mismatch histograms.
	AliLogDiHadronPID::Debug2(Form("Constructor called, mass assumption: %s, %3.1f < pT < %3.1f, %3.1f < eta < %3.1f.", fSettings->GetSpeciesName(fMassAssumption), ptmin, ptmax, etamin, etamax), __func__, ClassName());

	// Error message when settings singleton is not found!
	if (!fSettings) {AliLogDiHadronPID::Error(Form("Settings Singleton not found!"), __func__, ClassName());}

	// Let this class own the peaks.
	fPeaks->SetOwner(kTRUE);

	// Call the constructors of the six peaks. Mu, Sigma and Tail are automatically initialized by the peaks. Also, 
	// wether the peak is included is taken care of by the peak itself.
	for (Int_t iPeak = 0; iPeak < fPeaks->GetSize(); iPeak++) {
		AliTOFTPCPeakDiHadronPID* peak = new AliTOFTPCPeakDiHadronPID(fMassAssumption, (AliSettingsDiHadronPID::Species)iPeak, fMeanPt, fAbsMeanEta, fTOFbinWidth, fTPCbinWidth);
		fPeaks->AddLast(peak);
	}

	// The last parameters to estimate are the particle yields, and the mismatch yield, using the data and mismatch histograms.
	if (data) {
		EstimateParticleYields(data);
		if (mismatch) {
			EstimateMismatchYield(data, mismatch);
		} else {
			AliLogDiHadronPID::Warning(Form("No Mismatch histogram supplies, cannot estimate mismatch yield."), __func__, ClassName());
		}
	} else {
		AliLogDiHadronPID::Warning(Form("No Data histogram suplied, cannot estimate particle yields and mismatch yield."), __func__, ClassName());
	}

	// Calculate the fit region based on the current settings. Note that the fit region will need to be updated
	// by the fitter, since the fitter may decide to exclude certain peaks.
	UpdateFitRegion();

}

// -----------------------------------------------------------------------
AliTOFTPCFitFunctionDiHadronPID::AliTOFTPCFitFunctionDiHadronPID(const AliTOFTPCFitFunctionDiHadronPID& other):
	TObject(other),
	fSettings(AliSettingsDiHadronPID::Instance()),
	fMassAssumption(other.fMassAssumption),
	fMeanPt(other.fMeanPt),
	fAbsMeanEta(other.fAbsMeanEta),
	fTOFbinWidth(other.fTOFbinWidth),
	fTPCbinWidth(other.fTPCbinWidth),	
	fPeaks(new TObjArray(AliSettingsDiHadronPID::GetNSpecies())),
	fMismatchYield(other.fMismatchYield),
	fMismatchYieldError(other.fMismatchYieldError),
	fMismatchYieldStep(other.fMismatchYieldStep),
	fMismatchYieldIsFixed(other.fMismatchYieldIsFixed),
	fFitRegionTOFMin(other.fFitRegionTOFMin),
	fFitRegionTOFMax(other.fFitRegionTOFMax),
	fFitRegionTPCMin(other.fFitRegionTPCMin),
	fFitRegionTPCMax(other.fFitRegionTPCMax),	
	fFitStatus(-1)

{

	AliLogDiHadronPID::Debug2(Form("Copy constructor called."), __func__, ClassName());

	// Error message when settings singleton is not found!
	if (!fSettings) {AliLogDiHadronPID::Error(Form("Settings Singleton not found!"), __func__, ClassName());}

	// Let this class own the peaks.
	fPeaks->SetOwner(kTRUE);

	// Copy the contents of fPeaks.
	for (Int_t iPeak = 0; iPeak < fPeaks->GetSize(); iPeak++) {
		
		// Obtain the peak from other.
		AliTOFTPCPeakDiHadronPID* otherPeak = (AliTOFTPCPeakDiHadronPID*)(other.fPeaks->At(iPeak));
		if (!otherPeak) {
			AliLogDiHadronPID::Error(Form("Could not obtain one of the peaks from the other function."), __func__, ClassName());
			continue;
		}
		
		// Construct a new peak for self.
		AliTOFTPCPeakDiHadronPID* peak = new AliTOFTPCPeakDiHadronPID(*otherPeak);
		fPeaks->AddLast(peak);

	}
	
}

// -----------------------------------------------------------------------
AliTOFTPCFitFunctionDiHadronPID& AliTOFTPCFitFunctionDiHadronPID::operator=(const AliTOFTPCFitFunctionDiHadronPID& other) {

	AliLogDiHadronPID::Debug2(Form("Called assignment operator."), __func__, ClassName());
	
	// Protect agains self-assignment.
	if (this != &other) {

		// Assume fSettings is already set properly upon creation of this.
		fMassAssumption = other.fMassAssumption;
		fMeanPt = other.fMeanPt;
		fAbsMeanEta = other.fAbsMeanEta;
		fTOFbinWidth = other.fTOFbinWidth;
		fTPCbinWidth = other.fTPCbinWidth;

		// Give contents of fPeaks to ROOT's garbage collector, and set all elements to 0x0;
		fPeaks->Delete();

		// Copy the contents of fPeaks.
		for (Int_t iPeak = 0; iPeak < fPeaks->GetSize(); iPeak++) {
			
			// Obtain the peak from other.
			AliTOFTPCPeakDiHadronPID* otherPeak = (AliTOFTPCPeakDiHadronPID*)(other.fPeaks->At(iPeak));
			if (!otherPeak) {
				AliLogDiHadronPID::Error(Form("Could not obtain one of the peaks from the other function."), __func__, ClassName());
				continue;
			}
			
			// Construct a new peak for self.
			AliTOFTPCPeakDiHadronPID* peak = new AliTOFTPCPeakDiHadronPID(*otherPeak);
			fPeaks->AddLast(peak);

		}

		fMismatchYield = other.fMismatchYield;
		fMismatchYieldError = other.fMismatchYieldError;
		fMismatchYieldStep = other.fMismatchYieldStep;
		fMismatchYieldIsFixed = other.fMismatchYieldIsFixed;
		fFitRegionTOFMin = other.fFitRegionTOFMin;
		fFitRegionTOFMax = other.fFitRegionTOFMax;
		fFitRegionTPCMin = other.fFitRegionTPCMin;
		fFitRegionTPCMax = other.fFitRegionTPCMax;
		fFitStatus = other.fFitStatus;

	}

	return *this;

}

// -----------------------------------------------------------------------
AliTOFTPCFitFunctionDiHadronPID::~AliTOFTPCFitFunctionDiHadronPID() {

	AliLogDiHadronPID::Debug2("Destructor called.", __func__, ClassName());

	if (fPeaks) {delete fPeaks; fPeaks = 0x0;}

}

// -----------------------------------------------------------------------
double AliTOFTPCFitFunctionDiHadronPID::operator()(double* xy) {

	//AliLogDiHadronPID::Debug4(Form("Called."), __func__, ClassName());

	double result = 0.;
	for (Int_t iSpecies = 0; iSpecies < fPeaks->GetEntriesFast(); iSpecies++) {

		AliTOFTPCPeakDiHadronPID* currentPeak = (AliTOFTPCPeakDiHadronPID*)fPeaks->At(iSpecies);
		if (currentPeak) {
			if (currentPeak->IsUsedInFit()) {result += (*currentPeak)(xy);}
		} else {
			AliLogDiHadronPID::Error(Form("Fit function is missing a peak..."), __func__, ClassName());
			continue;
		}

	}

	return result;

}
/*
// -----------------------------------------------------------------------
Double_t AliTOFTPCFitFunctionDiHadronPID::EvalSpecies(Double_t TOF, Double_t TPC, Int_t species) {

	// Evaluate the fit function for a certain species.
	//AliLogDiHadronPID::Debug4(Form("Called with (TOF,TPC) = (%f,%f), species = %i", TOF, TPC, species), __func__, ClassName());
	
	Double_t tailposTOF = fSigmaTOF[species] * fSigmaTOF[species] * fTailTOF[species];
	Double_t tailposTPC = fSigmaTPC[species] * fSigmaTPC[species] * fTailTPC[species];

	return (AliFunctionsDiHadronPID::Gaussian2DTailXY(TOF, TPC, fYield[species], 
		fMuTOF[species], fMuTPC[species], fSigmaTOF[species], fSigmaTPC[species], 
		tailposTOF, tailposTPC, fTOFbinWidth, fTPCbinWidth));

}

// -----------------------------------------------------------------------
Double_t AliTOFTPCFitFunctionDiHadronPID::EvalSpeciesIntOne(Double_t TOF, Double_t TPC, Int_t species) {

	// Evaluate the fit function for a certain species, but return a normalized version,
	// i.e., yield = 1;
	
	Double_t tailposTOF = fSigmaTOF[species] * fSigmaTOF[species] * fTailTOF[species];
	Double_t tailposTPC = fSigmaTPC[species] * fSigmaTPC[species] * fTailTPC[species];

	return (AliFunctionsDiHadronPID::Gaussian2DTailXY(TOF, TPC, fYield[species], 
		fMuTOF[species], fMuTPC[species], fSigmaTOF[species], fSigmaTPC[species], 
		tailposTOF, tailposTPC, fTOFbinWidth, fTPCbinWidth));

}
*/
// -----------------------------------------------------------------------
void AliTOFTPCFitFunctionDiHadronPID::EstimateParticleYields(const TH2F* data) {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {

		AliTOFTPCPeakDiHadronPID* currentPeak = (AliTOFTPCPeakDiHadronPID*)fPeaks->At(iSpecies);
		if (!currentPeak) {
			AliLogDiHadronPID::Error(Form("Current peak not found!"), __func__, ClassName());
			continue;
		}

		// For electrons, muons and deuterons the expected yield is so small, that the standard
		// way of estimating will not work well.
		AliSettingsDiHadronPID::Species currentSpecies = currentPeak->GetPeakSpecies();
		if (currentSpecies == AliSettingsDiHadronPID::kElectron ||
			currentSpecies == AliSettingsDiHadronPID::kMuon ||
			currentSpecies == AliSettingsDiHadronPID::kDeuteron) {

			currentPeak->SetParValue(AliTOFTPCPeakDiHadronPID::kYield, 100.);

		} else {

			Int_t peakbinTOF = data->GetXaxis()->FindBin(currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTOF));
			Int_t peakbinTPC = data->GetYaxis()->FindBin(currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTPC));

			Double_t peakheight = data->GetBinContent(peakbinTOF, peakbinTPC);

			Double_t integralestimate = (2 * TMath::Pi() * currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTOF) * currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTPC) * peakheight) / (fTOFbinWidth * fTPCbinWidth);
			
			currentPeak->SetParValue(AliTOFTPCPeakDiHadronPID::kYield, (integralestimate < 10.) ? 100. : integralestimate);
			
		}

	}

}

// -----------------------------------------------------------------------
void AliTOFTPCFitFunctionDiHadronPID::EstimateMismatchYield(const TH2F* data, const TH2F* mismatch) {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	// Get a pointer to the TPC axis.

	Int_t minBinTOF = fSettings->GetMismatchRegionTOFminBin(fMassAssumption, fMeanPt, fAbsMeanEta);
	Int_t maxBinTOF = fSettings->GetMismatchRegionTOFmaxBin(fMassAssumption, fMeanPt, fAbsMeanEta);

	Double_t DataSum = 0, MismatchSum = 0;
	for (Int_t iBinTOF = minBinTOF; iBinTOF <= maxBinTOF; ++iBinTOF) {
		for (Int_t iBinTPC = 1; iBinTPC <= data->GetYaxis()->GetNbins(); ++iBinTPC) {
			DataSum += data->GetBinContent(iBinTOF, iBinTPC);
			MismatchSum += mismatch->GetBinContent(iBinTOF, iBinTPC);
		}
	}

	Double_t Ratio = DataSum / MismatchSum;
	fMismatchYield = (Double_t)mismatch->GetEntries() * Ratio;

}

// -----------------------------------------------------------------------
void AliTOFTPCFitFunctionDiHadronPID::ReInitialiseYields(const TH2F* data, const TH2F* mismatch) {

	// When a better estimate for the peak positions is provided externally 
	// (for example from a previous fit), this method will reinitialse the yields.
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	if (mismatch && data) {
		
		EstimateParticleYields(data);
		EstimateMismatchYield(data, mismatch);

	} else {

		AliLogDiHadronPID::Warning(Form("Eiter data or mismatch histogram not provided."), __func__, ClassName());
		
	}

}

// -----------------------------------------------------------------------
Bool_t AliTOFTPCFitFunctionDiHadronPID::IsParFixedAtIndex(Int_t index) const {

	AliLogDiHadronPID::Debug4(Form("Called."), __func__, ClassName());
	
	if (!IndexExists(index)) {return kTRUE;}

	if (index == 0) {return fMismatchYieldIsFixed;}
	else {
		AliTOFTPCPeakDiHadronPID* currentPeak = (AliTOFTPCPeakDiHadronPID*)fPeaks->At((Int_t)GetSpeciesFromIndex(index));
		if (currentPeak) {
			//currentPeak->Print(0);
			//cout << " Parameter from index: " << GetParameterFromIndex(index) << " fixed: "<<currentPeak->ParameterIsFixed(GetParameterFromIndex(index)) << endl;
			return (currentPeak->ParameterIsFixed(GetParameterFromIndex(index)));
		}
		else {
			AliLogDiHadronPID::Error(Form("Could not find current peak!"), __func__, ClassName());
			return kTRUE;
		}
	}

}

// -----------------------------------------------------------------------
TString AliTOFTPCFitFunctionDiHadronPID::GetParNameAtIndex(Int_t index) {

	AliLogDiHadronPID::Debug4(Form("Called."), __func__, "AliTOFTPCFitFunctionDiHadronPID.");
	
	if (!IndexExists(index)) {return TString();}

	if (index == 0) {return TString("MismatchYield");}
	else {return AliTOFTPCPeakDiHadronPID::GetParNameWithSpecies(GetParameterFromIndex(index), GetSpeciesFromIndex(index));}

}

// -----------------------------------------------------------------------
Double_t AliTOFTPCFitFunctionDiHadronPID::GetParValueAtIndex(Int_t index) const {

	AliLogDiHadronPID::Debug4(Form("Called."), __func__, ClassName());

	if (!IndexExists(index)) {return -999.;}

	if (index == 0) {return fMismatchYield;}
	else {
		AliTOFTPCPeakDiHadronPID* currentPeak = (AliTOFTPCPeakDiHadronPID*)fPeaks->At((Int_t)GetSpeciesFromIndex(index));
		if (currentPeak) {return currentPeak->GetParValue(GetParameterFromIndex(index));}
		else {return -999.;}
	}

}

// -----------------------------------------------------------------------
Double_t AliTOFTPCFitFunctionDiHadronPID::GetParErrorAtIndex(Int_t index) const {

	AliLogDiHadronPID::Debug4(Form("Called."), __func__, ClassName());

	if (!IndexExists(index)) {return -999.;}

	if (index == 0) {return fMismatchYieldError;}
	else {
		AliTOFTPCPeakDiHadronPID* currentPeak = (AliTOFTPCPeakDiHadronPID*)fPeaks->At((Int_t)GetSpeciesFromIndex(index));
		if (currentPeak) {return currentPeak->GetParError(GetParameterFromIndex(index));}
		else {return -999.;}
	}

}

// -----------------------------------------------------------------------
Double_t AliTOFTPCFitFunctionDiHadronPID::GetParStepAtIndex(Int_t index) const {

	AliLogDiHadronPID::Debug4(Form("Called."), __func__, ClassName());

	if (!IndexExists(index)) {return -999.;}

	if (index == 0) {return fMismatchYield;}
	else {
		AliTOFTPCPeakDiHadronPID* currentPeak = (AliTOFTPCPeakDiHadronPID*)fPeaks->At((Int_t)GetSpeciesFromIndex(index));
		if (currentPeak) {return currentPeak->GetParStepSize(GetParameterFromIndex(index));}
		else {return -999.;}
	}

}

// -----------------------------------------------------------------------
void AliTOFTPCFitFunctionDiHadronPID::UpdateFitRegion() {

	// Calculates the fit region based on the included peaks and the settings.
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	fFitRegionTOFMin = fFitRegionTOFMax = fFitRegionTPCMin = fFitRegionTPCMax = 0.;
        if (fSettings->GetUseDEDxRatios()) {
          fFitRegionTPCMin = 1.;
          fFitRegionTPCMax = 1.;
        }

	// Loop over species.
	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {

		// Check whether the peak is included in this fit function (stage).
		AliTOFTPCPeakDiHadronPID* currentPeak = (AliTOFTPCPeakDiHadronPID*)fPeaks->At(iSpecies);
		if (currentPeak->IsUsedInFit()) {
			fFitRegionTOFMin = TMath::Min(fFitRegionTOFMin, AliFunctionsDiHadronPID::EstimateTOFPosition(fMeanPt, fAbsMeanEta, fMassAssumption, (AliSettingsDiHadronPID::Species)iSpecies));
			fFitRegionTOFMax = TMath::Max(fFitRegionTOFMax, AliFunctionsDiHadronPID::EstimateTOFPosition(fMeanPt, fAbsMeanEta, fMassAssumption, (AliSettingsDiHadronPID::Species)iSpecies));
                        if (fSettings->GetUseDEDxRatios()) {
      			   fFitRegionTPCMin = TMath::Min(fFitRegionTPCMin, AliFunctionsDiHadronPID::EstimateTPCPositionRatio(fMeanPt, fAbsMeanEta, fMassAssumption, (AliSettingsDiHadronPID::Species)iSpecies));
			   fFitRegionTPCMax = TMath::Max(fFitRegionTPCMax, AliFunctionsDiHadronPID::EstimateTPCPositionRatio(fMeanPt, fAbsMeanEta, fMassAssumption, (AliSettingsDiHadronPID::Species)iSpecies));			
                        }
                        else {
        			fFitRegionTPCMin = TMath::Min(fFitRegionTPCMin, AliFunctionsDiHadronPID::EstimateTPCPosition(fMeanPt, fAbsMeanEta, fMassAssumption, (AliSettingsDiHadronPID::Species)iSpecies));
         			fFitRegionTPCMax = TMath::Max(fFitRegionTPCMax, AliFunctionsDiHadronPID::EstimateTPCPosition(fMeanPt, fAbsMeanEta, fMassAssumption, (AliSettingsDiHadronPID::Species)iSpecies));			
                        }
		}

	}

	fFitRegionTOFMin -= fSettings->GetFitRegionTOFmin();
	fFitRegionTOFMax += fSettings->GetFitRegionTOFmax();
	fFitRegionTPCMin -= fSettings->GetFitRegionTPCmin();
	fFitRegionTPCMax += fSettings->GetFitRegionTPCmax();

}
// -----------------------------------------------------------------------
void AliTOFTPCFitFunctionDiHadronPID::SetFitRegion(AliTOFTPCFitFunctionDiHadronPID const *other) {
  // Set fit region equal to other function; used for deltaChi2 and deltaLL calculations
  
  fFitRegionTOFMin = other->GetFitRegionTOFMin();
  fFitRegionTOFMax = other->GetFitRegionTOFMax();
  fFitRegionTPCMin = other->GetFitRegionTPCMin();
  fFitRegionTPCMax = other->GetFitRegionTPCMax();
}
// -----------------------------------------------------------------------
void AliTOFTPCFitFunctionDiHadronPID::SetPars(const double* pars) {

	// Quick but unsafe way to set the parameters.
	AliLogDiHadronPID::Debug4(Form("Called."), __func__, ClassName());
	
	fMismatchYield = pars[0];

	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {
		
		AliTOFTPCPeakDiHadronPID* currentPeak = (AliTOFTPCPeakDiHadronPID*)(fPeaks->At(iSpecies));
		if (!currentPeak->IsUsedInFit()) {continue;}

		Int_t offset = iSpecies * AliTOFTPCPeakDiHadronPID::GetNParameters() + 1;

		currentPeak->SetParValue(AliTOFTPCPeakDiHadronPID::kYield, pars[0 + offset]);
		currentPeak->SetParValue(AliTOFTPCPeakDiHadronPID::kMuTOF, pars[1 + offset]);
		currentPeak->SetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTOF, pars[2 + offset]);
		currentPeak->SetParValue(AliTOFTPCPeakDiHadronPID::kTailTOF, pars[3 + offset]);
		currentPeak->SetParValue(AliTOFTPCPeakDiHadronPID::kMuTPC, pars[4 + offset]);
		currentPeak->SetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTPC, pars[5 + offset]);
		if (currentPeak->TPCTailIsUsed()) {currentPeak->SetParValue(AliTOFTPCPeakDiHadronPID::kTailTPC, pars[6 + offset]);}

	}

}

// -----------------------------------------------------------------------
void AliTOFTPCFitFunctionDiHadronPID::SetParErrors(const double* errors) {

	// Quick but unsafe way to set the parameters.
	AliLogDiHadronPID::Debug4(Form("Called."), __func__, ClassName());
	
	fMismatchYieldError = errors[0];

	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {
		
		AliTOFTPCPeakDiHadronPID* currentPeak = (AliTOFTPCPeakDiHadronPID*)(fPeaks->At(iSpecies));
		if (!currentPeak->IsUsedInFit()) {continue;}

		Int_t offset = iSpecies * AliTOFTPCPeakDiHadronPID::GetNParameters() + 1;

		currentPeak->SetParError(AliTOFTPCPeakDiHadronPID::kYield, errors[0 + offset]);
		currentPeak->SetParError(AliTOFTPCPeakDiHadronPID::kMuTOF, errors[1 + offset]);
		currentPeak->SetParError(AliTOFTPCPeakDiHadronPID::kSigmaTOF, errors[2 + offset]);
		currentPeak->SetParError(AliTOFTPCPeakDiHadronPID::kTailTOF, errors[3 + offset]);
		currentPeak->SetParError(AliTOFTPCPeakDiHadronPID::kMuTPC, errors[4 + offset]);
		currentPeak->SetParError(AliTOFTPCPeakDiHadronPID::kSigmaTPC, errors[5 + offset]);
		if (currentPeak->TPCTailIsUsed()) {currentPeak->SetParError(AliTOFTPCPeakDiHadronPID::kTailTPC, errors[6 + offset]);}

	}

}

// -----------------------------------------------------------------------
void AliTOFTPCFitFunctionDiHadronPID::Print(Option_t*) const {

	// Print out of the fit results.
	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

	cout << "---------------------------------------------------" << endl;
	cout << " Fit Function, m.a.: " << fSettings->GetSpeciesName(fMassAssumption) << endl;
	cout << "---------------------------------------------------" << endl;
	cout << " Included peaks: " << endl;

	// Print a reduced amount of information about the peaks.
	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {
			
		AliTOFTPCPeakDiHadronPID* currentPeak = (AliTOFTPCPeakDiHadronPID*)fPeaks->At(iSpecies);

		if (!currentPeak) {
			AliLogDiHadronPID::Warning(Form("Could not find peak of species: %s", AliSettingsDiHadronPID::GetSpeciesName((AliSettingsDiHadronPID::Species)iSpecies)), __func__, ClassName());
			continue;
		}

		if (currentPeak->IsUsedInFit()) {

			cout << "   " << currentPeak->GetPeakSpeciesName() << " peak" << endl;

			for (Int_t iParameter = 0; iParameter < AliTOFTPCPeakDiHadronPID::GetNParameters(); iParameter++) {

				AliTOFTPCPeakDiHadronPID::Parameter currentPar = (AliTOFTPCPeakDiHadronPID::Parameter)iParameter;
				
				// Only print the TPC tail if it is included.
				if (!(currentPeak->TPCTailIsUsed()) && currentPar == AliTOFTPCPeakDiHadronPID::kTailTPC) {continue;}

				cout << "     " << AliTOFTPCPeakDiHadronPID::GetParName(currentPar) << ": " << currentPeak->GetParValue(currentPar) << " ± " << currentPeak->GetParError(currentPar) << (currentPeak->ParameterIsFixed(currentPar) ? " (FIXED)" : " (FREE)") << endl;  

			}
		
		cout << endl;

		}
	}

	// Print the mismatches
	cout << " Mismatch Yield: " << fMismatchYield << " ± " << fMismatchYieldError << (fMismatchYieldIsFixed ? " (FIXED)" : " (FREE)") << endl;

	cout << endl;

	cout << " TOF bin width: " << fTOFbinWidth << endl;
	cout << " TPC bin width: " << fTPCbinWidth << endl;

	cout << endl;

	cout << " Fit region TOF: " << fFitRegionTOFMin << " < TOF < " << fFitRegionTOFMax << endl;
	cout << " Fit region TPC: " << fFitRegionTPCMin << " < TPC < " << fFitRegionTPCMax << endl;

	cout << endl;

	cout << " Fit status: " << fFitStatus << endl;

	cout << "---------------------------------------------------" << endl;
	cout << endl;

}

// -----------------------------------------------------------------------
AliSettingsDiHadronPID::Species AliTOFTPCFitFunctionDiHadronPID::GetSpeciesFromIndex(Int_t index) {

	// Not well defined for index = 0;
	return (AliSettingsDiHadronPID::Species)((index - 1) / AliTOFTPCPeakDiHadronPID::GetNParameters());

}

// -----------------------------------------------------------------------
AliTOFTPCPeakDiHadronPID::Parameter AliTOFTPCFitFunctionDiHadronPID::GetParameterFromIndex(Int_t index) {

	// Not well defined for index = 0;
	return ((AliTOFTPCPeakDiHadronPID::Parameter)((index - 1) % AliTOFTPCPeakDiHadronPID::GetNParameters()));

}

// -----------------------------------------------------------------------
Int_t AliTOFTPCFitFunctionDiHadronPID::GetIndexFromSpeciesAndParameter(AliSettingsDiHadronPID::Species species, AliTOFTPCPeakDiHadronPID::Parameter parameter) {

	return (1 + ((Int_t)species) * AliTOFTPCPeakDiHadronPID::GetNParameters() + (Int_t)parameter);

}
