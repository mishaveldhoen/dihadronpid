/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  This Task will run over a data sample of Mini Data, (or MC), and 
//  it will generate QA histograms / performs sanity checks.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include "AliHistToolsDiHadronPID.h"
#include "AliMiniEventDiHadronPID.h"
#include "AliMiniTrackDiHadronPID.h"

#include "AliQATaskDiHadronPID.h"
#include "AliLogDiHadronPID.h"

ClassImp(AliQATaskDiHadronPID);

// -----------------------------------------------------------------------
AliQATaskDiHadronPID::AliQATaskDiHadronPID(const char* name, const char* title):
	AliAnalyzeDataDiHadronPID(name, title),
	fPtSpectrumUITotal(0x0),
	fFractionUITOFHit(0x0),
	fFractionUISelForPID(0x0),
	fFractionUITPCClusterCut(0x0),
    fPtEtaSpectrumUITotal(0x0),
    fPtEtaFractionUITOFHit(0x0),
    fPtEtaFractionUISelForPID(0x0),
    fPtEtaFractionUITPCClusterCut(0x0)

{
	
	// Default Constructor
	AliLogDiHadronPID::Debug2("Default constructor called.", __func__, ClassName());

	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		fPtSpectrumPIDTotal[iSpecies] = 0x0;
		fFractionPIDTOFHit[iSpecies] = 0x0;
		fFractionPIDTPCClusterCut[iSpecies] = 0x0;
	}

	fOutputFileName = "QAhistos.root";

}

// -----------------------------------------------------------------------
AliQATaskDiHadronPID::~AliQATaskDiHadronPID() {
	
	// Destructor
	AliLogDiHadronPID::Debug2("Destructor called.", __func__, ClassName());

	if (fPtSpectrumUITotal) {delete fPtSpectrumUITotal; fPtSpectrumUITotal = 0x0;}
	if (fFractionUITOFHit) {delete fFractionUITOFHit; fFractionUITOFHit = 0x0;}
	if (fFractionUISelForPID) {delete fFractionUISelForPID; fFractionUISelForPID = 0x0;}
	if (fFractionUITPCClusterCut) {delete fFractionUITPCClusterCut; fFractionUITPCClusterCut = 0x0;}

	if (fPtEtaSpectrumUITotal) {delete fPtEtaSpectrumUITotal; fPtEtaSpectrumUITotal = 0x0;}
	if (fPtEtaFractionUITOFHit) {delete fPtEtaFractionUITOFHit; fPtEtaFractionUITOFHit = 0x0;}
	if (fPtEtaFractionUISelForPID) {delete fPtEtaFractionUISelForPID; fPtEtaFractionUISelForPID = 0x0;}
	if (fPtEtaFractionUITPCClusterCut) {delete fPtEtaFractionUITPCClusterCut; fPtEtaFractionUITPCClusterCut = 0x0;}

	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		if (fPtSpectrumPIDTotal[iSpecies]) {delete fPtSpectrumPIDTotal[iSpecies]; fPtSpectrumPIDTotal[iSpecies] = 0x0;}
		if (fFractionPIDTOFHit[iSpecies]) {delete fFractionPIDTOFHit[iSpecies]; fFractionPIDTOFHit[iSpecies] = 0x0;}
		if (fFractionPIDTPCClusterCut[iSpecies]) {delete fFractionPIDTPCClusterCut[iSpecies]; fFractionPIDTPCClusterCut[iSpecies] = 0x0;}
	}

}

// -----------------------------------------------------------------------
void AliQATaskDiHadronPID::UserCreateOutput() {

	fPtSpectrumUITotal = (TH1D*)AliHistToolsDiHadronPID::MakeHist("fPtSpectrumUITotal", "p_{T} distribution (total)", "D", fSettings->GetPtAxis());
	fFractionUITOFHit = (TH1D*)AliHistToolsDiHadronPID::MakeHist("fFractionUITOFHit", "fraction with TOF hit", "D", fSettings->GetPtAxis());
	fFractionUISelForPID = (TH1D*)AliHistToolsDiHadronPID::MakeHist("fFractionUISelForPID", "fraction selected for PID", "D", fSettings->GetPtAxis());
	fFractionUITPCClusterCut = (TH1D*)AliHistToolsDiHadronPID::MakeHist("fFractionUITPCClusterCut", "fraction surviving TPC cluster cut", "D", fSettings->GetPtAxis());

	fPtEtaSpectrumUITotal = (TH2D*)AliHistToolsDiHadronPID::MakeHist("fPtEtaSpectrumUITotal", "p_{T} distribution (total)", "D", fSettings->GetPtAxis(), fSettings->GetEtaAxis());
	fPtEtaFractionUITOFHit = (TH2D*)AliHistToolsDiHadronPID::MakeHist("fPtEtaFractionUITOFHit", "fraction with TOF hit", "D", fSettings->GetPtAxis(), fSettings->GetEtaAxis());
	fPtEtaFractionUISelForPID = (TH2D*)AliHistToolsDiHadronPID::MakeHist("fPtEtaFractionUISelForPID", "fraction selected for PID", "D", fSettings->GetPtAxis(), fSettings->GetEtaAxis());
	fPtEtaFractionUITPCClusterCut = (TH2D*)AliHistToolsDiHadronPID::MakeHist("fPtEtaFractionUITPCClusterCut", "fraction surviving TPC cluster cut", "D", fSettings->GetPtAxis(), fSettings->GetEtaAxis());

//	const char* speciesNames[3] = {"Pion", "Kaon", "Proton"};
//	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
//		fPtSpectrumPIDTotal[iSpecies] = (TH1D*)AliHistToolsDiHadronPID::MakeHist(Form("fPtSpectrumPIDTotal%s", ))
//	}

}

// -----------------------------------------------------------------------
void AliQATaskDiHadronPID::UserExec() {

	// Get a ptr to the current event.
	AliMiniEventDiHadronPID* currentEvent = GetCurrentEvent();

	// Identify pairs in the same event.
	for (Int_t iTrack = 0; iTrack < currentEvent->GetNTracks(); ++iTrack) { 

		AliMiniTrackDiHadronPID* currentTrack = currentEvent->GetTrack(iTrack);

		// Check track filtermap against settings filtermask.
		if (fSettings->GetFilterMask()) {if (!(currentTrack->GetFilterMap() & fSettings->GetFilterMask())) {continue;}}
		fPtSpectrumUITotal->Fill(currentTrack->Pt());
        fPtEtaSpectrumUITotal->Fill(currentTrack->Pt(), currentTrack->Eta());

		// Check the TOFout requirement.
		if (currentTrack->IsFlagSet(AliMiniTrackDiHadronPID::kTOFout)) {
			fFractionUITOFHit->Fill(currentTrack->Pt());
            fPtEtaFractionUITOFHit->Fill(currentTrack->Pt(), currentTrack->Eta());
		}

		// Check if track passes all the PID cuts. 
		if (fSettings->IsTrackSelectedForPID(currentTrack)) {
			fFractionUISelForPID->Fill(currentTrack->Pt());
            fPtEtaFractionUISelForPID->Fill(currentTrack->Pt(), currentTrack->Eta());
		}

		// Check the NClusters requirement.
		if (currentTrack->GetTPCSignalN() > fSettings->GetMinClustersdEdx()) {
			fFractionUITPCClusterCut->Fill(currentTrack->Pt());
            fPtEtaFractionUITPCClusterCut->Fill(currentTrack->Pt(), currentTrack->Eta());
		}

	}

}

// -----------------------------------------------------------------------
void AliQATaskDiHadronPID::UserFinalizeAnalysis() {

	// Note the binomial errors, as the tracks with a PID flag are a strict 
	// subset of the total number of tracks.
	fPtSpectrumUITotal->Sumw2();
	fFractionUITOFHit->Sumw2();
	fFractionUISelForPID->Sumw2();
	fFractionUITPCClusterCut->Sumw2();

	fFractionUITOFHit->Divide(fFractionUITOFHit, fPtSpectrumUITotal, 1, 1, "B");
	fFractionUISelForPID->Divide(fFractionUISelForPID, fPtSpectrumUITotal, 1, 1, "B");
	fFractionUITPCClusterCut->Divide(fFractionUITPCClusterCut, fPtSpectrumUITotal, 1, 1, "B");

	fPtEtaSpectrumUITotal->Sumw2();
	fPtEtaFractionUITOFHit->Sumw2();
	fPtEtaFractionUISelForPID->Sumw2();
	fPtEtaFractionUITPCClusterCut->Sumw2();

	fPtEtaFractionUITOFHit->Divide(fPtEtaFractionUITOFHit, fPtEtaSpectrumUITotal, 1, 1, "B");
	fPtEtaFractionUISelForPID->Divide(fPtEtaFractionUISelForPID, fPtEtaSpectrumUITotal, 1, 1, "B");
	fPtEtaFractionUITPCClusterCut->Divide(fPtEtaFractionUITPCClusterCut, fPtEtaSpectrumUITotal, 1, 1, "B");

}

// -----------------------------------------------------------------------
void AliQATaskDiHadronPID::UserSaveOutput() {

	fPtSpectrumUITotal->Write();
	fFractionUITOFHit->Write();
	fFractionUISelForPID->Write();
	fFractionUITPCClusterCut->Write();

	fPtEtaSpectrumUITotal->Write();
	fPtEtaFractionUITOFHit->Write();
	fPtEtaFractionUISelForPID->Write();
	fPtEtaFractionUITPCClusterCut->Write();

}

// -----------------------------------------------------------------------
void AliQATaskDiHadronPID::Print(Option_t*) const {
	
	// Print info about the class.
	AliLogDiHadronPID::Debug3("Called.", __func__, ClassName());

}
