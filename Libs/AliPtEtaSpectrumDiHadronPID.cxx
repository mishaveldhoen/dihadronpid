/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Identified spectrum class, with bins in pT and eta.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)	

#include "AliTOFTPCPeakDiHadronPID.h"
#include "AliPtEtaSpectrumDiHadronPID.h"
#include "AliTOFTPCFitContainerDiHadronPID.h"
#include "AliTOFTPCFitterLeanDiHadronPID.h"
#include "AliHistToolsDiHadronPID.h"
#include "AliMiscToolsDiHadronPID.h"
#include "AliLogDiHadronPID.h"
#include "TStopwatch.h"
#include "TString.h"

ClassImp(AliPtEtaSpectrumDiHadronPID);

// -----------------------------------------------------------------------
AliPtEtaSpectrumDiHadronPID::AliPtEtaSpectrumDiHadronPID():
	TNamed(),
	fSettings(AliSettingsDiHadronPID::Instance()),
	fMassAssumption(AliSettingsDiHadronPID::kUnknownSpecies),
	fPtAxis(0x0),
	fEtaAxis(0x0),
	fFitResults(0x0)

{
	
	/** Default Constructor */
	AliLogDiHadronPID::Debug2(Form("Default constructor called."), __func__, ClassName());
	
}

// -----------------------------------------------------------------------
AliPtEtaSpectrumDiHadronPID::AliPtEtaSpectrumDiHadronPID(const char* name, const char* title, AliSettingsDiHadronPID::Species massAssumption):
	TNamed(name, title),
	fSettings(AliSettingsDiHadronPID::Instance()),
	fMassAssumption(massAssumption),
	fPtAxis(0x0),
	fEtaAxis(0x0),
	fFitResults(0x0)

{
	
	/** Named Constructor */
	AliLogDiHadronPID::Debug2(Form("Named Constructor called, name: %s, title: %s", name, title), __func__, ClassName());

	// Call copy constructors on the axes.
	fPtAxis	= new TAxis(*(fSettings->GetPtAxis()));
	fEtaAxis = new TAxis(*(fSettings->GetEtaAxis()));

	// Create the array of fit results. Will contain instances of AliTOFTPCFitContainerDiHadronPID.
	fFitResults = new AliObjectArray2DDiHadronPID(fPtAxis->GetNbins(), fEtaAxis->GetNbins());

}

// -----------------------------------------------------------------------
AliPtEtaSpectrumDiHadronPID::~AliPtEtaSpectrumDiHadronPID() {
	
	/** Destructor */
	AliLogDiHadronPID::Debug2(Form("Destructor called."), __func__, ClassName());
	if (fPtAxis) {delete fPtAxis; fPtAxis = 0x0;}
	if (fEtaAxis) {delete fEtaAxis; fEtaAxis = 0x0;}
	if (fFitResults) {delete fFitResults; fPtAxis = 0x0;}

}

// -----------------------------------------------------------------------
Bool_t AliPtEtaSpectrumDiHadronPID::ImportPIDDataForBin(const TH2F* data, const TH2F* mismatch, Int_t ptBin, Int_t etaBin) {

	/**
	 * Used to import a PID histogram \f$(TOF,TPC)\f$ for a single bin in bin in \f$p_T\f$ and \f$\eta\f$.
	 *
	 * \param data The data histogram in \f$(TOF,TPC)\f$.
	 * \param mismatch The mismatch template histogram in \f$(TOF,TPC)\f$.
	 * \param ptBin Index of the \f$p_T\f$ bin.
	 * \param etaBin Index of the \f$\eta\f$ bin.
	 */

	// Creates a fit container class, and fills it with the supplied PID data.
	AliLogDiHadronPID::Debug3(Form("Called for ptBin: %i, etaBin: %i.", ptBin, etaBin), __func__, ClassName());
	
	// Check if there already exists a fit container at this spot.
	if (fFitResults->GetAt(ptBin - 1, etaBin - 1)) {
		AliLogDiHadronPID::Error(Form("PID data for ptBin: %i, etaBin: %i already available...", ptBin, etaBin), __func__, ClassName());
		return kFALSE;
	}

	// Create a new fit container. Parameter initialisation is based on supplied histograms, and 
	// kinematic info.
	AliTOFTPCFitContainerDiHadronPID* container = new AliTOFTPCFitContainerDiHadronPID(
		Form("cont_ptBin%i_etaBin%i",ptBin,etaBin),Form("cont_ptBin%i_etaBin%i",ptBin,etaBin),data, mismatch, 
		fPtAxis->GetBinLowEdge(ptBin), fPtAxis->GetBinUpEdge(ptBin),
		fEtaAxis->GetBinLowEdge(etaBin), fEtaAxis->GetBinUpEdge(etaBin),
		fMassAssumption);

	//cout << "Container mass assumption:" << container->GetMassAssumption() << " m.a. spectrum: " << fMassAssumption << endl;

	// Add fit container to the fFitResults array.
	fFitResults->SetAt(container, ptBin - 1, etaBin - 1);

	return kTRUE;

}

// -----------------------------------------------------------------------
Int_t AliPtEtaSpectrumDiHadronPID::FitBin(Int_t ptBin, Int_t etaBin, Bool_t saveImages, Bool_t reset, Bool_t showFullDebugInfo) {

	/** 
     * Fit a single bin. 
     *
	 * \param ptBin Index of the \f$p_T\f$ bin.
	 * \param etaBin Index of the \f$\eta\f$ bin.
	 * \param saveImages If true, then store pdf images of the fit in a subdirectory of the current directory
	 * \c ./InclusiveFits/Species/PtBin/EtaBin.
	 * \param reset In case this bin has been fitted before, then this class remembers the status of the fitter. If this
	 * parameter is false, then the next fit will take the parameters from the previous fit as initial parameters. If true,
	 * then it will start from scratch.
	 */
	AliLogDiHadronPID::Debug4(Form("Called, ptBin: %i, etaBin: %i", ptBin, etaBin), __func__, ClassName());

	// Check if a fit container is present.
	if (!fFitResults->GetAt(ptBin - 1, etaBin - 1)) {
		AliLogDiHadronPID::Error(Form("No data imported yet for ptBin: %i, etaBin: %i", ptBin, etaBin), __func__, ClassName());
		return -999;
	}

	// Get a pointer to the fit container.
	AliTOFTPCFitContainerDiHadronPID* container = (AliTOFTPCFitContainerDiHadronPID*)(fFitResults->GetAt(ptBin - 1, etaBin - 1));

	// By default, the fit function initialises itself based on the data and mismatch histograms, as well 
	// as the kinematic info (pt and eta windows). However, if a fit of lower p_T is available, then it's
	// probably better to get those parameters as start conditons.

	// REVISION: This is likely not the case at low pT! For now we turn this off.
	// if (ptBin > 1) {
	// 	if (fFitResults->GetAt(ptBin - 2, etaBin - 1)) {
	// 		AliTOFTPCFitContainerDiHadronPID* otherContainer = (AliTOFTPCFitContainerDiHadronPID*)fFitResults->GetAt(ptBin - 2, etaBin - 1);
	// 		if (otherContainer->GetCurrentFitStatus() == 0) {
	// 			AliLogDiHadronPID::Info(Form("Copying initial peak positons and resolution from previous fit."), __func__, ClassName());
				
	// 			container->ReInitialise(otherContainer);

	// 		} else {
	// 			AliLogDiHadronPID::Info(Form("No previous fit available to copy parameters from."), __func__, ClassName());
				
	// 		}
	// 	}
	// }

	// Create a fitter.
	AliTOFTPCFitterLeanDiHadronPID* fitter = new AliTOFTPCFitterLeanDiHadronPID(container);

	// Perform the fit, according to the strategy defined in the settings.
	//Int_t strategy = fSettings->GetFitStrategyForPtBinAndSpecies(ptBin, fMassAssumption);
	AliLogDiHadronPID::Info(Form("Starting fit for: m.a.: %i, ptBin: %i, etaBin: %i.",fMassAssumption, ptBin, etaBin), __func__, ClassName());

	Int_t fitStatus = fitter->Fit(reset, showFullDebugInfo);

	// Save fit stages to file if requested.	
	if (saveImages) {container->SaveFitStages(Form("InclusiveFits/Sp%i/Pt%02i/Eta%02i", fMassAssumption, ptBin, etaBin));}
	
	return fitStatus;

}

// -----------------------------------------------------------------------
Int_t AliPtEtaSpectrumDiHadronPID::FitBinRange(Int_t ptMinBin, Int_t ptMaxBin, Int_t etaMinBin, Int_t etaMaxBin, Bool_t saveImages, Bool_t reset) {

	/**
	 * Similar to FitBin(), but now fits a range in \f$p_T\f$ and \f$\eta\f$.
	 */

	AliLogDiHadronPID::Debug3(Form("Called for pt bins: (%i, %i), and eta bins: (%i, %i).", ptMinBin, ptMaxBin, etaMinBin, etaMaxBin), __func__, ClassName());
	
	// Start the timer.
	TStopwatch sw;
	sw.Start();

	// Return the sum of the statusses.
	Int_t status = 0;

	for (Int_t iPtBin = ptMinBin; iPtBin <= ptMaxBin; iPtBin++) {
		for (Int_t iEtaBin = etaMinBin; iEtaBin <= etaMaxBin; iEtaBin++) {
			status += FitBin(iPtBin, iEtaBin, saveImages, reset);
		}	
	}

	cout << "Fitting time: " << AliMiscToolsDiHadronPID::FormatTime(sw.RealTime()).Data() << endl;

	return status;

}

// -----------------------------------------------------------------------
Int_t AliPtEtaSpectrumDiHadronPID::FitAllBins(Bool_t saveImages, Bool_t reset) {

	/**
	 * Similar to FitBin(), but now fits the entire range in \f$p_T\f$ and \f$\eta\f$.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	return FitBinRange(1, fPtAxis->GetNbins(), 1, fEtaAxis->GetNbins(), saveImages, reset);

}

// -----------------------------------------------------------------------
Int_t AliPtEtaSpectrumDiHadronPID::FixBin(Int_t ptBin, Int_t etaBin, Bool_t saveImages) {

	/**
	 * This method will try to fix a fit by selectively reinitialising the container to values 
	 * of neighboring (succeeded) fits. 
	 *
	 * \note This method works, but is not actively used.
	 */
	AliLogDiHadronPID::Debug3(Form("Called, ptBin: %i, etaBin: %i", ptBin, etaBin), __func__, ClassName());

	// Reset the container.
	AliTOFTPCFitContainerDiHadronPID* centralContainer = (AliTOFTPCFitContainerDiHadronPID*)(fFitResults->GetAt(ptBin - 1, etaBin - 1));
	if (!centralContainer) {
		AliLogDiHadronPID::Error(Form("Cannot find container, ptBin: %i, etaBin: %i", ptBin, etaBin), __func__, ClassName());
		return -1;
	}
	centralContainer->ResetFitStages();
	centralContainer->NewFitStage(kFALSE); // Create a first fit stage, without copying from a previous one (since there is none).
	AliTOFTPCFitFunctionDiHadronPID* newFitStage = centralContainer->CurrentFitStage();

	// Get a pointer to the fit container's neighbors. Most of this array is redundant, but it will make the code
	// more readable.
	AliTOFTPCFitContainerDiHadronPID* containers[fEtaAxis->GetNbins() + 2][fPtAxis->GetNbins() + 2]; // [etaBin][ptBin]
	AliTOFTPCFitFunctionDiHadronPID* lastFits[fEtaAxis->GetNbins() + 2][fPtAxis->GetNbins() + 2]; // [etaBin][ptBin]
	for (Int_t ii = 0; ii < fEtaAxis->GetNbins() + 2; ii++) {
		for (Int_t jj = 0; jj < fPtAxis->GetNbins() + 2; jj++) {
			containers[ii][jj] = 0x0;
			lastFits[ii][jj] = 0x0;
		}
	}

	for (Int_t iEtaBin = TMath::Max(1, etaBin - 1); iEtaBin <= TMath::Min(fEtaAxis->GetNbins(), etaBin + 1); iEtaBin++) {
		for (Int_t iPtBin = TMath::Max(1, ptBin - 1); iPtBin <= TMath::Min(fPtAxis->GetNbins(), ptBin + 1); iPtBin++) {

			containers[iEtaBin][iPtBin] = (AliTOFTPCFitContainerDiHadronPID*)(fFitResults->GetAt(iPtBin - 1, iEtaBin - 1));
			
			// If the container exists, and contains a good last fit, then obtain a ptr to that last fit stage.
			// Else, do nothing. The null pointer will later be interpreted as invalid or missing.
			if ((containers[iEtaBin][iPtBin]) && ((containers[iEtaBin][iPtBin])->GetCurrentFitStatus() == 0)) {
				lastFits[iEtaBin][iPtBin] = (containers[iEtaBin][iPtBin])->LastFitStage();
				AliLogDiHadronPID::Info(Form("Fit from ptBin: %i, etaBin: %i can be used for copying parameters.", iPtBin, iEtaBin), __func__, ClassName());
			} else {
				AliLogDiHadronPID::Info(Form("Fit from ptBin: %i, etaBin: %i has probaly failed, and will not be used for copying parameters.", iPtBin, iEtaBin), __func__, ClassName());
			}
		}
	}

	// *************************************
	// Determine the average sigmaTOF, tailTOF, sigmaTPC by taking the average of the neighbors in pT.
	// *************************************
	Double_t avgSigmaTOF[AliSettingsDiHadronPID::GetNSpecies()];
	Double_t avgTailTOF[AliSettingsDiHadronPID::GetNSpecies()];
	Double_t avgSigmaTPC[AliSettingsDiHadronPID::GetNSpecies()];
	for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {
		avgSigmaTOF[iSpecies] = 0.;
		avgTailTOF[iSpecies] = 0.;
		avgSigmaTPC[iSpecies] = 0.;
	}
	Int_t nOtherFitsUsed = 0;

	// Look at one pT bin lower...
	if (lastFits[etaBin][ptBin - 1]) {
		nOtherFitsUsed++;
		for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {
			avgSigmaTOF[iSpecies] += (lastFits[etaBin][ptBin - 1])->GetPeak(iSpecies)->GetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTOF);
			avgTailTOF[iSpecies] += (lastFits[etaBin][ptBin - 1])->GetPeak(iSpecies)->GetParValue(AliTOFTPCPeakDiHadronPID::kTailTOF);;
			avgSigmaTPC[iSpecies] += (lastFits[etaBin][ptBin - 1])->GetPeak(iSpecies)->GetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTPC);;
		}
	}

	// And one higher.
	if (lastFits[etaBin][ptBin + 1]) {
		nOtherFitsUsed++;
		for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {
			avgSigmaTOF[iSpecies] += (lastFits[etaBin][ptBin + 1])->GetPeak(iSpecies)->GetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTOF);
			avgTailTOF[iSpecies] += (lastFits[etaBin][ptBin + 1])->GetPeak(iSpecies)->GetParValue(AliTOFTPCPeakDiHadronPID::kTailTOF);;
			avgSigmaTPC[iSpecies] += (lastFits[etaBin][ptBin + 1])->GetPeak(iSpecies)->GetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTPC);;
		}
	}

	if (nOtherFitsUsed == 0) {
		AliLogDiHadronPID::Warning(Form("Tried to fix a fit with no succeeded fits directly above or below in pT... Skipping this fit for now."), __func__, ClassName());
		return -9;
	}
	if (nOtherFitsUsed == 1) {AliLogDiHadronPID::Warning(Form("Tried to fix a fit with only one succeeded fits directly above or below in pT... Fit has a higher chance of failing."), __func__, ClassName());}
	if (nOtherFitsUsed == 2) {
		for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {
			avgSigmaTOF[iSpecies] /= nOtherFitsUsed;
			avgTailTOF[iSpecies] /= nOtherFitsUsed;
			avgSigmaTPC[iSpecies] /= nOtherFitsUsed;
		}
	}

	// Copying parameters into the new fit stage.
	for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {
		newFitStage->GetPeak(iSpecies)->SetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTOF, avgSigmaTOF[iSpecies]);//SetSigmaTOFForSpecies(avgSigmaTOF[iSpecies], iSpecies);
		newFitStage->GetPeak(iSpecies)->SetParValue(AliTOFTPCPeakDiHadronPID::kTailTOF, avgTailTOF[iSpecies]);//->SetTailTOFForSpecies(avgTailTOF[iSpecies], iSpecies);
		newFitStage->GetPeak(iSpecies)->SetParValue(AliTOFTPCPeakDiHadronPID::kSigmaTPC, avgSigmaTPC[iSpecies]);//->SetSigmaTPCForSpecies(avgSigmaTPC[iSpecies], iSpecies);
	}

	// *************************************
	// Determine the average muTOF, muTPC, by taking the average of neighbors in eta.
	// *************************************
	Double_t avgMuTOF[AliSettingsDiHadronPID::GetNSpecies()];
	Double_t avgMuTPC[AliSettingsDiHadronPID::GetNSpecies()];
	for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {
		avgMuTOF[iSpecies] = 0.;
		avgMuTPC[iSpecies] = 0.;
	}	
	nOtherFitsUsed = 0;

	// Look at one eta bin lower...
	if (lastFits[etaBin - 1][ptBin]) {
		nOtherFitsUsed++;
		for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {
			avgMuTOF[iSpecies] += (lastFits[etaBin - 1][ptBin])->GetPeak(iSpecies)->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTOF);
			avgMuTPC[iSpecies] += (lastFits[etaBin - 1][ptBin])->GetPeak(iSpecies)->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTPC);
		}
	}

	// And one higher.
	if (lastFits[etaBin + 1][ptBin]) {
		nOtherFitsUsed++;
		for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {
			avgMuTOF[iSpecies] += (lastFits[etaBin + 1][ptBin])->GetPeak(iSpecies)->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTOF);
			avgMuTPC[iSpecies] += (lastFits[etaBin + 1][ptBin])->GetPeak(iSpecies)->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTPC);
		}
	}

	if (nOtherFitsUsed == 0) {
		AliLogDiHadronPID::Warning(Form("Tried to fix a fit with no succeeded fits directly above or below in eta... Skipping this fit for now."), __func__, ClassName());
		return -9;
	}
	if (nOtherFitsUsed == 1) {AliLogDiHadronPID::Warning(Form("Tried to fix a fit with only one succeeded fits directly above or below in eta... Fit has a higher chance of failing."), __func__, ClassName());}
	if (nOtherFitsUsed == 2) {
		for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {
			avgMuTOF[iSpecies] /= nOtherFitsUsed;
			avgMuTPC[iSpecies] /= nOtherFitsUsed;
		}
	}

	// Copying parameters into the new fit stage.
	for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {
		newFitStage->GetPeak(iSpecies)->SetParValue(AliTOFTPCPeakDiHadronPID::kMuTOF, avgMuTOF[iSpecies]);//SetMuTOFForSpecies(avgMuTOF[iSpecies], iSpecies);
		newFitStage->GetPeak(iSpecies)->SetParValue(AliTOFTPCPeakDiHadronPID::kMuTPC, avgMuTPC[iSpecies]);
	}

	// Reinitialise the yields, based on the estimates of the fit parameters.
	newFitStage->ReInitialiseYields(centralContainer->GetDataHistogram(), centralContainer->GetMismatchHistogram());
	
	// Check what things look like!
	//newFitStage->Print(0);

	// Perform the fit. Do not request images, since we want them to end up in a different folder.
	Int_t fitResult = FitBin(ptBin, etaBin, kFALSE, kFALSE);

	// Save fit stages to file if requested.	
	if (saveImages) {centralContainer->SaveFitStages(Form("InclusiveRefits/Sp%i/Pt%02i/Eta%02i", fMassAssumption, ptBin, etaBin));}

	return fitResult;

}

// -----------------------------------------------------------------------
Int_t AliPtEtaSpectrumDiHadronPID::FixBinRange(Int_t ptMinBin, Int_t ptMaxBin, Int_t etaMinBin, Int_t etaMaxBin, Bool_t saveImages, Bool_t onlyFailedFits) {

	/**
	 * See FixBin().
	 */
	AliLogDiHadronPID::Debug3(Form("Called for pt bins: (%i, %i), and eta bins: (%i, %i).", ptMinBin, ptMaxBin, etaMinBin, etaMaxBin), __func__, ClassName());
	
	// Start the timer.
	TStopwatch sw;
	sw.Start();

	// Return the sum of the statusses.
	Int_t status = 0;

	for (Int_t iPtBin = ptMinBin; iPtBin <= ptMaxBin; iPtBin++) {
		for (Int_t iEtaBin = etaMinBin; iEtaBin <= etaMaxBin; iEtaBin++) {

			// Skip the fits that were OK, if that flag is requested.
			if (onlyFailedFits) {

				// Get a pointer to the fit container.
				AliTOFTPCFitContainerDiHadronPID* container = (AliTOFTPCFitContainerDiHadronPID*)(fFitResults->GetAt(iPtBin - 1, iEtaBin - 1));
				if (container->GetCurrentFitStatus() == 0) {continue;}

			}

			AliLogDiHadronPID::Info(Form("Fit from ptBin: %i, etaBin: %i has probaly failed, will try to fix...", iPtBin, iEtaBin), __func__, ClassName());
			
			status += FixBin(iPtBin, iEtaBin, saveImages);

		}	
	}

	cout << "Fitting time: " << AliMiscToolsDiHadronPID::FormatTime(sw.RealTime()).Data() << endl;

	return status;

}

// -----------------------------------------------------------------------
Int_t AliPtEtaSpectrumDiHadronPID::FixAllBins(Bool_t saveImages, Bool_t onlyFailedFits) {

	/**
	 * See FixBin().
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	return FixBinRange(1, fPtAxis->GetNbins(), 1, fEtaAxis->GetNbins(), saveImages, onlyFailedFits);

}

// -----------------------------------------------------------------------
Int_t AliPtEtaSpectrumDiHadronPID::SmoothenFitPar(AliSettingsDiHadronPID::Species species, AliTOFTPCPeakDiHadronPID::Parameter parameter, Int_t nTimes, Option_t* option) {

	/**
	 * This method expects all fits to be done (not succeeded perse). It will
	 * smoothen the fit parameters by looking at the fit parameters as a function
	 * of \f$p_T\f$ and \f$\eta\f$. After this smoothening, the fit parameters 
	 * are fixed, and only the yields are refitted. The aim was to reduce the 
	 * effect of statistical noise.
	 *
	 * \note This method was shown to be not effective, the smoothened fit parameters
	 * were actually quite far from the optimal value.
	 */
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());
	
	// Get a histogram to the desired variable.
	TH2F* fitParHist = GetFitParameter(AliTOFTPCFitFunctionDiHadronPID::GetIndexFromSpeciesAndParameter(species, parameter));
	if (!fitParHist) {
		AliLogDiHadronPID::Debug2(Form("Could not obtain parameter histogram."), __func__, ClassName());
		return -1;
	}

	// Figure out wether there are some bins that were never fitted, and should not be
	// used in the smoothening procedure.
	TAxis* axisPt = fitParHist->GetXaxis();
	for (Int_t iBin = 1; iBin <= axisPt->GetNbins(); iBin++) {
		if (!(fSettings->IsPeakIncludedInFit(fMassAssumption, species, axisPt->GetBinCenter(iBin)))) {
			axisPt->SetRange(iBin + 1, axisPt->GetNbins());
		}
	}

	// Smoothen nTimes, with the desired option.
	for (Int_t iSmoothening = 0; iSmoothening < nTimes; iSmoothening++) {fitParHist->Smooth(1, option);}

	// Set the parameters to the last fit stage.
	for (Int_t iPtBin = 1; iPtBin <= fitParHist->GetNbinsX(); iPtBin++) {
		for (Int_t iEtaBin = 1; iEtaBin <= fitParHist->GetNbinsY(); iEtaBin++) {
			AliTOFTPCPeakDiHadronPID* currentPeak = (GetFitContainer(iPtBin, iEtaBin)->LastFitStage())->GetPeak(species);
			currentPeak->SetParValue(parameter, fitParHist->GetBinContent(iPtBin, iEtaBin));
			currentPeak->SetParError(parameter, fitParHist->GetBinError(iPtBin, iEtaBin));
		}
	
	}

	return 1;

}

// -----------------------------------------------------------------------
Int_t AliPtEtaSpectrumDiHadronPID::SmoothenAllFitParsInNewStage(Int_t nTimes, Option_t* option) {

	/**
	 * Make a new fit stage with smoothened fit parameters (see SmoothenFitPar()).
	 * This method will not redo the fits.
	 */
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());
	
	// First start a new fit stage on every (pT, eta) bin.
	for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {
		for (Int_t iEtaBin = 1; iEtaBin < fEtaAxis->GetNbins(); iEtaBin++) {
			GetFitContainer(iPtBin, iEtaBin)->NewFitStage();
		}
	}

	// Loop over all parameters (except mismatches), and smoothen.
	for (Int_t iSpecies = 0; iSpecies < fSettings->GetNSpecies(); iSpecies++) {
		AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;

		for (Int_t iPar = 0; iPar < AliTOFTPCPeakDiHadronPID::GetNParameters(); iPar++) {
			AliTOFTPCPeakDiHadronPID::Parameter currentParameter = (AliTOFTPCPeakDiHadronPID::Parameter)iPar;

			SmoothenFitPar(currentSpecies, currentParameter, nTimes, option);

		}
	}

	return 1;

}

// -----------------------------------------------------------------------
Int_t AliPtEtaSpectrumDiHadronPID::RefitYields(Bool_t saveImages) {

	/**
	 * Refit only the yields (while keeing all fit parameters fixed). This 
	 * method can for example be used after SmoothenAllFitParsInNewStage().
	 */
	AliLogDiHadronPID::Debug2(Form("Called."), __func__, ClassName());
	
	// Start the timer.
	TStopwatch sw;
	sw.Start();

	for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {
		for (Int_t iEtaBin = 1; iEtaBin <= fEtaAxis->GetNbins(); iEtaBin++) {

			AliTOFTPCFitContainerDiHadronPID* currentContainer = GetFitContainer(iPtBin, iEtaBin);

			if (!currentContainer) {
				AliLogDiHadronPID::Error(Form("No data imported yet for ptBin: %i, etaBin: %i", iPtBin, iEtaBin), __func__, ClassName());
				continue;
			}

			AliTOFTPCFitterLeanDiHadronPID* fitter = new AliTOFTPCFitterLeanDiHadronPID(currentContainer);

			AliLogDiHadronPID::Info(Form("Starting REFIT for: m.a.: %i, ptBin: %i, etaBin: %i.",fMassAssumption, iPtBin, iEtaBin), __func__, ClassName());

			fitter->FitOnlyYields();

			// Save fit stages to file if requested.	
			if (saveImages) {currentContainer->SaveFitStages(Form("InclusiveRefits/Sp%i/Pt%02i/Eta%02i", fMassAssumption, iPtBin, iEtaBin));}

			delete fitter; fitter = 0x0;

		}	
	}

	cout << "Fitting time: " << AliMiscToolsDiHadronPID::FormatTime(sw.RealTime()).Data() << endl;

	return 0;

}

// -----------------------------------------------------------------------
void AliPtEtaSpectrumDiHadronPID::Print(Option_t*) const {

	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	cout << "----------------------------------------" << endl;
	cout << " (pt,eta) spectrum Info" << endl;
	cout << "----------------------------------------" << endl;
	cout << " Pt Axis: " << (fPtAxis ? "OK" : "N/A") << endl;
	cout << " Eta Axis: " << (fEtaAxis ? "OK" : "N/A") << endl;
	cout << " Fit Results Container: " << (fFitResults ? "OK" : "N/A") << endl;
	cout << "----------------------------------------" << endl;

}

// -----------------------------------------------------------------------
void AliPtEtaSpectrumDiHadronPID::SaveFitStatusAndParameters(TString directory) const {

	/**
	 * This method creates a root file containing plots of the fit status and 
	 * the fit parameters. 
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	TFile* fout = AliMiscToolsDiHadronPID::OpenROOTFile(Form("%s/Sp%i/FitParsAndStatus.root", directory.Data(), fMassAssumption), "recreate");
	if (!fout) {
		AliLogDiHadronPID::Error(Form("Could not create output file..."), __func__, ClassName());
		return;
	}
	
	// Create the file containing the fit status.
	TH2I* hFitStat = (TH2I*)AliHistToolsDiHadronPID::MakeHist("fitStatus", "fitStatus", "I", fPtAxis, fEtaAxis);

	for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {
			
		for (Int_t iEtaBin = 1; iEtaBin <= fEtaAxis->GetNbins(); iEtaBin++) {
				
			Int_t fitStat = -9;

			if (fFitResults->GetAt(iPtBin - 1, iEtaBin - 1)) {
				AliTOFTPCFitContainerDiHadronPID* container = (AliTOFTPCFitContainerDiHadronPID*)(fFitResults->GetAt(iPtBin - 1, iEtaBin - 1));			
				fitStat = container->GetCurrentFitStatus();
			}

			hFitStat->SetBinContent(iPtBin, iEtaBin, fitStat);
		
		}
	
	}
	hFitStat->Write();
	delete hFitStat;

	// Create histograms of all fit parameters.
	for (Int_t iFitPar = 0; iFitPar < AliTOFTPCFitFunctionDiHadronPID::GetNParameters(); iFitPar++) {
			
		TH2F* hFitParameter = GetFitParameter(iFitPar);
		hFitParameter->Write();
		delete hFitParameter;
	
	}

	fout->Close();
	
}

// -----------------------------------------------------------------------
void AliPtEtaSpectrumDiHadronPID::SaveFitStages(TString directory) const {

	/**
	 * This method calls AliTOFTPCFitContainerDiHadronPID::SaveFitStages()
	 * on all bins in \f$p_T\f$ and \f$\eta\f$.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {
			
		for (Int_t iEtaBin = 1; iEtaBin <= fEtaAxis->GetNbins(); iEtaBin++) {

			if (fFitResults->GetAt(iPtBin - 1, iEtaBin - 1)) {
				AliTOFTPCFitContainerDiHadronPID* container = (AliTOFTPCFitContainerDiHadronPID*)(fFitResults->GetAt(iPtBin - 1, iEtaBin - 1));			

				container->SaveFitStages(Form("%s/Pt%02i/Eta%02i", directory.Data(), iPtBin, iEtaBin));

			}

		}

	}

}

// -----------------------------------------------------------------------
TH2F* AliPtEtaSpectrumDiHadronPID::GetFitParameter(Int_t fitparameter) const {

	/**
	 * Returns a histogram of one fit parameter as a function of \f$p_T\f$ and \f$\eta\f$.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());

	TH2F* hFitParameter = (TH2F*)AliHistToolsDiHadronPID::MakeHist(AliTOFTPCFitFunctionDiHadronPID::GetParNameAtIndex(fitparameter), AliTOFTPCFitFunctionDiHadronPID::GetParNameAtIndex(fitparameter), "F", fPtAxis, fEtaAxis);

	for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {
			
		for (Int_t iEtaBin = 1; iEtaBin <= fEtaAxis->GetNbins(); iEtaBin++) {
				
			Double_t parVal = 0.;
			Double_t parErr = 0.;
			Int_t fitStat = -9;

			if (fFitResults->GetAt(iPtBin - 1, iEtaBin - 1)) {
				AliTOFTPCFitContainerDiHadronPID* container = (AliTOFTPCFitContainerDiHadronPID*)(fFitResults->GetAt(iPtBin - 1, iEtaBin - 1));			
				AliTOFTPCFitFunctionDiHadronPID* fitStage = container->CurrentFitStage();

				fitStat = container->GetCurrentFitStatus();
				parVal = fitStage->GetParValueAtIndex(fitparameter); 
				parErr = fitStage->GetParErrorAtIndex(fitparameter);
			}

			hFitParameter->SetBinContent(iPtBin, iEtaBin, parVal);
			hFitParameter->SetBinError(iPtBin, iEtaBin, parErr);
		
		}
	
	}

	return hFitParameter;

}

// -----------------------------------------------------------------------
AliTOFTPCFitContainerDiHadronPID* AliPtEtaSpectrumDiHadronPID::GetFitContainer(Int_t ptBin, Int_t etaBin) {	

	/**
	 * Returns a pointer to a fit container of a certain bin in \f$p_T\f$ and \f$\eta\f$.
	 * To be safe, it's best to first check if the axes that the user has correspond
	 * to the axes of this object, i.e., call: AxesAreTheSame().
	 */
	AliLogDiHadronPID::Debug4(Form("Called for ptBin = %i, etaBin = %i", ptBin, etaBin), __func__, ClassName());

	return (AliTOFTPCFitContainerDiHadronPID*)(fFitResults->GetAt(ptBin - 1, etaBin - 1));

}

// -----------------------------------------------------------------------
Bool_t AliPtEtaSpectrumDiHadronPID::AxesAreTheSame(const TAxis* ptAxis, const TAxis* etaAxis) const {

	/**
	 * Checks whether the axes that the user uses are the same as the ones in this object.
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	if (AliHistToolsDiHadronPID::DoAxesMatch(fPtAxis, ptAxis) &&
		AliHistToolsDiHadronPID::DoAxesMatch(fEtaAxis, etaAxis)) {return kTRUE;}
	else {return kFALSE;}

}

// -----------------------------------------------------------------------
Int_t AliPtEtaSpectrumDiHadronPID::GetNumberOfFailedFits() const {

	/** 
	 * Returns how many bins in this spectrum do not have a fit that
	 * succeeded (i.e., have status 0).
	 */
	AliLogDiHadronPID::Debug3(Form("Called."), __func__, ClassName());
	
	Int_t nFailedFits = 0;
	for (Int_t iPtBin = 1; iPtBin <= fPtAxis->GetNbins(); iPtBin++) {
		for (Int_t iEtaBin = 1; iEtaBin <= fEtaAxis->GetNbins(); iEtaBin++) {
				
			if (fFitResults->GetAt(iPtBin - 1, iEtaBin - 1)) {
				AliTOFTPCFitContainerDiHadronPID* container = (AliTOFTPCFitContainerDiHadronPID*)(fFitResults->GetAt(iPtBin - 1, iEtaBin - 1));			
				if (container->GetCurrentFitStatus()) {
					nFailedFits++;	
				}
			} else {
				AliLogDiHadronPID::Warning(Form("This Spectrum seems to have a bin with a missing fit container."), __func__, ClassName());
				
			}
		}	
	}

	return nFailedFits;

}
