// -------------------------------------------------------
//  Correct spectra.
// -------------------------------------------------------

// const char* trackcutsname = "FilterMask4";	
const char* trackcutsname = "FilterMask0";
// const char* trackcutsname = "FilterMask5_kTOFout_kTIME";

const Bool_t normalizationScaling = kTRUE;
const Bool_t vertexcorrection = kTRUE;

void CorrectSpectrappLow(const char* rawspectra="../PIDwithTOF/RawTOFspectrapp_low.root", 
					const char* efficiency="../Efficiency/efficiency_low.root", 
					const char* secondaries="../SecondariesCorrection/secondaries_low.root", 
					const char* comparison="Barbara_io.root",
					const char* outputfile="../PIDwithTOF/CorrTOFspectra_low.root") {

	// Load histtools
	gROOT->LoadMacro("../../Libs/AliHistToolsDiHadronPID.cxx+g");

	// Load the raw spectra.
	TFile* frawspectra = TFile::Open(rawspectra);
	if (!frawspectra) {cout<<"Raw Spectra file not found..."<<endl;return;}
	TList* keys_rawspectra = frawspectra->GetListOfKeys();
	// keys_rawspectra->Print();

	// Load the efficiency.
	TFile* fefficiency = TFile::Open(efficiency);
	if (!fefficiency) {cout<<"Efficiency file not found..."<<endl;return;}
	TList* list_efficiency = fefficiency->Get(Form(trackcutsname));
	// list_efficiency->Print();

	// Load the secondaries.
	TFile* fsecondaries = TFile::Open(secondaries);
	if (!fsecondaries) {cout<<"Secondaries file not found..."<<endl;return;}
	TList* list_secondaries = fsecondaries->Get(Form("FilterMask0"));
	//list_secondaries->Print();

	TFile* fcomparison = TFile::Open(comparison);
	if (!fcomparison) {cout<<"Comparison file not found..."<<endl;return;}

	// TH1F* temphist = (TH1F*) list_efficiency->FindObject("EffTrackingPosProton");

	// Create the output file.
	TFile* fcorrspectra = new TFile(outputfile,"recreate");

	const char* histclasses[6] = {"PosPion","NegPion","PosKaon","NegKaon","PosProton","NegProton"};
	const char* rawclasses[6] = {"piSpectrumTOF_Pos","piSpectrumTOF_Neg","kaSpectrumTOF_Pos","kaSpectrumTOF_Neg",
									  "prSpectrumTOF_Pos","prSpectrumTOF_Neg"};
	const char* barbaraclasses[6] = {"pion_plus", "pion_minus", "kaon_plus", "kaon_minus", "proton_plus", "proton_minus"};


	TList* outputlist = new TList();
	outputlist->SetOwner(kTRUE);
	outputlist->SetName("CorrectedResult");

	const TArrayD* xaxis;
	Int_t Nbins;

	// Loop over the keys of the raw spectra file to find the correct spectra
	for (Int_t ii = 0; ii < 6; ii++) {
		
		fCanvas = TCanvas::MakeDefCanvas();

		cout<<"Correction spectra for "<<histclasses[ii]<<"..."<<endl;

		TH1F* rawspec = (TH1F*) frawspectra->Get(Form(rawclasses[ii]));
		rawspec->SetDirectory(0);
	
		// dont do primaries for kaons	
		if ((ii != 2) && (ii != 3)) {
			// note that we take the correction after the DCA cut
			TH1F* primfrac = (TH1F*)((TH1F*)list_secondaries->FindObject(Form("FractionPrim%s",histclasses[ii])))->Clone();
			primfrac->SetTitle(Form("Secondaries %s",histclasses[ii]));
			primfrac->SetName(Form("%sSecondaries",histclasses[ii]));
			primfrac->SetMarkerStyle(2);
			primfrac->SetMarkerSize(0.5);

			xaxis = primfrac->GetXaxis()->GetXbins();
			Nbins = primfrac->GetNbinsX();
		}

		TH1F* trackeff = (TH1F*)((TH1F*)list_efficiency->FindObject(Form("EffTracking%s",histclasses[ii])))->Clone();
		trackeff->SetName(Form("%sTracking",histclasses[ii]));

		TH1F* corrspec = AliHistToolsDiHadronPID::RebinVariableBinning(rawspec,xaxis->GetArray(),Nbins);
		TH1F* trackeffrebin = AliHistToolsDiHadronPID::RebinVariableBinning(trackeff,xaxis->GetArray(),Nbins);
		corrspec->SetName(Form("%sCorrected",histclasses[ii]));
		corrspec->SetTitle(Form("%sCorrected",histclasses[ii]));

		// corrspec->Draw();
		if ((ii != 2) && (ii != 3)) {
			corrspec->Multiply(primfrac);
		}
		corrspec->Divide(trackeffrebin);
		if (normalizationScaling) corrspec->Scale(0.883); // normalize for inelastic.
		if (vertexcorrection) corrspec->Scale(1.055);
		corrspec->Draw();

		TH1F* compar = (TH1F*)fcomparison->Get(Form("TOFBG_%s",barbaraclasses[ii]));
		compar->SetName(Form("%sBarbara",histclasses[ii]));
		compar->SetTitle(Form("Barbara histogram %s",histclasses[ii]));

		//if (!compar) {cout<<"no comparison histo found"<<endl;}
		TH1F* comparrebin = AliHistToolsDiHadronPID::RebinVariableBinning(compar,xaxis->GetArray(),Nbins);
		TH1F* difference = corrspec->Clone();
		difference->Divide(comparrebin);
		difference->SetName(Form("%sDifference",histclasses[ii]));
		difference->SetTitle("Difference = Corrected / Barbara");

		// NOTE: for now correct with primfrac, not with the DCA cut to make it easier.

		fCanvas->SaveAs(Form("Corrected%s.pdf",histclasses[ii]));
		fCanvas->Close();

		outputlist->Add(rawspec);
		outputlist->Add(trackeff);
		outputlist->Add(trackeffrebin);
		outputlist->Add(primfrac);
		outputlist->Add(corrspec);
		outputlist->Add(compar);
		outputlist->Add(comparrebin);
		outputlist->Add(difference);

	}

	outputlist->Write(0,TObject::kSingleKey);

}

		// OLD
		// Find it in the efficiency list
		// for(Int_t jj = 0; jj < list_efficiency->GetSize(); jj++) {
		// 	TString* list_string = new TString(list_efficiency->At(jj)->GetName());
		// 	if (list_string->EqualTo(Form("EffTracking%s",histclasses[ii]))) {
		// 		TH1F* trackeff = list_efficiency->At(jj);
		// 		// cout<<trackeff->GetName()<<endl;
		// 		trackeff->Draw();
		// 	}
		// 	if (list_string->EqualTo(Form("PrimRecFraction%s",histclasses[ii]))) {
		// 		TH1F* primfrac = list_efficiency->At(jj);
		// 	}
		// }