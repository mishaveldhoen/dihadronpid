#!/usr/bin/python
# -----------------------------------------------------------------------------
#  Updates the Di-Hadron PID project in the local AliRoot trunk from the 
#  master branch of the project.
#
#  TODO:
#   1) Add flag to tell the macro to fix problems itself.
#   2) Check not only if environment variables are set, but also if the
#      directory they point to exists.
# -----------------------------------------------------------------------------
#  Misha Veldhoen (misha.veldhoen@cern.ch)

import os
import sys
import argparse
import string
import subprocess

# Define function OK, WARNING and ERROR.
def OK():
	print '\033[92m' + "OK" + '\033[0m'
	return

gWarnings = 0
def WARNING(message = "", askforconfirmation = 0):	
	print '\033[93m' + "WARNING" + '\033[0m'
	if (message): print " >>> " + message 
	global gWarnings 
	gWarnings += 1
	if (askforconfirmation):
		inputvar = raw_input(" >>> Do you wish to proceed anyway (y/n)? ")
		if not (inputvar == "y"): sys.exit()

gErrors = 0
def ERROR(message = "", abort = 0):	
	print '\033[91m' + "ERROR" + '\033[0m'
	if (message): print " >>> " + message 
	global gErrors 
	gErrors += 1
	if (abort): sys.exit()

# Retruns the revision of the local or remote repository.
def getRevision(isLocal):
	currentworkingdir = os.getcwd()
	os.chdir(gAliceSrc)
	svninfo = []
	if (isLocal): svninfo = subprocess.check_output(["svn", "info"]).split()
	else: svninfo = subprocess.check_output(["svn", "info", "-r", "HEAD"]).split()
	os.chdir(currentworkingdir)	
	return svninfo[svninfo.index("Revision:") + 1] 

# Returns True when local revision is unmodified.
def isLocalRevisionUnchanged():
	currentworkingdir = os.getcwd()
	os.chdir(gAliceSrc)	
	isUnchanged = (subprocess.check_output(["svn","diff"])=="")
	os.chdir(currentworkingdir)	
	return isUnchanged

# Returns the branch that we're on.
def getBranch():
	currentworkingdir = os.getcwd()
	os.chdir(gDiHadronSrc)
	gitinfo = subprocess.check_output(["git", "branch"]).split()
	os.chdir(currentworkingdir)
	return gitinfo[gitinfo.index("*") + 1]

# Searches filename in path and returns the path. Latestinks are excluded from the list.
def findFile(path, filename):
	result = []
	for root, dirs, files in os.walk(path):
		if filename in files:
			fullname = os.path.join(root, filename)
			if not (os.path.islink(fullname)):
				result.append(fullname)
	return result

# Deal with the user supplied arguments.
gParser = argparse.ArgumentParser(description="Updates the trunk of AliROOT with the code in the master branch.")
gArguments = gParser.parse_args()

# -----------------------------------------------------------------------------
#  Check if the system is OK.
# -----------------------------------------------------------------------------

# Check if the current we're working in the trunk.
gAliceVer = os.getenv("ALICE_VER")
print "Current AliRoot version: {}...".format(gAliceVer),
if os.getenv("ALICE_VER") == "trunk": OK()
else: ERROR(message="You should work in the trunk", abort=0)

# Check if the ALICE_ROOT variable is set. # CHECK IF DIR EXISTS
gAliceSrc = os.getenv("ALICE_ROOT")
print "AliRoot source directory: {}...".format(gAliceSrc),
if (os.getenv("ALICE_ROOT")): OK()  
else: ERROR(message="Check if Dario's script has been sourced.", abort=1)

# Check if the ALICE_ROOT variable is set. # CHECK IF DIR EXISTS
gAliceBuild = os.getenv("ALICE_BUILD")
print "AliRoot build directory: {}...".format(gAliceBuild),
if (os.getenv("ALICE_BUILD")): OK()  
else: ERROR(message="Check if Dario's script has been sourced.", abort=1)

# Check if the local and remote revision of the trunk is the same.
gLocalRev = getRevision(isLocal=1)
gRemoteRev = getRevision(isLocal=0)
print "Latest (remote) revision: {}, local revision: {}...".format(gRemoteRev, gLocalRev),
if (gLocalRev == gRemoteRev): OK()
else: ERROR(message="You should update the local trunk.", abort=0)

# Check if something was changed in the local trunk.
print "Check if local revision is unchanged compared to remote...",
if (isLocalRevisionUnchanged()): OK()
else: WARNING(message="Local revision is different.", askforconfirmation=1)

# Check if the DIHADRON variable is set. # CHECK IF DIR EXISTS
gDiHadronSrc = os.getenv("DIHADRON")
print "DiHadron project directory: {}...".format(gDiHadronSrc),
if (gDiHadronSrc): OK()			# TODO: Should also check existence of the directory!
else: ERROR(message="You should export the environment variable DIHADRON to point to the DiHadron project directory.", abort=1)

# Check if we're on the master branch.
gBranch = getBranch()
print "We're on branch: {}...".format(gBranch),
if (gBranch == "master"): OK()
else: ERROR(message="You have to switch to the master branch.", abort=0)

# Print the number of warnings if applicable.
if (gWarnings):
	print "Found {} warning(s)...".format(gWarnings)

# Print the number of errors if applicable.
if (gErrors): 
	print "Found {} problem(s): aborting...".format(gErrors)
	sys.exit()

# -----------------------------------------------------------------------------
#  Update Libraries.
# -----------------------------------------------------------------------------
gAliceSrcMyLibs = gAliceSrc + "/PWGCF/Correlations/DPhi/DiHadronPID"
gListOfMyLibs = subprocess.check_output(["ls", gAliceSrcMyLibs]).split()

for filename in gListOfMyLibs:
	print filename + "...",

	# Look up the library in the local version of the code.
	matchedfiles = findFile(path = gDiHadronSrc, filename = filename)	# Should be a list of one element.
	if not (len(matchedfiles) == 1):
		if (len(matchedfiles) == 0): ERROR("File not found in local project folder...")
		else: ERROR("Multiple copies of this file found in the local project folder...")
	else: 
		print " is replaced by " + matchedfiles[0],
		subprocess.call(["cp",matchedfiles[0],gAliceSrcMyLibs])
		OK()

# -----------------------------------------------------------------------------
#  Clean up the include directory.
# -----------------------------------------------------------------------------
gAliceSrcIncludeDir = gAliceSrc + "/include"

for filename in gListOfMyLibs:		# A bit sloppy, since the list also includes cxx files.
	matchedfiles = findFile(path = gAliceSrcIncludeDir, filename = filename)
	if not (len(matchedfiles) == 0):
		filetoremove = gAliceSrcIncludeDir + "/" + filename
		print "Removing " + gAliceSrcIncludeDir + "/" + filename
		subprocess.call(["rm","-f",filetoremove])

# -----------------------------------------------------------------------------
#  Update Macros.
# -----------------------------------------------------------------------------
gAliceSrcMyMacros = gAliceSrc + "/PWGCF/Correlations/macros/DiHadronPID"
gListOfMyMacros = subprocess.check_output(["ls", gAliceSrcMyMacros]).split()

for filename in gListOfMyMacros:
	print filename + "...",

	# Look up the library in the local version of the code.
	matchedfiles = findFile(path = gDiHadronSrc, filename = filename)	# Should be a list of one element.
	if not (len(matchedfiles) == 1):
		if (len(matchedfiles) == 0): ERROR("File not found in local project folder...")
		else: ERROR("Multiple copies of this file found in the local project folder...")
	else: 
		print " is replaced by " + matchedfiles[0],
		subprocess.call(["cp","-f",matchedfiles[0],gAliceSrcMyMacros])
		OK()

# -----------------------------------------------------------------------------
#  Try to recompile AliRoot. (Should be optional!)
# -----------------------------------------------------------------------------
cwd = os.getcwd()
os.chdir(gAliceBuild)
numberofcores = os.getenv("MJ")
subprocess.call(["make", "-j",numberofcores])
os.chdir(cwd)

# -----------------------------------------------------------------------------
#  Export a diff file.
# -----------------------------------------------------------------------------
cwd = os.getcwd()
os.chdir(gAliceSrc)
diff = subprocess.check_output(["svn", "diff"])
os.chdir(cwd)

print "Writing DiHadronPID.diff"
fout = open("DiHadronPID.diff","w")
fout.write(diff)



