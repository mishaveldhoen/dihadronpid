// -----------------------------------------------------------------------
//  Load histograms into a AliSpectrumDiHadronPID instance, and save to
//  a file. 
// -----------------------------------------------------------------------

// TString datafilename = "/Users/philip/Dropbox/Thesis/DiHadronPID/AnalysisResults_pp_7TeV_AOD057_train_all.root"; 	// 150M 7TeV LHC10d+e events

// With standard cuts.
// TString datafilename = "/Users/philip/Dropbox/Thesis/DiHadronPID/RawSpectra_AOD057_All_pp.root"; 
// TString mcfilename = "/Users/philip/Dropbox/Thesis/DiHadronPID/MCSpectra_AOD041_All_pp.root"; 
// const char* eventcutsname = "EventCuts";
// const char* trackcutsname = "FilterMask5_kTOFout_kTIME_maxrap_0.5";

// With marek's cuts
// TString datafilename = "/Users/philip/Dropbox/Thesis/DiHadronPID/RawSpectra_AOD057_All_pp_MarekCuts.root"; 
// TString mcfilename = "/Users/philip/Dropbox/Thesis/DiHadronPID/MCSpectra_AOD041_All_pp_MarekCuts.root"; 
// const char* eventcutsname = "EventCuts";
// const char* trackcutsname = "FilterMask0";

// LHC10e as Andrei uses this:
TString datafilename = "/Users/philip/Dropbox/Thesis/DiHadronPID/RawSpectra_pp_AOD057_LHC10e.root"; 
TString mcfilename = "/Users/philip/Dropbox/Thesis/DiHadronPID/MCSpectra_pp_AOD060_LHC10e.root"; 
const char* eventcutsname = "EventCuts";
const char* trackcutsname = "FilterMask5_kTOFout_kTIME_maxrap_0.5";
const char* secondariestrackcutsname = "FilterMask4";

const char* suffix[3] = {"PosPion", "PosKaon", "PosProton"}; 

// also correct and output the following species.
Bool_t* correctspecies[3] = {1,1,1};
Bool_t secondariesfromfits = kTRUE;

AliAODEventCutsDiHadronPID* eventcuts;
AliAODTrackCutsDiHadronPID* cuts;
AliAODTrackCutsDiHadronPID* mccuts;
AliAODTrackCutsDiHadronPID* secondariescuts;
AliAODTrackCutsDiHadronPID* secondariesmccuts;

TAxis* fptaxis;

CreateSpectrappPos() {

	// load tools/
	gROOT->LoadMacro("../../Libs/AliHistToolsDiHadronPID.cxx+g");	

	// Retrieving the output file and list.
	TFile* fin = TFile::Open(datafilename.Data());
	if (!fin) {
		cout<<"Error: File " << datafilename.Data() << " not found!"<<endl;
		return;
	}
	TList* lin = fin->Get("CompareAODTrackCuts");

	 // Retrieving the MC file and list.
	TFile* finmc = TFile::Open(mcfilename.Data());
	if (!finmc) {
		cout<<"Error: File " << datafilename.Data() << " not found!"<<endl;
		return;
	}
	TList* linmc = finmc->Get("CompareAODTrackCuts");	
 	
 	// Get the cuts for Data and MC.
	eventcuts = (AliAODEventCutsDiHadronPID*)lin->FindObject(eventcutsname);
	cuts = (AliAODTrackCutsDiHadronPID*)lin->FindObject(trackcutsname);
	mccuts = (AliAODTrackCutsDiHadronPID*)linmc->FindObject(trackcutsname);	
	secondariescuts = (AliAODTrackCutsDiHadronPID*)lin->FindObject(secondariestrackcutsname);
	secondariesmccuts = (AliAODTrackCutsDiHadronPID*)linmc->FindObject(secondariestrackcutsname);	

	// Get the number of accepted events needed to normalize the spectra.
	Double_t Naccepted = (Double_t)eventcuts->GetNAcceptedEvents();
	cout << "Accepted events: "<< Naccepted<< endl; 

	// Making spectra from the cuts object.
	fptaxis = new TAxis(cuts->GetNPtBinsPID(), cuts->GetPtAxisPID());

	for (Int_t iSpecies = 0; iSpecies < 1; iSpecies++) {

		// if (iSpecies == 1) {continue;} // dont do kaons for now.

		TFile* fout = new TFile(Form("RawSpectrapp_%i_Pos.root",iSpecies),"recreate");

		cuts->SetDebugLevel(0);
		TObjArray* PIDHistos = cuts->GetDataTOFProjection(1,iSpecies);
		TObjArray* MismatchHistos = cuts->GetDataTOFMismatch(1,iSpecies);

		AliSpectrumDiHadronPID* spectrum = new AliSpectrumDiHadronPID(Form("spectrumTOF_%i",iSpecies), 
			Form("spectrumTOF_%i",iSpecies), iSpecies, "TOF", kFALSE );
		
		spectrum->SetDebugLevel(0);
		spectrum->ImportPIDHistograms(PIDHistos, fptaxis);
		spectrum->ImportMismatchHistograms(MismatchHistos);
		spectrum->Write();

		if (correctspecies[iSpecies]) {
			// fit with spectrum strategy and vary the fit.
			spectrum->ImportEfficiency(GetEfficiencyForSpecies(iSpecies));
			spectrum->FitBins(3,33,0); // Kaons!
			// spectrum->DetermineSystematicsForAllBins();
			spectrum->CreateCorrectedSpectrum();

			// write the corrected spectra to file.
			spectrum->WriteToFile(Form("CorrSpectrapp_%i_Pos.root",iSpecies));
		}

		fout->Close();

	}

	fout->Close();

}

// -------------------------------------------------------
TH1F* GetEfficiencyForSpecies(Int_t iSpecies) {

	TH1F* primrec = (TH1F*)mccuts->GetHistPrimRecMC(Form("fHistPrimRecMCPt%s",suffix[iSpecies]));
	TH1F* primgen = (TH1F*)mccuts->GetHistPrimGenMC(Form("fHistPrimGenMCPt%s",suffix[iSpecies]));
	TH1F* secrec = (TH1F*)mccuts->GetHistSecRecMC(Form("fHistSecRecMCPt%s",suffix[iSpecies]));

	// Tracking Efficiency
	TH1F* trackingeff = (TH1F*)primrec->Clone();
	trackingeff->Sumw2();
	trackingeff->SetDirectory(0);
	trackingeff->Divide(primgen);
	TH1F* trackingeffrebin = AliHistToolsDiHadronPID::RebinVariableBinning(trackingeff, fptaxis, kTRUE);
	trackingeffrebin->SetTitle(Form("Tracking efficiency %s",suffix[iSpecies]));
	trackingeffrebin->GetYaxis()->SetRangeUser(0.0,0.6);
	trackingeffrebin->GetYaxis()->SetTitle("#epsilon_{tracking}");	
	TCanvas* cvstracking = TCanvas::MakeDefCanvas();
	trackingeffrebin->Draw();
	cvstracking->SaveAs(Form("TrackingEfficiency%s.pdf",suffix[iSpecies]));
	delete cvstracking;

	// Fraction of primaries (reconstructed) (binomial errors, since a track is either primary or secondary)
	if (iSpecies != 1) {
		if (secondariesfromfits) {
			TH1F* primfractionrec = GetPrimariesForSpecies(iSpecies);
		} else {
			TH1F* primfractionrec = (TH1F*)primrec->Clone(Form("primfractionrec",suffix[iSpecies]));
			primfractionrec->Sumw2();
			primfractionrec->SetDirectory(0);
			TH1F* totrec = primrec->Clone();
			totrec->Add(secrec);
			totrec->Sumw2();
			primfractionrec->Divide(primfractionrec,totrec,1,1,"B");
		}
		primfractionrecrebin = AliHistToolsDiHadronPID::RebinVariableBinning(primfractionrec, fptaxis, kTRUE);
		primfractionrecrebin->SetName(Form("PrimRecFraction%s",suffix[iSpecies]));
		primfractionrecrebin->SetTitle("Primaries efficiency");
	}

	// Total Efficiency = trackingeff / primfractionrec --> because later we divide by this efficiency to get the total result.
	TH1F* hout = (TH1F*)trackingeffrebin->Clone();
	if (iSpecies != 1) {hout->Divide(primfractionrecrebin);}
	hout->SetName(Form("Efficiency%s",suffix[iSpecies]));
	hout->SetTitle(Form("Total efficiency %s",suffix[iSpecies]));
	hout->GetYaxis()->SetTitle("#epsilon_{tracking} * #epsilon_{primaries}");

	TCanvas* cvsbla = new TCanvas(Form("corrections_%s",suffix[iSpecies]),Form("Corrections %s",suffix[iSpecies]),1300,400); 
	cvsbla->Divide(3,1);
	cvsbla->cd(1);	trackingeffrebin->Draw();
	if (iSpecies != 1) {cvsbla->cd(2);	primfractionrecrebin->Draw();}
	cvsbla->cd(3);	hout->Draw();
	cvsbla->Update();
	cvsbla->SaveAs(Form("Corrections_%s.pdf",suffix[iSpecies]));
	cvsbla->Close();
	delete cvsbla;

	return hout;

}

// -------------------------------------------------------
TH1F* GetPrimariesForSpecies(Int_t iSpecies) {

	const Bool_t debug = kFALSE;
	const Double_t ptAxis[47] = {0.20,0.25,0.30,0.35,0.40,0.45,0.50,0.55,0.60,0.65,0.70,0.75,0.80,0.85,0.90,0.95,1.00,
							1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3.0,
							3.2,3.4,3.6,3.8,4.0,4.2,4.4,4.6,4.8,5.0};
	const Int_t nPtBins = 46;

	// Create histograms used for fitting.
	TH1F* fractionPrim = new TH1F(Form("FractionPrim%s",suffix[iSpecies]), Form("Fraction Primaries %s; p_{T} (GeV/c); fraction",suffix[iSpecies]), nPtBins,ptAxis);
	TH1F* fractionSec = new TH1F(Form("FractionSec%s",suffix[iSpecies]), Form("Fraction Secondaries %s; p_{T} (GeV/c); fraction",suffix[iSpecies]), nPtBins,ptAxis);
	TH1F* fractionWeak = new TH1F(Form("FractionWeak%s",suffix[iSpecies]), Form("Fraction Weak %s; p_{T} (GeV/c); fraction",suffix[iSpecies]), nPtBins,ptAxis);
	TH1F* fractionMat = new TH1F(Form("FractionMat%s",suffix[iSpecies]), Form("Fraction Material %s; p_{T} (GeV/c); fraction",suffix[iSpecies]), nPtBins,ptAxis);
	TH1F* fractionPrimDCACut = new TH1F(Form("FractionPrim%sDCACut",suffix[iSpecies]), Form("Fraction After Cut %s; p_{T} (GeV/c); fraction",suffix[iSpecies]), nPtBins,ptAxis);

	fractionPrim->SetMarkerStyle(21);
	fractionPrim->SetMarkerSize(1);
	fractionSec->SetMarkerStyle(21);
	fractionSec->SetMarkerSize(1);
	fractionWeak->SetMarkerStyle(21);
	fractionWeak->SetMarkerSize(1);
	fractionMat->SetMarkerStyle(21);
	fractionMat->SetMarkerSize(1);
	fractionPrimDCACut->SetMarkerStyle(21);
	fractionPrimDCACut->SetMarkerSize(1);

	// Loop over pT.
	for (Int_t iPtBin = 5; iPtBin < 34; iPtBin ++) {

		Double_t frac[3] = {0,0,0};
		Double_t err[3] = {0,0,0};

		// project out the
		mcPrimProjection = ((TH2F*) secondariesmccuts->GetHistPrimRecMC(Form("fHistPrimRecDCA%s",suffix[iSpecies])))->ProjectionY("_py", iPtBin, iPtBin);
		mcWeakProjection = ((TH2F*) secondariesmccuts->GetHistSecRecMC(Form("fHistSecRecDCAWeak%s",suffix[iSpecies])))->ProjectionY("_py", iPtBin, iPtBin);
		mcMatProjection = ((TH2F*) secondariesmccuts->GetHistSecRecMC(Form("fHistSecRecDCAMat%s",suffix[iSpecies])))->ProjectionY("_py", iPtBin, iPtBin);
		dataProjection = ((TH2F*) secondariescuts->GetHistData(Form("fHistDataDCAxyOneSigma%s",suffix[iSpecies])))->ProjectionY("_py",iPtBin, iPtBin);

		// Check for empty histogram TODO DOESNT WORK!
		if ((!mcPrimProjection) || (!mcWeakProjection) || (!mcMatProjection)) {
			cout<<"EMPTY HISTOGRAM!!"<<endl;
			return 0x0;
		}

		// Scale input mc and data histograms for better fits
		Double_t dataIntegral = dataProjection->Integral();
		Double_t mcIntegral = mcPrimProjection->Integral() + mcWeakProjection->Integral() + mcMatProjection->Integral();
		mcPrimProjection->Scale(dataIntegral / mcIntegral);
		mcWeakProjection->Scale(dataIntegral / mcIntegral);
		mcMatProjection->Scale(dataIntegral / mcIntegral);

		// Define fit output prediction histograms
		TH1D* mcPrimPrediction = mcPrimProjection;
		TH1D* mcWeakPrediction = mcWeakProjection;
		TH1D* mcMatPrediction = mcMatProjection;

		// Put MC histos in array for fitter object
		TObjArray* mcArray = new TObjArray(3);
		mcArray->Add(mcPrimProjection);
		mcArray->Add(mcWeakProjection);
		mcArray->Add(mcMatProjection);

		// Create fitter object and fit
		TFractionFitter* fit = new TFractionFitter(dataProjection, mcArray, "Q"); // Quiet mode

		// Constraints
		fit->Constrain(1,0.00000001,1.);
		fit->Constrain(2,0.00000001,1.);
		fit->Constrain(3,0.00000001,1.);
		// if (iHistoClass == 10 || iHistoClass == 11) {fit->Constrain(3,0.0,0.0000001);}
		fit->SetRangeX(1,300);

		// Fit
		Int_t status = fit->Fit();
		cout << "fit status: " << status << endl;
		if (status == 0) {
			// Get fractions and errors
			fit->GetResult(0, frac[0], err[0]);
			fit->GetResult(1, frac[1], err[1]);
			fit->GetResult(2, frac[2], err[2]);

			if( debug && iSpecies == 0) {
				fCanvas = TCanvas::MakeDefCanvas();
				fCanvas->SetName(Form("Fit%sBin%d",suffix[iSpecies],iPtBin));
				fCanvas->SetTitle(Form("Fit Result %s Bin %d",suffix[iSpecies], iPtBin));
				fCanvas->SetLogy();
				dataProjection->SetMarkerStyle(20);
				dataProjection->Draw("eP");

				TH1D* fitResult = (TH1D*) fit->GetPlot();
				fitResult->SetLineColor(9);
				fitResult->Draw("same");

				TH1D* mcPrimPrediction = (TH1D*) fit->GetMCPrediction(0);
				mcPrimPrediction->SetLineColor(2);
				mcPrimPrediction->Draw("same");
				TH1D* mcWeakPrediction = (TH1D*) fit->GetMCPrediction(1);
				mcWeakPrediction->SetLineColor(7);
				mcWeakPrediction->Draw("same");
				TH1D* mcMatPrediction = (TH1D*) fit->GetMCPrediction(2);
				mcMatPrediction->SetLineColor(3);
				mcMatPrediction->Draw("same");

				mcPrimProjection->SetLineColor(2);
				//mcPrimProjection->Draw("same");

				mcWeakProjection->SetLineColor(7);
				// mcWeakProjection->Draw("same");

				mcMatProjection->SetLineColor(3);
				//mcMatProjection->Draw("same");

				TLegend *leg = new TLegend(0.12,0.72,0.33,0.89);
				leg->SetFillColor(kWhite);
				leg->AddEntry(dataProjection,"Data","p");
				leg->AddEntry(fitResult,"Fit","l");
				leg->AddEntry(mcPrimPrediction,"Primaries","l");
				leg->AddEntry(mcWeakPrediction,"Weak decay","l");
				leg->AddEntry(mcMatPrediction,"Material","l");
				leg->Draw();
				leg->Paint();

				fCanvas->SaveAs(Form("%s_Fit_PtBin%d.pdf",suffix[iSpecies],iPtBin));
				fCanvas->Close();
			}

			// Get resulting fit plot too for pt Bin 7 (0.5 to 0.55 GeV)
			if (debug && iPtBin == 17 /*&& iSpecies == 5*/) {

				fCanvas = TCanvas::MakeDefCanvas();
				fCanvas->SetName(Form("Fit%s",suffix[iSpecies]));
				fCanvas->SetTitle(Form("Fit Result %s",suffix[iSpecies]));
				fCanvas->SetLogy();

				// Smooth
				// dataProjection->Smooth();
				// mcPrimProjection->Smooth();
				// mcWeakProjection->Smooth();
				// mcMatProjection->Smooth();

				dataProjection->SetMarkerStyle(20);
				dataProjection->Draw("eP");

				TH1D* fitResult = (TH1D*) fit->GetPlot();
				fitResult->SetLineColor(9);
				fitResult->Draw("same");
				TH1D* mcPrimPrediction = (TH1D*) fit->GetMCPrediction(0);
				mcPrimPrediction->SetLineColor(2);
				mcPrimPrediction->Draw("same");
				TH1D* mcWeakPrediction = (TH1D*) fit->GetMCPrediction(1);
				mcWeakPrediction->SetLineColor(7);
				mcWeakPrediction->Draw("same");
				TH1D* mcMatPrediction = (TH1D*) fit->GetMCPrediction(2);
				mcMatPrediction->SetLineColor(3);
				mcMatPrediction->Draw("same");

				TLegend *leg = new TLegend(0.12,0.72,0.33,0.89);
				leg->SetFillColor(kWhite);
				leg->AddEntry(dataProjection,"Data","p");
				leg->AddEntry(fitResult,"Fit","l");
				leg->AddEntry(mcPrimPrediction,"Primaries","l");
				leg->AddEntry(mcWeakPrediction,"Weak decay","l");
				leg->AddEntry(mcMatPrediction,"Material","l");
				leg->Draw();
				leg->Paint();

				fCanvas->SaveAs(Form("Fit_PtBin%d_%s.pdf",iPtBin,suffix[iSpecies]));
				fCanvas->Close();
			}
			cout<<"Fit for "<<suffix[iSpecies]<<" bin "<<iPtBin<<": Prim "<<frac[0]<<"; weak "<<frac[1]<<"; Mat "<<frac[2]<<"; Chisquared "<<fit->GetChisquare()<<endl;

		}
		fit->Delete();

		// Fill primary fractions histogram and secondary fractions from weak and material
		fractionPrim->SetBinContent(iPtBin + 1, frac[0]);
		fractionPrim->SetBinError(iPtBin + 1, err[0]);
		fractionSec->SetBinContent(iPtBin + 1, frac[1] + frac[2]);
		fractionSec->SetBinError(iPtBin + 1, err[1] + err[2]);
		fractionWeak->SetBinContent(iPtBin + 1, frac[1]);
		fractionWeak->SetBinError(iPtBin + 1, err[1]);
		fractionMat->SetBinContent(iPtBin + 1, frac[2]);
		fractionMat->SetBinError(iPtBin + 1, err[2]);

		// Calculate fraction AFTER DCA cut;
		Double_t DCACut = 7.*(2.6 + 5. / (TMath::Power((Double_t)ptAxis[iPtBin],1.01)))* 0.001;
		// cout<<"7 sigma standard DCA cut in for pt "<<ptAxis[iPtBin]<<": "<<DCACut<<endl;
		Int_t lowerdcabin = (Int_t) TMath::Floor((3. - DCACut) / 0.02);
		Int_t upperdcabin = (Int_t) TMath::Ceil((3. + DCACut) / 0.02);
		// cout<<"DCA bins low: "<<lowerdcabin<<"; up: "<<upperdcabin<<endl;

		// Errors for integral
		Double_t mcPrimPredictionError;
		Double_t mcWeakPredictionError;
		Double_t mcMatPredictionError;

		// Calculate fraction after DCA cut and error
		Double_t fractionAfterDCACut = mcPrimPrediction->Integral(lowerdcabin, upperdcabin) / (mcPrimPrediction->IntegralAndError(lowerdcabin, upperdcabin,mcPrimPredictionError) + mcWeakPrediction->IntegralAndError(lowerdcabin, upperdcabin, mcWeakPredictionError) + mcMatPrediction->IntegralAndError(lowerdcabin, upperdcabin, mcMatPredictionError));
		Double_t errorFractionAfterDCACut = fractionAfterDCACut * 
		TMath::Sqrt( 
			TMath::Power(mcPrimPredictionError / mcPrimPrediction->Integral(lowerdcabin, upperdcabin),2) 
			+ TMath::Power( TMath::Sqrt(TMath::Power(mcPrimPredictionError,2) + TMath::Power(mcWeakPredictionError,2) + TMath::Power(mcMatPredictionError,2)) 
				/ (mcPrimPrediction->Integral(lowerdcabin, upperdcabin) + mcWeakPrediction->Integral(lowerdcabin, upperdcabin) + mcMatPrediction->Integral(lowerdcabin, upperdcabin)) ,2) );
		cout<<"Fraction prim after cut: "<<fractionAfterDCACut<<"; error: "<<errorFractionAfterDCACut<<";"<<endl;

		if (status == 0) {
			fractionPrimDCACut->SetMarkerStyle(21);
			fractionPrimDCACut->SetMarkerSize(1);
			fractionPrimDCACut->SetBinContent(iPtBin + 1, fractionAfterDCACut);
			fractionPrimDCACut->SetBinError(iPtBin + 1, err[0] * mcPrimPrediction->Integral(lowerdcabin, upperdcabin) / mcPrimPrediction->Integral() ); // TODO: fix error propagation
		}
	}	

	fractionPrimDCACut->SetTitle(Form("Primaries fraction %s",suffix[iSpecies]));
	fractionPrimDCACut->GetYaxis()->SetRangeUser(0.8,1.0);
	fractionPrimDCACut->GetYaxis()->SetTitle("Fraction of primaries");	
	TCanvas* cvsdcacut = TCanvas::MakeDefCanvas();
	fractionPrimDCACut->Draw();
	cvsdcacut->SaveAs(Form("SecondariesFraction%s.pdf",suffix[iSpecies]));
	delete cvsdcacut;

	return fractionPrimDCACut;
}