

DetermineBeta() {
	
	TFile* fin[3] = {0x0};
	AliSpectrumDiHadronPID* spec[3] = {0x0};
	TObjArray* parhistos[3] = {0x0};
	TH1F* sigma[3] = {0x0};
	TH1F* tail[3] = {0x0};
	TH1F* beta[3] = {0x0};
	const TString name[3] = {"pion","kaon","proton"};
	const Int_t color[3] = {kBlack,kBlue,kBlack};

	TCanvas::MakeDefCanvas();

	for (Int_t iSpecies = 0; iSpecies < 1; iSpecies++) {

		fin[iSpecies] = TFile::Open(Form("RawSpectraDone_%i.root"), iSpecies);
		fin[iSpecies]->ls();
		spec[iSpecies] = (AliSpectrumDiHadronPID*)((fin[iSpecies])->Get(Form("spectrumTOFTPC_%i",iSpecies)));
		parhistos[iSpecies] = spec[iSpecies]->GetParHistos();
		parhistos[iSpecies]->ls();

		sigma[iSpecies] = (TH1F*)parhistos[iSpecies]->FindObject("sTOFProtons");
		tail[iSpecies] = (TH1F*)parhistos[iSpecies]->FindObject("tailTOFProtons");

		cout<<sigma[iSpecies]<<" "<<tail[iSpecies]<<endl;

		beta[iSpecies] = (TH1F*)sigma[iSpecies]->Clone(Form("beta_%s",name[iSpecies]));
		beta[iSpecies]->SetMinimum(0.);
		beta[iSpecies]->SetMaximum(200.);
		beta[iSpecies]->SetTitle(Form("beta_%s",name[iSpecies]));
		beta[iSpecies]->Multiply(sigma[iSpecies]);
		beta[iSpecies]->Divide(tail[iSpecies]);

		beta[iSpecies]->SetLineColor(color[iSpecies]);
		beta[iSpecies]->SetMarkerColor(color[iSpecies]);

		cout<<beta[iSpecies]<<endl;
		beta[iSpecies]->Inspect();


		if (!iSpecies) {beta[iSpecies]->Draw();}
		else {beta[iSpecies]->Draw("same");}

	}

}
