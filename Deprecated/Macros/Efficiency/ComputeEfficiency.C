// -------------------------------------------------------
//  Computes Efficiency for Spectra. (NOT MAINTAINED)
// -------------------------------------------------------
TList* ComputeEfficiencyForCut(AliAODTrackCutsDiHadronPID* cuts) {

	// Creates a TList with efficiency histograms.
	if (!cuts) return 0x0;
	TList* lijst = new TList();
	lijst->SetOwner(kTRUE);
	lijst->SetName(cuts->GetName());

	const char* suffix[6] = {"PosPion","NegPion","PosKaon","NegKaon","PosProton","NegProton"};

	// Loop over all species.
	for (Int_t ii = 0; ii < 6; ii++) {

		TH1F* primrec = (TH1F*)cuts->GetHistPrimRecMC(Form("fHistPrimRecMCPt%s",suffix[ii]));
		TH1F* primgen = (TH1F*)cuts->GetHistPrimGenMC(Form("fHistPrimGenMCPt%s",suffix[ii]));
		TH1F* secrec = (TH1F*)cuts->GetHistSecRecMC(Form("fHistSecRecMCPt%s",suffix[ii]));
		TH1F* secgen = (TH1F*)cuts->GetHistSecGenMC(Form("fHistSecGenMCPt%s",suffix[ii]));

		if (!primrec||!primgen||!secrec||!secgen) continue;

		// Tracking Efficiency
		TH1F* efftracking = (TH1F*)primrec->Clone();
		efftracking->Sumw2();
		efftracking->SetDirectory(0);
		efftracking->SetName(Form("EffTracking%s",suffix[ii]));
		efftracking->SetTitle(Form("Tracking efficiency %s",suffix[ii]));
		efftracking->GetYaxis()->SetTitle("#epsilon");
		efftracking->Divide(primgen);
		lijst->Add(efftracking);

		// Fraction of primaries (generator level) (binomial errors, since a track is either primary or secondary)
		TH1F* primfraction = (TH1F*)primgen->Clone();
		primfraction->Sumw2();
		primfraction->SetDirectory(0);
		primfraction->SetName(Form("PrimGenFraction%s",suffix[ii]));
		primfraction->SetTitle(Form("Fraction of Primaries (GL) %s",suffix[ii]));
		TH1F* totgen = primgen->Clone();
		totgen->Add(secgen);
		totgen->Sumw2();
		primfraction->Divide(primfraction,totgen,1,1,"B");	
		lijst->Add(primfraction);

		// Fraction of secondaries (generator level) (binomial errors, since a track is either primary or secondary)
		TH1F* secfraction = (TH1F*)secgen->Clone();
		secfraction->Sumw2();
		secfraction->SetDirectory(0);
		secfraction->SetName(Form("SecGenFraction%s",suffix[ii]));
		secfraction->SetTitle(Form("Fraction of Secondaries (GL) %s",suffix[ii]));
		secfraction->Divide(secfraction,totgen,1,1,"B");	
		lijst->Add(secfraction);

		// Fraction of primaries (reconstructed) (binomial errors, since a track is either primary or secondary)
		TH1F* primfractionrec = (TH1F*)primrec->Clone();
		primfractionrec->Sumw2();
		primfractionrec->SetDirectory(0);
		primfractionrec->SetName(Form("PrimRecFraction%s",suffix[ii]));
		primfractionrec->SetTitle(Form("Fraction of Primaries (Rec) %s",suffix[ii]));
		TH1F* totrec = primrec->Clone();
		totrec->Add(secrec);
		totrec->Sumw2();
		primfractionrec->Divide(primfractionrec,totrec,1,1,"B");	
		lijst->Add(primfractionrec);

		// Fraction of secondaries (reconstructed) (binomial errors, since a track is either primary or secondary)
		TH1F* secfractionrec = (TH1F*)secrec->Clone();
		secfractionrec->Sumw2();
		secfractionrec->SetDirectory(0);
		secfractionrec->SetName(Form("SecRecFraction%s",suffix[ii]));
		secfractionrec->SetTitle(Form("Fraction of Secondaries (Rec) %s",suffix[ii]));
		secfractionrec->Divide(secfractionrec,totrec,1,1,"B");	
		lijst->Add(secfractionrec);

	}

	return lijst;

}

// -------------------------------------------------------
void ComputeEfficiencyAllCuts() {

	// Computes the efficiency of all the track cuts that are
	// found in the MC, as well as primary/secondary fractions
	// on generator level and of reconstructed tracks.

	const char* inputfile="../../AliAnalysisTaskCompareAODTrackCuts/MCSpectra_AOD048_All.root"; 
	const char* outputfile = "efficiency.root";

	TFile* fin = TFile::Open(inputfile);
	if (!fin) return;
	TList* lin = fin->Get("CompareAODTrackCuts");

	TFile* fout = new TFile(outputfile,"recreate");

	// Loop over all objects in the list, and look for track cut objects.
	for (Int_t jj = 0; jj < lin->GetEntries(); jj++) {

		TString* objecttype = new TString(lin->At(jj)->ClassName());

		if (objecttype->EqualTo("AliAODTrackCutsDiHadronPID")) {

			AliAODTrackCutsDiHadronPID* cuts = (AliAODTrackCutsDiHadronPID*)lin->At(jj);

			if (!cuts->GetIsMC()) {cout<<"Cut object does not contain MC histograms..."<<endl;continue;}

			cout<<"Calculating Efficiencies for "<<cuts->GetName()<<endl;
			TList* lijstje = ComputeEfficiencyForCut(cuts);

			fout->cd();
			lijstje->Write(0,TObject::kSingleKey);

		}

		delete objecttype;

	}

	fin->Close();
	fout->Close();

}

// -------------------------------------------------------
void ComputeEfficiencyTrackingMatching(Bool_t rapcut = kTRUE) {

	// Computes tracking and matching efficiencies.
	// Note that to compute the matching efficiency, we will need
	// a histogram with the fraction of mismatches.

	const char* inputfileRawSpectra = "../PIDwithTOF/RawTOFSpectra.root";

	const char* inputfileMCSpectra = "../../AliAnalysisTaskCompareAODTrackCuts/MCSpectra_AOD048_All.root";
	const char* outputfile = "efficiencyAll.root";

	//const char* inputfileMCSpectra="../../AliAnalysisTaskCompareAODTrackCuts/MCSpectra_139510_AOD048_270.root"; 
	//const char* outputfile = "efficiency139510.root";

	TFile* fin = TFile::Open(inputfileMCSpectra);
	TList* lin = fin->Get("CompareAODTrackCuts");

	// Input mismatches. This was computed for the proton fits All/Pos/Neg
	TFile* finRawSpectra = TFile::Open(inputfileRawSpectra);
	TH1F* hMismatch[3];
	TCanvas* cc1 = TCanvas::MakeDefCanvas();
	cc1->Divide(2,1);
	for (Int_t iCharge = 0; iCharge < 3; iCharge++) {
		hMismatch[iCharge] = (TH1F*)finRawSpectra->Get(Form("mismatch_%i",iCharge));
		cc1->cd(1);
		hMismatch[iCharge]->Draw(iCharge?"same":"");
	}

	//finRawSpectra->ls();

	// Compute efficiencies.
	AliAODTrackCutsDiHadronPID* FilterMask5 = lin->FindObject(Form("FilterMask5%s", rapcut ? "_maxrap_0.5" : ""));
	TList* lFilterMask5 = ComputeEfficiencyForCut(FilterMask5);
	//lFilterMask5->ls();

	AliAODTrackCutsDiHadronPID* FilterMask5_kTOFout_kTIME = lin->FindObject(Form("FilterMask5_kTOFout_kTIME%s", rapcut ? "_maxrap_0.5" : ""));
	TList* lFilterMask5_kTOFout_kTIME = ComputeEfficiencyForCut(FilterMask5_kTOFout_kTIME);
	//lFilterMask5_kTOFout_kTIME->ls();

	TH1F* hTrackingEff[2][3];		// [+/-][pi/Ka/pr]
	TH1F* hMatchingEffRaw[2][3];	// == tracking eff with TOF hit / tracking eff without
	TH1F* hMatchingEff[2][3];		// == same, but now multiplied by the fraction of good matches i.e.
									//    from the fits we get Nmismatch/Ntot.

	TString charges[2] = {"Pos","Neg"};
	TString species[3] = {"Pion","Kaon","Proton"};
	TFile* fout = new TFile(outputfile,"recreate");

	TCanvas* cc2 = TCanvas::MakeDefCanvas();
	cc2->Divide(3,2);


	for (Int_t iCharge = 0; iCharge < 2; iCharge++) { 
		for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {

			// Tracking Efficiency.
			//cout<<"ch: "<<iCharge<<" sp: "<<iSpecies<<endl;
			hTrackingEff[iCharge][iSpecies] = (TH1F*)((TH1F*)lFilterMask5->FindObject(Form("EffTracking%s%s",charges[iCharge].Data(),species[iSpecies].Data())))->Clone();
			hTrackingEff[iCharge][iSpecies]->SetNameTitle(Form("hEffTracking%s%s",charges[iCharge].Data(),species[iSpecies].Data()),
				Form("hEffTracking%s%s",charges[iCharge].Data(),species[iSpecies].Data()));
			cc2->cd(iCharge*3 + iSpecies+1);
			hTrackingEff[iCharge][iSpecies]->Draw();
			hTrackingEff[iCharge][iSpecies]->Write();

			// "Raw" Matching Efficiency, i.e, not corrected for mismatch fraction.
			hMatchingEffRaw[iCharge][iSpecies] = (TH1F*)((TH1F*)lFilterMask5_kTOFout_kTIME->FindObject(Form("EffTracking%s%s",charges[iCharge].Data(),species[iSpecies].Data())))->Clone();
			hMatchingEffRaw[iCharge][iSpecies]->Divide(hTrackingEff[iCharge][iSpecies]);
			hMatchingEffRaw[iCharge][iSpecies]->SetNameTitle(Form("hEffMatchingRaw%s%s",charges[iCharge].Data(),species[iSpecies].Data()),
				Form("hEffMatchingRaw%s%s",charges[iCharge].Data(),species[iSpecies].Data()));
			hMatchingEffRaw[iCharge][iSpecies]->SetLineColor(kRed);
			hMatchingEffRaw[iCharge][iSpecies]->SetMarkerColor(kRed);
			hMatchingEffRaw[iCharge][iSpecies]->Draw("same");
			hMatchingEffRaw[iCharge][iSpecies]->Write();

			// First copy the mismatch histogram, and compute (1-mismatch)
			hMatchingEff[iCharge][iSpecies] = (TH1F*)hMismatch[iCharge+1]->Clone();
			hMatchingEff[iCharge][iSpecies]->SetNameTitle(Form("hEffMatching%s%s",charges[iCharge].Data(),species[iSpecies].Data()),
				Form("hEffMatching%s%s",charges[iCharge].Data(),species[iSpecies].Data()));
			AliHistToolsDiHadronPID::ConstMinusHist(hMatchingEff[iCharge][iSpecies],1.);
			
			TH1F* tmp = AliHistToolsDiHadronPID::RebinVariableBinning(hMatchingEffRaw[iCharge][iSpecies],hMismatch[iCharge+1]);
			//hMatchingEff[iCharge][iSpecies] = tmp;
			hMatchingEff[iCharge][iSpecies]->Multiply(tmp);
			delete tmp;
			hMatchingEff[iCharge][iSpecies]->SetLineColor(kGreen);
			hMatchingEff[iCharge][iSpecies]->SetMarkerColor(kGreen);
			hMatchingEff[iCharge][iSpecies]->Draw("same");
			hMatchingEff[iCharge][iSpecies]->Write();

		}
	}

	fout->Close();

}

// -------------------------------------------------------
void GetRobertosEfficiencies() {

	// Takes the files from Roberto, and extracts the 
	// needed histograms. Then Compare those to my own
	// efficiencies.

	TFile* fout = new TFile("Refficiencies.root","recreate");

	// Collect Robertos Tracking Efficiency Histograms.
	TString charges[2] = {"positive","negative"};
	TString species[3] = {"pion","kaon","proton"};

	TFile* 		fTrackingEff[2];
	TCanvas* 	cTrackingEff[2];
	TList*		lTrackingEff[2];
	TH1F*		hTrackingEff[2][3];

	for (Int_t iCharge = 0; iCharge < 2; iCharge++) {
		fTrackingEff[iCharge] = new TFile(Form("../../RobertosHistos/TOF_trackingEfficiency_%s.root",charges[iCharge].Data()),"read");
		fTrackingEff[iCharge]->ls();
		if (!iCharge) {cTrackingEff[iCharge] = (TCanvas*)fTrackingEff[iCharge]->Get("c1");}
		else {cTrackingEff[iCharge] = (TCanvas*)fTrackingEff[iCharge]->Get("c1_n2");}
		lTrackingEff[iCharge] = cTrackingEff[iCharge]->GetListOfPrimitives();
		for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
			//TCanvas::MakeDefCanvas();
			hTrackingEff[iCharge][iSpecies] = (TH1F*)lTrackingEff[iCharge]->FindObject(Form("hTrackingEff_cent0_%s_%s",species[iSpecies].Data(),charges[iCharge].Data()));
			//hTrackingEff[iCharge][iSpecies]->Draw();
			fout->cd();
			hTrackingEff[iCharge][iSpecies]->Write();
		}
	}

	// Collect Robertos Matching Efficiency Histograms.
	TFile* 		fMatchingEff[2];
	TCanvas* 	cMatchingEff[2];
	TList*		lMatchingEff[2];
	TH1F*		hMatchingEff[2][3];

	for (Int_t iCharge = 0; iCharge < 2; iCharge++) {
		fMatchingEff[iCharge] = new TFile(Form("../../RobertosHistos/TOF_matchingEfficiency_%s.root",charges[iCharge].Data()),"read");
		fMatchingEff[iCharge]->ls();
		if (!iCharge) {cMatchingEff[iCharge] = (TCanvas*)fMatchingEff[iCharge]->Get("c1_n3");}
		else {cMatchingEff[iCharge] = (TCanvas*)fMatchingEff[iCharge]->Get("c1_n4");}
		lMatchingEff[iCharge] = cMatchingEff[iCharge]->GetListOfPrimitives();
		for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
			//TCanvas::MakeDefCanvas();
			hMatchingEff[iCharge][iSpecies] = (TH1F*)lMatchingEff[iCharge]->FindObject(Form("hMatchEff_cent0_%s_%s",species[iSpecies].Data(),charges[iCharge].Data()));
			//hMatchingEff[iCharge][iSpecies]->Draw();
			fout->cd();
			hMatchingEff[iCharge][iSpecies]->Write();
		}
	}

	fout->Close();

}

// -------------------------------------------------------
void CompareEfficiencies() {

	// Plots of the comparison with Spectra note.
	// First one has to run:
	//
	//   ComputeEfficiencyTrackingMatching(),
	//   GetRobertosEfficiencies()
	//

	gStyle->SetOptStat(0);

	TFile* finR = new TFile("Refficiencies.root");
	TFile* fin = new TFile("efficiencyAll.root");
	//TFile* fin = new TFile("efficiency139510.root");

	//finR->ls();
	//fin->ls();

	TString chargesR[2] = {"positive","negative"};
	TString speciesR[3] = {"pion","kaon","proton"};

	TString charges[2] = {"Pos","Neg"};
	TString species[3] = {"Pion","Kaon","Proton"};

	TH1F* hTrackingEffR[2][3];	
	TH1F* hMatchingEffR[2][3];
	TH1F* hTrackingEff[2][3];
	TH1F* hMatchingEffRaw[2][3];
	TH1F* hMatchingEff[2][3];

	for (Int_t iCharge = 0; iCharge < 2; iCharge++) { 
		for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {

			hTrackingEffR[iCharge][iSpecies] = (TH1F*)finR->Get(Form("hTrackingEff_cent0_%s_%s",speciesR[iSpecies].Data(),chargesR[iCharge].Data()));
			hMatchingEffR[iCharge][iSpecies] = (TH1F*)finR->Get(Form("hMatchEff_cent0_%s_%s",speciesR[iSpecies].Data(),chargesR[iCharge].Data()));
	
			hTrackingEff[iCharge][iSpecies] = (TH1F*)fin->Get(Form("hEffTracking%s%s",charges[iCharge].Data(),species[iSpecies].Data()));
			hMatchingEffRaw[iCharge][iSpecies] = (TH1F*)fin->Get(Form("hEffMatchingRaw%s%s",charges[iCharge].Data(),species[iSpecies].Data()));			
			hMatchingEff[iCharge][iSpecies] = (TH1F*)fin->Get(Form("hEffMatching%s%s",charges[iCharge].Data(),species[iSpecies].Data()));

		}
	}

	// Tracking.
	TCanvas* c1 = new TCanvas("c1","c1",0,0,1200,800);
	c1->SetTitle("Tracking Efficiency");
	c1->Divide(3,2);
	for (Int_t iCharge = 0; iCharge < 2; iCharge++) {
		for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
			hTrackingEffR[iCharge][iSpecies]->SetMinimum(0.);
			hTrackingEffR[iCharge][iSpecies]->SetMaximum(1.);
			hTrackingEffR[iCharge][iSpecies]->SetTitle(Form("%s %s",speciesR[iSpecies].Data(),chargesR[iCharge].Data()));
			c1->cd(iCharge * 3 + iSpecies + 1);
			hTrackingEffR[iCharge][iSpecies]->Draw();
			hTrackingEff[iCharge][iSpecies]->SetLineColor(kBlack);
			hTrackingEff[iCharge][iSpecies]->SetMarkerColor(kBlack);
			hTrackingEff[iCharge][iSpecies]->SetLineWidth(2);		
			hTrackingEff[iCharge][iSpecies]->Draw("same");
		}
	}

	// Positive matching.
	TCanvas* c3 = new TCanvas("c3","c3",40,40,1200,800);
	c3->SetTitle("Matching Efficiency");
	c3->Divide(3,2);
	for (Int_t iCharge = 0; iCharge < 2; iCharge++) {	
		for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
			hMatchingEffR[iCharge][iSpecies]->SetMinimum(0.);
			hMatchingEffR[iCharge][iSpecies]->SetMaximum(1.);
			hMatchingEffR[iCharge][iSpecies]->SetTitle(Form("%s %s",speciesR[iSpecies].Data(),chargesR[iCharge].Data()));			
			c3->cd(iCharge * 3 + iSpecies + 1);
			hMatchingEffR[iCharge][iSpecies]->Draw();
			hMatchingEff[iCharge][iSpecies]->SetLineColor(kBlack);
			hMatchingEff[iCharge][iSpecies]->SetMarkerColor(kBlack);
			hMatchingEff[iCharge][iSpecies]->SetLineWidth(2);		
			hMatchingEff[iCharge][iSpecies]->Draw("same");
		}
	}

	// Positive matching (raw).
	TCanvas* c5 = new TCanvas("c5","c5",80,80,1200,800);
	c5->SetTitle("Matching Efficiency (no corr).");
		c5->Divide(3,2);
		for (Int_t iCharge = 0; iCharge < 2; iCharge++) {	
		for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
			hMatchingEffR[iCharge][iSpecies]->SetMinimum(0.);
			hMatchingEffR[iCharge][iSpecies]->SetMaximum(1.);		
			c5->cd(iCharge * 3 + iSpecies + 1);
			hMatchingEffR[iCharge][iSpecies]->Draw();
			hMatchingEffRaw[iCharge][iSpecies]->SetLineColor(kBlack);
			hMatchingEffRaw[iCharge][iSpecies]->SetMarkerColor(kBlack);
			hMatchingEffRaw[iCharge][iSpecies]->SetLineWidth(2);		
			hMatchingEffRaw[iCharge][iSpecies]->Draw("same");
		}
	}


}
