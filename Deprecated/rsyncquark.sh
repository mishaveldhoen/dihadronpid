#!/bin/bash

# rsync to quark.
echo "Sending to quark..."
rsync -rvhtm --update --exclude ".DS_Store" --exclude "Templates_*" --exclude "*.xml" --exclude "Libs/build/*" --exclude "*Fits/*" --exclude "AliAnalysisTaskDiHadronPIDMini/old/*" /Users/philip/Dropbox/Thesis/DiHadronPID/Libs /Users/philip/Dropbox/Thesis/DiHadronPID/AliAnalysisTaskDiHadronPIDMini 3632229@quark.science.uu.nl:/home/students/3632229/DiHadronPID

# rsync -rvhtm --update --exclude "*.pdf" /Users/philip/Dropbox/Thesis/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_MC_Spectra/InclusiveFits 3632229@quark.science.uu.nl:/home/students/3632229/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_MC_Spectra/InclusiveFits
# rsync -rvhtm --update --exclude "*.pdf" /Users/philip/Dropbox/Thesis/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_MC_Spectra_FreeTOFTails/InclusiveFits 3632229@quark.science.uu.nl:/home/students/3632229/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_MC_Spectra_FreeTOFTails/InclusiveFits
# rsync -rvhtm --update --exclude "*.pdf" /Users/philip/Dropbox/Thesis/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_MC_NAS2/InclusiveFits 3632229@quark.science.uu.nl:/home/students/3632229/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_MC_NAS/InclusiveFits
# rsync -rvhtm --update --exclude "*.pdf" /Users/philip/Dropbox/Thesis/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_Data_10e/InclusiveFits 3632229@quark.science.uu.nl:/home/students/3632229/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_Data_10e
# rsync -rvhtm --update --exclude "*.pdf" /Users/philip/Dropbox/Thesis/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_Data_10d/InclusiveFits 3632229@quark.science.uu.nl:/home/students/3632229/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_Data_10d
# rsync -rvhtm --update --exclude "*.pdf" /Users/philip/Dropbox/Thesis/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_Data_10d_AOD099/InclusiveFits 3632229@quark.science.uu.nl:/home/students/3632229/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_Data_10d_AOD099
# rsync -rvhtm --update --exclude "*.pdf" /Users/philip/Dropbox/Thesis/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_Data_10e_AOD099/InclusiveFits 3632229@quark.science.uu.nl:/home/students/3632229/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_Data_10e_AOD099
# rsync -rvhtm --update --exclude "*.pdf" /Users/philip/Dropbox/Thesis/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_Data_NAS2/InclusiveFits 3632229@quark.science.uu.nl:/home/students/3632229/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_MC_NAS/InclusiveFits
# rsync -rvhtm --update /Users/philip/Dropbox/Thesis/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_Data_NAS/InclusiveFits 3632229@quark.science.uu.nl:/home/students/3632229/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_Data_NAS
# rsync -rvhtm --update --exclude "*.pdf" /Users/philip/Dropbox/Thesis/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_Data_NAS_FreeTOFTail/InclusiveFits 3632229@quark.science.uu.nl:/home/students/3632229/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_Data_NAS_FreeTOFTail
# rsync -rvhtm --update /Users/philip/Dropbox/Thesis/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_Data_NAS2/InclusiveFits 3632229@quark.science.uu.nl:/home/students/3632229/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_Data_NAS2
# rsync -rvhtm --update --exclude "*.pdf" /Users/philip/Dropbox/Thesis/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_Data_NAS2_FreeTOFTail/InclusiveFits 3632229@quark.science.uu.nl:/home/students/3632229/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_Data_NAS2_FreeTOFTail
# rsync -rvhtm --update --exclude "*.pdf" /Users/philip/Dropbox/Thesis/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_Data_Spectra_FreeTOFTail/InclusiveFits 3632229@quark.science.uu.nl:/home/students/3632229/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/pp_Data_Spectra_FreeTOFTail

# rsync back.
# echo "Receiving from quark..."
rsync -rvhtm --update --exclude "job*.*" --exclude "Templates_*" 3632229@quark.science.uu.nl:/home/students/3632229/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/* /Users/philip/Dropbox/Thesis/DiHadronPID/AliAnalysisTaskDiHadronPIDMini
