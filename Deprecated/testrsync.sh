#!/bin/bash

#rsync to quark.
rsync -rvh --dry-run --delete --update --exclude ".DS_Store" --exclude "Libs/build/*" --exclude "AliAnalysisTaskDiHadronPIDMini/PostAnalysis*/PtEtaSpectra*" --exclude "AliAnalysisTaskDiHadronPIDMini/PostAnalysis*/*Fit*" --exclude "AliAnalysisTaskDiHadronPIDMini/old/*" Libs AliAnalysisTaskDiHadronPIDMini 3632229@quark.science.uu.nl:/home/students/3632229/DiHadronPID
rsync -rvh --dry-run --update --exclude "config.C" 3632229@quark.science.uu.nl:/home/students/3632229/DiHadronPID/AliAnalysisTaskDiHadronPIDMini/PostAnalysis* AliAnalysisTaskDiHadronPIDMini