/**
 * \file MakePtSpectra.C
 * \brief Create \f$p_T\f$ spectra from the fit results in \f$(p_T,\eta)\f$. 
 *
 * This macro projects identified spectra \f$N(p_T,\eta)\f$ onto the \f$p_T\f$ axis while applying a cut \f$y < 0.5\f$.
 * It will also normalize the spectra to the number of events that passed the event cuts (i.e. MC or INT7 events). Note
 * that for pp it is customary to normalize to the number of inelastic events, which requires a conversion factor.
 * This is done in the macro CorrectPtSpectra.C
 *
 * \bug Review the crappy fix for a failed fit in PbPb.
 */
#include "AliSettingsDiHadronPID.h"
#include "AliPtEtaSpectrumDiHadronPID.h"
#include "AliLogDiHadronPID.h"
#include "AliHistToolsDiHadronPID.h"
#include "TString.h"
#include "TH1.h"
#include "TSystem.h"

const AliSettingsDiHadronPID* settings;

void MakePtSpectra(Int_t Species = -1) {

    TH1::AddDirectory(0);

    // Check species.
    if (Species < -1 || Species > 2) {
        AliLogDiHadronPID::Error("Species must be -1 (all, default), 0, 1 or 2", "CreatePtSpectra", "");
        return;
    }

    // Get ptr to settings singleton.
	settings = AliSettingsDiHadronPID::Instance();
	if (!settings) {
        AliLogDiHadronPID::Error("Initialize settings first.", "CreatePtSpectra", "");
        return;
    }
	
	// Save current working directory.
	TString pwd = gSystem->pwd();

	// Open raw PtEtaSpectra.
	TFile* fInSpectraRaw = TFile::Open(settings->GetSpectraFileName());
	if (!fInSpectraRaw) {
		AliLogDiHadronPID::Error(Form("Couldn't find raw spectra file."), "CreatePtSpectra", "");
		return;
	}
	TH1I* histNEvents = (TH1I*)fInSpectraRaw->Get("nEvents");
	if (!histNEvents) {
		AliLogDiHadronPID::Error(Form("Couldn't find the number of events from the spectra file."), "CreatePtSpectra", "");
		return;
	}

	// Load the norm.
	const Double_t norm = histNEvents->GetBinContent(2);

	// Create output file.
	TFile* fOut = new TFile(settings->GetPtSpectraFileName(), "recreate");

	// Loop over fitted spectra files.
	for (Int_t iSpecies = 0; iSpecies < settings->GetNMassAssumptions(); iSpecies++) {

		// If an argument has been given, then only correct specific Species spectra.
		if (Species > -1 && iSpecies != Species) {continue;}

		// Open Spectra file.
		TFile* fInSpectra = TFile::Open(settings->GetSpectraFileNameForSpecies(iSpecies));
		if (!fInSpectra) {
			AliLogDiHadronPID::Error(Form("Couldn't find spectra file for mass assumption: %s", settings->GetSpeciesName((AliSettingsDiHadronPID::Species)iSpecies)), "CreatePtSpectra", "");
			return;
		}
			
		// Get a ptr to the fitted spectrum.
		AliPtEtaSpectrumDiHadronPID* spectrum = (AliPtEtaSpectrumDiHadronPID*)fInSpectra->Get(Form("Spec_%i", iSpecies));
		if (!spectrum) {
			AliLogDiHadronPID::Error(Form("Couldn't find fitted spectra for mass assumption: %s", settings->GetSpeciesName((AliSettingsDiHadronPID::Species)iSpecies)), "CreatePtSpectra", "");
			return;
		} 	

		// Check if it has all succeeded fits. If not continue, but with a warning...
		if (spectrum->GetNumberOfFailedFits()) {
			AliLogDiHadronPID::Warning(Form("The spectrum for mass assumption: %s has %i fits that have nonzero exit code...", settings->GetSpeciesName((AliSettingsDiHadronPID::Species)iSpecies), spectrum->GetNumberOfFailedFits()), "FillTemplates", "");
		}

		// Create a pt eta mask.
		TH2F* hPtEtaMask = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("ptetamask_sp%i",iSpecies), Form("ptetamask_sp%i",iSpecies), "F", settings->GetPtAxis(), settings->GetEtaAxis());
		AliMiscToolsDiHadronPID::MakePtEtaMask(hPtEtaMask, iSpecies, 1000);

		// Get the spectra.
		TH2F* hPtEtaSpectrum = (TH2F*)spectrum->GetPtEtaSpectra(iSpecies);

		// Very crappy fix for a failed fit in PbPb...
		if (iSpecies == 1) {hPtEtaSpectrum->SetBinError(22, 11, hPtEtaSpectrum->GetBinError(22, 12));}

		// Multiply bin by bin with the mask and project on pt axis.
        AliLogDiHadronPID::Info("Applying rapidity cut...", "CreatePtSpectra", "");
        for (Int_t iEtaBin = 1; iEtaBin <= (settings->GetEtaAxis())->GetNbins(); ++iEtaBin) {
            for (Int_t iPtBin = 1; iPtBin <= (settings->GetPtAxis())->GetNbins(); ++iPtBin) {
                if (hPtEtaMask->GetBinContent(iPtBin, iEtaBin) > 1e-10) {
                    hPtEtaSpectrum->SetBinContent(iPtBin, iEtaBin, hPtEtaSpectrum->GetBinContent(iPtBin, iEtaBin) * hPtEtaMask->GetBinContent(iPtBin, iEtaBin));
                    hPtEtaSpectrum->SetBinError(iPtBin, iEtaBin, hPtEtaSpectrum->GetBinError(iPtBin, iEtaBin) * hPtEtaMask->GetBinContent(iPtBin, iEtaBin));
                } else {
                    hPtEtaSpectrum->SetBinContent(iPtBin, iEtaBin, 0.);
                    hPtEtaSpectrum->SetBinError(iPtBin, iEtaBin, 0.);
                }
            }
        }

        TH1F* hpt = (TH1F*) hPtEtaSpectrum->ProjectionX(Form("specpt_sp%i",iSpecies),1, (settings->GetEtaAxis())->GetNbins());

		// Normalize and divide by binwidth.
		hpt->Scale(1./norm,"width"); 
		hpt->SetYTitle("1/N_{MB} d^{2}N/(dydp_{T}) (GeV/c)^{-1}");
		AliLogDiHadronPID::Info(Form("Normalizing raw pt spectra with number of selected events: %.0f", norm), "CreatePtSpectra", "");

		// Save to an output file.
		fOut->cd();
		hpt->Write();
		hPtEtaSpectrum->Write();
		hPtEtaMask->Write();
		
		AliLogDiHadronPID::Info(Form("Writing raw pt spectra to file: %s", settings->GetPtSpectraFileName().Data()), "CreatePtSpectra", "");

		if (fInSpectra) {fInSpectra->Close();}

	}

	fOut->Close();
}

