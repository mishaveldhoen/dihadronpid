/**
 * \file ComparePeakRegionSizes.C 
 * \brief This macro expects analyses to be carried out with different peak region sizes, and compares the influence on the final result.
 * \author Misha Veldhoen (misha.veldhoen@cern.ch)
 */

#include "TFile.h"
#include "TCanvas.h"
#include "TH1.h"

#include "PrettyPlotUtils.C"

const char* gCollisionSystemPaths[3] = {"./PbPb_Data_4GeV_NAS2_Mask5_TailK_0.009", "./pPb_Data_4GeV_NAS2_Mask5_TailK_0.009", "./pp_Data_4GeV_NAS2_Mask5_TailK_0.009"};
const Int_t nPeakRegions = 2;
const Double_t gPeakRegionDirs[2] = {4.,3.};
const char* gSpeciesNames[3] = {"Pion", "Kaon", "Proton"};
const char* gCollSystNames[3] = {"PbPb", "pPb", "pp"};

void ComparePeakRegionSizes() {

    TH1::AddDirectory(0);
    SetMyStyle("third");
    gStyle->SetOptTitle(1);

    // [coll syst][species]
    TH1F* hPeakRatioJet[3][3] = {{0x0}};
    TH1F* hPeakRatioBulk[3][3] = {{0x0}};
    
    for (Int_t iCollSyst = 0; iCollSyst < 3; iCollSyst++) {
        for (Int_t iPeakRegion = 0; iPeakRegion < nPeakRegions; iPeakRegion++) {
        
            TFile* fin = TFile::Open(Form("%s/AssociatedYieldAnalysis_Alpha%3.1f/AssociatedYieldAnalysis.root", gCollisionSystemPaths[iCollSyst], gPeakRegionDirs[iPeakRegion]));
            /*fin->ls();*/
            for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
                
                if (!iPeakRegion) {
                    hPeakRatioJet[iCollSyst][iSpecies] = (TH1F*)fin->Get(Form("hNSJetYield%s", gSpeciesNames[iSpecies]));
                    hPeakRatioBulk[iCollSyst][iSpecies] = (TH1F*)fin->Get(Form("hBackgroundDensity%s", gSpeciesNames[iSpecies]));
                }
                else {
                    hPeakRatioJet[iCollSyst][iSpecies]->Divide(hPeakRatioJet[iCollSyst][iSpecies], (TH1F*)fin->Get(Form("hNSJetYield%s", gSpeciesNames[iSpecies])),1.,1., "b");
                    hPeakRatioBulk[iCollSyst][iSpecies]->Divide(hPeakRatioBulk[iCollSyst][iSpecies], (TH1F*)fin->Get(Form("hBackgroundDensity%s", gSpeciesNames[iSpecies])),1.,1.,"b");
                }
            
            }
        }        
    }

    TCanvas* cvsJet = new TCanvas("cvsJet","cvsJet",1000,1000);
    cvsJet->Divide(3,3);

    for (Int_t iCollSyst = 0; iCollSyst < 3; iCollSyst++) {
        for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
            cvsJet->cd(1+iSpecies+3*iCollSyst);
            hPeakRatioJet[iCollSyst][iSpecies]->SetTitle(Form("%s %s", gCollSystNames[iCollSyst], gSpeciesNames[iSpecies]));
            hPeakRatioJet[iCollSyst][iSpecies]->SetYTitle("");
            hPeakRatioJet[iCollSyst][iSpecies]->UseCurrentStyle();
            hPeakRatioJet[iCollSyst][iSpecies]->Draw();
        }
    }

    TCanvas* cvsBulk = new TCanvas("cvsBulk","cvsBulk",1000,1000);
    cvsBulk->Divide(3,3);

    for (Int_t iCollSyst = 0; iCollSyst < 3; iCollSyst++) {
        for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
            cvsBulk->cd(1+iSpecies+3*iCollSyst);
            hPeakRatioBulk[iCollSyst][iSpecies]->SetTitle(Form("%s %s", gCollSystNames[iCollSyst], gSpeciesNames[iSpecies]));
            hPeakRatioBulk[iCollSyst][iSpecies]->SetYTitle("");
            hPeakRatioBulk[iCollSyst][iSpecies]->UseCurrentStyle();
            hPeakRatioBulk[iCollSyst][iSpecies]->Draw();
        }
    }
}
