// The goal of this macro is to look a the particle fractions as a function of DEta. The initial plan was:
//
//  1) Plot particle fractions as a function of N_hits,
//  2) Plot \sum PID/ UI as a function of N_hits 
//
// The first plan is not a good idea, as the particle fractions depend on pT and the position in (DPhi,DEta),
// i.e., are you on the near/away side jet, etc.
// The second plan was also not a good idea, as the matching efficiency turned out to be very dependent on
// pT and eta (see: CompareNumberOfTracksInAndOutOfBox.C). This means that at large DEta, where the matching
// efficiency gets lower, one automatically gets a smaller \sum PID/ UI.

// What this macro does now instead, is to plot the corrected particle fractions as a function of DEta,
// and compares them to the inclusive particle fractions. Note that we do make the following assumption:
//  - The inclusive result is integrated over eta, and therefore has some sort of "average" matching efficiency.
//  - The di-hadron result is corrected with mixed events, assuming that at (0,0) the efficiency is exactly
//    the same as for the inclusives, while anywhere else it's less. The mixed events should correct in such
//    a way that the efficiecy on the entire (DPhi, Deta) plane is the same as for the inclusives.
// 
// One has the options to only look at the away-side when comparing inclusive ratios and di-hadron ratios,
// in order to not be biased by the near-side jet peak.
// 

Int_t gPtBin = 10;
const Bool_t gUseAwaySideOnly = kTRUE;
const char* speciesNames[3] = {"Pion", "Kaon", "Proton"};

void CompareDiHadronUIvsPID() {
	

	TFile* finDiHadronUI = TFile::Open("PIDCorrelations_0.root");
	TFile* finSpectraPID[3] = {0x0};
	TFile* finDiHadronPID[3] = {0x0};

	// Open Files.
	if (!finDiHadronUI) {
		cout << "ERROR: PIDCorrelations_0.root not found." << endl; 
		return;
	}

	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
			
		finDiHadronPID[iSpecies] = TFile::Open(Form("PIDCorrelations_Fitted_%i.root", iSpecies));
		if (!finDiHadronPID[iSpecies]) {
			cout << "ERROR: PIDCorrelations_Fitted_" << iSpecies << ".root not found." << endl;
			return;
		}
	
		finSpectraPID[iSpecies] = TFile::Open(Form("InclusiveFits/Sp%i/FitParsAndStatus.root", iSpecies));
		if (!finSpectraPID[iSpecies]) {
			cout << "ERROR: Could not open " << Form("InclusiveFits/Sp%i/FitParsAndStatus.root", iSpecies) << endl;
			return;
		}

	}

	// Make ratios PID / UI for the signal.
	TH2F* signalUI = (TH2F*)finDiHadronUI->Get(Form("fUICorrelationsSumBeforeMECorr_PtBin%i", gPtBin));
	TH2F* signalPi = (TH2F*)finDiHadronPID[0]->Get(Form("diHadronPionsPtBin%i", gPtBin));
	TH2F* signalKa = (TH2F*)finDiHadronPID[1]->Get(Form("diHadronKaonsPtBin%i", gPtBin));
	TH2F* signalPr = (TH2F*)finDiHadronPID[2]->Get(Form("diHadronProtonsPtBin%i", gPtBin));

	THnF* signalPi4D = (THnF*)finDiHadronUI->Get(Form("CorrelationsPID_pt%i", gPtBin));
	signalPi4D->GetAxis(2)->SetRange(1,signalPi4D->GetAxis(2)->GetNbins());
	signalPi4D->GetAxis(3)->SetRange(1,signalPi4D->GetAxis(3)->GetNbins());
	TH2F* signalUIFromPIDHist = (TH2F*)signalPi4D->Projection(1,0);

	// signalUIFromPIDHist->Divide(signalUI);
	// TCanvas::MakeDefCanvas();
	// signalUIFromPIDHist->Draw("colz");

	signalPi->Divide(signalUIFromPIDHist);
	signalKa->Divide(signalUIFromPIDHist);
	signalPr->Divide(signalUIFromPIDHist);

	TCanvas::MakeDefCanvas();
	signalPi->Draw("colz");
	TCanvas::MakeDefCanvas();
	signalKa->Draw("colz");
	TCanvas::MakeDefCanvas();
	signalPr->Draw("colz");

	// Make ratios PID / UI for the mixed events.
	TH2F* mixedUI = (TH2F*)finDiHadronUI->Get(Form("fUIMixedEventsSumBeforeMECorr_PtBin%i", gPtBin));
	AliHistToolsDiHadronPID::NormaliseMixedEvents(mixedUI);
	TH2F* mixedPi = (TH2F*)finDiHadronPID[0]->Get(Form("mixedPionsPtBin%i", gPtBin));
	AliHistToolsDiHadronPID::NormaliseMixedEvents(mixedPi);
	TH2F* mixedKa = (TH2F*)finDiHadronPID[1]->Get(Form("mixedKaonsPtBin%i", gPtBin));
	AliHistToolsDiHadronPID::NormaliseMixedEvents(mixedKa);
	TH2F* mixedPr = (TH2F*)finDiHadronPID[2]->Get(Form("mixedProtonsPtBin%i", gPtBin));
	AliHistToolsDiHadronPID::NormaliseMixedEvents(mixedPr);

	THnF* mixedPi4D = (THnF*)finDiHadronUI->Get(Form("MixedEventsPID_pt%i", gPtBin));
	mixedPi4D->GetAxis(2)->SetRange(1, mixedPi4D->GetAxis(2)->GetNbins());
	mixedPi4D->GetAxis(3)->SetRange(1, mixedPi4D->GetAxis(3)->GetNbins());
	TH2F* mixedUIFromPIDHist = (TH2F*)mixedPi4D->Projection(1,0);
	AliHistToolsDiHadronPID::NormaliseMixedEvents(mixedUIFromPIDHist);

	// TCanvas::MakeDefCanvas();
	// mixedUIFromPIDHist->Draw("colz");

	mixedPi->Divide(mixedUIFromPIDHist);
	mixedKa->Divide(mixedUIFromPIDHist);
	mixedPr->Divide(mixedUIFromPIDHist);

	TCanvas::MakeDefCanvas();
	mixedPi->Draw("colz");
	TCanvas::MakeDefCanvas();
	mixedKa->Draw("colz");
	TCanvas::MakeDefCanvas();
	mixedPr->Draw("colz");

	// Make Projections onto the DEta axis.
	TH1F* signalPiProj = AliHistToolsDiHadronPID::ProjectionY_RMSError(signalPi, (gUseAwaySideOnly ? signalPi->GetNbinsX() / 2 : 1), signalPi->GetNbinsX());
	TH1F* signalKaProj = AliHistToolsDiHadronPID::ProjectionY_RMSError(signalKa, (gUseAwaySideOnly ? signalKa->GetNbinsX() / 2 : 1), signalKa->GetNbinsX());
	TH1F* signalPrProj = AliHistToolsDiHadronPID::ProjectionY_RMSError(signalPr, (gUseAwaySideOnly ? signalPr->GetNbinsX() / 2 : 1), signalPr->GetNbinsX());

	signalPi->Divide(mixedPi);
	signalKa->Divide(mixedKa);
	signalPr->Divide(mixedPr);

	TH1F* corrPiProj = AliHistToolsDiHadronPID::ProjectionY_RMSError(signalPi, (gUseAwaySideOnly ? signalPi->GetNbinsX() / 2 : 1), signalPi->GetNbinsX());
	TH1F* corrKaProj = AliHistToolsDiHadronPID::ProjectionY_RMSError(signalKa, (gUseAwaySideOnly ? signalKa->GetNbinsX() / 2 : 1), signalKa->GetNbinsX());
	TH1F* corrPrProj = AliHistToolsDiHadronPID::ProjectionY_RMSError(signalPr, (gUseAwaySideOnly ? signalPr->GetNbinsX() / 2 : 1), signalPr->GetNbinsX());

	TCanvas::MakeDefCanvas();
	signalPiProj->Draw();
	corrPiProj->SetLineColor(kRed);
	corrPiProj->Draw("same");

	TCanvas::MakeDefCanvas();
	signalKaProj->Draw();
	corrKaProj->SetLineColor(kRed);	
	corrKaProj->Draw("same");

	TCanvas::MakeDefCanvas();
	signalPrProj->Draw();
	corrPrProj->SetLineColor(kRed);	
	corrPrProj->Draw("same");

	// Particle ratios as a function of DEta and Eta (uncorrected.)
	TH1F* specPion = (TH1F*)(((TH2F*)finSpectraPID[0]->Get("Pion Yield"))->ProjectionX());
	TH1F* specKaon = (TH1F*)(((TH2F*)finSpectraPID[1]->Get("Kaon Yield"))->ProjectionX());
	TH1F* specProton = (TH1F*)(((TH2F*)finSpectraPID[2]->Get("Proton Yield"))->ProjectionX());

	// TCanvas::MakeDefCanvas();
	// specPion->Draw();
	// specKaon->Draw("same");
	// specProton->Draw("same");

	TH1F* hPrToPi = corrPrProj->Clone("PrToPi");
	hPrToPi->SetTitle("p/#pi");
	hPrToPi->GetXaxis()->SetRange(2, hPrToPi->GetNbinsX() - 1);
	hPrToPi->Divide(corrPiProj);
	TCanvas::MakeDefCanvas();
	hPrToPi->SetMinimum(0.);
	hPrToPi->SetMaximum(1.);
	hPrToPi->Draw();
	TF1* fPrToPiFit = new TF1("fPrToPiFit", "[0]", -1.5,1.5);
	fPrToPiFit->SetLineColor(kGreen);
	hPrToPi->Fit(fPrToPiFit);

	TF1* fPrToPi = new TF1("fPrToPi", "[0]", -1.6, 1.6);
	fPrToPi->SetLineColor(kBlue);
	fPrToPi->SetParameter(0, specProton->GetBinContent(gPtBin) / specPion->GetBinContent(gPtBin));
	fPrToPi->Draw("same");

	TH1F* hKaToPi = corrKaProj->Clone("KaToPi");
	hKaToPi->SetTitle("K/#pi");
	hKaToPi->GetXaxis()->SetRange(2, hKaToPi->GetNbinsX() - 1);	
	hKaToPi->Divide(corrPiProj);
	TCanvas::MakeDefCanvas();
	hKaToPi->SetMinimum(0.);
	hKaToPi->SetMaximum(1.);	
	hKaToPi->Draw();

	TF1* fKaToPiFit = new TF1("fKaToPiFit", "[0]", -1.5,1.5);
	fKaToPiFit->SetLineColor(kGreen);
	hKaToPi->Fit(fKaToPiFit);

	TF1* fKaToPi = new TF1("fKaToPi", "[0]", -1.6, 1.6);
	fKaToPi->SetLineColor(kBlue);
	fKaToPi->SetParameter(0, specKaon->GetBinContent(gPtBin) / specPion->GetBinContent(gPtBin));
	fKaToPi->Draw("same");

	TH1F* hPrToKa = corrPrProj->Clone("PrToKa");
	hPrToKa->SetTitle("p/K");
	hPrToKa->GetXaxis()->SetRange(2, hPrToKa->GetNbinsX() - 1);	
	hPrToKa->Divide(corrKaProj);
	hPrToKa->SetMinimum(0.);
	hPrToKa->SetMaximum(1.5);	
	TCanvas::MakeDefCanvas();
	hPrToKa->Draw();

	TF1* fPrToKaFit = new TF1("fPrToKaFit", "[0]", -1.5,1.5);
	fPrToKaFit->SetLineColor(kGreen);
	hPrToKa->Fit(fPrToPiFit);

	TF1* fPrToKa = new TF1("fPrToKa", "[0]", -1.6, 1.6);
	fPrToKa->SetLineColor(kBlue);	
	fPrToKa->SetParameter(0, specProton->GetBinContent(gPtBin) / specKaon->GetBinContent(gPtBin));
	fPrToKa->Draw("same");


}
