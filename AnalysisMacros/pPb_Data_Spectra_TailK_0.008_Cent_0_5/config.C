// This macro performs the same track cuts as the 0-5% pPb spectra analysis.
// ALICE pub: http://arxiv.org/abs/1307.6796

void config() {

	// Load all required libraries.
	gROOT->LoadMacro("../../Libs/make.C");
	make("MiniPostAnalysis", 0);

	// Create and configure an instance of the logger.
	AliLogDiHadronPID* logger = AliLogDiHadronPID::Instance();
	logger->SetVerboseLevel(3);
	logger->SetDebugLevel(0);

	// Create and configure an instance of the analysis settings.
	AliSettingsDiHadronPID* settings = AliSettingsDiHadronPID::Instance(1);

	// General Settings.
	settings->SetBeamType(1);
	settings->SetDataPath(Form("%s/data/DiHadronPIDMini_pPb_Data_AOD154", gSystem->HomeDirectory()));
	settings->SetWorkingDirectory(gSystem->pwd());
	settings->SetEfficienciesDirectory("../pPb_MC_Spectra");

	// Correlation Settings.
	settings->SetNTriggersToMix(150);
	settings->SetTriggerPtRange(4., 10.);
	settings->SetAssociatedPtRange(1., 4.);

	// Filter mask settings.
	settings->SetFilterMask(32);

	// DCA Settings.
	settings->SetFilterMaskForDCATemplates(16);
	settings->SetDCAAxis(50);
	// settings->SetDemandTightDCACut(kTRUE); // Is not necessary if filtermas for tracks is 32.

	// PID Settings.
	settings->SetMinClustersdEdx(60.);
	settings->SetDemandTOFTPCPIDFlags(kTRUE);

	// Fit Settings.
	settings->SetUseChi2BelowPt(1.);
	settings->UseFitPenalty(kFALSE);

	// Peak inclusions (default is NOT included) [m.a.][peak species][min pT][max pT]
	settings->SetIncludePeakInFit(AliSettingsDiHadronPID::kPion, AliSettingsDiHadronPID::kKaon, 1.0, 5.0); 
	settings->SetIncludePeakInFit(AliSettingsDiHadronPID::kPion, AliSettingsDiHadronPID::kProton, 1.2, 5.0);
	settings->SetIncludePeakInFit(AliSettingsDiHadronPID::kKaon, AliSettingsDiHadronPID::kPion, 1.0, 5.0); 
	settings->SetIncludePeakInFit(AliSettingsDiHadronPID::kKaon, AliSettingsDiHadronPID::kProton, 1.2, 5.0);
	settings->SetIncludePeakInFit(AliSettingsDiHadronPID::kProton, AliSettingsDiHadronPID::kPion, 1.2, 5.0); 
	settings->SetIncludePeakInFit(AliSettingsDiHadronPID::kProton, AliSettingsDiHadronPID::kKaon, 1.2, 5.0);

	// Setting the Axes.
	Double_t ptaxis[24] = {1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5,2.6,2.8,3.0,3.2,3.4,3.6,3.8,4.0};
	settings->SetPtAxis(23, ptaxis);

	Double_t centAxis[2] = {0., 5.};
	settings->SetCentralityAxis(1, centAxis);
	settings->SetVtxZAxis(1,-7.,7.);
	settings->SetEtaAxis(16, -0.8, 0.8);
	settings->SetDEtaAxis(16, -1.6, 1.6);
	settings->SetPhiAxis(16);
	settings->SetDPhiAxis(16);

	// PID Axes Settings.
	settings->SetTOFAxis(400, -2000, 6000, AliSettingsDiHadronPID::kPion);
	settings->SetTPCAxis(120, -30, 30, AliSettingsDiHadronPID::kPion);
	settings->SetTOFAxis(450, -3000, 6000, AliSettingsDiHadronPID::kKaon); // Changed this, to have enough space to determine the mismatches.
	settings->SetTPCAxis(100, -30, 30, AliSettingsDiHadronPID::kKaon);
	settings->SetTOFAxis(450, -5000, 4000, AliSettingsDiHadronPID::kProton); // id.
	settings->SetTPCAxis(120, -30, 30, AliSettingsDiHadronPID::kProton);

	// Fixing the Kaon Tail.
	settings->SetFixTOFTail(AliSettingsDiHadronPID::kPion, AliSettingsDiHadronPID::kKaon, 1., 4., 0.008);
	settings->SetFixTOFTail(AliSettingsDiHadronPID::kKaon, AliSettingsDiHadronPID::kKaon, 1., 4., 0.008);
	settings->SetFixTOFTail(AliSettingsDiHadronPID::kProton, AliSettingsDiHadronPID::kKaon, 1., 4., 0.008);

	// Print out the settings.
	settings->Print();

}
