#ifndef __CLING__
#include "TROOT.h"
#include "TSystem.h"
//#include "AliAnalysisTaskDiHadronPIDMini.h"
//#include "AliAODInputHandler.h"
//#include "AliAnalysisAlien.h"
//#include "AliAnalysisGrid.h"
//#include "AliAnalysisManager.h"
//#include "AliAnalysisTaskPIDResponse.h"
#include <iostream>
#include <fstream>
#include <cstring>
#endif

using namespace std;

// Macro for a test run on Grid.
// runmode = "test" "full", "merge", or "terminate"
// Latest ROOT Version on Quark.
const char* gAliPhysicsVer = "vAN-20180710-1";
    
const char* gRunMode = "full";
const char* gDescription = "pPb";
const Bool_t gIsMC = kFALSE;
const Int_t gAODProduction = 160; // TODO check for 16q

const char* gRunList = "../../RunLists/RunList_pPb_LHC16q.txt"; // TODO check for 16q

// Analysis Config.
const Int_t gSystemType = 1;  // 1: pPb
const Double_t gMinCentrality = 20.;
const Double_t gMaxCentrality = 0.;
const char* gCentralityEstimator = "V0A";
const Double_t gMaxVertexZ = 7.;
const Double_t gMaxAbsEta = 0.8;
const Double_t gMaxAbsMCY = 10.;
const Double_t gMinPt = 1.0;
const Double_t gMaxPt = 15.0;
const Int_t gFilterMask = 816; // 816 = 16 (Loose DCA) + 32 (Tight DCA) + 256 + 512 (Hybrids)  // TODO: Check for 16q
const char* gOutputFileName = "";
const char* gContainerName = "DiHadronPIDMini";
const char* gFolderName = "PWGCF_DiHadronPIDMini";

#ifdef __CLING__
#include "../../Libs/make.C"
#endif

AliAnalysisGrid* CreateAlienHandler(const char* runMode); // forward declaration for ROOT6

void runGridpPbDataLHC16q(TString runMode) {

	runMode.ToLower();
	if ((!(runMode.EqualTo("full"))) &&  (!(runMode.EqualTo("merge"))) && (!(runMode.EqualTo("terminate")))) {
		cout << "runMode has to be equal to full, merge, or terminate." << endl;
		return;
	}

    // Compile Libraries and Analysis Task.
#ifndef __CLING__
	gROOT->LoadMacro("../../Libs/make.C");
#endif
	make("MiniGridAnalysis", 1);

	// Includes.
	gROOT->ProcessLine(".include $ALICE_ROOT/include");	
	gROOT->ProcessLine(".include $ALICE_PHYSICS/include");	

	// Creating the Alien handler.
	AliAnalysisGrid *alienHandler = CreateAlienHandler(runMode.Data());
	if (!alienHandler) return;
	alienHandler->Print();

	// Create AnalysisManager and Input Handler.
	AliAnalysisManager *mgr = new AliAnalysisManager("AnalysisMan");
	AliAODInputHandler *aodH = new AliAODInputHandler();
	mgr->SetInputEventHandler(aodH);
	mgr->SetGridHandler(alienHandler);
	
    // Add the (run 2) centrality task
    AliMultSelectionTask *centtask = reinterpret_cast<AliMultSelectionTask *>(gInterpreter->ProcessLine(Form(".x %s(kFALSE)",gSystem->ExpandPathName("$ALICE_PHYSICS/OADB/COMMON/MULTIPLICITY/macros/AddTaskMultSelection.C"))));
    centtask->SetSelectedTriggerClass(AliVEvent::kINT7); // kINT7 is the default
    // centtask->SetUseDefaultCalib(kTRUE);
    
	// Add the PID Response Task.
	//gROOT->LoadMacro("$ALICE_ROOT/ANALYSIS/macros/AddTaskPIDResponse.C");   
        //AliAnalysisTaskPIDResponse *pidTask = AddTaskPIDResponse(gIsMC);
        AliAnalysisTaskPIDResponse *pidrestask = reinterpret_cast<AliAnalysisTaskPIDResponse *>(gInterpreter->ProcessLine(Form(".x %s(%d)", gSystem->ExpandPathName("$ALICE_ROOT/ANALYSIS/macros/AddTaskPIDResponse.C"),gIsMC)));

    /*
    gROOT->LoadMacro("../AddTaskDiHadronPIDMini.C");

    AliAnalysisTaskDiHadronPIDMini* DiHadronPIDMiniTask = AddTaskDiHadronPIDMini(
        AliMiniEventDiHadronPID::kpPb, 
        AliVEvent::kINT7,
        gMinCentrality, 
        gMaxCentrality,
        gCentralityEstimator, 
        gMaxVertexZ, 
        gMaxAbsEta,
        gMaxAbsMCY, 
        gMinPt, 
        gMaxPt,
        gFilterMask, 
        gIsMC, 
        gOutputFileName, 
        gContainerName, 
        gFolderName);
*/
        AliAnalysisTask *dihadrontask = reinterpret_cast<AliAnalysisTask *>(gInterpreter->ProcessLine(Form(".x ../AddTaskDiHadronPIDMini.C(%d,%d,%g,%g,\"%s\",%g,%g,%g,%g,%g,%d,%d,\"%s\",\"%s\",\"%s\")",gSystemType,
        AliVEvent::kINT7,
        gMinCentrality,
        gMaxCentrality,
        gCentralityEstimator,
        gMaxVertexZ,
        gMaxAbsEta,
        gMaxAbsMCY,
        gMinPt,
        gMaxPt,
        gFilterMask,
        gIsMC,
        gOutputFileName,
        gContainerName,
        gFolderName)));

	// Proceed with running.
	mgr->SetDebugLevel(10);
	if (!mgr->InitAnalysis()) return;
	mgr->PrintStatus();
	
	// The annoying thing about this is that all .xml files go to the /Libs dir...
	// At the moment I don't see how you can have the analysis code in a different
	// directory from the xml files...
	TString workingDir(gSystem->pwd());
	gSystem->cd("../../Libs"); // cd to the library dir.
	mgr->StartAnalysis("grid");
	gSystem->cd(workingDir.Data());

}

// -------------------------------------------------------------------------
AliAnalysisGrid* CreateAlienHandler(const char* runMode) {

	// Create Alien Handler
	AliAnalysisAlien *plugin = new AliAnalysisAlien();

	// Configure Run
	plugin->SetOverwriteMode();
	plugin->SetRunMode(runMode);
	plugin->SetAPIVersion("V1.1x");
        plugin->SetAliPhysicsVersion(gAliPhysicsVer);

        plugin->AddIncludePath("-I$ALICE_ROOT/include -I$ALICE_PHYSICS/include");
	// Load task
	plugin->SetAnalysisSource("AliLogDiHadronPID.cxx AliFunctionsDiHadronPID.cxx AliMiscToolsDiHadronPID.cxx AliHistToolsDiHadronPID.cxx AliMiniTrackDiHadronPID.cxx AliMiniEventDiHadronPID.cxx AliAnalysisTaskDiHadronPIDMini.cxx");
	plugin->SetAdditionalLibs("libPWGLFforward2.so AliLogDiHadronPID.cxx AliLogDiHadronPID.h AliFunctionsDiHadronPID.cxx AliFunctionsDiHadronPID.h AliMiscToolsDiHadronPID.cxx AliMiscToolsDiHadronPID.h AliHistToolsDiHadronPID.cxx AliHistToolsDiHadronPID.h AliMiniTrackDiHadronPID.cxx AliMiniTrackDiHadronPID.h AliMiniEventDiHadronPID.cxx AliMiniEventDiHadronPID.h AliAnalysisTaskDiHadronPIDMini.cxx AliAnalysisTaskDiHadronPIDMini.h TOFmismatchHistos.root");

	// Add Data Set
	if (gIsMC) {
		plugin->SetGridDataDir("/alice/sim/LHC11a10a_bis");
		plugin->SetDataPattern(Form("*AOD%03d/*AliAOD.root",gAODProduction));
		plugin->SetRunPrefix("");
		if (strlen(gRunList)==0) {plugin->AddRunNumber(139510);} 
		 else {
		 	// open gRunList
		 	ifstream in;
			in.open(gRunList);
			int runnumber;

			while (in.good()) {
				in>>runnumber;			// Expects collumn, no commas.
				plugin->AddRunNumber((Int_t)runnumber);
			}
			in.close();
		 }
		plugin->SetGridWorkingDir(Form("work/DiHadronPIDMini_MC_AOD%03i",gAODProduction));
	} else {
		plugin->SetGridDataDir("/alice/data/2016/LHC16q");
		//plugin->SetDataPattern(Form("*ESDs/pass3/AOD%03d/*AliAOD.root",gAODProduction));
plugin->SetDataPattern(Form("*pass1_CENT_wSDD/*AliAOD.root"));		
plugin->SetRunPrefix("000");
		if (strlen(gRunList)==0) {plugin->AddRunNumber(265525);} 
		 else {
		 	// open gRunList
		 	ifstream in;
			in.open(gRunList);
			int runnumber;

			while (in.good()) {
				in>>runnumber;			// Expects collumn, no commas.
				plugin->AddRunNumber((Int_t)runnumber);
			}
			in.close();
		 }
		//plugin->AddRunNumber(138275);
                if (strlen(gDescription))
                  plugin->SetGridWorkingDir(Form("work/DiHadronPIDMini_%s_DataLHC16q_AOD%03i", gDescription, gAODProduction));
                else
		  plugin->SetGridWorkingDir(Form("work/DiHadronPIDMini_DataLHC16q_AOD%03i", gAODProduction));
	}

	// Analysis Executable      
	plugin->SetAnalysisMacro(Form("DiHadronPIDMini_%s_Data.C", gDescription));
    plugin->SetExecutable(Form("DiHadronPIDMini_%s_Data.sh", gDescription));
    plugin->SetJDLName(Form("DiHadronPIDMini_%s_Data.jdl", gDescription));

    // Additional Settings
    plugin->SetExecutableCommand("aliroot -q -b"); 
    plugin->SetGridOutputDir("output");
  	plugin->SetMergeViaJDL(1);
  	plugin->SetOneStageMerging(kFALSE);
  	plugin->SetMaxMergeStages(2);
    plugin->SetTTL(30000);
  	plugin->SetInputFormat("xml-single");
  	plugin->SetPrice(1);    
    plugin->SetSplitMode("se");
    //plugin->SetDefaultOutputs(); // Q: What does this do??
	if (!gIsMC) plugin->SetSplitMaxInputFileNumber(30);
	else plugin->SetSplitMaxInputFileNumber(90);    // MC files contain much less events I think.
	plugin->SetOutputToRunNo();

	// Change AnalysisMacro name, etc.
	// Does the grid make a merged file for all run numbers?

	return plugin;

}

