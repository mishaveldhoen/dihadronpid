// Dataset config.
const char* gRunNumber[3] = {"LHC10h", "138275", "AOD049"};
const Bool_t gIsMC = kFALSE;
const Int_t gNFiles = 3;

// Analysis Config.
const Double_t gMinCentrality = 10.;
const Double_t gMaxCentrality = 0.;
const char* gCentralityEstimator = "V0M";
const Double_t gMaxVertexZ = 7.;
const Double_t gMaxAbsEta = 0.8;
const Double_t gMaxAbsMCY = 10.;
const Double_t gMinPt = 1.0;
const Double_t gMaxPt = 15.0;
const Int_t gFilterMask = 48; // 16 = Loose DCA, 32 = Tight DCA
const char* gOutputFileName = 0;
const char* gContainerName = "DiHadronPIDMini";
const char* gFolderName = "PWGCF_DiHadronPIDMini";

void runLocalPbPbDataAOD049() {

    // Compile Libraries and Analysis Task.
	gROOT->LoadMacro("../../Libs/make.C");
	make("MiniGridAnalysis", 1);

	// Includes.
	gROOT->ProcessLine(".include $ALICE_ROOT/include");	

	// Create AnalysisManager and Input Handler.
	AliAnalysisManager *mgr = new AliAnalysisManager("AnalysisMan");
	AliAODInputHandler *aodH = new AliAODInputHandler();
	mgr->SetInputEventHandler(aodH);

    // Set output file name.
    mgr->SetCommonFileName("AnalysisResults_PbPb_Data.root");

	// Add the PID Response Task.
	gROOT->LoadMacro("$ALICE_ROOT/ANALYSIS/macros/AddTaskPIDResponse.C");   
    AliAnalysisTaskPIDResponse *pidTask = AddTaskPIDResponse(gIsMC);
	
	gROOT->LoadMacro("../AddTaskDiHadronPIDMini.C");
    AliAnalysisTaskDiHadronPIDMini* DiHadronPIDMiniTask = AddTaskDiHadronPIDMini(
    	AliMiniEventDiHadronPID::kPbPb, 
    	AliVEvent::kMB,
    	gMinCentrality, 
    	gMaxCentrality,
    	gCentralityEstimator, 
    	gMaxVertexZ, 
    	gMaxAbsEta,
    	gMaxAbsMCY, 
    	gMinPt, 
    	gMaxPt,
    	gFilterMask, 
    	gIsMC, 
    	gOutputFileName, 
    	gContainerName, 
    	gFolderName);
    
    DiHadronPIDMiniTask->SetCutOnTOFHitBelowPt(4.);

    // Make a chain of the local data set.
	TChain *analysisChain = new TChain("aodTree");

	for (Int_t iFile = 1; iFile <= gNFiles; ++iFile) {

		if (gRunNumber[0] == "LHC10h") {analysisChain->Add(Form("/Users/mishaveldhoen/LocalData/data/2010/LHC10h/000%s/ESDs/pass2/%s/%04d/AliAOD.root", gRunNumber[1], gRunNumber[2], iFile));}
		if (gRunNumber[0] == "LHC11h") {analysisChain->Add(Form("/Users/mishaveldhoen/LocalData/data/2011/LHC11h_2/000%s/ESDs/pass2/%s/%04d/AliAOD.root", gRunNumber[1], gRunNumber[2], iFile));}

	}

	//TGrid::Connect("alien://");
	AliLogDiHadronPID* logger = AliLogDiHadronPID::Instance();
	logger->SetLogLevel(0);
	logger->SetDebugLevel(0);

	// Print Info, and Start Analysis.
	mgr->SetDebugLevel(10);
	if (!mgr->InitAnalysis()) return;
	mgr->PrintStatus();

	mgr->StartAnalysis("local", analysisChain);

}
