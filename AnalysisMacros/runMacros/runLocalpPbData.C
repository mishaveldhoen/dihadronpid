// Dataset config.

const char* gRunNumber[3] = {"LHC13c", "195529", "AOD139"};
const Bool_t gIsMC = kFALSE;
const Int_t gNFiles = 10;

// Analysis Config.
const Double_t gMinCentrality = 20.;
const Double_t gMaxCentrality = 0.;
const char* gCentralityEstimator = "V0A";
const Double_t gMaxVertexZ = 7.;
const Double_t gMaxAbsEta = 0.8;
const Double_t gMaxAbsMCY = 10.;
const Double_t gMinPt = 1.0;
const Double_t gMaxPt = 15.0;
const Int_t gFilterMask = 768;
const char* gOutputFileName = 0;
const char* gContainerName = "DiHadronPIDMini";
const char* gFolderName = "PWGCF_DiHadronPIDMini";

void runLocalpPbData() {

    // Compile Libraries and Analysis Task.
	gROOT->LoadMacro("../../Libs/make.C");
	make("MiniGridAnalysis", 1);

	// Create AnalysisManager and Input Handler.
	AliAnalysisManager *mgr = new AliAnalysisManager("AnalysisMan");
	AliAODInputHandler *aodH = new AliAODInputHandler();
	mgr->SetInputEventHandler(aodH);

    // Set output file name.
    mgr->SetCommonFileName("AnalysisResults_pPb_Data.root");

	// Add the PID Response Task.
	gROOT->LoadMacro("$ALICE_ROOT/ANALYSIS/macros/AddTaskPIDResponse.C");   
    AliAnalysisTaskPIDResponse *pidTask = AddTaskPIDResponse(gIsMC);

    gROOT->LoadMacro("../AddTaskDiHadronPIDMini.C");

    AliAnalysisTaskDiHadronPIDMini* DiHadronPIDMiniTask = AddTaskDiHadronPIDMini(
        AliMiniEventDiHadronPID::kpPb, 
        AliVEvent::kINT7,
        gMinCentrality, 
        gMaxCentrality,
        gCentralityEstimator, 
        gMaxVertexZ, 
        gMaxAbsEta,
        gMaxAbsMCY, 
        gMinPt, 
        gMaxPt,
        gFilterMask, 
        gIsMC, 
        gOutputFileName, 
        gContainerName, 
        gFolderName);

    //DiHadronPIDMiniTask->SetCutOnTOFHitBelowPt(4.);  
    
    // Make a chain of the local data set.
	TChain *analysisChain = new TChain("aodTree");

	for (Int_t iFile = 1; iFile <= gNFiles; ++iFile) {

		if (gRunNumber[0] == "LHC13c") {
			if (gRunNumber[2] == "AOD") {analysisChain->Add(Form("/Users/mishaveldhoen/LocalData/data/2013/LHC13c/000%s/ESDs/pass2/%s/%03d/AliAOD.root", gRunNumber[1], gRunNumber[2], iFile));}
			else {analysisChain->Add(Form("/Users/mishaveldhoen/LocalData/data/2013/LHC13c/000%s/ESDs/pass2/%s/%04d/AliAOD.root", gRunNumber[1], gRunNumber[2], iFile));}
		}

	}

	// Print Info, and Start Analysis.
	mgr->SetDebugLevel(10);
	if (!mgr->InitAnalysis()) return;
	mgr->PrintStatus();

	mgr->StartAnalysis("local", analysisChain);

}
