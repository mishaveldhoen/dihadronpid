// Macro for a test run on Grid.
// runmode = "full", "merge", or "terminate"
//const char* gRunList = "RunList_Data_LHC10h_AOD086_CFtrain_PART1.txt"; // 310 subjobs
//const char* gRunList = "RunList_Data_LHC10h_AOD086_CFtrain_PART2.txt"; // 650 subjobs
//const char* gRunList = "RunList_Data_LHC10h_AOD086_CFtrain_PART3.txt"; // ? subjobs

const char* gROOTver = "v5-34-08-6";
const char* gAliROOTver = "vAN-20140525";

char* gRunMode = "full";
const char* gDescription = "4GevTrig";
const Bool_t gIsMC = kTRUE;
const Int_t gAODProduction = 000;

// const char* gRunList = "../../RunLists/RunList_Data_LHC10e_AOD099_CFtrain.txt"; 		// Total
// const char* gRunList = "../../RunLists/RunList_Data_LHC10e_AOD099_CFtrain_part1.txt"; 	// 1400 subjobs
// const char* gRunList = "../../RunLists/RunList_Data_LHC10e_AOD099_CFtrain_part2.txt";  	// 1400 subbjobs
const char* gRunList = "../../RunLists/RunList_Data_LHC10e_AOD099_CFtrain_part3.txt";  	// 1350 subjobs

// const Int_t gAODProduction = 60;
// const char* gRunList = "../RunLists/RunList_MC_LHC10e20_AOD060_CFtrain.txt"; 		// Total 15M events, anchor LHC10e

// Analysis Config.
const Double_t gMinCentrality = 10.;
const Double_t gMaxCentrality = 0.;
const char* gCentralityEstimator = "V0M";
const Double_t gMaxVertexZ = 10.;
const Double_t gMaxAbsEta = 0.8;
const Double_t gMaxAbsMCY = 0.5;
const Double_t gMinPt = 1.0;
const Double_t gMaxPt = 10.0;
const Int_t gFilterMask = 816; // bits 16 and 32 for incusive spectra corrections and hybrid tracks are 256 and 512.
const char* gOutputFileName = 0;
const char* gContainerName = "DiHadronPIDMini";
const char* gFolderName = "PWGCF_DiHadronPIDMini";

void runGridMCppHybrid(char* runmode = "full") {

	gRunMode = runmode;
	
    // Compile Libraries and Analysis Task.
	gROOT->LoadMacro("../../Libs/make.C");
	make("MiniGridAnalysis", 1);

	// Includes.
	gROOT->ProcessLine(".include $ALICE_ROOT/include");	

	// Creating the Alien handler.
	AliAnalysisGrid *alienHandler = CreateAlienHandler();
	if (!alienHandler) return;
	alienHandler->Print();

	// Create AnalysisManager and Input Handler.
	AliAnalysisManager *mgr = new AliAnalysisManager("AnalysisMan");
	AliAODInputHandler *aodH = new AliAODInputHandler();
	mgr->SetInputEventHandler(aodH);
	mgr->SetGridHandler(alienHandler);
	
	// Add the PID Response Task.
	gROOT->LoadMacro("$ALICE_ROOT/ANALYSIS/macros/AddTaskPIDResponse.C");   
    AliAnalysisTaskPIDResponse *pidTask = AddTaskPIDResponse(gIsMC, kTRUE, kTRUE);

    gROOT->LoadMacro("../AddTaskDiHadronPIDMini.C");

    AliAnalysisTaskDiHadronPIDMini* DiHadronPIDMiniTask = AddTaskDiHadronPIDMini(
	   	AliMiniEventDiHadronPID::kpp,
   		AliVEvent::kMB,
        gMinCentrality, 
        gMaxCentrality,
        gCentralityEstimator, 
        gMaxVertexZ, 
        gMaxAbsEta, 
        gMaxAbsMCY,
        gMinPt, 
        gMaxPt,
        gFilterMask, 
        gIsMC, 
        gOutputFileName, 
        gContainerName, 
        gFolderName);

    // DiHadronPIDMiniTask->SetCutOnTOFHitBelowPt(4.);

	// Proceed with running.
	mgr->SetDebugLevel(10);
	if (!mgr->InitAnalysis()) return;
	mgr->PrintStatus();
	
	// The annoying thing about this is that all .xml files go to the /Libs dir...
	// At the moment I don't see how you can have the analysis code in a different
	// directory from the xml files...
	TString workingDir(gSystem->pwd());
	gSystem->cd("../../Libs"); // cd to the library dir.
	mgr->StartAnalysis("grid");
	gSystem->cd(workingDir.Data());

}

// -------------------------------------------------------------------------
AliAnalysisGrid* CreateAlienHandler() {

	// Create Alien Handler
	AliAnalysisAlien *plugin = new AliAnalysisAlien();

	// Configure Run
	plugin->SetOverwriteMode();
	plugin->SetRunMode(gRunMode);
	plugin->SetAPIVersion("V1.1x");
	plugin->SetROOTVersion(gROOTver);
	plugin->SetAliROOTVersion(gAliROOTver);

	// Load task
	plugin->SetAnalysisSource("AliLogDiHadronPID.cxx AliFunctionsDiHadronPID.cxx AliHistToolsDiHadronPID.cxx AliMiniTrackDiHadronPID.cxx AliMiniEventDiHadronPID.cxx AliAnalysisTaskDiHadronPIDMini.cxx");
	plugin->SetAdditionalLibs("AliLogDiHadronPID.cxx AliLogDiHadronPID.h AliFunctionsDiHadronPID.cxx AliFunctionsDiHadronPID.h AliHistToolsDiHadronPID.cxx AliHistToolsDiHadronPID.h AliMiniTrackDiHadronPID.cxx AliMiniTrackDiHadronPID.h AliMiniEventDiHadronPID.cxx AliMiniEventDiHadronPID.h AliAnalysisTaskDiHadronPIDMini.cxx AliAnalysisTaskDiHadronPIDMini.h TOFmismatchHistos.root");

	// Add Data Set
	if (gIsMC) {
		plugin->SetGridDataDir("/alice/sim/2013/LHC13e2_fix");
		// plugin->SetGridDataDir("/alice/sim/LHC10e20");
		plugin->SetDataPattern(Form("*AOD/*AliAOD.root"));
		plugin->SetRunPrefix("");
		if (gRunList=="") {plugin->AddRunNumber(139510);} 
		 else {
		 	// open gRunList
		 	ifstream in;
			in.open(gRunList);
			int runnumber;

			while (in.good()) {
				in>>runnumber;			// Expects collumn, no commas.
				plugin->AddRunNumber((Int_t)runnumber);
			}
			in.close();
		 }
		plugin->SetGridWorkingDir(Form("work/DiHadronPIDMini_MC_Hybrid"));
	} else {
		plugin->SetGridDataDir("/alice/data/2010/LHC10e");
		plugin->SetDataPattern(Form("*ESDs/pass2/AOD%03d/*AliAOD.root",gAODProduction));
		plugin->SetRunPrefix("000");
		if (gRunList=="") {plugin->AddRunNumber(139510);} 
		 else {
		 	// open gRunList
		 	ifstream in;
			in.open(gRunList);
			int runnumber;

			while (in.good()) {
				in>>runnumber;			// Expects collumn, no commas.
				plugin->AddRunNumber((Int_t)runnumber);
			}
			in.close();
		 }
		//plugin->AddRunNumber(138275);
		plugin->SetGridWorkingDir(Form("work/DiHadronPIDMini%s%s_Data_AOD%03i",(gDescription == "" ? "" : "_"), gDescription, gAODProduction));
	}

	/*
	if (isMC) {
		plugin->SetGridDataDir("/alice/sim/LHC11a10a_bis");
		plugin->SetDataPattern(Form("*AOD%03d/*AliAOD.root",gAODProduction));
		plugin->SetRunPrefix("");
		if (runlist=="") plugin->AddRunNumber(139510);
		else AddRunList(plugin, runlist);

		plugin->SetGridWorkingDir("work/DiHadronPID_MC");
	} else {
		plugin->SetGridDataDir("/alice/data/2010/LHC10h");
		plugin->SetDataPattern(Form("*ESDs/pass2/AOD%03d/*AliAOD.root",gAODProduction));
		plugin->SetRunPrefix("000");
		plugin->AddRunNumber(139510); // 0.20M  ~34 subjobs
		plugin->AddRunNumber(139465); // 0.89M  ~99 subjobs
 		plugin->AddRunNumber(139437); // 0.70M  ~77 subjobs
		plugin->AddRunNumber(139107); // 0.76M  ~92 subjobs
		plugin->AddRunNumber(139038); // 0.62M  ~89 subjobs
		plugin->AddRunNumber(138653); // 0.65M ~101 subjobs
		plugin->AddRunNumber(138534); // 2.3?M ~183 subjobs
		plugin->AddRunNumber(138442); // 0.9?M  ~94 subjobs
		plugin->AddRunNumber(138396); // 0.8?M  ~90 subjobs
		plugin->AddRunNumber(138364); // 1.1?M ~106 subjobs
		plugin->AddRunNumber(138275); // 2.5?M ~202 subjobs
				
		// Total: ~ 13.0M, 1167 subjobs.
		plugin->SetGridWorkingDir("work/DiHadronPID_TPCTOF_PiKaPr_TOFtimes1.5");
	}
*/
	// Analysis Executable      
	plugin->SetAnalysisMacro("DiHadronPIDMini.C");
    plugin->SetExecutable("DiHadronPIDMini.sh");
    plugin->SetJDLName("DiHadronPIDMini.jdl");

    // Additional Settings
    plugin->SetExecutableCommand("aliroot -q -b"); 
    plugin->SetGridOutputDir("output");
  	plugin->SetMergeViaJDL(1);
  	plugin->SetOneStageMerging(kFALSE);
  	plugin->SetMaxMergeStages(2);
    plugin->SetTTL(30000);
  	plugin->SetInputFormat("xml-single");
  	plugin->SetPrice(1);    
    plugin->SetSplitMode("se");
    //plugin->SetDefaultOutputs(); // Q: What does this do??
	if (!gIsMC) plugin->SetSplitMaxInputFileNumber(30);
	else plugin->SetSplitMaxInputFileNumber(30);    // MC files contain much less events I think.
	plugin->SetOutputToRunNo();

	// Change AnalysisMacro name, etc.
	// Does the grid make a merged file for all run numbers?

	return plugin;

}
