void MakeTemplates() {

	AliProcessDataDiHadronPIDMini* proc = new AliProcessDataDiHadronPIDMini(0);

	proc->MakeTemplates(AliSettingsDiHadronPID::Instance()->GetTemplatesFileName());

	delete proc;
	proc = 0x0;

}
