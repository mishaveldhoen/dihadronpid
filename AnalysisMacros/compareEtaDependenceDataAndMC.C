/**
 * @file        compareUIdataAndEfficiency.C  
 * @brief 	    Compare the \f$\eta\f$-dependence of the reconstructed data and MC tracks.
 * @author		Misha Veldhoen (misha.veldhoen@cern.ch)
 *
 * This macro requires the output from the QA analysis and the single particle 
 * efficiency analysis.
 *
 * Run this macro from the AnalysisMacros directory.
 */
#include "TObjArray.h"
#include "TFile.h"
#include "TString.h"
#include "TH2.h"
#include "TF2.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TLegend.h"
#include "TStyle.h"

#include <iostream>
using namespace std;

const char* pathMC[3] = {"PbPb_MC_4GeV_NAS2_Mask5_Efficiency", "pPb_MC_4GeV_NAS2_Mask5_Efficiency", "pp_MC_4GeV_NAS2_Mask5_Efficiency"};
const char* pathData[3] = {"PbPb_Data_4GeV_NAS2_Mask5_TailK_0.009", "pPb_Data_4GeV_NAS2_Mask5_TailK_0.009", "pp_Data_4GeV_NAS2_Mask5_TailK_0.009"};

const char* speciesNames[3] = {"Pion", "Kaon", "Proton"};

// Global pointers.
TFile* gFinQATask = 0x0;
TFile* gFinEfficiency = 0x0;
TObjArray* gArrRawHistos = 0x0;
TH2D* gHistPtEtaSpectrumUI = 0x0;

// Forward function declarations.
Int_t throwError(const TString& message);
Int_t cleanUp();

Int_t compareEtaDependenceDataAndMC(Int_t collSyst = 0) {

    if (collSyst < 0 || collSyst > 2) {return throwError("Choose a correct value for the collision system.");}
    
    // Some general settings.
    gStyle->SetCanvasDefW(500);
    gStyle->SetCanvasDefH(500);
    TH1::AddDirectory(0);

    // Open files.
    gFinQATask = TFile::Open(Form("%s/qaTask.root", pathData[collSyst]));
    if (!gFinQATask) {return throwError("Could not open qaTask.root");}
    gFinEfficiency = TFile::Open(Form("%s/Efficiencies.root", pathMC[collSyst]));
    if (!gFinEfficiency) {return throwError("Could not open Efficiencies.root");}

    // Load Histograms - Only consider MC pions, kaons and protons.
    gHistPtEtaSpectrumUI = (TH2D*)gFinQATask->Get("fPtEtaSpectrumUITotal");
    gArrRawHistos = (TObjArray*)gFinEfficiency->Get("RawHistos");

    gArrRawHistos->ls();

    TCanvas::MakeDefCanvas();
    gHistPtEtaSpectrumUI->Draw("colz");

    return cleanUp(); 

}

Int_t throwError(const TString& message) {

    cout << "ERROR: " << message.Data() << endl;
    cleanUp();
    return -1;

}

Int_t cleanUp() {

    // Sent close signal to file handlers. 
    if (gFinQATask) {gFinQATask->Close();}
    if (gFinEfficiency) {gFinEfficiency->Close();}
    if (gArrRawHistos) {delete gArrRawHistos; gArrRawHistos = 0x0;}
    return 0;

}
