#!/bin/bash

# This script syncs the config.C file of the current directory with quark. 
# One can pass any additional commands to the script, for example --dry-run ($1).

# Sync AnalysisMacros.  
/usr/local/bin/rsync -avzru $1 ./config.C veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/AnalysisMacros/${PWD##*/}
/usr/local/bin/rsync -avzru $1 veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/AnalysisMacros/${PWD##*/}/config.C .

