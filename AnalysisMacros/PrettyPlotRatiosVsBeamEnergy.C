/**
 * \file   PrettyPlotRatiosVsBeamEnergy.C
 * \brief  Plots ratios of pp, pPb and PbPb vs beam energy, with PYTHIA comparison.
 * \author Misha Veldhoen (misha.veldhoen@cern.ch)
 *
 * Initial attempts of this plot are in PrettyPlotFinalFirugres, they had different pT bins, but that
 * contained no additional useful information.
 */

#include "TLine.h"
#include "TLatex.h"
#include "TFile.h"
#include "TLegend.h"
#include "TGaxis.h"
#include "TH1F.h"
#include "TMarker.h"
#include "TCanvas.h"
#include "TGraphErrors.h"
#include "AliSettingsDiHadronPID.h"
#include "AliFunctionsDiHadronPID.h"

#include "PrettyPlotUtils.C"
AliSettingsDiHadronPID* gSettings = 0x0;

const char* gPythiaResultPath[3] = {"../DiHadronPID_Pythia/2760GeV/AssociatedYieldAnalysis_Alpha3.0/AssociatedYieldAnalysis.root",
    "../DiHadronPID_Pythia/5020GeV/AssociatedYieldAnalysis_Alpha3.0/AssociatedYieldAnalysis.root",
    "../DiHadronPID_Pythia/7000GeV/AssociatedYieldAnalysis_Alpha3.0/AssociatedYieldAnalysis.root"};
const char* gCollisionSystemPaths[3] = {"./PbPb_Data_4GeV_NAS2_Mask5_TailK_0.009", "./pPb_Data_4GeV_NAS2_Mask5_TailK_0.009", "./pp_Data_4GeV_NAS2_Mask5_TailK_0.009"};
const char* gAssociatedYieldDirectory[3] = {"AssociatedYieldAnalysis_Alpha3.0", "AssociatedYieldAnalysis_Alpha3.0", "AssociatedYieldAnalysis_Alpha3.0"};

const Double_t gCollisionSystemEnergies[3] = {2.76,5.02,7.};

// Data [beamType>PbPb|pPb|pp][species].
TH1F* gHistJetYield[3][3] = {{0x0}};
TH1F* gHistBackgroundDensity[3][3] = {{0x0}};

// PYTHIA [energy][species]
TH1F* gHistJetYieldPYTHIA[3][3] = {{0x0}};
TH1F* gHistBackgroundDensityPYTHIA[3][3] = {{0x0}};

// Settings controlling plot appearance.
Double_t gXAxisTickLength = 0.02;
Double_t gDesiredMinPt = 2.6;            // A warning will be given when this doesn't correspond to a bin edge.
Double_t gDesiredMaxPt = 4.0;

Double_t gYmin = -0.05;
Double_t gYmax = 0.8;

Int_t gPYTHIALineWidth = 2;
Int_t gPYTHIAMarkerSize = 1;
Int_t gPYTHIALineColor = kBlack;
Int_t gPYTHIALineStyle = 2;

// [beamType]
Size_t gMeasurementMarkerSize = 2;
Style_t gMeasurementMarkerStyle[3] = {kFullCircle, kFullStar, kFullCross};
Color_t gMeasurementMarkerColor[3] = {kBlue, kRed, kGreen-2};

// Forward function declarations.
Bool_t isApprox(Float_t num1, Float_t num2, Float_t err);
void getRatioAndErr(TH1F* histNum, TH1F* histDen, Int_t minPtBin, Int_t maxPtBin, Double_t& ratio, Double_t& error);
void makeUpPYTHIAGraph(TGraph* graph);
void makeUpMeasurementPoint(TGraphErrors* point, Int_t collSyst);

// -----------------------------------------------------------------------------------------------------
void PrettyPlotRatiosVsBeamEnergy() {

    TH1::AddDirectory(kFALSE);

    // Get a ptr to the settings.
    gSettings = AliSettingsDiHadronPID::Instance();    // Create our own instance of the settings.

    // Load PYTHIA results.
    for (Int_t iEnergy = 0; iEnergy < 3; iEnergy++) {
        TFile* fin = TFile::Open(gPythiaResultPath[iEnergy]);

        if (fin) {
            for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
                gHistJetYieldPYTHIA[iEnergy][iSpecies] = (TH1F*)fin->Get(Form("hNSJetYield%s", gSettings->GetSpeciesName((AliSettingsDiHadronPID::Species)(iSpecies))));
                gHistJetYieldPYTHIA[iEnergy][iSpecies]->SetXTitle("p_{T} (GeV/c)");
                gHistBackgroundDensityPYTHIA[iEnergy][iSpecies] = (TH1F*)fin->Get(Form("hBackgroundDensity%s", gSettings->GetSpeciesName((AliSettingsDiHadronPID::Species)(iSpecies))));
                gHistBackgroundDensityPYTHIA[iEnergy][iSpecies]->SetXTitle("p_{T} (GeV/c)");
            }
            fin->Close();
        } else {
            AliLogDiHadronPID::Error(Form("Could not open results from directory: %s", gPythiaResultPath[iEnergy]), "", "");
        }
    }

    // Load measurements.
    for (Int_t iCollSyst = 0; iCollSyst < 3; iCollSyst++) {
        TFile* fin = TFile::Open(Form("%s/%s/AssociatedYieldAnalysis.root", gCollisionSystemPaths[iCollSyst], gAssociatedYieldDirectory[iCollSyst]));
        if (fin) {
            for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
                gHistJetYield[iCollSyst][iSpecies] = (TH1F*)fin->Get(Form("hNSJetYield%s", gSettings->GetSpeciesName((AliSettingsDiHadronPID::Species)(iSpecies))));
                gHistJetYield[iCollSyst][iSpecies]->SetXTitle("p_{T} (GeV/c)");
                gHistBackgroundDensity[iCollSyst][iSpecies] = (TH1F*)fin->Get(Form("hBackgroundDensity%s", gSettings->GetSpeciesName((AliSettingsDiHadronPID::Species)(iSpecies))));
                gHistBackgroundDensity[iCollSyst][iSpecies]->SetXTitle("p_{T} (GeV/c)");
                gHistBackgroundDensity[iCollSyst][iSpecies]->SetYTitle("#frac{1}{N_{trig}} #frac{d^{3}N_{bgd}}{dp_{T} d#Delta#eta d#Delta#phi}");
            }   
            fin->Close();
        } else {
            AliLogDiHadronPID::Error(Form("Could not open results from directory: %s", gCollisionSystemPaths[iCollSyst]), "", "");
        }
    }
  
    // Check if the desired pt bounds correspond with bin edges in both data and pythia results. Find Min/Max bins.
    Int_t ptMinBinData = gHistJetYield[0][0]->FindBin(gDesiredMinPt+0.00001);
    if (!isApprox(gHistJetYield[0][0]->GetBinLowEdge(ptMinBinData), gDesiredMinPt, 0.00001)) {
        cout << "WARNING: " << gDesiredMinPt << " is not a bin edge in the measured data." << endl;
    }
    Int_t ptMaxBinData = gHistJetYield[0][0]->FindBin(gDesiredMaxPt-0.00001);
    if (!isApprox(gHistJetYield[0][0]->GetBinLowEdge(ptMaxBinData+1), gDesiredMaxPt, 0.00001)) {
        cout << "WARNING: " << gDesiredMaxPt << " is not a bin edge in the measured data." << endl;
    }
    Int_t ptMinBinPYTHIA = gHistJetYieldPYTHIA[0][0]->FindBin(gDesiredMinPt+0.00001);
    if (!isApprox(gHistJetYieldPYTHIA[0][0]->GetBinLowEdge(ptMinBinPYTHIA), gDesiredMinPt, 0.00001)) {
        cout << "WARNING: " << gDesiredMinPt << " is not a bin edge in the PYTHIA data." << endl;
    }
    Int_t ptMaxBinPYTHIA = gHistJetYieldPYTHIA[0][0]->FindBin(gDesiredMaxPt-0.00001);
    if (!isApprox(gHistJetYieldPYTHIA[0][0]->GetBinLowEdge(ptMaxBinPYTHIA+1), gDesiredMaxPt, 0.00001)) {
        cout << "WARNING: " << gDesiredMaxPt << " is not a bin edge in the PYTHIA data." << endl;
    }

    // Create the figure canvas. 
    SetMyStyle("third"); 
    gStyle->SetTitleOffset(2.2, "X");
    gStyle->SetTitleOffset(2.5, "Y");

    Double_t leftMargin3x2[6] = {0.2,0.01,0.2,0.01};    // Define the margins.
    Double_t topMargin3x2[6] = {0.04,0.04,0.04,0.04};
    Double_t rightMargin3x2[6] = {0.04,0.04,0.04,0.04};
    Double_t bottomMargin3x2[6] = {0.01,0.01,0.2,0.2};
 
    Double_t gridY = 0.55; 
    Double_t gridX = 0.55;
    Double_t yBoundaryUp3x2[4] = {1.,1., gridY, gridY};     // Define the grid.
    Double_t yBoundaryDown3x2[4] = {gridY, gridY, 0.,0.};
    Double_t xBoundaryUp3x2[4] = {gridX,1.0,gridX,1.0};
    Double_t xBoundaryDown3x2[4] = {0.0, gridX, 0.0, gridX};

    TCanvas* cvsFourInOne = new TCanvas("cvsFourInOne", "cvsFourInOne", 1000, 1000);
    for (Int_t iPad = 1; iPad <= 4; ++iPad) {
        TPad* pad = new TPad(Form("pad%i", iPad), Form("pad%i", iPad), 
                xBoundaryDown3x2[iPad-1], yBoundaryDown3x2[iPad-1], xBoundaryUp3x2[iPad-1], yBoundaryUp3x2[iPad-1]);
        pad->SetNumber(iPad); 
        pad->SetLeftMargin(leftMargin3x2[iPad-1]);
        pad->SetTopMargin(topMargin3x2[iPad-1]);
        pad->SetRightMargin(rightMargin3x2[iPad-1]);
        pad->SetBottomMargin(bottomMargin3x2[iPad-1]);
        pad->SetTicks(0,1);
        pad->Draw();
    }

    // Create PYTHIA graphs.
    TGraph* graphPYTHIAJetKaPi = new TGraph(3);
    TGraph* graphPYTHIAJetPrPi = new TGraph(3);
    TGraph* graphPYTHIABackgroundKaPi = new TGraph(3);
    TGraph* graphPYTHIABackgroundPrPi = new TGraph(3);
    for (Int_t iEnergy = 0; iEnergy < 3; iEnergy++) {
        Double_t ratio;
        Double_t error;

        getRatioAndErr(gHistJetYieldPYTHIA[iEnergy][1], gHistJetYieldPYTHIA[iEnergy][0], ptMinBinPYTHIA, ptMaxBinPYTHIA,ratio, error);
        graphPYTHIAJetKaPi->SetPoint(iEnergy,gCollisionSystemEnergies[iEnergy], ratio); 
        
        getRatioAndErr(gHistJetYieldPYTHIA[iEnergy][2], gHistJetYieldPYTHIA[iEnergy][0], ptMinBinPYTHIA, ptMaxBinPYTHIA,ratio, error);
        graphPYTHIAJetPrPi->SetPoint(iEnergy,gCollisionSystemEnergies[iEnergy], ratio); 
        
        getRatioAndErr(gHistBackgroundDensityPYTHIA[iEnergy][1], gHistBackgroundDensityPYTHIA[iEnergy][0], ptMinBinPYTHIA, ptMaxBinPYTHIA,ratio, error);
        graphPYTHIABackgroundKaPi->SetPoint(iEnergy,gCollisionSystemEnergies[iEnergy], ratio); 
        
        getRatioAndErr(gHistBackgroundDensityPYTHIA[iEnergy][2], gHistBackgroundDensityPYTHIA[iEnergy][0], ptMinBinPYTHIA, ptMaxBinPYTHIA,ratio, error);
        graphPYTHIABackgroundPrPi->SetPoint(iEnergy,gCollisionSystemEnergies[iEnergy], ratio); 
    }

    // Create Data Points (I don't think you can make points with errors, so we make them a graph).
    TGraphErrors* markerMeasurementJetKaPi[3] = {0x0};
    TGraphErrors* markerMeasurementJetPrPi[3] = {0x0};
    TGraphErrors* markerMeasurementBackgroundKaPi[3] = {0x0};
    TGraphErrors* markerMeasurementBackgroundPrPi[3] = {0x0};
    for (Int_t iCollSyst = 0; iCollSyst < 3; iCollSyst++) {
        Double_t ratio;
        Double_t error;

        getRatioAndErr(gHistJetYield[iCollSyst][1], gHistJetYield[iCollSyst][0], ptMinBinData, ptMaxBinData,ratio, error);
        markerMeasurementJetKaPi[iCollSyst] = new TGraphErrors(1);
        markerMeasurementJetKaPi[iCollSyst]->SetPoint(1, gCollisionSystemEnergies[iCollSyst], ratio);
        markerMeasurementJetKaPi[iCollSyst]->SetPointError(1,0.,error);
        makeUpMeasurementPoint(markerMeasurementJetKaPi[iCollSyst], iCollSyst);
        
        getRatioAndErr(gHistJetYield[iCollSyst][2], gHistJetYield[iCollSyst][0], ptMinBinData, ptMaxBinData,ratio, error);
        markerMeasurementJetPrPi[iCollSyst] = new TGraphErrors(1);
        markerMeasurementJetPrPi[iCollSyst]->SetPoint(1, gCollisionSystemEnergies[iCollSyst], ratio);
        markerMeasurementJetPrPi[iCollSyst]->SetPointError(1,0.,error);
        makeUpMeasurementPoint(markerMeasurementJetPrPi[iCollSyst], iCollSyst);
        
        getRatioAndErr(gHistBackgroundDensity[iCollSyst][1], gHistBackgroundDensity[iCollSyst][0], ptMinBinData, ptMaxBinData,ratio, error);
        markerMeasurementBackgroundKaPi[iCollSyst] = new TGraphErrors(1);
        markerMeasurementBackgroundKaPi[iCollSyst]->SetPoint(1, gCollisionSystemEnergies[iCollSyst], ratio);
        markerMeasurementBackgroundKaPi[iCollSyst]->SetPointError(1,0.,error);
        makeUpMeasurementPoint(markerMeasurementBackgroundKaPi[iCollSyst], iCollSyst);
        
        getRatioAndErr(gHistBackgroundDensity[iCollSyst][2], gHistBackgroundDensity[iCollSyst][0], ptMinBinData, ptMaxBinData,ratio, error);
        markerMeasurementBackgroundPrPi[iCollSyst] = new TGraphErrors(1);
        markerMeasurementBackgroundPrPi[iCollSyst]->SetPoint(1, gCollisionSystemEnergies[iCollSyst], ratio);
        markerMeasurementBackgroundPrPi[iCollSyst]->SetPointError(1,0.,error);
        makeUpMeasurementPoint(markerMeasurementBackgroundPrPi[iCollSyst], iCollSyst);

    }

    // Make up and draw PYTHIA curves and data points.
    TGaxis::SetMaxDigits(3);

    TH1F* placeHolderHist = new TH1F("placeHolderHist", "placeHolderHist", 1000,2,7.76);
    placeHolderHist->SetLineColor(kWhite);
    placeHolderHist->SetBinContent(1,gYmax);
    placeHolderHist->SetBinContent(2,gYmin);
    placeHolderHist->SetMinimum(gYmin);
    placeHolderHist->SetMaximum(gYmax);
    placeHolderHist->GetXaxis()->SetBinLabel(placeHolderHist->FindBin(2.76), "2.76");
    placeHolderHist->GetXaxis()->SetBinLabel(placeHolderHist->FindBin(5.02), "5.02");
    placeHolderHist->GetXaxis()->SetBinLabel(placeHolderHist->FindBin(7.), "7.00");
    placeHolderHist->GetXaxis()->LabelsOption("h");
    placeHolderHist->GetXaxis()->SetTickSize(0);

    TLatex* latex = new TLatex();

    TLegend* legend = new TLegend(0.245,0.617,0.546,0.917);
    cvsFourInOne->cd(1); 
    placeHolderHist->GetYaxis()->SetTitle("K/#pi");
    placeHolderHist->DrawCopy();
    makeUpPYTHIAGraph(graphPYTHIAJetKaPi);
    graphPYTHIAJetKaPi->Draw("lp same");
    for (Int_t iCollSyst = 0; iCollSyst < 3; iCollSyst++) {markerMeasurementJetKaPi[iCollSyst]->Draw("pe same");}
    legend->AddEntry(markerMeasurementJetKaPi[0], "PbPb", "p");
    legend->AddEntry(markerMeasurementJetKaPi[1], "pPb", "p");
    legend->AddEntry(markerMeasurementJetKaPi[2], "pp", "p");
    legend->AddEntry(graphPYTHIAJetKaPi, "PYTHIA");
    legend->Draw();
    latex->DrawLatexNDC(0.713764, 0.867471, "Jet Peak");
    latex->DrawLatexNDC(0.438, 0.09, Form("%3.1f < p_{T} < %3.1f", gDesiredMinPt, gDesiredMaxPt));
    TLine* tick1 = new TLine(2.76,gYmin,2.76,gYmin+gXAxisTickLength);
    tick1->Draw();
    TLine* tick2 = new TLine(5.02,gYmin,5.02,gYmin+gXAxisTickLength);
    tick2->Draw();
    TLine* tick3 = new TLine(7,gYmin,7,gYmin+gXAxisTickLength);
    tick3->Draw();

    cvsFourInOne->cd(2); 
    placeHolderHist->GetYaxis()->SetTitle("");
    placeHolderHist->DrawCopy();
    makeUpPYTHIAGraph(graphPYTHIABackgroundKaPi);
    graphPYTHIABackgroundKaPi->Draw("lp same");
    for (Int_t iCollSyst = 0; iCollSyst < 3; iCollSyst++) markerMeasurementBackgroundKaPi[iCollSyst]->Draw("pe same");
    latex->DrawLatexNDC(0.58, 0.867471, "Background");
    tick1->Draw();
    tick2->Draw();
    tick3->Draw();
    
    cvsFourInOne->cd(3); 
    makeUpPYTHIAGraph(graphPYTHIAJetPrPi);
    placeHolderHist->GetYaxis()->SetTitle("p/#pi");
    placeHolderHist->GetXaxis()->SetTitle("#sqrt{s_{NN}} (TeV)");
    placeHolderHist->DrawCopy();
    graphPYTHIAJetPrPi->Draw("lp same");
    for (Int_t iCollSyst = 0; iCollSyst < 3; iCollSyst++) markerMeasurementJetPrPi[iCollSyst]->Draw("pe same");
    latex->DrawLatexNDC(0.713764, 0.867471, "Jet Peak");
    tick1->Draw();
    tick2->Draw();
    tick3->Draw();

    cvsFourInOne->cd(4); 
    placeHolderHist->GetYaxis()->SetTitle("");
    placeHolderHist->DrawCopy();
    makeUpPYTHIAGraph(graphPYTHIABackgroundPrPi);
    graphPYTHIABackgroundPrPi->Draw("lp same");
    for (Int_t iCollSyst = 0; iCollSyst < 3; iCollSyst++) markerMeasurementBackgroundPrPi[iCollSyst]->Draw("pe same");
    latex->DrawLatexNDC(0.58, 0.867471, "Background");
    tick1->Draw();
    tick2->Draw();
    tick3->Draw();

    cvsFourInOne->SaveAs("../PrettyPictures/RatiosVsBeamEnergy.pdf");

}

// -----------------------------------------------------------------------------------------------------
Bool_t isApprox(Float_t num1, Float_t num2, Float_t err) {
    return ((num1 < (num2+err)) && (num1 > (num2-err)));
}

// -----------------------------------------------------------------------------------------------------
void getRatioAndErr(TH1F* histNum, TH1F* histDen, Int_t minPtBin, Int_t maxPtBin, Double_t& ratio, Double_t& error) {

    if (maxPtBin < minPtBin) {
        cout << "ERROR: max pt bin is smaller than min pt bin." << endl;
        return;
    }

    Double_t numTot = 0.;
    Double_t denTot = 0.;
    Double_t numErr2 = 0.;
    Double_t denErr2 = 0.;

    for (Int_t iPtBin = minPtBin; iPtBin <= maxPtBin; iPtBin++) {
        numTot += histNum->GetBinContent(iPtBin);
        denTot += histDen->GetBinContent(iPtBin);
        numErr2 += (histNum->GetBinError(iPtBin) * histNum->GetBinError(iPtBin));
        denErr2 += (histDen->GetBinError(iPtBin) * histDen->GetBinError(iPtBin));
    }

    if (TMath::Abs(denTot) < 0.0000001) {
        cout << "ERROR: Denominator is 0." << endl;
        return;
    }
    ratio = numTot / denTot;

    if (TMath::Abs(denErr2) < 0.0000000000001) {
        error = 0;
    } else {
        error = AliFunctionsDiHadronPID::ErrorRatio(numTot, TMath::Sqrt(numErr2), denTot, TMath::Sqrt(denErr2));
    }

}

// -----------------------------------------------------------------------------------------------------
void makeUpPYTHIAGraph(TGraph* graph) {

    graph->SetMinimum(gYmin);
    graph->SetMaximum(gYmax);
    graph->SetLineWidth(gPYTHIALineWidth);
    graph->SetLineColor(gPYTHIALineColor);
    graph->SetLineStyle(gPYTHIALineStyle);
    graph->SetMarkerStyle(kFullSquare);
    graph->SetMarkerSize(gPYTHIAMarkerSize);
    graph->SetFillColor(0);

} 

// -----------------------------------------------------------------------------------------------------
void makeUpMeasurementPoint(TGraphErrors* point, Int_t collSyst) {

    point->SetMarkerSize(gMeasurementMarkerSize);
    point->SetMarkerStyle(gMeasurementMarkerStyle[collSyst]);
    point->SetMarkerColor(gMeasurementMarkerColor[collSyst]);
    point->SetFillColor(0);

}
