// This macro creates a root file with the unfitted spectra for all species.

void MakeEfficiencies() {

	AliProcessDataDiHadronPIDMini* proc = new AliProcessDataDiHadronPIDMini(0);

	proc->MakeEfficiencies(AliSettingsDiHadronPID::Instance()->GetEfficienciesFileName());

	delete proc;
	proc = 0x0;

}
