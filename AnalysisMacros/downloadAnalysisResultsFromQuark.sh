#!/bin/bash

# This script syncs the all root files of the current directory with quark. 
# Run this macro from inside an analysis directory! (i.e., the one with the config.C file)
# One can pass any additional commands to the script, for example --dry-run ($1).

/usr/local/bin/rsync -avzu --progress $1 veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/AnalysisMacros/${PWD##*/}/*.root .

mkdir -p ./InclusiveFits/Sp0
/usr/local/bin/rsync -avzu --progress $1 veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/AnalysisMacros/${PWD##*/}/InclusiveFits/Sp0/*.root ./InclusiveFits/Sp0/
mkdir -p ./InclusiveFits/Sp1
/usr/local/bin/rsync -avzu --progress $1 veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/AnalysisMacros/${PWD##*/}/InclusiveFits/Sp1/*.root ./InclusiveFits/Sp1/
mkdir -p ./InclusiveFits/Sp2
/usr/local/bin/rsync -avzu --progress $1 veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/AnalysisMacros/${PWD##*/}/InclusiveFits/Sp2/*.root ./InclusiveFits/Sp2/

/usr/local/bin/rsync -avzru --progress $1 veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/AnalysisMacros/${PWD##*/}/AssociatedYieldAnalysis .

