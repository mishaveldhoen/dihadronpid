#!/bin/sh
#
# Name:
#$ -N jobFitPtEtaSpectraProton
#
# Run through bash shell
#$ -S /bin/bash

#$ -cwd

# Load environment variables for aliroot.
source /scratch/software/ns-sap-env.sh vAN-20150513

# Run the task.
aliroot -b -q config.C ../FitPtEtaSpectraProton.C
