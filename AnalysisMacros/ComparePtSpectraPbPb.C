// This macro compares our results to Christian's relativistic rise analysis, and the combined spectra.
const char* gFileNameTOFTPCFit = "PtSpectra_Corrected.root";
const char* gFileNameTOFTPCFitPos = "../PbPb_Data_Spectra_FixKTail_Pos/PtSpectra_Corrected.root";
const char* gFileNameTOFTPCFitNeg = "../PbPb_Data_Spectra_FixKTail_Neg/PtSpectra_Corrected.root";

const char* gFileNameTrackingEffPosCombined = "../../ComparisonHistos/RobertosTOFEfficiencyAndQA/TOF_trackingEfficiency_positive.root";
const char* gFileNameTrackingEffNegCombined = "../../ComparisonHistos/RobertosTOFEfficiencyAndQA/TOF_trackingEfficiency_negative.root";
const char* gFileNameMatchingEffPosCombined = "../../ComparisonHistos/RobertosTOFEfficiencyAndQA/TOF_matchingEfficiency_positive.root";
const char* gFileNameMatchingEffNegCombined = "../../ComparisonHistos/RobertosTOFEfficiencyAndQA/TOF_matchingEfficiency_negative.root";
const char* gFileNameEfficienciesTOFTPCFit = "Efficiencies.root";

const char* gFileNamePrimFractionsPionCombined = "../../ComparisonHistos/RobertosTOFEfficiencyAndQA/TOF_dca_pion_primaryfractions_fit.root";
const char* gFileNamePrimFractionsProtonCombined = "../../ComparisonHistos/RobertosTOFEfficiencyAndQA/TOF_dca_proton_primaryfractions_fit.root";

const char* gOutputDir = "CorrectedSpectraComparison";

void ComparePtSpectraPbPb() {

	// Get a ptr to the settings.
	AliSettingsDiHadronPID* settings = AliSettingsDiHadronPID::Instance();
	if (!settings) {AliLogDiHadronPID::Error("Initialize settings first.", "ComparePtSpectraPbPb", "");}

	// Get a ptr to the systematic uncertainty object for each species.
	gSystem->Load("../SystematicUncertainty.C");
	AliSystUncertaintyGenDiHadronPID* systUncertainty[3] = {0x0};
	systUncertainty[0] = GetTotalSystUncertaintyPbPbInclusive(0);
	systUncertainty[1] = GetTotalSystUncertaintyPbPbInclusive(1);
	systUncertainty[2] = GetTotalSystUncertaintyPbPbInclusive(2);

	// Create output dir.
	AliMiscToolsDiHadronPID::CreateDir(gOutputDir);

	const char* speciesLaTeX[3][2] = {"#pi^{-}", "#pi^{+}","K^{-}","K^{+}","#bar{p}","p"};

	// Load published spectra.
	gSystem->Load("../../ComparisonHistos/PublishedMeasurement.C");
	TGraphAsymmErrors* hSpectrum2014[3] = {0x0};
	hSpectrum2014[0] = ALICE_PbPb_0_5_PionSpec_2014();
	hSpectrum2014[1] = ALICE_PbPb_0_5_KaonSpec_2014();
	hSpectrum2014[2] = ALICE_PbPb_0_5_ProtonSpec_2014();

	TFile* fTOFTPCFit = TFile::Open(gFileNameTOFTPCFit);
	TH1F* hSpectraTOFTPCFit[3] = {0x0};
	hSpectraTOFTPCFit[0] = (TH1F*)fTOFTPCFit->Get("specpt_sp0;1");
	hSpectraTOFTPCFit[1] = (TH1F*)fTOFTPCFit->Get("specpt_sp1;1");
	hSpectraTOFTPCFit[2] = (TH1F*)fTOFTPCFit->Get("specpt_sp2;1");

	// Rescale (1/2pi*pT)
	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
			
		for (Int_t iBin = 1; iBin <= hSpectraRelativisticRise[iSpecies]->GetNbinsX(); iBin++) {
	
			Double_t factor = 2. * TMath::Pi() * hSpectraTOFTPCFit[iSpecies]->GetXaxis()->GetBinCenter(iBin);
			hSpectraTOFTPCFit[iSpecies]->SetBinContent(iBin, hSpectraTOFTPCFit[iSpecies]->GetBinContent(iBin) / factor);
			hSpectraTOFTPCFit[iSpecies]->SetBinError(iBin, hSpectraTOFTPCFit[iSpecies]->GetBinError(iBin) / factor);

		}

	}



	// Draw the results.
	TCanvas* cvsSpectra[3] = {0x0};
	TSpline3* sSpectraTOFTPCFit[3] = {0x0};
	TH1F* hSpectraTOFTPCFitRatio[3] = {0x0};
	TH1F* hSpectraCombinedRatio[3] = {0x0};
	TH1F* hSpectraRelativisticRiseRatio[3] = {0x0};

	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
			
		cvsSpectra[iSpecies] = new TCanvas(Form("cvsSpectra_sp%i", iSpecies), Form("cvsSpectra_sp%i", iSpecies), iSpecies * 500, 0, 500, 600);
		cvsSpectra[iSpecies]->Divide(1,2,0.,0.);
		cvsSpectra[iSpecies]->cd(1);
		cvsSpectra[iSpecies]->GetPad(1)->SetLogy();

		// hSpectraTOFTPCFit[iSpecies]->Scale(0.9);
		hSpectraTOFTPCFit[iSpecies]->SetMarkerStyle(23);
		hSpectraTOFTPCFit[iSpecies]->SetMarkerColor(kMagenta);
		hSpectraTOFTPCFit[iSpecies]->SetLineColor(kMagenta);
		hSpectraTOFTPCFit[iSpecies]->Draw();

		// Create errors on the total object.
		TGraphAsymmErrors* errors = systUncertainty[iSpecies]->GetGraph(hSpectraTOFTPCFit[iSpecies]);
		errors->SetFillColor(kMagenta);
		errors->SetFillStyle(3001);	
		errors->Draw("2");
		hSpectraTOFTPCFit[iSpecies]->Draw("same");	// Crappy fix, to not let the graph make the axes.

		sSpectraTOFTPCFit[iSpecies] = new TSpline3(hSpectraTOFTPCFit[iSpecies]);
		sSpectraTOFTPCFit[iSpecies]->Draw("same");

		hSpectraRelativisticRise[iSpecies]->SetMarkerStyle(21);
		hSpectraRelativisticRise[iSpecies]->SetMarkerColor(kRed);
		hSpectraRelativisticRise[iSpecies]->SetLineColor(kRed);
		hSpectraRelativisticRise[iSpecies]->Draw("same");

		hSpectraCombined[iSpecies]->SetMarkerStyle(22);
		hSpectraCombined[iSpecies]->SetMarkerSize(1);
		hSpectraCombined[iSpecies]->SetMarkerColor(kBlue);
		hSpectraCombined[iSpecies]->SetLineColor(kBlue);
		hSpectraCombined[iSpecies]->Draw("same");

		TLegend* legend = new TLegend(0.75,0.75,0.95,0.95);
		legend->AddEntry(hSpectraRelativisticRise[iSpecies], "Rel. Rise");
		legend->AddEntry(hSpectraCombined[iSpecies], "Combined");
		legend->AddEntry(hSpectraTOFTPCFit[iSpecies], "TOF TPC Fit");
		legend->Draw();

		cvsSpectra[iSpecies]->cd(2);
		hSpectraTOFTPCFitRatio[iSpecies] = (TH1F*)hSpectraTOFTPCFit[iSpecies]->Clone();
		hSpectraTOFTPCFitRatio[iSpecies]->GetYaxis()->SetTitle("Ratio");
		AliHistToolsDiHadronPID::DivideThroughSpline(hSpectraTOFTPCFitRatio[iSpecies], sSpectraTOFTPCFit[iSpecies]);
		hSpectraTOFTPCFitRatio[iSpecies]->SetMinimum(0.8);
		hSpectraTOFTPCFitRatio[iSpecies]->SetMaximum(1.2);
		hSpectraTOFTPCFitRatio[iSpecies]->Draw();

		// Create errors on the ratio.
		TGraphAsymmErrors* errorsRatio = systUncertainty[iSpecies]->GetGraph(hSpectraTOFTPCFitRatio[iSpecies]);
		errorsRatio->SetFillColor(kMagenta);
		errorsRatio->SetFillStyle(3001);	
		errorsRatio->Draw("2");
		hSpectraTOFTPCFitRatio[iSpecies]->Draw("same");	// Crappy fix, to not let the graph make the axes.

		hSpectraCombinedRatio[iSpecies] = (TH1F*)hSpectraCombined[iSpecies]->Clone();
		AliHistToolsDiHadronPID::DivideThroughSpline(hSpectraCombinedRatio[iSpecies], sSpectraTOFTPCFit[iSpecies]);
		hSpectraCombinedRatio[iSpecies]->Draw("same");

		hSpectraRelativisticRiseRatio[iSpecies] = (TH1F*)hSpectraRelativisticRise[iSpecies]->Clone();
		AliHistToolsDiHadronPID::DivideThroughSpline(hSpectraRelativisticRiseRatio[iSpecies], sSpectraTOFTPCFit[iSpecies]);
		hSpectraRelativisticRiseRatio[iSpecies]->Draw("same");

		cvsSpectra[iSpecies]->SaveAs(Form("%s/SpectraCompSp%i.pdf", gOutputDir, iSpecies));

		//AliHistToolsDiHadronPID::FixPlotRange(cvsSpectra[iSpecies]);

	}

	// -------------------------------
	//  COMPARE TRACKING EFFICIENCIES
	// -------------------------------

	// Figure out the efficiency correction file path.
	TString effPath = settings->GetEfficienciesDirectory();
	effPath+="/";
	effPath+=settings->GetEfficienciesFileName();

	// Open TOFTPCFit Tracking efficiencies.
	TH1F* hTrackingEffTOFTPC[3][2] = {0x0};
	TFile* fEfficienciesTOFTPCFit = TFile::Open(effPath.Data());
	hTrackingEffTOFTPC[0][0] = (TH1F*)fEfficienciesTOFTPCFit->Get("trackingpt_sp0_ch0;1");
	hTrackingEffTOFTPC[1][0] = (TH1F*)fEfficienciesTOFTPCFit->Get("trackingpt_sp1_ch0;1");
	hTrackingEffTOFTPC[2][0] = (TH1F*)fEfficienciesTOFTPCFit->Get("trackingpt_sp2_ch0;1");
	hTrackingEffTOFTPC[0][1] = (TH1F*)fEfficienciesTOFTPCFit->Get("trackingpt_sp0_ch1;1");
	hTrackingEffTOFTPC[1][1] = (TH1F*)fEfficienciesTOFTPCFit->Get("trackingpt_sp1_ch1;1");
	hTrackingEffTOFTPC[2][1] = (TH1F*)fEfficienciesTOFTPCFit->Get("trackingpt_sp2_ch1;1");

	// Open Combined Tracking efficiencies.
	TH1D* hTrackingEffCombined[3][2] = {0x0};

	TFile* fTrackingEffNegCombined = TFile::Open(gFileNameTrackingEffNegCombined);
	TCanvas* cvsTrackingEffNegCombined = (TCanvas*)fTrackingEffNegCombined->Get("c1_n2;1");
	TList* lTrackingEffNegCombined = cvsTrackingEffNegCombined->GetListOfPrimitives();
	// lTrackingEffNegCombined->ls();
	hTrackingEffCombined[0][0] = (TH1D*)lTrackingEffNegCombined->FindObject("hTrackingEff_cent0_pion_negative");
	hTrackingEffCombined[1][0] = (TH1D*)lTrackingEffNegCombined->FindObject("hTrackingEff_cent0_kaon_negative");
	hTrackingEffCombined[2][0] = (TH1D*)lTrackingEffNegCombined->FindObject("hTrackingEff_cent0_proton_negative");
	
	TFile* fTrackingEffPosCombined = TFile::Open(gFileNameTrackingEffPosCombined);
	TCanvas* cvsTrackingEffPosCombined = (TCanvas*)fTrackingEffPosCombined->Get("c1;1");
	TList* lTrackingEffPosCombined = cvsTrackingEffPosCombined->GetListOfPrimitives();
	// lTrackingEffPosCombined->ls();
	hTrackingEffCombined[0][1] = (TH1D*)lTrackingEffPosCombined->FindObject("hTrackingEff_cent0_pion_positive");
	hTrackingEffCombined[1][1] = (TH1D*)lTrackingEffPosCombined->FindObject("hTrackingEff_cent0_kaon_positive");
	hTrackingEffCombined[2][1] = (TH1D*)lTrackingEffPosCombined->FindObject("hTrackingEff_cent0_proton_positive");

	// Draw Comparisons.
	/*
	TCanvas* cvsTrackingEffPosCompared[3][2] = {0x0};
	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		for (Int_t iCharge = 0; iCharge < 2; iCharge++) {
			cvsTrackingEffPosCompared[iSpecies][iCharge] = TCanvas::MakeDefCanvas();
			hTrackingEffTOFTPC[iSpecies][iCharge]->SetMinimum(0.6);
			hTrackingEffTOFTPC[iSpecies][iCharge]->SetMaximum(0.8);
			hTrackingEffTOFTPC[iSpecies][iCharge]->Draw();
			hTrackingEffTOFTPC[iSpecies][iCharge]->SetTitle(Form("Tracking Eff. %s", speciesLaTeX[iSpecies][iCharge]));
			hTrackingEffCombined[iSpecies][iCharge]->Draw("same");

			cvsTrackingEffPosCompared[iSpecies][iCharge]->SaveAs(Form("%s/TrackingEffCompSp%iCh%i.pdf", gOutputDir, iSpecies, iCharge));

		}
	}
*/
	// -------------------------------
	//  COMPARE MATCHING EFFICIENCIES
	// -------------------------------

	// Open TOFTPCFit Tracking efficiencies.
	TH1F* hPIDEffTOFTPC[3][2] = {0x0};
	hPIDEffTOFTPC[0][0] = (TH1F*)fEfficienciesTOFTPCFit->Get("goodmatchingpt_sp0_ch0;1");
	hPIDEffTOFTPC[1][0] = (TH1F*)fEfficienciesTOFTPCFit->Get("goodmatchingpt_sp1_ch0;1");
	hPIDEffTOFTPC[2][0] = (TH1F*)fEfficienciesTOFTPCFit->Get("goodmatchingpt_sp2_ch0;1");

	hPIDEffTOFTPC[0][1] = (TH1F*)fEfficienciesTOFTPCFit->Get("goodmatchingpt_sp0_ch1;1");
	hPIDEffTOFTPC[1][1] = (TH1F*)fEfficienciesTOFTPCFit->Get("goodmatchingpt_sp1_ch1;1");
	hPIDEffTOFTPC[2][1] = (TH1F*)fEfficienciesTOFTPCFit->Get("goodmatchingpt_sp2_ch1;1");

	// Open Combined Tracking efficiencies.
	TH1D* hMatchingEffCombined[3][2] = {0x0};

	TFile* fMatchingEffNegCombined = TFile::Open(gFileNameMatchingEffNegCombined);
	TCanvas* cvsMatchingEffNegCombined = (TCanvas*)fMatchingEffNegCombined->Get("c1_n4;1");
	TList* lMatchingEffNegCombined = cvsMatchingEffNegCombined->GetListOfPrimitives();
	// lMatchingEffNegCombined->ls();
	hMatchingEffCombined[0][0] = (TH1D*)lMatchingEffNegCombined->FindObject("hMatchEff_cent0_pion_negative");
	hMatchingEffCombined[1][0] = (TH1D*)lMatchingEffNegCombined->FindObject("hMatchEff_cent0_kaon_negative");
	hMatchingEffCombined[2][0] = (TH1D*)lMatchingEffNegCombined->FindObject("hMatchEff_cent0_proton_negative");
	
	TFile* fMatchingEffPosCombined = TFile::Open(gFileNameMatchingEffPosCombined);
	TCanvas* cvsMatchingEffPosCombined = (TCanvas*)fMatchingEffPosCombined->Get("c1_n3;1");
	TList* lMatchingEffPosCombined = cvsMatchingEffPosCombined->GetListOfPrimitives();
	// lMatchingEffPosCombined->ls();
	hMatchingEffCombined[0][1] = (TH1D*)lMatchingEffPosCombined->FindObject("hMatchEff_cent0_pion_positive");
	hMatchingEffCombined[1][1] = (TH1D*)lMatchingEffPosCombined->FindObject("hMatchEff_cent0_kaon_positive");
	hMatchingEffCombined[2][1] = (TH1D*)lMatchingEffPosCombined->FindObject("hMatchEff_cent0_proton_positive");

	// Draw Comparisons.
/*
	TCanvas* cvsMatchingEffPosCompared[3][2] = {0x0};
	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
		for (Int_t iCharge = 0; iCharge < 2; iCharge++) {
			cvsMatchingEffPosCompared[iSpecies][iCharge] = TCanvas::MakeDefCanvas();
			hPIDEffTOFTPC[iSpecies][iCharge]->SetMinimum(0.45);
			hPIDEffTOFTPC[iSpecies][iCharge]->SetMaximum(0.7);
			hPIDEffTOFTPC[iSpecies][iCharge]->Draw();
			hPIDEffTOFTPC[iSpecies][iCharge]->SetTitle(Form("PID/Matching Eff. %s", speciesLaTeX[iSpecies][iCharge]));
			hMatchingEffCombined[iSpecies][iCharge]->Draw("same");
	
			cvsMatchingEffPosCompared[iSpecies][iCharge]->SaveAs(Form("%s/MatchingEffCompSp%iCh%i.pdf", gOutputDir, iSpecies, iCharge));

		}
	}
*/
	// ---------------------------
	//  EFFICIENCIES - NICE PLOTS
	// ---------------------------
	// TCanvas* cTrackingPos = TCanvas::MakeDefCanvas();
	// hTrackingEffTOFTPC


	// ---------------------------
	//  COMPARE PRIMARY FRACTIONS
	// ---------------------------

	// TFile* fPrimFractionsPionCombined = TFile::Open(gFileNamePrimFractionsPionCombined);
	// TCanvas* fPrimFractionsPionCombined->Get("c1;1");

	// lTrackingEffPosCombined->ls();
	// cvsTrackingEffCombined->Draw();
	// fTrackingEffPosCombined->ls();



	// Draw ratio to spline interpolation.


	// Test with the graph
	// AliFigureDiHadronPID* figTest = new AliFigureDiHadronPID("figTest", "figTest");
	// AliGraphDiHadronPID* test1 = new AliGraphDiHadronPID(hSpectraRelativisticRise[0]);
	// figTest->AddGraph(test1);
	// AliGraphDiHadronPID* test2 = new AliGraphDiHadronPID(hSpectraCombined[0]);
	// figTest->AddGraph(test2);
	// AliGraphDiHadronPID* test3 = new AliGraphDiHadronPID(hSpectraTOFTPCFit[0]);
	// figTest->AddGraph(test3);
	// figTest->Show(1,1,0);

}
