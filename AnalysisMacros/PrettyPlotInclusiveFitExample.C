/**
 * \file PrettyPlotInclusiveFitExample.C 
 * \brief Make a pretty plot of the TOF,TPC inclusive fit (for thesis, etc.)
 * \author Misha Veldhoen (misha.veldhoen@cern.ch)
 */

#include "TH1.h"
#include "TLegend.h"
#include "TFile.h"
#include "TLatex.h"
#include "TMath.h"
#include "TLatex.h"
#include "TBox.h"
#include "TGaxis.h"

#include "../Libs/AliPtEtaSpectrumDiHadronPID.h"
#include "../Libs/AliTOFTPCFitContainerDiHadronPID.h"
#include "../Libs/AliTOFTPCFitFunctionDiHadronPID.h"
#include "../Libs/AliFunctionsDiHadronPID.h"
//#include "../Libs/AliStyleDiHadronPID.h"
#include "PrettyPlotUtils.C"

const char* gCollisionSystemPaths[3] = {"./PbPb_Data_4GeV_NAS2_Mask5_TailK_0.009", "./pPb_Data_4GeV_NAS2_Mask5_TailK_0.009", "./pp_Data_4GeV_NAS2_Mask5_TailK_0.009"};

// Settings.
const Int_t gBoxLineWidth = 3;
const Int_t gFontSize = gPlotTextSize;
const Int_t gFont = 43;

const Int_t gSpecies = 0;
const Int_t ptBins[2] = {5,15};
const Int_t etaBins[2] = {9,9};
const Double_t gPlotRangeTOFFigure[2] = {0.4,8.e4};

const Bool_t gChangeTOFLegendToNanoSeconds = kTRUE;

const char* speciesNamesLaTeX[3] = {"#pi", "K", "p"};
const Color_t speciesColor[3] = {kRed, kGreen, kBlue};

// Forward declarations.
void PlotTOFProjection(const TH2F* data, const TH2F* mismatch, AliTOFTPCFitFunctionDiHadronPID* fitFunc, const char* baseName = "", Bool_t plotLegend = kFALSE, Bool_t plotYlegends = kTRUE);
void SetHistogramStyle(TH1* hist);
void DrawFitRegion(AliTOFTPCFitFunctionDiHadronPID* currentFunc);
void DrawPeakLegends(AliTOFTPCFitFunctionDiHadronPID* currentFunc);

void PrettyPlotInclusiveFitExample() {

    TH1::AddDirectory(0);
    SetMyStyle("half");
    //gStyle->SetLineWidth(2);
    if (gChangeTOFLegendToNanoSeconds) {TGaxis::SetExponentOffset(1.,1.,"x");}

    // Instance of TLatex.
    TLatex* latex = new TLatex(); 
    latex->SetTextSize(gPlotTextSize);

    // Make canvases.
    TCanvas* cvsTPCTOF = new TCanvas("toftpc", "toftpc", 1000, 500);
    TPad* pad1 = new TPad("pad1", "pad1", 0.,0.,0.52,1.0);
    pad1->SetNumber(1); 
    pad1->SetLeftMargin(0.15);
    pad1->SetTopMargin(0.01);
    pad1->SetRightMargin(0.01);
    pad1->SetBottomMargin(0.15);
    pad1->SetTicks(1,1);
    pad1->Draw();

    TPad* pad2 = new TPad("pad2", "pad2", 0.52,0.,1.,1.);
    pad2->SetNumber(2);
    pad2->SetLeftMargin(0.05);
    pad2->SetTopMargin(0.01);
    pad2->SetRightMargin(0.01);
    pad2->SetBottomMargin(0.15);
    pad2->SetTicks(1,1);
    pad2->Draw(); 

    TCanvas* cvsTOF = new TCanvas("tof", "tof", 1000, 500);
    TPad* pad3 = new TPad("pad1", "pad1", 0.,0.,0.52,1.0);
    pad3->SetNumber(1); 
    pad3->SetLeftMargin(0.15);
    pad3->SetTopMargin(0.01);
    pad3->SetRightMargin(0.01);
    pad3->SetBottomMargin(0.15);
    pad3->SetTicks(1,1);
    pad3->Draw();

    TPad* pad4 = new TPad("pad2", "pad2", 0.52,0.,1.,1.);
    pad4->SetNumber(2);
    pad4->SetLeftMargin(0.05);
    pad4->SetTopMargin(0.01);
    pad4->SetRightMargin(0.01);
    pad4->SetBottomMargin(0.15);
    pad4->SetTicks(1,1);
    pad4->Draw(); 

    TFile* finPbPb = TFile::Open(Form("%s/PtEtaSpectra_%i.root", gCollisionSystemPaths[0], gSpecies));
    TFile* finPP = TFile::Open(Form("%s/PtEtaSpectra_%i.root", gCollisionSystemPaths[2], gSpecies));

    if (finPbPb && finPP) {
        AliPtEtaSpectrumDiHadronPID* spectrumPbPb = (AliPtEtaSpectrumDiHadronPID*)finPbPb->Get(Form("Spec_%i", gSpecies)); 
        AliPtEtaSpectrumDiHadronPID* spectrumPP = (AliPtEtaSpectrumDiHadronPID*)finPP->Get(Form("Spec_%i", gSpecies)); 
        
        // Get the fit containers.
        AliTOFTPCFitContainerDiHadronPID* contPbPbLowPt = spectrumPbPb->GetFitContainer(ptBins[0],etaBins[0]);
        AliTOFTPCFitContainerDiHadronPID* contPbPbHighPt = spectrumPbPb->GetFitContainer(ptBins[1],etaBins[1]);
        AliTOFTPCFitContainerDiHadronPID* contPPLowPt = spectrumPP->GetFitContainer(ptBins[0],etaBins[0]);
        
        // Calculate the "shared" fit region + 10%.
        AliTOFTPCFitFunctionDiHadronPID* fitFunctionPbPbLowPt = contPbPbLowPt->LastFitStage();
        AliTOFTPCFitFunctionDiHadronPID* fitFunctionPbPbHighPt = contPbPbHighPt->LastFitStage();
        AliTOFTPCFitFunctionDiHadronPID* fitFunctionPPLowPt = contPPLowPt->LastFitStage();
        
        Double_t xMin = TMath::Min(fitFunctionPbPbLowPt->GetFitRegionTOFMin(), fitFunctionPbPbHighPt->GetFitRegionTOFMin());
        Double_t xMax = TMath::Max(fitFunctionPbPbLowPt->GetFitRegionTOFMax(), fitFunctionPbPbHighPt->GetFitRegionTOFMax());
        Double_t dX = xMax - xMin;
        xMin -= dX * 0.1;
        xMax += dX * 0.1;

        Double_t yMin = TMath::Min(fitFunctionPbPbLowPt->GetFitRegionTPCMin(), fitFunctionPbPbHighPt->GetFitRegionTPCMin());
        Double_t yMax = TMath::Max(fitFunctionPbPbLowPt->GetFitRegionTPCMax(), fitFunctionPbPbHighPt->GetFitRegionTPCMax());
        Double_t dY = yMax - yMin;
        yMin -= dY * 0.1;
        yMax += dY * 0.1;

        // DRAW (TOF, TPC) PLANE // 
        TH2F* hTOFTPCPbPbLowPt = contPbPbLowPt->GetDataHistogram();
        hTOFTPCPbPbLowPt->UseCurrentStyle();
        hTOFTPCPbPbLowPt->GetXaxis()->SetRangeUser(xMin, xMax);
        hTOFTPCPbPbLowPt->SetXTitle("");
        hTOFTPCPbPbLowPt->GetYaxis()->SetRangeUser(yMin, yMax);
        hTOFTPCPbPbLowPt->SetYTitle(Form("dE/dx - #LTdE/dx#GT_{%s} (a.u.)", speciesNamesLaTeX[gSpecies]));
        cvsTPCTOF->cd(1);
        gPad->SetLogz();
        SetHistogramStyle(hTOFTPCPbPbLowPt);
        hTOFTPCPbPbLowPt->Draw("col");
        DrawFitRegion(fitFunctionPbPbLowPt);
        DrawPeakLegends(fitFunctionPbPbLowPt); 

        TH2F* hTOFTPCPbPbHighPt = contPbPbHighPt->GetDataHistogram();
        hTOFTPCPbPbHighPt->UseCurrentStyle();
        hTOFTPCPbPbHighPt->GetXaxis()->SetRangeUser(xMin, xMax);
        hTOFTPCPbPbHighPt->SetXTitle(Form("#Delta t - #LT#Delta t#GT_{%s} (%ss)", speciesNamesLaTeX[gSpecies],
                    gChangeTOFLegendToNanoSeconds ? "n" : "p"));
        hTOFTPCPbPbHighPt->GetYaxis()->SetRangeUser(yMin, yMax);
        hTOFTPCPbPbHighPt->SetYTitle("");
        cvsTPCTOF->cd(2);
        gPad->SetLogz();
        SetHistogramStyle(hTOFTPCPbPbHighPt);
        hTOFTPCPbPbHighPt->GetYaxis()->SetLabelSize(0);
        hTOFTPCPbPbHighPt->Draw("col");

        DrawFitRegion(fitFunctionPbPbHighPt);
        DrawPeakLegends(fitFunctionPbPbHighPt); 
        cvsTPCTOF->SaveAs("../PrettyPictures/PbPbTOFTPCExample.pdf");

        TH2F* hTOFTPCPPLowPt = contPPLowPt->GetDataHistogram();
        hTOFTPCPPLowPt->GetXaxis()->SetRangeUser(-750, 5500);
        hTOFTPCPbPbLowPt->GetXaxis()->SetRangeUser(-750, 5500);
  
        // Use T instead of Dt - <Dt> 
//        hTOFTPCPPLowPt->SetXTitle(Form("#Delta t - #LT#Delta t#GT_{%s} (%ss)", speciesNamesLaTeX[gSpecies], 
//                    gChangeTOFLegendToNanoSeconds ? "n" : "p"));
        hTOFTPCPPLowPt->SetXTitle(Form("T_{%s} (%ss)", speciesNamesLaTeX[gSpecies], 
                    gChangeTOFLegendToNanoSeconds ? "n" : "p"));
//        hTOFTPCPbPbLowPt->SetXTitle(Form("#Delta t - #LT#Delta t#GT_{%s} (%ss)", speciesNamesLaTeX[gSpecies],
//                    gChangeTOFLegendToNanoSeconds ? "n" : "p"));
        hTOFTPCPbPbLowPt->SetXTitle(Form("T_{%s} (%ss)", speciesNamesLaTeX[gSpecies],
                    gChangeTOFLegendToNanoSeconds ? "n" : "p"));
        hTOFTPCPPLowPt->SetYTitle(Form("dE/dx - #LTdE/dx#GT_{%s} (a.u.)", speciesNamesLaTeX[gSpecies]));   

        // DRAW TOF PROJECTION // 
        TH2F* hTOFTPCMismatchPPLowPt = contPPLowPt->GetMismatchHistogram();
        TH2F* hTOFTPCMismatchPbPbLowPt = contPbPbLowPt->GetMismatchHistogram();
       
        cvsTOF->cd(1); 
        PlotTOFProjection(hTOFTPCPbPbLowPt, hTOFTPCMismatchPbPbLowPt, fitFunctionPbPbLowPt, "PbPb", kFALSE);
        latex->DrawLatexNDC(0.41,0.9,"PbPb (0-10%)");
        cvsTOF->cd(2);
        PlotTOFProjection(hTOFTPCPPLowPt, hTOFTPCMismatchPPLowPt, fitFunctionPPLowPt, "pp", kTRUE, kFALSE);
        latex->DrawLatexNDC(0.5,0.9,"pp");

        cvsTOF->SaveAs("../PrettyPictures/ppPbPbTOFExample.pdf");

        finPbPb->Close();
        finPP->Close();
    } else {return;}

}

void PlotTOFProjection(const TH2F* data, const TH2F* mismatch, AliTOFTPCFitFunctionDiHadronPID* fitFunc, const char* baseName, Bool_t plotLegend, Bool_t plotYlegends) {

    gPad->SetLogy();

    TAxis* tofAxis = data->GetXaxis();
    TAxis* tpcAxis = data->GetYaxis();
	Int_t tpcMinBin = data->GetYaxis()->FindBin(fitFunc->GetFitRegionTPCMin());
	Int_t tpcMaxBin = data->GetYaxis()->FindBin(fitFunc->GetFitRegionTPCMax());

	// Make a Legend.
	TLegend* legend = new TLegend(0.658635,0.585624,0.955823,0.957717);
    legend->SetTextSize(gLegendTextSize);
    legend->SetLineColor(kWhite);
	Double_t nLegendEntries = 3.;

	// Draw TOF projection of the data.
	TH1F* data_proj = (TH1F*)(data->ProjectionX(Form("%s",baseName), tpcMinBin, tpcMaxBin));
    data_proj->UseCurrentStyle();
    data_proj->Sumw2();
    data_proj->SetDirectory(0);
    data_proj->SetYTitle("Counts");    
    data_proj->SetTitle("");
	data_proj->SetLineColor(kBlack);
    SetHistogramStyle(data_proj);
    if (!plotYlegends) {data_proj->GetYaxis()->SetLabelSize(0);}
    data_proj->SetTitleOffset(1.25, "Y");

    data_proj->SetAxisRange(gPlotRangeTOFFigure[0], gPlotRangeTOFFigure[1], "Y");
    data_proj->Draw();
	legend->AddEntry(data_proj, "Data");

	// Draw the TOF projection of the mismatch.
	TH1F* mismatch_proj = (TH1F*)(mismatch->ProjectionX(Form("%s_mm",baseName), tpcMinBin, tpcMaxBin));
	mismatch_proj->SetDirectory(0);
	mismatch_proj->SetLineColor(kGray + 1);
	mismatch_proj->Scale(fitFunc->GetMismatchYield() / mismatch->GetEntries());
    mismatch_proj->Draw("same");
	legend->AddEntry(mismatch_proj, "Mismatch");

    // Total fit result histogram.
    TH1F* totfit = new TH1F(*mismatch_proj);
    totfit->SetName("totfit");
    totfit->SetDirectory(0);
    totfit->SetLineColor(kViolet);	

    // Determine norms of the included peak functions.
    Double_t norms[AliSettingsDiHadronPID::GetNSpecies()];
    for (Int_t iSpecies = 0; iSpecies < AliSettingsDiHadronPID::GetNSpecies(); iSpecies++) {
            
        norms[iSpecies] = 0.;
        AliTOFTPCPeakDiHadronPID* currentPeak = (AliTOFTPCPeakDiHadronPID*)fitFunc->GetPeak(iSpecies);
        if (currentPeak) {
            if (currentPeak->IsUsedInFit()) {norms[iSpecies] = currentPeak->Norm();}
        } 
    
    }

	// Draw the peaks.
    Double_t xy[2] = {0.,0.};	
    for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {

        // Get a ptr to the peak of species iSpecies.
        AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
        AliTOFTPCPeakDiHadronPID* currentPeak = fitFunc->GetPeak(currentSpecies);

        // Check if the peak was used in the fit.
        if (!currentPeak->IsUsedInFit()) {continue;}

        // Create a histogram of the current peak.
        TH1F* hist = (TH1F*)mismatch_proj->Clone(Form("TOF_%i", iSpecies));
        hist->SetDirectory(0);
        hist->SetLineColor(speciesColor[currentSpecies]);

        // Fill the projection histogram.
        for (Int_t iBinTOF = 1; iBinTOF <= hist->GetNbinsX(); iBinTOF++) {

            Double_t binContent = 0.;

            //xy[0] = tofAxis->GetBinCenter(iBinTOF);

            for (Int_t iBinTPC = tpcMinBin; iBinTPC <= tpcMaxBin; iBinTPC++) {

                //xy[1] = tpcAxis->GetBinCenter(iBinTPC);

                //binContent += (*currentPeak)(xy);  //fitFunc->EvalSpecies(tofAxis->GetBinCenter(iBinTOF), tpcAxis->GetBinCenter(iBinTPC), iSpecies);
                binContent += currentPeak->Integral(tofAxis->GetBinLowEdge(iBinTOF), tofAxis->GetBinUpEdge(iBinTOF),
                        tpcAxis->GetBinLowEdge(iBinTPC), tpcAxis->GetBinUpEdge(iBinTPC));
            }

            hist->SetBinContent(iBinTOF, binContent * norms[iSpecies]);

        }

        legend->AddEntry(hist->DrawCopy("same"), speciesNamesLaTeX[currentSpecies]);
        nLegendEntries += 1.;
        totfit->Add(hist);
        delete hist;
    
    }

	/*legend->AddEntry(totfit->DrawCopy("same"), "Total Fit");*/
	/*delete totfit;*/

	/*delete mismatch_proj;*/

	/*legend->SetY1(0.9 - nLegendEntries * 0.04);*/
    if(plotLegend) {legend->Draw();}

   // return data_proj;

}

void SetHistogramStyle(TH1* hist) {
/*
    hist->SetLineWidth(2);

    // Style of the labels.
  
    hist->SetLabelFont(gFont,"X");
    hist->SetLabelFont(gFont,"Y");
    hist->SetLabelSize(gFontSize, "X");
    hist->SetLabelSize(gFontSize, "Y");
    hist->SetLabelOffset(0.02,"X");
    hist->SetLabelOffset(0.02,"Y");

    // Style of the title (also axis titles!). 
    
    hist->SetTitleFont(gFont, "X");
    hist->SetTitleFont(gFont,"Y");
    hist->SetTitleSize(gFontSize, "X");
    hist->SetTitleSize(gFontSize, "Y");
    hist->SetTitleOffset(1.3, "X");
    hist->SetTitleOffset(1.15, "Y");
    //hist->SetTitleOffset(0.95, "Y");

    hist->GetXaxis()->SetNdivisions(505);
    hist->GetYaxis()->SetNdivisions(505);
    */
}

void DrawFitRegion(AliTOFTPCFitFunctionDiHadronPID* currentFunc) {

    TBox* fitregion = new TBox(currentFunc->GetFitRegionTOFMin(), currentFunc->GetFitRegionTPCMin(), currentFunc->GetFitRegionTOFMax(), currentFunc->GetFitRegionTPCMax());
    fitregion->SetLineColor(kBlack);
    fitregion->SetLineWidth(2);
    fitregion->SetFillStyle(1);
    fitregion->SetLineWidth(gBoxLineWidth);
    fitregion->Draw("same");

}

void DrawPeakLegends(AliTOFTPCFitFunctionDiHadronPID* currentFunc) {

    for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {

        AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
        AliTOFTPCPeakDiHadronPID* currentPeak = currentFunc->GetPeak(currentSpecies);

        // Check if the peak was used in the fit.
        if (!currentPeak->IsUsedInFit()) {continue;}

        // Draw markers.
        TLatex* marker = new TLatex(currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTOF) - 70, currentPeak->GetParValue(AliTOFTPCPeakDiHadronPID::kMuTPC) - 1, speciesNamesLaTeX[iSpecies]);
        //marker->SetTextFont(gFont);
        marker->SetTextSize(gFontSize);
        marker->Draw("same");

    }
}

