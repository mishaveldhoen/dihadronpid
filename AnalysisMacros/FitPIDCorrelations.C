/**
 * \file FitPIDCorrelations.C
 * \brief Fits PID correlations using templates.
 * 
 * This macro uses the template fitter to fit PID correlations.
 *  - If oneBin is set to kTRUE, then only one bin is fitted, otherwise all are fitted.
 *  - If oneBin is set, no outputfiles are created, but the plots are recreated.
 *
 * The memory usage of this macro is fairly excessive, which is mainly due to loading 4D histograms.
 * On OSX it seems to use about 1.1G real and 1.2G virtual on top of the base usage of aliroot.
 * On top of that, memory consumption slowly rises when running this macro (seems to be about 400MB/30min),
 * which is most likely due to the fact that histograms are constantly being created (TOF projection, etc.),
 * and drawn, but never thrown out. Most likely this will not cause any crashes, but if it does an option
 * may be to not draw histograms in a canvas, but only save to file. That way the user can still delete the
 * histograms.
 */

#include "../Libs/AliLogDiHadronPID.h"
#include "../Libs/AliSettingsDiHadronPID.h"
#include "../Libs/AliMiscToolsDiHadronPID.h"
#include "../Libs/AliHistToolsDiHadronPID.h"
#include "../Libs/AliTemplateFitterDiHadronPID.h"
#include "../Libs/AliFitTemplatesDiHadronPID.h"
#include "TFile.h"
#include "TCanvas.h"
#include "THn.h"
#include "TH1.h"
#include "TString.h"
#include "TStopwatch.h"

// -----------------------------------------------------------------------
void FitPIDCorrelations(Int_t massAssumption, Bool_t oneBin = kFALSE, Int_t chosenPtBin = -1, Int_t chosendEtaBin = -1, Int_t chosendPhiBin = -1) {
	
	TH1::AddDirectory(kTRUE);

	AliSettingsDiHadronPID::Species currentMassAssumption = (AliSettingsDiHadronPID::Species)massAssumption;

	// Get a ptr to the settings.
	AliSettingsDiHadronPID* settings = AliSettingsDiHadronPID::Instance();

	// Open files.// (dPhi, DEta, TOF, TPC).
	TFile* finCorrelationData = TFile::Open(settings->GetPIDCorrelationsFileNameForSpecies((AliSettingsDiHadronPID::Species)massAssumption));
	if (!finCorrelationData) {
		AliLogDiHadronPID::Error(Form("Could not open correlations file."), "FitPIDCorrelations", "");
		return;
	}

	TFile* finTemplates = TFile::Open(settings->GetTemplatesFileNameForSpecies((AliSettingsDiHadronPID::Species)massAssumption));
	if (!finTemplates) {
		AliLogDiHadronPID::Error(Form("Could not open templates file."), "FitPIDCorrelations", "");
		finCorrelationData->Close();
		return;
	}

	// Check if the right templates are available in the templates file.
	TString specNameToLower = TString(settings->GetSpeciesName((AliSettingsDiHadronPID::Species)massAssumption));
	specNameToLower.ToLower();

	// Get a ptr to the templates object.
	AliFitTemplatesDiHadronPID* templates = (AliFitTemplatesDiHadronPID*)finTemplates->Get(Form("%sTemplates", specNameToLower.Data()));
	if (!templates) {
		AliLogDiHadronPID::Error(Form("Could not find templates object."), "FitPIDCorrelations", "");
		finCorrelationData->Close();
		finTemplates->Close();
		return;
	}

	// Create an output file.
	TFile* fout = 0x0;
	if (!oneBin) {fout = TFile::Open(settings->GetPIDCorrelationsFileNameForSpeciesFitted((AliSettingsDiHadronPID::Species)massAssumption), "recreate");}

	// Start the timer.
	TStopwatch sw;
	sw.Start();

	// Determine the range of the pt bins that are fitted.
	const TAxis* ptAxis = settings->GetPtAxis();
	Int_t ptMinBin = 1;
	Int_t ptMaxBin = ptAxis->GetNbins();
	if (oneBin && chosenPtBin >= ptMinBin && chosenPtBin <= ptMaxBin) {
		ptMinBin = chosenPtBin;
		ptMaxBin = chosenPtBin;
	}

	// Create some objects used throughout the following loops.
	TCanvas* currentCanvas = TCanvas::MakeDefCanvas();
	TObjArray* peakArray = new TObjArray(6);
	AliTemplateFitterDiHadronPID* templateFitter = new AliTemplateFitterDiHadronPID();

	// Loop over all pT bins.
	for (Int_t iPtBin = ptMinBin; iPtBin <= ptMaxBin; iPtBin++) {
		
		// Get ptrs to correlation histograms.
		THnF* correlation = (THnF*)finCorrelationData->Get(Form("CorrelationsPID_pt%i", iPtBin));
		TAxis* dPhiAxis = correlation->GetAxis(0);
		TAxis* dEtaAxis = correlation->GetAxis(1);

		THnF* mixedevents = (THnF*)finCorrelationData->Get(Form("MixedEventsPID_pt%i", iPtBin));
		TAxis* dPhiAxisMixed = mixedevents->GetAxis(0);
		TAxis* dEtaAxisMixed = mixedevents->GetAxis(1);

		// Create histograms for di-hadron signal and mixed events.
		TH2F* diHadronPions = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("diHadronPionsPtBin%i", iPtBin), Form("diHadronPionsPtBin%i", iPtBin), "F", dPhiAxis, dEtaAxis);
		TH2F* mixedPions = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("mixedPionsPtBin%i", iPtBin), Form("mixedPionsPtBin%i", iPtBin), "F", dPhiAxis, dEtaAxis);

		TH2F* diHadronKaons = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("diHadronKaonsPtBin%i", iPtBin), Form("diHadronKaonsPtBin%i", iPtBin), "F", dPhiAxis, dEtaAxis);
		TH2F* mixedKaons = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("mixedKaonsPtBin%i", iPtBin), Form("mixedKaonsPtBin%i", iPtBin), "F", dPhiAxis, dEtaAxis);

		TH2F* diHadronProtons = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("diHadronProtonsPtBin%i", iPtBin), Form("diHadronProtonsPtBin%i", iPtBin), "F", dPhiAxis, dEtaAxis);
		TH2F* mixedProtons = (TH2F*)AliHistToolsDiHadronPID::MakeHist(Form("mixedProtonsPtBin%i", iPtBin), Form("mixedProtonsPtBin%i", iPtBin), "F", dPhiAxis, dEtaAxis);

		// Get the mean Pt.
		Double_t meanPt = ptAxis->GetBinCenter(iPtBin);

		// Determine the range of the bins in Dphi and dEta that we want to fit.
		Int_t dPhiMinBin = 1;
		Int_t dPhiMaxBin = dPhiAxis->GetNbins();
		if (oneBin && chosendPhiBin >= dPhiMinBin && chosendPhiBin <= dPhiMaxBin) {
			dPhiMinBin = chosendPhiBin;
			dPhiMaxBin = chosendPhiBin;
		}

		Int_t dEtaMinBin = 1;
		Int_t dEtaMaxBin = dEtaAxis->GetNbins();
		if (oneBin && chosendEtaBin >= dEtaMinBin && chosendEtaBin <= dEtaMaxBin) {
			dEtaMinBin = chosendEtaBin;
			dEtaMaxBin = chosendEtaBin;
		}

		for (Int_t iDPhiBin = dPhiMinBin; iDPhiBin <= dPhiMaxBin; iDPhiBin++) {
			for (Int_t iDEtaBin = dEtaMinBin; iDEtaBin <= dEtaMaxBin; iDEtaBin++) {

				cout << "pT bin: " << iPtBin << " dPhi bin: " << iDPhiBin << " dEta bin: " << iDEtaBin << endl;

				// Change this range to get the correct projections.
				dPhiAxis->SetRange(iDPhiBin, iDPhiBin);
				dEtaAxis->SetRange(iDEtaBin, iDEtaBin);

				dPhiAxisMixed->SetRange(iDPhiBin, iDPhiBin);
				dEtaAxisMixed->SetRange(iDEtaBin, iDEtaBin);

				// Make an array of the peak templates. Add only with the ones that are included.
				peakArray->Clear(); // peakArray does not own the templates!
				for (Int_t iSpecies = 0; iSpecies < settings->GetNSpecies(); iSpecies++) {

					AliSettingsDiHadronPID::Species currentSpecies = (AliSettingsDiHadronPID::Species)iSpecies;
						
					if (settings->IsPeakIncludedInFit(currentMassAssumption, currentSpecies, meanPt)) {
						peakArray->AddAt((TH2F*)templates->GetPeakTemplate(iPtBin, iDEtaBin, currentSpecies), iSpecies);
					}

				}

				// We do not own these templates!
				TH2F* mismatchTemplate = (TH2F*)(templates->GetMismatchTemplate(iPtBin, iDEtaBin));
				// mismatchTemplate->SetDirectory(0);

				//cout << "Mismatch template sum: " << AliHistToolsDiHadronPID::GetSumHist(mismatchTemplate) << endl; 

				// --------------------
				// FIT THE MIXED EVENTS
				// --------------------
				// We do own the projections.
				TH2F* mixed = (TH2F*)(mixedevents->Projection(3,2));
				mixed->SetDirectory(0);

				// Set The axes titles (not very neat, should be done in process data).
				(mixed->GetXaxis())->SetTitle(settings->GetTOFAxis((AliSettingsDiHadronPID::Species)massAssumption)->GetTitle());
				(mixed->GetYaxis())->SetTitle(settings->GetTPCAxis((AliSettingsDiHadronPID::Species)massAssumption)->GetTitle());

				templateFitter->Set(currentMassAssumption, meanPt, mixed, peakArray, mismatchTemplate);
				Int_t fitstatus = templateFitter->Fit();
				
				mixedPions->SetBinContent(iDPhiBin, iDEtaBin, templateFitter->GetFitParameters()[0]);
				mixedKaons->SetBinContent(iDPhiBin, iDEtaBin, templateFitter->GetFitParameters()[1]);
				mixedProtons->SetBinContent(iDPhiBin, iDEtaBin, templateFitter->GetFitParameters()[2]);
				
				mixedPions->SetBinError(iDPhiBin, iDEtaBin, templateFitter->GetFitErrors()[0]);
				mixedKaons->SetBinError(iDPhiBin, iDEtaBin, templateFitter->GetFitErrors()[1]);
				mixedProtons->SetBinError(iDPhiBin, iDEtaBin, templateFitter->GetFitErrors()[2]);

				// Draw the fits to file.
				TString mmDir = (Form("./MixedEventFits/Sp%i/PtBin%02i/DEtaBin%02i/DPhiBin%02i/", massAssumption, iPtBin, iDEtaBin, iDPhiBin));
				AliMiscToolsDiHadronPID::CreateDir(mmDir);
				TString mmTOFFile = mmDir + "TOFfit.pdf";
				TString mmTOFFileNL = mmDir + "TOFfitNoLog.root";
				TString mmTPCFile = mmDir + "TPCfit.pdf";
				TString mmTPCFileNL = mmDir + "TPCfitNoLog.root";
				TString mmTOFTPCFile = mmDir + "TOFTPCfit.pdf";

				// TCanvas* tmpCanvas = 0x0;
				
				templateFitter->DrawTOFProjection(currentCanvas);
				currentCanvas->SaveAs(mmTOFFile.Data());
				currentCanvas->SetLogy(kFALSE); 
				currentCanvas->SaveAs(mmTOFFileNL.Data());
				
				currentCanvas = templateFitter->DrawTPCProjection(currentCanvas);
				currentCanvas->SaveAs(mmTPCFile.Data());
				currentCanvas->SetLogy(kFALSE); 
				currentCanvas->SaveAs(mmTPCFileNL.Data());				
				
				currentCanvas = templateFitter->DrawFitData(currentCanvas);
				currentCanvas->SaveAs(mmTOFTPCFile.Data());

				// Get the mismatch fraction from the mixed events.
				Double_t mmFracFromME = (templateFitter->GetFitParameters()[settings->GetNSpecies()]) / AliHistToolsDiHadronPID::GetSumHist(mixed);
				cout << "Mismatch fraction from ME: " << mmFracFromME << endl;

				// If only one bin is fitted, then assume that more detailed output is desired.
				if (oneBin) {
					templateFitter->Print(0);
					templateFitter->PrintHessian();
				}

				delete mixed; mixed = 0x0;

				// ----------------------
				// FIT THE DATA HISTOGRAM
				// ----------------------
				TH2F* data = (TH2F*)(correlation->Projection(3,2));
				data->SetDirectory(0);

				// Set The axes titles (not very neat, should be done in process data).
				(data->GetXaxis())->SetTitle(settings->GetTOFAxis((AliSettingsDiHadronPID::Species)massAssumption)->GetTitle());
				(data->GetYaxis())->SetTitle(settings->GetTPCAxis((AliSettingsDiHadronPID::Species)massAssumption)->GetTitle());

				// Give the mismatch fraction measured in the mixed events to the data fit.
				// However in case the mixed events didn't have any mismatches, this won't work.
				if (mmFracFromME < 1.e-5) {mmFracFromME = -1.;}
				templateFitter->Set(currentMassAssumption, meanPt, data, peakArray, mismatchTemplate, mmFracFromME);
				fitstatus = templateFitter->Fit();
				diHadronPions->SetBinContent(iDPhiBin, iDEtaBin, templateFitter->GetFitParameters()[0]);
				diHadronKaons->SetBinContent(iDPhiBin, iDEtaBin, templateFitter->GetFitParameters()[1]);			
				diHadronProtons->SetBinContent(iDPhiBin, iDEtaBin, templateFitter->GetFitParameters()[2]);			

				diHadronPions->SetBinError(iDPhiBin, iDEtaBin, templateFitter->GetFitErrors()[0]);
				diHadronKaons->SetBinError(iDPhiBin, iDEtaBin, templateFitter->GetFitErrors()[1]);			
				diHadronProtons->SetBinError(iDPhiBin, iDEtaBin, templateFitter->GetFitErrors()[2]);	

				// Draw the fits to file.
				TString dataDir = (Form("./DiHadronFits/Sp%i/PtBin%02i/DEtaBin%02i/DPhiBin%02i/", massAssumption, iPtBin, iDEtaBin, iDPhiBin));
				AliMiscToolsDiHadronPID::CreateDir(dataDir);
				TString dataTOFFile = dataDir + "TOFfit.pdf";
				TString dataTPCFile = dataDir + "TPCfit.pdf";
				TString dataTOFTPCFile = dataDir + "TOFTPCfit.pdf";

				templateFitter->DrawTOFProjection(currentCanvas);
				currentCanvas->SaveAs(dataTOFFile.Data());
				
				templateFitter->DrawTPCProjection(currentCanvas);
				currentCanvas->SaveAs(dataTPCFile.Data());
				
				templateFitter->DrawFitData(currentCanvas);
				currentCanvas->SetLogy(kFALSE); 
				currentCanvas->SaveAs(dataTOFTPCFile.Data());

				// If only one bin is fitted, then assume that more detailed output is desired.
				if (oneBin) {
					templateFitter->Print(0);
					templateFitter->PrintHessian();
				}

				delete data; data = 0x0;

			}
			
		}

		// Write the fit result to file, if an output file has been opened.
		if (fout) {

			fout->cd();

			diHadronPions->Write();
			diHadronKaons->Write();	
			diHadronProtons->Write();

			TH2F* mixedNoScalingPions = (TH2F*)(mixedPions->Clone(Form("mixedNoScalingPionsPtBin%i", iPtBin)));
			mixedNoScalingPions->Write();

			TH2F* mixedNoScalingKaons = (TH2F*)(mixedKaons->Clone(Form("mixedNoScalingKaonsPtBin%i", iPtBin)));
			mixedNoScalingKaons->Write();

			TH2F* mixedNoScalingProtons = (TH2F*)(mixedProtons->Clone(Form("mixedNoScalingProtonsPtBin%i", iPtBin)));
			mixedNoScalingProtons->Write();

			AliHistToolsDiHadronPID::NormaliseMixedEvents(mixedPions, kTRUE);
			AliHistToolsDiHadronPID::NormaliseMixedEvents(mixedKaons, kTRUE);
			AliHistToolsDiHadronPID::NormaliseMixedEvents(mixedProtons, kTRUE);

			mixedPions->Write();
			mixedKaons->Write();	
			mixedProtons->Write();

			TH2F* correctedPions = (TH2F*)(diHadronPions->Clone(Form("correctedPionsPtBin%i", iPtBin)));
			correctedPions->Divide(mixedPions);
			correctedPions->Write();

			TH2F* correctedKaons = (TH2F*)(diHadronKaons->Clone(Form("correctedKaonsPtBin%i", iPtBin)));
			correctedKaons->Divide(mixedKaons);
			correctedKaons->Write();

			TH2F* correctedProtons = (TH2F*)(diHadronProtons->Clone(Form("correctedProtonsPtBin%i", iPtBin)));
			correctedProtons->Divide(mixedProtons);
			correctedProtons->Write();
		
			// Clones should be deleted (?)
			if (mixedNoScalingPions) {delete mixedNoScalingPions; mixedNoScalingPions = 0x0;}
			if (mixedNoScalingKaons) {delete mixedNoScalingKaons; mixedNoScalingKaons = 0x0;}
			if (mixedNoScalingProtons) {delete mixedNoScalingProtons; mixedNoScalingProtons = 0x0;}

			// Histograms created with AliHistToolsDiHadronPID::MakeHist should be deleted.
			if (correctedPions) {delete correctedPions; correctedPions = 0x0;}
			if (correctedKaons) {delete correctedKaons; correctedKaons = 0x0;}
			if (correctedProtons) {delete correctedProtons; correctedProtons = 0x0;}

		}

		// Delete data histograms.
		if (diHadronPions) {delete diHadronPions; diHadronPions = 0x0;}
		if (diHadronKaons) {delete diHadronKaons; diHadronKaons = 0x0;}
		if (diHadronProtons) {delete diHadronProtons; diHadronProtons = 0x0;}

		// Delete data histograms.
		if (mixedPions) {delete mixedPions; mixedPions = 0x0;}
		if (mixedKaons) {delete mixedKaons; mixedKaons = 0x0;}
		if (mixedProtons) {delete mixedProtons; mixedProtons = 0x0;}

		// Delete the 4D histograms for this pT bin.
		if (correlation) {delete correlation; correlation = 0x0;}
		if (mixedevents) {delete mixedevents; mixedevents = 0x0;}
		
	}

	if (templates) {delete templates; templates = 0x0;}

	if (finCorrelationData) {finCorrelationData->Close();}
	if (finTemplates) {finTemplates->Close();}
	if (fout) {fout->Close();}

	cout << "Analysis time: " << AliMiscToolsDiHadronPID::FormatTime(sw.RealTime()).Data() << endl;	

}
