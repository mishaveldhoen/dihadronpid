#!/bin/bash

# This script downloads the results from the associated yield analysis from quark (both root files and pdfs). 
# Run this macro from inside an analysis directory! (i.e., the one with the config.C file)
# One can pass any additional commands to the script, for example --dry-run ($1).

/usr/local/bin/rsync -avzru $1 veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/AnalysisMacros/${PWD##*/}/AssociatedYieldAnalysis .

