#!/bin/bash

# a = Archive, meaning that creating time, owner, etc. are preserved.

# quark -> local
#  - Copy analysis macros to quark (not recursive).
/usr/local/bin/rsync -avzu veldh122@quark.science.uu.nl:/home/staff/leeuw179/pythia/dihadron_pid/pythia_dihadrons_7000GeV_merged.root .
/usr/local/bin/rsync -avzu veldh122@quark.science.uu.nl:/home/staff/leeuw179/pythia/dihadron_pid/plot_dihadron.C .
