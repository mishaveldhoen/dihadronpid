/**
 * \file   analyze_dihadron.C
 * \brief  Analyzes the MC di-hadron analysis, in the same way as AnalyzeAssociatedYield.C
 * \author Misha Veldhoen (misha.veldhoen@cern.ch)
 *
 * Due to the file format being slightly different, and no need for efficiency correction, etc, 
 * we have a separate macro to analyze the MC generated associated yield distributions. For this
 * reason, the macro AnalyzeAssociatedYield.C is included, and its parameters are used.
 *
 * run:
 *  aliroot -b -q config.C analyze_dihadron.C
 *
 */

#include "TH3.h"
#include "TH2.h"
#include "TH1.h"
#include "TCanvas.h"
#include "TFile.h"
#include "AliHistToolsDiHadronPID.h"
#include "../AnalysisMacros/AnalyzeAssociatedYield.C"
#include <iostream>

const char* gEnergy[3]={"2760","5020","7000"};

using namespace std;

const Int_t gDEtaBinOffset = 4;         // Histograms in pythia results start at Deta = -2.0 instead of -1.6
TAxis* gPtAxis = 0x0;

void analyze_dihadron(Double_t alpha = 3.0, Int_t energy = 0) {

    if (energy < 0 || energy > 2) {
        cout << "Choose energy level 0=2.76TeV, 1=5.02TeV or 2=7TeV" << endl;
        return;
    }

    gSettings = AliSettingsDiHadronPID::Instance();
    TH1::AddDirectory(0);

    // Create and configure an instance of the logger.
	AliLogDiHadronPID* logger = AliLogDiHadronPID::Instance();
	logger->SetVerboseLevel(1);
	logger->SetDebugLevel(0);

    // Load 3D histograms.
    TFile* fin = TFile::Open(Form("./%s/pythia_dihadrons_%s_merged.root", Form("%sGeV", gEnergy[energy]), Form("%sGeV",gEnergy[energy])));
    if (!fin) {
        cout << Form("ERROR: Could not open ./%s/pythia_dihadrons_%s_merged.root", Form("%sGeV",gEnergy[energy]), Form("%sGeV",gEnergy[energy]));
        return;
    }

    TH3F* hSame[3] = {0x0};
    TH3F* hMixed[3] = {0x0};

    for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
        hSame[iSpecies] = (TH3F*)fin->Get(Form("hDetaDphiPt%s", gSpeciesNames[iSpecies]));
        hMixed[iSpecies] = (TH3F*)fin->Get(Form("hDetaDphiPt%sMix", gSpeciesNames[iSpecies]));
        if (!hSame[iSpecies]) {
            cout << "ERROR: Could not obtain same-event distribution for " << gSpeciesNames[iSpecies] << endl;
            return;
        }
        if (!hMixed[iSpecies]) {
            cout << "ERROR: Could not obtain mixed-event distribution for " << gSpeciesNames[iSpecies] << endl;
            return;
        }
    }

    // Make projections on DPhi, DEta axis.
    Int_t nPtBins = hSame[0]->GetNbinsZ();
    gPtAxis = hSame[0]->GetZaxis();

    if (nPtBins > 100) {
        cout << "ERROR: There are more than 30 pt bins..." << endl;
        return;
    }
    TH2F* hSamePt[3][100];
    TH2F* hMixedPt[3][100];
    AliAssociatedYieldDiHadronPID* hAssociatedDistribution[3][100];

    for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
        for (Int_t iPtBin = 1; iPtBin <= nPtBins; ++iPtBin) {

            hSamePt[iSpecies][iPtBin-1] = new TH2F(Form("same_%i_%i", iSpecies, iPtBin), 
                                                 Form("same_%i_%i;#Delta#phi (rad); #Delta#eta", iSpecies, iPtBin),
                                                 32, -TMath::Pi()/2., 3.*TMath::Pi()/2.,
                                                 32, -1.6, 1.6);

            hMixedPt[iSpecies][iPtBin-1] = new TH2F(Form("mixed_%i_%i", iSpecies, iPtBin), 
                                                  Form("mixed_%i_%i;#Delta#phi (rad); #Delta#eta", iSpecies, iPtBin),
                                                  32, -TMath::Pi()/2., 3.*TMath::Pi()/2.,
                                                  32, -1.6, 1.6);

            for (Int_t iDPhiBin = 1; iDPhiBin <= hSame[0]->GetNbinsX(); ++iDPhiBin) {
                for (Int_t iDEtaBin = 1+gDEtaBinOffset; iDEtaBin <= hSame[0]->GetNbinsX()-gDEtaBinOffset; ++iDEtaBin) {
                    hSamePt[iSpecies][iPtBin-1]->SetBinContent(iDPhiBin, iDEtaBin - gDEtaBinOffset, 
                            hSame[iSpecies]->GetBinContent(iDEtaBin, iDPhiBin, iPtBin));
                    hSamePt[iSpecies][iPtBin-1]->SetBinError(iDPhiBin, iDEtaBin - gDEtaBinOffset, 
                            hSame[iSpecies]->GetBinError(iDEtaBin, iDPhiBin, iPtBin));

                    hMixedPt[iSpecies][iPtBin-1]->SetBinContent(iDPhiBin, iDEtaBin - gDEtaBinOffset, 
                            hMixed[iSpecies]->GetBinContent(iDEtaBin, iDPhiBin, iPtBin));
                    hMixedPt[iSpecies][iPtBin-1]->SetBinError(iDPhiBin, iDEtaBin - gDEtaBinOffset, 
                            hMixed[iSpecies]->GetBinError(iDEtaBin, iDPhiBin, iPtBin));
                }
            }

            // Normalize mixed events.
			AliHistToolsDiHadronPID::NormaliseMixedEvents(hMixedPt[iSpecies][iPtBin-1], "all");
            hSamePt[iSpecies][iPtBin-1]->Divide(hMixedPt[iSpecies][iPtBin-1]);

            // Create associated yield distribution.
            hAssociatedDistribution[iSpecies][iPtBin-1] = new AliAssociatedYieldDiHadronPID(*hSamePt[iSpecies][iPtBin-1]);

        }
    }

    // Perform analysis.
	AliMiscToolsDiHadronPID::CreateDir(Form("%s/%s_Alpha%3.1f", Form("%sGeV",gEnergy[energy]),gOutputBaseDirectory, alpha));
	TFile* fOut = TFile::Open(Form("%s/%s_Alpha%3.1f/%s", Form("%sGeV",gEnergy[energy]), gOutputBaseDirectory, alpha, gOutputFileName), "recreate");

    // FROM HERE MOSTLY COPIED FROM AnalyzeAssociatedYield.C
	for (Int_t iSpecies = 0; iSpecies < 3; iSpecies++) {
	    AliMiscToolsDiHadronPID::CreateDir(Form("%s/%s_Alpha%3.1f/Sp%i", Form("%sGeV",gEnergy[energy]),gOutputBaseDirectory, alpha, iSpecies));

		// Create some pT dependent histograms (jet yield, peak shape, flow, etc.).
		TH1F* hNSJetYield = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("hNSJetYield%s", gSpeciesNames[iSpecies]), Form("hNSJetYield%s;p_{T}", gSpeciesNames[iSpecies]),"F", gPtAxis);
		TH1F* hNSJetDPhiWidth = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("hNSJetDPhiWidth%s", gSpeciesNames[iSpecies]), Form("hNSJetDPhiWidth%s;p_{T}", gSpeciesNames[iSpecies]),"F", gPtAxis);
		TH1F* hNSJetDEtaWidth = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("hNSJetDEtaWidth%s", gSpeciesNames[iSpecies]), Form("hNSJetDEtaWidth%s;p_{T}", gSpeciesNames[iSpecies]),"F", gPtAxis);
		TH1F* hNSJetDEtaWidth1 = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("hNSJetDEtaWidth1%s", gSpeciesNames[iSpecies]), Form("hNSJetDEtaWidth1%s;p_{T}", gSpeciesNames[iSpecies]),"F", gPtAxis);
		TH1F* hNSJetDEtaWidth2 = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("hNSJetDEtaWidth2%s", gSpeciesNames[iSpecies]), Form("hNSJetDEtaWidth2%s;p_{T}", gSpeciesNames[iSpecies]),"F", gPtAxis);
		TH1F* hNSJetDEtaFrac = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("hNSJetDEtaFrac%s", gSpeciesNames[iSpecies]), Form("hNSJetDEtaFrac%s;p_{T}", gSpeciesNames[iSpecies]),"F", gPtAxis);	
		TH1F* hBackgroundDensity = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("hBackgroundDensity%s", gSpeciesNames[iSpecies]), Form("hBackgroundDensity%s;p_{T}", gSpeciesNames[iSpecies]),"F", gPtAxis);
		TH1F* hVnDelta[16] = {0x0};
		for (Int_t iFlowCoefficient = 1; iFlowCoefficient <= gMaxFlowSubtracted[gSettings->GetBeamType()]; iFlowCoefficient++) {
			hVnDelta[iFlowCoefficient-1] = (TH1F*)AliHistToolsDiHadronPID::MakeHist(Form("hV%dDelta%s", iFlowCoefficient, gSpeciesNames[iSpecies]), Form("hV%dDelta%s", iFlowCoefficient, gSpeciesNames[iSpecies]),"F", gPtAxis);
		}

        TCanvas* cvs = TCanvas::MakeDefCanvas();

		for (Int_t iPtBin = 1; iPtBin <= nPtBins; iPtBin++) {

			// Set the range of the DEta axis. (can be removed I think).
			TAxis* aDEta = hAssociatedDistribution[iSpecies][iPtBin - 1]->GetYaxis();
			Int_t minBinDEta = aDEta->FindBin(-(gMaxDEta - 0.0001));
			Int_t maxBinDEta = aDEta->FindBin(gMaxDEta - 0.0001);
			aDEta->SetRange(minBinDEta, maxBinDEta);
	
			// Scale with binwidth (create a density).
			cout << "Total number of entries: " << hAssociatedDistribution[iSpecies][iPtBin - 1]->GetEntries() << endl;
            hAssociatedDistribution[iSpecies][iPtBin - 1]->DivideThroughNTriggers();
			hAssociatedDistribution[iSpecies][iPtBin - 1]->Scale(1., "width");
            
            // Calculate the appropriate peak radius, based on species and average pT.
            // NOTE: Previously we used an exponential here, but a 2nd order polynomial seems to work better. The exponential prediction is still calculated, however only the polynomial is used.
            Double_t currentPeakRadiusExp = TMath::Min(alpha * TMath::Exp(gPeakRadiusExpSlope[iSpecies] * gPtAxis->GetBinCenter(iPtBin) + gPeakRadiusExpConst[iSpecies]), gMaxPeakRadius);

            Double_t currentJetRadius = gPeakRadiusA0[iSpecies] + gPeakRadiusA1[iSpecies]*gPtAxis->GetBinCenter(iPtBin) + gPeakRadiusA2[iSpecies]*TMath::Power(gPtAxis->GetBinCenter(iPtBin), 2);
            Double_t currentPeakRadius = TMath::Min(alpha * currentJetRadius, gMaxPeakRadius);

            cout << "Current peak radius: " << currentPeakRadius << " (deprecated exponential prediction was: " << currentPeakRadiusExp << ")" << endl;
			
            // Perform DPhi and DEta Analysis.
			hAssociatedDistribution[iSpecies][iPtBin - 1]->PerformDPhiAnalysis(gMaxDEta, currentPeakRadius, gMinFlowSubtracted[gSettings->GetBeamType()], gMaxFlowSubtracted[gSettings->GetBeamType()], kFALSE);
			hAssociatedDistribution[iSpecies][iPtBin - 1]->PerformDEtaAnalysis(currentPeakRadius);
	
            // Set the default style for half-sized figures.
            SetMyStyle("halfLargeZLabel");
            TGaxis::SetMaxDigits(3);

			// Save the associated yield as a pdf.
            hAssociatedDistribution[iSpecies][iPtBin - 1]->UseCurrentStyle();
			hAssociatedDistribution[iSpecies][iPtBin - 1]->Draw("surf1");	
			cvs->SaveAs(Form("./%s/%s/Sp%i/AssocDistribution%sPtBin%i.pdf", Form("%sGeV",gEnergy[energy]), Form("%s_Alpha%3.1f",gOutputBaseDirectory,alpha), iSpecies, gSpeciesNames[iSpecies], iPtBin));
            cvs->Clear();
	
            // Set the default style for half-sized figures.
            SetMyStyle("halfLargeYLabel");
            TGaxis::SetMaxDigits(3);

			// Save the fourier transformation (plus error band) as a pdf.
			cvs = hAssociatedDistribution[iSpecies][iPtBin - 1]->PlotBackgroundDPhiErrorBand();	
			cvs->SaveAs(Form("./%s/%s/Sp%i/DPhiDistributionDCTAndError%sPtBin%i.pdf", Form("%sGeV",gEnergy[energy]),Form("%s_Alpha%3.1f",gOutputBaseDirectory,alpha), iSpecies, gSpeciesNames[iSpecies], iPtBin));
            cvs->Clear();

			// Save the fourier transformation (decomposed) as a pdf.
			cvs = hAssociatedDistribution[iSpecies][iPtBin - 1]->PlotBackgroundDPhiDecomposed();	
			cvs->SaveAs(Form("./%s/%s/Sp%i/DPhiDistributionDCTDecomposed%sPtBin%i.pdf", Form("%sGeV",gEnergy[energy]), Form("%s_Alpha%3.1f",gOutputBaseDirectory,alpha), iSpecies, gSpeciesNames[iSpecies], iPtBin));
			cvs->SaveAs(Form("./%s/%s/Sp%i/DPhiDistributionDCTDecomposed%sPtBin%i.root", Form("%sGeV",gEnergy[energy]), Form("%s_Alpha%3.1f",gOutputBaseDirectory,alpha), iSpecies, gSpeciesNames[iSpecies], iPtBin));
            cvs->Close();
            cvs->Clear();
	
			// Save the DEta peak (SG fit).
			cvs = hAssociatedDistribution[iSpecies][iPtBin - 1]->PlotDEtaFit();	
			cvs->SaveAs(Form("./%s/%s/Sp%i/DEtaFitGauss%sPtBin%i.pdf", Form("%sGeV",gEnergy[energy]), Form("%s_Alpha%3.1f",gOutputBaseDirectory,alpha), iSpecies, gSpeciesNames[iSpecies], iPtBin));
            cvs->Clear();
	
			// Save the DEta peak (DG fit).
			cvs = hAssociatedDistribution[iSpecies][iPtBin - 1]->PlotDEtaFitDG();	
			cvs->SaveAs(Form("./%s/%s/Sp%i/DEtaFitDoubleGauss%sPtBin%i.pdf", Form("%sGeV",gEnergy[energy]), Form("%s_Alpha%3.1f",gOutputBaseDirectory,alpha), iSpecies, gSpeciesNames[iSpecies], iPtBin));
            cvs->Close();
            cvs->Clear();
	
			// Save the DPhi peak width plot.
			cvs = hAssociatedDistribution[iSpecies][iPtBin - 1]->PlotDPhiFit();	
			cvs->SaveAs(Form("./%s/%s/Sp%i/DPhiFitGauss%sPtBin%i.pdf", Form("%sGeV",gEnergy[energy]), Form("%s_Alpha%3.1f",gOutputBaseDirectory,alpha), iSpecies, gSpeciesNames[iSpecies], iPtBin));
            cvs->Clear();
	
			// Save the Flow in a plot.
			TH1F* hFlowCoefficients = hAssociatedDistribution[iSpecies][iPtBin - 1]->GetCloneDCTCoefficients();
            hFlowCoefficients->UseCurrentStyle();
            hFlowCoefficients->Draw();
			cvs->SaveAs(Form("./%s/%s/Sp%i/VnDelta%sPtBin%i.pdf", Form("%sGeV",gEnergy[energy]), Form("%s_Alpha%3.1f",gOutputBaseDirectory,alpha), iSpecies, gSpeciesNames[iSpecies], iPtBin));
            cvs->Clear();
	
            // Save the jet-peak 2D stack in a pdf.
			cvs = hAssociatedDistribution[iSpecies][iPtBin - 1]->PlotJetPeak2DStack();
			cvs->SaveAs(Form("./%s/%s/Sp%i/JetPeakStack%sPtBin%i.pdf", Form("%sGeV",gEnergy[energy]), Form("%s_Alpha%3.1f",gOutputBaseDirectory,alpha), iSpecies, gSpeciesNames[iSpecies], iPtBin));
            cvs->Clear();

			// Print the NS peak yield.
			Double_t nsYield = 0.;
			Double_t nsYieldError = 0.;
			hAssociatedDistribution[iSpecies][iPtBin - 1]->GetNSJetYield(nsYield, nsYieldError, currentPeakRadius, 0x0);
			cout << "NS peak yield: " << nsYield << " +/- " << nsYieldError << endl;

			// Print the background density
			Double_t bgdDensity = 0.;
			Double_t bgdDensityError = 0.;
			hAssociatedDistribution[iSpecies][iPtBin - 1]->GetBackgroundDensityDPhiDEta(bgdDensity, bgdDensityError);
			cout << "Background density: " << bgdDensity << " +/- " << bgdDensityError << endl;

			// Fill p_T dependent histograms.
			hNSJetYield->SetBinContent(iPtBin, nsYield);
			hNSJetYield->SetBinError(iPtBin, nsYieldError);

			hBackgroundDensity->SetBinContent(iPtBin, bgdDensity);
			hBackgroundDensity->SetBinError(iPtBin, bgdDensityError);

			Double_t dphiWidth = 0.;
			Double_t dphiWidthError = 0.;
			hAssociatedDistribution[iSpecies][iPtBin - 1]->GetNSPeakWidthDPhi(dphiWidth, dphiWidthError);
			hNSJetDPhiWidth->SetBinContent(iPtBin, dphiWidth);
			hNSJetDPhiWidth->SetBinError(iPtBin, dphiWidthError);
	
			Double_t detaFrac = 0.;
			Double_t detaFracError = 0.;
			hAssociatedDistribution[iSpecies][iPtBin - 1]->GetNSPeakDEtaFrac(detaFrac, detaFracError);
			hNSJetDEtaFrac->SetBinContent(iPtBin, detaFrac);
			hNSJetDEtaFrac->SetBinError(iPtBin, detaFracError);
	
			Double_t detaWidth = 0.;
			Double_t detaWidthError = 0.;
			hAssociatedDistribution[iSpecies][iPtBin - 1]->GetNSPeakWidthDEta(detaWidth, detaWidthError);
			hNSJetDEtaWidth->SetBinContent(iPtBin, detaWidth);
			hNSJetDEtaWidth->SetBinError(iPtBin, detaWidthError);

			Double_t detaWidth1 = 0.;
			Double_t detaWidth1Error = 0.;
			hAssociatedDistribution[iSpecies][iPtBin - 1]->GetNSPeakWidth1DEta(detaWidth1, detaWidth1Error);
			hNSJetDEtaWidth1->SetBinContent(iPtBin, detaWidth1);
			hNSJetDEtaWidth1->SetBinError(iPtBin, detaWidth1Error);

			Double_t detaWidth2 = 0.;
			Double_t detaWidth2Error = 0.;
			hAssociatedDistribution[iSpecies][iPtBin - 1]->GetNSPeakWidth2DEta(detaWidth2, detaWidth2Error);
			hNSJetDEtaWidth2->SetBinContent(iPtBin, detaWidth2);
			hNSJetDEtaWidth2->SetBinError(iPtBin, detaWidth2Error);

			for (Int_t iFlowCoefficient = 1; iFlowCoefficient <= gMaxFlowSubtracted[gSettings->GetBeamType()]; iFlowCoefficient++) {
				Double_t flowCoefficient = 0.;
				Double_t flowCoefficientError = 0.;
				hAssociatedDistribution[iSpecies][iPtBin - 1]->GetFlowCoefficient(iFlowCoefficient, flowCoefficient, flowCoefficientError);
				hVnDelta[iFlowCoefficient-1]->SetBinContent(iPtBin, flowCoefficient);	
				hVnDelta[iFlowCoefficient-1]->SetBinError(iPtBin, flowCoefficientError);	
			}

            // Print out the peak widths in DEta.
            cout << "Jet width DEta (SG, DG1, DG2): " << detaWidth << " +/- " << detaWidthError << ", " << detaWidth1 << " +/- " << detaWidth1Error << ", " << detaWidth2 << " +/- " << detaWidth2Error << endl;

        }	
        
        // Set the correct axis labels on the spectra. Divide the spectra through the number of triggers if available.
        //if (nTriggers > 1.) {
		    hNSJetYield->GetYaxis()->SetTitle("#frac{1}{N_{trig}}#kern[0.05]{#frac{dN_{jet}}{dp_{T}}}");
        //} else {
         //   hNSJetYield->GetYaxis()->SetTitle("#frac{dN_{jet}}{dp_{T}}");
          //  cout << "ERROR: It seems like something went wrong with determining the number of triggers..." << endl;
        //}

		hNSJetYield->Scale(1., "width");
		hBackgroundDensity->Scale(1.,"width");
		hBackgroundDensity->GetYaxis()->SetTitle("#frac{1}{N_{trig}}#kern[0.05]{#frac{d^{3}N_{bulk}}{dp_{T}d#Delta#eta d#Delta#phi}}");		
		
        fOut->cd();
	    
		hNSJetYield->Write();

		hNSJetDPhiWidth->Write();
		hNSJetDEtaWidth->Write();

		hNSJetDEtaFrac->Write();
	
		hNSJetDEtaWidth1->Write();
		hNSJetDEtaWidth2->Write();
        
		hBackgroundDensity->Write();

		for (Int_t iFlowCoefficient = 1; iFlowCoefficient <= gMaxFlowSubtracted[gSettings->GetBeamType()]; iFlowCoefficient++) {
			hVnDelta[iFlowCoefficient-1]->Write();
		}		

	}

}

