#!/bin/bash

# This script syncs all analysis code to and from quark.
# One can pass any additional commands to the script, for example --dry-run ($1).
# a = Archive, meaning that creating time, owner, etc. are preserved.

# Sync AnalysisMacros.  
/usr/local/bin/rsync -avzru $1 ./AnalysisMacros/*.C veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/AnalysisMacros
/usr/local/bin/rsync -avzru $1 ./AnalysisMacros/*.sh veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/AnalysisMacros

#/usr/local/bin/rsync -avzru $1 veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/AnalysisMacros/*.C ./AnalysisMacros
#/usr/local/bin/rsync -avzru $1 veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/AnalysisMacros/*.sh ./AnalysisMacros

# Sync Libraries.  
/usr/local/bin/rsync -avzru $1 ./Libs/*.cxx veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/Libs
/usr/local/bin/rsync -avzru $1 ./Libs/*.h veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/Libs
/usr/local/bin/rsync -avzru $1 ./Libs/make.C veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/Libs

#/usr/local/bin/rsync -avzru $1 veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/Libs/*.cxx ./Libs
#/usr/local/bin/rsync -avzru $1 veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/Libs/*.h ./Libs
#/usr/local/bin/rsync -avzru $1 veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/Libs/make.C ./Libs

# Sync run lists.
/usr/local/bin/rsync -avzru $1 ./RunLists/*.txt veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/RunLists
#/usr/local/bin/rsync -avzru $1 veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/RunLists/*.txt ./RunLists

# Sync run macros.
/usr/local/bin/rsync -avzru $1 ./AnalysisMacros/runMacros/*.C veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/AnalysisMacros/runMacros
#/usr/local/bin/rsync -avzru $1 veldh122@quark.science.uu.nl:/home/staff/veldh122/DiHadronPID/AnalysisMacros/runMacros/*.C ./AnalysisMacros/runMacros
